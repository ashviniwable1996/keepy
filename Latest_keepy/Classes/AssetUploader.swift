//
//  AssetUploader.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 11/20/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import AWSS3
import Mixpanel

class AssetUploader: NSObject, URLSessionTaskDelegate {
    fileprivate let accessKey = "AKIAIMK5EH2KQAJL4DXA"
    fileprivate let secretKey = "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"  // Until presignedUrl is given by our server API.
    fileprivate var retainCycle: AssetUploader!
    
    var uploadOperations: [Operation] = []
    
    let fileUrl: URL
    let assetId: Int
    let assetHash: String
    let key: String
    let contentType: String
    var task: URLSessionUploadTask?
    
    init(fileUrl aFileUrl: URL, assetId anAssetId: Int, assetHash anAssetHash: String, video: Bool) {
        fileUrl = aFileUrl
        assetId = anAssetId
        assetHash = anAssetHash
        if video {
            key = "assets/\(assetHash).mp4"
            contentType = "video/mp4"
        } else {
            key = "assets/\(assetHash).jpg"
            contentType = "image/jpg"
        }
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.usEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        super.init()
        retainCycle = self
    }
    
    convenience init(sessionIdentifier: String) {
        let arr = sessionIdentifier.components(separatedBy: " ")
        if arr.count == 3 {
            self.init(fileUrl: URL(string: arr[2])!, assetId: Int(arr[0])!, assetHash: arr[1], video: true)
        } else {
            self.init(fileUrl: URL(string: "")!, assetId: 0, assetHash: "", video: true)  // Kludge
        }
    }
    
    // MARK: -
    
    func upload() {
        let conf = URLSessionConfiguration.background(withIdentifier: "\(assetId) \(assetHash) \(fileUrl.absoluteString)")
        let sess = Foundation.URLSession(configuration: conf, delegate: self, delegateQueue: OperationQueue())
        
        let preSignedReq: AWSS3GetPreSignedURLRequest! = AWSS3GetPreSignedURLRequest()
        preSignedReq.bucket = Defaults.isDevMode ? "keepy-dev" : "keepy"
        preSignedReq.setValue("public-read", forRequestParameter: "x-amz-acl")
        preSignedReq.key = key
        
//        UploadTracker.sharedInstance.addItemToWatchList("\(assetId)")
        
        preSignedReq.contentType = contentType
        preSignedReq.expires = Date(timeIntervalSinceNow: 60*60)
        preSignedReq.httpMethod = AWSHTTPMethod.PUT
        
        let urlBuilder = AWSS3PreSignedURLBuilder.default()!
        urlBuilder.getPreSignedURL(preSignedReq).continue({ (task) -> AnyObject! in
            if task?.error != nil {
                print("getPreSignedURL error: \(task?.error)")
                self.cleanup()
                return nil
            }
            
            let preSignedUrl = task?.result as! URL
            print("preSignedUrl: %@", preSignedUrl)
            
            let request = NSMutableURLRequest(url: preSignedUrl)
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "PUT"
            request.setValue(preSignedReq.contentType, forHTTPHeaderField: "Content-Type")
            //request.setValue(, forHTTPHeaderField: "Content-Type")
            //
            print("FNTRACE ASSETUPLOADER fileurl: \(self.fileUrl)")
            let task = sess.uploadTask(with: request as URLRequest, fromFile: self.fileUrl)
            self.task = task
            task.resume()
            return nil
        })
    }
    
    fileprivate func cleanup() {
        retainCycle = nil
        do { try FileManager.default.removeItem(at: fileUrl) } catch {}
    }
    
    // MARK: - NSURLSessionDelegate
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        if error != nil
        {
            print("------ URLSession:didBecomeInvalidWithError!!: %@", (error as? NSError)?.debugDescription)
        }
        else
        {
            print("------ URLSession:didBecomeInvalidWithError")
        }
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        print("------ URLSession:didReceiveChallenge")
        completionHandler(.performDefaultHandling, nil);
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        print("------ URLSessionDidFinishEventsForBackgroundURLSession")
    }
    
    // MARK: - NSURLSessionTaskDelegate
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        //print("URLSession:task:didReceiveChallenge")
        completionHandler(.performDefaultHandling, nil);
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let dict: [AnyHashable : Any] =  ["totalBytesWritten": NSNumber(value: totalBytesSent as Int64), "totalBytesExpectedToWrite": NSNumber(value: totalBytesExpectedToSend as Int64),
            "assetId" : assetId, "assetHash": assetHash
        ]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "VideoUploadProgress"), object: nil, userInfo: dict)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        print("------  URLSession:task:didCompleteWithError If error:\(error?.localizedDescription)")
        
//        UploadTracker.sharedInstance.removeItemToWatchList(key)
        
        if let e = error {
            Mixpanel.sharedInstance()?.track("assetuploader-error", properties: ["error" : e.localizedDescription])
            print("failed to upload: \(e)")
        } else {
            commit()
        }
        
        session.invalidateAndCancel()
        cleanup()
    }
    
    // MARK: - 
    
    func commit() {
        NSLog("*************** commit")
        let assetId = UInt(self.assetId)
        
        let countOfBytesSent = Int(self.task?.countOfBytesSent ?? 0)
        
        print(countOfBytesSent)
        
        print(countOfBytesSent)
        
        after(3) {
            
            
            ServerComm.instance().commitAsset(UInt(assetId), andFileSize: countOfBytesSent, withSuccessBlock: { response in
                    print("commit: \(response)")
                }, andFail: { (error: Error!) in
                    print("commit error: \(error)")
                    Mixpanel.sharedInstance()?.track("assetuploader-error", properties: ["error" : "\(error.localizedDescription)"])
                }, onHandler: {
            })
        }
        cleanup()
    }
}
