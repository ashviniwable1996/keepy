 //
//  AddItemOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/1/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Photos

class AddItemOperation: AsyncOperation {
    fileprivate var item: StoryItem
    
    init(item anItem: StoryItem) {
        item = anItem
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        isExecuting = true
        
        if item.type == .video {
            addVideo()
        } else {
            addItem()
        }
    }
    
    // MARK: -
    fileprivate func addItem() {
        
        //add the actual image upload to the queue
//        UploadTracker.sharedInstance.addItemToWatchList("--\(item.assetId)")
        
        ServerComm.instance().addItem(item.title,
            selectedKids: item.kids,
            andDate: item.date,
            andPlace: item.placeId,
            andCropRect: item.pointsAsString(),
            andOriginalImageHash: item.assetHash,
            andOriginalAssetId: item.assetId,
            andRotation: item.editInfo.rotation,
            andSat: item.editInfo.saturation,
            andCon: item.editInfo.contrast,
            andBri: item.editInfo.brightness,
            andAutoEnhanceOn: item.editInfo.autoEnhance,
            andItemType: UInt(item.type.rawValue),
            andExtraData: item.extraData,
            andShareWithFans: item.share,
            andTags: Array(item.tags),
            andExtraParams: [:],  // TBD: item.extraParams,
            withSuccessBlock: { response in
                
//                UploadTracker.sharedInstance.removeItemToWatchList("--\(self.item.assetId)")
                
                let addItemResponse = AddItemResponse(response: response as! AnyObject)
                
                switch addItemResponse {
                case .result(let itemId, let assetId, let assetHash, let originalAssetHash, let originalAssetId):
                    self.item.itemId = itemId
                    self.item.assetHash = assetHash
                    self.item.originalAssetHash = originalAssetHash
                    self.item.assetId = assetId
                    self.item.originalAssetId = originalAssetId

                    NSLog("^^^^^^^^^^^^^^^^^^^^^^^^^^^^ assetId: \(assetId), originalAssetId: \(originalAssetId)")
                    NSLog("^^^^^^^^^^^^^^^^^^^^^^^^^^^^ assetHash: \(assetHash), originalAssetHash: \(originalAssetHash)")
                    
                    NSLog("addItem: itemId(\(itemId))")
                    
                case .error(let code, let message):
                    NSLog("additem/error: \(code) / \(message)")
                    self.error = NSError(string: message, code: code)
                }
                self.finishOperation()
                
            }, andFail: { (e: Error!) in
                NSLog("additem/failure: \(e)")
                self.error = e
                self.finishOperation()
        })
    }
    
    fileprivate func addVideo() {
        ServerComm.instance().addVideo(0, withTitle: item.title, selectedKids: item.kids, andDate: item.date, andPlace: item.placeId, andCropRect: "", andVideoAssetId: Int(item.assetId), andThumpAssetId: Int(item.thumbnailAssetId), andAutoEnhanceOn: false, andItemType: Int(item.type.rawValue), andExtraData: item.extraData, andShareWithFans: item.share, andTags: Array(item.tags), andDuration: String(Int(item.asset!.duration)), withSuccessBlock: { response in
            
            let addItemResponse = AddVideoItemResponse(response: response as! AnyObject)
            
            switch addItemResponse {
            case .result(let itemId, _/*assetId*/, let assetHash, let originalAssetHash, let originalAssetId, let sdVideoAssetId, let hdVideoAssetId, let sdVideoAssetHash, let hdVideoAssetHash, let short_url):
                self.item.itemId = itemId
                self.item.assetHash = assetHash
                self.item.originalAssetHash = originalAssetHash
                self.item.originalAssetId = originalAssetId
                self.item.sdVideoAssetId = sdVideoAssetId
                self.item.hdVideoAssetId = hdVideoAssetId
                self.item.sdVideoAssetHash = sdVideoAssetHash
                self.item.hdVideoAssetHash = hdVideoAssetHash
                self.item.short_url = short_url
                
                NSLog("addItem: itemId(\(itemId))")
                
            case .error(let code, let message):
                NSLog("additem/error: \(code) / \(message)")
                self.error = NSError(string: message, code: code)
            }
            self.finishOperation()
            
        }) { (e: Error!) in
            NSLog("addvideo/failure: \(e)")
            self.error = e
            self.finishOperation()
        }
    }
}

// MARK: -
enum AddItemResponse {
    // {"status":"-1","result":null,"error":{"status":"-1","error":"not authorized"}}
    
    case result (UInt, UInt, String, String, UInt)
    case error (Int, String)
    init(response: AnyObject) {
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status != 0 {
                if let error = dict["error"] as? [String : AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            } else {
                if let result = dict["result"] as? [String:AnyObject] {
                    if let assetId = result["assetId"] as? UInt,
                        let itemId = result["id"] as? UInt,
                        let assetHash = result["assetHash"] as? String,
                        let originalAssetHash = result["originalAssetHash"] as? String,
                        let originalAssetId = result["originalAssetId"] as? String {
                            self = .result(itemId, assetId, assetHash, originalAssetHash, UInt(originalAssetId)!)
                    } else {
                        self = .error(-1, "something went wrong")
                    }
                } else {
                    self = .error(-2, "something went wrong")
                }
            }
        } else {
            self = .error(-3, "something went wrong")
        }
    }
}

// MARK: -
// {"status":0,
//  "result":{
//      "id":310427,
//      "createDate":"2015-10-19T17:24:13.230Z",
//      "assetId":939939,
//      "originalAssetId":"939937",
//      "assetHash":"3771D390768611E59CEC158C17110041",
//      "originalAssetHash":"0BE9E000768611E59CEC158C17110041",
//      "sdVideoAssetId":939940,
//      "hdVideoAssetId":"939938",
//      "sdVideoAssetHash":"37768E80768611E59CEC158C17110041",
//      "hdVideoAssetHash":"0C0DBBB0768611E59CEC158C17110041",
//      "short_url":"http://dev.keepy.me/w.html?path=Y2FyZC8zNzcxRDM5MDc2ODYxMUU1OUNFQzE1OEMxNzExMDA0MT9pdGVtSWRzPTMxMDQyNw=="},
//  "error":null}
enum AddVideoItemResponse {
    
    case result (UInt, UInt, String, String, UInt, Int, String, String, String, String)
    case error (Int, String)
    init(response: AnyObject) {
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status != 0 {
                if let error = dict["error"] as? [String : AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            } else {
                if let result = dict["result"] as? [String:AnyObject] {
                    if let assetId = result["assetId"] as? UInt,
                        let itemId = result["id"] as? UInt,
                        let assetHash = result["assetHash"] as? String,
                        let originalAssetHash = result["originalAssetHash"] as? String,
                        let originalAssetId = result["originalAssetId"] as? String,
                        let sdVideoAssetId = result["sdVideoAssetId"] as? Int,
                        let hdVideoAssetId = result["hdVideoAssetId"] as? String,
                        let sdVideoAssetHash = result["sdVideoAssetHash"] as? String,
                        let hdVideoAssetHash = result["hdVideoAssetHash"] as? String,
                        let short_url = result["short_url"] as? String
                    {
                        self = .result(itemId, assetId, assetHash, originalAssetHash, UInt(originalAssetId)!, sdVideoAssetId, hdVideoAssetId, sdVideoAssetHash, hdVideoAssetHash, short_url)
                    } else {
                        self = .error(-1, "something went wrong")
                    }
                } else {
                    self = .error(-2, "something went wrong")
                }
            }
        } else {
            self = .error(-3, "something went wrong")
        }
    }
}
