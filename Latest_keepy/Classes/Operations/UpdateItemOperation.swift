//
//  UpdateItemOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/13/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import SDWebImage

class UpdateItemOperation: AsyncOperation {
    var item: KPLocalItem
    var info: ImageEditInfo
    var tags: [UInt]
    
    init(item anItem: KPLocalItem, tags aTags: [UInt], info anInfo: ImageEditInfo) {
        item = anItem
        info = anInfo
        tags = aTags
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        isExecuting = true
        
        // This is so stupid but until things got cleared and efficient...
        // Download the original photo to re-cache it with new asset name. Dumb.
        let la = KPLocalAsset.fetch(byLocalID: UInt(self.item.imageAssetID))!
        let manager = SDWebImageManager.shared()!
        let url = "\(Defaults.assetsURLString)\(la.urlHash()).jpg"
        manager.downloadImage(with: URL(string: url), options: SDWebImageOptions(rawValue: 0),
            progress: { (receivedSize: Int, expectedSize: Int) in
                
            },
            completed: { (image: UIImage!, error: Error!, cacheType: SDImageCacheType, finished: Bool, url: URL!) in
                if let image = image {
                    self.info.originalImage = image
                    self.update()
                    
                } else {
                    NSLog("image not found: \(url)")
                }
        })
    }
    
    fileprivate func update() {
        
        ServerComm.instance().update(item, imageChanged: true, withTags: tags, andExtraParams: nil, withSuccessBlock: { result in
            let response = result as AnyObject
            if let assetId = (response.value(forKey: "result") as AnyObject).value(forKey: "assetId") as? Int,
                let hash = (response.value(forKey: "result") as AnyObject).value(forKey: "assetHash") as? String {
                let imageAsset = KPDataParser.instance().createAsset(byId: assetId, withType: 0, withHash: hash)!
                self.item.imageAssetID = Int(imageAsset.identifier)
                self.item.saveChanges()
                NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()
                
                if let image = self.info.editedImage ?? self.info.originalImage {
                    cacheImages(image, hash: hash)
                } else {
                    NSLog("BUG! - no image to cache")
                }
            }
            
            self.finishOperation()
            
        }) { (e: Error!) -> Void in
            self.error = e
            self.finishOperation()
        }
        
//        
//        ] updateItem:self.item imageChanged:(self.changedImage != nil) withTags:(self.tagsChanged) ? self.tagIds : nil andExtraParams:self.editedImageInfoExtraParams withSuccessBlock:^(id result) {
//            
//            
//            if (self.tagsChanged) {
//            
//            NSArray *tagIDs = self.tagIds;
//            
//            // NSArray *newItemTags = [KPLocalTag idsToObjects:self.tagIds];
//            NSMutableArray *newItemTags = @[].mutableCopy;
//            for(NSNumber *currentTagID in tagIDs){
//            
//            int currentServerID = currentTagID.intValue;
//            KPLocalTag *currentTag = [KPLocalTag fetchByServerID:currentServerID];
//            
//            [newItemTags addObject:currentTag];
//            }
//            
//            NSArray *originalItemTagIDs = [KPLocalItemTag fetchTagIDsForItem:self.item];
//            NSMutableArray *originalItemTags = [KPLocalTag idsToObjects:originalItemTagIDs].mutableCopy;
//            
//            [newItemTags addObjectsFromArray:originalItemTags];
//            [KPLocalItemTag setLinkedTags:newItemTags forItem:self.item];
//            
//            }
//            
//            if (self.storyChanged) {
//            [self uploadStory:NO];
//            }
//            else if (self.videoStoryChanged) {
//            [self uploadVideoStory:NO];
//            }
//            else {
//            [self updateItemSuccess];
//            }
//            
//            [self.item saveChanges];

    }
}

// {"status":0,"result":{"assetId":939421,"assetHash":"F411568071E311E5AB66E972304F1ECD","originalAssetId":"939309","originalAssetHash":"8A335220713611E5A3B2736CA2E920F0"},"error":null}

func cacheImages(_ image: UIImage, hash: String) {
    let name = "\(Defaults.assetsURLString)\(hash)"
    
    SDImageCache.shared().store(image, forKey:"\(name).jpg")
    SDImageCache.shared().store(image, forKey:"\(name)_iphone.jpg")
    
    let s = image.imageScaled(toFit: CGSize(width: 192, height: 192))
    SDImageCache.shared().store(s, forKey:"\(name)_small.jpg")
    
    let m = image.imageScaled(toFit: CGSize(width: 600, height: 600))
    SDImageCache.shared().store(m, forKey:"\(name)_medium.jpg")
}
