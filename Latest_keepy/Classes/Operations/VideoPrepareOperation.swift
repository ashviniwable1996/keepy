//
//  VideoPrepareOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/9/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class VideoPrepareOperation: PrepareOperation {
    fileprivate let item: StoryItem
    
    var index = -1
    
    init(item anItem: StoryItem) {
        item = anItem
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        
        isExecuting = true
        prepare()
    }
    
    // MARK: -
    
    func prepare() {
        if isCancelled { NSLog("cancelled"); finishOperation(); return }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UploadOperation"), object: nil, userInfo: ["index":self.index, "title":"video"])
        
        ServerComm.instance().prepareVideoAsset( { response in
            
            switch PrepareAssetResponse(response: response as AnyObject) {
            case .result(let assetId, let assetHash, let policy, let policyHash):
                self.item.assetId = assetId
                self.item.assetHash = assetHash
                
                print("prepare: assetId(\(assetId)) hash(\(assetHash))")
                self.upload(assetId, assetHash: assetHash, policy: policy, policyHash: policyHash)
                
                self.prepareImage()
                
            case .error(let status, let message):
                print("prepare/error: status(\(status)) \(message)")
                self.error = NSError(string: message, code: status)
                self.finishOperation()
            }
            
        }, andFail: { (e: Error!) in
            print("prepare/failure: \(e)")
            self.error = e
            self.finishOperation()
        })
    }
    
    func upload(_ assetId: UInt, assetHash: String, policy: String, policyHash: String) {
        self.item.requestUrl() {
            guard let url = $0 else { return }
            AssetUploader(fileUrl: url, assetId: Int(assetId), assetHash: assetHash, video: true).upload()
        }
    }
    
    func prepareImage() {
        if isCancelled { print("cancelled"); finishOperation(); return }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UploadOperation"), object: nil, userInfo: ["index":self.index, "title":"photo"])
        
        print("prepare: item(\(item.type.rawValue))")
        ServerComm.instance().prepareAsset( { response in
            
            switch PrepareAssetResponse(response: response as AnyObject) {
            case .result(let assetId, let assetHash, let policy, let policyHash):
                print("prepare: assetId(\(assetId)) hash(\(assetHash))")
                
                self.item.thumbnailAssetId = assetId  // Finally, save it.
                self.item.thumbnailAssetHash = assetHash
                
                self.uploadImage(assetId, assetHash: assetHash, policy: policy, policyHash: policyHash)
                
            case .error(let status, let message):
                print("prepare/error: status(\(status)) \(message)")
                self.error = NSError(string: message, code: status)
            }
            self.finishOperation()
            
            }, andFail: { (e: Error!) in
                print("prepare/failure: \(e)")
                self.error = e
                self.finishOperation()
        })
    }
    
    func uploadImage(_ assetId: UInt, assetHash: String, policy: String, policyHash: String) {
        self.item.requestImageData() {
            guard let data = $0 else { return }
            print("FNTRACE UPLOADIMAGE : \(assetHash)")
            let op = ImageUploadOperation(data: data, assetId: assetId, assetHash: assetHash, policy: policy, policyHash: policyHash)
            self.uploadOperations.append(op)
        }
    }
}

// MARK: - Response

private enum PrepareAssetResponse {
    case result (UInt, String, String, String)
    case error (Int, String)
    init(response: AnyObject) {
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status == 0 {
                if let result = dict["result"] as? [String:AnyObject],
                    let assetId = result["id"] as? UInt,
                    let hash = result["hash"] as? String,
                    let policy = result["policy"] as? String,
                    let policyHash = result["policyHash"] as? String {
                        self = .result(assetId, hash, policy, policyHash)
                } else {
                    self = .error(-1, "something went wrong")
                }
            } else {
                if let error = dict["error"] as? [String:AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            }
        } else {
            self = .error(-2, "something went very wrong")
        }
    }
}
