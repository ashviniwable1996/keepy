//
//  FinalProduct.swift
//  Keepy
//
//  Created by Aakash Wadhwa on 04/05/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

import UIKit
struct Product {
    var imageURL = String()
    var productSKU = String()
    var productPrice = String()
    var productHighEndMultiplicationFactor = String()
    var productName = String()
}
class FinalProduct: NSObject {
//imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/1930939be47b58bfa6913defebb3b91b.png","sku":"PhoneCase-iphone6Plus-Gloss", "price":"11", "productId":"61", "highPrice":"25","name":"PhoneCase-iphone6Plus"
    
    static let shared = FinalProduct()
   
    
    var finalProductArray = [Product] ()
}
