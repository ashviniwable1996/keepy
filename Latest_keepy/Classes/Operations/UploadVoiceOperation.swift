//
//  UploadVoiceOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/7/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class UploadVoiceOperation: AsyncOperation {
    fileprivate let item: KPLocalItem
    fileprivate var voiceRecordingLength: Int
    fileprivate var voiceRecordingData: Data
    
    init(item anItem: KPLocalItem, length: Int, data: Data) {
        item = anItem
        voiceRecordingLength = length
        voiceRecordingData = data
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        
        isExecuting = true
        async { self.upload() }  // Needs to run in main thread. WTF.
    }
    
    func upload() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UploadOperation"), object: nil, userInfo: ["title":"voice story"])
        
        ServerComm.instance().uploadStory(item.serverID, withStoryLength: voiceRecordingLength, with: voiceRecordingData, withSuccessBlock: { response in
            switch UploadVoiceResponse(response: response as! AnyObject) {
            case .result(let storyId, let assetId, let hash):
                let asset = KPLocalAsset.create()!
                asset.serverID = assetId
                asset.assetHash = hash
                asset.assetType = 2
                
                let story = KPLocalStory.create()!
                story.serverID = storyId
                story.storyLength = self.voiceRecordingLength
                story.assetID = Int(asset.identifier)
                
                self.item.storyID = Int(story.identifier)
                
                ServerComm.instance().saveAssetData(self.voiceRecordingData, withFileExt: "mp4", andAssetHash: asset.urlHash())
                
                asset.saveChanges()
                story.saveChanges()
                self.item.saveChanges()
                
                NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()

            case .error(let status, let message):
                NSLog("uploadStory/error: status(\(status)) message(\(message))")
                self.error = NSError(string: message, code: status)
            }
            self.finishOperation()
            
        }, andFail: { (error: Error!) in
            NSLog("failed to upload voice data: \(error)")
            self.error = error
            self.finishOperation()
            //[SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem uploading audio story, please mare sure you are connected to the internet and try again")];
            
        }) { (bytesWritten: UInt, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) in
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UploadOperation"), object: nil,
                userInfo: ["title":"voice story", "bytesWritten":Int(bytesWritten), "totalBytesWritten":Int(totalBytesWritten), "totalBytesExpectedToWrite":Int(totalBytesExpectedToWrite)])
        }
    }
}

// MARK: - Response
// {"status":0,"storyId":34557,"assetId":4614167,"hash":"2D42FDF06DED11E599DBC9E031A1DAB4","error":null}

private enum UploadVoiceResponse {
    case result (UInt, UInt, String)
    case error (Int, String)
    init(response: AnyObject) {
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status == 0 {
                if let storyId = dict["storyId"] as? UInt, let assetId = dict["assetId"] as? UInt, let hash = dict["hash"] as? String {
                    if storyId > 0 && assetId > 0 {
                        self = .result(storyId, assetId, hash)
                    } else {
                        self = .error(-501, "something went wrong")
                    }
                } else {
                    self = .error(-502, "something went wrong")
                }
            } else {
                if let error = dict["error"] as? [String:AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            }
        } else {
            self = .error(-503, "something went very wrong")
        }
    }
}
