//
//  VideoUploadOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class VideoUploadOperation: AsyncOperation {
    fileprivate let data: Data, assetId: UInt, assetHash: String, policy: String, policyHash: String
    
    init(data aData: Data, assetId anAssetId: UInt, assetHash anAssetHash: String, policy aPolicy: String, policyHash aPolicyHash: String) {
        data = aData
        assetId = anAssetId
        assetHash = anAssetHash
        policy = aPolicy
        policyHash = aPolicyHash
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        isExecuting = true
        upload()
    }
    
    // MARK: -
    
    func upload() {
        ServerComm.instance().uploadVideoAsset(assetHash, with: data, andPolicy: policy, andSignature: policyHash, withSuccessBlock: { response in
            
            NSLog("UploadVideo/uploaded: \(self.assetHash)")
            
            // Just give few seconds to the server to process the video file on S3
            // until the whole uploading process will be fully refactored and simplified.
            after(3) {
                self.commit()
            }
            
            }, andFail: { (e: Error!) in
                NSLog("UploadVideo/failure: \(e)")
                self.error = e
                self.finishOperation()
                
            }, onProgress: { (bytesWritten: UInt, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) in
                if totalBytesWritten == totalBytesExpectedToWrite {
                    NSLog("UploadVideo/uploaded: \(totalBytesExpectedToWrite) bytes uploaded")
                }
                let dict: [AnyHashable : Any] =  ["totalBytesWritten": NSNumber(value: totalBytesWritten as Int64), "totalBytesExpectedToWrite":NSNumber(value: totalBytesExpectedToWrite as Int64),
                    "assetId" : self.assetId, "assetHash": self.assetHash
                ]
                NotificationCenter.default.post(name: Notification.Name(rawValue: "VideoUploadProgress"), object: nil, userInfo: dict)
                //                NSNotificationCenter.defaultCenter().postNotificationName("UploadOperation", object: nil,
                //                    userInfo: ["index":self.index, "title":"video", "bytesWritten":Int(bytesWritten), "totalBytesWritten":Int(totalBytesWritten), "totalBytesExpectedToWrite":Int(totalBytesExpectedToWrite)])
            }) {
                // WTF
        }
    }
    
    func commit() {
        ServerComm.instance().commitAsset(assetId, andFileSize: data.count, withSuccessBlock: { response in
            
            switch CommitAssetResponse(response: response as AnyObject) {
            case .error(let status, let message):
                NSLog("UploadVideo/error: status(\(status)) message(\(message))")
                self.error = NSError(string: message, code: status)
                
            case .success:
                NSLog("UploadVideo/success")
            }
            self.finishOperation()
            
            }, andFail: { (e: Error!) in
                NSLog("UploadVideo/failure: \(e)")
                self.error = e
                self.finishOperation()
                
            }) {
                NSLog("UploadVideo/handler")  // TODO: what is this for?
        }
    }
}

private enum CommitAssetResponse {
    case success
    case error (Int, String)
    init(response: AnyObject) {
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status == 0 {
                self = .success
            } else {
                if let error = dict["error"] as? [String:AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            }
        } else {
            self = .error(-1, "something went very wrong")
        }
    }
}
