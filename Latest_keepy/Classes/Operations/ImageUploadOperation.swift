//
//  ImageUploadOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class ImageUploadOperation: AsyncOperation {
    fileprivate let data: Data, assetId: UInt, assetHash: String, policy: String, policyHash: String
    
    init(data aData: Data, assetId anAssetId: UInt, assetHash anAssetHash: String, policy aPolicy: String, policyHash aPolicyHash: String) {
        print("UploadImage STarted1")
        data = aData
        assetId = anAssetId
        assetHash = anAssetHash
        policy = aPolicy
        policyHash = aPolicyHash
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        isExecuting = true
        upload()
    }
    
    // MARK: -
    
    func upload() {
        
        print("UploadImage STarted2")
        ServerComm.instance().uploadAsset(assetHash, with: data, andPolicy: policy, andSignature: policyHash, withSuccessBlock: { response in
            
            print("UploadImage/uploaded: \(self.assetHash)")
            self.commit()
            
            }, andFail: { (e: Error!) in
                print("UploadImage/failure: \(e)")
                self.error = e
                self.finishOperation()
                
            }) { (bytesWritten: UInt, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) in
        }
    }
    
    func commit() {
        print("UploadImage STarted3")
        ServerComm.instance().commitAsset(assetId, andFileSize: data.count, withSuccessBlock: { response in
            
            switch CommitAssetResponse(response: response as AnyObject) {
            case .error(let status, let message):
                print("UploadImage/error: status(\(status)) message(\(message))")
                self.error = NSError(string: message, code: status)
                
            case .success:
                print("UploadImage/success")
            }
            self.finishOperation()
            
            }, andFail: {
                (e: Error!) in
                print("UploadImage/failure: \(e)")
                self.error = e
                self.finishOperation()
                
            }) {
                print("UploadImage/handler")  // TODO: what is this for?
        }
        
    }
}

private enum CommitAssetResponse {
    case success
    case error (Int, String)
    init(response: AnyObject) {
        print("UploadImage STarted4")
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status == 0 {
                self = .success
            } else {
                if let error = dict["error"] as? [String:AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            }
        } else {
            self = .error(-1, "something went very wrong")
        }
    }
}
