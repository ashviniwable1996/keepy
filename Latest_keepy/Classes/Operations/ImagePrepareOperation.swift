//
//  ImagePrepareOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/1/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class PrepareOperation: AsyncOperation {
    var uploadOperations: [Operation] = []
}

class ImagePrepareOperation: PrepareOperation {
    fileprivate let item: StoryItem
    
    var index = -1
    
    init(item anItem: StoryItem) {
        item = anItem
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        
        isExecuting = true
        prepare()
    }
    
    // MARK: -
    
    func upload(_ assetId: UInt, assetHash: String, policy: String, policyHash: String) {
        self.item.requestUrl() {
            guard let url = $0 else { return }
            AssetUploader(fileUrl: url, assetId: Int(assetId), assetHash: assetHash, video: false).upload()
        }
    }

    func prepare() {
        if isCancelled { NSLog("cancelled"); finishOperation(); return }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UploadOperation"), object: nil, userInfo: ["index":self.index, "title":"photo"])
        
        NSLog("prepare: item(\(item.type.rawValue))")
        ServerComm.instance().prepareAsset( { response in
            
            switch PrepareAssetResponse(response: response as! AnyObject) {
            case .result(let assetId, let assetHash, let policy, let policyHash):
                NSLog("prepare: assetId(\(assetId)) hash(\(assetHash))")
                
                self.item.assetId = assetId  // Finally, save it.
                self.item.assetHash = assetHash
                
                self.upload(assetId, assetHash: assetHash, policy: policy, policyHash: policyHash)

            case .error(let status, let message):
                NSLog("prepare/error: status(\(status)) \(message)")
                self.error = NSError(string: message, code: status)
            }
            self.finishOperation()
            
        }, andFail: { (e: Error!) in
            NSLog("prepare/failure: \(e)")
            self.error = e
            self.finishOperation()
        })
    }
}

// MARK: - Response

private enum PrepareAssetResponse {
    case result (UInt, String, String, String)
    case error (Int, String)
    init(response: AnyObject) {
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status == 0 {
                if let result = dict["result"] as? [String:AnyObject],
                    let assetId = result["id"] as? UInt,
                    let hash = result["hash"] as? String,
                    let policy = result["policy"] as? String,
                    let policyHash = result["policyHash"] as? String {
                        self = .result(assetId, hash, policy, policyHash)
                } else {
                    self = .error(-1, "something went wrong")
                }
            } else {
                if let error = dict["error"] as? [String:AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            }
        } else {
            self = .error(-2, "something went very wrong")
        }
    }
}
