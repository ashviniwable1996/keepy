//
//  UploadVideoCommentOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/8/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import SDWebImage
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class UploadVideoCommentOperation: AsyncOperation {
    
    fileprivate let item: KPLocalItem
    fileprivate var videoRecordingLength: Int
    fileprivate var videoRecordingData: Data
    fileprivate var previewImage: UIImage
    
    init(item anItem: KPLocalItem, length: Int, data: Data, preview: UIImage) {
        item = anItem
        videoRecordingLength = length
        videoRecordingData = data
        previewImage = preview
        super.init()
    }
    
    override func start() {
        if isCancelled {
            isFinished = true
            return
        }
        
        isExecuting = true
        async { self.upload() }  // Needs to run in main thread. WTF.
    }
    
    func upload() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UploadOperation"), object: nil,
            userInfo: ["title":"video story"])
        
        ServerComm.instance().uploadItemComment(item.serverID, withVideo: videoRecordingData, andPreviewImage: previewImage, andCommentLength: videoRecordingLength, withType: 0, isStory: true, withExtraData: "", withSuccessBlock: { response in
            switch UploadVideoResponse(response: response as! AnyObject) {
            case .result(let commentId, let assetId, let hash, let previewAssetId, let previewAssetHash):
                
                let comment = KPLocalComment.create()!
                comment.serverID = commentId
                comment.commentDate = Date().timeIntervalSince1970
                comment.commentLength = self.videoRecordingLength
                comment.wasRead = true
                comment.isStory = true
                comment.commentType = 0
                
                let asset = KPLocalAsset.create()!
                asset.serverID = assetId
                asset.assetHash = hash
                asset.assetType = 1
                asset.saveChanges()
                
                comment.videoAssetID = Int(asset.identifier)
                ServerComm.instance().saveAssetData(self.videoRecordingData, withFileExt: "mov", andAssetHash: asset.urlHash())
                
                let asset2 = KPLocalAsset.create()!
                asset2.serverID = previewAssetId
                asset2.assetHash = previewAssetHash
                asset2.assetType = 0
                asset2.saveChanges()
                comment.previewImageAssetID = Int(asset2.identifier)
                
                SDImageCache.shared().store(self.previewImage, forKey:"\(Defaults.assetsURLString)\(asset.urlHash).jpg")
                
                comment.itemID = Int(self.item.identifier)
                comment.saveChanges()
                
            NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()

            case .error(let status, let message):
                NSLog("uploadStory/error: status(\(status)) message(\(message))")
                self.error = NSError(string: message, code: status)
            }
            self.finishOperation()
            
        }, andFail: { (error: Error!) in
            NSLog("failed to upload voice data: \(error)")
            self.error = error
            self.finishOperation()
                //[SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem uploading audio story, please mare sure you are connected to the internet and try again")];
            
        }) { (bytesWritten: UInt, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) in
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UploadOperation"), object: nil,
                userInfo: ["title":"video story", "bytesWritten":Int(bytesWritten), "totalBytesWritten":Int(totalBytesWritten), "totalBytesExpectedToWrite":Int(totalBytesExpectedToWrite)])
        }
    }
}

// MARK: - Response
// {"status":0,"assetId":"4615513","previewAssetId":"4615514","hash":"CD60F6406DFF11E599DBC9E031A1DAB4","previewAssetHash":"CD8A77406DFF11E599DBC9E031A1DAB4","commentId":208498,"error":null}
private enum UploadVideoResponse {
    case result (UInt, UInt, String, UInt, String)
    case error (Int, String)
    init(response: AnyObject) {
        if let dict = response as? [String : AnyObject], let status = dict["status"] as? Int {
            if status == 0 {
                if let commentId = dict["commentId"] as? UInt, let assetId = dict["assetId"] as? String, let hash = dict["hash"] as? String,
                        let previewAssetId = dict["previewAssetId"] as? String, let previewAssetHash = dict["previewAssetHash"] as? String {
                    if commentId > 0 && UInt(assetId) > 0 && UInt(previewAssetId) > 0 {
                        self = .result(commentId, UInt(assetId)!, hash, UInt(previewAssetId)!, previewAssetHash)
                    } else {
                        self = .error(-501, "something went wrong")
                    }
                } else {
                    self = .error(-502, "something went wrong")
                }
            } else {
                if let error = dict["error"] as? [String:AnyObject], let message = error["error"] as? String {
                    self = .error(status, message)
                } else {
                    self = .error(status, "something went wrong")
                }
            }
        } else {
            self = .error(-503, "something went very wrong")
        }
    }
}
