//
//  AsyncOperation.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/1/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class AsyncOperation: Operation {
    var error: Error?
    
    fileprivate var _executing : Bool = false
    override var isExecuting : Bool {
        get { return _executing }
        set {
            willChangeValue(forKey: "isExecuting")
            _executing = newValue
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    fileprivate var _finished: Bool = false
    override var isFinished: Bool {
        get { return _finished }
        set {
            willChangeValue(forKey: "isFinished")
            _finished = newValue
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    func finishOperation() {
        isExecuting = false
        isFinished = true
    }
}
