//
//  NSError.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/26/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension NSError {
    convenience init(string: String, code: Int = -1) {
        let dict = [ NSLocalizedDescriptionKey: string ]
        let id = Bundle.main.infoDictionary?["CFBundleIdentifier"] as! String
        self.init(domain: id, code: code, userInfo: dict)
    }
}
