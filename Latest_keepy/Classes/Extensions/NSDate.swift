//
//  NSDate.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/29/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension NSDate {
    var stringOfTimestamp: String {
        var past = Int(fabs(timeIntervalSinceNow))
        
        if past < 60 {  // within 60 seconds
            return "just now"
        }
        past /= 60
        
        if past < 60 {  // within 60 minutes
            return "\(past)m ago"
        }
        past /= 60
        
        if past < 24 {  // within 24 hours
            return "\(past)h ago"
        }
        past /= 24
        
        if past == 1 {  // 1 day ago
            return "yesterday"
        }
        
        if past < 7 {  // within 7 days
            return "\(past)d ago"
        }
        past /= 7
        
        if past < 52 {  // within 52 weeks
            return "\(past)w ago"
        }
        past /= 52
        
        return "\(past)y ago"
    }
    
    func daysBetweenDates(startDate: NSDate, endDate: NSDate) -> Int {
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        
        let components = calendar?.components([.day], from: startDate as Date, to: endDate as Date, options: [])
        
        return components!.day!
    }
}
