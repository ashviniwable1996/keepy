//
//  SKProduct.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/8/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

import UIKit
import StoreKit

extension SKProduct {
    var localizedPrice: String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = self.priceLocale
        return formatter.string(from: self.price)
    }
}

extension SKProductsResponse {
    func product(withIdentifier sku: String) -> SKProduct? {
        for i in products {
            if i.productIdentifier == sku {
                return i
            }
        }
        return nil
    }
}
