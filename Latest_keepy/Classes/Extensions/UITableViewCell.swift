//
//  UITableViewCell.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension UITableViewCell {
    var tableView: UITableView? {
        var table = superview
        while !(table is UITableView) && table != nil {
            table = table?.superview
        }
        return table as? UITableView
    }
}

extension UITableViewCell {
    class var identifier: String {
        let s = NSStringFromClass(self)
        return s.components(separatedBy: ".").last as String!
    }
    
    class func register(tableView tv: UITableView) {
        tv.register(self, forCellReuseIdentifier: self.identifier)
    }
    
    class func registerNib(tableView tv: UITableView) {
        let nib = UINib(nibName: self.identifier, bundle: nil)
        tv.register(nib, forCellReuseIdentifier: self.identifier)
    }
    
    class func dequeue(tableView tv: UITableView, forIndexPath indexPath: IndexPath) -> UITableViewCell {
        return tv.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath)
    }
}


