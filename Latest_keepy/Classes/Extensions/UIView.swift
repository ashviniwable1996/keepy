//
//  UIView.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/5/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension UIView {
    func addSubviewCenter(_ view: UIView) {
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
}
