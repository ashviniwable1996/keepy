//
//  UIColor.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(rgb: UInt, alpha: CGFloat = 1.0) {
        let r = CGFloat((rgb >> 16) & 0xff) / 0xff
        let g = CGFloat((rgb >> 8) & 0xff) / 0xff
        let b = CGFloat((rgb >> 0) & 0xff) / 0xff
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    convenience init(rgba: UInt) {
        let rgb = rgba >> 8
        let a = CGFloat((rgb >> 0) & 0xff) / 0xff
        self.init(rgb: rgb, alpha: a)
    }
}

extension UIColor {
    class func keepyGreenColor() -> UIColor {
        return UIColor(rgb: 0xD7FF46)
    }
    
    class func keepyBrownColor() -> UIColor {
        return UIColor(rgb: 0x4D3C36)
    }
    
    class func keepyLightBrownColor() -> UIColor {
        return UIColor(rgb: 0xB8A7A3)
    }
    
    class func keepyBlueColor() -> UIColor {
        return UIColor(rgb: 0x0BC5DB)
    }
    
    class func keepyDarkGreenColor() -> UIColor {
        return UIColor(rgb: 0x67C469)
    }
    
    class func keepyPinkColor() -> UIColor {
        return UIColor(rgb: 0xFF2C71)
    }
    
    class func disabledButtonTitleColor() -> UIColor {
        return UIColor(rgb: 0x877C7A)
    }
    
    class func keepyPaywallTopColor() -> UIColor {
        return UIColor(rgb: 0x008092)
    }
}
