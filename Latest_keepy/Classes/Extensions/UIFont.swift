//
//  UIFont.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 11/6/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension UIFont {
    class func lightFontOfSize(_ size: CGFloat) -> UIFont {
        return UIFont(name: "ARSMaquettePro-Light", size:size)!
    }
    
    class func regularFontOfSize(_ size: CGFloat) -> UIFont {
        return UIFont(name: "ARSMaquettePro-Medium", size: size)!
    }
        
    class func boldFontOfSize(_ size: CGFloat) -> UIFont {
        return UIFont(name: "ARSMaquettePro-Bold", size: size)!
    }
}
