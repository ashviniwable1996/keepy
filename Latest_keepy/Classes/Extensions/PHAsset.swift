//
//  PHAsset.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/22/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Photos

extension PHAsset {

    var thumbnail: UIImage? {
        let opts = PHImageRequestOptions()
        opts.isSynchronous = true
        opts.resizeMode = .exact;
        
        var result: UIImage?
        
        let scale = UIScreen.main.scale
        let square = CGSize(width: 60 * scale, height: 60 * scale)
//        print("FNTRACE extension PHASSET")

        PHImageManager.default().requestImage(for: self, targetSize: square, contentMode: .aspectFill, options: opts) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            result = image
        }
        return result
    }
}
