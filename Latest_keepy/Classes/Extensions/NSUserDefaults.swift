//
//  NSUserDefaults.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/11/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import Foundation
//api.dev.keepy.me


private let API_ENDPOINT_KEEPY          = "https://api.keepy.me/"
private let API_ENDPOINT_KEEPY_DEV      = "http://api.dev.keepy.me/"
private let API_ENDPOINT_S3             = "https://keepy.s3.amazonaws.com"
private let API_ENDPOINT_S3_DEV         = "https://keepy-dev.s3.amazonaws.com"
private let API_ENDPOINT_CLOUDFRONT     = "https://d1z121v9p153xj.cloudfront.net/assets/"
private let API_ENDPOINT_CLOUDFRONT_DEV = "https://dexfyxiz2huwx.cloudfront.net/assets/"


//private let API_ENDPOINT_KEEPY          = "http://api.dev.keepy.me/"
//private let API_ENDPOINT_KEEPY_DEV      = "http://api.dev.keepy.me/"
//private let API_ENDPOINT_S3             = "https://keepy-dev.s3.amazonaws.com"
//private let API_ENDPOINT_S3_DEV         = "https://keepy-dev.s3.amazonaws.com"
//private let API_ENDPOINT_CLOUDFRONT     = "https://dexfyxiz2huwx.cloudfront.net/assets/"
//private let API_ENDPOINT_CLOUDFRONT_DEV = "https://dexfyxiz2huwx.cloudfront.net/assets/"


class Defaults : UserDefaults {
    static var singleton: UserDefaults = { return UserDefaults.standard }()
}

// MARK: - PlanInfo
extension Defaults {
    @objc enum PlanType : Int {
        case unknown = -1
        case free = 0
        case premium = 1
        case unlimited = 2
    }

    class var KeepiesUsedThisMonthKey: String { return "KeepiesUsedThisMonth" }
    class var keepiesUsedThisMonth: Int {
        set(n) { setInteger(n, forKey: KeepiesUsedThisMonthKey) }
        get { return integerForKey(KeepiesUsedThisMonthKey) }
    }
    
    class var KeepyMonthlySpaceKey: String { return "KeepyMonthlySpace" }
    class var keepyMonthlySpace: Int {
        set(n) { setInteger(n, forKey: KeepyMonthlySpaceKey) }
        get { return integerForKey(KeepyMonthlySpaceKey) }
    }
    
    
    class var MaxItemsPerCollectionKey: String { return "MaxItemsPerCollection" }
    class var maxItemsPerCollection: Int {
        set(n) { setInteger(n, forKey: MaxItemsPerCollectionKey) }
        get { return integerForKey(MaxItemsPerCollectionKey) }
    }
    
    
    class var PlanTypeKey: String { return "PlanType" }
    class var planTypeInt: Int {
        set(n) { setInteger(n, forKey: PlanTypeKey) }
        get { return integerForKey(PlanTypeKey) }
    }
    @objc class var planType: PlanType {
        set(n) { setInteger(n.rawValue, forKey: PlanTypeKey) }
        get { return PlanType(rawValue: integerForKey(PlanTypeKey)) ?? .unknown }
    }

    class var PlanExpirationDateKey: String { return "PlanExpirationDate" }
    class var planExpirationDate: Date? {
        set(d) { setDate(d, forKey: PlanExpirationDateKey) }
        get { return dateForKey(PlanExpirationDateKey) }
    }
}

extension Defaults {
    class var FirstTimeOpenNameKey: String { return "firstTimeOpen" }
    class var firstTimeOpen: Bool {
        set(flag) { setBool(flag, forKey: FirstTimeOpenNameKey) }
        get { return boolForKey(FirstTimeOpenNameKey) }
    }
    
    class var InviteFansCounterKey: String { return "inviteFansCounter" }
    class var inviteFansCounter: Int {
        set(n) { setInteger(n, forKey: InviteFansCounterKey ) }
        get { return integerForKey(InviteFansCounterKey) }
    }
    
    class var DeviceTokenKey: String { return "deviceToken" }
    class var deviceToken: String? {
        set(s) { setString(s, forKey: DeviceTokenKey ) }
        get { return stringForKey(DeviceTokenKey, defaultValue: nil) }
    }
    
    class var RegistrationSignatureKey: String { return "registrationSignature" }
    class var registrationSignature: String? {
        set(s) { setString(s, forKey: RegistrationSignatureKey ) }
        get { return stringForKey(RegistrationSignatureKey, defaultValue: nil) }
    }
    
    class var ItemsLastUpdatedAtKey: String { return "itemsLastUpdatedAt" }
    class var itemsLastUpdatedAt: Date? {
        set(d) { setDate(d, forKey: ItemsLastUpdatedAtKey ) }
        get { return dateForKey(ItemsLastUpdatedAtKey) }
    }
    
    class var UserLastUpdatedAtKey: String { return "userLastUpdatedAt" }
    class var UserLastUpdatedAt: Date? {
        set(d) { setDate(d, forKey: UserLastUpdatedAtKey ) }
        get { return dateForKey(UserLastUpdatedAtKey) }
    }
}

extension Defaults {
    class var ShareWithFacebookNameKey: String { return "shareWithFacebook" }
    class var shareWithFacebook: Bool {
        set(flag) { setBool(flag, forKey: ShareWithFacebookNameKey) }
        get { return boolForKey(ShareWithFacebookNameKey) }
    }
    class var ShareWithTwitterNameKey: String { return "shareWithTwitter" }
    class var shareWithTwitter: Bool {
        set(flag) { setBool(flag, forKey: ShareWithTwitterNameKey) }
        get { return boolForKey(ShareWithTwitterNameKey) }
    }
    class var ShareWithKeepyNameKey: String { return "shareWithKeepy" }
    class var shareWithKeepy: Bool {
        set(flag) { setBool(flag, forKey: ShareWithKeepyNameKey) }
        get { return boolForKey(ShareWithKeepyNameKey) }
    }
}

// MARK: - API Endpoints
extension Defaults {
    class var isDevMode: Bool {
        return apiEndpoint != API_ENDPOINT_KEEPY
    }
    
    class var APIEndpointKey: String { return "APIEndpoint" }
    class var apiEndpoint: String {
        set(s) { setString(s, forKey: APIEndpointKey) }
        get { return stringForKey(APIEndpointKey, defaultValue: API_ENDPOINT_KEEPY)! }
        
    }
    
    class var S3URLStringKey: String { return "S3URLString" }
    class var s3URLString: String {
        set(s) { setString(s, forKey: S3URLStringKey) }
        get {
            let defaultURL = isDevMode ? API_ENDPOINT_S3_DEV : API_ENDPOINT_S3
            return stringForKey(S3URLStringKey, defaultValue: defaultURL )!
        }
    }
    
    class var AssetsURLStringKey: String { return "assetsURLString" }
    class var assetsURLString: String {
        set(s) { setString(s, forKey: AssetsURLStringKey) }
        get {
            let defaultURL = isDevMode ? API_ENDPOINT_CLOUDFRONT_DEV : API_ENDPOINT_CLOUDFRONT
            return stringForKey(AssetsURLStringKey, defaultValue: defaultURL )!
        }
    }
}

// MARK: - Private
extension Defaults {
    class func setBool(_ value: Bool, forKey defaultName: String) {
        singleton.set(value, forKey: defaultName)
    }
    class func boolForKey(_ defaultName: String) -> Bool {
        if let n = singleton.object(forKey: defaultName) as? NSNumber {
            return n.int32Value != 0
        }
        return singleton.bool(forKey: defaultName)
    }
    
    class func setInteger(_ value: Int, forKey defaultName: String) {
        singleton.set(value, forKey: defaultName)
    }
    class func integerForKey(_ defaultName: String) -> Int {
        return singleton.integer(forKey: defaultName)
    }
    
    class func setString(_ value: String?, forKey defaultName: String) {
        singleton.set(value, forKey: defaultName)
    }
    class func stringForKey(_ defaultName: String, defaultValue: String?) -> String? {
        return singleton.string(forKey: defaultName) ?? defaultValue
    }
    
    class func setDate(_ value: Date?, forKey defaultName: String) {
        singleton.set(value, forKey: defaultName)
    }
    class func dateForKey(_ defaultName: String) -> Date? {
        return singleton.object(forKey: defaultName) as? Date
    }
}
