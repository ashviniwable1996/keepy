//
//  UIImage.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/13/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension UIImage {
    convenience init(color: UIColor, size: CGSize = CGSize(width: 1, height: 1), shadow: CGFloat = 0) {
        let actualSize = CGSize(width: size.width + shadow*2, height: size.width + shadow*2)
        
        UIGraphicsBeginImageContextWithOptions(actualSize, false, 0)
        let c = UIGraphicsGetCurrentContext()!
        
        c.setFillColor(color.cgColor)
        c.setShadow(offset: CGSize.zero, blur: shadow)
//        CGContextSetShadowWithColor(c, CGSizeZero, shadow, UIColor.lightGrayColor().CGColor)
        c.fill(CGRect(x: shadow, y: shadow, width: size.width, height: size.height))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.init(cgImage: image.cgImage!, scale: UIScreen.main.scale, orientation: .up)
    }

    class func plate(_ color: UIColor, size: CGSize = CGSize(width: 1, height: 1), shadow: CGFloat = 0) -> UIImage {
        let actualSize = CGSize(width: size.width + shadow*2, height: size.width + shadow*2)
        
        UIGraphicsBeginImageContextWithOptions(actualSize, false, 0)
        let c = UIGraphicsGetCurrentContext()!
        
        c.setFillColor(color.cgColor)
        c.setShadow(offset: CGSize.zero, blur: shadow)
        c.fill(CGRect(x: shadow, y: shadow, width: actualSize.width, height: actualSize.height))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

    func square() -> UIImage {
        let w = self.size.width
        let h = self.size.height
        let size = min(w, h)
        let x = (w - size) / 2
        let y = (h - size) / 2

        UIGraphicsBeginImageContextWithOptions(CGSize(width: size, height: size), false, 0.0);
        draw(at: CGPoint(x: -x, y: -y))
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image

    }
    
    func resize(size aSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(aSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: aSize.width, height: aSize.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func enhance(_ flag: Bool, rotation: Int) -> UIImage {
        let cgimage: CoreImage.CGImage
        if flag {
            guard let filter = CIFilter(name: "CIVibrance") else { return self }
            filter.setValue(CoreImage.CIImage(cgImage: self.cgImage!), forKey: kCIInputImageKey)
            filter.setValue(1.0, forKey: "inputAmount")
        
            guard let ciimage = filter.outputImage else { return self }
            let context = CIContext(options: nil)
            cgimage = context.createCGImage(ciimage, from: ciimage.extent)!
        } else {
            cgimage = self.cgImage!
        }
        
        let o: UIImageOrientation
        switch self.imageOrientation {
        case .down:
            switch rotation {
            case 90:  o = .left
            case 180: o = .up
            case 270: o = .right
            default:  o = .down
            }
        case .left:
            switch rotation {
            case 90:  o = .up
            case 180: o = .right
            case 270: o = .down
            default:  o = .left
            }
        case .right:
            switch rotation {
            case 90:  o = .down
            case 180: o = .left
            case 270: o = .up
            default:  o = .right
            }
        default:
            switch rotation {
            case 90:  o = .right
            case 180: o = .down
            case 270: o = .left
            default:  o = .up
            }
        }
        let result = UIImage(cgImage: cgimage, scale: 1, orientation: o)
        return result
    }
    
    var normalizedImage: UIImage {
        if imageOrientation == .up { return self }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(at: CGPoint.zero)
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
}
enum WaterMarkCorner{
    case TopLeft
    case TopRight
    case BottomLeft
    case BottomRight
}
extension UIImage {
    func insert(_ image: UIImage) -> UIImage {
        return append(image, insert: true)
    }
    
    func append(_ image: UIImage, insert: Bool = false) -> UIImage {
        let w = max(self.size.width, image.size.width)
        let h = max(self.size.height, image.size.height)
        let size = CGSize(width: w, height: h)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let c = UIGraphicsGetCurrentContext()!
        
        c.translateBy(x: size.width/2, y: size.height/2)
        c.scaleBy(x: 1.0, y: -1.0);
        if insert {
            c.draw(image.cgImage!, in: CGRect(x: -image.size.width/2, y: -image.size.height/2, width: image.size.width, height: image.size.height))
            c.draw(cgImage!, in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        } else {
            c.draw(cgImage!, in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
            c.draw(image.cgImage!, in: CGRect(x: -image.size.width/2, y: -image.size.height/2, width: image.size.width, height: image.size.height))
        }
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func addWatermark(_ name: String, margin: CGFloat) -> UIImage {
        if let watermark = UIImage(named: name) {
            if watermark.size.width + abs(margin) > size.width || watermark.size.height + abs(margin) > size.height {
                return self  // Too small to add the watermark.
            }
            
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            draw(at: CGPoint.zero)
            if margin < 0 {  // Bottom right
                watermark.draw(at: CGPoint(x: size.width + margin - watermark.size.width, y: size.height + margin - watermark.size.height))
            } else if margin > 0 {  // Bottom left
                watermark.draw(at: CGPoint(x: margin, y: size.height - margin - watermark.size.height))
            } else {  // Center
                watermark.draw(at: CGPoint(x: (size.width - watermark.size.width) / 2, y: (size.height - watermark.size.height) / 2))
            }
            let image = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return image
        }
        
        return self  // There's no watermark image.
    }
    
    func addFrame() -> UIImage {
        let topIndent: CGFloat = 40
        //let bottomIndent: CGFloat = 104
        let bottomIndent: CGFloat = 50
        let leftIndent: CGFloat = 40
        let rightIndent: CGFloat = 40
        let fullImageWidth: CGFloat = self.size.width //1080
        
        let width: CGFloat = fullImageWidth - leftIndent - rightIndent
        let scale = width / size.width
        let scaledImageSize = CGSize(width: width, height: size.height * scale)
        
        let fullSize = CGSize(width: fullImageWidth, height: scaledImageSize.height + topIndent + bottomIndent)
        
        UIGraphicsBeginImageContextWithOptions(fullSize, false, 0)
        
        let imageRect = CGRect(origin: CGPoint(x: leftIndent, y: topIndent), size: CGSize(width: scaledImageSize.width, height: scaledImageSize.height))
        draw(in: imageRect)
        
        if let watermark = UIImage(named: "watermark") {
            let watermarkSize = CGSize(width: CGFloat(400), height: CGFloat(60))
          //  let watermarkSize = CGSize(width: self.size.width, height: CGFloat(60))
//           watermark.draw(in: CGRect(origin: CGPoint(x: fullImageWidth - watermarkSize.width - 8, y: topIndent + scaledImageSize.height), size: watermarkSize))
            watermark.draw(in: CGRect(origin: CGPoint(x: fullImageWidth - watermarkSize.width - 35, y: topIndent + scaledImageSize.height), size: watermarkSize))
            
        }
        
        let path: UIBezierPath = UIBezierPath(roundedRect: imageRect, cornerRadius: 10)
        path.lineWidth = 10
        UIColor(red: 40.0/255.0, green: 191.0/255.0, blue: 214.0/255.0, alpha: 1.0).setStroke()
      //  path.stroke()
        
        UIColor.white.setFill()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func addFrameNew() -> UIImage {
       // let topIndent: CGFloat = 40
        let bottomIndent: CGFloat = 50
      //  let leftIndent: CGFloat = 40
      //  let rightIndent: CGFloat = 40
        let fullImageWidth: CGFloat = self.size.width
        
        let width: CGFloat = fullImageWidth
        let scale = width / size.width
        let scaledImageSize = CGSize(width: width, height: size.height * scale)
        
        let fullSize = CGSize(width: fullImageWidth, height: scaledImageSize.height + bottomIndent)
        
        UIGraphicsBeginImageContextWithOptions(fullSize, false, 0)
        
       // let imageRect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: scaledImageSize.width, height: scaledImageSize.height))
        let imageRect = CGRect(origin: CGPoint(x: 3, y: 3), size: CGSize(width: scaledImageSize.width - 6, height: scaledImageSize.height - 2))
        draw(in: imageRect)
        
        if let watermark = UIImage(named: "watermark") {
            let watermarkSize = CGSize(width: CGFloat(400), height: CGFloat(60))
            watermark.draw(in: CGRect(origin: CGPoint(x: fullImageWidth - watermarkSize.width - 35, y: scaledImageSize.height), size: watermarkSize))
        }
        
        let path: UIBezierPath = UIBezierPath(roundedRect: imageRect, cornerRadius: 10)
        path.lineWidth = 10
        UIColor(red: 40.0/255.0, green: 191.0/255.0, blue: 214.0/255.0, alpha: 1.0).setStroke()
        path.stroke()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    
    func waterMarkedImage(_ waterMarkText:String, corner:WaterMarkCorner = .TopRight, margin:CGPoint = CGPoint(x: 20, y: 20), waterMarkTextColor:UIColor = UIColor.black, waterMarkTextFont:UIFont = UIFont.systemFont(ofSize: 40), backgroundColor:UIColor = UIColor(white: 1.0, alpha: 0.5)) -> UIImage?{
        
        let textAttributes = [NSForegroundColorAttributeName:waterMarkTextColor, NSFontAttributeName:waterMarkTextFont, NSBackgroundColorAttributeName: backgroundColor]
        let textSize = NSString(string: waterMarkText).size(attributes: textAttributes)
        var textFrame = CGRect(x:0, y:0, width:textSize.width, height:textSize.height)
        
        let imageSize = self.size
        switch corner{
        case .TopLeft:
            textFrame.origin = margin
        case .TopRight:
            textFrame.origin = CGPoint(x: imageSize.width - textSize.width - margin.x, y: margin.y)
        case .BottomLeft:
            textFrame.origin = CGPoint(x: margin.x, y: imageSize.height - textSize.height - margin.y)
        case .BottomRight:
            textFrame.origin = CGPoint(x: imageSize.width - textSize.width - margin.x, y: imageSize.height - textSize.height - margin.y)
        }
        
        /// Start creating the image with water mark
        UIGraphicsBeginImageContext(imageSize)
        self.draw(in: CGRect(x:0, y:0, width:imageSize.width, height:imageSize.height))
        
        NSString(string: waterMarkText).draw(in: textFrame, withAttributes: textAttributes)
        
        let waterMarkedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return waterMarkedImage
    }
    
    
}

func DegreeToRadian(_ degree: CGFloat) -> CGFloat { return degree * CGFloat(M_PI) / CGFloat(180) }
func RadianToDegree(_ radian: CGFloat) -> CGFloat { return radian * CGFloat(180) / CGFloat(M_PI) }

extension UIImage {
    func rotate(_ degree: CGFloat) -> UIImage {
        
        let v = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let t = CGAffineTransform(rotationAngle: DegreeToRadian(degree))
        v.transform = t
        let rotatedSize = v.frame.size
        
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, 0)
        let c = UIGraphicsGetCurrentContext()!
        
        c.setAllowsAntialiasing(true)
        
        c.translateBy(x: rotatedSize.width/2, y: rotatedSize.height/2)
        c.rotate(by: DegreeToRadian(degree))
        c.scaleBy(x: 1.0, y: -1.0);
        c.draw(cgImage!, in: CGRect(x: -size.width/2, y: -size.height/2, width: size.width, height: size.height))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
}
