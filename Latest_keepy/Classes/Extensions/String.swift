//
//  String.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/2/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension String {
    var length: Int {
        return self.lengthOfBytes(using: String.Encoding.utf8)
    }
    
    func trim() -> String {
        return trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    func replace(_ string: String, replacement: String, options: NSString.CompareOptions = .literal) -> String {
        return replacingOccurrences(of: string, with: replacement, options: options)
    }
}

extension NSString {
    func trim() -> NSString {
        return trimmingCharacters(in: CharacterSet.whitespaces) as NSString
    }
    
    var isEmailAddress: Bool {
        let regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$"
        let p = NSPredicate(format: "SELF MATCHES %@", regex)
        return p.evaluate(with: self)
    }
    
    func sizeWithFont(_ font: UIFont, thatFitsToSize size: CGSize, lineBreakMode: NSLineBreakMode) -> CGSize {
        let style = NSMutableParagraphStyle()
        style.lineBreakMode = lineBreakMode
        let rect = boundingRect(with: size, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : font, NSParagraphStyleAttributeName : style], context: nil)
        return rect.size
    }
    
    func sizeWithFont(_ font: UIFont, thatFitsToSize size: CGSize) -> CGSize {
        let rect = boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName : font], context: nil)
        return rect.size
    }
}
