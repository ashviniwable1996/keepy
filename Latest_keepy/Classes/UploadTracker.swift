//
//  UploadTracker.swift
//  Keepy
//
//  Created by Ryan Lons on 2/2/16.
//  Copyright © 2016 Keepy. All rights reserved.
//

import Foundation
import Mixpanel
import UIKit

@objc class UploadTracker : NSObject
{
    static let sharedInstance = UploadTracker()
    
    var itemsBeingUploaded: [String] = []
    var failedItems: [String] = []
    var finishedItems: [String] = []
    fileprivate var maxCount : Int = 0
    var isRetrying : Bool = false
    var hasFailures : Bool = false
    fileprivate var uploadCheckerTimer : Timer!
    
    override init()
    {
        super.init()
        uploadCheckerTimer = Timer(timeInterval: 4, target: self, selector: "checkAssetStatusServerSide", userInfo: nil, repeats: true)
        RunLoop.current.add(uploadCheckerTimer, forMode: RunLoopMode.commonModes)
    }
    
    func addItemToWatchList(_ key : String)
    {
        itemsBeingUploaded.append(key)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOAD_TRACKER_COUNT_CHANGED"), object: nil, userInfo: ["count":itemsBeingUploaded.count])
//        print(".\n\n\n --------- ADDED  : items being uploaded count: \(itemsBeingUploaded.count)\n\n\n\n")
        maxCount += 1;
    }
    
    func itemIsPartOfUploadBatch(_ key : String) -> Bool
    {
        if itemsBeingUploaded.containsKey(key) || finishedItems.containsKey(key) || failedItems.containsKey(key)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func removeItemToWatchList(_ key : String)
    {
//        print(".............. removeItemToWatchList")
        var i = 0
        var foundKey = false
        for keys in itemsBeingUploaded
        {
            let keyArray = keys.components(separatedBy: ",")
            if keyArray.contains(key)
            {
                finishedItems.append(keys)
                foundKey = true
                break
            }
            i += 1
        }
        
        if foundKey
        {
            itemsBeingUploaded.remove(at: i)
        }
        else
        {
//            print(".\n\n\n --------- NOT FOUND : \n looking for: \(key) \n in \(itemsBeingUploaded)\n\n\n\n")
        }
        
        if itemsBeingUploaded.count == 0 // finished batch
        {
           
            isRetrying = false
            hasFailures = false
            maxCount = 0
            failedItems.removeAll()
            finishedItems.removeAll()
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOAD_TRACKER_COUNT_CHANGED"), object: nil, userInfo: ["count":itemsBeingUploaded.count])
    }
    
    func setItemFailed(_ assetId : String)
    {
        print(assetId)
        if !failedItems.containsKey(assetId)
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "cancelAllUploadsNotification"), object: nil)
            Mixpanel.sharedInstance()?.track("Upload failed", properties: ["number-items" : itemsBeingUploaded.count + finishedItems.count, "number-items-already-uploaded" : finishedItems.count])
            for keys in finishedItems
            {
                let serverIdString = keys.components(separatedBy: ",")[0]
                let serverId : Int = Int(serverIdString)!
                
                ServerComm.instance().deleteItem(serverId, withSuccessBlock: { (result) -> Void in
                    
                    Mixpanel.sharedInstance()?.people.increment("Number of photos", by: -1)
                    GlobalUtils.updateAnalyticsUser()
                    
                    }, andFail: { (error) -> Void in
                       print("error deleting object: \(error)")
                })

                if let localItem = KPLocalItem.fetch(byServerID: UInt(serverId))
                {
                    localItem.remove()
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ItemDeletedNotification"), object:serverId)
                }
            }
            
            for keys in failedItems
            {
                let serverIdString = keys.components(separatedBy: ",")[0]
                let serverId : Int = Int(serverIdString)!
                
                if let localItem = KPLocalItem.fetch(byServerID: UInt(serverId))
                {
                    localItem.remove()
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ItemDeletedNotification"), object:serverId)
                }
            }
            
            for keys in itemsBeingUploaded
            {
                let serverIdString = keys.components(separatedBy: ",")[0]
                let serverId : Int = Int(serverIdString)!
                if let localItem = KPLocalItem.fetch(byServerID: UInt(serverId))
                {
                    localItem.remove()
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ItemDeletedNotification"), object:serverId)
                }
            }
            
            
            
            NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()
            //then reset the uploader
            isRetrying = false
            hasFailures = false
            maxCount = 0
            failedItems.removeAll()
            finishedItems.removeAll()
            itemsBeingUploaded.removeAll()
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UPLOAD_TRACKER_COUNT_CHANGED"), object: nil, userInfo: ["count":itemsBeingUploaded.count])
            self.hasFailures = true
        }
    }
    
    func containsKey(_ key : String) -> Bool
    {
        var i = 0
        var foundKey = false
        for keys in itemsBeingUploaded
        {
            let keyArray = keys.components(separatedBy: ",")
            if keyArray.contains(key)
            {
                foundKey = true
                break
            }
            i += 1
        }
        return foundKey
    }
    
    func numberOfItemsBeingProcessed() -> Int
    {
        return itemsBeingUploaded.count
    }
    
    func highestNumberOfItemsThisBatch() -> Int
    {
        return maxCount
    }
    
    func checkAssetStatusServerSide()
    {
//        print("....................... itemsBeingUploaded.count: \(itemsBeingUploaded.count)")
        for keys in itemsBeingUploaded
        {
            let keyArray = keys.components(separatedBy: ",")
            let serverId = keyArray[1]
            let imgAssetID = keyArray[2]
//            print("....................... checking on: \(serverId), \(keyArray)")
            checkAssetsStatus(serverId, imgAssetID)
        }
    }
    
    fileprivate func checkAssetsStatus(_ assetId : String,_ imagAssetID: String)
    {
        
        ServerComm.instance().getVideoAssetStatus(assetId, successBlock: { (result) -> Void in
            
            if let a = (((result as AnyObject).object(forKey: "result")! as AnyObject).object(forKey: "assetStatus")! as AnyObject).int32Value
            {
                print("a = \(a)");
                
                if a == -1
                {
                    print("was deleted")
                    self.removeItemToWatchList(assetId)
                }
                else if a == 2
                {
                    print(".......................  finished")
                    print("Assets ID:\(assetId)")
                    if let imagesItemID = UserDefaults.standard.value(forKey: "ImageItemID") as? Int {
                        print("ImageItemID:\(imagesItemID) == Img Assets ID:\(imagAssetID)")

                        if imagAssetID == "\(imagesItemID)" {
                            if  let str = UserDefaults.standard.value(forKey: "RenderingImage") as? String {
                            let pro = ProductCollectionView.init(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
                            FinalProduct.shared.finalProductArray.removeAll()
                            pro.getTheImagesArrayForAllTheProducsts(imagesURL: str)
                            pro.getThePriceForTheProducts()
                           }
                        }
                    }
                    self.removeItemToWatchList(assetId)
                }
                else if a == 3   //added by amitg
                {
                    print(".......................  heic error it seems!")
                    self.removeItemToWatchList(assetId)
                }
                else
                {
                    print("....................... NOT finished")
                }
            }
            else //server doesnt know what it is
            {
                self.removeItemToWatchList(assetId)
            }
            
            }) { (error) -> Void in
                print("error: \(error)")
                print("---")
        }
    }
}


extension Collection {
    func containsKey(_ key : String) -> Bool
    {
        var i = 0
        var foundKey = false
        for keysString in self
        {
            let keyArray = (keysString as? String)!.components(separatedBy: ",")
            if keyArray.contains(key)
            {
                foundKey = true
                break
            }
            i += 1
        }
        return foundKey
    }
}
