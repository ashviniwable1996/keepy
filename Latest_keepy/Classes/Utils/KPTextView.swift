//
//  KPTextView.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/16/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class KPTextView: UITextView {
    @IBInspectable var placeholder: String? {
        didSet { setNeedsDisplay() }
    }
    
    @IBInspectable var placeholderColor: UIColor? {
        didSet { setNeedsDisplay() }
    }
    
    override var text: String! {
        didSet { setNeedsDisplay() }
    }
    
    // MARK: -
    
    init(frame: CGRect) {
        super.init(frame: frame, textContainer: nil)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if self.text.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            if let s = (placeholder as NSString?), let f = self.font, let c = placeholderColor {
                let attrs = [ NSFontAttributeName : f, NSForegroundColorAttributeName : c ];
                
                s.draw(in: rect.insetBy(dx: 4, dy: 6 + contentInset.top), withAttributes: attrs)
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: -
    
    fileprivate func initialize() {
        backgroundColor = UIColor.clear
        NotificationCenter.default.addObserver(self, selector: "textChanged:", name: NSNotification.Name.UITextViewTextDidChange, object: nil)
        placeholderColor = UIColor(white: 0.702, alpha: 1)
    }
    
    func textChanged(_ notificaiton: Notification) {
        setNeedsDisplay()
    }
}
