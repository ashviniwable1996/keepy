//
//  UIButtonIconLabel.m
//  Keepy
//
//  Created by Felipe Oliveira on 4/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "UIButtonIconLabel.h"

@implementation UIButtonIconLabel

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    [self.imageView centerHorizontallyInSuperview];
    self.imageView.frameY = 0;

    [self.titleLabel sizeToFit];
    [self.titleLabel centerHorizontallyInSuperview];
    self.titleLabel.frameY = self.imageView.frameBottom + 3;


    /*
    
    CGRect frame = self.imageView.frame;
    frame = CGRectMake(truncf((self.bounds.size.width - frame.size.width) / 2), 0.0f, frame.size.width, frame.size.height);
    self.imageView.frame = frame;
    
    frame = self.titleLabel.frame;
    frame = CGRectMake(truncf((self.bounds.size.width - frame.size.width) / 2), self.bounds.size.height - frame.size.height, frame.size.width, frame.size.height);
    self.titleLabel.frame = frame;*/
}

@end
