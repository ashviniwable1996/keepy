//
//  AlbumPickerController.h
//
//  Created by ELC on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "GalleryPickerViewController.h"
#import "ELCAssetSelectionDelegate.h"
#import "KPPopViewController.h"

@interface ELCAlbumPickerController : KPPopViewController <ELCAssetSelectionDelegate>

@property(nonatomic, assign) id <ELCAssetSelectionDelegate> parent;
@property(nonatomic, retain) NSMutableArray *assetGroups;
@property(nonatomic, assign) id <GalleryPickerDelegate> delegate;
@property(nonatomic, strong) NSString *filter;

@end

