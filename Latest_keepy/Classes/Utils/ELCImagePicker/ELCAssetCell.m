//
//  AssetCell.m
//
//  Created by ELC on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import <CoreMedia/CoreMedia.h>
#import <Photos/Photos.h>

#import "ELCAssetCell.h"

#import "Keepy-Swift.h"

@interface ELCAssetCell ()

@property(nonatomic, retain) NSArray *rowAssets;
@property(nonatomic, retain) NSMutableArray *imageViewArray;
@property(nonatomic, retain) NSMutableArray *overlayViewArray;
@property(nonatomic, retain) NSMutableArray *labelViewArray;
@property(nonatomic, retain) NSMutableArray *iconViewArray;

@end

@implementation ELCAssetCell

@synthesize rowAssets = _rowAssets;

- (instancetype)initWithAssets:(NSArray *)assets reuseIdentifier:(NSString *)identifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    if (self) {


        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellTapped:)];
        [self addGestureRecognizer:tapRecognizer];

        NSMutableArray *mutableArray = [[NSMutableArray alloc] initWithCapacity:4];
        self.imageViewArray = mutableArray;

        NSMutableArray *overlayArray = [[NSMutableArray alloc] initWithCapacity:4];
        self.overlayViewArray = overlayArray;

        NSMutableArray *labelArray = [[NSMutableArray alloc] initWithCapacity:4];
        self.labelViewArray = labelArray;

        NSMutableArray *iconArray = [[NSMutableArray alloc] initWithCapacity:4];
        self.iconViewArray = iconArray;

        [self setAssets:assets];
    }
    return self;
}

- (void)setAssets:(NSArray *)assets {
    self.rowAssets = assets;
    for (UIView *view in [self subviews]) {
        if ([view isKindOfClass:UIImageView.class]) {
            [view removeFromSuperview];
        }
    }
    //set up a pointer here so we don't keep calling [UIImage imageNamed:] if creating overlays
    UIImage *overlayImage = nil;
    for (int i = 0; i < [_rowAssets count]; ++i) {

        KPAsset *asset = [_rowAssets objectAtIndex:i];
        
        PHImageRequestOptions *options = [PHImageRequestOptions new];
        options.resizeMode = PHImageRequestOptionsResizeModeExact;
        options.synchronous = YES;
        
        NSInteger retinaMultiplier = [UIScreen mainScreen].scale;
        CGSize retinaSquare = CGSizeMake(60 * retinaMultiplier, 60 * retinaMultiplier);
        
        [[PHImageManager defaultManager] requestImageForAsset:asset.asset
                                                   targetSize:retinaSquare
                                                  contentMode:PHImageContentModeAspectFill
                                                      options:options
                                                resultHandler:^(UIImage *image, NSDictionary *info) {

                                                    if (i < [_imageViewArray count]) {
                                                        UIImageView *imageView = [_imageViewArray objectAtIndex:i];
                                                        imageView.image = image;
                                                        
                                                    } else {
                                                        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
                                                        [_imageViewArray addObject:imageView];
                                                    }
                                                }];

        if (asset.asset.mediaType == PHAssetMediaTypeVideo) {
            UILabel *labelView = nil;
            if (i < [_labelViewArray count]) {
                labelView = [_labelViewArray objectAtIndex:i];

            } else {
                labelView = [UILabel new];
                labelView.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
                labelView.textColor = [UIColor whiteColor];
                labelView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
                labelView.textAlignment = NSTextAlignmentRight;
                [_labelViewArray addObject:labelView];
                
                UIImage *icon = [UIImage imageNamed:@"add-video"];
                UIImageView *iconView = [[UIImageView alloc] initWithImage:icon];
                iconView.frame = CGRectMake(3, 4, 14, 8);
                [labelView addSubview:iconView];
            }
            
            unsigned long duration = asset.asset.duration;
            labelView.text = [NSString stringWithFormat:@"%01lu:%02lu\t", duration / 60, duration % 60];
            labelView.hidden = NO;
        } else {
            if (i < [_labelViewArray count]) {
                UILabel* labelView = [_labelViewArray objectAtIndex:i];
                labelView.hidden = YES;
            }
        }


        if (i < [_overlayViewArray count]) {
            UIImageView *overlayView = [_overlayViewArray objectAtIndex:i];
            overlayView.hidden = asset.selected ? NO : YES;
        } else {
            if (overlayImage == nil) {
                overlayImage = [UIImage imageNamed:@"Overlay"];
            }
            UIImageView *overlayView = [[UIImageView alloc] initWithImage:overlayImage];
            [_overlayViewArray addObject:overlayView];
            overlayView.hidden = asset.selected ? NO : YES;
        }
    }
}

- (void)cellTapped:(UITapGestureRecognizer *)tapRecognizer {
    CGPoint point = [tapRecognizer locationInView:self];
    CGFloat totalWidth = self.rowAssets.count * 75 + (self.rowAssets.count - 1) * 4;
    CGFloat startX = (self.bounds.size.width - totalWidth) / 2;

    CGRect frame = CGRectMake(startX, 2, 75, 75);

    for (int i = 0; i < [_rowAssets count]; ++i) {
        if (CGRectContainsPoint(frame, point)) {
            KPAsset *asset = [_rowAssets objectAtIndex:i];
            asset.selected = !asset.selected;
            UIImageView *overlayView = [_overlayViewArray objectAtIndex:i];
            overlayView.hidden = !asset.selected;
            break;
        }
        frame.origin.x = frame.origin.x + frame.size.width + 4;
    }
}

- (void)layoutSubviews {
    CGFloat totalWidth = self.rowAssets.count * 75 + (self.rowAssets.count - 1) * 4;
    CGFloat startX = (self.bounds.size.width - totalWidth) / 2;

    CGRect frame = CGRectMake(startX, 2, 75, 75);
    CGRect labelFrame = CGRectMake(startX, 62, 75, 15);

    for (int i = 0; i < [_rowAssets count]; ++i) {
        UIImageView *imageView = [_imageViewArray objectAtIndex:i];
        [imageView setFrame:frame];
        [self addSubview:imageView];

        UIImageView *overlayView = [_overlayViewArray objectAtIndex:i];
        [overlayView setFrame:frame];
        [self addSubview:overlayView];
        if ([_labelViewArray count] > i) {
            UILabel *labelView = [_labelViewArray objectAtIndex:i];
            [labelView setFrame:labelFrame];
            [self addSubview:labelView];

        }

        frame.origin.x = frame.origin.x + frame.size.width + 4;
        labelFrame.origin.x = labelFrame.origin.x + labelFrame.size.width + 4;
    }
}

@end
