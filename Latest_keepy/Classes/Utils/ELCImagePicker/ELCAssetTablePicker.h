//
//  AssetTablePicker.h
//
//  Created by ELC on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

@import UIKit;
@import Photos;

#import "ELCAssetSelectionDelegate.h"

#import "Keepy-Swift.h"

@interface ELCAssetTablePicker : UITableViewController <KPAssetDelegate>

@property(nonatomic, assign) id <ELCAssetSelectionDelegate> parent;
@property(nonatomic, retain) NSArray<KPAsset*>* assets;
@property(nonatomic, retain) IBOutlet UILabel *selectedAssetsLabel;
@property(nonatomic, assign) BOOL singleSelection;
@property(nonatomic, assign) BOOL immediateReturn;

- (int)totalSelectedAssets;

- (void)doneAction:(id)sender;

- (void)assetSelected:(KPAsset *)asset;

@end