//
//  KPAsset.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/15/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Photos

class KPAsset: NSObject {
    var asset: PHAsset?
    var selected: Bool {
        didSet {
            if (selected) {
                parent?.assetSelected?(self)
            }
        }
    }
    weak var parent: KPAssetDelegate?
    
    init(asset anAsset: PHAsset) {
        asset = anAsset
        selected = false
        super.init()
    }
    
    func toggleSelection() {
        selected = !selected
    }
}

@objc protocol KPAssetDelegate: NSObjectProtocol {
    @objc optional func assetSelected(_ asset: KPAsset);
}
