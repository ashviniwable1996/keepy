//
//  AlbumPickerController.m
//
//  Created by ELC on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import "ELCAlbumPickerController.h"

@import Photos;
#import "Keepy-Swift.h"

@interface ELCAlbumPickerController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, retain) UITableView *tableView;

@end

@implementation ELCAlbumPickerController

@synthesize parent = _parent;
@synthesize assetGroups = _assetGroups;
//@synthesize library = _library;

#pragma mark -
#pragma mark View lifecycle

- (void)loadView {
    self.view = [[UIView alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"album-picker", @"source", nil]];

    self.title = KPLocalizedString(@"albums");
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollsToTop = NO;
    self.tableView.backgroundView = nil;
    [self.view addSubview:self.tableView];

    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    self.assetGroups = tempArray;

    //self.library = [GlobalUtils defaultAssetsLibrary];
    //[assetLibrary release];

    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    
    [smartAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        
        PHFetchResult *result = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
        NSLog(@"album title: %@ (%ld)", collection.localizedTitle, (long)result.count);
        if (result.count > 0) {
            [self.assetGroups addObject:collection];
            [self performSelectorOnMainThread:@selector(reloadTableView) withObject:nil waitUntilDone:NO];
        }
    }];
    
    //    Get all photos
    PHFetchResult *allPhotosResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    NSLog(@"all photos: %d", (int)allPhotosResult.count);
    
    //    Get all photos
    PHFetchResult *allVideosResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:nil];
    NSLog(@"all videos: %d", (int)allVideosResult.count);
}

- (void)reloadTableView {
    [self.tableView reloadData];
    //[self.navigationItem setTitle:@"Select an Album"];
}

- (void)selectedAssets:(NSArray *)assets {
    [_parent selectedAssets:assets];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.assetGroups count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

#if IOS7_SUPPORT
    // DDLogInfo(@"%@",_filter);

    // Get count
    ALAssetsGroup *g = (ALAssetsGroup *) [self.assetGroups objectAtIndex:indexPath.row];
    if ([_filter isEqualToString:@"allPhotos"]) {
        [g setAssetsFilter:[ALAssetsFilter allPhotos]];
    } else {
        [g setAssetsFilter:[ALAssetsFilter allVideos]];
    }

    NSInteger gCount = [g numberOfAssets];

    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%ld)", [g valueForProperty:ALAssetsGroupPropertyName], (long)gCount];
    [cell.imageView setImage:[UIImage imageWithCGImage:[(ALAssetsGroup *) [self.assetGroups objectAtIndex:indexPath.row] posterImage]]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
#else
    PHAssetCollection* collection = _assetGroups[indexPath.row];
    
    PHFetchResult* result = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
    
    // Increment the cell's tag
    NSInteger currentTag = cell.tag + 1;
    cell.tag = currentTag;

    PHImageRequestOptions *options = [PHImageRequestOptions new];
    options.resizeMode = PHImageRequestOptionsResizeModeExact;
    
    NSInteger retinaMultiplier = [UIScreen mainScreen].scale;
    CGSize retinaSquare = CGSizeMake(60 * retinaMultiplier, 60 * retinaMultiplier);

    [[PHImageManager defaultManager] requestImageForAsset:result.lastObject
                                               targetSize:retinaSquare
                                              contentMode:PHImageContentModeAspectFill
                                                  options:options
                                            resultHandler:^(UIImage *image, NSDictionary *info) {
                                                // Only update the thumbnail if the cell tag hasn't changed. Otherwise, the cell has been re-used.
                                                if (cell.tag == currentTag) {
                                                    cell.imageView.image = image;
                                                }
                                            }];

    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%ld)", collection.localizedTitle, (long)result.count];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
#endif
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"album-picker", @"source", @"ok", @"result", nil]];

    NSMutableArray* arr = [NSMutableArray array];
    
    PHAssetCollection* collection = _assetGroups[indexPath.row];
    PHFetchOptions* options = [PHFetchOptions new];
    options.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO] ];
    PHFetchResult<PHAsset *> * result = [PHAsset fetchAssetsInAssetCollection:collection options:options];
    
    [result enumerateObjectsUsingBlock:^(PHAsset* obj, NSUInteger idx, BOOL *stop) {
        [arr addObject:[[KPAsset alloc] initWithAsset:obj]];
        
        if (result.count == idx + 1) {
            GalleryPickerViewController *avc = [[GalleryPickerViewController alloc] initWithAssets:arr];
            avc.title = collection.localizedTitle;
            avc.delegate = self.delegate;
            [self.navigationController pushViewController:avc animated:YES];
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 57;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)topBtnTap:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"album-picker", @"source", @"cancel", @"result", nil]];

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(galleryPickerControllerDidCancel:)]) {

        [self.delegate performSelector:@selector(galleryPickerControllerDidCancel:) withObject:self];
    }
}

@end

