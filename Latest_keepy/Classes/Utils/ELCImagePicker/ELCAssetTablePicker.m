//
//  AssetTablePicker.m
//
//  Created by ELC on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

@import Photos;

#import "ELCAssetTablePicker.h"
#import "ELCAssetCell.h"

#import "Keepy-Swift.h"

@interface ELCAssetTablePicker () {
    PHFetchResult* _result;
}

@property(nonatomic, assign) int columns;

@end

@implementation ELCAssetTablePicker

@synthesize parent = _parent;
@synthesize selectedAssetsLabel = _selectedAssetsLabel;
@synthesize singleSelection = _singleSelection;
@synthesize columns = _columns;

- (void)viewDidLoad {
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setAllowsSelection:NO];

    self.tableView.backgroundColor = [UIColor clearColor];

    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.tableView.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
    }

    if (self.immediateReturn) {

    } else {
        UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction:)];
        [self.navigationItem setRightBarButtonItem:doneButtonItem];
        [self.navigationItem setTitle:@"Loading..."];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.columns = self.view.bounds.size.width / 80;
}

- (void)viewDidLayoutSubviews {
    self.columns = self.view.bounds.size.width / 80;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.columns = self.view.bounds.size.width / 80;
    [self.tableView reloadData];
}

- (void)doneAction:(id)sender {
    NSMutableArray *selectedAssetsImages = [NSMutableArray new];

    for (KPAsset* a in self.assets) {
        if (a.selected) {
            [selectedAssetsImages addObject:a.asset];
        }
    }

    [self.parent selectedAssets:selectedAssetsImages];
}

- (void)assetSelected:(id)asset {
    if (self.singleSelection) {

        for (KPAsset* a in _assets) {
            if (asset != a) {
                a.selected = NO;
            }
        }

        [self.tableView reloadData];
    }
    if (self.immediateReturn) {
        NSArray *singleAssetArray = [NSArray arrayWithObject:[asset asset]];
        [(NSObject *) self.parent performSelector:@selector(selectedAssets:) withObject:singleAssetArray afterDelay:0];
    }


}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ceil(_assets.count / (float) self.columns);
}

- (NSArray *)assetsForIndexPath:(NSIndexPath *)path {
    NSUInteger index = path.row * self.columns;
    NSUInteger length = MIN(self.columns, _assets.count - index);
    NSArray<KPAsset*>* arr = [_assets subarrayWithRange:NSMakeRange(index, length)];
    for (KPAsset* i in arr) {
        i.parent = self;
    }
    return arr;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";

    ELCAssetCell *cell = (ELCAssetCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        cell = [[ELCAssetCell alloc] initWithAssets:[self assetsForIndexPath:indexPath] reuseIdentifier:CellIdentifier];

    } else {
        [cell setAssets:[self assetsForIndexPath:indexPath]];
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 79;
}

- (int)totalSelectedAssets {

    int count = 0;

    for (KPAsset* a in _assets) {
        if (a.selected) {
            count++;
        }
    }

    return count;
}

@end
