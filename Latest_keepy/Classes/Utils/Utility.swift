//
//  Utility.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/30/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

private var userAgent: String = {
    let d = Bundle.main.infoDictionary!
    let executable = d["CFBundleExecutable"]!
    let version = d["CFBundleShortVersionString"]!
    let build = d["CFBundleVersion"]!
    let model = UIDevice.current.model
    let system = UIDevice.current.systemVersion
    let scale = UIScreen.main.scale
    
    // Keepy/2.11 Build 114 (iPod touch; iOS 9.0.1; Scale/2.0)
    return String(format:"\(executable)/\(version) Build \(build) (\(model); iOS \(system); Scale/%0.1f)", scale)
}()

private var clientVersion: String = {
    let d = Bundle.main.infoDictionary!
    let version = d["CFBundleShortVersionString"]!
    
    // 2.11
    return String(format:"\(version)")
}()

extension NSString {
    class func stringOfUserAgent() -> NSString {
        return userAgent as NSString
    }
    
    class func stringOfClientVersion() -> NSString {
        return clientVersion as NSString
    }
}

class Stopwatch {
    let name: String
    let start: CFAbsoluteTime
    
    init(name aName: String) {
        name = aName
        start = CFAbsoluteTimeGetCurrent()
    }
    
    func lap(_ name: String) {
        let t = CFAbsoluteTimeGetCurrent()
        NSLog("STOPWATCH \(name) : \((t - start) * 1000)ms")
    }
}
