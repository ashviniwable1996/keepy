//
//  StoreKitManager.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/8/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

import UIKit
import StoreKit
import Mixpanel
import AppsFlyerLib
import FBSDKCoreKit

class StoreKitManager: NSObject {
    static let sharedManager = StoreKitManager()
    
    fileprivate override init() {
        super.init()
        SKPaymentQueue.default().add(self)
    }
    
    fileprivate var productsCompletion: ((_ response: SKProductsResponse?, _ error: Error?)->Void)?
    fileprivate var completion: ((_ response: NSDictionary?, _ error: Error?)->Void)?
    
    // MARK: -
    
    func productsWithIdentifiers(_ identifiers: Set<String>, completion:@escaping ((_ response: SKProductsResponse?, _ error: Error?)->Void)) {
        productsCompletion = completion
        
        let request = SKProductsRequest(productIdentifiers: identifiers)
        request.delegate = self
        request.start()
    }

    func buy(_ product: SKProduct, planType: String, couponCode: String, completion:@escaping ((_ response: NSDictionary?, _ error: Error?)->Void)) {
        self.completion = completion
        
        let d = UserDefaults.standard
        if d.object(forKey: "_planType") == nil {
            if SKPaymentQueue.canMakePayments() {
                d.set(planType, forKey: "_planType")
                d.set(couponCode, forKey: "_couponCode")
                let payment = SKPayment(product: product)
                SKPaymentQueue.default().add(payment)
            } else {
                Mixpanel.sharedInstance()?.track("my-keepies-failed", properties: [ "reason" : "not allowed"])
                
                self.error("please make sure in-app purchases are allowed. go to Settings>General>Restrictions")
            }
        } else {
            self.error("please restore your purchases")
        }
    }
    
    func restorePurchases(_ completion:@escaping ((_ response: NSDictionary?, _ error: Error?)->Void)) {
        self.completion = completion
        let q = SKPaymentQueue.default()
        q.add(self)
        q.restoreCompletedTransactions()
    }
    func commonMethodForPurchaseAndRenewalCode(transaction: SKPaymentTransaction, state: SKPaymentTransactionState) {
        // Aakash
        if  let values = UserDefaults.standard.value(forKey: "PlansObject") as? NSMutableArray {
            if values.count > 0 {
                for temp in values {
                    let dict = temp as? NSDictionary
                    if transaction.payment.productIdentifier == dict?.value(forKey: AFEventParamContentId) as? String {
                        
                        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: dict as? [String: Any])
                        if state == .purchased {
                        let priceOfProduct: Double = (dict?.value(forKey: AFEventParamRevenue) as! Double)
                        FBSDKAppEvents.logPurchase(priceOfProduct, currency: dict?.value(forKey: AFEventParamCurrency) as! String)
                        }
                        completeTransaction(transaction)
                        break
                    }
                }
            }
        }
    }
}

extension StoreKitManager : SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        productsCompletion?(response, nil)
    }
}

extension StoreKitManager : SKRequestDelegate {
    func requestDidFinish(_ request: SKRequest) {
        request.delegate = nil
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        productsCompletion?(nil, error)
        request.delegate = nil
    }
}

extension StoreKitManager : SKPaymentTransactionObserver {
    @available(iOS 11.0, *)
    func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
        return UserManager.sharedInstance().isLoggedIn()
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        print("removed transaction: \(transactions)")
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        print("restoreCompletedTransactionsFailedWithError: \(error)")
        completion?(nil, error)
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {

    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        Mixpanel.sharedInstance()?.track("receipt-updating-transactions", properties: [ "transaction-count": transactions.count ])

        if transactions.count == 0 {
            print("no completed transactions found.")
            self.error("sorry, it seems like no purchase was made from this account")
            return
        } else {
            print("transactions updated: \(transactions.count)")
        }
        
        for t in transactions {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TransactionUpdated"), object: nil, userInfo: ["transaction":t])
            
            switch t.transactionState {
            case .purchasing:
                break
            case .restored:
                self.commonMethodForPurchaseAndRenewalCode(transaction: t,state: .restored)

                break
            case .purchased:
                print("Transaction Details: \(t)")
               // Aakash
                self.commonMethodForPurchaseAndRenewalCode(transaction: t,state: .purchased)
//                if  let values = UserDefaults.standard.value(forKey: "PlansObject") as? NSMutableArray {
//                    if values.count > 0 {
//                        for temp in values {
//                            let dict = temp as? NSDictionary
//                            if t.payment.productIdentifier == dict?.value(forKey: AFEventParamContentId) as? String {
//
//                                AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: dict as? [String: Any])
////                                let priceOfProduct: Double = (dict?.value(forKey: AFEventParamRevenue) as! Double)
////                                FBSDKAppEvents.logPurchase(priceOfProduct, currency: dict?.value(forKey: AFEventParamCurrency) as! String)
//
//                                completeTransaction(t)
//                                break
//                            }
//                        }
//                    }
//                }
                

                break
            case .failed:
                failTransaction(t)
                break
            case .deferred:
                break
            }
        }
    }
}

// MARK: - Private

private extension StoreKitManager {
    
    func failTransaction(_ t: SKPaymentTransaction) {
        var properties = ["error" : "unkown error"]
        
        if let e = t.error {
            print("failTransaction: \(e)")
            if (e as NSError).code == SKError.Code.paymentCancelled.rawValue {
                properties["result"] = "cancelled"
            } else {
                properties["result"] = "failed"
            }
            completion?(nil, e)
            
        } else {
            print("failTransaction: WTF")
            self.error()
        }
        
        Mixpanel.sharedInstance()?.track("my-keepies-failed", properties: properties)
        finishTransaction(t)
    }
    
    func completeTransaction(_ t: SKPaymentTransaction) {
        if let url = Bundle.main.appStoreReceiptURL {
            if let data = try? Data(contentsOf: url) {
                let receipt = data.base64EncodedString(options: NSData.Base64EncodingOptions())
                saveReceipt(t, receipt: receipt)
                return
            }
        }
        
        self.error()
        finishTransaction(t)
    }

    func finishTransaction(_ t: SKPaymentTransaction) {
        SKPaymentQueue.default().finishTransaction(t)
        
        let d = UserDefaults.standard
        d.removeObject(forKey: "_planType")
        d.removeObject(forKey: "_couponCode")
        
        let q = SKPaymentQueue.default()
        NSLog("remaining transactions: \(q.transactions.count)")
        
        let n = NotificationCenter.default
        n.post(name: Notification.Name(rawValue: "PlanRefreshedNotification"), object: nil)
    }
    
    func saveReceipt(_ t: SKPaymentTransaction, receipt: String) {
        let d = UserDefaults.standard
        let planType = d.string(forKey: "_planType")
        let couponCode = d.string(forKey: "_couponCode")
        
        let productIdentifier = t.payment.productIdentifier
        print("Receipt of IAP: \n \n \(receipt) \n end")
        ServerComm.instance().verifyAppStoreReceipt(receipt, productId: productIdentifier, planType: planType, couponCode: couponCode) {
            (response: [AnyHashable: Any]!, error: Error!) in
            
            if let e = error {
                self.error(e.localizedDescription)
            } else {
                if let s = response["status"] as? NSNumber, s.intValue == 0 {
                    if let result = response["result"] as? NSDictionary {
                        self.handleResult(result)
                    } else {
                        self.error("unexpected server error")
                    }
                } else {
                    self.error("server response")
                }
            }
            self.finishTransaction(t)
        }
    }
    
    func handleResult(_ result: NSDictionary) {
        // {"status":0,"result":{"planType":2,"keepySpace":50,"keepyMonthlySpace":15,"expirationDate":"2015-11-03T00:19:33.000Z"},"error":null}
        
        print("transaction remaining: \( SKPaymentQueue.default().transactions.count )")
        if SKPaymentQueue.default().transactions.count == 1 {
            completion?(result, nil)
        }
        
        if let t = result["planType"] as? Int, let s = result["keepyMonthlySpace"] as? Int, let d = result["expirationDate"] as? String {
            Defaults.planType = Defaults.PlanType(rawValue: t) ?? .unknown
            Defaults.keepyMonthlySpace = s
            
            let f = DateFormatter()
            f.timeZone = TimeZone(abbreviation: "UTC")
            f.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
            Defaults.planExpirationDate = f.date(from: d.replacingOccurrences(of: "Etc/GMT", with: "GMT"))
            
            Mixpanel.sharedInstance()?.track("my-keepies-completed", properties:["result" : "success"])
        } else {
            Mixpanel.sharedInstance()?.track("my-keepies-completed", properties:["result" : "server error", "response" : String(describing: result)])
        }
    }
    
    func error(_ message: String = "there was a problem verifying your purchase. please try again soon") {
        Mixpanel.sharedInstance()?.track("my-keepies-completed", properties:["result" : "failed", "error" : message])
        
        let e = NSError(string: NSLocalizedString(message, comment: ""))
        completion?(nil, e)
    }
}
