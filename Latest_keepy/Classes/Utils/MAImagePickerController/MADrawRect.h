//
//  MADrawRect.h
//  instaoverlay
//
//  Created by Maximilian Mackh on 11/6/12.
//  Copyright (c) 2012 mackh ag. All rights reserved.
//

typedef enum {
    kOriginalCropMode,
    kCustomCropMode,
    kSquareCropMode,
    kSkewCropMode
} CropMode;

#import <UIKit/UIKit.h>
#import "MAConstants.h"

//            cd
//  d   -------------   c
//     |             |
//     |             |
//  da |             |  bc 
//     |             |
//     |             |
//     |             |
//  a   -------------   b
//            ab
//
// a = 1, b = 2, c = 3, d = 4

@class MADrawRect;
@protocol MADrawRectDelegate <NSObject>

@optional

- (void)pointTouchUp:(NSInteger)pointIndex;
- (void)rectDidChange:(MADrawRect* _Nonnull)rect;

@end

@interface MADrawRect : UIView {
    CGPoint touchOffset;
    CGPoint a;
    CGPoint b;
    CGPoint c;
    CGPoint d;

    BOOL frameMoved;

    CGPoint lastTouch;

    float originalSquareWidth;
    UIView *placeHolder;

}

@property(strong, nonatomic) UIButton * _Nonnull pointD;
@property(strong, nonatomic) UIButton * _Nonnull pointC;
@property(strong, nonatomic) UIButton * _Nonnull pointB;
@property(strong, nonatomic) UIButton * _Nonnull pointA;
@property(nonatomic, assign) id <MADrawRectDelegate> _Nullable delegate;
@property(nonatomic) CropMode cropMode;
@property(nonatomic) CGRect originalRect;


- (BOOL)frameEdited;

- (void)resetFrame;

- (CGPoint)coordinatesForPoint:(NSInteger)point withScaleFactor:(CGFloat)scaleFactor;

- (void)bottomLeftCornerToCGPoint:(CGPoint)point;

- (void)bottomRightCornerToCGPoint:(CGPoint)point;

- (void)topRightCornerToCGPoint:(CGPoint)point;

- (void)topLeftCornerToCGPoint:(CGPoint)point;

- (void)changeCropMode:(CropMode)cropMode;

- (void)caculateSquareSide;

- (void)updateOriginalRect;
@end
