//
//  MADrawRect.m
//  instaoverlay
//
//  Created by Maximilian Mackh on 11/6/12.
//  Copyright (c) 2012 mackh ag. All rights reserved.
//
// CGPoints Illustraition
//
//            cd
//  d   -------------   c
//     |             |
//     |             |
//  da |             |  bc
//     |             |
//     |             |
//     |             |
//  a   -------------   b
//            ab
//
// a = 1, b = 2, c = 3, d = 4

#import "MADrawRect.h"


@implementation MADrawRect

@synthesize pointD = _pointD;
@synthesize pointC = _pointC;
@synthesize pointB = _pointB;
@synthesize pointA = _pointA;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setPoints];
        [self setClipsToBounds:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        [self setUserInteractionEnabled:YES];
        [self setContentMode:UIViewContentModeRedraw];

        _pointD = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointD setTag:4];
        [_pointD setShowsTouchWhenHighlighted:YES];
        [_pointD addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointD addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointD addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointD addTarget:self action:@selector(pointTouchUp:withEvent:) forControlEvents:UIControlEventTouchUpInside];


        //[_pointD setImage:[self squareButtonWithWidth:kCropButtonSize] forState:UIControlStateNormal];
        [_pointD setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
        [_pointD setAccessibilityLabel:@"Draggable Crop Button Upper Left hand Corner. Double tap & hold to drag and adjust image frame."];
        [self addSubview:_pointD];

        _pointC = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointC setTag:3];
        [_pointC setShowsTouchWhenHighlighted:YES];
        [_pointC addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointC addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointC addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointC addTarget:self action:@selector(pointTouchUp:withEvent:) forControlEvents:UIControlEventTouchUpInside];

        //[_pointC setImage:[self squareButtonWithWidth:kCropButtonSize] forState:UIControlStateNormal];
        [_pointC setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
        [_pointC setAccessibilityLabel:@"Draggable Crop Button Upper Right hand Corner."];
        [self addSubview:_pointC];

        _pointB = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointB setTag:2];
        [_pointB setShowsTouchWhenHighlighted:YES];
        [_pointB addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointB addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointB addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointB addTarget:self action:@selector(pointTouchUp:withEvent:) forControlEvents:UIControlEventTouchUpInside];

        //[_pointB setImage:[self squareButtonWithWidth:kCropButtonSize] forState:UIControlStateNormal];
        [_pointB setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
        [_pointB setAccessibilityLabel:@"Draggable Crop Button Bottom Right hand Corner."];
        [self addSubview:_pointB];

        _pointA = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointA setTag:1];
        [_pointA setShowsTouchWhenHighlighted:YES];
        [_pointA addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointA addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointA addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointA addTarget:self action:@selector(pointTouchUp:withEvent:) forControlEvents:UIControlEventTouchUpInside];

        //[_pointA setImage:[self squareButtonWithWidth:kCropButtonSize] forState:UIControlStateNormal];
        [_pointA setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
        [_pointA setAccessibilityLabel:@"Draggable Crop Button Bottom Left hand Corner."];
        [self addSubview:_pointA];

        /*
        [_pointA setShowsTouchWhenHighlighted:NO];
        [_pointB setShowsTouchWhenHighlighted:NO];
        [_pointC setShowsTouchWhenHighlighted:NO];
        [_pointD setShowsTouchWhenHighlighted:NO];
        */
        [self setButtons];

    }
    return self;
}

- (UIImage *)squareButtonWithWidth:(NSInteger)width {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, width), NO, 0.0);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return blank;
}

- (void)setPoints {
    a = CGPointMake(0 + 10, self.bounds.size.height - 10);
    b = CGPointMake(self.bounds.size.width - 10, self.bounds.size.height - 10);
    c = CGPointMake(self.bounds.size.width - 10, 0 + 10);
    d = CGPointMake(0 + 10, 0 + 10);
}

- (void)setButtons {
    if (self.cropMode == kCustomCropMode) {
        [_pointD setFrame:CGRectMake(d.x - kCropButtonSize / 2 + 7, d.y - kCropButtonSize / 2 + 7, kCropButtonSize, kCropButtonSize)];
        [_pointC setFrame:CGRectMake(c.x - kCropButtonSize / 2 - 7, c.y - kCropButtonSize / 2 + 7, kCropButtonSize, kCropButtonSize)];
        [_pointB setFrame:CGRectMake(b.x - kCropButtonSize / 2 - 7, b.y - kCropButtonSize / 2 - 7, kCropButtonSize, kCropButtonSize)];
        [_pointA setFrame:CGRectMake(a.x - kCropButtonSize / 2 + 7, a.y - kCropButtonSize / 2 - 7, kCropButtonSize, kCropButtonSize)];

    } else {
        [_pointD setFrame:CGRectMake(d.x - kCropButtonSize / 2, d.y - kCropButtonSize / 2, kCropButtonSize, kCropButtonSize)];
        [_pointC setFrame:CGRectMake(c.x - kCropButtonSize / 2, c.y - kCropButtonSize / 2, kCropButtonSize, kCropButtonSize)];
        [_pointB setFrame:CGRectMake(b.x - kCropButtonSize / 2, b.y - kCropButtonSize / 2, kCropButtonSize, kCropButtonSize)];
        [_pointA setFrame:CGRectMake(a.x - kCropButtonSize / 2, a.y - kCropButtonSize / 2, kCropButtonSize, kCropButtonSize)];
    }
}

- (void)bottomLeftCornerToCGPoint:(CGPoint)point {
    a = point;
    [self needsRedraw];
}

- (void)bottomRightCornerToCGPoint:(CGPoint)point {
    b = point;
    [self needsRedraw];
}

- (void)topRightCornerToCGPoint:(CGPoint)point {
    c = point;
    [self needsRedraw];
}

- (void)topLeftCornerToCGPoint:(CGPoint)point {
    d = point;
    [self needsRedraw];
}

- (void)caculateSquareSide {
    originalSquareWidth = c.x - d.x;
    // DLog(@"originalSquareWidth %f", originalSquareWidth);
}

- (void)needsRedraw {
    frameMoved = YES;
    [self setNeedsDisplay];
    [self setButtons];
    [self drawRect:self.bounds];
}

- (void)drawRect:(CGRect)rect; {
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (context) {

        //[UIColor colorWithWhite:0.000 alpha:1.000];

        //CGContextSetRGBFillColor(context, 0.533f, 0.502f, 0.498f, 0.7f);
        CGContextSetRGBFillColor(context, 0, 0, 0, 0.65f);
        //CGContextSetRGBStrokeColor(context, 1.00f, 0.43f, 0.08f, 1.0f);
        CGContextSetRGBStrokeColor(context, 1.00f, 1.00f, 1.00f, 0.8f);
        CGContextSetLineJoin(context, kCGLineJoinRound);
        CGContextSetLineWidth(context, 1.0f);



        // CGRect boundingRect = CGContextGetClipBoundingBox(context);
        CGContextAddRect(context, self.originalRect);
        CGContextFillRect(context, self.originalRect);


        CGMutablePathRef pathRef = CGPathCreateMutable();
        CGPathMoveToPoint(pathRef, NULL, a.x, a.y);
        CGPathAddLineToPoint(pathRef, NULL, b.x, b.y);
        CGPathAddLineToPoint(pathRef, NULL, c.x, c.y);
        CGPathAddLineToPoint(pathRef, NULL, d.x, d.y);
        CGPathCloseSubpath(pathRef);

        //draw white line border
        CGContextAddPath(context, pathRef);
        CGContextStrokePath(context);

        //clear the crop area
        CGContextSetBlendMode(context, kCGBlendModeClear);

        CGContextAddPath(context, pathRef);
        CGContextFillPath(context);

        CGContextSetBlendMode(context, kCGBlendModeNormal);

        CGFloat dashArray[2];
        dashArray[0] = 5;
        dashArray[1] = 2;

        /*float lineWith = 1.0f;
        
        UIBezierPath *dash1V = [UIBezierPath bezierPath];
        [dash1V setLineWidth:lineWith];
        [dash1V setLineDash:dashArray count:2 phase:0];
        UIBezierPath *dash2V = [UIBezierPath bezierPath];
        [dash2V setLineWidth:lineWith];
        [dash2V setLineDash:dashArray count:2 phase:0];
        UIBezierPath *dash3V = [UIBezierPath bezierPath];
        [dash3V setLineWidth:lineWith];
        [dash3V setLineDash:dashArray count:2 phase:0];
        
        UIBezierPath *dash1H = [UIBezierPath bezierPath];
        [dash1H setLineWidth:lineWith];
        [dash1H setLineDash:dashArray count:2 phase:0];
        UIBezierPath *dash2H = [UIBezierPath bezierPath];
        [dash2H setLineWidth:lineWith];
        [dash2H setLineDash:dashArray count:2 phase:0];
        UIBezierPath *dash3H = [UIBezierPath bezierPath];
        [dash3H setLineWidth:lineWith];
        [dash3H setLineDash:dashArray count:2 phase:0];
        
        CGPoint v1Start = CGPointMake(c.x - (c.x-d.x)/2 / 0.6666, c.y - (c.y-d.y)/2 / 0.6666); // (2/3)
        CGPoint v1End = CGPointMake(a.x - (a.x-b.x)/2 / 2, a.y - (a.y-b.y)/2 / 2);
        [dash1V moveToPoint:v1Start];
        [dash1V addLineToPoint:v1End];
        [dash1V stroke];
        
        CGPoint v2Start = CGPointMake(c.x - (c.x-d.x)/2, c.y - (c.y-d.y)/2);
        CGPoint v2End = CGPointMake(a.x - (a.x-b.x)/2, a.y - (a.y-b.y)/2);
        [dash2V moveToPoint:v2Start];
        [dash2V addLineToPoint:v2End];
        [dash2V stroke];
        
        CGPoint v3Start = CGPointMake(c.x - (c.x-d.x)/2 / 2, c.y - (c.y-d.y)/2 / 2);
        CGPoint v3End = CGPointMake(a.x - (a.x-b.x)/2 / 0.6666, a.y - (a.y-b.y)/2 / 0.6666);
        [dash3V moveToPoint:v3Start];
        [dash3V addLineToPoint:v3End];
        [dash3V stroke];
        
        CGPoint h1Start = CGPointMake(d.x - (d.x-a.x)/2 / 0.6666, d.y - (d.y-a.y)/2 / 0.6666);
        CGPoint h1End = CGPointMake(b.x - (b.x-c.x)/2 / 2, b.y - (b.y-c.y)/2 / 2);
        [dash1H moveToPoint:h1Start];
        [dash1H addLineToPoint:h1End];
        [dash1H stroke];
        
        CGPoint h2Start = CGPointMake(d.x - (d.x-a.x)/2, d.y - (d.y-a.y)/2);
        CGPoint h2End = CGPointMake(b.x - (b.x-c.x)/2, b.y - (b.y-c.y)/2);
        [dash2H moveToPoint:h2Start];
        [dash2H addLineToPoint:h2End];
        [dash2H stroke];
        
        CGPoint h3Start = CGPointMake(d.x - (d.x-a.x)/2 / 2, d.y - (d.y-a.y)/2 / 2);
        CGPoint h3End = CGPointMake(b.x - (b.x-c.x)/2 / 0.6666, b.y - (b.y-c.y)/2 / 0.6666);
        [dash3V moveToPoint:h3Start];
        [dash3V addLineToPoint:h3End];
        [dash3V stroke];
        */
        CGPathRelease(pathRef);
    }
}

- (IBAction)pointMoveEnter:(id)sender withEvent:(UIEvent *)event {
    if (self.cropMode == kSquareCropMode) {
        return;
    }
    //DLog(@"move enter");
    UIControl *control = sender;
    CGPoint raw = [[[event allTouches] anyObject] locationInView:self];
    touchOffset = CGPointMake(raw.x - control.center.x, raw.y - control.center.y);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //Touches should only work on Square Crop Mode
    if (self.cropMode != kSquareCropMode) {
        return;
    }

    //DLog(@"touchesBegan");


    UITouch *aTouch = [touches anyObject];

    lastTouch = [aTouch locationInView:self];

    float minX = MIN(a.x, d.x);
    float minY = MIN(d.y, c.y);

    float maxX = MAX(b.x, c.x);
    float maxY = MAX(a.y, b.y);


    //make a square
    float squaredWidth = MIN(self.originalRect.size.height, self.originalRect.size.width);


    minX = MAX(self.originalRect.origin.x, lastTouch.x - (squaredWidth / 2));
    minY = MAX(self.originalRect.origin.y, lastTouch.y - (squaredWidth / 2));
    minX = MIN(self.originalRect.origin.x + self.originalRect.size.width - squaredWidth, minX);
    minY = MIN(self.originalRect.origin.y + self.originalRect.size.height - squaredWidth, minY);


    maxX = minX + squaredWidth;
    maxY = minY + squaredWidth;

    a = CGPointMake(minX, maxY);
    b = CGPointMake(maxX, maxY);
    c = CGPointMake(maxX, minY);
    d = CGPointMake(minX, minY);


    [self setNeedsDisplay];
    [self drawRect:self.bounds];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    //Touches should only work on Square Crop Mode
    if (self.cropMode != kSquareCropMode) {
        return;
    }

    /*
    UITouch *aTouch = [touches anyObject];
    CGPoint location = [aTouch locationInView:self.superview];
    [UIView beginAnimations:@"Dragging A DraggableView" context:nil];
    placeHolder.frame = CGRectMake(location.x, location.y,
                            placeHolder.frame.size.width, placeHolder.frame.size.height);
    [UIView commitAnimations];
    
    return;
    
    */

    // DLog(@"touchesMoved");




    //get square width
    float minX = MIN(a.x, d.x);
    float minY = MIN(d.y, c.y);

    float maxX = MAX(b.x, c.x);
    float maxY = MAX(a.y, b.y);


    //make a square
    float squaredWidth = MIN(self.originalRect.size.height, self.originalRect.size.width);


    UITouch *aTouch = [touches anyObject];
    CGPoint location = [aTouch locationInView:self];


    CGPoint Delta;

    Delta.x = lastTouch.x - location.x;
    Delta.y = lastTouch.y - location.y;

    //DLog(@"Delta %@", NSStringFromCGPoint(Delta));
    /*
    a.x += Delta.x;
    b.x += Delta.x;
    c.x += Delta.x;
    d.x += Delta.x;
    
    a.y += Delta.y;
    b.y += Delta.y;
    c.y += Delta.y;
    d.y += Delta.y;
    */

    /*minX = MAX(self.originalRect.origin.x, d.x-Delta.x*0.09);
    minY = MAX(self.originalRect.origin.y, d.y-Delta.y*0.09);
    minX = MIN(self.originalRect.origin.x + self.originalRect.size.width  - squaredWidth, minX);
    minY = MIN(self.originalRect.origin.y + self.originalRect.size.height - squaredWidth, minY);
    */

    // DLog(@"location %@", NSStringFromCGPoint(location));

    minX = MAX(self.originalRect.origin.x, location.x - (squaredWidth / 2));
    minY = MAX(self.originalRect.origin.y, location.y - (squaredWidth / 2));
    minX = MIN(self.originalRect.origin.x + self.originalRect.size.width - squaredWidth, minX);
    minY = MIN(self.originalRect.origin.y + self.originalRect.size.height - squaredWidth, minY);


    maxX = minX + squaredWidth;
    maxY = minY + squaredWidth;

    a = CGPointMake(minX, maxY);
    b = CGPointMake(maxX, maxY);
    c = CGPointMake(maxX, minY);
    d = CGPointMake(minX, minY);


    [self setNeedsDisplay];
    [self drawRect:self.bounds];

    if ([self.delegate respondsToSelector:@selector(rectDidChange:)]) {
        [self.delegate rectDidChange:self];
    }
}


- (IBAction)pointMoved:(id)sender withEvent:(UIEvent *)event {
    //DDLogInfo(@"moved");

    if (self.cropMode == kSquareCropMode) {
        return;
    }


    CGPoint point = [[[event allTouches] anyObject] locationInView:self];
    point = CGPointMake(point.x - touchOffset.x, point.y - touchOffset.y);

    if (CGRectContainsPoint(self.bounds, point)) {
        frameMoved = YES;
    }
    else {
        float kLineOffsetWidth = 2.0f;

        if (point.x < kLineOffsetWidth || point.x > self.bounds.size.width - kLineOffsetWidth) {
            if (point.x < kLineOffsetWidth) {
                point.x = kLineOffsetWidth;
            }
            else if (point.x > self.bounds.size.width - kLineOffsetWidth) {
                point.x = self.bounds.size.width - kLineOffsetWidth;
            }
        }

        if (point.y < kLineOffsetWidth || point.y > self.bounds.size.height - kLineOffsetWidth) {
            if (point.y < kLineOffsetWidth) {
                point.y = kLineOffsetWidth;
            }
            else if (point.y > self.bounds.size.height) {
                point.y = self.bounds.size.height - kLineOffsetWidth;
            }
        }

        frameMoved = YES;
    }

    if (self.cropMode == kSquareCropMode) {

    } else {


        UIControl *control = sender;
        control.center = point;

        switch (control.tag) {
            case 1:
                a = point;

                if (self.cropMode == kCustomCropMode) {
                    b = CGPointMake(b.x, a.y);
                    d = CGPointMake(a.x, d.y);
                }
                break;
            case 2:
                b = point;
                if (self.cropMode == kCustomCropMode) {
                    a = CGPointMake(a.x, b.y);
                    c = CGPointMake(b.x, c.y);
                }
                break;
            case 3:
                c = point;
                if (self.cropMode == kCustomCropMode) {
                    b = CGPointMake(c.x, b.y);
                    d = CGPointMake(d.x, c.y);
                }
                break;
            case 4:
                d = point;
                if (self.cropMode == kCustomCropMode) {
                    c = CGPointMake(c.x, d.y);
                    a = CGPointMake(d.x, a.y);
                }
                break;
        }
    }


    [self adjustPointsInsideThePhoto];

    [self setNeedsDisplay];
    [self setButtons];

    [self drawRect:self.bounds];
}

- (void)adjustPointsInsideThePhoto {
    //make sure that the crop only happens inside the photo area
    a.x = MAX(a.x, self.originalRect.origin.x);
    b.x = MAX(b.x, self.originalRect.origin.x);
    c.x = MAX(c.x, self.originalRect.origin.x);
    d.x = MAX(d.x, self.originalRect.origin.x);

    a.x = MIN(a.x, self.originalRect.origin.x + self.originalRect.size.width);
    b.x = MIN(b.x, self.originalRect.origin.x + self.originalRect.size.width);
    c.x = MIN(c.x, self.originalRect.origin.x + self.originalRect.size.width);
    d.x = MIN(d.x, self.originalRect.origin.x + self.originalRect.size.width);

    a.y = MAX(a.y, self.originalRect.origin.y);
    b.y = MAX(b.y, self.originalRect.origin.y);
    c.y = MAX(c.y, self.originalRect.origin.y);
    d.y = MAX(d.y, self.originalRect.origin.y);

    a.y = MIN(a.y, self.originalRect.origin.y + self.originalRect.size.height);
    b.y = MIN(b.y, self.originalRect.origin.y + self.originalRect.size.height);
    c.y = MIN(c.y, self.originalRect.origin.y + self.originalRect.size.height);
    d.y = MIN(d.y, self.originalRect.origin.y + self.originalRect.size.height);

    if (self.cropMode == kSquareCropMode) {



        /*
        
        c.x = b.x = MIN(c.x, a.x+squarewidth);
        
        d.y = c.y = MAX(d.y, self.originalRect.origin.y);
        
        a.y = b.y = MIN(a.y, d.y+squarewidth);*/


    }

}

- (IBAction)pointMoveExit:(id)sender withEvent:(UIEvent *)event {
    if (self.cropMode == kSquareCropMode) {
        return;
    }
    //DDLogInfo(@"move exit");
    touchOffset = CGPointZero;
}

- (IBAction)pointTouchUp:(id)sender withEvent:(UIEvent *)event {

    if (self.cropMode == kSquareCropMode) {
        return;
    }
    if ([self.delegate respondsToSelector:@selector(pointTouchUp:)]) {
        [self.delegate pointTouchUp:[(UIControl *) sender tag]];
    }
    if ([self.delegate respondsToSelector:@selector(rectDidChange:)]) {
        [self.delegate rectDidChange:self];
    }
}


- (void)resetFrame {
    [self setPoints];
    [self setNeedsDisplay];
    [self drawRect:self.bounds];
    frameMoved = NO;
    [self setButtons];
}

- (BOOL)frameEdited {
    return frameMoved;
}

- (CGPoint)coordinatesForPoint:(NSInteger)point withScaleFactor:(CGFloat)scaleFactor {
    CGPoint tmp = CGPointMake(0, 0);

    switch (point) {
        case 1:
            tmp = CGPointMake(a.x / scaleFactor, a.y / scaleFactor);
            break;
        case 2:
            tmp = CGPointMake(b.x / scaleFactor, b.y / scaleFactor);
            break;
        case 3:
            tmp = CGPointMake(c.x / scaleFactor, c.y / scaleFactor);
            break;
        case 4:
            tmp = CGPointMake(d.x / scaleFactor, d.y / scaleFactor);
            break;
    }

    //DDLogInfo(@"%@", NSStringFromCGPoint(tmp));

    return tmp;
}

//IPHONE-11
- (void)changeCropMode:(CropMode)cropMode {




    //UIPanGestureRecognizer * panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    switch (cropMode) {
        case kCustomCropMode:
            [_pointA setImage:[UIImage imageNamed:@"crop-custom-A-corner"] forState:UIControlStateNormal];
            [_pointB setImage:[UIImage imageNamed:@"crop-custom-B-corner"] forState:UIControlStateNormal];
            [_pointC setImage:[UIImage imageNamed:@"crop-custom-C-corner"] forState:UIControlStateNormal];
            [_pointD setImage:[UIImage imageNamed:@"crop-custom-D-corner"] forState:UIControlStateNormal];
            break;

        case kSquareCropMode:
            [_pointA setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
            [_pointB setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
            [_pointC setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
            [_pointD setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];


            break;

        case kSkewCropMode:
            [_pointA setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
            [_pointB setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
            [_pointC setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
            [_pointD setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
            break;

        default:
            [_pointA setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
            [_pointB setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
            [_pointC setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
            [_pointD setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
            break;
    }

    [_pointD setShowsTouchWhenHighlighted:(cropMode != kSquareCropMode)];
    [_pointC setShowsTouchWhenHighlighted:(cropMode != kSquareCropMode)];
    [_pointA setShowsTouchWhenHighlighted:(cropMode != kSquareCropMode)];
    [_pointB setShowsTouchWhenHighlighted:(cropMode != kSquareCropMode)];

    self.cropMode = cropMode;

    float minX;
    float minY;

    float maxX;
    float maxY;

    if (self.cropMode == kSquareCropMode) {
        //Square crop always starts at the center of the image
        minX = self.originalRect.origin.x;
        minY = self.originalRect.origin.y;;


        float squaredWidth = MIN(self.originalRect.size.width, self.originalRect.size.height);

        if (self.originalRect.size.width > self.originalRect.size.height) {
            minX += (self.originalRect.size.width - squaredWidth) / 2;
        } else {
            minY += (self.originalRect.size.height - squaredWidth) / 2;
        }


        maxX = minX + squaredWidth;
        maxY = minY + squaredWidth;

        a = CGPointMake(minX, maxY);
        b = CGPointMake(maxX, maxY);
        c = CGPointMake(maxX, minY);
        d = CGPointMake(minX, minY);

    } else {
        if (self.cropMode == kCustomCropMode) {
            //Custom mode adjusts the existing corner points
            float minX = MIN(a.x, d.x);
            float minY = MIN(d.y, c.y);

            float maxX = MAX(b.x, c.x);
            float maxY = MAX(a.y, b.y);

            a = CGPointMake(minX, maxY);
            b = CGPointMake(maxX, maxY);
            c = CGPointMake(maxX, minY);
            d = CGPointMake(minX, minY);
        } else {
            //skew mode should start with the photo bounds
            a = CGPointMake(self.originalRect.origin.x, self.originalRect.origin.y + self.originalRect.size.height);
            b = CGPointMake(self.originalRect.origin.x + self.originalRect.size.width, self.originalRect.origin.y + self.originalRect.size.height);
            c = CGPointMake(self.originalRect.origin.x + self.originalRect.size.width, self.originalRect.origin.y);
            d = CGPointMake(self.originalRect.origin.x, self.originalRect.origin.y);
        }
    }

    [self setNeedsDisplay];
    [self drawRect:self.bounds];

    [self setButtons];
}


- (void)updateOriginalRect {

    /*  placeHolder = [[UIView alloc] initWithFrame:self.originalRect];
     placeHolder.backgroundColor = [UIColor colorWithRed:1.000 green:0.000 blue:0.000 alpha:0.450];
     [self addSubview:placeHolder];
     */
}

/*

- (void)handlePanGesture:(UIPanGestureRecognizer *)gestureRecognizer
{
    CGRect frame = myDraggedView.frame;
    frame.origin = [gestureRecognizer locationInView:myDraggedView.superview];
    myDraggedView.frame = frame;
}*/

@end

