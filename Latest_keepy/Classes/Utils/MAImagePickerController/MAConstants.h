//
//  MAConstants.h
//  instaoverlay
//
//  Created by Maximilian Mackh on 11/6/12.
//  Copyright (c) 2012 mackh ag. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kImageCapturedSuccessfully @"imageCapturedSuccessfully"
#define kCameraToolBarHeight 66
#define kCameraFlashDefaultsKey @"MAImagePickerControllerFlashIsOn"
#define kCropButtonSize 100
#define kActivityIndicatorSize 100

@interface MAConstants : NSObject

@end
