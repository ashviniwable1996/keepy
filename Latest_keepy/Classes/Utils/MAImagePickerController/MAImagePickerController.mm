//
//  MAImagePickerController.m
//  instaoverlay
//
//  Created by Maximilian Mackh on 11/5/12.
//  Copyright (c) 2012 mackh ag. All rights reserved.
//

#import <Photos/Photos.h>

#import "MAImagePickerController.h"
#import "EditPicture2ViewController.h"
#import "ImageCropViewController.h"
#import "AppDelegate.h"
#import "GalleryPickerViewController.h"
#import "ELCAlbumPickerController.h"

#import <StoreKit/StoreKit.h>  // Horrible workaround.
#import "Keepy-Swift.h"

@interface MAImagePickerController () <EditPictureDelegate, GalleryPickerDelegate>
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;

@property(nonatomic, strong) ServerComm *serverComm;
@property(nonatomic, strong) UIPopoverController *popover;
@property(nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property(nonatomic, strong) NSMutableArray *assetsGroups;
@property(nonatomic, strong) UINavigationController *pickerNav;
@property(nonatomic) BOOL startedWithGallery;

@end

@implementation MAImagePickerController

@synthesize captureManager = _captureManager;
@synthesize cameraToolbar = _cameraToolbar;
//@synthesize flashButton = _flashButton;
@synthesize pictureButton = _pictureButton;
@synthesize cameraPictureTakenFlash = _cameraPictureTakenFlash;

@synthesize invokeCamera = _invokeCamera;

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:YES];
    [self.view setBackgroundColor:[UIColor blackColor]];

    self.title = @"take photo";
    //[self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];

    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[[UIImage imageNamed:@"button-defaultBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 15.5, 0, 15.5)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"button-defaultBG-disabled"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 15.5, 0, 15.5)] forState:UIControlStateDisabled];
    [btn setTitleColor:[UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:0.529 green:0.486 blue:0.478 alpha:1] forState:UIControlStateDisabled];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:12];
    [btn setTitle:KPLocalizedString(@"cancel") forState:UIControlStateNormal];

    btn.frame = CGRectMake(9, 15, 50, 30);
    [btn addTarget:self action:@selector(topBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];


    if (self.mode == 1) {
        [[Mixpanel sharedInstance] track:@"enter to kid photo camera mode"];
    } else {
        [[Mixpanel sharedInstance] track:@"enter to camera mode"];
    }


    //self.view.layer.cornerRadius = 8;
    //self.view.layer.masksToBounds = YES;

    if (_imageSource == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {

        [self setCaptureManager:[[MACaptureSession alloc] init]];
        [_captureManager addVideoInputFromCamera];
        [_captureManager addStillImageOutput];
        [_captureManager addVideoPreviewLayer];
        _captureManager.mode = self.mode;

        CGRect layerRect = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - kCameraToolBarHeight);
        [[_captureManager previewLayer] setBounds:layerRect];
        [[_captureManager previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect))];

        UIView *backView = [[UIView alloc] initWithFrame:self.view.frame];
        [self.view addSubview:backView];
        [backView.layer addSublayer:self.captureManager.previewLayer];
        [self.view sendSubviewToBack:backView];
        //[[[self view] layer] addSublayer:[[self captureManager] previewLayer]];

        UIImage *gridImage;

        /*
        if ([[UIScreen mainScreen] bounds].size.height == 568.000000)
        {
            gridImage = [UIImage imageNamed:@"camera-grid-1136@2x"];
        }
        else
        {
            gridImage = [UIImage imageNamed:@"camera-grid"];
        }
        */
        UIImageView *gridCameraView = [[UIImageView alloc] initWithImage:gridImage];
        [gridCameraView setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - kCameraToolBarHeight)];
        [[self view] addSubview:gridCameraView];

        _cameraToolbar = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - kCameraToolBarHeight, self.view.bounds.size.width, kCameraToolBarHeight)];
        _cameraToolbar.backgroundColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];


        self.pictureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.pictureButton.frame = CGRectMake((self.view.frame.size.width - 88) / 2, (kCameraToolBarHeight - 53) / 2, 88, 53);
        [self.pictureButton setImage:[UIImage imageNamed:@"camera_active"] forState:UIControlStateNormal];
        [self.pictureButton setImage:[UIImage imageNamed:@"camera_active_tapped"] forState:UIControlStateHighlighted];

        [self.pictureButton addTarget:self action:@selector(pictureMAIMagePickerController) forControlEvents:UIControlEventTouchUpInside];
        [_cameraToolbar addSubview:self.pictureButton];

        UIButton *libButton = [UIButton buttonWithType:UIButtonTypeCustom];
        libButton.frame = CGRectMake(20, (kCameraToolBarHeight - 44) / 2, 44, 44);
        [libButton setImage:[UIImage imageNamed:@"choose-from-library"] forState:UIControlStateNormal];
        [libButton addTarget:self action:@selector(openImageFromLibrary) forControlEvents:UIControlEventTouchUpInside];

        self.assetsGroups = [[NSMutableArray alloc] init];

        dispatch_async(dispatch_get_global_queue(0, 0), ^{

            self.assetsLibrary = [[ALAssetsLibrary alloc] init];
            [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                if (group) {
                    [self.assetsGroups addObject:group];
                }
                else {
                    group = [self.assetsGroups objectAtIndex:0];
                    [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {

                        if (result == nil) {
                            return;
                        }

                        if ([[result valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypePhoto]) {
                            UIImage *image = [UIImage imageWithCGImage:result.thumbnail];

                            CALayer *imageLayer = [CALayer layer];
                            imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                            imageLayer.contents = (id) image.CGImage;

                            imageLayer.masksToBounds = YES;
                            imageLayer.cornerRadius = 15.0;
                            imageLayer.borderColor = [[UIColor whiteColor] CGColor];
                            imageLayer.borderWidth = 5.0;

                            UIGraphicsBeginImageContext(image.size);
                            [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
                            UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
                            UIGraphicsEndImageContext();

                            dispatch_async(dispatch_get_main_queue(), ^{
                                [libButton setImage:img forState:UIControlStateNormal];
                            });

                            *stop = YES;

                        }
                    }];
                }
            }                               failureBlock:^(NSError *error) {
                DDLogInfo(@"A problem occured %@", [error description]);
                // an error here means that the asset groups were inaccessable.
                // Maybe the user or system preferences refused access.
            }];

        });


        [_cameraToolbar addSubview:libButton];


        if ([[NSUserDefaults standardUserDefaults] objectForKey:kCameraFlashDefaultsKey] == nil) {
            [self storeFlashSettingWithBool:YES];
        }

        /*
        if ([[NSUserDefaults standardUserDefaults] boolForKey:kCameraFlashDefaultsKey])
        {
            _flashButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"flash-on-button"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleFlash)];
            _flashButton.accessibilityLabel = @"Disable Camera Flash";
            flashIsOn = YES;
            [_captureManager setFlashOn:YES];
        }
        else
        {
            _flashButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"flash-off-button"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleFlash)];
            _flashButton.accessibilityLabel = @"Enable Camera Flash";
            flashIsOn = NO;
            [_captureManager setFlashOn:NO];
        }
        */
        [self.view addSubview:_cameraToolbar];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transitionToMAImagePickerControllerAdjustViewController:) name:kImageCapturedSuccessfully object:@(self.mode)];

        _cameraPictureTakenFlash = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - kCameraToolBarHeight)];
        [_cameraPictureTakenFlash setBackgroundColor:[UIColor colorWithRed:0.99f green:0.99f blue:1.00f alpha:1.00f]];
        [_cameraPictureTakenFlash setUserInteractionEnabled:NO];
        [_cameraPictureTakenFlash setAlpha:0.0f];
        [self.view addSubview:_cameraPictureTakenFlash];

        /*
        [GlobalUtils showTip:4 withCompleteBlock:^{
        }];
*/
    }
    else {
        /*
        _invokeCamera = [[UIImagePickerController alloc] init];
        _invokeCamera.delegate = self;
        _invokeCamera.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        _invokeCamera.allowsEditing = NO;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
            self.popover = [[UIPopoverController alloc] initWithContentViewController:_invokeCamera];
            [self.popover presentPopoverFromRect:CGRectMake(50, 100, 320, 500) inView:d.window permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        } else {
            
            [self addChildViewController:_invokeCamera];
            [_invokeCamera didMoveToParentViewController:self];
            [self.view addSubview:_invokeCamera.view];
        }    
         */

        self.startedWithGallery = YES;

        self.assetsGroups = [[NSMutableArray alloc] init];

        dispatch_async(dispatch_get_global_queue(0, 0), ^{

            self.assetsLibrary = [[ALAssetsLibrary alloc] init];
            [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                if (group) {
                    [self.assetsGroups addObject:group];
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self openImageFromLibrary];
                    });
                }
            }                               failureBlock:^(NSError *error) {
                DDLogInfo(@"A problem occured %@", [error description]);
                // an error here means that the asset groups were inaccessable.
                // Maybe the user or system preferences refused access.
            }];
        });
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    DDLogInfo(@"%2f %2f", self.view.frame.size.width, self.view.frame.size.height);

    if (imagePickerDismissed) {
        return;
    }

    if (self.serverComm != nil) {
        [self.serverComm cancelOperation];
    }

    if (_imageSource == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [_pictureButton setEnabled:YES];
        [[_captureManager captureSession] startRunning];
    }
    else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && self.popover == nil) {
        AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
        self.popover = [[UIPopoverController alloc] initWithContentViewController:_invokeCamera];
        [self.popover presentPopoverFromRect:CGRectMake(50, 100, 320, 500) inView:d.window permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
        //[self presentModalViewController:_invokeCamera animated:YES];
    }

}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)dismissMAImagePickerController {
    if (_imageSource == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [[_captureManager captureSession] stopRunning];
    }
    else {
        [_invokeCamera removeFromParentViewController];
        imagePickerDismissed = YES;
        [self dismissViewControllerAnimated:YES completion:NULL];
    }

    if (self.mode == 1) {
        [self.delegate imagePicked:nil];
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MAIPCFail" object:nil];
    }

    /*[self.navigationController dismissViewControllerAnimated:YES completion:^(void)
    {
    }];
     */
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (_imageSource == 0) {
        [[_captureManager captureSession] stopRunning];
    }
}

- (void)pictureMAIMagePickerController {
    [_pictureButton setEnabled:NO];
    [_captureManager captureStillImage];
}

- (void)openImageFromLibrary {
    [[UserManager sharedInstance] setCameraMode:1];


    [[Mixpanel sharedInstance] track:@"Photo Library"];
    [[Mixpanel sharedInstance] registerSuperProperties:[NSDictionary dictionaryWithObjectsAndKeys:@"Camera roll", @"PhotoSource", nil]];

    PHFetchOptions* options = [PHFetchOptions new];
    PHFetchResult* images = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
    
    NSMutableArray* arr = [NSMutableArray array];
    
    [images enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
        [arr addObject:[[KPAsset alloc] initWithAsset:asset]];
        
        if (idx + 1 == images.count) {
            ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName:nil bundle:nil];
            albumController.delegate = self;
            albumController.filter = @"allPhotos";
            GalleryPickerViewController *avc = [[GalleryPickerViewController alloc] initWithAssets:arr];
            avc.delegate = self;
            self.pickerNav = [[UINavigationController alloc] initWithRootViewController:albumController];
            [self.pickerNav pushViewController:avc animated:NO];
            self.pickerNav.view.bounds = CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:self.pickerNav.view];
        }
    }];
}

- (void)toggleFlash {
    /*
    if (flashIsOn)
    {
        flashIsOn = NO;
        [_captureManager setFlashOn:NO];
        [_flashButton setImage:[UIImage imageNamed:@"flash-off-button"]];
        _flashButton.accessibilityLabel = @"Enable Camera Flash";
        [self storeFlashSettingWithBool:NO];
    }
    else
    {
        flashIsOn = YES;
        [_captureManager setFlashOn:YES];
        [_flashButton setImage:[UIImage imageNamed:@"flash-on-button"]];
        _flashButton.accessibilityLabel = @"Disable Camera Flash";
        [self storeFlashSettingWithBool:YES];
    }*/
}

- (void)storeFlashSettingWithBool:(BOOL)flashSetting {
    [[NSUserDefaults standardUserDefaults] setBool:flashSetting forKey:kCameraFlashDefaultsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)transitionToMAImagePickerControllerAdjustViewController:(NSNotification *)notification {
    if ([notification.object intValue] != self.mode) {
        return;
    }

    [[_captureManager captureSession] stopRunning];

    [UIView beginAnimations:@"fade" context:nil];
    [UIView setAnimationDuration:0.05f];
    [UIView setAnimationDelegate:self];
    _cameraPictureTakenFlash.alpha = 0.5f;
    [UIView commitAnimations];

    [UIView beginAnimations:@"fade" context:nil];
    [UIView setAnimationDelay:0.05f];
    [UIView setAnimationDuration:0.01f];
    [UIView setAnimationDelegate:self];
    _cameraPictureTakenFlash.alpha = 0.0f;
    [UIView setAnimationDidStopSelector:@selector(transitionToMAImagePickerControllerAdjustViewControllerSelector)];
    [UIView commitAnimations];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissMAImagePickerController];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    [_invokeCamera removeFromParentViewController];
    imagePickerDismissed = YES;
    [self.navigationController popViewControllerAnimated:NO];
    if (self.mode == 1) {
        [self showEditProfilePhoto:[info objectForKey:UIImagePickerControllerOriginalImage]];
    } else {
        [self showFinalView:info applyCrop:YES];
    }

    [self.popover dismissPopoverAnimated:YES];
    self.popover = nil;
}

- (void)transitionToMAImagePickerControllerAdjustViewControllerSelector {
    if (self.mode == 1) {
        [self showEditProfilePhoto:self.captureManager.stillImage];
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImageWriteToSavedPhotosAlbum(self.captureManager.stillImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
        });
        [self showFinalView:[NSDictionary dictionaryWithObjectsAndKeys:self.captureManager.stillImage, UIImagePickerControllerOriginalImage, nil] applyCrop:YES];
    }

}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (error != NULL) {
    }

    DDLogInfo(@"saved image to camera roll");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)topBtnTap:(UIButton *)sender {
    [self dismissMAImagePickerController];
}

- (void)showFinalView:(NSDictionary *)info applyCrop:(BOOL)applyCrop {
    /*
    NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], @"xxx.jpg"];
    NSData* imageData = UIImagePNGRepresentation([info valueForKey:UIImagePickerControllerOriginalImage]);
    [imageData writeToFile:tmpPath atomically:NO];
    */
    //ImageEditorViewController *avc = [[ImageEditorViewController alloc] initWithImageInfo:info applyCrop:applyCrop];

    UIImage *aimage = [info valueForKey:UIImagePickerControllerOriginalImage];
    /*
    NSData* jpgData =  UIImageJPEGRepresentation(aimage, 0.8);
    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)jpgData, NULL);
    NSDictionary *metadata = (__bridge NSDictionary *)CGImageSourceCopyPropertiesAtIndex(source, 0, NULL);
    DDLogInfo(@"metadata:%@", metadata);
    */
    DDLogInfo(@"image dims:%2f %2f", aimage.size.width, aimage.size.height);
    if (aimage.size.width > 1500 || aimage.size.height > 1500) {
        aimage = [aimage scaleProportionalToSize:CGSizeMake(1500, 1500)];
    }

    aimage = [aimage fixOrientation];

    NSMutableDictionary *newInfo = [[NSMutableDictionary alloc] initWithDictionary:info];
    [newInfo setValue:aimage forKey:UIImagePickerControllerOriginalImage];

    ImageCropViewController *avc = [[ImageCropViewController alloc] initWithImageInfo:newInfo];
    [self.navigationController pushViewController:avc animated:YES];

    /*
    NSMutableDictionary *newInfo = [[NSMutableDictionary alloc] initWithDictionary:info];
    UIImage *fixedImage = [info valueForKey:UIImagePickerControllerOriginalImage];//[KPOpenCV rotateImage:[info valueForKey:UIImagePickerControllerOriginalImage]];
    
    [[ServerComm instance] prepareAsset:^(id result) {
        int assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] intValue];
        if (assetId > 0)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                NSData *data = UIImageJPEGRepresentation(fixedImage, 0.8);
                if (self.serverComm == nil)
                    self.serverComm = [ServerComm instance];
                
                [self.serverComm uploadAsset:assetId withData:data withSuccessBlock:^(id result)
                 {
                     DDLogInfo(@"Uploaded...");
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                 } andFailBlock:^(NSError *error) {
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                 }];                
            });
            
            MAImagePickerFinalViewController *finalView = [[MAImagePickerFinalViewController alloc] init];
            finalView.sourceImage = fixedImage;
            finalView.imageFrameEdited = YES;
            
            NSMutableDictionary *newInfo = [[NSMutableDictionary alloc] initWithDictionary:info];
            [newInfo setValue:@(assetId) forKey:@"assetId"];
            [newInfo setValue:fixedImage forKey:UIImagePickerControllerOriginalImage];
            finalView.originalImageInfo = newInfo;
            
            CATransition* transition = [CATransition animation];
            transition.duration = 0.4;
            transition.type = kCATransitionFade;
            transition.subtype = kCATransitionFromBottom;
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController pushViewController:finalView animated:NO];
            
        }
    } andFailBlock:^(NSError *error) {
        
    }];
     */
}

- (void)showEditProfilePhoto:(UIImage *)image {
    image = [image fixOrientation];
    EditPicture2ViewController *avc = [[EditPicture2ViewController alloc] initWithImage:image];
    avc.delegate = self;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromBottom;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:avc animated:NO];
}

- (void)pictureSelected:(UIImage *)selectedImage {
    [self.delegate imagePicked:selectedImage];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)galleryPickerController:(GalleryPickerViewController *)picker didFinishPickingMediaWithInfo:(NSArray *)info {
    imagePickerDismissed = YES;
    [self.navigationController popViewControllerAnimated:NO];
    if (self.mode == 1) {
        [self showEditProfilePhoto:[[info objectAtIndex:0] objectForKey:UIImagePickerControllerOriginalImage]];
    } else {
        [self showFinalView:[info objectAtIndex:0] applyCrop:YES];
    }

    [self.popover dismissPopoverAnimated:YES];
    self.popover = nil;
}

- (void)galleryPickerControllerDidCancel:(GalleryPickerViewController *)picker {
    if (self.startedWithGallery) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    } else {
        [picker.navigationController.view removeFromSuperview];
    }

}


@end
