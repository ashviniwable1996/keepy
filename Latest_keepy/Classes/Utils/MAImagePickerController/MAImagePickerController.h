//
//  MAImagePickerController.h
//  instaoverlay
//
//  Created by Maximilian Mackh on 11/5/12.
//  Copyright (c) 2012 mackh ag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MACaptureSession.h"
#import "MAConstants.h"

@protocol MAImagePickerDelegate

- (void)imagePicked:(UIImage *)image;

@end

@interface MAImagePickerController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    BOOL flashIsOn;
    BOOL imagePickerDismissed;
}

@property(strong, nonatomic) MACaptureSession *captureManager;
@property(strong, nonatomic) UIView *cameraToolbar;
@property(strong, nonatomic) UIView *cameraPictureTakenFlash;
@property(strong, nonatomic) UIButton *pictureButton;

@property(strong, nonatomic) UIImagePickerController *invokeCamera;
@property int imageSource; // 0 -> Camera, 1 -> Library
@property(nonatomic, assign) id <MAImagePickerDelegate> delegate;
@property(nonatomic) int mode; //0 - > Item Snap, 1 - > Kid Profile Image Snap

@end
