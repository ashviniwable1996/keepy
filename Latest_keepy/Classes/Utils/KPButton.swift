//
//  KPButton.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/5/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class KPButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let imageView = imageView, let titleLabel = titleLabel {
            titleLabel.sizeToFit()
            
            imageView.frame.origin.x = (bounds.width - imageView.bounds.width) / 2
            titleLabel.frame.origin.x = (bounds.width - titleLabel.bounds.width) / 2
            
            imageView.frame.origin.y = (bounds.height - (imageView.bounds.height + titleLabel.bounds.height)) / 2
            titleLabel.frame.origin.y = imageView.frame.maxY
        }
    }
}
