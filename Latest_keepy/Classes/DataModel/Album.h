//
//  Album.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item, Kid;

@interface Album : NSManagedObject

@property(nonatomic, retain) NSNumber *year;
@property(nonatomic, retain) NSSet *items;
@property(nonatomic, retain) Kid *kid;
@end

@interface Album (CoreDataGeneratedAccessors)

- (void)addItemsObject:(Item *)value;

- (void)removeItemsObject:(Item *)value;

- (void)addItems:(NSSet *)values;

- (void)removeItems:(NSSet *)values;

@end
