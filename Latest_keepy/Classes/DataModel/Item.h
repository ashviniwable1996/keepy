//
//  Item.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Album, Asset, Comment, Kid, Place, Story, Tag;

@interface Item : NSManagedObject

@property(nonatomic, retain) NSNumber *autoEnhanceOn;
@property(nonatomic, retain) NSNumber *bri;
@property(nonatomic, retain) NSNumber *con;
@property(nonatomic, retain) NSDate *createDate;
@property(nonatomic, retain) NSString *cropRect;
@property(nonatomic, retain) NSString *extraData;
@property(nonatomic, retain) NSNumber *filter;
@property(nonatomic, retain) NSNumber *filterId;
@property(nonatomic, retain) NSDate *itemDate;
@property(nonatomic, retain) NSNumber *itemId;
@property(nonatomic, retain) NSNumber *itemType;
@property(nonatomic, retain) NSString *kidsIds;
@property(nonatomic, retain) NSNumber *readAction;
@property(nonatomic, retain) NSNumber *rotation;
@property(nonatomic, retain) NSNumber *sat;
@property(nonatomic, retain) NSNumber *shareStatus;
@property(nonatomic, retain) NSString *title;
@property(nonatomic, retain) NSDate *updateDate;
@property(nonatomic, retain) NSSet *albums;
@property(nonatomic, retain) NSSet *comments;
@property(nonatomic, retain) Asset *image;
@property(nonatomic, retain) NSSet *kids;
@property(nonatomic, retain) Asset *originalImage;
@property(nonatomic, retain) Place *place;
@property(nonatomic, retain) Story *story;
@property(nonatomic, retain) NSSet *tags;
@end

@interface Item (CoreDataGeneratedAccessors)

- (void)addAlbumsObject:(Album *)value;

- (void)removeAlbumsObject:(Album *)value;

- (void)addAlbums:(NSSet *)values;

- (void)removeAlbums:(NSSet *)values;

- (void)addCommentsObject:(Comment *)value;

- (void)removeCommentsObject:(Comment *)value;

- (void)addComments:(NSSet *)values;

- (void)removeComments:(NSSet *)values;

- (void)addKidsObject:(Kid *)value;

- (void)removeKidsObject:(Kid *)value;

- (void)addKids:(NSSet *)values;

- (void)removeKids:(NSSet *)values;

- (void)addTagsObject:(Tag *)value;

- (void)removeTagsObject:(Tag *)value;

- (void)addTags:(NSSet *)values;

- (void)removeTags:(NSSet *)values;

@end
