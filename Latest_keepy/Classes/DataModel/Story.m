//
//  Story.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Story.h"


@implementation Story

@dynamic storyId;
@dynamic storyLength;
@dynamic asset;
@dynamic item;

@end
