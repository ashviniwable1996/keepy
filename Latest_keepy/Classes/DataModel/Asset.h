//
//  Asset.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment, Item, Kid, Story;

@interface Asset : NSManagedObject

@property(nonatomic, retain) NSString *assetHash;
@property(nonatomic, retain) NSNumber *assetId;
@property(nonatomic, retain) NSNumber *assetStatus;
@property(nonatomic, retain) NSNumber *assetType;
@property(nonatomic, retain) Comment *commentForPreview;
@property(nonatomic, retain) Comment *commentForVideo;
@property(nonatomic, retain) Item *item;
@property(nonatomic, retain) Item *itemForOriginalImage;
@property(nonatomic, retain) Kid *kid;
@property(nonatomic, retain) Story *story;

@end
