//
//  Place.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

@interface Place : NSManagedObject

@property(nonatomic, retain) NSNumber *lat;
@property(nonatomic, retain) NSNumber *lon;
@property(nonatomic, retain) NSNumber *placeId;
@property(nonatomic, retain) NSString *title;
@property(nonatomic, retain) NSSet *items;
@end

@interface Place (CoreDataGeneratedAccessors)

- (void)addItemsObject:(Item *)value;

- (void)removeItemsObject:(Item *)value;

- (void)addItems:(NSSet *)values;

- (void)removeItems:(NSSet *)values;

@end
