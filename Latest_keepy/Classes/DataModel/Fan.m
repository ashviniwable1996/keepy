//
//  Fan.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Fan.h"


@implementation Fan

@dynamic birthdate;
@dynamic didApprove;
@dynamic email;
@dynamic facebookId;
@dynamic fanId;
@dynamic nickname;
@dynamic relationType;
@dynamic userId;
@dynamic kids;

@end
