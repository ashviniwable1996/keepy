//
//  Kid.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//


@implementation Kid

@dynamic birthdate;
@dynamic gender;
@dynamic isMine;
@dynamic kidId;
@dynamic name;
@dynamic userNickname;
@dynamic userRelationType;
@dynamic albums;
@dynamic fans;
@dynamic image;
@dynamic items;
@dynamic parents;

@end
