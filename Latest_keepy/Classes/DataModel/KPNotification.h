//
//  KPNotification.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface KPNotification : NSManagedObject

@property(nonatomic, retain) NSString *items;
@property(nonatomic, retain) NSDate *notificationDate;
@property(nonatomic, retain) NSNumber *notificationId;
@property(nonatomic, retain) NSNumber *notificationType;
@property(nonatomic, retain) NSNumber *senderId;
@property(nonatomic, retain) NSNumber *status;
@property(nonatomic, retain) NSString *subject;

@end
