//
//  Asset.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//


@implementation Asset

@dynamic assetHash;
@dynamic assetId;
@dynamic assetStatus;
@dynamic assetType;
@dynamic commentForPreview;
@dynamic commentForVideo;
@dynamic item;
@dynamic itemForOriginalImage;
@dynamic kid;
@dynamic story;

@end
