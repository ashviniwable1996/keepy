//
//  Tag.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

@interface Tag : NSManagedObject

@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSNumber *tagId;
@property(nonatomic, retain) NSSet *items;
@end

@interface Tag (CoreDataGeneratedAccessors)

- (void)addItemsObject:(Item *)value;

- (void)removeItemsObject:(Item *)value;

- (void)addItems:(NSSet *)values;

- (void)removeItems:(NSSet *)values;

@end
