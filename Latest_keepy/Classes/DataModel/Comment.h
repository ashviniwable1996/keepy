//
//  Comment.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset, Item, User;

@interface Comment : NSManagedObject

@property(nonatomic, retain) NSDate *commentDate;
@property(nonatomic, retain) NSNumber *commentId;
@property(nonatomic, retain) NSNumber *commentLength;
@property(nonatomic, retain) NSNumber *commentType;
@property(nonatomic, retain) NSNumber *creatorFanId;
@property(nonatomic, retain) NSString *creatorName;
@property(nonatomic, retain) NSNumber *creatorRelationType;
@property(nonatomic, retain) NSNumber *creatorUserId;
@property(nonatomic, retain) NSString *extraData;
@property(nonatomic, retain) NSNumber *isStory;
@property(nonatomic, retain) NSNumber *wasRead;
@property(nonatomic, retain) Item *item;
@property(nonatomic, retain) Asset *previewImage;
@property(nonatomic, retain) User *user;
@property(nonatomic, retain) Asset *video;

@end
