//
//  KPNotification.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPNotification.h"


@implementation KPNotification

@dynamic items;
@dynamic notificationDate;
@dynamic notificationId;
@dynamic notificationType;
@dynamic senderId;
@dynamic status;
@dynamic subject;

@end
