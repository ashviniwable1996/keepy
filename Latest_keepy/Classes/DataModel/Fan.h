//
//  Fan.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Kid;

@interface Fan : NSManagedObject

@property(nonatomic, retain) NSDate *birthdate;
@property(nonatomic, retain) NSNumber *didApprove;
@property(nonatomic, retain) NSString *email;
@property(nonatomic, retain) NSString *facebookId;
@property(nonatomic, retain) NSNumber *fanId;
@property(nonatomic, retain) NSString *nickname;
@property(nonatomic, retain) NSNumber *relationType;
@property(nonatomic, retain) NSNumber *userId;
@property(nonatomic, retain) NSSet *kids;
@end

@interface Fan (CoreDataGeneratedAccessors)

- (void)addKidsObject:(Kid *)value;

- (void)removeKidsObject:(Kid *)value;

- (void)addKids:(NSSet *)values;

- (void)removeKids:(NSSet *)values;

@end
