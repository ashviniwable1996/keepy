//
//  Story.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset, Item;

@interface Story : NSManagedObject

@property(nonatomic, retain) NSNumber *storyId;
@property(nonatomic, retain) NSNumber *storyLength;
@property(nonatomic, retain) Asset *asset;
@property(nonatomic, retain) Item *item;

@end
