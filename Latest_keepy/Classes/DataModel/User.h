//
//  User.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment, Kid, User;

@interface User : NSManagedObject

@property(nonatomic, retain) NSString *authKey;
@property(nonatomic, retain) NSDate *birthdate;
@property(nonatomic, retain) NSString *country;
@property(nonatomic, retain) NSDate *createDate;
@property(nonatomic, retain) NSString *email;
@property(nonatomic, retain) NSDate *expirationDate;
@property(nonatomic, retain) NSString *facebookId;
@property(nonatomic, retain) NSNumber *isMe;
@property(nonatomic, retain) NSNumber *keepiesUsedThisMonth;
@property(nonatomic, retain) NSNumber *keepyMonthlySpace;
@property(nonatomic, retain) NSNumber *keepySpace;
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSString *otherTxt;
@property(nonatomic, retain) NSNumber *parentType;
@property(nonatomic, retain) NSNumber *planType;
@property(nonatomic, retain) NSString *registrationSignature;
@property(nonatomic, retain) NSNumber *storageLimit;
@property(nonatomic, retain) NSNumber *totalUsedBytes;
@property(nonatomic, retain) NSString *twitterHandle;
@property(nonatomic, retain) NSString *twitterId;
@property(nonatomic, retain) NSNumber *userId;
@property(nonatomic, retain) NSString *zazzleUrl;
@property(nonatomic, retain) NSSet *comments;
@property(nonatomic, retain) NSSet *fans;
@property(nonatomic, retain) NSSet *following;
@property(nonatomic, retain) NSSet *kids;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(Comment *)value;

- (void)removeCommentsObject:(Comment *)value;

- (void)addComments:(NSSet *)values;

- (void)removeComments:(NSSet *)values;

- (void)addFansObject:(User *)value;

- (void)removeFansObject:(User *)value;

- (void)addFans:(NSSet *)values;

- (void)removeFans:(NSSet *)values;

- (void)addFollowingObject:(User *)value;

- (void)removeFollowingObject:(User *)value;

- (void)addFollowing:(NSSet *)values;

- (void)removeFollowing:(NSSet *)values;

- (void)addKidsObject:(Kid *)value;

- (void)removeKidsObject:(Kid *)value;

- (void)addKids:(NSSet *)values;

- (void)removeKids:(NSSet *)values;

@end
