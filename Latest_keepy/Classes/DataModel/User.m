//
//  User.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//


@implementation User

@dynamic authKey;
@dynamic birthdate;
@dynamic country;
@dynamic createDate;
@dynamic email;
@dynamic expirationDate;
@dynamic facebookId;
@dynamic isMe;
@dynamic keepiesUsedThisMonth;
@dynamic keepyMonthlySpace;
@dynamic keepySpace;
@dynamic name;
@dynamic otherTxt;
@dynamic parentType;
@dynamic planType;
@dynamic registrationSignature;
@dynamic storageLimit;
@dynamic totalUsedBytes;
@dynamic twitterHandle;
@dynamic twitterId;
@dynamic userId;
@dynamic zazzleUrl;
@dynamic comments;
@dynamic fans;
@dynamic following;
@dynamic kids;

@end
