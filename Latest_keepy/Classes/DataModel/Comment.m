//
//  Comment.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Comment.h"


@implementation Comment

@dynamic commentDate;
@dynamic commentId;
@dynamic commentLength;
@dynamic commentType;
@dynamic creatorFanId;
@dynamic creatorName;
@dynamic creatorRelationType;
@dynamic creatorUserId;
@dynamic extraData;
@dynamic isStory;
@dynamic wasRead;
@dynamic item;
@dynamic previewImage;
@dynamic user;
@dynamic video;

@end
