//
//  Tag.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Tag.h"


@implementation Tag

@dynamic name;
@dynamic tagId;
@dynamic items;

@end
