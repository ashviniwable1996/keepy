//
//  Item.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//


@implementation Item

@dynamic autoEnhanceOn;
@dynamic bri;
@dynamic con;
@dynamic createDate;
@dynamic cropRect;
@dynamic extraData;
@dynamic filter;
@dynamic filterId;
@dynamic itemDate;
@dynamic itemId;
@dynamic itemType;
@dynamic kidsIds;
@dynamic readAction;
@dynamic rotation;
@dynamic sat;
@dynamic shareStatus;
@dynamic title;
@dynamic updateDate;
@dynamic albums;
@dynamic comments;
@dynamic image;
@dynamic kids;
@dynamic originalImage;
@dynamic place;
@dynamic story;
@dynamic tags;

@end
