//
//  Kid.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Album, Asset, Fan, Item, User;

@interface Kid : NSManagedObject

@property(nonatomic, retain) NSDate *birthdate;
@property(nonatomic, retain) NSNumber *gender;
@property(nonatomic, retain) NSNumber *isMine;
@property(nonatomic, retain) NSNumber *kidId;
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSString *userNickname;
@property(nonatomic, retain) NSNumber *userRelationType;
@property(nonatomic, retain) NSSet *albums;
@property(nonatomic, retain) NSSet *fans;
@property(nonatomic, retain) Asset *image;
@property(nonatomic, retain) NSSet *items;
@property(nonatomic, retain) NSSet *parents;
@end

@interface Kid (CoreDataGeneratedAccessors)

- (void)addAlbumsObject:(Album *)value;

- (void)removeAlbumsObject:(Album *)value;

- (void)addAlbums:(NSSet *)values;

- (void)removeAlbums:(NSSet *)values;

- (void)addFansObject:(Fan *)value;

- (void)removeFansObject:(Fan *)value;

- (void)addFans:(NSSet *)values;

- (void)removeFans:(NSSet *)values;

- (void)addItemsObject:(Item *)value;

- (void)removeItemsObject:(Item *)value;

- (void)addItems:(NSSet *)values;

- (void)removeItems:(NSSet *)values;

- (void)addParentsObject:(User *)value;

- (void)removeParentsObject:(User *)value;

- (void)addParents:(NSSet *)values;

- (void)removeParents:(NSSet *)values;

@end
