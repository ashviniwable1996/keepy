//
//  Album.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Album.h"


@implementation Album

@dynamic year;
@dynamic items;
@dynamic kid;

@end
