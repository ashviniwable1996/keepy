//
//  Place.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//


@implementation Place

@dynamic lat;
@dynamic lon;
@dynamic placeId;
@dynamic title;
@dynamic items;

@end
