//
//  KidView.h
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface KidView : UIView

@property(nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property(nonatomic, weak) IBOutlet UIImageView *imageView;
@property(nonatomic, weak) IBOutlet UILabel *name;

+ (CGFloat)width;

@end
