//
//  ImageCropViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 3/30/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "ImageCropViewController.h"
#import "MADrawRect.h"
#import "UIImageView+ContentFrame.h"
#import "KPOpenCV.h"
#import "ImageEditorViewController.h"
#import "UIButtonIconLabel.h"

@interface ImageCropViewController () <MADrawRectDelegate>

@property(strong, nonatomic) UIImage *sourceImage;
@property(strong, nonatomic) NSArray *points;
@property(strong, nonatomic) NSArray *originalPoints;
@property(strong, nonatomic) MADrawRect *adjustRect;
@property(strong, nonatomic) UIImageView *itemImageView;
@property(strong, nonatomic) NSDictionary *imageInfo;
@property(nonatomic) CropMode cropMode;
@property(strong, nonatomic) UIButtonIconLabel *customModeBtn;
@property(strong, nonatomic) UIButtonIconLabel *squareModeBtn;
@property(strong, nonatomic) UIButtonIconLabel *skewModeBtn;
@property(strong, nonatomic) UIButtonIconLabel *originalModeBtn;
@end

@implementation ImageCropViewController

- (instancetype)initWithImage:(UIImage *)image andPoints:(NSArray *)points {
    self = [super initWithNibName:@"ImageCropViewController" bundle:nil];
    if (self) {
        self.sourceImage = image;
        self.points = points;
    }
    return self;
}

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo {
    self = [super initWithNibName:@"ImageCropViewController" bundle:nil];
    if (self) {
        self.imageInfo = imageInfo;
        self.sourceImage = [[UIImage alloc] initWithCGImage:((UIImage *) [self.imageInfo valueForKey:UIImagePickerControllerOriginalImage]).CGImage scale:1 orientation:UIImageOrientationUp];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"photo-crop", @"source", nil]];

    self.title = KPLocalizedString(@"crop photo");
    if ([self.navigationController.viewControllers objectAtIndex:0] == self) {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"apply"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];
    } else {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"apply"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];
    }


    [self.view setBackgroundColor:UIColorFromRGB(0x5e4f4a)];
    //self.view.tintColor = UIColorFromRGB(0xd7ff46);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    /*
     self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
     self.navigationController.navigationBar.tintColor = UIColorFromRGB(0xd7ff46);
     self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
     */

}

- (void)viewDidLayoutSubviews {
    if (self.itemImageView != nil) {
        return;
    }

    self.itemImageView = [[UIImageView alloc] initWithFrame:CGRectMake(27, 94, self.view.frameWidth - 54, self.view.frameHeight - 60 - 15 - 94 - 46)];
    //CGRectMake(20, 74, self.view.bounds.size.width-40, self.view.bounds.size.height - 60 - 84)];
    [self.itemImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.itemImageView setImage:self.sourceImage];
    [self.view addSubview:self.itemImageView];

    UIView *bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 90, self.view.bounds.size.width, 90)];
    bottomBar.backgroundColor = [UIColor colorWithRed:1.000 green:0.317 blue:0.267 alpha:1.000];// [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
    [self.view addSubview:bottomBar];

    //Buttons

    //original mode
    self.originalModeBtn = [UIButtonIconLabel buttonWithType:UIButtonTypeCustom];
    self.originalModeBtn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
    [self.originalModeBtn setTitle:KPLocalizedString(@"Original") forState:UIControlStateNormal];
    [self.originalModeBtn setImage:[UIImage imageNamed:@"crop-original"] forState:UIControlStateNormal];
    [self.originalModeBtn setImage:[UIImage imageNamed:@"crop-original-on"] forState:UIControlStateSelected];
    [self.originalModeBtn setTitleColor:UIColorFromRGB(0xd7ff46) forState:UIControlStateSelected];

    [self.originalModeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.originalModeBtn setTintColor:[UIColor whiteColor]];
    self.originalModeBtn.selected = false;
    self.originalModeBtn.frame = CGRectMake(0, 20, self.view.frameWidth / 4, 100);
    [self.originalModeBtn addTarget:self action:@selector(revertToOriginal) forControlEvents:UIControlEventTouchUpInside];
    //[originalModeBtn addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.originalModeBtn.tag = kOriginalCropMode;
    [bottomBar addSubview:self.originalModeBtn];

    //Line divider
    UIView *lineDivider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 60)];
    lineDivider.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.400];
    [bottomBar addCenteredSubview:lineDivider];
    lineDivider.frameX = self.originalModeBtn.frameRight - 2;
    lineDivider.frameY += 5;

    //Custom
    self.cropMode = kCustomCropMode;
    self.customModeBtn = [UIButtonIconLabel buttonWithType:UIButtonTypeCustom];
    self.customModeBtn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
    [self.customModeBtn setTitle:KPLocalizedString(@"Custom") forState:UIControlStateNormal];
    [self.customModeBtn setImage:[UIImage imageNamed:@"crop-custom"] forState:UIControlStateNormal];
    [self.customModeBtn setImage:[UIImage imageNamed:@"crop-custom-on"] forState:UIControlStateSelected];
    [self.customModeBtn setTitleColor:UIColorFromRGB(0xd7ff46) forState:UIControlStateSelected];
    [self.customModeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.customModeBtn setTintColor:[UIColor whiteColor]];
    self.customModeBtn.selected = true;
    self.customModeBtn.tag = kCustomCropMode;
    self.customModeBtn.frame = CGRectMake(self.originalModeBtn.frameRight, 20, self.view.frameWidth / 4, 100);
    [self.customModeBtn addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:self.customModeBtn];


    //Square
    self.squareModeBtn = [UIButtonIconLabel buttonWithType:UIButtonTypeCustom];
    self.squareModeBtn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
    [self.squareModeBtn setTitle:KPLocalizedString(@"Square") forState:UIControlStateNormal];
    [self.squareModeBtn setImage:[UIImage imageNamed:@"crop-square"] forState:UIControlStateNormal];
    [self.squareModeBtn setImage:[UIImage imageNamed:@"crop-square-on"] forState:UIControlStateSelected];
    [self.squareModeBtn setTitleColor:UIColorFromRGB(0xd7ff46) forState:UIControlStateSelected];
    [self.squareModeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.squareModeBtn setTintColor:[UIColor whiteColor]];
    self.squareModeBtn.selected = false;
    self.squareModeBtn.tag = kSquareCropMode;
    self.squareModeBtn.frame = CGRectMake(self.customModeBtn.frameRight, 20, self.view.frameWidth / 4, 100);
    [self.squareModeBtn addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:self.squareModeBtn];

    //Skew
    self.skewModeBtn = [UIButtonIconLabel buttonWithType:UIButtonTypeCustom];
    self.skewModeBtn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
    [self.skewModeBtn setTitle:KPLocalizedString(@"Skew") forState:UIControlStateNormal];
    [self.skewModeBtn setImage:[UIImage imageNamed:@"crop-skew"] forState:UIControlStateNormal];
    [self.skewModeBtn setImage:[UIImage imageNamed:@"crop-skew-on"] forState:UIControlStateSelected];
    [self.skewModeBtn setTitleColor:UIColorFromRGB(0xd7ff46) forState:UIControlStateSelected];
    [self.skewModeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.skewModeBtn setTintColor:[UIColor whiteColor]];
    self.skewModeBtn.selected = false;
    self.skewModeBtn.tag = kSkewCropMode;
    self.skewModeBtn.frame = CGRectMake(self.squareModeBtn.frameRight, 20, self.view.frameWidth / 4, 100);
    [self.skewModeBtn addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:self.skewModeBtn];


    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        bottomBar.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = UIColorFromRGB(0x5E4F4A);
        self.itemImageView.backgroundColor = UIColorFromRGB(0x5E4F4A);

        /*
         UIButton *resetButton = [UIButton buttonWithType:UIButtonTypeSystem];
         resetButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
         [resetButton setTitle:KPLocalizedString(@"skip") forState:UIControlStateNormal];
         
         resetButton.frame = CGRectMake((bottomBar.frame.size.width-150)/2, (bottomBar.frame.size.height-30)/2, 150, 30);
         [resetButton addTarget:self action:@selector(resetTap:) forControlEvents:UIControlEventTouchUpInside];
         [bottomBar addSubview:resetButton];
         */
    }
    else {
        /*
         UIButton *resetButton = [UIButton buttonWithType:UIButtonTypeCustom];
         [resetButton setBackgroundImage:[[UIImage imageNamed:@"button-defaultBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 15.5, 0, 15.5)] forState:UIControlStateNormal];
         
         [resetButton setTitleColor:[UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1] forState:UIControlStateNormal];
         resetButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:12];
         [resetButton setTitle:KPLocalizedString(@"skip") forState:UIControlStateNormal];
         
         resetButton.frame = CGRectMake((bottomBar.frame.size.width-70)/2, (bottomBar.frame.size.height-30)/2, 70, 30);
         [resetButton addTarget:self action:@selector(resetTap:) forControlEvents:UIControlEventTouchUpInside];
         [bottomBar addSubview:resetButton];
         */
    }

    /*
     UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, bottomBar.frame.size.height)];
     lbl.textColor = [UIColor whiteColor];
     lbl.textAlignment = NSTextAlignmentLeft;
     lbl.backgroundColor = [UIColor clearColor];
     lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:13];
     [bottomBar addSubview:lbl];
     lbl.text = @"Drag the circles to crop";
     */

    self.adjustRect = [[MADrawRect alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.bounds.size.height - 90 / 1.5 - 80)];
    [self.view addSubview:self.adjustRect];
    self.adjustRect.delegate = self;
    // self.adjustRect.backgroundColor = [UIColor redColor];

    [GlobalUtils showTip:5 withView:nil withCompleteBlock:^{
    }];


    if (self.points == nil) {

//        if (self.navigationController.view.tag == 99998) {
//            NSDictionary *cropResultData = [KPOpenCV kpGetImageBestRect:self.sourceImage];
//            if (cropResultData == nil) {
//                //DLog(@">>>>>>> cropResultData is nil");
//                /* self.points = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0, 0)],
//                                [NSValue valueWithCGPoint:CGPointMake(0, self.sourceImage.size.height)],
//                                [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, self.sourceImage.size.height)],
//                                [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, 0)],
//                                nil];*/
//
//                self.points = [self originalImagePoints];
//            }
//            else {
//                //DLog(@">>>>>>> got points from cropResultData");
//                self.points = (NSArray *) [cropResultData valueForKey:@"points"];
//            }
//
//        } else {
//
//            // DLog(@">>>>>>> got points from imagesize");

            self.points = [self originalImagePoints];
//        }


    }


    CGPoint ptTopLeft = [[self.points objectAtIndex:0] CGPointValue];
    CGPoint ptTopRight = [[self.points objectAtIndex:3] CGPointValue];
    CGPoint ptBottomRight = [[self.points objectAtIndex:2] CGPointValue];
    CGPoint ptBottomLeft = [[self.points objectAtIndex:1] CGPointValue];

    BOOL isSkewMode = ((ptTopLeft.x != ptBottomLeft.x) || (ptTopRight.x != ptBottomRight.x) || (ptTopLeft.y != ptTopRight.y) || (ptBottomLeft.y != ptBottomRight.y));

    [self buttonTap:(isSkewMode) ? self.skewModeBtn : self.customModeBtn];

    [self showPoints];

}

- (void)viewWillAppear:(BOOL)animated {
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        // self.view.tintColor = UIColorFromRGB(0xd7ff46);
        self.navigationController.navigationBar.tintColor = UIColorFromRGB(0x2CA0C4);
        self.navigationController.navigationBar.barTintColor = nil;
        self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1], NSForegroundColorAttributeName, [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17], NSFontAttributeName, nil];

    }
}

- (NSArray *)originalImagePoints {
    return [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0, 0)],
                                     [NSValue valueWithCGPoint:CGPointMake(0, self.sourceImage.size.height)],
                                     [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, self.sourceImage.size.height)],
                                     [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, 0)],
                    nil];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}


- (void)resetCrop {

}

- (void)showPoints {
    float ratio = self.itemImageView.contentScale;
    float dx = (self.itemImageView.bounds.size.width - self.itemImageView.image.size.width * ratio) / 2;
    float dy = (self.itemImageView.bounds.size.height - self.itemImageView.image.size.height * ratio) / 2;

    dx += (self.itemImageView.frame.origin.x - self.adjustRect.frame.origin.x);
    dy += (self.itemImageView.frame.origin.y - self.adjustRect.frame.origin.y);


    if (CGRectIsEmpty(self.adjustRect.originalRect)) {
        NSArray *originalPoints = [self originalImagePoints];;
        /*
         NSDictionary *cropResultData = [KPOpenCV kpGetImageBestRect:self.sourceImage];
         if (cropResultData == nil)
         {
         originalPoints= [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0, 0)],
         [NSValue valueWithCGPoint:CGPointMake(0, self.sourceImage.size.height)],
         [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, self.sourceImage.size.height)],
         [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, 0)],
         nil];
         }
         else
         {
         originalPoints = (NSArray*)[cropResultData valueForKey:@"points"];
         }*/



        CGPoint ptTopLeft = [[originalPoints objectAtIndex:0] CGPointValue];
        CGPoint ptTopRight = [[originalPoints objectAtIndex:3] CGPointValue];
        CGPoint ptBottomRight = [[originalPoints objectAtIndex:2] CGPointValue];
        CGPoint ptBottomLeft = [[originalPoints objectAtIndex:1] CGPointValue];
        [_adjustRect topLeftCornerToCGPoint:CGPointMake(ptTopLeft.x * ratio + dx, ptTopLeft.y * ratio + dy)];
        [_adjustRect topRightCornerToCGPoint:CGPointMake(ptTopRight.x * ratio + dx, ptTopRight.y * ratio + dy)];
        [_adjustRect bottomRightCornerToCGPoint:CGPointMake(ptBottomRight.x * ratio + dx, ptBottomRight.y * ratio + dy)];
        [_adjustRect bottomLeftCornerToCGPoint:CGPointMake(ptBottomLeft.x * ratio + dx, ptBottomLeft.y * ratio + dy)];


        [self.adjustRect caculateSquareSide];

        float width = (ptTopRight.x * ratio + dx) - (ptTopLeft.x * ratio + dx);
        float height = (ptBottomLeft.y * ratio + dy) - (ptTopRight.y * ratio + dy);
        float x = ptTopLeft.x * ratio + dx;
        float y = ptTopLeft.y * ratio + dy;

        self.adjustRect.originalRect = CGRectMake(x, y, width, height);
        [self.adjustRect updateOriginalRect];
        //         self.adjustRect.frame = self.adjustRect.originalRect;

    }


    CGPoint ptTopLeft = [[self.points objectAtIndex:0] CGPointValue];
    CGPoint ptTopRight = [[self.points objectAtIndex:3] CGPointValue];
    CGPoint ptBottomRight = [[self.points objectAtIndex:2] CGPointValue];
    CGPoint ptBottomLeft = [[self.points objectAtIndex:1] CGPointValue];
    [_adjustRect topLeftCornerToCGPoint:CGPointMake(ptTopLeft.x * ratio + dx, ptTopLeft.y * ratio + dy)];
    [_adjustRect topRightCornerToCGPoint:CGPointMake(ptTopRight.x * ratio + dx, ptTopRight.y * ratio + dy)];
    [_adjustRect bottomRightCornerToCGPoint:CGPointMake(ptBottomRight.x * ratio + dx, ptBottomRight.y * ratio + dy)];
    [_adjustRect bottomLeftCornerToCGPoint:CGPointMake(ptBottomLeft.x * ratio + dx, ptBottomLeft.y * ratio + dy)];



    // DLog(@"original rect: %f,%f - %f x %f", x, y, width, height);
}

- (NSArray *)getNewPoints {
    CGFloat scaleFactor = 1;//  [self.itemImageView contentScale];

    float tx = (self.itemImageView.frame.origin.x - self.adjustRect.frame.origin.x);
    float ty = (self.itemImageView.frame.origin.y - self.adjustRect.frame.origin.y);

    CGPoint ptBottomLeft = [_adjustRect coordinatesForPoint:1 withScaleFactor:scaleFactor];
    CGPoint ptBottomRight = [_adjustRect coordinatesForPoint:2 withScaleFactor:scaleFactor];
    CGPoint ptTopRight = [_adjustRect coordinatesForPoint:3 withScaleFactor:scaleFactor];
    CGPoint ptTopLeft = [_adjustRect coordinatesForPoint:4 withScaleFactor:scaleFactor];

    scaleFactor = [self.itemImageView contentScale];
    ptBottomLeft = CGPointMake((ptBottomLeft.x - tx) / scaleFactor, (ptBottomLeft.y - ty) / scaleFactor);
    ptBottomRight = CGPointMake((ptBottomRight.x - tx) / scaleFactor, (ptBottomRight.y - ty) / scaleFactor);
    ptTopRight = CGPointMake((ptTopRight.x - tx) / scaleFactor, (ptTopRight.y - ty) / scaleFactor);
    ptTopLeft = CGPointMake((ptTopLeft.x - tx) / scaleFactor, (ptTopLeft.y - ty) / scaleFactor);

    float dx = (self.itemImageView.bounds.size.width - self.itemImageView.image.size.width * scaleFactor) / 2 / scaleFactor;
    float dy = (self.itemImageView.bounds.size.height - self.itemImageView.image.size.height * scaleFactor) / 2 / scaleFactor;

    ptBottomLeft = CGPointMake(ptBottomLeft.x - dx, ptBottomLeft.y - dy);
    ptBottomRight = CGPointMake(ptBottomRight.x - dx, ptBottomRight.y - dy);
    ptTopRight = CGPointMake(ptTopRight.x - dx, ptTopRight.y - dy);
    ptTopLeft = CGPointMake(ptTopLeft.x - dx, ptTopLeft.y - dy);

    return [NSArray arrayWithObjects:
            [NSValue valueWithCGPoint:ptTopLeft],
            [NSValue valueWithCGPoint:ptBottomLeft],
            [NSValue valueWithCGPoint:ptBottomRight],
            [NSValue valueWithCGPoint:ptTopRight], nil];

}

#pragma mark - Actions

- (void)topBtnTap:(UIButton *)sender {
    if (sender.tag == 1) //Ok
    {
        NSArray *arr;
        if (self.cropMode == kOriginalCropMode) {
            arr = self.points;
        } else {

            arr = [self getNewPoints];
            BOOL pointsChanged = NO;

            if (self.points == nil || [self.points count] == 0) {
                pointsChanged = YES;
            }
            else {
                for (int i = 0; i < [arr count]; i++) {
                    CGPoint p1 = [[arr objectAtIndex:i] CGPointValue];
                    CGPoint p2 = [[self.points objectAtIndex:i] CGPointValue];
                    if (floor(p1.x) != floor(p2.x) || floor(p1.y) != floor(p2.y)) {
                        pointsChanged = YES;
                        break;
                    }
                }
            }
        }

        //[[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"photo-crop", @"source", @"ok", @"result", nil]];

        if (self.imageInfo != nil) {
            [SVProgressHUD showWithStatus:KPLocalizedString(@"cropping")];
            ImageEditorViewController *avc = [[ImageEditorViewController alloc] initWithImageInfo:self.imageInfo andPoints:arr];
            [self.navigationController pushViewController:avc animated:YES];
        }
        else {
            [self.delegate cropRectSelected:arr withImage:self.sourceImage];
            [self.navigationController popViewControllerAnimated:YES];

        }
    }
    else {
        //[[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"photo-crop", @"source", @"cancel", @"result", nil]];


        [[Mixpanel sharedInstance] track:@"Tap back on crop screen"];


        if (self.navigationController.topViewController == self) {
            [self.navigationController dismissViewControllerAnimated:YES completion:^{

            }];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }

}

- (void)buttonTap:(UIButton *)sender {
    //      DLog(@">>>>>>>>>>>> buttonTap");
    self.adjustRect.hidden = false;

    self.customModeBtn.selected = self.squareModeBtn.selected = self.skewModeBtn.selected = false;
    sender.selected = true;

    self.cropMode = (CropMode) sender.tag;

    [self.adjustRect changeCropMode:self.cropMode];
}

- (void)revertToOriginal {

    // DLog(@">>>>>>>>>>>> revertToOriginal");

    self.points = [self originalImagePoints];
    self.cropMode = kOriginalCropMode;
    self.customModeBtn.selected = self.squareModeBtn.selected = self.skewModeBtn.selected = false;
    self.originalModeBtn.selected = true;

    self.adjustRect.hidden = true;


    //[self buttonTap:self.customModeBtn];
    //[self showPoints];
}

- (void)resetTap:(UIButton *)sender {

    if (self.imageInfo != nil) {
        ImageEditorViewController *avc = [[ImageEditorViewController alloc] initWithImageInfo:self.imageInfo andPoints:nil];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
        [self.delegate cropRectSelected:nil withImage:self.sourceImage];
        /*
         self.points = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0, 0)],
         [NSValue valueWithCGPoint:CGPointMake(0, self.sourceImage.size.height)],
         [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, self.sourceImage.size.height)],
         [NSValue valueWithCGPoint:CGPointMake(self.sourceImage.size.width, 0)],
         nil];
         [self showPoints];
         */
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MADrawRectDelegate

- (void)pointTouchUp:(NSInteger)pointIndex {
    return;

    CGFloat scaleFactor = 1;

    float tx = (self.itemImageView.frame.origin.x - self.adjustRect.frame.origin.x);
    float ty = (self.itemImageView.frame.origin.y - self.adjustRect.frame.origin.y);

    CGPoint apoint = [_adjustRect coordinatesForPoint:pointIndex withScaleFactor:scaleFactor];

    scaleFactor = [self.itemImageView contentScale];
    apoint = CGPointMake((apoint.x - tx) / scaleFactor, (apoint.y - ty) / scaleFactor);

    float dx = (self.itemImageView.bounds.size.width - self.itemImageView.image.size.width * scaleFactor) / 2 / scaleFactor;
    float dy = (self.itemImageView.bounds.size.height - self.itemImageView.image.size.height * scaleFactor) / 2 / scaleFactor;

    apoint = CGPointMake(apoint.x - dx, apoint.y - dy);

    CGPoint newPoint = [KPOpenCV findCorner:self.sourceImage atPoint:apoint];


    dx += (self.itemImageView.frame.origin.x - self.adjustRect.frame.origin.x);
    dy += (self.itemImageView.frame.origin.y - self.adjustRect.frame.origin.y);

    newPoint = CGPointMake(newPoint.x * scaleFactor + dx, newPoint.y * scaleFactor + dy);

    switch (pointIndex) {
        case 1:
            [_adjustRect bottomLeftCornerToCGPoint:newPoint];
            break;
        case 2:
            [_adjustRect bottomRightCornerToCGPoint:newPoint];
            break;
        case 3:
            [_adjustRect topRightCornerToCGPoint:newPoint];
            break;
        case 4:
            [_adjustRect topLeftCornerToCGPoint:newPoint];
            break;
    }

}

@end
