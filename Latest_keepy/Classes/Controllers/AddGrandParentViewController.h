//
//  AddGrandParentViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    AddGrandParentViewControllerTypeGrandma = 2,
    AddGrandParentViewControllerTypeGrandpa = 3
} AddGrandParentViewControllerType;

@interface AddGrandParentViewController : UIViewController

@property(nonatomic, assign) AddGrandParentViewControllerType addGrandParentViewControllerType;
@property(nonatomic, strong) NSArray *family;

- (CGFloat)height;

@end
