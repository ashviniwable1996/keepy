//
//  AddGrandParentsViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol AddGrandParentsViewControllerDelegate;

@interface AddGrandParentsViewController : UIViewController

@property(nonatomic, weak) id <AddGrandParentsViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *kids;
@property(nonatomic, strong) NSArray *family;

- (CGFloat)height;

@end

@protocol AddGrandParentsViewControllerDelegate <NSObject>

- (void)doneAddingGrandParents:(NSArray *)family;

@end
