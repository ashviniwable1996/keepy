//
//  NoteEditorViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 8/4/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "KPLocalAsset.h"
#import "KPLocalItem.h"

#import "NoteEditorViewController.h"
#import "KPDataParser.h"
#import "ItemEditViewController.h"

#import "Keepy-Swift.h"

@interface NoteEditorViewController () <UITextViewDelegate>

@property(nonatomic, strong) UITextView *noteTF;
@property(nonatomic, strong) NSString *kidName;
@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic) NSInteger mode;
@property(nonatomic, strong) UILabel *placeHolderLabel;
@property(nonatomic, strong) ItemEditViewController *itemEditVC;

@end

@implementation NoteEditorViewController

- (instancetype)initWithKidName:(NSString *)kidName {
    self = [super initWithNibName:@"NoteEditorViewController" bundle:nil];
    if (self) {
        self.kidName = kidName;
    }

    return self;
}

- (instancetype)initWithItem:(KPLocalItem *)item {
    self = [super initWithNibName:@"NoteEditorViewController" bundle:nil];
    if (self) {
        self.item = item;
        if (self.item.itemType == 2) {
            self.mode = 1;
        }
    }

    return self;
}

- (instancetype)initWithMode:(NSInteger)mode {
    self = [super initWithNibName:@"NoteEditorViewController" bundle:nil];
    if (self) {
        self.mode = mode;
    }

    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //[self.navigationController setNavigationBarHidden:YES];

    self.title = KPLocalizedString(@"add note");
    if (self.item != nil) {
        self.title = KPLocalizedString(@"edit note");
    }

    if (self.mode == 1) {
        if (self.item == nil) {
            [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-quote", @"source", nil]];
        } else {
            [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-quote", @"source", nil]];
        }
    }
    else {
        if (self.item == nil) {
            [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-note", @"source", nil]];
        } else {
            [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-note", @"source", nil]];
        }
    }

    if (self.mode == 1) {
        if (self.item != nil) {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];

        }
        else {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"next"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];
        }
    }
    else {
        if (self.navigationController) {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];
        } else {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];
        }

    }

   
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    float dy = 0;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        dy = 64;
    }
    
    UIImageView *bkgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, dy + 15, self.view.frame.size.width - 60, 160)];
    if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
       bkgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, dy + 38, self.view.frame.size.width - 60, 160)];
    }
//    else {
//        bkgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, dy + 15, self.view.frame.size.width - 60, 160)];
//    }
    
    [bkgImageView.layer setCornerRadius:15.0];
    bkgImageView.backgroundColor = UIColorFromRGB(0x3EBCD2);
    if (self.mode == 1) {
        bkgImageView.backgroundColor = UIColorFromRGB(0xffffff);
    }
    [bkgImageView.layer setShadowOffset:CGSizeMake(1, 1)];
    [bkgImageView.layer setShadowRadius:100.0];
    [bkgImageView.layer setShadowColor:[[UIColor redColor] CGColor]];
    [self.view addSubview:bkgImageView];
    
   // self.noteTF = [[UITextView alloc] initWithFrame:CGRectMake(35, dy + 20, self.view.frame.size.width - 70, 150)];
    if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
        self.noteTF = [[UITextView alloc] initWithFrame:CGRectMake(35, dy + 43, self.view.frame.size.width - 70, 150)];
    } else {
        self.noteTF = [[UITextView alloc] initWithFrame:CGRectMake(35, dy + 20, self.view.frame.size.width - 70, 150)];
    }
    
    self.noteTF.textColor = [UIColor whiteColor];
    self.noteTF.backgroundColor = [UIColor clearColor];
    if (self.mode == 0) {
        self.noteTF.textAlignment = NSTextAlignmentCenter;
        self.noteTF.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:18];
    }
    else {
        self.noteTF.textColor = UIColorFromRGB(0x523c37);
        self.noteTF.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16];
    }
    
    if (self.item != nil) {
        self.noteTF.text = self.item.extraData;
    } else if (self.mode == 0) {
        self.noteTF.text = [NSString stringWithFormat:KPLocalizedString(@"dear %@,\nthese are your keepies - special things that you made, collected or experienced. enjoy them now, later and forever!"), self.kidName];
    }
    
    self.noteTF.delegate = self;
    [self.view addSubview:self.noteTF];
    
    if (self.mode == 1) {
        self.placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.noteTF.frame.origin.x + 10, self.noteTF.frame.origin.y + 7, self.noteTF.frame.size.width - 20, self.noteTF.frame.size.height - 20)];
        self.placeHolderLabel.font = self.noteTF.font;
        self.placeHolderLabel.alpha = 0.2;
        self.placeHolderLabel.numberOfLines = 10;
        self.placeHolderLabel.backgroundColor = [UIColor clearColor];
        self.placeHolderLabel.textColor = self.noteTF.textColor;
        self.placeHolderLabel.text = KPLocalizedString(@"Jot down a quote, a love note, a memory, a moment or anything else you want to keep");
        self.placeHolderLabel.hidden = (self.item != nil);
        CGRect frame = self.placeHolderLabel.frame;
        frame.size.height = [self.placeHolderLabel.text sizeWithFont:self.placeHolderLabel.font thatFitsToSize:CGSizeMake(self.placeHolderLabel.frame.size.width, self.placeHolderLabel.frame.size.height) lineBreakMode:NSLineBreakByWordWrapping].height;
        self.placeHolderLabel.frame = frame;
        [self.view addSubview:self.placeHolderLabel];
    }
    
    if (self.mode == 0) {
        UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [clearButton setImage:[UIImage imageNamed:@"clear-field-button"] forState:UIControlStateNormal];
        clearButton.frame = CGRectMake(250, dy + 20, 33, 33);
        [clearButton addTarget:self action:@selector(clearTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:clearButton];
    }
    
    [self.noteTF becomeFirstResponder];
}
- (void)topBtnTap:(UIButton *)sender {

    if (sender.tag == 1) //Ok
    {
        float fontSize = 36.0;
        float ah = 600;
        float ch = 550;
        float ih = 50;
        UIColor *bkgColor = UIColorFromRGB(0x3EBCD2);
        UIColor *fontColor = UIColorFromRGB(0xffffff);
        NSTextAlignment ta = NSTextAlignmentCenter;
        if (self.mode == 1) {
            fontSize = 32.0;
            ch = 20000;
            bkgColor = UIColorFromRGB(0xffffff);
            fontColor = UIColorFromRGB(0x523c37);
            ta = NSTextAlignmentLeft;
        }

        CGSize tsize = [self.noteTF.text sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:fontSize] thatFitsToSize:CGSizeMake(520, ch) lineBreakMode:NSLineBreakByWordWrapping];

        if (self.mode == 1) {
            ah = tsize.height + ih + 20 + 10;
            if (ah < 600) {
                ah = 600;
            }
        }


        UIGraphicsBeginImageContextWithOptions(CGSizeMake(600, ah), NO, 1.0);

        UIBezierPath *p = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 600, ah)
                                                     cornerRadius:0.0];
        
//        if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
//           p = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 640, ah)                                                         cornerRadius:0.0];
//        } else {
//            p = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 600, ah)
//                                                         cornerRadius:0.0];
//        }
        
        [p addClip];

        [bkgColor setFill];

        [p fill];

        float ay = (ah - 10 - (tsize.height + ih + 20)) / 2;

        if (self.mode == 0) {
            UIImage *heartImg = [UIImage imageNamed:@"note-heart-icon"];
            [heartImg drawInRect:CGRectMake((600 - 52) / 2, ay, 52, 44)];
        }
        else {
            UIImage *quoteImg = [UIImage imageNamed:@"quote-icon"];
            [quoteImg drawInRect:CGRectMake((600 - 32) / 2, ay, 32, 24)];
        }

        [fontColor set];

        NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = ta;
        NSDictionary* attrs = @{ NSFontAttributeName: [UIFont fontWithName:@"ARSMaquettePro-Regular" size:fontSize], NSParagraphStyleAttributeName: paragraphStyle };
        [self.noteTF.text drawInRect:CGRectMake(40, ay + ih + 20, 520, ah - 10 - ay) withAttributes: attrs];
//        if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
//            [self.noteTF.text drawInRect:CGRectMake(40, ay + ih + 40, 520, ah - 10 - ay) withAttributes: attrs];
//        } else {
//            [self.noteTF.text drawInRect:CGRectMake(40, ay + ih + 20, 520, ah - 10 - ay) withAttributes: attrs];
//        }

        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        /*
        NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], [NSString stringWithFormat:@"test.jpg"]];
        NSData* imageData = UIImageJPEGRepresentation(img, 0.8);
        [imageData writeToFile:tmpPath atomically:NO];
        */
        [self uploadImage:img];
        //[self dismissModalViewControllerAnimated:YES];
    }
    else {
        if (self.mode == 1) {
            if (self.item == nil) {
                [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-quote", @"source", @"cancel", @"result", nil]];
            } else {
                [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-quote", @"source", @"cancel", @"result", nil]];
            }
        }
        else {
            if (self.item == nil) {
                [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-note", @"source", @"cancel", @"result", nil]];
            } else {
                [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-note", @"source", @"cancel", @"result", nil]];
            }
        }
        if ((self.mode == 1 && self.item == nil) || !self.navigationController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clearTap:(id)sender {
    self.noteTF.text = @"";
    self.rightButtonEnabled = NO;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)string {
    if (self.mode == 1) {
        return true;
    }

    NSString *temp = [textView.text stringByReplacingCharactersInRange:range withString:string];
    CGSize size = [temp sizeWithFont:textView.font thatFitsToSize:CGSizeMake(textView.frame.size.width, 999) lineBreakMode:NSLineBreakByWordWrapping];
    int numLines = size.height / textView.font.lineHeight;
    if (numLines <= 9) {
        return true;
    }
    return false;
}

- (void)textViewDidChange:(UITextView *)textView {
    self.rightButtonEnabled = (textView.text.length > 0);
    self.placeHolderLabel.hidden = ([textView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    self.placeHolderLabel.hidden = ([textView.text length] > 0);
}

- (void)createImage {

}

- (void)uploadImage:(UIImage *)image {
    [SVProgressHUD showWithStatus:KPLocalizedString(@"preparing")];
    [[ServerComm instance] prepareAsset:^(id result) {
        int assetId = 0;
        if ([[result valueForKey:@"result"] valueForKey:@"id"] != [NSNull null]) {
            assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] intValue];
        }

        if (assetId > 0) {
            //[SVProgressHUD dismiss];

            if (self.mode == 1) {
                if (self.item == nil) {
                    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-quote", @"source", @"ok", @"result", nil]];
                } else {
                    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-quote", @"source", @"ok", @"result", nil]];
                }
            }
            else {
                if (self.item == nil) {
                    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-note", @"source", @"ok", @"result", nil]];
                } else {
                    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-note", @"source", @"ok", @"result", nil]];
                }
            }

            if (self.mode == 1) {
                [SVProgressHUD dismiss];
                NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
                [result setValue:image forKey:@"resultImage"];
                [result setValue:@(assetId) forKey:@"assetId"];
                [result setValue:@(2) forKey:@"itemType"];
                [result setValue:self.noteTF.text forKey:@"extraData"];

                if (self.item != nil) {
                    [self.delegate noteEditorFinished:self.noteTF.text withNoteImage:image andAssetId:assetId];
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else {
                    if (self.itemEditVC == nil) {
                        self.itemEditVC = [[ItemEditViewController alloc] initWithImageInfo:result];
                    } else {
                        [self.itemEditVC refreshImageInfo:result];
                    }

                    [self.navigationController pushViewController:self.itemEditVC animated:YES];
                }
            }
            else {
                if (self.item != nil) {
                    [self updateItem:image withAssetId:assetId];
                } else if (self.delegate) {
                    [self.delegate noteEditorFinished:self.noteTF.text withNoteImage:image andAssetId:assetId];
                }
            }

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                NSData *data = UIImageJPEGRepresentation(image, 0.8);

                [[ServerComm instance] uploadAsset:[[result valueForKey:@"result"] valueForKey:@"hash"] withData:data andPolicy:[[result valueForKey:@"result"] valueForKey:@"policy"] andSignature:[[result valueForKey:@"result"] valueForKey:@"policyHash"] withSuccessBlock:^(id result) {
                    [[ServerComm instance] commitAsset:assetId andFileSize:data.length withSuccessBlock:^(id result) {
                        DDLogInfo(@"Uploaded...");
                    }                     andFailBlock:^(NSError *error) {

                    }                        onHandler:nil];

                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                }                     andFailBlock:^(NSError *error) {
                    [GlobalUtils updateAssetUpload:assetId totalBytesWritten:0 totalBytesExpectedToWrite:0];

                    if (error != nil) {
                        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading original image")];
                    }

                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                }                       onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                    //float p = ((float)totalBytesWritten / (float)totalBytesExpectedToWrite);
                    [GlobalUtils updateAssetUpload:assetId totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
                    //DDLogInfo(@"uploading:%2f", 100 * p);

                }];
            });
        }
        else {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];
        }
    }                      andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];

    }];
}

- (void)updateItem:(UIImage *)image withAssetId:(int)assetId {
    [SVProgressHUD showWithStatus:KPLocalizedString(@"updating")];

    KPLocalAsset *itemOriginalImage = [[KPDataParser instance] createAssetById:assetId withType:1 withHash:nil];
    self.item.originalImageAssetID = itemOriginalImage.identifier;
    
    // self.item.originalImage = [[KPDataParser instance] createAssetById:assetId withType:1 withHash:nil];
    self.item.extraData = self.noteTF.text;
    [[ServerComm instance] updateItem:self.item imageChanged:YES withTags:nil andExtraParams:nil withSuccessBlock:^(id result) {

        if ([[result valueForKey:@"status"] intValue] == 0) {

            [[Analytics sharedAnalytics] track:@"item-update" properties:[NSDictionary dictionaryWithObjectsAndKeys:(self.item.itemType == 1) ? @"note" : @"quote", @"itemType", nil]];

            int newAssetId = [[[result valueForKey:@"result"] valueForKey:@"assetId"] intValue];
            
            KPLocalAsset *itemImage = [[KPDataParser instance] createAssetById:newAssetId withType:0 withHash:[[result valueForKey:@"result"] valueForKey:@"assetHash"]];
            self.item.imageAssetID = itemImage.identifier;


            //Save local files with assets ids

            [[SDImageCache sharedImageCache] storeImage:image forKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

            [[SDImageCache sharedImageCache] storeImage:image forKey:[NSString stringWithFormat:@"%@%@_iphone.jpg", Defaults.assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];


            //save thumbnails
            UIImage *smallImage = [image imageScaledToFitSize:CGSizeMake(192, 192)];

            [[SDImageCache sharedImageCache] storeImage:smallImage forKey:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

            UIImage *mediumImage = [image imageScaledToFitSize:CGSizeMake(600, 600)];
            [[SDImageCache sharedImageCache] storeImage:mediumImage forKey:[NSString stringWithFormat:@"%@%@_medium.jpg", Defaults.assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

            [self.item saveChanges];
            

            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

            [SVProgressHUD dismiss];

            if (self.delegate) {
                [self.delegate noteEditorFinished:self.noteTF.text withNoteImage:image andAssetId:assetId];
            }

            if (self.navigationController) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];
        }

    }                    andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];
    }];
}

@end
