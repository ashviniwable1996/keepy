//
//  AddParentsViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddParentsViewController.h"
#import "AddParentViewController.h"
#import "KidsCell.h"
#import "FamilySelectorCell.h"
#import "ProgressCell.h"

@interface AddParentsViewController () <UITableViewDataSource, UITableViewDelegate, FamilySelectorCellDelegate, ProgressCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AddParentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"onboarding-add-parents-open"];

    [self.tableView registerNib:[UINib nibWithNibName:@"KidsCell" bundle:nil] forCellReuseIdentifier:@"KidsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilyCell" bundle:nil] forCellReuseIdentifier:@"FamilyCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilySelectorCell" bundle:nil] forCellReuseIdentifier:@"FamilySelectorCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProgressCell" bundle:nil] forCellReuseIdentifier:@"ProgressCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        static NSString *KidsCellIdentifier = @"KidsCell";
        KidsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KidsCellIdentifier];
        cell.kids = self.kids;
        [cell layout];
        return cell;
    } else if (indexPath.row == 1) {
        static NSString *FamilySelectorCellIdentifier = @"FamilySelectorCell";
        FamilySelectorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FamilySelectorCellIdentifier];
        cell.delegate = self;
        cell.familySelectorCellType = FamilySelectorCellTypeParents;
        cell.title.text = [self title];
        [cell layout];
        return cell;
    } else if (indexPath.row == 2) {
        static NSString *ProgressCellIdentifier = @"ProgressCell";
        ProgressCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ProgressCellIdentifier];
        cell.delegate = self;
        cell.progressCellState = ProgressCellStateOne;
        [cell layout];
        return cell;
    }
    return nil;
}

- (NSString *)title {
    switch (self.kids.count) {
        case 1: {
            NSDictionary *kid = [self.kids objectAtIndex:0];
            return [NSString stringWithFormat:@"Add %@'s other mom or dad so they can add memories too", [kid objectForKey:@"name"]];
        }

        case 2: {
            NSDictionary *kid1 = [self.kids objectAtIndex:0];
            NSDictionary *kid2 = [self.kids objectAtIndex:1];
            return [NSString stringWithFormat:@"Add %@ & %@'s other mom or dad so they can add memories too", [kid1 objectForKey:@"name"], [kid2 objectForKey:@"name"]];
        }

        case 3: {
            NSDictionary *kid1 = [self.kids objectAtIndex:0];
            NSDictionary *kid2 = [self.kids objectAtIndex:1];
            NSDictionary *kid3 = [self.kids objectAtIndex:2];
            return [NSString stringWithFormat:@"Add %@, %@ & %@'s other mom or dad so they can add memories too", [kid1 objectForKey:@"name"], [kid2 objectForKey:@"name"], [kid3 objectForKey:@"name"]];
        }

        default: {
            return @"Add your kids' other mom or dad so they can add memories too";
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [KidsCell height];
    } else if (indexPath.row == 1) {
        return [FamilySelectorCell height:[self title]];
    } else if (indexPath.row == 2) {
        return [ProgressCell height];
    }
    return 0.0f;
}

- (CGFloat)height {
    return [KidsCell height] + [FamilySelectorCell height:[self title]] + [ProgressCell height];
}

- (IBAction)addParentsExit:(UIStoryboardSegue *)segue {
    AddParentViewController *addParentViewController = (AddParentViewController *) segue.sourceViewController;
    if (addParentViewController.family) {
        [self.delegate doneAddingParents:addParentViewController.family];
    }
}

- (void)leftSelected:(id)sender {
    [[Mixpanel sharedInstance] track:@"onboarding-add-parents-add-mom-tap"];

    [self performSegueWithIdentifier:@"AddMom" sender:self];
}

- (void)rightSelected:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-parents-add-dad-tap"];

    [self performSegueWithIdentifier:@"AddDad" sender:self];
}

- (void)skip:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-parents-skip-tap"];

    [self.delegate doneAddingParents:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddMom"]) {
        AddParentViewController *addParentViewController = (AddParentViewController *) segue.destinationViewController;
        addParentViewController.addParentViewControllerType = AddParentViewControllerTypeMom;
    } else if ([segue.identifier isEqualToString:@"AddDad"]) {
        AddParentViewController *addParentViewController = (AddParentViewController *) segue.destinationViewController;
        addParentViewController.addParentViewControllerType = AddParentViewControllerTypeDad;
    }
}

@end
