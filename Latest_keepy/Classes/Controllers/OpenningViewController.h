//
//  OpenningViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 5/10/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenningViewController : UIViewController

- (instancetype)initWithMode:(int)mode;

@end
