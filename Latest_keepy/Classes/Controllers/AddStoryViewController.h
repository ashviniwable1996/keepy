//
//  AddStoryViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface AddStoryViewController : UIViewController

@end

@interface InfoCell : UITableViewCell

+ (CGFloat)height;

@end