//
//  OnBoardingScreenView.h
//  Keepy
//
//  Created by Joao Barbosa on 05/08/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#ifndef Keepy_OnBoardingScreenView_h
#define Keepy_OnBoardingScreenView_h

@interface OnBoardingScreenView : UIView

- (void) initScreenWithAction:(NSString *) action description:(NSString*) description andImage:(UIImage*) image;

+ (OnBoardingScreenView*) myView;
@end

#endif
