//
//  ItemShareViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 7/2/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ItemShareViewController.h"
//#import <Pinterest/Pinterest.h>
#import "Tag.h"
#import "DKMultyUploaderManger.h"
#import "DKVideoUploaderManager.h"
#import "KPLocalAsset.h"
#import "KPLocalItem.h"
#import "KPLocalKidItem.h"
#import "KPLocalKid.h"
#import "KPLocalItemTag.h"
#import "KPLocalTag.h"
#import "ItemsViewController.h"

#import "Keepy-Swift.h"

#define kTwitterSharingMaxTextSize 117 // 23 symbols are reserved for the attached picture url

@interface ItemShareViewController () <UIActionSheetDelegate>

//@property (nonatomic, strong) Pinterest *pinterest;
@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic, strong) UIButton *fbButton;
@property(nonatomic, strong) UIButton *twitterButton;
@property(nonatomic, strong) UIButton *pinterestButton;
@property(nonatomic, strong) UIButton *keepyfbButton;
@property(nonatomic, strong) UITextView *captionTV;
@property(nonatomic, strong) UIScrollView *scrollView;

@property(nonatomic, strong) ACAccountStore *accountStore;
@property(nonatomic, strong) NSArray *accounts;

@property(nonatomic) BOOL isAfterAddItem;


@end

@implementation ItemShareViewController


- (instancetype)initWithItem:(KPLocalItem *)item {
    return [self initWithItem:(id)item isAfterAddItem:NO];
}

- (instancetype)initWithItem:(KPLocalItem *)item isAfterAddItem:(BOOL)isAfterAddItem {
    self = [super initWithNibName:@"ItemShareViewController" bundle:nil];
    if (self != nil) {
        self.item = item;
        self.accountStore = [[ACAccountStore alloc] init];
        self.isAfterAddItem = isAfterAddItem;

    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = KPLocalizedString(@"share");

    [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source" : @"item-share"}];

    if (self.isAfterAddItem) {
        [self setButtons:@{@"title" : KPLocalizedString(@"close"), @"buttonType" : @(BUTTON_TYPE_NORMAL)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"done"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(NO)}];
    } else if (self.navigationController != nil && self.navigationController.view.tag != 1) {
        [self setButtons:@{@"title" : KPLocalizedString(@"back"), @"buttonType" : @(BUTTON_TYPE_BACK)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"share"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(YES)}];
    } else {
        [self setButtons:@{@"title" : KPLocalizedString(@"close"), @"buttonType" : @(BUTTON_TYPE_NORMAL)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"share"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(YES)}];
    }

    //self.pinterest = [[Pinterest alloc] initWithClientId:@"1432237"];

    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.scrollView.contentSize = self.view.frame.size;
    [self.view insertSubview:self.scrollView atIndex:0];

    float ay = 20;

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((320 - 100) / 2, ay, 100, 100)];
    //if (self.isAfterAddItem)
    //    imgView.image = [[[GlobalUtils getCurrentItemData] valueForKey:@"imageInfo"] valueForKey:@"resultImage"];
    //else

    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];

    // TODO: fix this!
    [imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, itemImage.urlHash]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    [self.scrollView addSubview:imgView];

    ay += 125;
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake((320 - 120) / 2, ay, 120, 20)];
    if (self.isAfterAddItem) {
        lbl.text = [NSString stringWithFormat:@"%@:", KPLocalizedString(@"share also on")];
    } else {
        lbl.text = [NSString stringWithFormat:@"%@:", KPLocalizedString(@"share on")];
    }

    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    lbl.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
    lbl.textAlignment = NSTextAlignmentCenter;
    [self.scrollView addSubview:lbl];

    ay += 10;

    float ax = 40;
    BOOL isMyKid = YES;

    if (!self.isAfterAddItem) {
        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];
        KPLocalKid *firstKid = itemKids[0];

        // isMyKid = [[UserManager sharedInstance] isMyKid:itemKids[0]];
        isMyKid = firstKid.isMine;
    }

    if (!isMyKid) {
        ax = 90;
    }

    //facebook
    self.fbButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.fbButton.frame = CGRectMake(ax, ay, 60, 90);
    [self.fbButton setImage:[UIImage imageNamed:@"facebook-off"] forState:UIControlStateNormal];
    [self.fbButton setImage:[UIImage imageNamed:@"facebook-on"] forState:UIControlStateSelected];
    [self.fbButton addTarget:self action:@selector(fbButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.fbButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    [self.fbButton setTitleColor:[UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1] forState:UIControlStateNormal];
    [self.fbButton setTitle:@"facebook" forState:UIControlStateNormal];
    self.fbButton.titleEdgeInsets = UIEdgeInsetsMake(70, -53, 0, 0);
    self.fbButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.scrollView addSubview:self.fbButton];
    //self.fbButton.selected = [[UserManager sharedInstance] facebookShare];

    //twitter
    self.twitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.twitterButton.frame = CGRectMake(ax + 90, ay, 60, 90);
    [self.twitterButton setImage:[UIImage imageNamed:@"twitter-off"] forState:UIControlStateNormal];
    [self.twitterButton setImage:[UIImage imageNamed:@"twitter-on"] forState:UIControlStateSelected];
    [self.twitterButton addTarget:self action:@selector(twitterButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.twitterButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    [self.twitterButton setTitleColor:[UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1] forState:UIControlStateNormal];
    [self.twitterButton setTitle:@"twitter" forState:UIControlStateNormal];
    self.twitterButton.titleEdgeInsets = UIEdgeInsetsMake(70, -53, 0, 0);
    self.twitterButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.scrollView addSubview:self.twitterButton];
    //self.twitterButton.selected = [[UserManager sharedInstance] twitterShare];

    //keepy facebook page
    self.keepyfbButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.keepyfbButton.frame = CGRectMake(ax + 180, ay, 100, 90);
    [self.keepyfbButton setImage:[UIImage imageNamed:@"keepy-off"] forState:UIControlStateNormal];
    [self.keepyfbButton setImage:[UIImage imageNamed:@"keepy-community"] forState:UIControlStateSelected];
    [self.keepyfbButton addTarget:self action:@selector(keepyFBButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.keepyfbButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    self.keepyfbButton.titleLabel.numberOfLines = 2;
    self.keepyfbButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.keepyfbButton setTitleColor:[UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1] forState:UIControlStateNormal];
    [self.keepyfbButton setTitle:KPLocalizedString(@"keepy\ncommunity") forState:UIControlStateNormal];
    self.keepyfbButton.titleEdgeInsets = UIEdgeInsetsMake(80, -93, 0, 0);
    self.keepyfbButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.keepyfbButton.hidden = (!isMyKid);
    [self.scrollView addSubview:self.keepyfbButton];
    //self.keepyfbButton.selected = [[UserManager sharedInstance] keepyFBShare];    

    /*
    //pinterest
    self.pinterestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.pinterestButton.frame = CGRectMake(170, ay + 120, 60, 90);
    [self.pinterestButton setImage:[UIImage imageNamed:@"pinterest-off"] forState:UIControlStateNormal];
    [self.pinterestButton setImage:[UIImage imageNamed:@"pinterest-on"] forState:UIControlStateSelected];
    [self.pinterestButton addTarget:self action:@selector(pinterestButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.pinterestButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:12];
    [self.pinterestButton setTitleColor:[UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1] forState:UIControlStateNormal];
    [self.pinterestButton setTitle:@"pinterest" forState:UIControlStateNormal];
    self.pinterestButton.titleEdgeInsets = UIEdgeInsetsMake(70, -53, 0, 0);
    self.pinterestButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.scrollView addSubview:self.pinterestButton];
    self.pinterestButton.selected = [[UserManager sharedInstance] pinterestShare];
    */

    ay += 120;

    imgView = [[UIImageView alloc] initWithFrame:CGRectMake((320 - 259) / 2, ay, 259, 109)];
    imgView.image = [UIImage imageNamed:@"message-field"];
    [self.scrollView addSubview:imgView];

    self.captionTV = [[UITextView alloc] initWithFrame:CGRectMake((320 - 250) / 2, ay + 4.5, 250, 100)];
    self.captionTV.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:14];
    self.captionTV.backgroundColor = [UIColor clearColor];
    self.captionTV.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
    [self.scrollView addSubview:self.captionTV];

    NSString *kidName = nil;

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

    if (itemKids.count > 1) {
        kidName = [GlobalUtils getKidNames:itemKids];
    } else {
        kidName = ((KPLocalKid *)itemKids[0]).name;
    }

    NSMutableString *tagsNames = [NSMutableString string];

    NSArray *itemTagIDs = [KPLocalItemTag fetchTagIDsForItem:self.item];
    NSArray *itemTags = [KPLocalTag idsToObjects:itemTagIDs];

    NSString *tagname = nil;
    if (itemTagIDs.count > 0) {
        for (KPLocalTag *tag in itemTags) {
            tagname = [NSString stringWithFormat:@"#%@ ", [tag.name stringByReplacingOccurrencesOfString:@" " withString:@""]];
            [tagsNames appendString:tagname];

        }
    }
    else {
        tagsNames = [NSMutableString stringWithFormat:@"#parenting #keepythat #memories", nil];
    }


    NSString *url = nil;

    if (self.short_url) {
        url = [NSString stringWithFormat:@"%@", self.short_url];
    }
    else {
        url = [NSString stringWithFormat:@""];
    }

    if (self.item.title) {
        self.captionTV.text = [NSString stringWithFormat:@"\"%@\"\n%@'s latest memory via @keepyme\nCheck it out:\n%@\n%@", self.item.title, kidName, url, [NSString stringWithString:tagsNames]];
    } else {
        self.captionTV.text = [NSString stringWithFormat:@"%@'s latest memory via @keepyme\nCheck it out:\n%@\n%@", kidName, url, [NSString stringWithString:tagsNames]];
    }


    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    [self.scrollView addGestureRecognizer:gesture];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardDidShow:)
                   name:UIKeyboardDidShowNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];
}


- (void)viewWillAppear:(BOOL)animated {

}

- (void)viewDidLayoutSubviews {
    self.scrollView.frame = self.view.frame;
    self.scrollView.contentSize = self.view.frame.size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)topBtnTap:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"item-share", @"result" : (sender.tag) ? @"ok" : @"cancel"}];

    if (sender.tag == 1) //Ok
    {
        [self doShare];
        /*
        if (self.isAfterAddItem)
        {
            [GlobalUtils upladCurrentItem:^(id result) {
                if (result != nil)
                {
                    self.item = result;
                    [self doShare];
                }
                else
                {
                    //[SVProgressHUD showErrorWithStatus:@""];
                }
            }];
        }
        else
        {            
            [self doShare];
        }*/
    }
    else {
        //if (self.isAfterAddItem)
        //    [self.navigationController popViewControllerAnimated:YES];
        //else
        [self closeView];
    }
}

- (void)doShare {
    if (self.fbButton.selected || self.twitterButton.selected || self.keepyfbButton.selected) {

        [[Mixpanel sharedInstance] track:@"share" properties:@{@"facebook" : @(self.fbButton.selected),
                @"twitter" : @(self.twitterButton.selected),
                @"keepyfb" : @(self.keepyfbButton.selected),
                @"source" : (self.isAfterAddItem) ? @"add photo" : @"share"}];

        if (self.twitterButton.selected && self.captionTV.text.length > kTwitterSharingMaxTextSize) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"the message should be shorter if you would like to share it in Twitter")];
        }
        else {
            [SVProgressHUD showWithStatus:KPLocalizedString(@"updating")];
            [[ServerComm instance] shareItem:self.item.serverID andMessage:self.captionTV.text andFacebook:self.fbButton.selected andTwitter:self.twitterButton.selected andKeepyFBShare:self.keepyfbButton.selected withSuccessBlock:^(id result) {

                [SVProgressHUD dismiss];

                if (self.pinterestButton.selected) {
                    [self shareOnPinterest];
                } else {
                    [self closeView];
                }
            }                   andFailBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
            }];
        }
    }
    else {
        [self closeView];
    }
}

- (void)closeView {
    if (self.navigationController != nil && self.navigationController.view.tag != 1) {
        if (self.isAfterAddItem) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//            [self.navigationController dismissViewControllerAnimated:YES completion:^{
//                [[NSNotificationCenter defaultCenter] postNotificationName:ADD_PHOTO_END_NOTIFICATION object:nil];
//            }];
            [GlobalUtils postNotificationForNewItem:self.item];

        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }

    
    GlobalUtils *globalUtils = [GlobalUtils sharedInstance];
    ItemsViewController *globalItemsVC = globalUtils.itemsVC;
    
    NSArray *arr = [[DKMultyUploaderManger sharedClient] allVideosUploadingProccess];
    for (DKVideoUploaderManager *obj in arr) {
        
        if (obj.startFromHere == 0) {
            [obj zeroOutTheProgressBar];
        }
    }
    
    if(self.item.itemType == 4){ // it's a video
        [globalItemsVC feedModeTap:self];
    }
    
}

- (void)shareOnPinterest {
    /*[self.pinterest createPinWithImageURL:<#(NSURL *)#> sourceURL:<#(NSURL *)#> description:<#(NSString *)#>]
     */
    [self closeView];
}

- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self.captionTV resignFirstResponder];
}

- (void)fbButtonTap:(id)sender {
    if (!self.fbButton.selected) {
        [SVProgressHUD showWithStatus:KPLocalizedString(@"connecting to facebook")];

        [[Mixpanel sharedInstance] track:@"fb-share-on-tap"];
        
        NSArray *permissions = @[@"publish_actions"];
        
        FBSDKLoginManager* fb = [FBSDKLoginManager new];
        [fb logInWithPublishPermissions:permissions
#if FBSDK_46
                     fromViewController:self
#endif
                                handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            [SVProgressHUD dismiss];
            
            if (!result.isCancelled) {
                [[Mixpanel sharedInstance] track:@"fb-share-on"];
                
                self.fbButton.selected = YES;
                [[UserManager sharedInstance] setFacebookShare:YES];
                [self checkEnableSaveButton];
                
            }
        }];
    }
    else {
        [[Mixpanel sharedInstance] track:@"fb-share-off"];

        self.fbButton.selected = NO;
        [self checkEnableSaveButton];
        [[UserManager sharedInstance] setFacebookShare:NO];
    }
}

- (void)twitterButtonTap:(id)sender {
    
}

/*
-(void)pinterestButtonTap:(id)sender
{
    if (!self.pinterestButton.selected)
    {
        if ([self.pinterest canPinWithSDK])
        {
            self.pinterestButton.selected = YES;
            [[UserManager sharedInstance] setPinterestShare:YES];
        }
        else
        {
            NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:@(-1), @"id", KPLocalizedString(@"to use pinterest you need to download the pinterest app"), @"body", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @"", @"link", nil], [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"get it"), @"title", @"https://itunes.apple.com/en/app/pinterest/id429047995?mt=8", @"link", nil], nil], @"buttons", @"download-pinterest", @"name", nil];
            [GlobalUtils showKeepyPopup:d];            
        }
    }
    else
    {
        self.pinterestButton.selected = NO;
        [[UserManager sharedInstance] setPinterestShare:NO];
    }
}
*/
- (void)keepyFBButtonTap:(id)sender {

    if (![[UserManager sharedInstance] approvedKeepyGalleryShare]) {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"keepy community", comment: @"")
                                                                    message:NSLocalizedString(@"This keepy will be posted on our Keepy Community page - a public webpage where proud keepers (like yourself!) share their families memories.", comment: @"")
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:nil]];
        [ac addAction:[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [[UserManager sharedInstance] setApprovedKeepyGalleryShare:YES];
            [self keepyFBButtonTap:nil];
        }]];
        [self presentViewController:ac animated:YES completion:nil];
        return;
    }

    if (!self.keepyfbButton.selected) {

        [[Mixpanel sharedInstance] track:@"keepyfb-share-on-tap"];

        //[[UserManager sharedInstance] requireFacebook:^(id result) {
        [[Mixpanel sharedInstance] track:@"keepyfb-share-on"];


        self.keepyfbButton.selected = (!self.keepyfbButton.selected);
        [[UserManager sharedInstance] setKeepyFBShare:self.keepyfbButton.selected];
        [self checkEnableSaveButton];
        //}];
    }
    else {
        self.keepyfbButton.selected = (!self.keepyfbButton.selected);
        [[UserManager sharedInstance] setKeepyFBShare:self.keepyfbButton.selected];

        [[Mixpanel sharedInstance] track:@"keepyfb-share-off"];

        [self checkEnableSaveButton];
    }
}

#pragma mark - Keyboard hide/show

- (void)keyboardDidShow:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(54.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

    CGPoint point = self.captionTV.frame.origin;
    CGRect aRect = CGRectMake(point.x, point.y, CGRectGetWidth(self.captionTV.frame), CGRectGetHeight(self.captionTV.frame));
    [self.scrollView scrollRectToVisible:aRect animated:YES];
    //[self.scrollView scrollRectToVisible:CGRectMake(0, -100, 10, 10) animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    [self.scrollView scrollRectToVisible:CGRectMake(54, 0, 10, 10) animated:YES];
    /*
     UIEdgeInsets contentInsets = UIEdgeInsetsZero;
     self.scrollView.contentInset = contentInsets;
     self.scrollView.scrollIndicatorInsets = contentInsets;
     */
    //
}

- (void)checkEnableSaveButton {
    self.rightButtonEnabled = (self.isAfterAddItem || (self.fbButton.selected || self.twitterButton.selected || self.keepyfbButton.selected));
}

@end
