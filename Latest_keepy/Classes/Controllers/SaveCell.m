//
//  SaveCell.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "SaveCell.h"

@interface SaveCell ()

@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *saveButton;

@end

@implementation SaveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17.0f];
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f];
}

+ (CGFloat)height {
    return 44.0f;
}

- (IBAction)cancel:(id)sender {
    [self.delegate cancelled];
}

- (IBAction)save:(id)sender {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.saveButton setEnabled:NO];
    [self.saveButton setTitle:@"saving..." forState:UIControlStateNormal];
    [self.delegate saving];
}

- (void)success {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.saveButton setTitle:@"saved!" forState:UIControlStateNormal];
    [self performSelector:@selector(saved) withObject:nil afterDelay:1.25f];
}

- (void)saved {
    [self.saveButton setEnabled:YES];
    [self.delegate saved];
}

- (void)failed {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.saveButton setTitle:@"failed!" forState:UIControlStateNormal];
    [self performSelector:@selector(fail) withObject:nil afterDelay:1.25f];
}

- (void)fail {
    [self.saveButton setTitle:@"save" forState:UIControlStateNormal];
    [self.saveButton setEnabled:YES];
    [self.delegate failed];
}

- (void)disable {
    [self.saveButton setEnabled:NO];
}

- (void)enable {
    [self.saveButton setEnabled:YES];
}

@end
