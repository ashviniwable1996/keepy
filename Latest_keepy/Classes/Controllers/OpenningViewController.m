//
//  OpenningViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 5/10/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>




#import "OpenningViewController.h"
#import "PreRegViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"

#import "Keepy-Swift.h"

@interface OpenningViewController () <LoginViewDelegate, UIScrollViewDelegate>

@property(nonatomic, strong) UIView *frontView;
@property(nonatomic, strong) UIView *backView;
@property(nonatomic, strong) UIView *welcomeView;
@property(nonatomic, strong) UIPageControl *pControl;
@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic) int mode;

@end

@implementation OpenningViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithMode:(int)mode {
    self = [super initWithNibName:@"OpenningViewController" bundle:nil];
    if (self) {
        self.mode = mode;
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"open screen"];


    self.title = KPLocalizedString(@"back");

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];

    dispatch_async(dispatch_get_global_queue(0, 0), ^{

        NSArray *arr = [NSArray arrayWithObjects:@(1479), @(1480), @(1486), @(22593)
                , @(22594), @(22595), @(22596), @(22597), @(22598), @(22600), nil];
        for (int j = 0; j > [arr count]; j++) {
            int i = [[arr objectAtIndex:j] intValue];

            Asset *asset = [Asset MR_createEntity];
            asset.assetId = @(i);
            asset.assetType = @(0);
            //[[ServerComm instance] downloadAssetFile:asset withSuccessBlock:nil andFailBlock:nil];
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d_medium.jpg", Defaults.assetsURLString, i]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            }];
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d_iphone.jpg", Defaults.assetsURLString, i]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            }];


        }

        Asset *asset = [Asset MR_createEntity];
        asset.assetId = @(1639);
        asset.assetType = @(0);
        //[[ServerComm instance] downloadAssetFile:asset withSuccessBlock:nil andFailBlock:nil];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d.jpg", Defaults.assetsURLString, 1639]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        }];


        asset = [Asset MR_createEntity];
        asset.assetId = @(1634);
        asset.assetType = @(0);
        //[[ServerComm instance] downloadAssetFile:asset withSuccessBlock:nil andFailBlock:nil];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d.jpg", Defaults.assetsURLString, 1634]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        }];

    });

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

    self.scrollView.contentSize = CGSizeMake(320 * 3, self.view.frame.size.height);
    self.scrollView.contentInset = UIEdgeInsetsZero;

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.scrollView.contentSize = CGSizeMake(320 * 3, self.view.frame.size.height);
    self.scrollView.contentInset = UIEdgeInsetsZero;
    //self.scrollView.contentOffset = CGPointMake(0, 0);
}

/*
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.welcomeView!=nil)
    {
        [(UIScrollView*)[self.welcomeView.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, 320, 568)];
        [(UIScrollView*)[self.welcomeView.subviews objectAtIndex:0] scrollRectToVisible:CGRectMake(0, 0, 10, 10) animated:NO];
        [(UIScrollView*)[self.welcomeView.subviews objectAtIndex:0] setContentInset:UIEdgeInsetsZero];
        [(UIScrollView*)[self.welcomeView.subviews objectAtIndex:0] setContentSize:CGSizeMake(320 * 3, 568)];
        
    }
}

-(void) viewWillDisappear:(BOOL)animated
{
//    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO];
}
 */

- (void)fbloginTap:(NSNotification*)note {
    NSAssert(NO, @"fbloginTap: isn't implemented");
}

- (void)viewDidLayoutSubviews {
    if (self.backView != nil) {
        return;
    }

    CGRect aframe = self.view.bounds;
    //aframe.origin.y -= 20;
    //aframe.size.height += 20;

    //back view
    self.backView = [[UIView alloc] initWithFrame:self.view.frame];
    self.backView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((320 - 159) / 2, 115, 159, 74)];
    img.image = [UIImage imageNamed:@"keepy-logo-with-tagline"];
    [self.backView addSubview:img];

    //register
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerButton setBackgroundImage:[[UIImage imageNamed:@"green-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];

    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registerButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:18];
    [registerButton setTitle:KPLocalizedString(@"sign up") forState:UIControlStateNormal];
    [registerButton setTitleColor:UIColorFromRGB(0x4BD64D) forState:UIControlStateNormal];

    registerButton.frame = CGRectMake((self.view.frame.size.width - 194) / 2, 250, 194, 44);
    registerButton.titleEdgeInsets = UIEdgeInsetsMake(-2, 0, 0, 0);
    [registerButton addTarget:self action:@selector(registerTap:) forControlEvents:UIControlEventTouchUpInside];

    [self.backView addSubview:registerButton];

    //login
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginButton setBackgroundImage:[[UIImage imageNamed:@"orange-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];

    [loginButton setTitleColor:UIColorFromRGB(0xFD8F45) forState:UIControlStateNormal];
    loginButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:18];
    loginButton.titleEdgeInsets = UIEdgeInsetsMake(-2, 0, 0, 0);
    [loginButton setTitle:KPLocalizedString(@"log in") forState:UIControlStateNormal];

    loginButton.frame = CGRectMake((self.view.frame.size.width - 194) / 2, 310, 194, 44);
    [loginButton addTarget:self action:@selector(loginTap:) forControlEvents:UIControlEventTouchUpInside];

    [self.backView addSubview:loginButton];

    //facebook
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 0;
    [btn setTitleColor:UIColorFromRGB(0x3C5B93) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:18];
    btn.frame = CGRectMake((self.view.frame.size.width - 194) / 2, 370, 194, 44);
    [btn setBackgroundImage:[[UIImage imageNamed:@"blue-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"f-icon"] forState:UIControlStateNormal];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 150, 0, 0);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);
    [btn setTitle:KPLocalizedString(@"connect with") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(fbloginTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.backView addSubview:btn];


    //front view
    self.frontView = [[UIView alloc] initWithFrame:self.view.frame];
    aframe.origin.y = 0;
    img = [[UIImageView alloc] initWithFrame:aframe];
    img.image = [UIImage imageNamed:@"Default"];
    img.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //img.contentMode = UIViewContentModeTop;
    [self.frontView addSubview:img];
    [self.view addSubview:self.frontView];


    //welcome view
    self.welcomeView = [[UIView alloc] initWithFrame:self.view.frame];
    self.welcomeView.backgroundColor = UIColorFromRGB(0xe6e4e3);

    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    //scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.welcomeView addSubview:self.scrollView];

    NSArray *welcomeTxtArr = [NSArray arrayWithObjects:KPLocalizedString(@"Organize, save & share the magic of childhood."),
                                                       KPLocalizedString(@"Video comments from loved ones enrich the keepy experience."),
                                                       KPLocalizedString(@"Enjoy keepies everywhere: phone, web, email."),
                                                       nil];

    float ax = 0;
    for (int i = 0; i < 3; i++) {
        img = [[UIImageView alloc] initWithFrame:CGRectMake(ax, 0, 320, 568)];
        img.image = [UIImage imageNamed:[NSString stringWithFormat:@"welcome-image%d", i + 1]];
        [self.scrollView addSubview:img];

        CGSize titlTextSize = [[welcomeTxtArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17] thatFitsToSize:CGSizeMake(100, 230) lineBreakMode:NSLineBreakByWordWrapping];


        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(ax + 30, 280, 260, titlTextSize.height)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:19];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = [welcomeTxtArr objectAtIndex:i];
        lbl.numberOfLines = 3;
        [self.scrollView addSubview:lbl];

        ax += 320;
    }

    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:19];
    [btn setTitle:KPLocalizedString(@"sign up FREE") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(registerTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(15, self.view.frame.size.height - 15 - 44, 175, 44);
    [btn setBackgroundImage:[[UIImage imageNamed:@"signupFREE-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)] forState:UIControlStateNormal];
    [self.welcomeView addSubview:btn];

    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:19];
    [btn setTitle:KPLocalizedString(@"login") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(loginTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(200, self.view.frame.size.height - 15 - 44, 105, 44);
    [btn setBackgroundImage:[[UIImage imageNamed:@"login-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)] forState:UIControlStateNormal];
    [self.welcomeView addSubview:btn];

    self.scrollView.contentSize = CGSizeMake(320 * 3, self.view.frame.size.height);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    self.scrollView.bounces = NO;

    self.pControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 390, 320, 20)];
    self.pControl.numberOfPages = 3;
    self.pControl.currentPageIndicatorTintColor = UIColorFromRGB(0xd6ff46);
    self.pControl.pageIndicatorTintColor = UIColorFromRGB(0xE6E4E3);

    [self.pControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    [self.welcomeView addSubview:self.pControl];

    /////////////////

    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [UIView transitionFromView:self.frontView toView:(self.mode == 0) ? self.welcomeView : self.backView
                          duration:1.0
                           options:UIViewAnimationOptionTransitionFlipFromRight
                        completion:NULL];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)registerTap:(id)sender {
    PreRegViewController *avc = [[PreRegViewController alloc] init];
    [self.navigationController pushViewController:avc animated:YES];
}

- (void)loginTap:(id)sender {

    [[Mixpanel sharedInstance] track:@"Login tap"];

    LoginViewController *avc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    avc.delegate = self;
    [self.navigationController pushViewController:avc animated:YES];
}


- (void)startKeepyingTap:(id)sender {


    [[Mixpanel sharedInstance] track:@"guide" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"start-keepying", @"label", nil]];


    [UIView transitionFromView:self.welcomeView toView:self.backView
                      duration:1.0
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    completion:NULL];
}

- (void)changePage:(id)sender {
    UIScrollView *scrollView = (UIScrollView *) [self.welcomeView.subviews objectAtIndex:0];
    CGFloat x = self.pControl.currentPage * scrollView.frame.size.width;
    [scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

#pragma mark - LoginViewDelegate

- (void)didLogin:(NSDictionary *)userInfo {
    [GlobalUtils hideAllTips];

    AppDelegate *d = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [d createHome:YES];
    [d.navController setViewControllers:[NSArray arrayWithObject:d.viewController] animated:YES];

    [[UserManager sharedInstance] setIsAfterLogin:YES];

    [GlobalUtils saveUser:userInfo];


}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pControl.currentPage = page;


    [[Mixpanel sharedInstance] track:@"guide" properties:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)page], @"label", nil]];


}

- (void)nextTap:(id)sender {
    self.pControl.currentPage = self.pControl.currentPage + 1;
    UIScrollView *scrollView = (UIScrollView *) [self.welcomeView.subviews objectAtIndex:0];
    CGFloat x = self.pControl.currentPage * scrollView.frame.size.width;
    [scrollView setContentOffset:CGPointMake(x, 0) animated:YES];


    [[Mixpanel sharedInstance] track:@"guide" properties:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)self.pControl.currentPage], @"label", nil]];


}

- (void)appBecomeActive {
    DDLogInfo(@"active:%2f", self.view.frame.size.height);
    self.scrollView.contentSize = CGSizeMake(320 * 3, self.view.frame.size.height);
    self.scrollView.contentInset = UIEdgeInsetsZero;
}

@end
