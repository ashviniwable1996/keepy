//
//  HomeViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/7/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

@import Photos;

#import "HomeViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

#import "KidsMenuViewController.h"
#import "MAImagePickerController.h"
#import "KPDataParser.h"
#import "PKRevealController.h"
#import "AppDelegate.h"
#import "NotificationsViewController.h"
#import "NoteEditorViewController.h"
#import "GalleryPickerViewController.h"
#import "ELCAlbumPickerController.h"
#import "ImageEditorViewController.h"
#import "CameraViewController.h"
#import "NextStepViewController.h"
#import "FansViewController.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>
#import "ItemEditViewController.h"
#import "FamilyViewController.h"
#import "AssetsDataIsInaccessibleViewController.h"
#import "DKMoviePlayerViewController.h"
#import "KidView.h"
#import "TFAContactsSelectorViewController.h"
#import <Twitter/Twitter.h>
#import "KPPhotoLibrary.h"
#import "KPLocalKid.h"
#import "CalendarCollectionViewController.h"
#import "CalendarCollectionViewLayout.h"
#import "GlobalUtils.h"
#import "PrintIOManager.h"

#import "Keepy-Swift.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )

typedef void (^ResultBlock)( BOOL);


@interface HomeViewController () <EditKidDelegate, GalleryPickerDelegate, NextStepDelegate, CarouselViewProtocol,ProductCollectionViewProtocol > {
    CarouselViewClass * carouselView ;
    NSInteger counter ;
//    ProductCollectionView * productView;
}
@property (weak, nonatomic) IBOutlet UIView *backgroundViewOfTabButtons;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *backgroundVEOfTabButtons;

@property(nonatomic, strong) IBOutlet UIView *bottomPanel;
@property(nonatomic, strong) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerTopConstraint;

@property(nonatomic, strong) IBOutlet UIButton *kidsNavButton;
@property(nonatomic, strong) IBOutlet UIButton *homeButton;
@property(nonatomic, strong) IBOutlet UIButton *gridButton;
@property(nonatomic, strong) IBOutlet UIButton *settingsNavButton;
@property(nonatomic, strong) IBOutlet UIButton *notificationsButton;
@property(nonatomic, strong) IBOutlet UILabel *notificationsLabel;
@property(nonatomic, strong) IBOutlet UIButton *addKidButton;
@property(nonatomic, strong) IBOutlet UIButton *addKeepyButton;
@property (weak, nonatomic) IBOutlet UIButton *storeButton;

@property(nonatomic, strong) NotificationsViewController *notificationsVC;
@property(nonatomic) BOOL bottomPanelHidden;
@property(nonatomic, strong) UIView *uploadProgressView;
@property(nonatomic, strong) UIView *menuView;
@property(nonatomic, strong) UIButton *transparentButton;

@property(nonatomic, strong) UINavigationController *addPhotoNavController;
@property(nonatomic, strong) NextStepViewController *nextStepVC;
@property(nonatomic) BOOL isLockedKeepyFeed;

@property(nonatomic, strong) ServerComm *globalServer;

@property(nonatomic, strong) NSDictionary *activatorData;

@property(nonatomic, strong) NSArray *kids;
@property (weak, nonatomic) IBOutlet UILabel *greenButtonExplanationLabel;

@property (strong, nonatomic) CalendarCollectionViewController *calendarViewController;

- (IBAction)takePhotoTap:(id)sender;

- (IBAction)kidsNavTap:(id)sender;

- (IBAction)settingsNavTap:(id)sender;

- (IBAction)notificationsTap:(id)sender;

- (IBAction)addKidTap:(id)sender;

- (IBAction)keepyBtnTap:(id)sender;

@end

@implementation HomeViewController
@synthesize productView;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSArray *children = [KPLocalKid fetchAll];
        // self.isLockedKeepyFeed = (([[Kid MR_findAll] count] < 2) && !self.isFromLogin);
        self.isLockedKeepyFeed = ((children.count < 2) && !self.isFromLogin);
        if (self.isLockedKeepyFeed) {
            self.editKidVC = [[EditKidViewController alloc] initWithKid:nil];
            self.editKidVC.delegate = self;
        }

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNextStep) name:SHOW_NEXTSTEP_NOTIFICATION object:nil];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
        
        // Ashvini
       // self.containerTopConstraint.constant = 13;
        
        [self.view layoutIfNeeded];
    }
    
    [GlobalUtils setIsEditMode:YES];
    [self notificationsRefreshed:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(notificationsRefreshed:)
                   name:NOTIFICATIONS_NEED_TO_BE_RESET
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(TLAContactMassage:)
                   name:TELL_A_FRIEND_CONTACT_SHARING_DONE
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(TLATriggedMassage:)
                   name:TELL_A_FRIEND_TRIGGERD
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(userDidLogin:)
                   name:USER_LOGIN_NOTIFICATION
                 object:nil];

//    [[NSNotificationCenter defaultCenter]
//            addObserver:self
//               selector:@selector(albumSelected:)
//                   name:ALBUM_SELECT_NOTIFICATION
//                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(appModeChanged:)
                   name:APP_MODE_CHANGED_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(filterOn:)
                   name:FILTER_ON_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(filterOff:)
                   name:FILTER_OFF_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(openCamera:)
                   name:OPEN_CAMERA_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(showKidsMenu:)
                   name:SHOW_KIDS_MENU_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(notificationsRefreshed:)
                   name:NOTIFICATIONS_REFRESHED_NOTIFICATION
                 object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showUnlimitedScreen)
     name:SHOW_UNLIMITED_AFTER_ONBOARD
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(openGallery)
     name:@"NotificationTriggerKeepyButtonTap"
     object:nil];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CloseClickedOnShare" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkTheTopVC)
                                                 name:@"CloseClickedOnShare" object:nil];

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadAssetsProgress:) name:UPLOAD_ASSETS_PROGRESS_NOTIFICATION object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kidsRefreshedNotification:) name:KIDS_REFRESHED_NOTIFICATION object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeTheView) name:CLOSE_FLOATING_VIEW object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPhotoEnd:) name:ADD_PHOTO_END_NOTIFICATION object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(afterKidAdded:) name:KID_ADDED_NOTIFICATION object:nil];

    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];

    self.view.backgroundColor = UIColorFromRGB(0xe6e4e3);

    self.itemsVC = [[ItemsViewController alloc] initWithNibName:@"ItemsViewController" bundle:nil];
   
    [self.itemsVC.view setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
//    /* UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
//    [aFlowLayout setItemSize:CGSizeMake(73, 73)];
//    [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical]; */
//    UICollectionViewLayout *calendarLayout = [CalendarCollectionViewLayout new];
//    UICollectionViewController *calendarViewController = [[CalendarCollectionViewController alloc] initWithCollectionViewLayout:calendarLayout];
//
//    // [self addChildViewController:calendarViewController];
//    // [calendarViewController didMoveToParentViewController:self]; // we need to make sure we are focussed on ourselves here
//    // self.calendarViewController = calendarViewController;
//    // [self.view addSubview:calendarViewController.view];
//
//    //[self.view insertSubview:calendarViewController.view atIndex:0];
//    // [self.container addSubview:calendarViewController.view];
//    [self presentViewController:calendarViewController animated:YES completion:nil];
    
    [self.container addSubview:self.itemsVC.view];
    BOOL isFeedMode = ([[UserManager sharedInstance] feedMode] == 0);
    self.homeButton.selected = ([[UserManager sharedInstance] feedMode] == 0);
    self.gridButton.selected = ([[UserManager sharedInstance] feedMode] == 1);
    GlobalUtils *utils = [GlobalUtils sharedInstance];
    utils.itemsVC = self.itemsVC;
    /*
     self.addKidButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:15];
     [self.addKidButton setTitle:[NSString stringWithFormat:@"  +%@", KPLocalizedString(@"add kid")] forState:UIControlStateNormal];
     */
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gesture];

    UIColor *bgColor = UIColorFromRGB(0x4d3c36);
    [[self.bottomPanel.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
    [[self.bottomPanel.subviews objectAtIndex:0] setBackgroundColor:bgColor];
    [[self.bottomPanel.subviews objectAtIndex:1] setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
    
    self.bottomPanel.frame = CGRectMake(0, self.bottomPanel.frame.origin.y, [UIScreen mainScreen].bounds.size.width, self.bottomPanel.frame.size.height);
    self.backgroundVEOfTabButtons.frame = CGRectMake(0, self.backgroundVEOfTabButtons.frame.origin.y , [UIScreen mainScreen].bounds.size.width, self.backgroundVEOfTabButtons.frame.size.height );
    self.backgroundViewOfTabButtons.frame = CGRectMake(0, self.backgroundViewOfTabButtons.frame.origin.y , [UIScreen mainScreen].bounds.size.width, self.backgroundViewOfTabButtons.frame.size.height );
   
    if([UIScreen mainScreen].bounds.size.height == 812){
        self.bottomPanel.frame = CGRectMake(0, self.bottomPanel.frame.origin.y - 15, [UIScreen mainScreen].bounds.size.width, self.bottomPanel.frame.size.height + 15);
        self.backgroundVEOfTabButtons.frame = CGRectMake(0, self.backgroundVEOfTabButtons.frame.origin.y - 15, [UIScreen mainScreen].bounds.size.width, self.backgroundVEOfTabButtons.frame.size.height + 15);
        self.backgroundViewOfTabButtons.frame = CGRectMake(0, self.backgroundViewOfTabButtons.frame.origin.y - 15, [UIScreen mainScreen].bounds.size.width, self.backgroundViewOfTabButtons.frame.size.height + 15);
    }
   
//    CGRect bottomPanelFrame = self.bottomPanel.frame; // this was only used for debugging to find out the height of the bottom panel

    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        [(UIToolbar *) [self.bottomPanel.subviews objectAtIndex:1] setBarTintColor:UIColorFromRGB(0x4d3c36)];
    }

//    self.loader.center = self.view.center;
    CGFloat width = [UIScreen mainScreen].bounds.size.width / 5;
    self.homeButton.frame = CGRectMake(0, 0, 64, 42);
    self.gridButton.frame = CGRectMake(width, 0, 64, 42);
    CGFloat difference = 0.0;
    if (width > 51) {
        difference = width - 51;
    }
    self.addKeepyButton.frame = CGRectMake((width * 2) + (difference / 2), -13, 51, 51);
    self.storeButton.frame = CGRectMake(width * 3, 0, 64, 42);
    self.settingsNavButton.frame = CGRectMake(width * 4, 0, 64, 42);
    //[self showNextStep];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.bottomPanel.frame = CGRectMake(0, self.bottomPanel.frame.origin.y, [UIScreen mainScreen].bounds.size.width, self.bottomPanel.frame.size.height);
    self.backgroundVEOfTabButtons.frame = CGRectMake(0, self.backgroundVEOfTabButtons.frame.origin.y , [UIScreen mainScreen].bounds.size.width, self.backgroundVEOfTabButtons.frame.size.height );
    self.backgroundViewOfTabButtons.frame = CGRectMake(0, self.backgroundViewOfTabButtons.frame.origin.y , [UIScreen mainScreen].bounds.size.width, self.backgroundViewOfTabButtons.frame.size.height );
}
- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)showUnlimitedScreen{
    AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [d reloadTweaks];
    long tweakValue = [d.tweakValues[TweakPromoUnlimitedAfterOnboarding] integerValue];
    [[Mixpanel sharedInstance] track:TweakPromoUnlimitedAfterOnboarding properties:@{@"source": TweakPromoUnlimitedAfterOnboarding}];

    if (tweakValue == 1){
        [GlobalUtils showUpgradeScreenWithParameters:nil fromViewController:@"After OnBoard"];
    }
}
- (void)TLAContactMassage:(NSNotification *)data {

    [[Mixpanel sharedInstance] track:@"activator-select-medium" properties:@{@"launched" : @"t"}];

    NSString *campaignId = [data.userInfo objectForKey:@"campaignId"];
    NSArray *arrayData = (NSArray *) data.object;

    // DDLogInfo(@"%@",arrayData);

    [[ServerComm instance] addContactList:(NSArray *) arrayData withCampaignId:campaignId withSuccessBlock:^(id result) {

        //  DDLogInfo(@"%@",result);

        if ([[result objectForKey:@"status"] intValue] == 0) {


            [[[Mixpanel sharedInstance] people] increment:@"activator-count-of-emails" by:@(arrayData.count)];
            [[Mixpanel sharedInstance] track:@"activator-completed" properties:@{@"success": @"t"}];
        }

        else {
            [[Mixpanel sharedInstance] track:@"activator-completed" properties:@{@"success" : @"f"}];
        }

    }                        andFailBlock:^(NSError *error) {


        [[Mixpanel sharedInstance] track:@"activator-completed" properties:@{@"success" : @"f"}];

    }];


    NSTimeInterval delayInSeconds = 0.55;

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

        //do your tasks here

        UIView *contentView = [[UIView alloc] init];
        contentView.backgroundColor = [UIColor colorWithRed:230.0 / 255.0 green:228.0 / 255.0 blue:227.0 / 255.0 alpha:1.0];
        contentView.frame = CGRectMake(0.0, 0.0, 190, 145);

        UIImageView *heart = [[UIImageView alloc] initWithFrame:CGRectMake(95 - 16.5, 30, 33, 28)];
        heart.image = [UIImage imageNamed:@"heart"];
        [contentView addSubview:heart];


        //sub title
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 190, 100)];
        titleLabel.textColor = [UIColor colorWithRed:92.0 / 255.0 green:70.0 / 255.0 blue:67.0 / 255.0 alpha:1];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        titleLabel.numberOfLines = 3;
        titleLabel.text = [NSString stringWithFormat:@"Thanks for telling\nyour friends\nabout Keepy!"];
        [contentView addSubview:titleLabel];

        self.popup = [KLCPopup popupWithContentView:contentView];
        self.popup.shouldDismissOnBackgroundTouch = YES;
        [self.popup show];

        [self performSelector:@selector(dismissKLCPopup) withObject:nil afterDelay:3.0];


    });
}


- (void)runTLATriggedMassage {

    UIView *containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor colorWithRed:230.0 / 255.0 green:228.0 / 255.0 blue:227.0 / 255.0 alpha:1.0];
    containerView.frame = CGRectMake(0.0, 0.0, 280, 300);

    containerView.backgroundColor = [UIColor colorWithRed:230.0 / 255.0 green:228.0 / 255.0 blue:227.0 / 255.0 alpha:1.0];

    NSDictionary *serverData = self.activatorData;

    /*
    NSArray *filteredarray = [[[UserManager sharedInstance] dynamicMessages] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(source == %@)", @"trigger"]];
    
    if ([filteredarray count] > 0) {
        serverData = (NSDictionary*)[filteredarray firstObject];
    }*/

    //DDLogInfo(@"%@",serverData);




    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
            [serverData objectForKey:@"id"], @"activator-id",
            [serverData objectForKey:@"name"], @"activator-name",
            [serverData objectForKey:@"source"], @"activator-source",
            [serverData objectForKey:@"category"], @"activator-category",
            [[serverData objectForKey:@"callToAction"] objectForKey:@"id"], @"call-to-action-id",
            [[serverData objectForKey:@"campaign"] objectForKey:@"id"], @"campaign-id",
                    nil];


    [[Mixpanel sharedInstance] track:@"activator-call-to-action" properties:params];


    NSDictionary *callToAction = (NSDictionary *) [serverData objectForKey:@"callToAction"];

    NSString *titleString = [[callToAction objectForKey:@"subject"] objectForKey:@"text"];

    CGSize titleheight = [self text:titleString sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17] thatFitsToSize:CGSizeMake(self.popup.frame.size.width, 100)];


    float xpos = (containerView.frame.size.width / 2.0f) - ((containerView.frame.size.width - 30) / 2.0f);

    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    close.tag = 1010;
    close.frame = CGRectMake(containerView.frame.size.width - 25, 10, 16, 15);
    [close setBackgroundImage:[UIImage imageNamed:@"close-button"] forState:UIControlStateNormal];
    [close addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:close];

    //sub title
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, 10, containerView.frame.size.width - 30, 50)];
    titleLabel.textColor = [self colorFromHexString:[[callToAction objectForKey:@"subject"] objectForKey:@"color"]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    titleLabel.numberOfLines = 2;
    titleLabel.text = titleString;
    [containerView addSubview:titleLabel];

    //sub text
    UILabel *alabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, titleLabel.frame.origin.y + titleheight.height, containerView.frame.size.width - 30, 60)];
    alabel.textColor = [self colorFromHexString:[[callToAction objectForKey:@"message"] objectForKey:@"color"]];
    alabel.textAlignment = NSTextAlignmentCenter;
    alabel.backgroundColor = [UIColor clearColor];
    alabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    alabel.numberOfLines = 2;
    alabel.text = [[callToAction objectForKey:@"message"] objectForKey:@"text"];
    [containerView addSubview:alabel];


    UIButton *fbShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [fbShare setBackgroundImage:[UIImage imageNamed:@"facebook-on"] forState:UIControlStateNormal];
    fbShare.frame = CGRectMake(containerView.frame.size.width / 2 - 26.5 - 90, 125, 53.0f, 53.0f);
    [fbShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
    fbShare.tag = 0;
    [containerView addSubview:fbShare];


    UILabel *fbShareText = [[UILabel alloc] initWithFrame:CGRectMake(containerView.frame.size.width / 2 - 26.5 - 90, 180, 53.0f, 20.0f)];
    fbShareText.textAlignment = NSTextAlignmentCenter;
    fbShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
    fbShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    fbShareText.text = KPLocalizedString(@"Share");
    [containerView addSubview:fbShareText];


    UIButton *twitterShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [twitterShare setBackgroundImage:[UIImage imageNamed:@"twitter-on"] forState:UIControlStateNormal];
    twitterShare.frame = CGRectMake(containerView.frame.size.width / 2 - 26.5, 125, 53.0f, 53.0f);
    [twitterShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
    twitterShare.tag = 1;
    [containerView addSubview:twitterShare];

    UILabel *tweetShareText = [[UILabel alloc] initWithFrame:CGRectMake(containerView.frame.size.width / 2 - 26.5, 180, 53.0f, 20.0f)];
    tweetShareText.textAlignment = NSTextAlignmentCenter;
    tweetShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
    tweetShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    tweetShareText.text = KPLocalizedString(@"Tweet");
    [containerView addSubview:tweetShareText];


    UIButton *msgShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [msgShare setBackgroundImage:[UIImage imageNamed:@"email-on"] forState:UIControlStateNormal];
    msgShare.frame = CGRectMake(containerView.frame.size.width / 2 - 26.5 + 90, 125, 53.0f, 53.0f);
    [msgShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
    msgShare.tag = 2;
    [containerView addSubview:msgShare];


    UILabel *msgShareText = [[UILabel alloc] initWithFrame:CGRectMake(containerView.frame.size.width / 2 - 26.5 + 80, 180, 73.0f, 20.0f)];
    msgShareText.textAlignment = NSTextAlignmentCenter;
    msgShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
    msgShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    msgShareText.text = KPLocalizedString(@"Message");
    [containerView addSubview:msgShareText];


    UITextView *incentiveText = [[UITextView alloc] initWithFrame:CGRectMake(0, 240, containerView.frame.size.width, 60)];
    incentiveText.editable = NO;
    incentiveText.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    incentiveText.textAlignment = NSTextAlignmentCenter;
    incentiveText.textColor = [self colorFromHexString:[[callToAction objectForKey:@"disclaimer"] objectForKey:@"color"]];
    incentiveText.text = [[callToAction objectForKey:@"disclaimer"] objectForKey:@"text"];
    incentiveText.backgroundColor = [UIColor clearColor];
    [containerView addSubview:incentiveText];


    self.popup = [KLCPopup popupWithContentView:containerView];
    self.popup.shouldDismissOnBackgroundTouch = NO;
    [self.popup show];

}

- (void)TLATriggedMassage:(NSNotification *)notif {

    // DDLogInfo(@"%@",notif.object);



    [[ServerComm instance] activator:[notif.object objectForKey:@"id"] withSuccessBlock:^(id result) {

        // DDLogInfo(@"%@",result);

        if ([[result objectForKey:@"status"] intValue] == 0) {
            [self setActivatorData:[result objectForKey:@"result"]];
            [self runTLATriggedMassage];
        }


    }                   andFailBlock:^(NSError *error) {

    }];



    /*
    if (![[UserManager sharedInstance] dynamicMessages]) {
      
        [[ServerComm instance] getUserInfo:^(id result) {
            
            result = [result valueForKey:@"result"];
            
            //set dynamicMessages for tell a friend action
            if ([result valueForKey:@"activators"]) {
                
                NSArray *dArray = (NSArray*)[result valueForKey:@"activators"];
                
                if ([dArray count] > 0) {
                    [[UserManager sharedInstance] setDynamicMessages:dArray];
                    [self runTLATriggedMassage];
                }
            }
            
            
        } andFailBlock:^(NSError *error) {
            
        }];

    }
    else{
        [self runTLATriggedMassage];
    }
   
    
*/
}

- (void)dismissKLCPopup {
    [self.popup dismiss:YES];
}

- (void)shareActionClick:(UIButton*)sender {

    if ([sender tag] == 1010) {
        [self.popup dismiss:YES];
    }


    [self.popup dismiss:YES];


    NSTimeInterval delayInSeconds = 0.55;

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

        NSString *type = nil;

        switch ([sender tag]) {
            case 0:
                type = @"fb";
                break;
            case 1:
                type = @"twitter";
                break;
            case 2:
                type = @"email";
                break;
            default:
                break;
        }

        NSDictionary *params = nil;
        if ([sender tag] == 0 || [sender tag] == 1) {
            params = [NSDictionary dictionaryWithObjectsAndKeys:type, @"medium", @"launched", @"t", nil];
        } else {
            params = [NSDictionary dictionaryWithObjectsAndKeys:type, @"medium", nil];
        }


        [[Mixpanel sharedInstance] track:@"activator-select-medium" properties:params];

        switch ([sender tag]) {
            case 0:
                [self shareViaFacebook];
                [[Mixpanel sharedInstance] track:@"taf via facebook"];
                break;
            case 1:
                [self shareViaTwitter];
                [[Mixpanel sharedInstance] track:@"taf via twitter"];
                break;
            case 2:
                [self shareViaContacts];
                [[Mixpanel sharedInstance] track:@"taf via contact"];
                break;
            default:
                break;
        }

    });


}

- (void)hideWellcomOnboardMenu {
    if (self.OBContainer != nil) {
        [self.OBContainer removeFromSuperview];
        self.OBContainer = nil;
    }

}

- (void)dismissonBoard {


    [UIView transitionWithView:self.BGView duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve //change to whatever animation you like
                    animations:^{
                        [self.OBContainer setFrame:CGRectMake(10, -300, 300, 250)];
                        self.BGView.alpha = 0.0;
                        self.OBContainer.alpha = 0.0;


                    }
                    completion:^(BOOL finished) {
                        if (finished) {
                            [self.BGView removeFromSuperview];
                            self.BGView = nil;
                        }
                    }

    ];
}

- (void)onBoard {

    BOOL isAfterOnBoardProcess = [[UserManager sharedInstance] isAfterOnBoardProccess];
    BOOL isAfterRegistration = [[UserManager sharedInstance] isAfterRegistration];
    BOOL isAfterLogin = [[UserManager sharedInstance] isAfterLogin];
    
    BOOL isExplanationEnabled = YES;

    NSLog(@"-------------- Starting Home View Controller. After on board? %@. After registration? %@", isAfterOnBoardProcess ? @"Y":@"N", isAfterRegistration? @"Y":@"N");

    // TODO: make onboard appear after registration



    // if ([[UserManager sharedInstance] isAfterOnBoardProccess] || (!showPopUp && ![[UserManager sharedInstance] isAfterRegistration])) {
    
    if(isExplanationEnabled && !isAfterLogin && isAfterOnBoardProcess){ // this means that it is after the onboarding process that came after the registration
        DDLogInfo(@"TRIGGER ON BOARD");
        __weak HomeViewController *weakSelf = self;
        [[ServerComm instance] getKids:YES withSuccessBlock:^(NSDictionary *result) {
            weakSelf.kids = [NSArray arrayWithArray:result[@"result"]];
            
            
            // TODO: to re-enable the after-onboarding explanation, enable this
            // [weakSelf performSelector:@selector(start) withObject:nil afterDelay:0.5f];

        }                 andFailBlock:^(NSError *error) {
            // If we failed to load kids, forget onboarding.

        }];
    } else {
        DDLogInfo(@"NOT TRIGGER ON BOARD");
    }

    
    
    
    
    
    
    

    /*
    for (NSDictionary *kid in [Kid MR_findAll]) {
        DDLogInfo(@"%@",kid);
    }*/

}

- (void)start {

    if (self.BGView == nil) {
        return;
    }


    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissonBoard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.BGView addGestureRecognizer:gestureRecognizer];

    self.BGView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - 145);
    self.BGView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    self.BGView.alpha = 0;
    [self.view addSubview:self.BGView];


    CGRect OBContainerRect;

    if (IS_IPHONE5) {
        // this is an iPhone 5+
        OBContainerRect = CGRectMake(10, 145, 300, 250);
    }
    else {
        OBContainerRect = CGRectMake(10, 55, 300, 250);
    }


    [self.OBContainer setFrame:CGRectMake(10, -300, 300, 250)];
    self.OBContainer.alpha = 0;

    
    self.greenButtonExplanationLabel.text = @"Use the GREEN BUTTON \nto add notes, photos \nand videos.";
    
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 110, 300, 1)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    [self.OBContainer addSubview:lineView];

    [UIView transitionWithView:self.BGView duration:0.5
                       options:UIViewAnimationOptionTransitionNone //change to whatever animation you like
                    animations:^{

                        self.BGView.alpha = 1;
                        self.OBContainer.alpha = 1;


                        [self.OBContainer setFrame:OBContainerRect];
                        // OBView.backgroundColor = [UIColor whiteColor];
                        [self.BGView addSubview:self.OBContainer];


                    }
                    completion:^(BOOL finished) {
                        if (finished) {
                            [self keepyBtnTap:nil];
                            // [self.view bringSubviewToFront:self.menuView];
                        }
                    }

    ];


    int positions_3[] = {23, 111.5, 200};
    int positions_2[] = {73, 150};
    int positions_1[] = {111.5};

    // NSMutableArray *tkids = [[NSMutableArray alloc] initWithArray:[Kid MR_findAllSortedBy:@"isMine,kidId" ascending:NO]];

    // DDLogInfo(@"%@",tkids);

    NSInteger count = self.kids.count > 3 ? 3 : self.kids.count;

    for (int i = 0; i < count; i++) {


        NSDictionary *kid = [self.kids objectAtIndex:i];
        KidView *kidView = [[KidView alloc] init];
        //  Kid *kid_ =(Kid*)[tkids objectAtIndex:0];

        //  DDLogInfo(@"%@",kid_);

        if (count == 1) {
            kidView.frame = CGRectMake(positions_1[i], kidView.frame.origin.y + 23, kidView.frame.size.width, kidView.frame.size.height);
        }
        else if (count == 2) {
            kidView.frame = CGRectMake(positions_2[i], kidView.frame.origin.y + 23, kidView.frame.size.width, kidView.frame.size.height);
        }
        else if (count == 3) {
            kidView.frame = CGRectMake(positions_3[i], kidView.frame.origin.y + 23, kidView.frame.size.width, kidView.frame.size.height);
        }

        // DDLogInfo(@"%@", NSStringFromCGRect(kidView.frame));
        kidView.name.text = [kid objectForKey:@"name"];
        id hash = [kid objectForKey:@"hash"];
        if (hash != [NSNull null]) {
            NSString *url = [NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, (NSString *) hash];
            kidView.imageView.image = nil;
            [kidView.activityIndicatorView startAnimating];
            [kidView.imageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [kidView.activityIndicatorView stopAnimating];
                kidView.imageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];
            }];
        } else {
            int gender = [[kid objectForKey:@"gender"] intValue];
            kidView.imageView.image = [GlobalUtils getKidDefaultImage:gender];
        }

        [self.OBContainer addSubview:kidView];
    }

    [[UserManager sharedInstance] setIsAfterOnBoardProccess:NO];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.itemsVC.view.frame = self.view.frame;
    if (!self.notificationsButton) {
        self.notificationsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.notificationsButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 54, 20, 64, 64)];
        if ([UIScreen mainScreen].bounds.size.height >= 812) {
           [self.notificationsButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 54, 20 + 17, 64, 64)];
        }
        [self.notificationsButton setImage:[UIImage imageNamed: @"emptyNotifications"] forState:UIControlStateNormal];
        [self.notificationsButton addTarget:self action:@selector(notificationsTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.container addSubview:self.notificationsButton];
    }
    if (!self.notificationsLabel) {
        self.notificationsLabel = [[UILabel alloc] init];
        self.notificationsLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
        
       // self.notificationsLabel.backgroundColor = UIColor.cyanColor;
        self.notificationsLabel.textAlignment = NSTextAlignmentRight;
       // self.notificationsLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 49, 20, 50, 64);
        self.notificationsLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 95, 20, 60, 64);
        if ([UIScreen mainScreen].bounds.size.height >= 812) {
           // self.notificationsLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 49, 20 + 17, 50, 64);
            self.notificationsLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 95, 20 + 17, 60, 64);
        }
        self.notificationsLabel.textColor = [[UIColor alloc] initWithRed:(76.0f/255.0f) green:(60.0f/255.0f) blue:(54.0f/255.0f) alpha:1.0];
        [self.container addSubview:self.notificationsLabel];
    }
    
    // [self performSelector:@selector(TLATriggedMassage:) withObject:nil afterDelay:5.0];


    /*
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0f);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIStoryboard *onboardStoryboard = [UIStoryboard storyboardWithName:@"Onboard2" bundle:nil];
    FamilyViewController *familyViewController = (FamilyViewController *)[onboardStoryboard instantiateViewControllerWithIdentifier:@"FamilyViewController"];
    familyViewController.backgroundImage = backgroundImage;
    [self presentViewController:familyViewController animated:NO completion:nil];
    
    return;*/


    //[self performSelector:@selector(TLATriggedMassage:) withObject:nil afterDelay:3];

    // If we're not mom or dad, don't show onboarding.
    User *me = [[UserManager sharedInstance] getMe];
    // DDLogInfo(@"%d",[me.parentType intValue]);

    BOOL showOnboarding = [me.parentType intValue] <= 1;
    
    if (showOnboarding)
    {
        [self onBoard];
    }
    [self.itemsVC viewDidAppear:YES];
    
    if ([[UserManager sharedInstance] isAfterRegistration]) {
        // If we're not mom or dad, don't show onboarding.

        NSArray *myChildren = [KPLocalKid fetchMine];

        if (showOnboarding)
        {
            if (myChildren.count > 0)
            {
                UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0f);
                [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
                UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();

                UIStoryboard *onboardStoryboard = [UIStoryboard storyboardWithName:@"Onboard2" bundle:nil];
                FamilyViewController *familyViewController = (FamilyViewController *) [onboardStoryboard instantiateViewControllerWithIdentifier:@"FamilyViewController"];
                familyViewController.backgroundImage = backgroundImage;
                familyViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                [self presentViewController:familyViewController animated:NO completion:nil];
            } /*else {
                [[UserManager sharedInstance] setIsAfterRegistartion:NO];
            }*/
        } else {
            [[UserManager sharedInstance] setIsAfterRegistration:NO];
        }
    }
    
}

- (void)checkTheTopVC{
        if ([self.revealController.frontViewController isKindOfClass:[HomeViewController class]]) {
          [self showThetreasureView];
        }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    //[self.navigationController setNavigationBarHidden:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self notificationsRefreshed:nil];
    
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    

    /*
     if ([[UIApplication sharedApplication] isStatusBarHidden])
     {
     [[UIApplication sharedApplication] setStatusBarHidden:NO];
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
     
     [UIView animateWithDuration:0.2 animations:^{
     self.revealController.view.frame = CGRectMake(0, 20, self.revealController.view.frame.size.width, [UIScreen mainScreen].bounds.size.height-20);
     self.view.frame = CGRectMake(0, 0, self.revealController.view.frame.size.width, self.revealController.view.frame.size.height);
     }];
     }
     */

    /*
     UIView *aview = [self.view viewWithTag:9872];
     if (aview == nil)
     {
     aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
     aview.tag = 9872;
     aview.backgroundColor = [UIColor clearColor];
     aview.userInteractionEnabled = NO;
     if (self.nextStepVC!=nil)
     [self.view insertSubview:aview belowSubview:self.nextStepVC.view];
     else
     [self.view addSubview:aview];
     }
     else if ([aview.subviews count] == 0)
     {
     NSArray *allKids = [Kid MR_findAll];
     if ([allKids count] > 1 && [[[UserManager sharedInstance] myKids] count] == 0)
     {
     aview.userInteractionEnabled = YES;
     //            [GlobalUtils showTip:3 withView:aview withCompleteBlock:^{
     //                [aview removeFromSuperview];
     //            }];
     }
     else if (([[Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]] count] > [[[UserManager sharedInstance] myKids] count]) && ([[UserManager sharedInstance] feedMode] == 0))
     {
     aview.userInteractionEnabled = YES;
     //            [GlobalUtils showTip:1 withView:aview withCompleteBlock:^{
     //                [aview removeFromSuperview];
     //                [GlobalUtils showTip:2 withView:self.view withCompleteBlock:^{
     //
     //                }];
     //            }];
     
     }
     }
     */
    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"home", @"source", nil]];
}
- (void)closeTheViewWithSender:(CarouselViewClass *)sender{
    [self closeTheView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - User Notifications

- (void)userDidLogin:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.revealController showViewController:self.revealController.frontViewController];
    });
}

#pragma mark - Actions

- (void)openCamera:(NSNotification *)notification {
    [self takePhotoTap:notification.object];
}


- (IBAction)takeVideoTap:(id)sender {

    [self hideKeepyMenu];

    if ([KPPhotoLibrary authorizationIsDenied]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        return;
    }

    UIViewController *pvc = self.revealController;
    if (self.revealController.presentedViewController) {
        pvc = self.revealController.presentedViewController;
    }
    
    UIStoryboard* sb = [UIStoryboard storyboardWithName:@"AssetCollection" bundle:nil];
    UINavigationController* nc = [sb instantiateInitialViewController];
    PhotoGalleryViewController* vc = nc.viewControllers.firstObject;
    vc.addCamera = YES;
    vc.addNote = YES;
    
    [pvc presentViewController:nc animated:YES completion:nil];
    return;

#if not_accessible__tbd
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {

        AssetsDataIsInaccessibleViewController *assetsDataInaccessibleViewController =
                [self.storyboard instantiateViewControllerWithIdentifier:@"inaccessibleViewController"];

        NSString *errorMessage = nil;
        switch ([error code]) {
            case ALAssetsLibraryAccessUserDeniedError:
            case ALAssetsLibraryAccessGloballyDeniedError:
                errorMessage = @"The user has declined access to it.";
                break;
            default:
                errorMessage = @"Reason unknown.";
                break;
        }

        // if the user does not allow access to the camera
        if (assetsDataInaccessibleViewController) {
            assetsDataInaccessibleViewController.explanation = errorMessage;
            [self presentViewController:assetsDataInaccessibleViewController animated:NO completion:nil];
        }
    };
#endif
    
    PHFetchOptions* options = [PHFetchOptions new];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    PHFetchResult* videos = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:options];
    
    if (videos.count == 0) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"no videos found")];
        return;
    }
    
    NSMutableArray* arr = [NSMutableArray array];
    
    [videos enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
        [arr addObject:[[KPAsset alloc] initWithAsset:asset]];
        
        if (idx + 1 == videos.count) {
            ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName:nil bundle:nil];
            albumController.delegate = self;
            albumController.title = KPLocalizedString(@"albums");
            albumController.filter = @"allVideos";
            
            GalleryPickerViewController *avc = [[GalleryPickerViewController alloc] initWithAssets:arr];
            avc.title = KPLocalizedString(@"All Videos");
            avc.delegate = self;
            
            self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:albumController];
            [self.addPhotoNavController pushViewController:avc animated:NO];
            self.addPhotoNavController.view.bounds = CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height);
            [pvc presentViewController:self.addPhotoNavController animated:YES completion:nil];

            *stop = YES;

        }
    }];
}

- (IBAction)onHomeClick:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    
    [self hideKeepyMenu];
    sender.selected = YES;
    self.gridButton.selected = NO;
    [self.itemsVC feedModeTap:sender];
    
    [self showThetreasureView];
  
}
- (void)showThetreasureView{
    NSArray * products = [[NSUserDefaults standardUserDefaults] valueForKey:@"FinalProducts"];
    
    if (products != nil && products.count > 0) {
        [self checkIfTheProductsAreReadyToBeDisplayedAndResult:^(BOOL flag) {
            if (flag){
                [UIView animateWithDuration:0.8 animations:^{
                    CGRect currentFrame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 400);
                    
                    if(productView != nil){
                        [productView removeFromSuperview];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[Mixpanel sharedInstance] track:@"Product teasure screen shown" properties:@{@"source": @"Product teasure screen shown"}];

                        productView = [[ProductCollectionView alloc]initWithImageURL:@"" currentViewController:self frameOfView:currentFrame];
                        productView.delegate = self;
                        [self.view addSubview:productView];
                        productView.frame = CGRectMake(0, self.view.frame.size.height - 400, self.view.frame.size.width, 400);
                        [self.view layoutIfNeeded];
                    });
                }];
                [productView reloadCollectionViewData];
            }
        }];
    }
}
-(void)checkIfTheProductsAreReadyToBeDisplayedAndResult:(ResultBlock)block{
   
    NSArray * products = [[NSUserDefaults standardUserDefaults] valueForKey:@"FinalProducts"];
    NSString * imageString = @"";
    counter = 0;
   
    for (NSDictionary *temp in products ){
        imageString = @"";
        imageString = temp[@"productImageUrl"];
        NSLog(@"Image Str:%@",imageString);
        NSString* encodedUrl = [imageString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSURL * url = [NSURL URLWithString:encodedUrl];
        NSLog(@"URL : %@",url);
        
        [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (error == nil) {
                counter ++ ;
                if (counter == 4) {
                    counter = 0;
                    block(true);
                }
            } else {
                block(false);
            }
        }];
    }
}
-(void)closeTheView{
    [[Mixpanel sharedInstance] track:@"PopUpScreen_Close"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FinalProducts"];
    [UIView animateWithDuration:0.8 animations:^{
        productView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 400);
        [self.view layoutIfNeeded];
    }
    completion:^(BOOL finished){ [productView removeFromSuperview]; }];
}
-(void)openShowAllViewWithSender:(ProductCollectionView *)sender{
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"AssetCollection"
//                                                         bundle:nil];
//    AllProductsViewController *allProduct =
//    [storyboard instantiateViewControllerWithIdentifier:@"AllProductsViewController"];
//
//
//    [self presentViewController:allProduct animated:YES completion:nil];
//
//    [self presentViewController:allProduct animated:YES completion:^{
//        [self closeTheView];
//    }];
    [[Mixpanel sharedInstance] track:@"PopUpScreen_more"];

    [PrintIOManager showPrintIODialogInViewController:self.revealController.frontViewController withImage:nil];
//[self closeTheView];
    
}
- (IBAction)onGridClick:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    
    [self hideKeepyMenu];
    sender.selected = YES;
    self.homeButton.selected = NO;
    [self.itemsVC galleryModeTap:sender];
    [self showThetreasureView];
    
}

- (IBAction)onStoreClick:(UIButton *)sender {
    [self hideKeepyMenu];
    
    [PrintIOManager showPrintIODialogInViewController:self.revealController.frontViewController withImage:nil];
}

- (IBAction)takePhotoTap:(UIButton *)sender {
    [self hideKeepyMenu];

    //if (![GlobalUtils checkHasSpace])
    //    return;

    int cameraMode = [[UserManager sharedInstance] cameraMode];

    //if (sender != nil && [sender isKindOfClass:[NSURL class]])
    //    cameraMode = 1;

    UIViewController *pvc = self.revealController;
    if (self.revealController.presentedViewController) {
        pvc = self.revealController.presentedViewController;
    }

    if (sender != nil && [sender isKindOfClass:[NSURL class]]) {
        self.addPhotoNavController = nil;

        NSData *data = [[NSData alloc] initWithContentsOfURL:(NSURL *) sender];
        UIImage *aimage = [UIImage imageWithData:data];
        [self showFinalView:[NSArray arrayWithObject:[NSDictionary dictionaryWithObjectsAndKeys:aimage, UIImagePickerControllerOriginalImage, nil]] applyCrop:NO];
    }
    else if (cameraMode == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {


        [[Mixpanel sharedInstance] registerSuperProperties:[NSDictionary dictionaryWithObjectsAndKeys:@"Camera", @"PhotoSource", nil]];

        CameraViewController *avc = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
        avc.delegate = self;
        self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:avc];
        [pvc presentViewController:self.addPhotoNavController animated:YES completion:nil];
    }
    else {
        if ([KPPhotoLibrary authorizationIsDenied]) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
            return;
        }

        [[Mixpanel sharedInstance] registerSuperProperties:[NSDictionary dictionaryWithObjectsAndKeys:@"Camera Roll", @"PhotoSource", nil]];

        PHFetchOptions* options = [PHFetchOptions new];
        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        PHFetchResult* images = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
        
        if (images.count == 0) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"no photos found")];
            return;
        }
        
        NSMutableArray* arr = [NSMutableArray array];
        
        [images enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
            [arr addObject:[[KPAsset alloc] initWithAsset:asset]];
            
            if (idx + 1 == images.count) {
                *stop = YES;
                ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName:nil bundle:nil];
                albumController.delegate = self;
                albumController.title = KPLocalizedString(@"albums");
                albumController.filter = @"allPhotos";

                GalleryPickerViewController *avc = [[GalleryPickerViewController alloc] initWithAssets:arr];
                avc.showToolbar = YES;
                avc.delegate = self;
                self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:albumController];
                [self.addPhotoNavController pushViewController:avc animated:NO];
                self.addPhotoNavController.view.bounds = CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height);
                [pvc presentViewController:self.addPhotoNavController animated:YES completion:nil];
            }
        }];
    }
}

- (IBAction)kidsNavTap:(id)sender {
    [self hideKeepyMenu];

    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"kids-nav-btn", @"btn", nil]];

    [self.revealController showViewController:self.revealController.leftViewController];
}

- (IBAction)settingsNavTap:(id)sender {
    [self hideKeepyMenu];

    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"settings-nav-btn", @"btn", nil]];

    [self.revealController showViewController:self.revealController.rightViewController];
    self.revealController.rightViewController.view.layer.zPosition = 1;
}

- (IBAction)notificationsTap:(id)sender {
    [self hideKeepyMenu];

    self.notificationsVC = [[NotificationsViewController alloc] initWithNibName:@"NotificationsViewController" bundle:nil];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.notificationsVC];

    // added by Ashvini
   // [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.viewController  presentViewController:navController animated:YES completion:nil];
}

- (void)openGallery {
    [self hideKeepyMenu];
    
    if ([KPPhotoLibrary authorizationIsDenied]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        return;
    }
    
    UIViewController *pvc = self.revealController;
    if (self.revealController.presentedViewController) {
        pvc = self.revealController.presentedViewController;
    }
    
    UIStoryboard* sb = [UIStoryboard storyboardWithName:@"AssetCollection" bundle:nil];
    UINavigationController* nc = [sb instantiateInitialViewController];
    PhotoGalleryViewController* vc = nc.viewControllers.firstObject;
    vc.addCamera = YES;
    vc.addNote = YES;
    
    [pvc presentViewController:nc animated:YES completion:nil];
}

//#if DEBUG
- (void)tapKeepyButton{
    
    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-keepy-btn", @"btn", nil]];
    
    
    
    
    //if (![GlobalUtils checkHasSpace])
    //    return;
    
    float d = -1;
    if (self.menuView != nil) {
        d = 1;
        
        
    }
    else {
        self.menuView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bottomPanel.frame.origin.y, self.view.frame.size.width, 85)];
        self.menuView.backgroundColor = [UIColor clearColor];
        
        [self.view insertSubview:self.menuView belowSubview:self.bottomPanel];
        
        UIView *tbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 85)];
        
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            tbar.backgroundColor = UIColorFromRGBWithAlpha(0x4d3c36, 0.9);
        }
        
        [self.menuView addSubview:tbar];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 107, 70);
        //        btn.backgroundColor = [UIColor blackColor];
        [btn setImage:[UIImage imageNamed:@"add-note"] forState:UIControlStateNormal];
        [btn setTitle:KPLocalizedString(@"add note") forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
        btn.titleEdgeInsets = UIEdgeInsetsMake(40, -25, 0, 0);
        btn.imageEdgeInsets = UIEdgeInsetsMake(-3, (107 - 33) / 2, 0, 0);
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(addNoteTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.menuView addSubview:btn];
        
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(214, 0, 107, 70);
        //          btn.backgroundColor = [UIColor blackColor];
        [btn setImage:[UIImage imageNamed:@"add-photo"] forState:UIControlStateNormal];
        [btn setTitle:KPLocalizedString(@"add photo") forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
        btn.titleEdgeInsets = UIEdgeInsetsMake(40, -35, 0, 0);
        btn.imageEdgeInsets = UIEdgeInsetsMake(-3, (107 - 34) / 2, 0, 0);
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(takePhotoTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.menuView addSubview:btn];
        
        
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(107, 0, 107, 70);
        //          btn.backgroundColor = [UIColor redColor];
        [btn setImage:[UIImage imageNamed:@"add-video"] forState:UIControlStateNormal];
        [btn setTitle:KPLocalizedString(@"add video") forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
        btn.titleEdgeInsets = UIEdgeInsetsMake(40, -24, 0, 0);
        btn.imageEdgeInsets = UIEdgeInsetsMake(-3, (107 - 34) / 2 + 5, 0, 0);
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(takeVideoTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.menuView addSubview:btn];
        
        
        self.transparentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.transparentButton.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view insertSubview:self.transparentButton belowSubview:self.menuView];
        [self.transparentButton addTarget:self action:@selector(hideKeepyMenu) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        if (d < 0) {
            self.addKeepyButton.transform = CGAffineTransformMakeRotation(M_PI * -45 / 180);
        } else {
            self.addKeepyButton.transform = CGAffineTransformIdentity;
        }
        
        CGRect aframe = self.menuView.frame;
        aframe.origin.y += d * 85;
        self.menuView.frame = aframe;
        
    }                completion:^(BOOL finished) {
        if (d > 0) {
            [self.menuView removeFromSuperview];
            self.menuView = nil;
            
            [self.transparentButton removeFromSuperview];
            self.transparentButton = nil;
            
            if (self.BGView != nil) {
                [self dismissonBoard];
            }
        }
    }];
    
    
}
//#endif

- (IBAction)keepyBtnTap:(UIButton *)sender {
//#if DEBUG
//    [self tapKeepyButton];
//#else
//    NSUInteger c1 = [[Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]] count];
    NSArray *myKids = [KPLocalKid fetchMine];

    if (myKids.count > 0) {
        [self openGallery];

    } else {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"You cannot Upload content as your profile has no kids.")];

    }

//    NSArray * kids = [[UserManager sharedInstance] myKids];
//    if (kids.count > 0)
//    [self openGallery];
//    else {
//        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"You cannot Upload content as your profile has no kids.")];
//    }
//#endif
}

- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self hideKeepyMenu];
}

- (void)addNoteTap:(id)sender {
    [self hideKeepyMenu];

    //if (![GlobalUtils checkHasSpace])
    //    return;

    NoteEditorViewController *avc = [[NoteEditorViewController alloc] initWithMode:1];

    //UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:avc];
    self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:avc];

    [self.revealController presentViewController:self.addPhotoNavController animated:YES completion:nil];

    //[self addMANotificationObservers];
}

- (void)MAImagePickerClosed {
    DDLogInfo(@"No Image Chosen");
    [self removeMANotificationObservers];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)MAImagePickerChosen:(NSNotification *)notification {
    [self dismissViewControllerAnimated:YES completion:nil];

    [self removeMANotificationObservers];

}

- (void)addMANotificationObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MAImagePickerClosed) name:@"MAIPCFail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MAImagePickerChosen:) name:@"MAIPCSuccess" object:nil];
}

- (void)removeMANotificationObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MAIPCFail" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MAIPCSuccess" object:nil];
}

#pragma mark - App Mode Changed Notification

- (void)appModeChanged:(NSNotification *)notification {
    BOOL editMode = [notification.object boolValue];

    //DDLogInfo(@"EditMode:%@", @(editMode));

    if (editMode == [GlobalUtils isEditMode]) {
        return;
    }

    [GlobalUtils setIsEditMode:editMode];

    /*
     float ah = self.view.frame.size.height;
     if (editMode)
     {
     //ah -= self.bottomPanel.frame.size.height;
     }
     else
     {
     self.container.frame = CGRectMake(0, 0, self.view.frame.size.width, ah);
     }
     */
    
    
//    int64_t delayInSeconds = 0.1;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
//        float ay = self.view.frame.size.height;
//        if (editMode) {
//            ay = self.view.frame.size.height - self.bottomPanel.frame.size.height;
//        }
//
//        [UIView animateWithDuration:0.6 animations:^{
//
//            CGRect aframe = self.bottomPanel.frame;
//            aframe.origin.y = ay;
//            self.bottomPanel.frame = aframe;
//
////            CGRect uframe = self.uploadProgressView.frame;
////            uframe.origin.y = aframe.origin.y - 35;
////            self.uploadProgressView.frame = uframe;
////
//        }                completion:^(BOOL finished) {
//            //if ([GlobalUtils isEditMode])
//            //    self.container.frame = CGRectMake(0, 0, self.view.frame.size.width, ah);
//        }];
//    });
}

- (void)setViewsLayout {
    /*
     float ah = self.view.frame.size.height;
     if ([GlobalUtils isEditMode])
     {
     //ah -= self.bottomPanel.frame.size.height;
     }
     
     [UIView animateWithDuration:0.5 animations:^{
     self.container.frame = CGRectMake(0, 0, self.view.frame.size.width, ah);
     } completion:^(BOOL finished) {
     
     }];*/
}

- (void)filterOn:(NSNotification *)notification {
    [UIView animateWithDuration:0.5 animations:^{
        self.kidsNavButton.alpha = 0;
        self.settingsNavButton.alpha = 0;
    }];
}

- (void)filterOff:(NSNotification *)notification {
    [UIView animateWithDuration:0.5 animations:^{
        self.kidsNavButton.alpha = 1;
        self.settingsNavButton.alpha = 1;
    }];
}

- (void)showKidsMenu:(NSNotification *)notification {
    KidsMenuViewController *kidsMenuViewController = (KidsMenuViewController *) self.revealController.leftViewController;
    kidsMenuViewController.specificKids = notification.object;
    [self.revealController showViewController:self.revealController.leftViewController];
}

- (void)notificationsRefreshed:(NSNotification *)notification {
  //  AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
 //   if (d.cleanTheUnreadNotificationNumber) {
//        NSArray *notifications = [KPNotification MR_findByAttribute:@"status" withValue:@(0)];
//        for (KPNotification *notification in notifications) {
//            notification.status = @(1);
//        }
//        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
//
//        [self.notificationsLabel setText:@""];
//        [self.notificationsButton setImage:[UIImage imageNamed: @"emptyNotifications"] forState:UIControlStateNormal];
//        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//        return;
  //  }
    
    
    // original code
//    NSArray *arr = [KPNotification MR_findByAttribute:@"status" withValue:@(0)];
//    if ([arr count] > 0) {
//        [self.notificationsLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[arr count]]];
//    }
//    NSString *imageName = ([arr count] == 0) ? @"emptyNotifications" : @"notifications";
//    [self.notificationsButton setImage:[UIImage imageNamed: imageName] forState:UIControlStateNormal];
//
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[arr count]];
    
    // New code by Ashvini
dispatch_async(dispatch_get_main_queue(), ^{
//    NSInteger notificationCount =  [[UIApplication sharedApplication] applicationIconBadgeNumber];
//    if (notificationCount > 0) {
//        NSArray *arr = [KPNotification MR_findByAttribute:@"status" withValue:@(0)];
//        if ([arr count] > 0) {
//            [self.notificationsLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[arr count]]];
//        }
//        NSString *imageName = ([arr count] == 0) ? @"emptyNotifications" : @"notifications";
//        [self.notificationsButton setImage:[UIImage imageNamed: imageName] forState:UIControlStateNormal];
//        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[arr count]];
//    } else {
//        [self.notificationsLabel setText:@""];
//        [self.notificationsButton setImage:[UIImage imageNamed: @"emptyNotifications"] forState:UIControlStateNormal];
//    }
//   });
//
    
    // Now notifications count handled here
    BOOL *isNewNoticationReceivedFlag = [[NSUserDefaults standardUserDefaults] boolForKey: @"isNewNoticationReceived"];
    if (isNewNoticationReceivedFlag == false) {
        NSArray *arr = [KPNotification MR_findByAttribute:@"status" withValue:@(0)];
        if ([arr count] > 0) {
            [self.notificationsLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[arr count]]];
        }
        NSString *imageName = ([arr count] == 0) ? @"emptyNotifications" : @"notifications";
        [self.notificationsButton setImage:[UIImage imageNamed: imageName] forState:UIControlStateNormal];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[arr count]];
    }
    else {
        [self.notificationsLabel setText:@""];
        [self.notificationsButton setImage:[UIImage imageNamed: @"emptyNotifications"] forState:UIControlStateNormal];
    }
    });
}

- (IBAction)addKidTap:(id)sender {
    EditKidViewController *avc = [[EditKidViewController alloc] initWithKid:nil];
    avc.delegate = self;

    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController pushViewController:avc animated:YES];
}

#pragma mark - EditKidDelegate

- (void)kidAdded:(Kid *)kid {
    [GlobalUtils fireTrigger:TRIGGER_TYPE_ADDKID];

    self.isLockedKeepyFeed = NO;

    [[NSNotificationCenter defaultCenter] postNotificationName:CHANGE_KID_NOTIFICATION object:kid];

    [self afterKidAdded:nil];
}

- (void)setIsLockedKeepyFeed:(BOOL)isLockedKeepyFeed {
    _isLockedKeepyFeed = isLockedKeepyFeed;
    //self.addKidButton.hidden = (!isLockedKeepyFeed);
    self.revealController.recognizesPanningOnFrontView = (!isLockedKeepyFeed);
    //self.itemsVC.isLockedKeepyFeed = isLockedKeepyFeed;
    //self.settingsNavButton.hidden = isLockedKeepyFeed;
    //self.notificationsButton.hidden = isLockedKeepyFeed;
    //self.notificationsLabel.hidden = isLockedKeepyFeed;
}

- (void)kidsRefreshedNotification:(NSNotification *)notification {

    NSArray *children = [KPLocalKid fetchAll];
    self.isLockedKeepyFeed = (children.count < 2); // TODO: account for additional Keepy child

}

- (void)hideKeepyMenu {
    if (self.BGView != nil) {
        [self dismissonBoard];
    }

    if (self.menuView != nil) {
        [self keepyBtnTap:nil];
    }
}


#pragma mark - GalleryPickerDelegate

- (void)galleryPickerController:(GalleryPickerViewController *)picker didFinishPickingMediaWithInfo:(NSArray *)info {
    [self showFinalView:info applyCrop:YES];
}

- (void)galleryPickerControllerDidCancel:(GalleryPickerViewController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
        self.addPhotoNavController = nil;
    }];
}

- (void)showFinalView:(NSArray *)infos applyCrop:(BOOL)applyCrop {
    NSMutableArray *newInfos = [[NSMutableArray alloc] init];
    for (NSDictionary *info in infos) {
        UIImage *aimage = [info valueForKey:UIImagePickerControllerOriginalImage];
        //DDLogInfo(@"image dims:%2f %2f", aimage.size.width, aimage.size.height);
        //        if (aimage.size.width>2000 || aimage.size.height>2000)
        //            aimage = [aimage scaleProportionalToSize:CGSizeMake(2000, 2000)];

        //DDLogInfo(@"image dims2:%2f %2f", aimage.size.width, aimage.size.height);
        aimage = [aimage fixOrientation];

        NSMutableDictionary *newInfo = info.mutableCopy;
        newInfo[UIImagePickerControllerOriginalImage] = aimage;

        [newInfos addObject:newInfo];

    }


#ifdef TEACHERS
    [self.addPhotoNavController dismissViewControllerAnimated:YES completion:^{
        self.addPhotoNavController = nil;
    }];
    
    if ([[[UserManager sharedInstance] myKids] count] == 0)
        return;
    
    
    
    [SVProgressHUD showWithStatus:KPLocalizedString(@"uploading")];
    
    //[self uploadImageBatch:0 withInfos:newInfos];
    
#else

    DDLogInfo(@"%@", infos);

    if ([infos count] == 1) {
        NSDictionary *info = [infos objectAtIndex:0];
        
        NSInteger mediaType = [[info objectForKey:UIImagePickerControllerMediaType] integerValue];
        UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        if (mediaType == PHAssetMediaTypeImage || (image && !mediaType)) {
            ImageEditorViewController *avc = [[ImageEditorViewController alloc] initWithImageInfo:[newInfos objectAtIndex:0] applyCrop:NO];

            if (self.addPhotoNavController == nil) {
                UIViewController *pvc = self.revealController;
                if (self.revealController.presentedViewController) {
                    pvc = self.revealController.presentedViewController;
                }

                self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:avc];
                self.addPhotoNavController.view.bounds = CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height);
                [pvc presentViewController:self.addPhotoNavController animated:YES completion:nil];

            }
            else {
                [self.addPhotoNavController pushViewController:avc animated:YES];
            }

            self.addPhotoNavController.view.tag = 99998;
        }

        else {
            if ([info objectForKey:UIImagePickerControllerReferenceURL] && mediaType == PHAssetMediaTypeVideo) {
                NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
                DKMoviePlayerViewController *playView = [[DKMoviePlayerViewController alloc] initWithNibName:@"DKMoviePlayerViewController" bundle:nil];

                //Tell to DKMoviePlayerViewController we came from gallery
                playView.uploadVideoMode = YES;

                [playView playMovieFile:assetURL];

                if (self.addPhotoNavController == nil) {
                    UIViewController *pvc = self.revealController;
                    if (self.revealController.presentedViewController) {
                        pvc = self.revealController.presentedViewController;
                    }

                    self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:playView];
                    self.addPhotoNavController.view.bounds = CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height);
                    [pvc presentViewController:self.addPhotoNavController animated:YES completion:nil];

                }
                else {
                    [self.addPhotoNavController pushViewController:playView animated:YES];
                }

                self.addPhotoNavController.view.tag = 99998;


            }
        }

    }


    else {
        ItemEditViewController *avc = [[ItemEditViewController alloc] initWithImagesInfos:newInfos];
        [self.addPhotoNavController pushViewController:avc animated:YES];


    }
    /*
    ItemEditViewController *avc = [[ItemEditViewController alloc] initWithImagesInfos:newInfos];
    [self.addPhotoNavController pushViewController:avc animated:YES];*/

#endif
}
///*
//-(void)uploadImageBatch:(int)i withInfos:(NSArray*)newInfos
//{
//    int kidId = ((Kid*)[[[UserManager sharedInstance] myKids] objectAtIndex:0]).kidId.intValue;
//    
//    UIImage *image = [[newInfos objectAtIndex:i] valueForKey:UIImagePickerControllerOriginalImage];
//    
//    if (self.globalServer == nil)
//        self.globalServer = [ServerComm instance];
//    
//    __weak ServerComm *sc = self.globalServer;
//    
//    [sc prepareAsset:^(id assetResult) {
//        int assetId = 0;
//        if ([[assetResult valueForKey:@"result"] valueForKey:@"id"] != [NSNull null])
//            assetId = [[[assetResult valueForKey:@"result"] valueForKey:@"id"] intValue];
//        
//        if (assetId > 0)
//        {
//            [[SDImageCache sharedImageCache] storeImage:image forKey:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[assetResult valueForKey:@"result"] valueForKey:@"hash"]]];
//            
//            __weak ServerComm *sc = self.globalServer;
//            ///add item
//            [sc addItem:kidId
//              withTitle:@""
//                andDate:[NSDate date]
//               andPlace:0 andCropRect:@""
//     andOriginalAssetId:assetId andRotation:0
//                 andSat:0
//                 andCon:0
//                 andBri:0
//             andFilterId:0
//       andAutoEnhanceOn:false
//            andItemType:0
//           andExtraData:@""
//       andShareWithFans:YES
//                andTags:nil
//       withSuccessBlock:^(id result) {
//                
//                if ([[result valueForKey:@"status"] intValue] == 0)
//                {
//                    [[SDImageCache sharedImageCache] storeImage:image forKey:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
//                    
//                    [[SDImageCache sharedImageCache] storeImage:image forKey:[NSString stringWithFormat:@"%@%@_iphone.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
//                    
//                    
//                    //save thumbnails
//                    UIImage *smallImage = [image imageScaledToFitSize:CGSizeMake(192, 192)];
//                    
//                    [[SDImageCache sharedImageCache] storeImage:smallImage forKey:[NSString stringWithFormat:@"%@%@_small.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
//                    
//                    UIImage *mediumImage = [image imageScaledToFitSize:CGSizeMake(600, 600)];
//                    [[SDImageCache sharedImageCache] storeImage:mediumImage forKey:[NSString stringWithFormat:@"%@%@_medium.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
//                    
//                    
//                    //Add new Item to local database
//                    NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
//                    [itemData setValue:[NSDate date] forKey:@"itemDate"];
//                    [itemData setValue:@"" forKey:@"cropRect"];
//                    [itemData setValue:@(kidId) forKey:@"kidId"];
//                    [itemData setValue:@(1) forKey:@"shareStatus"];
//                    [itemData setValue:@(0) forKey:@"sat"];
//                    [itemData setValue:@(0) forKey:@"con"];
//                    [itemData setValue:@(0) forKey:@"bri"];
//                    [itemData setValue:@(0) forKey:@"rotation"];
//                    
//                    Item *item = [[KPDataParser instance] addNewItem:[result valueForKey:@"result"] andData:itemData];
//                    
//                    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
//                    
//                    /*[[MobileAppTracker sharedManager] trackActionForEventIdOrName:@"Add Keepy"
//                     eventIsId:NO];*/
//                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION  object:item];
//                    
//                    //upload to S3
//                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//                    NSData *data = UIImageJPEGRepresentation(image, 0.8);
//                    
//                    __weak ServerComm *sc = self.globalServer;
//                    
//                    [sc uploadAsset:[[assetResult valueForKey:@"result"] valueForKey:@"hash"] withData:data andPolicy:[[assetResult valueForKey:@"result"] valueForKey:@"policy"]  andSignature:[[assetResult valueForKey:@"result"] valueForKey:@"policyHash"]  withSuccessBlock:^(id result)
//                     {
//                         __weak ServerComm *sc = self.globalServer;
//                         
//                         [sc commitAsset:assetId andFileSize:data.length withSuccessBlock:^(id result) {
//                             
//                             DDLogInfo(@"Uploaded...");
//                             
//                             if (i >= [newInfos count]-1)
//                             {
//                                 [SVProgressHUD dismiss];
//                             }
//                             else
//                             {
//                                 dispatch_async(dispatch_get_main_queue(), ^{
//                                     [self uploadImageBatch:i+1 withInfos:newInfos];
//                                 });
//                             }
//                         } andFailBlock:^(NSError *error) {
//                             
//                         }];
//                         
//                         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//                         
//                     } andFailBlock:^(NSError *error) {
//                         //[GlobalUtils updateAssetUpload:assetId totalBytesWritten:0 totalBytesExpectedToWrite:0];
//                         
//                         if (error != nil)
//                         {
//                             dispatch_async(dispatch_get_main_queue(), ^{
//                                 [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading original image")];
//                             });
//                         }
//                         
//                         if (i >= [newInfos count]-1)
//                         {
//                             [SVProgressHUD dismiss];
//                         }
//                         else
//                         {
//                             dispatch_async(dispatch_get_main_queue(), ^{
//                                 [self uploadImageBatch:i+1 withInfos:newInfos];
//                             });
//                         }
//                         
//                         
//                         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//                     } onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//                         float p = ((float)totalBytesWritten / (float)totalBytesExpectedToWrite);
//                         //[GlobalUtils updateAssetUpload:assetId totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
//                         DDLogInfo(@"uploading:%2f", 100 * p);
//                         
//                     }];
//                    
//                    
//                }
//                else if ([[result valueForKey:@"status"] intValue] == 2) //no more space...
//                {
//                    [SVProgressHUD dismiss];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [GlobalUtils showNoSpace];
//                    });
//                }
//                else
//                {
//                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error adding item")];
//                }
//                
//            } andFailBlock:^(NSError *error) {
//                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
//            }];
//            //////////////
//        }
//        else
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];
//            });
//        }
//    } andFailBlock:^(NSError *error) {
//        
//        double delayInSeconds = 1.0;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"we can\'t establish connection to keepy\'s servers. are you connected to the internet?")];
//        });
//    }];
//}
//*/
#pragma mark - NextStepDelegate

- (void)nextStepSelected:(NSUInteger)atype {
    switch (atype) {
        case 0:
            [self addNoteTap:nil];
            break;
        case 1:
            [self takePhotoTap:nil];
            break;
        case 2:
            [self addFan];
            break;
        case 3:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"kpyme://spreadlove"]];
            break;
        case 4:
            [DBClientsManager authorizeFromController:[UIApplication sharedApplication]
                                           controller:self.presentedViewController
                                              openURL:^(NSURL *url) {
                                                  [[UIApplication sharedApplication] openURL:url];
                                              }];
            break;
    }
}

- (void)nextStepClosed {
    [self dismissViewControllerAnimated:YES completion:^{
        self.nextStepVC = nil;
    }];
    /*
     [UIView animateWithDuration:0.3 animations:^{
     CGRect aframe = self.nextStepVC.view.frame;
     aframe.origin.y = 500;
     self.nextStepVC.view.frame = aframe;
     } completion:^(BOOL finished) {
     [self.nextStepVC.view removeFromSuperview];
     self.nextStepVC = nil;
     
     UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
     [self.view addGestureRecognizer:gesture];
     }];*/
}

- (void)addFan {
    FansViewController *avc = [[FansViewController alloc] initWithNibName:@"FansViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    //[navController setNavigationBarHidden:YES];
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.viewController.presentedViewController presentViewController:navController animated:YES completion:nil];
}

- (void)showNextStep {
    if (![[UserManager sharedInstance] isAfterRegistration] && [[[UserManager sharedInstance] myFans] count] > 0) {
        return;
    }


    FansViewController *avc = [[FansViewController alloc] initWithNibName:@"FansViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    navController.view.tag = 1;
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.viewController presentViewController:navController animated:YES completion:nil];

    /*
     EditFanViewController *avc = [[EditFanViewController alloc] initWithFan:nil andRelationMode:1];
     UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
     navController.view.tag = 1;
     AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
     [d.viewController presentViewController:navController animated:YES completion:nil];
     */

    /*
     if ([self.view.gestureRecognizers count] > 0)
     [self.view removeGestureRecognizer:[self.view.gestureRecognizers objectAtIndex:0]];
     
     self.nextStepVC = [[NextStepViewController alloc] initWithNibName:@"NextStepViewController" bundle:nil];
     self.nextStepVC.delegate = self;
     
     UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.nextStepVC];
     AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
     [d.viewController presentViewController:navController animated:YES completion:nil];
     */

}

- (void)addPhotoEnd:(NSNotification *)notification {
    self.addPhotoNavController = nil;
}

- (void)afterKidAdded:(NSNotification *)notification {
    [[UserManager sharedInstance] setIsAfterNewKid:YES];
}

- (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width {
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height;
}

- (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font thatFitsToSize:(CGSize)size {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        CGRect frame = [text boundingRectWithSize:size
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:@{NSFontAttributeName : font}
                                          context:nil];
        return frame.size;
    }
    else {
        return [text sizeWithFont:font thatFitsToSize:size];
    }
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if ([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                                                 [cleanString substringWithRange:NSMakeRange(0, 1)], [cleanString substringWithRange:NSMakeRange(0, 1)],
                                                 [cleanString substringWithRange:NSMakeRange(1, 1)], [cleanString substringWithRange:NSMakeRange(1, 1)],
                                                 [cleanString substringWithRange:NSMakeRange(2, 1)], [cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if ([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }

    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];

    float red = ((baseValue >> 24) & 0xFF) / 255.0f;
    float green = ((baseValue >> 16) & 0xFF) / 255.0f;
    float blue = ((baseValue >> 8) & 0xFF) / 255.0f;
    float alpha = ((baseValue >> 0) & 0xFF) / 255.0f;

    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    
}

- (void)shareViaTwitter {

    /*
    NSArray *filteredarray = [[[UserManager sharedInstance] dynamicMessages] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(source == %@)", @"trigger"]];
    NSDictionary *serverData = nil;
    
    if ([filteredarray count] > 0) {
        serverData = [filteredarray firstObject];
    }
    */

    // DDLogInfo(@"%@",serverData);
    NSDictionary *serverData = self.activatorData;

    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];
    NSArray *fb = (NSArray *) [campaign objectForKey:@"mediums"];

    NSArray *filteredarray2 = [fb filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", @"twitter"]];
    NSDictionary *sdata = nil;
    if ([filteredarray2 count] > 0) {

        sdata = [(NSDictionary *) [filteredarray2 firstObject] objectForKey:@"params"];

        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[sdata objectForKey:@"picture"]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {

            [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"preparing")];

        }                                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {

            [SVProgressHUD dismiss];


            if (finished) {
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    [tweetSheet setInitialText:[sdata objectForKey:@"status"]];


                    [tweetSheet addImage:image];

                    if ([sdata objectForKey:@"link"]) {
                        [tweetSheet addURL:[NSURL URLWithString:[sdata objectForKey:@"link"]]];
                    }

                    [[[Mixpanel sharedInstance] people] increment:@"activator-count-of-twitter" by:@(1)];


                    [self presentViewController:tweetSheet animated:YES completion:nil];
                }
                else {
                    UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Sorry", comment: @"")
                                                                                message:NSLocalizedString(@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup", comment: @"")
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                    [ac addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:ac animated:YES completion:nil];
                }
            }
        }];
    }
}

- (void)shareViaContacts {

    /*
     NSArray *filteredarray = [[[UserManager sharedInstance] dynamicMessages] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(source == %@)", @"trigger"]];
    NSDictionary *serverData = nil;
    
    if ([filteredarray count] > 0) {
        serverData = (NSDictionary*)[filteredarray firstObject];
    }
    */
    NSDictionary *serverData = self.activatorData;
    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];

    TFAContactsSelectorViewController *taf = [[TFAContactsSelectorViewController alloc] initWithMode:11 andRelationMode:0];
    taf.comeFromView = @"triggerd";
    taf.campaignId = [campaign objectForKey:@"id"];
    [self.navigationController pushViewController:taf animated:YES];

}

- (void)shareViaFacebook {

    /*
     NSArray *filteredarray = [[[UserManager sharedInstance] dynamicMessages] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(source == %@)", @"trigger"]];
    NSDictionary *serverData = nil;
    
    if ([filteredarray count] > 0) {
        serverData = [filteredarray firstObject];
    }*/

    NSDictionary *serverData = self.activatorData;

    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];
    NSArray *fb = (NSArray *) [campaign objectForKey:@"mediums"];

    NSArray *filteredarray2 = [fb filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", @"fb"]];
    if ([filteredarray2 count] > 0) {
        [[UserManager sharedInstance] appInviteFromViewController:self];
    }
}
@end

