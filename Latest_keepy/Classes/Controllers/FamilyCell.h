//
//  FamilyCell.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface FamilyCell : UITableViewCell

@property(nonatomic, strong) NSArray *family;

+ (CGFloat)height;

- (void)layout;

@end
