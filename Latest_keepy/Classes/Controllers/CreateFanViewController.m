//
//  CreateFanViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/20/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "CreateFanViewController.h"
#import "FanCell.h"
#import "InputCell.h"
#import "KPDataParser.h"
#import "AppDelegate.h"


@interface CreateFanViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UILabel *theTitle;
@property(nonatomic, weak) IBOutlet UIButton *saveButton;
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, weak) UITextField *passwordField;

@end

@implementation CreateFanViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"onboarding-create-fan-open"];


    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    self.theTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];

    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    [self.tableView registerNib:[UINib nibWithNibName:@"SpacerCell" bundle:nil] forCellReuseIdentifier:@"SpacerCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FanCell" bundle:nil] forCellReuseIdentifier:@"FanCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"InputCell" bundle:nil] forCellReuseIdentifier:@"InputCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *SpacerCellIdentifier = @"SpacerCell";
    static NSString *FanCellIdentifier = @"FanCell";
    static NSString *InputCellIdentifier = @"InputCell";
    if (indexPath.row == 0) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 1) {
        FanCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FanCellIdentifier];
        cell.fanCellType = [[self.fan objectForKey:@"fanRelationType"] intValue];
        cell.fan = self.fan;
        [cell layout];
        return cell;
    } else if (indexPath.row == 2) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 3) {
        InputCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
        cell.input.placeholder = @"create password";
        cell.input.secureTextEntry = YES;
        self.passwordField = cell.input;
        self.passwordField.delegate = self;
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 25.0f;
    } else if (indexPath.row == 1) {
        return [FanCell height];
    } else if (indexPath.row == 2) {
        return 20.0f;
    } else if (indexPath.row == 3) {
        return [InputCell height];
    }
    return 0.0f;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newText = [textField.text stringByAppendingString:string];
    if (newText.length < 6 || newText.length > 32) {
        self.saveButton.enabled = NO;
    } else {
        self.saveButton.enabled = YES;
    }
    if (newText.length > 32 && range.length == 0) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.text.length < 6 || textField.text.length > 32) {
        return NO;
    } else {
        [self save:nil];
        return NO;
    }
}

- (IBAction)save:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-create-fan-save-tap"];


    [self.passwordField endEditing:YES];

    NSString *facebookId = nil;
    if (self.facebookData) {
        facebookId = [self.facebookData objectForKey:@"id"];
    }

    NSString *otherTitle = nil;
    int relationType = [[self.fan objectForKey:@"fanRelationType"] intValue];

    NSString *fanEmail = self.email;
    int parentType = 2;
    
    switch (relationType) {
        case 0: {
            otherTitle = @"Mom";
            parentType = 0;
            break;
        }

        case 1: {
            otherTitle = @"Dad";
            parentType = 1;
            break;
        }

        case 2: {
            otherTitle = @"Grandma";
            parentType = 2;
            break;
        }

        case 3: {
            otherTitle = @"Grandpa";
            parentType = 3;
            break;
        }

        case 4: {
            otherTitle = @"Family";
            parentType = 4;
            break;
        }

        default:
        case 5: {
            otherTitle = @"Friend";
            parentType = 5;
            break;
        }

        case 6: {
            otherTitle = @"Aunt";
            parentType = 6;
            break;
        }

        case 7: {
            otherTitle = @"Uncle";
            break;
        }
    }

    [SVProgressHUD showWithStatus:KPLocalizedString(@"processing registration")];
    [[ServerComm instance] registerUser:self.email withName:[self.fan objectForKey:@"fanName"] andPassword:self.passwordField.text andParentType:parentType andOtherTxt:otherTitle andFBId:facebookId andFBAccessToken:self.facebookAccessToken withSuccessBlock:^(id result) {
        int rstatus = [[result valueForKey:@"status"] intValue];
        if (rstatus == 1) {
            NSMutableString *errorMsg = [[NSMutableString alloc] init];
            NSArray *errors = [[result valueForKey:@"error"] valueForKey:@"errors"];
            if (errors.count == 0) {
                [errorMsg appendString:@"registration error"];
            } else {
                [errorMsg appendString:@"Please fix the following:\n"];
            }
            for (NSString *errorStr in errors) {
                [errorMsg appendFormat:@"%@\n", errorStr];
            }

            [SVProgressHUD showErrorWithStatus:errorMsg];
            [[Mixpanel sharedInstance] track:@"Registration Error" properties:[NSDictionary dictionaryWithObjectsAndKeys:errorMsg, @"error", nil]];
            [[Mixpanel sharedInstance] track:@"onboarding-create-fan-register-user-invalid"];

        } else {
            
            
            // this right here is a fan, so the DB initialization process was triggered elsewhere
            
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            [delegate initializeDataStoreForEmail:fanEmail];


            [[Mixpanel sharedInstance] track:@"become-active"];
            [[Mixpanel sharedInstance] track:@"onboarding-create-fan-register-user-success"];

            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"sign-up", @"source", @"ok", @"result", nil]];

            [[UserManager sharedInstance] setIsAfterRegistration:YES];
            [[KPDataParser instance] parseKids:(NSArray *) self.fan[@"kids"] complete:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [GlobalUtils saveUser:[result objectForKey:@"result"]];
                    [SVProgressHUD dismiss];
                });
            }];
        }
    }                      andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error connecting to keepy\'s servers. please try again soon")];
        [[Mixpanel sharedInstance] track:@"onboarding-create-fan-register-user-error"];

    }];
}

@end
