//
//  ItemsViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/10/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <CoreText/CoreText.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <Mixpanel/Mixpanel.h>
#import "ItemsViewController.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "KPDataParser.h"
#import "KPLocalKid.h"
#import "ItemEditViewController.h"
#import "RecordViewController.h"
#import "FullScreenItemViewController.h"
#import "KPLocalItem.h"
#import "FilterViewController.h"
#import "FullScreenContainerViewController.h"
#import "NoteEditorViewController.h"
#import "TagsViewController.h"
#import "DKMoviePlayerViewController.h"
#import "DKMultyUploaderManger.h"
#import "DKVideoUploaderManager.h"
#import "DKTableViewUploderData.h"
#import "KPLocalAsset.h"
#import "KPLocalKidItem.h"
#import "KPLocalTag.h"
#import "KPLocalStory.h"
#import "KPSQLiteManager.h"
#import "AgeCalculator.h"
#import "CalendarCollectionViewController.h"
#import "CalendarCollectionViewLayout.h"
#import "UploadProgressView.h"
#import "PrintIOManager.h"
#import "Keepy-Swift.h"


@interface ItemsViewController () <UITableViewDataSource, UITableViewDelegate, /*ASTableViewDataSource,*/ UITableViewDelegate, ItemEditDelegate, RecordViewControllerDelegate, NoteEditorDelegate, UIDocumentInteractionControllerDelegate, TagViewDelegate, UIActionSheetDelegate>

@property(nonatomic, strong) NSMutableDictionary *images;
@property(nonatomic, strong) NSIndexPath *currentSelectedIndexPath;
@property(nonatomic, strong) UIImageView *kidImageView;
@property(nonatomic, strong) UILabel *kidNameLabel;
@property(nonatomic, strong) UILabel *pagesLabel;

@property(nonatomic, strong) FilterViewController *filterVC;
@property(nonatomic, strong) UIImageView *kidArrowImageView;
@property(nonatomic, strong) UIImageView *popArrowImageView;
@property(nonatomic) NSInteger listMode;
@property(nonatomic) NSInteger currentFilterType;
@property(nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property(nonatomic, strong) NSCache *imageCache;
@property(nonatomic, strong) FullScreenItemViewController *fullscreenVC;
@property(nonatomic, strong) NSMutableDictionary *downloaders;
@property(nonatomic, strong) UIButton *feedModeButton;
@property(nonatomic, strong) UIButton *galleryModeButton;

@property(nonatomic) float lastContentOffset;
@property(nonatomic, strong) UIImageView *kidImageBkg;
@property(nonatomic, strong) UIButton *kidsButton;
@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIDocumentInteractionController *interactionController;
@property(nonatomic, strong) UIButton *filterButton;
@property(nonatomic, strong) DKTableViewUploderData *dataSource;

@property(nonatomic, strong) UploadProgressView * uploadView;
@property(nonatomic, strong) NSTimer * uploadCheckTimer;
@property (strong, nonatomic) NSMutableDictionary *kidFilterCache;
@property (strong, nonatomic) CalendarCollectionViewController *calendarViewController;
// these are item IDs for which we do not reload the data if we get an addition notification
@property(strong, nonatomic) NSMutableSet *reloadIgnoringItemIDs;
@property(nonatomic, strong) KPLocalTag *filterTag;
@property BOOL processedAddedKidDuringOnboarding;

@property(nonatomic) CGFloat topContentInset;
@property(nonatomic) NSInteger noOfRowsReturned;

@end

@implementation ItemsViewController

BOOL const IS_NEW_ALBUM_ACTIVE = YES;

- (BOOL)isNewAlbumActive{
    return IS_NEW_ALBUM_ACTIVE;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    }
    return self;
}

- (void)videoUploadFinish {
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateUploadTrackerView];
    });
}

//

-(void) updateUploadTrackerView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UploadTracker sharedInstance].numberOfItemsBeingProcessed > 0)
        {
            _uploadView.alpha = 1;
            CGRect temp = _tableView.frame;
            temp.origin.y = _uploadView.frame.size.height;
            _tableView.frame = temp;
            
            temp = _calendarViewController.collectionView.frame;
            temp.origin.y = _uploadView.frame.size.height;
            _calendarViewController.collectionView.frame = temp;
            
            NSInteger currentRemainingCount = [UploadTracker sharedInstance].numberOfItemsBeingProcessed;
            
            NSInteger maxCount = [UploadTracker sharedInstance].highestNumberOfItemsThisBatch;
            if(maxCount > 0)
            {
                NSInteger numberProcessed = (maxCount - currentRemainingCount) + 1;
                _uploadView.progressView.progress = numberProcessed/(float)maxCount;
                _uploadView.label.text = [NSString stringWithFormat:@"Uploading %ld of %ld", (long)numberProcessed, (long)maxCount];
            }
            else
            {
                _uploadView.label.text = [NSString stringWithFormat:@"Uploading, %ld items remaining", (long)currentRemainingCount];
            }
            
            if ([UploadTracker sharedInstance].isRetrying)
            {
                _uploadView.label.text = [NSString stringWithFormat:@"%@ - Retrying..", _uploadView.label.text];
            }
            else if ([UploadTracker sharedInstance].hasFailures)
            {
                _uploadView.label.text = [NSString stringWithFormat:@"%@ Failed", _uploadView.label.text];
            }
        }
        else if (_uploadView.alpha == 1)
        {
            CGRect temp = _tableView.frame;
            temp.origin.y = 0;
            _tableView.frame = temp;
            
            temp = _calendarViewController.collectionView.frame;
            temp.origin.y = 0;
            _calendarViewController.collectionView.frame = temp;
            _uploadView.alpha = 0;
            //get data from server & reload views
            [self reloadData];
        }
    });
}

- (void)viewWillDisappear:(BOOL)animated {
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.currentSelectedIndexPath == nil) {
   // if (!self.tableView) {
        //videoUploadFinish
        _uploadCheckTimer = [NSTimer scheduledTimerWithTimeInterval:1    target:self selector:@selector(updateUploadTrackerView) userInfo:nil repeats:YES];
        self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
        self.imageDownloadingQueue.maxConcurrentOperationCount = 4; // many servers limit how many concurrent requests they'll accept from a device, so make sure to set this accordingly
        self.imageCache = [[NSCache alloc] init];
       // self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain];
        self.tableView.backgroundColor = UIColorFromRGB(0xE4E2E1);
        self.kidFilterCache = @{}.mutableCopy;
        
        if([self isNewAlbumActive]) {
            self.calendarViewController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,  self.calendarViewController.view.frame.size.height);
            [self addChildViewController:self.calendarViewController];
            self.calendarViewController.collectionView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
            [self.view addSubview:self.calendarViewController.collectionView];
        }
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.showsVerticalScrollIndicator = YES; // NO; TODO: restore vertical scroll indicator hiding
        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.tableView.scrollsToTop = YES;
        self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
        self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
        self.tableView.separatorColor = [UIColor colorWithRed:0.447 green:0.361 blue:0.325 alpha:1];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.tableView];
        
        UIView *pview = self.view;
        float dy = 0;
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 90)];
            
            if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
               v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 90 + 19)];
            }
            
            v.backgroundColor = UIColorFromRGB(0xe6e4e3);
            v.alpha = 0.5;
            [self.view addSubview:v];
            
            pview = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 90)];
           
            if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
               pview = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 90 + 19)];
            }
            
            [(UIToolbar *) pview setBarTintColor:UIColorFromRGB(0xe6e4e3)];
            [self.view addSubview:pview];
            dy = 20;
        }
        
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            self.kidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(21, dy + 11, 53, 53)];
        } else {
            if (([UIScreen mainScreen].bounds.size.height >= 812) && ([UIScreen mainScreen].bounds.size.width == 375)){
                 self.kidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(21, dy + 11 + 17, [UIScreen mainScreen].bounds.size.width * 0.1480, [UIScreen mainScreen].bounds.size.width * 0.1480)];
            }
            else if ([UIScreen mainScreen].bounds.size.width == 375) {
               self.kidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(21, dy + 4 , [UIScreen mainScreen].bounds.size.width * 0.1610, [UIScreen mainScreen].bounds.size.width * 0.1610)];
            }
            else if ([UIScreen mainScreen].bounds.size.width == 414){
                self.kidImageView = [[UIImageView alloc] initWithFrame:CGRectMake(21, dy + 4 , [UIScreen mainScreen].bounds.size.width * 0.1480, [UIScreen mainScreen].bounds.size.width * 0.1480)];
            }
        }
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(kidsTap:)];
        [self.kidImageView addGestureRecognizer:gesture];
        self.kidImageView.userInteractionEnabled = YES;
        
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            self.kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(79, dy + 15, 180, 20)];

        } else if ([UIScreen mainScreen].bounds.size.width == 414) {
            self.kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(79 * 1.1, dy + 15, 180, 20)];

        } else if ([UIScreen mainScreen].bounds.size.height >= 812) {
              self.kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(79 * 1.15, dy + 15 + 17, 180, 20)];
        }
        else {
            self.kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(79 * 1.15, dy + 15, 180, 20)];
        }
//        self.kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(79, dy + 15, 180, 20)];
        self.kidNameLabel.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
        self.kidNameLabel.textAlignment = NSTextAlignmentLeft;
        self.kidNameLabel.backgroundColor = [UIColor clearColor];
        self.kidNameLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
        [pview addSubview:self.kidNameLabel];
        
        
        self.kidArrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, dy + 29, 11, 16)];
        if ([UIScreen mainScreen].bounds.size.height >= 812) {
           self.kidArrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, dy + 29 + 17, 11, 16)];
        }
        self.kidArrowImageView.image = [UIImage imageNamed:@"left-arrow"];
        [self.kidArrowImageView addGestureRecognizer:gesture];
        [pview addSubview:self.kidArrowImageView];
        
        /*
         self.pagesLabel = [[UILabel alloc] initWithFrame:CGRectMake(79, dy + 33, 180, 20)];
         self.pagesLabel.textColor = [UIColor colorWithRed:0.722 green:0.663 blue:0.643 alpha:1];
         self.pagesLabel.textAlignment = NSTextAlignmentLeft;
         self.pagesLabel.backgroundColor = [UIColor clearColor];
         self.pagesLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
         [pview addSubview:self.pagesLabel];
         */
        self.filterButton = [UIButton buttonWithType:UIButtonTypeSystem];
        if ([UIScreen mainScreen].bounds.size.width > 320 ) {
            self.filterButton.frame = CGRectMake(79 * 1.15, dy + 37, 200, 20);
            
            if ([UIScreen mainScreen].bounds.size.height >= 812) {
               self.filterButton.frame = CGRectMake(79 * 1.15, dy + 37 + 19, 200, 20);
            }
            
            if ([UIScreen mainScreen].bounds.size.width == 414){
                self.filterButton.frame = CGRectMake(79 * 1.1, dy + 37, 200, 20);
            }
        } else {
            self.filterButton.frame = CGRectMake(79, dy + 37, 200, 20);
        }
        
        self.filterButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
        self.filterButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.filterButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.filterButton setTitle:KPLocalizedString(@"show all") forState:UIControlStateNormal];
        [self.filterButton setTitleColor:UIColorFromRGB(0x2EA3EB) forState:UIControlStateNormal];
        [self.filterButton addTarget:self action:@selector(filterTap:) forControlEvents:UIControlEventTouchUpInside];
        [pview addSubview:self.filterButton];
        
        self.kidsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.kidsButton.frame = CGRectMake(0, dy, self.view.frame.size.width - 130, 40);
        [self.kidsButton addTarget:self action:@selector(kidsTap:) forControlEvents:UIControlEventTouchUpInside];
        
        //    self.galleryModeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //    [self.galleryModeButton setImage:[UIImage imageNamed:@"calendar-mode-off"] forState:UIControlStateNormal];
        //    [self.galleryModeButton setImage:[UIImage imageNamed:@"calendar-mode-on"] forState:UIControlStateSelected];
        //    self.galleryModeButton.frame = CGRectMake(self.view.bounds.size.width - 41.5 - 4, dy + 10, 44, 44);
        //    [self.galleryModeButton addTarget:self action:@selector(galleryModeTap:) forControlEvents:UIControlEventTouchUpInside];
        //    self.galleryModeButton.hidden = YES;
        //    [pview addSubview:self.galleryModeButton];
        
        //    self.lineView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 41.5 - 2.5 - 4, dy + 10, 1, 44)];
        //    self.lineView.backgroundColor = UIColorFromRGB(0xc8beba);
        //    [pview addSubview:self.lineView];
        //    self.feedModeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //    [self.feedModeButton setImage:[UIImage imageNamed:@"feed-mode-off"] forState:UIControlStateNormal];
        //    [self.feedModeButton setImage:[UIImage imageNamed:@"feed-mode-on"] forState:UIControlStateSelected];
        //    self.feedModeButton.frame = CGRectMake(self.view.bounds.size.width - 41.5 - 5 - 44 - 4, dy + 10, 44, 44);
        //    [self.feedModeButton addTarget:self action:@selector(feedModeTap:) forControlEvents:UIControlEventTouchUpInside];
        //    self.feedModeButton.selected = YES;
        //    self.feedModeButton.hidden = YES;
        //    [pview addSubview:self.feedModeButton];
        
        _uploadView = [[UploadProgressView alloc] initWithFrame:CGRectMake(0, pview.frame.origin.y+pview.frame.size.height, self.view.bounds.size.width, 30)];
        _uploadView.alpha = 0;
        [pview addSubview:_uploadView];
        [self updateUploadTrackerView];
        
        // Aakash
        [pview addSubview:self.kidImageView];
        [self.view bringSubviewToFront:self.kidImageView];
        [self.view insertSubview:self.kidImageView aboveSubview:pview];
        self.kidImageView.layer.zPosition = 1;
        [pview addSubview:self.kidsButton];
        self.kidsButton.layer.zPosition = 1;
        [self.view insertSubview:self.kidsButton aboveSubview:pview];
        [self.view insertSubview:self.filterButton aboveSubview:pview];
        [self.view bringSubviewToFront:self.kidsButton];
        self.kid = [[UserManager sharedInstance] selectedKid];
        self.calendarViewController.selectedKid = self.kid;
        BOOL isFeedMode = ([[UserManager sharedInstance] feedMode] == 0);
        self.feedModeButton.selected = ([[UserManager sharedInstance] feedMode] == 0);
        self.galleryModeButton.selected = ([[UserManager sharedInstance] feedMode] == 1);
        
        if([self isNewAlbumActive]){
            self.listMode = 0;
        }else {
            self.listMode = [[UserManager sharedInstance] feedMode];
        }
        [self calculateDenormalizedItemInfo];
        if (isFeedMode) {
            [self reloadData:FILTER_TYPE_ALL withFilterValue:@""];
            if([self isNewAlbumActive]){
                self.tableView.hidden = NO;
                self.calendarViewController.collectionView.hidden = YES;
            }
        } else {
            [self reloadData:FILTER_TYPE_CONTACTS withFilterValue:@""];
            
            if([self isNewAlbumActive]){
                
                self.tableView.hidden = YES;
                self.calendarViewController.collectionView.hidden = NO;
            }else{
                
            }
        }
        self.isLockedKeepyFeed = ![KPLocalKid hasKids];
        //Fix in 1.95.3
        
        for (UIScrollView *view in self.view.subviews) {
            if ([view isKindOfClass:[UIScrollView class]]) {
                view.scrollsToTop = NO;
            }
        }
        self.tableView.scrollsToTop = YES;
        self.reloadIgnoringItemIDs = [NSMutableSet set];
        [self initializeItemContainerInsets];
   // }
    [self reloadData];
    if ([UIScreen mainScreen].bounds.size.width == 414) {
       [self.tableView reloadData];
    }
    [self.view layoutIfNeeded];
 }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UICollectionViewLayout *calendarLayout = [CalendarCollectionViewLayout new];
    self.calendarViewController = [[CalendarCollectionViewController alloc] initWithCollectionViewLayout:calendarLayout];
    
  //  self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) style:UITableViewStylePlain];
    
    self.isFirstTimeLoading = YES;
    
    if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
        //self.topContentInset = 45;
      //  self.topContentInset = 90;
        self.topContentInset = 90 - 25;
    } else if ([UIScreen mainScreen].bounds.size.height == 568) {
        self.topContentInset = 90;
    }
//    else if ([UIScreen mainScreen].bounds.size.height == 667) { // for 6 gold
//        self.topContentInset = 90;
//    }
    else {
        self.topContentInset = 70;
    }

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(didAddPhoto)
                   name:ADD_PHOTO_END_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(userDidLogin:)
                   name:USER_LOGIN_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(kidSelected:)
                   name:KID_SELECT_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(itemAdded:)
                   name:ITEM_ADDED_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(itemAddedFromServer:)
                   name:ITEM_ADDED_FROM_SERVER_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(itemDeleted:)
                   name:ITEM_DELETED_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(itemWasUpdated:)
                   name:ITEM_UPDATED_NOTIFICATION
                 object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateUploadTrackerView)
     name:@"UPLOAD_TRACKER_COUNT_CHANGED"
     object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kidsChanged:) name:KIDS_REFRESHED_NOTIFICATION object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoUploadFinish) name:@"videoUploadFinish" object:nil];
    
}

- (void)initializeItemContainerInsets{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    UIEdgeInsets tableInsets = UIEdgeInsetsZero;
    
    if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
        //insets = UIEdgeInsetsMake(90, 0, 60, 0);
        insets = UIEdgeInsetsMake(109, 0, 60, 0);
    } else {
        insets = UIEdgeInsetsMake(90, 0, 60, 0);
    }
    
    tableInsets = UIEdgeInsetsMake(self.topContentInset, 0, 60, 0);
    
    self.calendarViewController.collectionView.contentInset = insets;
    
    
    if ([UIScreen mainScreen].bounds.size.height == 568) {
        self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset - 20, 0, 60, 0);;
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topContentInset - 20, 0, 60, 0);;
    } else {
        self.tableView.contentInset = tableInsets;
        self.tableView.scrollIndicatorInsets = tableInsets;
    }
    [self.view layoutIfNeeded];
}

- (void)viewDidLayoutSubviews {
    self.tableView.scrollsToTop = YES;
  //  self.calendarViewController.collectionView.scrollsToTop = YES;
    [self initializeItemContainerInsets];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self.imageCache removeAllObjects];
}

- (void)reloadData {
    [self reloadData:self.currentFilterType withFilterValue:nil];
}

- (void)calculateDenormalizedItemInfo{
    
    // this query links the item to all its comments and its originalImage
    
    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    
//    NSString *itemsTable = [KPLocalItem getDatabaseTable];
//    NSString *commentsTable = [KPLocalComment getDatabaseTable];
//    NSString *assetsTable = [KPLocalAsset getDatabaseTable];
    
    
    NSString *selectorString = @"SELECT *, \
    items.id AS localItemID, items.serverID AS serverItemID, items.extraData AS extraItemData, \
    comments.id AS localCommentID, comments.serverID AS serverCommentID, comments.extraData AS extraCommentData, \
    originalImageAssets.id AS localOriginalImageAssetID, originalImageAssets.serverID AS serverOriginalImageAssetID, \
    simpleImageAssets.id AS localSimpleImageAssetID, simpleImageAssets.serverID AS serverSimpleImageAssetID \
    FROM items \
    LEFT JOIN comments ON items.id = comments.itemID \
    LEFT JOIN assets AS originalImageAssets ON items.originalImageAssetID = originalImageAssets.id \
    LEFT JOIN assets AS simpleImageAssets ON items.imageAssetID = simpleImageAssets.id \
    ";
    
    NSMutableDictionary *denormalizedDetails = @{}.mutableCopy;
    NSMutableDictionary *denormalizedItems = @{}.mutableCopy;
    NSMutableDictionary *denormalizedComments = @{}.mutableCopy;
    NSMutableDictionary *denormalizedOriginalImages = @{}.mutableCopy;
    
    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *results = [db executeQuery:selectorString];
        while([results next]){
            
            NSDictionary *currentResult = results.resultDictionary;
            
            // [denormalizedResults addObject:currentResult];
            
            NSNumber *localItemID = currentResult[@"localItemID"];
            NSNumber *serverItemID = currentResult[@"serverItemID"];
            id extraItemData = currentResult[@"extraItemData"];
            KPLocalItem *currentItem = denormalizedItems[localItemID];
            
            NSNumber *localCommentID = currentResult[@"localCommentID"];
            NSNumber *serverCommentID = currentResult[@"serverCommentID"];
            id extraCommentData = currentResult[@"extraCommentData"];
            KPLocalComment *currentComment = denormalizedComments[localCommentID];
            
            NSNumber *localOriginamImageAssetID = currentResult[@"localOriginalImageAssetID"];
            NSNumber *serverOriginamImageAssetID = currentResult[@"serverOriginalImageAssetID"];
            KPLocalAsset *currentOriginalImage = denormalizedOriginalImages[localOriginamImageAssetID];
            
            // first, let's configure the item
            if(!currentItem){
                NSMutableDictionary *itemInfo = currentResult.mutableCopy;
                itemInfo[@"id"] = localItemID;
                itemInfo[@"serverID"] = serverItemID;
                
                if(extraItemData){
                    itemInfo[@"extraData"] = extraItemData;
                }
                
                currentItem = [KPLocalItem initializeFromFetchResponse:itemInfo];
                denormalizedItems[localItemID] = currentItem;
            }
            
            // second, let's configure the original image
            if(!currentOriginalImage){
                NSMutableDictionary *assetInfo = currentResult.mutableCopy;
                assetInfo[@"id"] = localOriginamImageAssetID;
                assetInfo[@"serverID"] = serverOriginamImageAssetID;
                
                currentOriginalImage = [KPLocalAsset initializeFromFetchResponse:assetInfo];
                denormalizedOriginalImages[localOriginamImageAssetID] = currentOriginalImage;
            }
            
            // and lastly, let's do the comment
            if(!currentComment && (id)localCommentID != [NSNull null]){
                NSMutableDictionary *commentInfo = currentResult.mutableCopy;
                commentInfo[@"id"] = localCommentID;
                commentInfo[@"serverID"] = serverCommentID;
                
                if(extraCommentData){
                    commentInfo[@"extraData"] = extraCommentData;
                }
                
                currentComment = [KPLocalComment initializeFromFetchResponse:commentInfo];
                denormalizedComments[localCommentID] = currentComment;
            }
            
            // finally, let's do the linking
            if(!denormalizedDetails[localItemID]){
                denormalizedDetails[localItemID] = @{}.mutableCopy;
                denormalizedDetails[localItemID][@"item"] = currentItem;
                denormalizedDetails[localItemID][@"originalImage"] = currentOriginalImage;
                denormalizedDetails[localItemID][@"comments"] = @[].mutableCopy;
            }
            
            if(currentComment){
                [denormalizedDetails[localItemID][@"comments"] addObject:currentComment];
            }
        }
        [results close];
    }];
    
//    NSLog(@"finished denormalization");
    
    self.denormalizedItems = denormalizedDetails.copy;
    self.calendarViewController.denormalizedItems = self.denormalizedItems;
}

- (void)reloadData:(NSInteger)filterType withFilterValue:(NSString *)filterValue {

    @synchronized (self) {

        // [SVProgressHUD showWithStatus:@"reloading kid data"];
        // [self calculateDenormalizedItemInfo];
        
        if([self isNewAlbumActive]){
            // [self.calendarViewController reloadEverything];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadDataUnsynchronized:filterType filterValue:filterValue];
            [self.calendarViewController reloadEverything];
        });
        


        dispatch_async(dispatch_get_main_queue(), ^{
            // [SVProgressHUD dismiss];
//            if (self.currentSelectedIndexPath == nil) {
//                [self.tableView scrollToRowAtIndexPath: self.currentSelectedIndexPath atScrollPosition:UITableViewScrollPositionTop  animated:NO];
//            }
        });
        // [SVProgressHUD dismiss];

        
        
    }
    
}

- (void)reloadDataUnsynchronized:(NSInteger)filterType filterValue:(NSString *)filterValue {

    // [self.calendarViewController.collectionView reloadData];

    BOOL scrooltoTop = NO;

    if (self.kid.serverID == 88) {
            self.feedModeButton.selected = YES;
            self.galleryModeButton.selected = NO;
            filterType = FILTER_TYPE_ALL;
        }

    self.feedModeButton.hidden = (self.isLockedKeepyFeed || self.kid.serverID == 88);
    self.galleryModeButton.hidden = (self.isLockedKeepyFeed || self.kid.serverID == 88);
    self.lineView.hidden = (self.isLockedKeepyFeed || self.kid.serverID == 88);

    BOOL ascendingOrder = NO;
    self.images = [[NSMutableDictionary alloc] init];
    self.currentFilterType = filterType;

    //self.kidArrowImageView.hidden = (self.kid.kidId.intValue==88);

    if (filterValue == nil || filterType == FILTER_TYPE_ALL || filterType == FILTER_TYPE_CONTACTS) {
            filterValue = @"";
        }

    if (filterType == FILTER_TYPE_CONTACTS) {
            self.listMode = 1;//thumbnails
           // self.tableView.contentInset = UIEdgeInsetsMake(85, 0, 65, 0);
        
        // Modified by Ashvini
        self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
        
        // Current by Ashvini
//        self.calendarViewController.collectionView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
//        self.calendarViewController.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
//        self.calendarViewController.collectionView.scrollsToTop = YES;
        }
        else {
            self.listMode = 0;
           // self.tableView.contentInset = UIEdgeInsetsMake(95, 0, 65, 0);
            
            // Modified by Ashvini
                self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
            //
            
            // Current by Ashvini
//            self.calendarViewController.collectionView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
//            self.calendarViewController.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
//            self.calendarViewController.collectionView.scrollsToTop = YES;
        }

    NSString *sortByFields = @"createDate,itemId";

    NSPredicate *predicate = nil;
    if (self.kid == nil) {

            if (!self.isLockedKeepyFeed) {
                self.kidNameLabel.text = [NSString stringWithFormat:@"%@ %@", KPLocalizedString(@"everyone"), filterValue];
            }

            self.kidImageView.image = [UIImage imageNamed:@"everyone-album"];
            if (filterType == FILTER_TYPE_COMMENTS) {
                predicate = [NSPredicate predicateWithFormat:@"comments.@count>0 AND not kids contains %@", [[UserManager sharedInstance] keepyKid]];
            }
            else if (filterType == FILTER_TYPE_LIKES) {
                predicate = [NSPredicate predicateWithFormat:@"readAction=%d AND not kids contains %@", 2, [[UserManager sharedInstance] keepyKid]];
            }
            else if (filterType == FILTER_TYPE_YEAR) {
                ascendingOrder = YES;
                sortByFields = @"itemDate,itemId";

                NSCalendar *cal = [NSCalendar currentCalendar];
                NSDateComponents *dc = [NSDateComponents new];

                [dc setYear:[filterValue intValue]];
                [dc setDay:1];
                [dc setMonth:1];
                NSDate *startDate = [cal dateFromComponents:dc];

                [dc setDay:31];
                [dc setMonth:12];
                [dc setHour:23];
                [dc setMinute:59];
                NSDate *endDate = [cal dateFromComponents:dc];

                predicate = [NSPredicate predicateWithFormat:
                        @"itemDate >= %@ AND itemDate <= %@ AND not kids contains %@",
                        startDate,
                        endDate, [[UserManager sharedInstance] keepyKid]];
            }
            else {
                predicate = [NSPredicate predicateWithFormat:@"not kids contains %@ and itemId<>3875", [[UserManager sharedInstance] keepyKid]];
            }
        }
        else {

            KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:self.kid.imageAssetID];

            if (imageAsset == nil) {
                self.kidImageView.image = [GlobalUtils getKidDefaultImage:self.kid.gender];
            }
            else {
                __weak ItemsViewController *_s = self;
                [self.kidImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

                    _s.kidImageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];

                }];
            }


            self.kidNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.kid.name, filterValue];
            if (self.isLockedKeepyFeed) {
                self.kidNameLabel.text = @"";
            }//KPLocalizedString(@"Welcome to keepy!");

            if (filterType == FILTER_TYPE_COMMENTS) {
                predicate = [NSPredicate predicateWithFormat:@"comments.@count>0 AND kids contains %@", self.kid];
            }
            else if (filterType == FILTER_TYPE_LIKES) {
                predicate = [NSPredicate predicateWithFormat:@"readAction=%d AND kids contains %@", 2, self.kid];
            }
            else if (filterType == FILTER_TYPE_YEAR) {
                ascendingOrder = YES;
                sortByFields = @"itemDate,itemId";

                NSCalendar *cal = [NSCalendar currentCalendar];
                NSDateComponents *dc = [NSDateComponents new];

                [dc setYear:[filterValue intValue]];
                [dc setDay:1];
                [dc setMonth:1];
                NSDate *startDate = [cal dateFromComponents:dc];

                [dc setDay:31];
                [dc setMonth:12];
                [dc setHour:23];
                [dc setMinute:59];
                NSDate *endDate = [cal dateFromComponents:dc];

                predicate = [NSPredicate predicateWithFormat:
                        @"itemDate >= %@ AND itemDate <= %@ AND kids contains %@",
                        startDate,
                        endDate, self.kid];
            }
            else {
                // DDLogInfo(@"%@",self.kid);
                scrooltoTop = YES;
                predicate = [NSPredicate predicateWithFormat:@"kids contains %@", self.kid];
            }

        }

    //tags filter
    if (self.filterTag != nil) {
        NSPredicate *tagPredicate = [NSPredicate predicateWithFormat:@"tags contains %@", self.filterTag];
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate, tagPredicate]];
    }

    NSString *itemDescStr = KPLocalizedString(@"keepy");
    if (self.listMode == 1) {
        sortByFields = @"itemDate,itemId";
        ascendingOrder = NO;
    }

    // DDLogInfo(@"Sort by field: %@, with Predictade : %@",sortByFields,predicate);


    // self.items = [Item MR_findAllInContext:[NSManagedObjectContext MR_contextForCurrentThread]];

    // TODO: adapt item finding mechanism for FMDB and ARM
    // self.items = [Item MR_findAllSortedBy:sortByFields ascending:ascendingOrder withPredicate:predicate inContext:[NSManagedObjectContext MR_contextForCurrentThread]];

    // self.items = @[];
    // NSArray *itemIDs = [KPLocalKidItem fetchItemIDsForKid:self.kid];
    NSArray *itemIDs = [KPLocalKidItem fetchItemIDsForKid:self.kid withTag:self.filterTag];
    NSMutableArray *items = @[].mutableCopy;
    // NSArray *items = [KPLocalItem idsToObjects:itemIDs];

    if (!self.kid)
    {
        NSMutableArray *kids = [KPLocalKid fetchAll].mutableCopy;
        
        for (KPLocalKid *currentKid in kids.copy) {
            // we don't want Keepy's tutorial images in the everyone feed
            if (currentKid.serverID == [KPLocalKid getKeepyKidID]) {
                [kids removeObject:currentKid];
                break;
            }
        }
        
        itemIDs = [KPLocalKidItem fetchItemIDsForKids:kids withTag:self.filterTag];
    }
    
    // items = [KPLocalItem idsToObjects:itemIDs];
    for(NSNumber *itemID in itemIDs){
        KPLocalObject *currentItem = self.denormalizedItems[itemID][@"item"];
        if(currentItem){
            [items addObject:currentItem];
        }
    }
    NSLog(@"There are %lu items in items array", (unsigned long)[items count]);
    NSSortDescriptor *dateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"itemDate" ascending:NO];
    NSSortDescriptor *serverIDSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"serverID" ascending:NO];
    NSSortDescriptor *localIDSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:NO];

    // sort the items
    self.items = [items sortedArrayUsingDescriptors:@[serverIDSortDescriptor, dateSortDescriptor, localIDSortDescriptor]];
    
    
    
    
    
    
    if (!self.kid) {
        
        NSMutableArray *kids = [KPLocalKid fetchMine].mutableCopy;
    
        for (KPLocalKid *currentKid in kids.copy) {
            // we don't want Keepy's tutorial images in the everyone feed
            if (currentKid.serverID == [KPLocalKid getKeepyKidID]) {
                [kids removeObject:currentKid];
                break;
            }
        }
    
        itemIDs = [KPLocalKidItem fetchItemIDsForKids:kids withTag:self.filterTag];
    
        // items = [KPLocalItem idsToObjects:itemIDs];
        NSMutableArray *mineItems = [NSMutableArray new];
        
        for(NSNumber *itemID in itemIDs){
            KPLocalObject *currentItem = self.denormalizedItems[itemID][@"item"];
            if(currentItem){
                [mineItems addObject:currentItem];
            }
        }
    
        NSSortDescriptor *dateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"itemDate" ascending:NO];
        NSSortDescriptor *serverIDSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"serverID" ascending:NO];
        NSSortDescriptor *localIDSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:NO];
    
        // sort the items
        mineItems = [mineItems sortedArrayUsingDescriptors:@[serverIDSortDescriptor, dateSortDescriptor, localIDSortDescriptor]];
        
        
        NSMutableArray *itemsForPhotoBook = [NSMutableArray arrayWithArray:mineItems];
        
        
        NSSet *printedImagesSet = [[NSUserDefaults standardUserDefaults] valueForKey:PrintedImagesDefaulsKey];
        if (printedImagesSet != nil) {
            NSMutableArray *discardedItems = [NSMutableArray array];
            for (KPLocalItem *item in itemsForPhotoBook) {
                KPLocalAsset *originalImage = self.denormalizedItems[@(item.identifier)][@"originalImage"];
                NSString *imageString = [NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, originalImage.urlHash];
                
                if ([printedImagesSet containsObject:imageString]) {
                    [discardedItems addObject:item];
                }
            }
            [itemsForPhotoBook removeObjectsInArray:discardedItems];
        }
    
        if (itemsForPhotoBook.count >= 40) {
            [PrintIOManager setItems:itemsForPhotoBook];
            [PrintIOManager setDenormalizedItems:self.denormalizedItems];
            
//            [PrintIOManager showBookReadyNotification];
//            [PrintIOManager showBookReadyDialogInViewController: self];
        }
    }


    
    NSUInteger itemsCount = [self.items count];


    ////---------ALGORITHM TO FILTER MULTI TAGGIN

    NSArray *originalArray = self.items; // original array of objects with duplicates
    NSMutableArray *uniqueArray = [NSMutableArray array];
    NSMutableSet *names = [NSMutableSet set];
    for (KPLocalItem *obj in originalArray) {
            NSNumber *destinationName = @(obj.serverID);
            if (![names containsObject:destinationName]) {
                [uniqueArray addObject:obj];
                [names addObject:destinationName];
            }
        }

    self.items = uniqueArray;
    //------------------------------------------
    
    self.calendarViewController.unsortedItems = self.items;
    
    if (self.listMode == 1) {
            NSMutableArray *newItems = [[NSMutableArray alloc] init];
            NSMutableDictionary *ages = [[NSMutableDictionary alloc] init];
            for (int i = 0; i < [self.items count]; i++) {
                KPLocalItem *item = (KPLocalItem *) self.items[i];
                //DLog(@">>>>>>>>> item: %@ cropRect %@", item.itemId,  item.cropRect);

                NSUInteger itemYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:[NSDate dateWithTimeIntervalSince1970:item.itemDate]].year;

                NSInteger age = itemYear; //if everyone - by year

                if (self.kid != nil && self.kid.birthdate > 0)
                {
                    if (self.kid.birthdate > 0)
                    {
                        age = [AgeCalculator calculateAgeForBirthday:[NSDate dateWithTimeIntervalSince1970:self.kid.birthdate] atDate:[NSDate dateWithTimeIntervalSince1970:item.itemDate]];
                    }
                }

                NSMutableArray *arr = (NSMutableArray *) ages[[NSString stringWithFormat:@"%lu", (unsigned long) age]];

                // DDLogInfo(@"%@",arr);

                if (arr == nil) {
                    arr = [[NSMutableArray alloc] init];
                    ages[[NSString stringWithFormat:@"%lu", (unsigned long) age]] = arr;
                }
                //  DDLogInfo(@"%@",[ages allKeys]);

                /*BEFOR THE FIX
                    [arr addObject:item];
                */

                //--AFTER HAGGAI FIX-----------------


                if ([self checkIfItemInProcces:item]) {
                    DDLogInfo(@"ITEM IN PROCCESS REMOVE FROM GRID VIEW");
                }
                else {[arr addObject:item];}
                //-------------------------------------

            }

            NSArray *sortedKeys = [[ages allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                int i1 = [obj1 intValue];
                int i2 = [obj2 intValue];

                if (i2 == -1) {
                    return NSOrderedDescending;
                } else if (i1 == -1) {
                    return NSOrderedAscending;
                }

                return [@(i2) compare:@(i1)];
            }];


            for (NSString *key in sortedKeys) {
                [newItems addObject:(NSArray *) ages[key]];
            }

            self.items = newItems;
        }

    if (itemsCount != 1) {
            itemDescStr = KPLocalizedString(@"keepies");
        }

    //self.pagesLabel.text = [NSString stringWithFormat:@"%d %@",itemsCount, itemDescStr];
    NSString *s = KPLocalizedString(@"show all");
    if (self.filterTag != nil)
    {
        s = [NSString stringWithFormat:@"%@", self.filterTag.name];
    }

    [self.filterButton setTitle:[NSString stringWithFormat:@"%@ (%lu)", s, (unsigned long) itemsCount] forState:UIControlStateNormal];


    /*
    NSMutableArray *newItems2 = [[NSMutableArray alloc] init];
    int kids = 0;
    int j =0;
    for (int i = 0; i < self.items.count; i++) {

        Item *item  = (Item*)[self.items objectAtIndex:i];
        j = (i+1) < self.items.count-1 ? i+1 :self.items.count-1;
        Item *item2 = (Item*)[self.items objectAtIndex:j];
        kids = item.kids.count;
        DDLogInfo(@"%d",j);

        if (kids == 1) {
            [newItems2 addObject:item];
        }

        else if (kids > 1 && item2.kids.count == 1){
            [newItems2 addObject:item];
        }
    }
    self.items = newItems2;*/

//    [self.tableView reloadData];
    
       dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.view layoutIfNeeded];
//        if (self.currentSelectedIndexPath != nil) {
//            [self.tableView scrollToRowAtIndexPath: self.currentSelectedIndexPath atScrollPosition:UITableViewScrollPositionTop  animated:NO];
//
//        }
    });

    if ([self.items count] > 0 && scrooltoTop) {
            //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}


- (BOOL)checkIfItemInProcces:(KPLocalItem *)item {

    //TODO: Check if this changes are OK---------------

    //Check the item if is in proccess
    if (item.itemType == 4) {

        NSArray *arr = [[DKMultyUploaderManger sharedClient] allVideosUploadingProccess];

        for (DKVideoUploaderManager *obj in arr) {

            NSString *a = obj.myID;
            NSString *b = @(item.serverID).stringValue;

            if ([a isEqualToString:b]) {//IF the item in proccess flag it
                return YES;
            }
        }
    }
    return NO;
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // return 200;
    
    
//    BOOL isAfterLogin = [[UserManager sharedInstance] isAfterLogin];
    
    
    if (self.listMode == 1) {
        return 75;
    } else {

        if(indexPath.row >= self.items.count){
            return 0; // something is wrong here, but well
            // TODO: find out why this thing is ever reached
        }
        
        KPLocalItem *item = self.items[indexPath.row];
        
        //check if item is uploading if yes height == 0.1
        //rlons
        
        
        // KPLocalAsset *originalImage = [KPLocalAsset fetchByLocalID:item.originalImageAssetID];
        // NSArray *comments = [KPLocalComment fetchCommentsForItem:item];
        
        KPLocalAsset *originalImage = self.denormalizedItems[@(item.identifier)][@"originalImage"];
        NSArray *comments = self.denormalizedItems[@(item.identifier)][@"comments"];

        
        BOOL isUploading = [[UploadTracker sharedInstance] itemIsPartOfUploadBatch:[NSString stringWithFormat:@"%ld",(long)item.imageAssetID]];
        if (isUploading)
        {
            return 0.1;
//            return 30;//for debugging
        }

        if (item.itemType == 4 && (originalImage.assetStatus == 0 || originalImage.assetStatus == 1))
        {
        // if (item.itemType == 4 && (/*originalImage.assetStatus == 0 || */originalImage.assetStatus == 1)) {
            
            return 51.0f;
        } else {
            float commentsHeight = 0;
            float cnt = comments.count;

            if (item.storyID > 0) {
                cnt += 1;
            }

            if (cnt > 8) {
                cnt = 8;
            }
            
            // added by Ashvini
//            if (([UIScreen mainScreen].bounds.size.width == 414) && ([UIScreen mainScreen].bounds.size.height == 736)) {
//                if (cnt > 10) {
//                    cnt = 10;
//                }
//            }
            //

            if (cnt > 0) {
                float c = floorf((self.view.bounds.size.width - 20.0) / 76.0);
                if (([UIScreen mainScreen].bounds.size.width == 320) && ([UIScreen mainScreen].bounds.size.height == 568)) {
                   c = ceil((self.view.bounds.size.width - 20.0) / 76.0);
                }
                
                int r = ceil((float) cnt / (float) c);
                commentsHeight = r * 76;
            }

            if (item.itemType == 2) //quote
            {

                NSDictionary *stringAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16]};

                CGSize textSize = [item.extraData boundingRectWithSize:CGSizeMake(280, 250)
                                                               options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin
                                                            attributes:stringAttributes context:nil].size;

                //CGSize textSize = [item.extraData sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:16] thatFitsToSize:CGSizeMake(280, 250) lineBreakMode:NSLineBreakByWordWrapping];

                float ah = textSize.height + 50;

                if (textSize.height < 180) {
                    ah += 60;
                }

                if (ah < 150) {
                    ah = 150;
                }

                return ah + 65 + commentsHeight;
            }
            else {
                return self.view.bounds.size.width + 35 + commentsHeight;
            }
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.listMode == 1) {
        return [self.items count];
    } else {
        return 1;
    }
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (self.listMode == 1) {
        float w = self.view.bounds.size.width;
        int maxCount = (w - 20) / 73;

        NSArray *arr = (NSArray *) self.items[section];

        //  DDLogInfo(@"%@",arr);
        NSUInteger r = ([arr count] / maxCount);
        if ([arr count] % maxCount != 0) {
            r++;
        }
        self.noOfRowsReturned = r;
        return r;
    }
    else {
        self.noOfRowsReturned = [self.items count];
        return [self.items count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.listMode == 1) {
        return 40;
    }
    else {
//        if ([[DKMultyUploaderManger sharedClient]allVideosUploadingProccess].count>0){
//        return (34*[[DKMultyUploaderManger sharedClient]allVideosUploadingProccess].count);
//    }
//
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.listMode == 1) {
        float w = self.view.bounds.size.width;

        UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];

        UIView *bkgView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, w - 20, 40)];
        bkgView.backgroundColor = UIColorFromRGB(0xE4E2E1);
        bkgView.alpha = 0.9;
        [aview addSubview:bkgView];

        /*
         UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, w-20, 40)];
         bar.barTintColor = UIColorFromRGB(0xE4E2E1);
         [bar setShadowImage:nil forToolbarPosition:UIBarPositionAny];
         [bkgView addSubview:bar];
         */
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, w - 20, 40)];
        lbl.textColor = UIColorFromRGB(0x523c37);
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:14];
        [bkgView addSubview:lbl];

        NSArray *arr = (NSArray *) self.items[section];
        KPLocalItem *item = arr[0];
        NSUInteger itemYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:[NSDate dateWithTimeIntervalSince1970:item.itemDate]].year;
        NSUInteger kidYear = itemYear + 1;

        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:item];
        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

        KPLocalKid *kid = itemKids[0];
        if (kid.birthdate > 0) {
            kidYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:[NSDate dateWithTimeIntervalSince1970:kid.birthdate]].year;
        }

        if (self.kid != nil && self.kid.birthdate > 0) {
            //int age = itemYear - kidYear;
            NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                    components:NSCalendarUnitYear
                      fromDate:
                              [NSDate dateWithTimeIntervalSince1970:self.kid.birthdate]
                        toDate:[NSDate dateWithTimeIntervalSince1970:item.itemDate]
                       options:0];
            NSUInteger age = [ageComponents year];

            if (age == -1) {
                lbl.text = KPLocalizedString(@"unknown age");
            } else {
                lbl.text = [NSString stringWithFormat:@"%@ %lu", KPLocalizedString(@"age"), (unsigned long) age];
            }
        }
        else {
            lbl.text = [NSString stringWithFormat:@"%lu", (unsigned long) itemYear];
        }

        return aview;
    }
//    } else if ([[DKMultyUploaderManger sharedClient]allVideosUploadingProccess].count>0){
//        
//        UITableView *uploaderTV = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, 34*[[[DKMultyUploaderManger sharedClient]allVideosUploadingProccess]count])];
//
//        if (!self.dataSource) {
//            self.dataSource = [[DKTableViewUploderData alloc]init];
//        }
//        uploaderTV.backgroundColor = [UIColor clearColor];
//        
//        uploaderTV.dataSource = self.dataSource;
//        uploaderTV.delegate = self.dataSource;
//        [uploaderTV reloadData];
//        
//        return uploaderTV;
//    }
    return nil;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {

    //if (self.listMode == 0)
    return nil;

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i = 0; i < [self.items count]; i++) {
        NSMutableArray *sarr = (NSMutableArray *) self.items[i];
        Item *item = (Item *) sarr[0];

        int itemYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:item.itemDate].year;
        int kidYear = itemYear + 1;
        Kid *kid = [item.kids anyObject];
        if (kid.birthdate != nil) {
            kidYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:kid.birthdate].year;
        }

        if (self.kid != nil && self.kid.birthdate > 0) {
            //int age = itemYear - kidYear;
            NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                    components:NSCalendarUnitYear
                      fromDate:[NSDate dateWithTimeIntervalSince1970:self.kid.birthdate]
                        toDate:item.itemDate
                       options:0];
            int age = [ageComponents year];

            if (age == -1) {
                [arr addObject:@"?"];
            } else {
                [arr addObject:[NSString stringWithFormat:@"%d", age]];
            }
        }
        else {
            [arr addObject:[NSString stringWithFormat:@"%d", itemYear]];
        }

    }

    return arr;
}


// tell our table what kind of cell to use and its title for the given row
// - (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isVideoTYPE = NO;

    if (self.listMode == 0) {
      //  KPLocalItem *item = (KPLocalItem *) self.items[indexPath.row];
        
      if(self.items.count > indexPath.row) {
        KPLocalItem *item = (KPLocalItem *) self.items[indexPath.row];
        
       // BOOL isUploading = [[UploadTracker sharedInstance] itemIsPartOfUploadBatch:[NSString stringWithFormat:@"%ld",(long)item.imageAssetID]];
        
        if (item != nil) {
          // NSString *inStr = [NSString stringWithFormat: @"%ld", (long)item.imageAssetID];
            
            // Ashvini
            long intvalue = (long)item.imageAssetID;
            NSString *inStr = [NSString stringWithFormat: @"%ld",intvalue];
            
            BOOL isUploading = [[UploadTracker sharedInstance] itemIsPartOfUploadBatch:inStr];
            
            if (isUploading)
            {
               UITableViewCell * cell = [[UITableViewCell alloc]init];
               cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, cell.frame.size.height);
               //for debugging
               //cell.backgroundColor = [UIColor redColor];
              //cell.contentView.layer.borderColor = [UIColor blackColor].CGColor;
               //cell.contentView.layer.borderWidth = 1;
               return cell;
            }
        }
          
        if (item.itemType != 0) {
            isVideoTYPE = YES;
        }

        KPLocalAsset *originalImage = self.denormalizedItems[@(item.identifier)][@"originalImage"];

//        BOOL isUploading = [[UploadTracker sharedInstance] containsKey:[NSString stringWithFormat:@"%ld",(long)item.imageAssetID]];
//        imageAssetID
        
//        NSLog(@"%@", [[UploadTracker sharedInstance] itemsBeingUploaded]);

        if (item.itemType == 4 && (originalImage.assetStatus == 0 || originalImage.assetStatus == 1))
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemUploadingCell"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ItemUploadingCell"];
            }
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [self configureUploaderCell:cell withIndexPath:indexPath];
            
            return cell;
        }
      }
    }

    NSString *cellIdentifier = @"ItemCell";
    if (self.listMode == 1) {
        cellIdentifier = @"ThumbCell";
    } else if (self.listMode == 2) {
        cellIdentifier = @"CommentCell";
    }

    if (self.listMode == 0) {
        // added below condition by Ashvini
        if(self.items.count > indexPath.row) {
            KPLocalItem *item = (KPLocalItem *) self.items[indexPath.row];
            if (item.itemType == 2) {//quote
                cellIdentifier = @"QuoteCell";
            }
        }
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width);
        cell.contentView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width);
        float w = self.view.bounds.size.width;

        if (self.listMode == 1) {
            float ax = 10;
            int maxCount = (w - 20) / 73;
            for (int i = 0; i < maxCount; i++) {
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(ax, 0, 73, 73)];

                //img.contentMode = UIViewContentModeScaleAspectFill;
                //img.clipsToBounds = YES;
                img.userInteractionEnabled = YES;
                UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(thumbTap:)];
                gesture.cancelsTouchesInView = NO;
                [img addGestureRecognizer:gesture];
                [img addSubview:[[UIControl alloc] init]];
                [cell.contentView addSubview:img];

                UIImageView *footerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 56, 73, 17)];
                footerImageView.image = [UIImage imageNamed:@"new-thumb-footer-video"];
                footerImageView.hidden = YES;
                [img addSubview:footerImageView];

                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(18, 55, 50, 17)];
                lbl.textColor = [UIColor whiteColor];
                lbl.textAlignment = NSTextAlignmentRight;
                lbl.backgroundColor = [UIColor clearColor];
                lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
                lbl.text = @"";
                [img addSubview:lbl];

                UIImageView *playView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play-video-small-gallery"]];

                playView.frame = CGRectMake(21, 21, 31, 31);
                playView.hidden = YES;
                [img addSubview:playView];

                ax += 75;
            }
        }
        else {
            //Image
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, w - 20, w - 20)];
            img.userInteractionEnabled = YES;
            img.contentMode = UIViewContentModeScaleAspectFill;
            //img.clipsToBounds = YES;
            [cell.contentView addSubview:img];
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(thumbTap:)];
            gesture.cancelsTouchesInView = NO;
            [img addGestureRecognizer:gesture];

            //Kid Image Background
            UIImageView *kidImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 53, 53)];
            kidImg.image = [UIImage imageNamed:@"kidPicBg"];
            [img addSubview:kidImg];

            //Kid Image
            UIImageView *kidImg2 = [[UIImageView alloc] initWithFrame:CGRectMake(12, 12, 49, 49)];
            kidImg2.contentMode = UIViewContentModeScaleAspectFill;
            kidImg2.clipsToBounds = YES;
            kidImg2.userInteractionEnabled = YES;
            
//            this code was used for to filter by children
//            gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(kidTap:)];
//            gesture.cancelsTouchesInView = NO;
//            [kidImg2 addGestureRecognizer:gesture];

            [img addSubview:kidImg2];

            [img addSubview:[[UIControl alloc] init]];

            //Mic button 1
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setImage:[UIImage imageNamed:@"record-VO-button"] forState:UIControlStateNormal];
            //[btn setImage:[UIImage imageNamed:@"record-voiceover_tapped"] forState:UIControlStateHighlighted];
            btn.frame = CGRectMake(w - 15 - 42, 44, 44, 44);
            [btn addTarget:self action:@selector(recordTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];

            //Edit button 2
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setImage:[UIImage imageNamed:@"edit-story-button"] forState:UIControlStateNormal];
            //[btn setImage:[UIImage imageNamed:@"edit-info_tapped"] forState:UIControlStateHighlighted];
            btn.frame = CGRectMake(w - 15 - 42, 4, 44, 44);
            [btn addTarget:self action:@selector(editTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];

            //Text 3
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 215, w - 30, 20)];
            lbl.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
            lbl.textAlignment = NSTextAlignmentLeft;
            lbl.backgroundColor = [UIColor clearColor];
            lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
            [cell.contentView addSubview:lbl];

            //Comments View 4
            UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(10, 10, w - 20, 140)];
            [cell.contentView addSubview:aview];

            //Likes button
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.hidden = YES;
            [cell.contentView addSubview:btn];
            
            CGFloat totWidth = [UIScreen mainScreen].bounds.size.width - 20;
            CGFloat buyBtnWidth = 44;
            CGFloat btnWidth = (totWidth - buyBtnWidth) / 2;
            //Share button 6
//            btn = [UIButton buttonWithType:UIButtonTypeCustom];
//            btn.tag = 300;
//            [btn setImage:[UIImage imageNamed:@"feed-image-share-icon"] forState:UIControlStateNormal];
//            btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
//            btn.frame = CGRectMake(138, w - 64, 128, 44);
//            btn.alpha = 0.9;
//            btn.backgroundColor = [UIColor whiteColor];
//            [btn setTitle:KPLocalizedString(@"share") forState:UIControlStateNormal];
//            [btn setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
//            btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
//            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
//            [btn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
//            [cell.contentView addSubview:btn];
//
//            //Comment button 7  comment-icon
//            btn = [UIButton buttonWithType:UIButtonTypeCustom];
//            btn.backgroundColor = [UIColor whiteColor];
//            [btn setImage:[UIImage imageNamed:@"comment-icon-"] forState:UIControlStateNormal];
//            btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
//            btn.frame = CGRectMake(10, w - 64, 128, 44);
//            btn.alpha = 0.9;
//            [btn setTitle:KPLocalizedString(@"comment") forState:UIControlStateNormal];
//            [btn setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
//            btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
//            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
//            [btn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
//            [cell.contentView addSubview:btn];
//
//            //buy button 8
//            // Item *item__ = (Item*)[self.items objectAtIndex:indexPath.row];
//
//            //buy button
//            btn = [UIButton buttonWithType:UIButtonTypeCustom];
//            btn.backgroundColor = [UIColor whiteColor];
//            btn.alpha = 0.9;
//            btn.tag = 1111;
//            // [btn setImage:[UIImage imageNamed:@"cart-icon"] forState:UIControlStateNormal];
//            [btn setImage:[UIImage imageNamed:@"album-icon"] forState:UIControlStateNormal];
//            btn.frame = CGRectMake(310 - 44, w - 64, 44, 44);
//            // btn.hidden = YES;
//            [btn addTarget:self action:@selector(buyTap:) forControlEvents:UIControlEventTouchUpInside];
//            [cell.contentView addSubview:btn];

            
            //   share button
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.tag = 300;
            btn.frame = CGRectMake(10 + btnWidth, w - 64, btnWidth, 44);
            btn.alpha = 0.9;
            btn.backgroundColor = [UIColor whiteColor];
            btn.contentMode = UIViewContentModeCenter;
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
            [btn setImage:[UIImage imageNamed:@"feed-image-share-icon"] forState:UIControlStateNormal];
            //   btn.frame = CGRectMake(138, w - 64, 128, 44);
            [btn setTitle:KPLocalizedString(@"share") forState:UIControlStateNormal];
            [btn setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
            [btn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];
            
            
            // Comment button 7  comment-icon
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(10, w - 64, btnWidth, 44);
            btn.alpha = 0.9;
            btn.backgroundColor = [UIColor whiteColor];
            btn.contentMode = UIViewContentModeCenter;
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
            [btn setImage:[UIImage imageNamed:@"comment-icon-"] forState:UIControlStateNormal];
            [btn setTitle:KPLocalizedString(@"comment") forState:UIControlStateNormal];
            [btn setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
            [btn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];
            
            
            //   buy button 8
            //   Item *item__ = (Item*)[self.items objectAtIndex:indexPath.row];
            
            //buy button
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(10 + (btnWidth * 2), w - 64, 44, 44);
            btn.backgroundColor = [UIColor whiteColor];
            btn.alpha = 0.9;
            btn.tag = 1111;
            [btn setImage:[UIImage imageNamed:@"album-icon"] forState:UIControlStateNormal];
            //  btn.frame = CGRectMake(310 - 44, w - 64, 44, 44);
            //   btn.hidden = YES;
            [btn addTarget:self action:@selector(buyTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];
            
            //Separator Line 9

            img = [[UIImageView alloc] initWithFrame:CGRectMake(0, w - 1, w, 1)];
            img.backgroundColor = UIColorFromRGB(0xc8beba);
            [cell.contentView addSubview:img];

            //Voice over icon 10
            img = [[UIImageView alloc] initWithFrame:CGRectMake(30, 5, 32, 32)];
            img.image = [UIImage imageNamed:@"vo-icon-feed"];
            [cell.contentView addSubview:img];

            //Cover Layer 11
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, w, w + 83)];

            img.backgroundColor = UIColorFromRGB(0xE4E2E1);


            [cell.contentView addSubview:img];

            //Loading indicator 12
            UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            av.frame = CGRectMake((w - 20 - 30) / 2, (w - 20 - 30) / 2, 30, 30);
            av.hidesWhenStopped = YES;
            [cell.contentView addSubview:av];

          if(self.items.count > indexPath.row) {
            KPLocalItem *item = (KPLocalItem *) self.items[indexPath.row];
            if (item.itemType == 2)//quote
            {
                //Text
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 66, w - 40, 40)];
                lbl.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
                lbl.textAlignment = NSTextAlignmentLeft;
                lbl.backgroundColor = [UIColor clearColor];
                lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
                lbl.numberOfLines = 50;
                [cell.contentView addSubview:lbl];

                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((w - 32) / 2, 16, 32, 24)];
                img.image = [UIImage imageNamed:@"quote-icon"];
                img.tag = 12345;
                [cell.contentView addSubview:img];

                lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 66, w - 30, 40)];
                lbl.textColor = UIColorFromRGB(0x2EA3EB);
                lbl.textAlignment = NSTextAlignmentLeft;
                lbl.backgroundColor = [UIColor clearColor];
                lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
                lbl.text = KPLocalizedString(@"continue reading...");
                lbl.hidden = YES;
                [cell.contentView addSubview:lbl];
            }

            //13
            aview = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            aview.backgroundColor = UIColorFromRGB(0xc8beba);
            aview.hidden = YES;
            [cell.contentView addSubview:aview];
            //14
            aview = [[UIView alloc] initWithFrame:CGRectMake((10 + btnWidth) - 1, w-64 , 1, 44)];
            aview.backgroundColor = UIColorFromRGB(0xc8beba);
            aview.hidden = YES;
            [cell.contentView addSubview:aview];
            //15
            aview = [[UIView alloc] initWithFrame:CGRectMake((10 + (2 * btnWidth)) - 1, w-64 , 1, 44)];
            aview.backgroundColor = UIColorFromRGB(0xc8beba);
            aview.hidden = YES;
            [cell.contentView addSubview:aview];

            //play button 16
            if (item.itemType != 2) { // quote
                UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play-video-big"]];
                imgView.hidden = YES;
                [cell.contentView addSubview:imgView];
            }
          }
        }

        //comments and share separator lines
    }

    /*
    if (isVideoTYPE) {
        DDLogInfo(@"HIDE BUY BUTTON for indexpath = %d",indexPath.row);
        UIButton *test = (UIButton*)[cell.contentView viewWithTag:1111];
        test.alpha = 0.2;

    }
    else{
        DDLogInfo(@"SHOW BUY BUTTON for indexpath = %d",indexPath.row);
        UIButton *test = (UIButton*)[cell.contentView viewWithTag:1111];
         test.alpha = 0.82;
   
    }*/
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell withIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {

    // return;

    if (self.listMode == 1) {
        float w = self.view.bounds.size.width;
        int maxCount = (w - 20) / 73;

        for (int i = 0; i < maxCount; i++) {
            NSArray *arr = (NSArray *) self.items[indexPath.section];
            UIImageView *img = (UIImageView *) cell.contentView.subviews[i];
            img.tag = i + maxCount * indexPath.row + 1000 * indexPath.section;
            [(UIControl *) img.subviews[0] setTag:i + maxCount * indexPath.row + 1000 * indexPath.section];
            if ([arr count] > i + maxCount * indexPath.row) {
                
                UIImageView *footerImage = (UIImageView *) img.subviews[1];
                UILabel *footerLabel = (UILabel *) img.subviews[2];

                __weak UIImageView *wimg = img;

                KPLocalItem *item = arr[i + maxCount * indexPath.row];
                
                // KPLocalAsset *originalImage = [KPLocalAsset fetchByLocalID:item.originalImageAssetID];
                // NSArray *comments = [KPLocalComment fetchCommentsForItem:item];
                
                // KPLocalAsset *originalImage = self.denormalizedItems[@(item.identifier)][@"originalImage"];
                // NSArray *comments = self.denormalizedItems[@(item.identifier)][@"comments"];

                
                
                NSArray *comments = [KPLocalComment fetchCommentsForItem:item];

                //  DDLogInfo(@"%@",item);

                footerImage.hidden = (comments.count == 0);
                if (footerImage.hidden) {
                    footerLabel.text = @"";
                } else {
                    footerLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long) comments.count];
                }

                // DDLogInfo(@"%@",[NSString stringWithFormat:@"%@%@_small.jpg", [[KPDefaults sharedDefaults] assetsURLString], item.image.urlHash]);

                KPImageProviderWrapper *imageProvider = [KPImageProviderWrapper instance];
                [imageProvider loadImageWithLocalItem:item imageSize:KPImageProviderWrapper.ImageSizeSmall completion:^(UIImage * _Nullable image) {
                    if (image) {
                        [wimg setImage:image];
                    }
                    [UIView animateWithDuration:0.5 animations:^{
                        wimg.alpha = 1.0;
                    }];
                }];

                if (item.itemType == 4) {
                    [[img subviews][3] setHidden:NO];
                } else {
                    [[img subviews][3] setHidden:YES];
                }

                img.hidden = NO;

                /*
                if (item.itemType.intValue == 4)
                {
                    
                    NSArray *arr = [[DKMultyUploaderManger sharedClient]allVideosUploadingProccess];
                    for (DKVideoUploaderManager *obj in arr) {
                        
                        NSString *a = obj.myID;
                        NSString *b = [NSString stringWithFormat:@"%@",item.itemId];
                        
                        if ([a isEqualToString:b]){
                            
                            img.hidden = YES;
                            
                        }
                    }
                }*/

            }
            else {
                img.image = nil;
                img.tag = 0;
                img.hidden = YES;
            }
        }
    } else {

        KPLocalItem *item = (KPLocalItem *) self.items[indexPath.row];
        
        KPLocalAsset *originalImage = self.denormalizedItems[@(item.identifier)][@"originalImage"];
        NSArray *comments = self.denormalizedItems[@(item.identifier)][@"comments"];
        
        // NSArray *comments = [KPLocalComment fetchCommentsForItem:item];
        // KPLocalAsset *originalImage = [KPLocalAsset fetchByLocalID:item.originalImageAssetID];

        if (item.itemType == 4 && (originalImage.assetStatus == 0 || originalImage.assetStatus == 1)) {
        // if (item.itemType == 4 && (/*originalImage.assetStatus == 0 || */originalImage.assetStatus == 1)) {
            DDLogInfo(@"why me");
        } else {
            //Cover Layer
            __weak UIImageView *img = (UIImageView *) cell.contentView.subviews[11];
            float commentsHeight = 0;

            float cnt = comments.count;
            if (item.storyID > 0) {
                cnt += 1;
            }

            int moreCommentsCount = 0;
            if (cnt > 8) {
                moreCommentsCount = cnt - 7;
                cnt = 8;
            }

            if (cnt > 0) {
                float c = ceil((self.view.bounds.size.width - 20.0) / 76.0);
                int r = ceil((float) cnt / (float) c);
                commentsHeight = r * 76;
            }
            float h = self.view.bounds.size.width + 83 + commentsHeight;

            img.frame = CGRectMake(0, 0, cell.frame.size.width, h - 20);
            img.alpha = 1.0;
            img.hidden = NO;

            [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 1] setHidden:YES];
            [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 2] setHidden:YES];
            [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 3] setHidden:YES];
            if (item.itemType == 2)//quote
            {
                img.hidden = YES;

                [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 1] setHidden:NO];
                [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 2] setHidden:NO];
                [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 3] setHidden:NO];
            }

            //Loading indicator
            UIActivityIndicatorView *av = (UIActivityIndicatorView *) cell.contentView.subviews[12];
            [av startAnimating];

            //Item Image
            img = (UIImageView *) cell.contentView.subviews[0];

            if ([img.subviews count] > 3) //remove prev bubble
            {
                for (NSUInteger i = [img.subviews count] - 1; i > 2; i--) {
                    [img.subviews[i] removeFromSuperview];
                }
            }
            [(UIControl *) img.subviews[2] setTag:indexPath.row];


            NSArray *kidIDs = [KPLocalKidItem fetchKidIDsForItem:item];
            NSArray *itemKids = [KPLocalKid idsToObjects:kidIDs];

            if (((KPLocalKid *) itemKids[0]).serverID == 88) {
                [self addKeepyBubble:img withItem:item];
            }

            [av startAnimating];
            img.alpha = 0;
            img.hidden = NO;
            img.frame = CGRectMake(10, 0, self.view.frame.size.width - 20, self.view.frame.size.width - 20);

            if (item.itemType == 2)//quote
            {
                img.backgroundColor = [UIColor whiteColor];
                [av stopAnimating];
                img.alpha = 1;
                [(UIImageView *) cell.contentView.subviews[11] setAlpha:0];

                //CGSize textSize = [item.extraData sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:16] thatFitsToSize:CGSizeMake(280, 250) lineBreakMode:NSLineBreakByWordWrapping];
                NSDictionary *stringAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16]};

                CGSize textSize = [item.extraData boundingRectWithSize:CGSizeMake(280, 250) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin attributes:stringAttributes context:nil].size;

                float ah = textSize.height + 50;

                UILabel *lbl = cell.contentView.subviews[13];
                UILabel *clbl = cell.contentView.subviews[15];
                CGRect aframe = lbl.frame;
                if (textSize.height >= 180) {
                    clbl.hidden = NO;
                    aframe.size.height = 160;

                    clbl.frame = CGRectMake(20, aframe.origin.y + 165, 250, 20);
                }
                else {
                    aframe.size.height = textSize.height + 10;
                    clbl.hidden = YES;
                    ah += 60;
                }
                lbl.frame = aframe;
                lbl.text = item.extraData;

                if (ah < 150) {
                    ah = 150;
                }

                img.frame = CGRectMake(10, 0, self.view.frame.size.width - 20, ah + 20);
            } else {
                img.backgroundColor = [UIColor clearColor];

                __weak UIImageView *wimg = img;
                KPImageProviderWrapper *imageProvider = [KPImageProviderWrapper instance];
                [imageProvider loadImageWithLocalItem:item imageSize:KPImageProviderWrapper.ImageSizeMedium completion:^(UIImage * _Nullable image) {
                    if (image) {
                        [wimg setImage:image];
                    }

                    [UIView animateWithDuration:0.1 animations:^{
                        wimg.alpha = 1.0;
                        [(UIImageView *) cell.contentView.subviews[11] setAlpha:0];

                        [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 2] setHidden:NO];
                        [(UIView *) cell.contentView.subviews[[cell.contentView.subviews count] - 3] setHidden:NO];

                        /*
                        NSData *imgData = UIImageJPEGRepresentation(image, 1); //1 it represents the quality of the image.
                        DDLogInfo(@"Size of Image(bytes):%d",[imgData length]);*/

                    }];
                    [av stopAnimating];
                }];
            }

            if (item.itemType == 4) {
                img.contentMode = UIViewContentModeScaleToFill;
            }


            [img.layer setBorderWidth:0.1];
            [img.layer setBorderColor:[UIColorFromRGB(0xffffff) CGColor]];

            [img.layer setShadowColor:[UIColorFromRGB(0x000000) CGColor]];

            CGMutablePathRef path = CGPathCreateMutable();
            CGPathAddRect(path, NULL, CGRectMake(-1, -1, img.frame.size.width + 1, img.frame.size.height + 1));
            img.layer.shadowPath = path;
            img.layer.shadowOpacity = 0.3;
            img.layer.shadowOffset = CGSizeMake(1.0, 1.0);
            img.layer.shadowRadius = 0.5;
            CFRelease(path);

			UIImageView *kidImgBkg = img.subviews[0];
			__weak UIImageView *kidImg = img.subviews[1];
			kidImg.image = nil;
			if (self.kid == nil) {
				//DDLogInfo(@"%@",item.kids);

				if (itemKids.count > 1) {
					kidImgBkg.hidden = YES;
					kidImg.image = nil;
				} else {
					kidImgBkg.hidden = NO;
					KPLocalKid *kid = itemKids[0];
					//DDLogInfo(@"%@",[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], kid.image.urlHash]);
					kidImg.tag = kid.serverID;

					KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];

					if (kidImage == nil) {
						kidImg.image = [GlobalUtils getKidDefaultImage:kid.gender];
					} else {
						[kidImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, kidImage.urlHash]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

							kidImg.image = [image imageMaskedWithElipse:CGSizeMake(98, 98)];
						}];
					}
				}
			}
			else {
				kidImgBkg.hidden = YES;
				kidImg.image = nil;
			}

            NSString *itemTitleStr = @"";
            NSString *yearStr = @"";
            UILabel *titleLbl = (UILabel *) cell.contentView.subviews[3];
            if (self.kid.serverID == 88) {
                itemTitleStr = item.title;
            }
            else {
                if (item.title != nil) {
                    itemTitleStr = item.title;
                }

                if (item.itemDate > 0) {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    //[formatter setDateFormat:@"M.d.YY"];
                    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                    [formatter setDateStyle:NSDateFormatterShortStyle];
                    yearStr = [NSString stringWithFormat:@"(%@)", [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:item.itemDate]]];
                }
                itemTitleStr = [NSString stringWithFormat:@"%@ %@", yearStr, itemTitleStr];
            }
            
            NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:itemTitleStr];
            NSRange boldRange = {NSNotFound, 0};
            if (item.title.length > 0) {
                boldRange = [[mutableAttributedString string] rangeOfString:item.title options:NSCaseInsensitiveSearch];
            }
            UIFont *boldSystemFont = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:14];
            CTFontRef font = CTFontCreateWithName((__bridge CFStringRef) boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
            if (font) {
                [mutableAttributedString addAttribute:(NSString *) kCTFontAttributeName value:(__bridge id) font range:boldRange];
                CFRelease(font);
            }
            titleLbl.attributedText = mutableAttributedString;

            //comments view
            UIView *commentsView = (UIView *) cell.contentView.subviews[4];
            commentsView.hidden = (cnt == 0);
            commentsHeight = 0;
            for (UIControl *control in commentsView.subviews) {
                [control removeFromSuperview];
            }

//            NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"isStory" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"commentDate" ascending:YES]];
//            NSArray *itemComments = [comments sortedArrayUsingDescriptors:sortDescriptors];
            if (!commentsView.hidden) {
                float ax = 0;
                float ay = 0;
                for (int i = 0; i < cnt; i++) {
                    UIImageView *pimg = [[UIImageView alloc] initWithFrame:CGRectMake(ax, ay, 70, 70)];
                    pimg.backgroundColor = [UIColor blackColor];
                    pimg.contentMode = UIViewContentModeScaleAspectFit;

                    if (i == cnt - 1 && moreCommentsCount > 0) {

                        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 50, 40)];
                        lbl.textColor = UIColorFromRGB(0x329fc7);
                        lbl.textAlignment = NSTextAlignmentCenter;
                        lbl.backgroundColor = [UIColor clearColor];
                        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:40];
                        lbl.text = [NSString stringWithFormat:@"%d", moreCommentsCount];
                        [pimg addSubview:lbl];

                        lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 45, 50, 15)];
                        lbl.textColor = UIColorFromRGB(0x329fc7);
                        lbl.textAlignment = NSTextAlignmentCenter;
                        lbl.backgroundColor = [UIColor clearColor];
                        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:9];
                        lbl.text = KPLocalizedString(@"more");
                        [pimg addSubview:lbl];

                        pimg.backgroundColor = [UIColor whiteColor];
                        [commentsView addSubview:pimg];

                        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentThumbTap:)];
                        gesture.cancelsTouchesInView = NO;
                        [pimg addGestureRecognizer:gesture];
                        pimg.userInteractionEnabled = YES;

                        break;
                    }

                    int aindex = i;
                    if (item.storyID > 0) {
                        aindex--;
                    }

                    NSString *assetHash = @"";
                    BOOL wasRead = NO;
                    float aLength = 0;

                    NSString *thumbImageName = @"video-thumbnail-bar";

                    KPLocalComment *comment = nil;
                    KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:item.storyID];

                    UIColor *lengthColor = [UIColor whiteColor];
                    if (aindex < 0) {
                        aLength = itemStory.storyLength;
                        wasRead = YES;
                        thumbImageName = @"VO-thumbnail-bar";
                        lengthColor = [UIColor blackColor];

                        KPLocalKid *kid = itemKids[0];
                        KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];

                        if (kidImage == nil) {
                            pimg.image = [GlobalUtils getKidDefaultImage:kid.gender];
                        }
                        else {

                            [pimg setImage:[UIImage imageNamed:@"story-thumbnail"]];
                            [pimg setBackgroundColor:[UIColor clearColor]];
                            // pimg.alpha = 0.7;
                            //
                            /*
                            assetHash = [(Kid*)item.kids.anyObject image].urlHash;
                            [pimg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], assetHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            }];*/
                        }

                    }
                    else {
                        comment = comments[aindex];
                        KPLocalAsset *commentPreviewImage = [KPLocalAsset fetchByLocalID:comment.previewImageAssetID];

                        assetHash = commentPreviewImage.urlHash;
                        wasRead = comment.wasRead;
                        aLength = comment.commentLength;

                        if (comment.isStory) {
                            lengthColor = [UIColor blackColor];

                            thumbImageName = @"Video-story-thumbnail-bar";

                            KPLocalKid *kid = itemKids[0];
                            KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];

                            if (kidImage == nil) {
                                pimg.image = [GlobalUtils getKidDefaultImage:kid.gender];
                            }
                            else {
                                [pimg setImage:[UIImage imageNamed:@"story-thumbnail"]];
                                [pimg setBackgroundColor:[UIColor clearColor]];
                                //pimg.alpha = 0.7;
                                /*
                                assetHash = [(Kid*)item.kids.anyObject image].urlHash;
                                // DDLogInfo(@"%@",[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], assetHash]);
                                [pimg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], assetHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                }];*/
                            }

                        }
                        else {
                            if (comment.commentType == 0) {
                                thumbImageName = @"video-thumbnail-bar";
                            } else if (comment.commentType == 1) {
                                thumbImageName = @"VO-comment-thumbnail-bar";
                            } else if (comment.commentType == 2) {
                                thumbImageName = @"text-comment-thumbnail-bar";
                            }


                            [pimg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, assetHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            }];
                        }
                    }

                    [commentsView addSubview:pimg];

                    //panel
                    UIImageView *panelImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 53, 70, 17)];
                    panelImg.image = [UIImage imageNamed:thumbImageName];
                    panelImg.alpha = 1.0;
                    [pimg addSubview:panelImg];

                    //  DDLogInfo(@"%@",thumbImageName);
                    lengthColor = [UIColor whiteColor];
                    if ([thumbImageName isEqualToString:@"VO-thumbnail-bar"]) {

                        [panelImg removeFromSuperview];
                    }

                    if ([thumbImageName isEqualToString:@"Video-story-thumbnail-bar"]) {
                        [panelImg removeFromSuperview];
                    }

                    // THIS IS WHERE NEW COMMENTS ARE DETERMINED

                    //new ribbon
                    UIImageView *ribbonImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34, 34)];
                    ribbonImg.image = [UIImage imageNamed:@"new-video-ribbon"];
                    ribbonImg.hidden = wasRead;
                    [pimg addSubview:ribbonImg];

                    //new label
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(-5.5, -5.5, 34, 34)];
                    lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
                    lbl.textAlignment = NSTextAlignmentCenter;
                    lbl.backgroundColor = [UIColor clearColor];
                    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:10];
                    lbl.text = @"new";
                    lbl.transform = CGAffineTransformMakeRotation(-M_PI / 4);
                    lbl.hidden = wasRead;
                    [pimg addSubview:lbl];

                    //time label
                    UILabel *timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(27, 70 - 17, 40, 17)];
                    timeLbl.backgroundColor = [UIColor clearColor];
                    timeLbl.textColor = lengthColor;
                    timeLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:10];
                    timeLbl.textAlignment = NSTextAlignmentRight;

                    int clength = aLength;

                    if (comment == nil || comment.isStory) {
                        int minutes = floor(clength / 60);
                        int seconds = round(clength - minutes * 60);

                        if (minutes < 0) {
                            minutes = 0;
                        }

                        if (seconds < 0) {
                            seconds = 0;
                        }

                        timeLbl.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
                    }
                    else {
                        if (comment.creatorUserID == [[UserManager sharedInstance] getMe].userId.intValue) {
                            timeLbl.text = [[UserManager sharedInstance] getMe].name;
                        } else {
                            timeLbl.text = comment.creatorName;
                        }
                    }


                    [pimg addSubview:timeLbl];

                    pimg.clipsToBounds = YES;
                    pimg.tag = aindex;

                    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentThumbTap:)];
                    gesture.cancelsTouchesInView = NO;
                    [pimg addGestureRecognizer:gesture];
                    pimg.userInteractionEnabled = YES;

                    if (i < cnt - 1) {
                        ax += 76;
                        if (ax + 70 > img.frame.size.width) {
                            ax = 0;
                            ay += 76;
                        }
                    }
                }
                CGRect aframe = commentsView.frame;
                aframe.size.height = ay + 80;
                aframe.origin.y = img.frame.origin.y + img.frame.size.height + 40;
                commentsView.frame = aframe;
                commentsHeight = aframe.size.height;
                commentsView.tag = indexPath.row;
            }


            for (int i = 0; i < [cell.contentView.subviews count]; i++) {
                UIControl *control = (UIControl *) cell.contentView.subviews[i];
                control.tag = indexPath.row;
                CGRect aframe = control.frame;

                if (i == [cell.contentView.subviews count] - 3) //comment *& share - hor sep line
                {
                    //DDLogInfo(@"%@", NSStringFromCGRect(aframe));

                    // control.hidden = YES;
//                    aframe.origin.y = img.frame.origin.y + img.frame.size.height - 44;

                    if (item.itemType == 4) {
//                        aframe.origin.x = img.frame.origin.x + 150;
                        aframe  = control.frame;

                    } else if(item.itemType != 2){
//                        aframe.origin.x = 137.5;
                        aframe  = control.frame;
                    }

                }
                else if (i == [cell.contentView.subviews count] - 2) //comment & share - ver sep line
                {
//                    aframe.origin.y = img.frame.origin.y + img.frame.size.height - 44;


                    if (item.itemType == 4) {
//                        aframe.origin.x = img.frame.origin.x + 355;
                        aframe  = control.frame;

                    } else if(item.itemType != 2){
//                        aframe.origin.x = img.frame.origin.x + 255;
                        aframe  = control.frame;

                    }
                }
                else if (i == [cell.contentView.subviews count] - 1) //comment & share - ver sep line
                {
                    if(item.itemType == 2) {
                        aframe.origin.y = img.frame.origin.y + img.frame.size.height - 44;
                    }
                }
                else if (i == 11 || i == 12 || i > 12) {
                    //do nothing
                }
                else if (i == 10) {
                    aframe.origin.y = img.frame.origin.y + img.frame.size.height - 52;
                }
                else if (i > 8) //Separator Line
                {
                    if (item.itemType == 2) {
                        aframe.origin.y = img.frame.size.height + 35 + commentsHeight + 10;
                    } else {
                        aframe.origin.y = img.frame.size.height + 35 + commentsHeight + 10;
                    }

                }
                else if (i > 5) //share and comment buttons
                {
                    aframe.origin.y = img.frame.origin.y + img.frame.size.height - 44;

                    //share
                    if (i == 6) {
                        if (item.itemType == 4) {
//                            aframe = CGRectMake(160, aframe.origin.y, 150, 44);
                            aframe  = control.frame;

                        } else {
//                            aframe = CGRectMake(138, aframe.origin.y, 128, 44);
                            aframe  = control.frame;

                        }


                    }
                    //comment
                    if (i == 7) {

                        if (item.itemType == 4) {
//                            aframe = CGRectMake(10, aframe.origin.y, 150, 44);
                            aframe  = control.frame;

                        } else {
//                            aframe = CGRectMake(10, aframe.origin.y, 128, 44);
                            aframe  = control.frame;

                        }


                    }
                    // Buy
                    if (i == 8) {
                        
                        if (item.itemType == 4) {
                            //                            aframe = CGRectMake(10, aframe.origin.y, 150, 44);
                            aframe  = control.frame;
                            
                        } else {
                            //                            aframe = CGRectMake(10, aframe.origin.y, 128, 44);
                            aframe  = control.frame;
                            
                        }
                        
                        
                    }

                }
                else if (i == 3) {
                    aframe.origin.y = img.frame.origin.y + img.frame.size.height + 5;
                }

                control.frame = aframe;
            }

            BOOL isMyKid = ((KPLocalKid *) itemKids[0]).isMine;


            //edit info button
            UIButton *btn = (UIButton *) cell.contentView.subviews[2];
            btn.hidden = (!isMyKid);

            // gift button
            btn = (UIButton *) cell.contentView.subviews[8];


            if (item.itemType == 4) {
                btn.enabled = NO;
                btn.hidden = YES;
//                btn.enabled = YES;
//                btn.hidden = NO;

            } else {
                btn.enabled = YES;
                btn.hidden = NO;
            }


            btn = (UIButton *) cell.contentView.subviews[7];
            if (item.itemType == 4) {

                // [btn setFrame:  v];
                // [img setFrame:CGRectMake(img.frame.origin.x + 20,img.frame.origin.y,img.frame.size.width,img.frame.size.height)];
            }
            else {
                // [btn setFrame:CGRectMake(btn.frame.origin.x - 20,btn.frame.origin.y,btn.frame.size.width,btn.frame.size.height)];
                // [img setFrame:CGRectMake(img.frame.origin.x - 20,img.frame.origin.y,img.frame.size.width,img.frame.size.height)];
            }

            btn = (UIButton *) cell.contentView.subviews[6];
            if (item.itemType == 4) {

                // [btn setFrame:CGRectMake(btn.frame.origin.x + 20,btn.frame.origin.y,btn.frame.size.width,btn.frame.size.height)];
            }
            else {
                // [btn setFrame:CGRectMake(btn.frame.origin.x - 20,btn.frame.origin.y,btn.frame.size.width,btn.frame.size.height)];
            }


            //voice over icon - deprecated...
            UIImageView *voImageView = (UIImageView *) cell.contentView.subviews[10];
            voImageView.hidden = YES;

            //record story button - deprecated...
            btn = (UIButton *) cell.contentView.subviews[1];
            btn.hidden = YES;//(item.story != nil || !isMyKid || item.itemType.intValue==1 || item.itemType.intValue==2);

            if (item.itemType != 2) {
                UIImageView *play = (UIImageView *) cell.contentView.subviews[16];
                play.center = img.center;
                CGRect frame = play.frame;
                frame.origin.y -= 20;
                play.frame = frame;

                UIView *uploadProgressView;

                if (item.itemType == 4) {

                    NSArray *arr = [[DKMultyUploaderManger sharedClient] allVideosUploadingProccess];
                    for (DKVideoUploaderManager *obj in arr) {

                        NSString *a = obj.myID;
                        NSString *b = @(item.serverID).stringValue;

                        if([obj.uploadProgressView viewWithTag:888])
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIActivityIndicatorView * activityIndicator = (UIActivityIndicatorView *) [obj.uploadProgressView viewWithTag:888];
                                [activityIndicator startAnimating];
                            });
                        }
                        
                        if ([a isEqualToString:b]) {

                            uploadProgressView = obj.uploadProgressView;
//                            if([uploadProgressView viewWithTag:888])
//                            {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    UIActivityIndicatorView * activityIndicator = (UIActivityIndicatorView *) [uploadProgressView viewWithTag:888];
//                                    [activityIndicator startAnimating];
//                                });
//                            }
                        }
                    }
                }

                if (uploadProgressView) {
                    uploadProgressView.hidden = NO;
                    [cell.contentView addSubview:uploadProgressView];
                    [cell.contentView bringSubviewToFront:uploadProgressView];
                    uploadProgressView.tag = 112;
                }

                if (item.itemType == 4 && !uploadProgressView) {
                    if (originalImage.assetStatus == 2) {
                        [play setHidden:NO];
                    } else {
//                        [self checkItemForVideoStatus:item indexPath:indexPath];
                        [play setHidden:YES];
                    }
                } else {

                    [play setHidden:YES];
                }
            }
        }
    }
}

- (void)configureUploaderCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    KPLocalItem *item = (KPLocalItem *) self.items[indexPath.row];
    // DDLogInfo(@"item = %@",item.itemId);

    UIView *uploadProgressView;

    NSArray *arr = [[DKMultyUploaderManger sharedClient] allVideosUploadingProccess];

    for (DKVideoUploaderManager *obj in arr) {
        NSString *a = obj.myID;
        NSString *b = @(item.serverID).stringValue;

        if ([a isEqualToString:b]) {
            uploadProgressView = obj.uploadProgressView;
        }
    }

    if (uploadProgressView) {
        uploadProgressView.hidden = NO;
        [cell.contentView addSubview:uploadProgressView];
        [cell.contentView bringSubviewToFront:uploadProgressView];
        uploadProgressView.tag = 112;
    } else {
        BOOL shouldReload = NO; // ![self.reloadIgnoringItemIDs containsObject:@(item.identifier)];
        [self.reloadIgnoringItemIDs addObject:@(item.identifier)];
        
        [[DKMultyUploaderManger sharedClient] addUploaderWithItem:item];
        
        if (shouldReload) {
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}


- (void)startToCheckVideoStatusItem:(Item *)titem atIndexPath:(NSIndexPath *)indexPath withResult:(BOOLBlock)BOOLresult {
    Asset *a = titem.originalImage;
    NSNumber *assetId = a.assetId;

    __block typeof(ItemsViewController * ) mySelf = self;
    __block typeof(NSIndexPath * ) myIndexPath = indexPath;
    __block typeof(Item * ) myItem = titem;

    [[ServerComm instance] getVideoAssetStatus:[assetId stringValue]
                                  successBlock:^(id result) {
                                      int a = [[[result objectForKey:@"result"] objectForKey:@"assetStatus"] intValue];
                                      DDLogInfo(@"a = %i", a);


                                      if (a == 2) {
                                          DDLogInfo(@"READY!!! (items view controller)");

                                          BOOLresult ? BOOLresult(YES) : ([self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:myIndexPath] withRowAnimation:UITableViewRowAnimationFade]);
                                      }else if (a ==3){ //added by amitg
                                      
                                          NSArray *cells = [self.tableView visibleCells];
                                          UITableViewCell *cell = [mySelf.tableView cellForRowAtIndexPath:indexPath];
                                          if ([cells containsObject:cell]) {
                                              NSDictionary *dict = @{@"item" : myItem, @"indexPath" : myIndexPath};
                                              
                                              [mySelf performSelector:@selector(refreshIndexPath:) withObject:dict afterDelay:2.0];
                                              
                                          }
                                          BOOLresult ? BOOLresult(NO) : (nil);
                                          
                                      } else {
                                          DDLogInfo(@"NOT READY!!!");

                                          NSArray *cells = [self.tableView visibleCells];
                                          UITableViewCell *cell = [mySelf.tableView cellForRowAtIndexPath:indexPath];
                                          if ([cells containsObject:cell]) {
                                              NSDictionary *dict = @{@"item" : myItem, @"indexPath" : myIndexPath};

                                              [mySelf performSelector:@selector(refreshIndexPath:) withObject:dict afterDelay:2.0];
                                          }
                                          BOOLresult ? BOOLresult(NO) : (nil);

                                      }

                                  } failBlock:^(NSError *error) {


                NSArray *cells = [self.tableView visibleCells];
                UITableViewCell *cell = [mySelf.tableView cellForRowAtIndexPath:indexPath];
                if ([cells containsObject:cell]) {
                    NSDictionary *dict = @{@"item" : myItem, @"indexPath" : myIndexPath};

                    [mySelf performSelector:@selector(refreshIndexPath:) withObject:dict afterDelay:2.0];

                }
                BOOLresult ? BOOLresult(NO) : (nil);


            }];
}

- (void)refreshIndexPath:(NSDictionary *)info {
    NSIndexPath *ip = info[@"indexPath"];
    Item *item = info[@"item"];

//    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self startToCheckVideoStatusItem:item atIndexPath:ip withResult:nil];
}


#pragma mark - UITableView Delegate methods

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[[NSNotificationCenter defaultCenter]postNotificationName:@"TEST" object:nil];
}
*/
#pragma mark - UITableViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([UIScreen mainScreen].bounds.size.height == 568) {
//        self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset - 20, 0, 60, 0);;
//        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topContentInset - 20, 0, 60, 0);;
    } else {
        
    if (!self.isLockedKeepyFeed) {
        [[NSNotificationCenter defaultCenter] postNotificationName:APP_MODE_CHANGED_NOTIFICATION object:@(scrollView.contentOffset.y < 300 || (scrollView.contentSize.height - scrollView.contentOffset.y > self.view.frame.size.height && self.lastContentOffset > scrollView.contentOffset.y))];
    }

    //DDLogInfo(@"%2f %2f", scrollView.contentOffset.y, scrollView.contentSize.height-scrollView.contentOffset.y);

    if (self.kid != nil && self.kid.serverID == 88) {
        //DDLogInfo(@"%2f %2f", scrollView.contentOffset.y, scrollView.contentSize.height-scrollView.contentOffset.y);
        if (scrollView.contentOffset.y == 480 || scrollView.contentSize.height - scrollView.contentOffset.y == 0) {
            [[Mixpanel sharedInstance] track:@"keepy-feed-scroll" properties:@{@"offset" : @(scrollView.contentOffset.y)}];
        }
    }

    self.lastContentOffset = scrollView.contentOffset.y;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CLOSE_FLOATING_VIEW object:nil userInfo:nil];
    
    // Modified by Ashvini
//    if ([UIScreen mainScreen].bounds.size.height == 568) {
//        self.lastContentOffset = 70;
//    } else {
//        self.lastContentOffset = scrollView.contentOffset.y;
//    }
 }
}

#pragma mark - Actions

- (void)recordTap:(UIButton *)sender {

    [[Mixpanel sharedInstance] track:@"record story tap" properties:@{@"source" : @"feed"}];


    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    Item *item = (Item *) self.items[self.currentSelectedIndexPath.row];

    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    RecordViewController *avc = [[RecordViewController alloc] initWithItem:(id)item withMode:0];
    avc.delegate = self;
    [d.navController pushViewController:avc animated:YES];
    //[d.viewController presentModalViewController:avc animated:YES];
}

- (void)editTap:(UIButton *)sender {
    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    KPLocalItem* localItem = self.items[self.currentSelectedIndexPath.row];
    
    UIStoryboard* sb = [UIStoryboard storyboardWithName:@"AssetCollection" bundle:nil];
    UINavigationController* nc = [sb instantiateViewControllerWithIdentifier:@"EditStory"];
    AddStoryViewController2* vc = nc.viewControllers.firstObject;
    vc.localItem = localItem;
    
    [self presentViewController:nc animated:YES completion:NULL];

#if TO_BE_DELETED_FOREVER
    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    Item *item = (Item *) self.items[self.currentSelectedIndexPath.row];

    id avc = nil;
    /*
     if (item.itemType.intValue == 0 || item.itemType.intValue == 2) //photo
     {
     avc = [[ItemEditViewController alloc] initWithItem:item andMode:0];
     [(ItemEditViewController*)avc setDelegate:self];
     }
     else if (item.itemType.intValue == 1) //cover-note
     {
     avc = [[NoteEditorViewController alloc] initWithItem:item];
     [(NoteEditorViewController*)avc setDelegate:self];
     }*/
    avc = [[ItemEditViewController alloc] initWithItem:item andMode:0];
    [(ItemEditViewController *) avc setDelegate:self];


    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    /*
     d.viewController.navigationItem.backBarButtonItem =
     [[UIBarButtonItem alloc] initWithTitle:KPLocalizedString(@"back") style:UIBarButtonItemStyleBordered target:nil action:nil];
     */
    [d.navController pushViewController:avc animated:YES];
#endif
}


- (void)commentsTap:(UIButton *)sender {
}

- (void)likesTap:(UIButton *)sender {

}

// ShareButtonClickTapEvent
- (void)shareTap:(UIButton *)sender {
    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    Item *item = (Item *) self.items[self.currentSelectedIndexPath.row];
    [GlobalUtils shareItem:(id)item withSource:@"Feed" sender:sender];
    return;
    /*
     ItemShareViewController *avc = [[ItemShareViewController alloc] initWithItem:item];
     
     AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
     [d.navController pushViewController:avc animated:YES];
     */
    //[self presentModalViewController:avc animated:YES];

    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, item.image.urlHash]] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {

        [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"downloading original image")];
    }                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {

        NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0], @"shared.igo"];

        [UIImageJPEGRepresentation(image, 0.8) writeToFile:tmpPath atomically:YES];
        self.interactionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:tmpPath]];

        self.interactionController.UTI = @"com.instagram.photo";
        self.interactionController.delegate = self;
        [self.interactionController presentOpenInMenuFromRect:CGRectMake(0, 0, 320, 100) inView:self.view animated:YES];
    }];
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application {
    DDLogInfo(@"SHARE:%@", application);
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application {

}

- (void)commentTap:(UIButton *)sender {
    if (self.kid.serverID == 88) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"can\'t comment on this feed")];
        return;
    }

    [[Mixpanel sharedInstance] track:@"Comment Tap"];


    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];

    UIActionSheet *asheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:KPLocalizedString(@"cancel") destructiveButtonTitle:nil otherButtonTitles:KPLocalizedString(@"video"), KPLocalizedString(@"voice"), KPLocalizedString(@"text"), nil];
    [asheet showInView:self.view];
}

- (void)buyTap:(UIButton *)sender {
    if (self.kid.serverID == 88) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"can\'t print & give on this feed")];
        return;
    }
    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];

    [[Mixpanel sharedInstance] track:@"Gift Tap"];

    Item *item = (Item *) self.items[self.currentSelectedIndexPath.row];
    [GlobalUtils buyItem:item withSource:@"Feed"];

}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 3) {
        return;
    }
    
    KPLocalItem *item = (KPLocalItem *) self.items[self.currentSelectedIndexPath.row];
    
  dispatch_async(dispatch_get_main_queue(), ^{
    RecordViewController *avc = [[RecordViewController alloc] initWithItem:(id)item withMode:(buttonIndex < 2) ? (NSInteger)(11 - buttonIndex) : 12];
    avc.delegate = self;
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController pushViewController:avc animated:YES];
  });
}

- (void)likeTap:(UIButton *)sender {

    [[Mixpanel sharedInstance] track:@"Like Tap"];


    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    Item *item = (Item *) self.items[self.currentSelectedIndexPath.row];

    int readAction = 2;
    int oldReadAction = item.readAction.intValue;
    NSString *imageStr = @"loved";
    NSString *oldImageStr = @"love";
    if (item.readAction.intValue == 2) {
        readAction = 1;
        imageStr = @"love";
        oldImageStr = @"loved";
    }

    [sender setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
    item.readAction = @(readAction);

    [[ServerComm instance] markItemRead:item.itemId.intValue andReadAction:readAction withSuccessBlock:^(id result) {

        if ([[result valueForKey:@"status"] intValue] != 0) {
            [sender setImage:[UIImage imageNamed:oldImageStr] forState:UIControlStateNormal];
            item.readAction = @(oldReadAction);
        }
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

        [[NSNotificationCenter defaultCenter] postNotificationName:LOVE_CHANGED_NOTIFICATION object:item];

    }                      andFailBlock:^(NSError *error) {
        [sender setImage:[UIImage imageNamed:oldImageStr] forState:UIControlStateNormal];
        item.readAction = @(oldReadAction);
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
        [[NSNotificationCenter defaultCenter] postNotificationName:LOVE_CHANGED_NOTIFICATION object:item];
    }];
}

- (void)thumbTap:(UITapGestureRecognizer *)gesture {
    
    NSInteger i = -1;
    NSInteger section = 0;
    NSArray *arr = self.items;
    if (self.listMode == 0) {
        i = [(UIControl *) gesture.view.subviews[2] tag];
    }
    else {
        NSInteger aindex = [(UIControl *) gesture.view.subviews[0] tag];
        section = aindex / 1000;
        i = aindex - 1000 * section;
        arr = (NSArray *) self.items[section];
        //arr = (NSArray*)[self.items objectAtIndex:section];
    }

    KPLocalItem *item = arr[i];
    if (item.serverID == 336) {
        [[Mixpanel sharedInstance] track:@"start-keepying-tap"];

        [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_CAMERA_NOTIFICATION object:nil];
        return;
    }

    if ([(UIImageView *) gesture.view image] == nil && self.listMode == 0 && item.itemType != 2) {
        return;
    }

    self.currentSelectedIndexPath = [NSIndexPath indexPathForRow:i inSection:section];
    
    [self showFullscreen:[NSIndexPath indexPathForRow:i inSection:section] withMode:0 andCommentIndex:-1];
}
//this code was used for to filter by children
//- (void)kidTap:(UITapGestureRecognizer *)gesture {
//    Kid *kid = [Kid MR_findFirstByAttribute:@"kidId" withValue:@(gesture.view.tag)];
//    [[NSNotificationCenter defaultCenter] postNotificationName:CHANGE_KID_NOTIFICATION object:kid];
//}

- (void)commentThumbTap:(UITapGestureRecognizer *)gesture {
    NSInteger commentIndex = gesture.view.tag;
    
    [self showFullscreen:[NSIndexPath indexPathForRow:gesture.view.superview.tag inSection:0] withMode:1 andCommentIndex:commentIndex/* +1 */];
}

- (void) galleryModeTap:(id)sender {
    if (self.galleryModeButton.selected) {
        return;
    }

    if ([self isNewAlbumActive]) {
        self.tableView.hidden = YES;
        self.calendarViewController.collectionView.hidden = NO;
    } else {
        self.listMode = 1;
        self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
    }
    
    self.feedModeButton.selected = NO;
    self.galleryModeButton.selected = YES;

    [[NSNotificationCenter defaultCenter] postNotificationName:APP_MODE_CHANGED_NOTIFICATION object:@(YES)];
    
    [[UserManager sharedInstance] setFeedMode:1];
}

- (void)didAddPhoto {
    
//    BOOL isFeedMode = ([[UserManager sharedInstance] feedMode] == 0);
    
    [self reloadData:FILTER_TYPE_ALL withFilterValue:@""];
    
    
}

- (void) feedModeTap:(id)sender {
    if (self.feedModeButton.selected) {
        return;
    }
    
    if ([self isNewAlbumActive]) {
        self.tableView.hidden = NO;
        self.calendarViewController.collectionView.hidden = YES;
    } else {
        self.listMode = 0;
    }
    
    self.galleryModeButton.selected = NO;
    self.feedModeButton.selected = YES;

    self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
    self.tableView.contentInset = UIEdgeInsetsMake(self.topContentInset, 0, 65, 0);
    
    [[UserManager sharedInstance] setFeedMode:0];
}

- (void)kidsTap:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_KIDS_MENU_NOTIFICATION object:nil];
}

#pragma mark - UserLoginNotification

- (void)userDidLogin:(NSNotification *)notification {
    NSLog(@"did login");
    /*
     [[ServerComm instance] getItems:^(id result) {
     int status = [[result valueForKey:@"status"] intValue];
     if (status == 0)
     {
     [[KPDataParser instance] parseKids:(NSArray*)[result valueForKey:@"result"]];
     [self reloadData];
     }
     } andFailBlock:^(NSError *error) {
     DDLogInfo(@"error refreshing data");
     }];
     */
}

#pragma mark - AlbumSelectNotification

- (void)kidSelected:(NSNotification *)notification {

    NSNumber *oldKidKey = @(-1);
    if(self.kid){
        oldKidKey = @(self.kid.identifier);
    }
    
    if(self.filterTag){
        self.kidFilterCache[oldKidKey] = self.filterTag;
    }else{
        [self.kidFilterCache removeObjectForKey:oldKidKey];
    }
    
    
    //self.isLockedKeepyFeed = ([KPLocalKid fetchAll].count < 2);
    self.isLockedKeepyFeed = ![KPLocalKid hasKids];
    // self.isLockedKeepyFeed = NO;

    self.kid = (KPLocalKid *) notification.object;
    
    NSNumber *newKidKey = @(-1);
    if(self.kid){
        newKidKey = @(self.kid.identifier);
    }
    
    KPLocalTag *restoredFilterTag = self.kidFilterCache[newKidKey];
    self.filterTag = restoredFilterTag;
    
    self.calendarViewController.selectedKid = self.kid;
    
    if(self.filterTag){
        self.calendarViewController.isFilterActive = YES;
    }else{
        self.calendarViewController.isFilterActive = NO;
    }
    

    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadData];
    });


}

#pragma mark - RecordViewControllerDelegate

- (void)recordingSuccessful:(NSData *)fileData withLength:(NSInteger)recordingLength andMode:(NSInteger)mode andPreviewImage:(UIImage *)previewImage {
    if (fileData == nil) {
        KPLocalItem *item = (KPLocalItem *) self.items[self.currentSelectedIndexPath.row];
        NSUInteger c = 0;
        
        NSArray *comments = [KPLocalComment fetchCommentsForItem:item];
        
        if (comments != nil) {
            c = comments.count;
        }

        [[Mixpanel sharedInstance] track:@"Add Comment" properties:@{@"Source" : @"Feed", @"Number of Video" : @(c)}];
    }
    else {

        [[Mixpanel sharedInstance] track:@"Record Story" properties:@{@"Source" : @"Feed"}];
    }

    [self.tableView reloadRowsAtIndexPaths:@[self.currentSelectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
  //  [self.tableView reloadRowsAtIndexPaths:@[self.currentSelectedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    
  //  [self.tableView scrollToRowAtIndexPath: self.currentSelectedIndexPath atScrollPosition:UITableViewScrollPositionTop  animated:NO];
    
}

- (void)recordingCancelled {
    
}

- (KPLocalComment*)videoStoryToDelete {
    NSAssert(NO, @"videoStoryToDelete is not implemented");
    return nil;
}

#pragma mark - ItemEditDelegate

- (void)itemEditFinished {
    [self.tableView reloadRowsAtIndexPaths:@[self.currentSelectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)itemEditCancelled {

}

#pragma mark - FilterViewDelegate

/*
 -(void)filterSelected:(int)filterType withValue:(NSString *)filterValue
 {
 
 [[Mixpanel sharedInstance] track:@"Filter Selected" properties:[NSDictionary dictionaryWithObjectsAndKeys:@(filterType), @"filterType", filterValue, @"filterValue", nil]];
 
 
 [self reloadData:filterType withFilterValue:filterValue];
 [self filterTap:nil];
 }
 */
#pragma mark - VideoRecordDelegate

- (void)videoCommentAdded:(Comment *)comment {
    [[NSNotificationCenter defaultCenter] postNotificationName:COMMENT_ADDED_NOTIFICATION object:comment];
    [self.tableView reloadData];
}

#pragma mark - ItemAddedNotification

- (void)itemAdded:(NSNotification *)notification {
    
    KPLocalItem *item = (KPLocalItem *) notification.object;
//    NSLog(@"itemAdded: %ld", (long)item.originalImageAssetID);
    NSLog(@"itemAdded: %@", notification.userInfo[@"itemID"]);
    
    dispatch_async(dispatch_queue_create(nil, nil), ^{ // perform these memory-intensive operations in the background
    
    
    
        BOOL needsCompleteReload = YES;
    
    
        if(notification.userInfo && notification.userInfo[@"itemID"]){
            
            NSNumber *itemIndex = notification.userInfo[@"itemID"];
            
            if([self.reloadIgnoringItemIDs containsObject:itemIndex]){ // this item is already here, we don't need to reload anything
                // return; // or maybe we do
            }
            // we need to do a smarter reload
            // reload that stuff
            KPLocalItem *updatedItem = self.denormalizedItems[itemIndex][@"item"];
            [updatedItem reload];
            self.denormalizedItems[itemIndex][@"item"] = updatedItem;
            
            KPLocalAsset *originalImage = [KPLocalAsset fetchByLocalID:updatedItem.originalImageAssetID];
            NSArray *comments = [KPLocalComment fetchCommentsForItem:updatedItem];
            
            self.denormalizedItems[itemIndex][@"comments"] = comments;
            self.denormalizedItems[itemIndex][@"originalImage"] = originalImage;
            
            needsCompleteReload = NO;
 
        }
    
    
        if(needsCompleteReload){
            [self calculateDenormalizedItemInfo];
        }
        
        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:item];
        
        if (self.kid != nil && item != nil) {
            NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];
            self.kid = itemKids[0];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadData];
        });
    });
}

- (void)itemAddedFromServer:(NSNotification *)notification {
    if ([[UserManager sharedInstance] isAfterLogin]) {
        return;
    }

    KPLocalKid *kid = (KPLocalKid *) notification.object;
    if (self.kid == nil || self.kid == kid) {
        [self calculateDenormalizedItemInfo];
        [self reloadData];
    }
}

- (void)itemDeleted:(NSNotification *)notification {
    [self calculateDenormalizedItemInfo];
    [self reloadData];
}


- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    return NO;
}

- (void)smartLoadImage:(UITableViewCell *)cell indexInCell:(NSInteger)indexInCell withAsset:(Asset *)asset andIndexPath:(NSIndexPath *)indexPath {
    UIImageView *coverLayer = nil;
    UIActivityIndicatorView *av = nil;

    if ([cell.contentView.subviews count] > 11) {
        coverLayer = (UIImageView *) cell.contentView.subviews[11];
        av = (UIActivityIndicatorView *) cell.contentView.subviews[12];
    }

    UIImageView *img = (UIImageView *) cell.contentView.subviews[indexInCell];
    if (self.listMode == 1) {
        av = (UIActivityIndicatorView *) img.subviews[1];
    }

    //if there's a downloader downloading prev asset that was at the cell then cancel it
    NSUInteger prevAssetId = img.tag;
    ServerComm *sc = (ServerComm *) [self.downloaders valueForKey:[NSString stringWithFormat:@"%lu", (unsigned long) prevAssetId]];
    if (sc != nil && self.listMode == 0) {
        [sc cancelOperation];
        //DDLogInfo(@"CANCELLING:%d", prevAssetId);
    }

    img.tag = asset.assetId.intValue;
    UIImage *cachedImage = [self.imageCache objectForKey:[NSString stringWithFormat:@"%d_%2f_%2f", asset.assetId.intValue, img.frame.size.width, img.frame.size.height]];
    if (cachedImage != nil) {
        img.image = cachedImage;
        img.alpha = 0;
        coverLayer.alpha = 1.0;
        [av stopAnimating];

        [UIView animateWithDuration:0.5 animations:^{
            img.alpha = 1;
            coverLayer.alpha = 0;
        }                completion:^(BOOL finished) {
        }];
    }
    else {
        img.image = nil;

        if (self.listMode == 1) {
            [img.layer setBorderWidth:4.0];
        } else {
            [img.layer setBorderWidth:10.0];
        }

        [img.layer setBorderColor:[UIColorFromRGB(0xffffff) CGColor]];
        [img.layer setShadowColor:[UIColorFromRGB(0x000000) CGColor]];

        CGMutablePathRef path = CGPathCreateMutable();
        CGPathAddRect(path, NULL, CGRectMake(0, 0, img.frame.size.width, img.frame.size.height));
        img.layer.shadowPath = path;
        img.layer.shadowOpacity = 0.3;
        img.layer.shadowOffset = CGSizeMake(1.0, 1.0);
        img.layer.shadowRadius = 0.5;
        CFRelease(path);

        /*
         [img.layer setShadowOffset:CGSizeMake(10.0, 10.0)];
         [img.layer setShadowRadius:10.0];
         */

        if (self.downloaders == nil) {
            self.downloaders = [[NSMutableDictionary alloc] init];
        }

        ServerComm *sc = [ServerComm instance];
        [self.downloaders setValue:sc forKey:[NSString stringWithFormat:@"%d", asset.assetId.intValue]];

        [self.imageDownloadingQueue addOperationWithBlock:^{
            [sc downloadAssetFile:asset withSuccessBlock:^(id result) {

                UIImage *productImage = [[[UIImage alloc] initWithContentsOfFile:result] imageScaledToFitSize:img.frame.size];

                if (productImage) {
                    //if (self.listMode != 1)
                    //productImage = [productImage generatePhotoStackWithImage];

                    [self.imageCache setObject:productImage forKey:[NSString stringWithFormat:@"%d_%2f_%2f", asset.assetId.intValue, img.frame.size.width, img.frame.size.height]];

                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        // make sure the cell is still visible
                        UITableViewCell *updateCell = [self.tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell) {
                            UIImageView *updatedImageView = (UIImageView *) cell.contentView.subviews[indexInCell];
                            if (updatedImageView.tag == asset.assetId.intValue) {
                                updatedImageView.alpha = 0;
                                updatedImageView.image = productImage;
                                coverLayer.alpha = 1.0;
                                [av stopAnimating];
                                [UIView animateWithDuration:0.5 animations:^{
                                    updatedImageView.alpha = 1;
                                    coverLayer.alpha = 0;
                                }                completion:^(BOOL finished) {
                                }];
                            }
                        }
                    }];

                    [self.downloaders removeObjectForKey:[NSString stringWithFormat:@"%d", asset.assetId.intValue]];

                }
            }        andFailBlock:^(NSError *error) {
                [self.downloaders removeObjectForKey:[NSString stringWithFormat:@"%d", asset.assetId.intValue]];
            }];
        }];
    }
}

- (void)showFullscreen:(NSIndexPath *)indexPath withMode:(NSInteger)mode andCommentIndex:(NSInteger)commentIndex {
    NSUInteger aindex = indexPath.row;

    NSMutableArray *arr2 = [[NSMutableArray alloc] init];
    NSLog(@"FNTRACE showfullscreen ListMode: %@", self.listMode);
    
    if (self.listMode == 1) {
        int j = 0;
        int s = 0;
        for (NSArray *barr in self.items) {
            int i = 0;
            for (Item *item in barr) {
                NSLog(@"FNTRACE showfullscreen obj1: %@", item);
                [arr2 addObject:item];
                if (i == indexPath.row && s == indexPath.section) {
                    aindex = j;
                }

                i++;
                j++;
            }
            s++;
        }
    }
    else {
        NSLog(@"FNTRACE showfullscreen obj2: %@", self.items);
        [arr2 addObjectsFromArray:self.items];
    }

    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:arr2.count];
    for (Item *item in arr2) {
//        if (item.itemType.intValue == 4 && (item.originalImage.assetStatus == nil || (item.originalImage.assetStatus != nil && (item.originalImage.assetStatus.intValue == 0 || item.originalImage.assetStatus.intValue == 1)))) {
//            aindex--;
//            continue;
//        }
        [arr addObject:item];
    }
    
    NSLog(@"FNTRACE showfullscreen1: %@", arr);
    NSLog(@"FNTRACE showfullscreen1: %@", indexPath);
    NSLog(@"FNTRACE showfullscreen2: %lu", aindex);

    FullScreenContainerViewController *avc = [[FullScreenContainerViewController alloc] initWithItems:arr andIndex:aindex withCommentIndex:commentIndex];
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    //[d.window setRootViewController:avc];
    [d.navController presentViewController:avc animated:YES completion:nil];
    
    /*
     [UIView transitionWithView:d.window
     duration:0.5
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
     [[UIApplication sharedApplication] setStatusBarHidden:YES];
     d.window.rootViewController = avc;
     }
     completion:nil];
    */
     

}

- (void)addKeepyBubble:(UIImageView *)img withItem:(KPLocalItem *)item {
    if ([item.cropRect isEqualToString:@""]) {
        return;
    }

    NSArray *arr = [item.cropRect componentsSeparatedByString:@"|"];

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 180, 20)];
    lbl.numberOfLines = 10;
    lbl.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
    lbl.textAlignment = NSTextAlignmentCenter;
    
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16];
    NSMutableString *txt = [[NSMutableString alloc] initWithString:arr[0]];
    [txt replaceOccurrencesOfString:@"\\n" withString:@"\n" options:NSCaseInsensitiveSearch range:(NSRange) {0, txt.length}];

    NSString *boldTxt = @"";
    NSRange startRange = [txt rangeOfString:@"$"];
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [txt length] - targetRange.location;
        NSRange endRange = [txt rangeOfString:@"$" options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            boldTxt = [txt substringWithRange:targetRange];
        }
    }

    [txt replaceOccurrencesOfString:@"$" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, txt.length)];

    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:txt];
    NSRange boldRange = {NSNotFound, 0};
    boldRange = [[mutableAttributedString string] rangeOfString:boldTxt options:NSCaseInsensitiveSearch];
    
    UIFont *boldSystemFont = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef) boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
    if (font) {
        [mutableAttributedString addAttribute:(NSString *) kCTFontAttributeName value:(__bridge id) font range:boldRange];
        CFRelease(font);
    }
    lbl.attributedText = mutableAttributedString;
    
    CGSize textSize = [txt sizeWithFont:lbl.font thatFitsToSize:CGSizeMake(300, 300) lineBreakMode:NSLineBreakByWordWrapping];
    lbl.frame = CGRectMake(10, 10, textSize.width, textSize.height);

    float ah = textSize.height + 21;
    float aw = textSize.width + 20;;

    float ax = (img.frame.size.width - aw) / 2;
    float ay = img.frame.size.height - [arr[1] intValue] - textSize.height - 20;

    if (item.serverID == 325) {
        ay -= 50;
        UIImageView *hintImageView = [[UIImageView alloc] initWithFrame:CGRectMake(60, ay + 90, 133, 29)];
        hintImageView.image = [UIImage imageNamed:@"tap-to-listen"];
        [img addSubview:hintImageView];
    }

    if (ay + ah >= 256) {
        ay -= 54;
    }

    UIImageView *bubble = [[UIImageView alloc] initWithFrame:CGRectMake(ax, ay, aw, ah)];
    bubble.image = [[UIImage imageNamed:@"keepy-bubble"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)];
    [img addSubview:bubble];

    [bubble addSubview:lbl];


}

- (void)itemWasUpdated:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{

    int asection = 0;
    int aindex = -1;

    KPLocalItem *updatedItem = (KPLocalItem *) notification.object;

    /*
     float w = self.view.bounds.size.width;
     int maxCount = (w-20)/96;
     */
    for (int i = 0; i < [self.items count]; i++) {
        if (self.listMode == 1) {
            /*
             NSArray *arr = (NSArray*)[self.items objectAtIndex:i];
             Item *item = [arr objectAtIndex:i + maxCount * indexPath.row];
             */
        }
        else {
            KPLocalItem *item = (KPLocalItem *) self.items[i];
            if (item.serverID == updatedItem.serverID) {
                aindex = i;
                break;
            }
        }
    }
    
    


    if (aindex == -1) {
        return;
    }

    
    [((KPLocalItem *)self.items[aindex]) reload];
    
    
    NSNumber *itemIndex = @(updatedItem.identifier);
    
    // let's re-fetch the related comments
    self.denormalizedItems[itemIndex][@"item"] = self.items[aindex];
    
//    if ((self.denormalizedItems.count > (NSUInteger)itemIndex) && (self.items.count > aindex)) {
//       self.denormalizedItems[itemIndex][@"item"] = self.items[aindex];
//    }
        
    KPLocalAsset *originalImage = [KPLocalAsset fetchByLocalID:updatedItem.originalImageAssetID];
    NSArray *comments = [KPLocalComment fetchCommentsForItem:updatedItem];
        
    self.denormalizedItems[itemIndex][@"comments"] = comments;
    self.denormalizedItems[itemIndex][@"originalImage"] = originalImage;
    
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:aindex inSection:asection];
   // [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
     
        
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.noOfRowsReturned > indexPath.row) {
            [self.tableView scrollToRowAtIndexPath: indexPath atScrollPosition:UITableViewScrollPositionTop  animated:NO];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    });
        
//    if(self.items.count > aindex) {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:aindex inSection:asection];
//        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    }
        
        
        
    // [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    // [self.tableView scrollToRowAtIndexPath: indexPath atScrollPosition:UITableViewScrollPositionTop  animated:NO];
    });
    
}

- (void)setIsLockedKeepyFeed:(BOOL)isLockedKeepyFeed {
    _isLockedKeepyFeed = isLockedKeepyFeed;
    
    dispatch_async(dispatch_get_main_queue(), ^{
    self.kidImageBkg.hidden = (isLockedKeepyFeed);
    self.kidImageView.hidden = (isLockedKeepyFeed);
    self.kidsButton.hidden = (isLockedKeepyFeed);
    self.filterButton.hidden = (isLockedKeepyFeed);
    self.kidArrowImageView.hidden = (isLockedKeepyFeed);
    self.pagesLabel.hidden = (isLockedKeepyFeed);
    
    // Modified by Ashvini (commented below line and added notification)
  //  self.calendarViewController.collectionView.hidden = (isLockedKeepyFeed);
   // self.calendarViewController.isLockedKeepyFeed = isLockedKeepyFeed;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"isLockedKeepyFeed" object: [NSNumber numberWithBool:isLockedKeepyFeed]];
    });
    
    self.feedModeButton.hidden = (self.isLockedKeepyFeed || self.kid.serverID == 88);
    self.galleryModeButton.hidden = (self.isLockedKeepyFeed || self.kid.serverID == 88);
    self.lineView.hidden = (self.isLockedKeepyFeed || self.kid.serverID == 88);

    if (isLockedKeepyFeed) {
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//            self.kidNameLabel.frame = CGRectMake(10, 37, 300, 20);
            if ([UIScreen mainScreen].bounds.size.width == 320){
                self.kidNameLabel.frame  = CGRectMake(79, 37, 180, 20);
            } else {
                 self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 37, 180, 20);
                 if ([UIScreen mainScreen].bounds.size.width == 414){
                     self.kidNameLabel.frame  = CGRectMake(79 * 1.1, 37, 180, 20);
                 }
                // Added by Ashvini
                if ([UIScreen mainScreen].bounds.size.height >= 812) {
                    self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 37 + 17, 180, 20);
                }
                //
            }
        } else {
            if ([UIScreen mainScreen].bounds.size.width == 320){
                self.kidNameLabel.frame  = CGRectMake(79, 17, 180, 20);
            } else {
                self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 17, 180, 20);
                if ([UIScreen mainScreen].bounds.size.width == 414){
                    self.kidNameLabel.frame  = CGRectMake(79 * 1.1, 37, 180, 20);
                }
                // Added by Ashvini
                if ([UIScreen mainScreen].bounds.size.height >= 812) {
                    self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 17 + 17, 180, 20);
                }
                //
            }
//            self.kidNameLabel.frame = CGRectMake(10, 17, 300, 20);
        }
        self.kidNameLabel.textAlignment = NSTextAlignmentCenter;
    } else {
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//            self.kidNameLabel.frame = CGRectMake(79, 37, 160, 20);
            if ([UIScreen mainScreen].bounds.size.width == 320){
                self.kidNameLabel.frame  = CGRectMake(79, 37, 180, 20);
            } else {
                self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 37, 180, 20);
                if ([UIScreen mainScreen].bounds.size.width == 414){
                    self.kidNameLabel.frame  = CGRectMake(79 * 1.1, 37, 180, 20);
                }
                 // Added by Ashvini
                if ([UIScreen mainScreen].bounds.size.height >= 812) {
                    self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 37 + 17, 180, 20);
                }
            }
        } else {
            if ([UIScreen mainScreen].bounds.size.width == 320){
                self.kidNameLabel.frame  = CGRectMake(79, 37, 180, 20);
            } else {
                self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 17, 180, 20);
                if ([UIScreen mainScreen].bounds.size.width == 414){
                    self.kidNameLabel.frame  = CGRectMake(79 * 1.1, 37, 180, 20);
                }
                // Added by Ashvini
                if ([UIScreen mainScreen].bounds.size.height >= 812) {
                   self.kidNameLabel.frame  = CGRectMake(79 * 1.15, 17 + 17, 180, 20);
                }
            }
//            self.kidNameLabel.frame = CGRectMake(79, 17, 160, 20);
        }

        self.kidNameLabel.textAlignment = NSTextAlignmentLeft;

        if (self.kid == nil) {
            self.kidNameLabel.text = KPLocalizedString(@"everyone");
        } else {
            self.kidNameLabel.text = [NSString stringWithFormat:@"%@", self.kid.name];
        }
    }
}

#pragma mark - NoteEditDelegate

- (void)noteEditorFinished:(NSString *)noteText withNoteImage:(UIImage *)noteImage andAssetId:(NSInteger)assetId {
    [self.tableView reloadRowsAtIndexPaths:@[self.currentSelectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Filters

- (void)filterTap:(id)sender {
    NSArray *selectedTags = nil;
    if (self.filterTag != nil) {
        selectedTags = @[@(self.filterTag.serverID)];
    }

    TagsViewController *avc = [[TagsViewController alloc] initWithMode:1 andSelectedTags:selectedTags];
    avc.delegate = self;
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController pushViewController:avc animated:YES];
}

#pragma mark - TagViewDelegate

- (void)tagViewClosed:(NSArray *)selectedTags {
    if (selectedTags == nil || [selectedTags count] == 0) {
        self.filterTag = nil;
        self.calendarViewController.isFilterActive = NO;
        //[self.filterButton setTitle:[NSString stringWithFormat:@"\\ %@", KPLocalizedString(@"show all")] forState:UIControlStateNormal];
    }
    else {
        self.filterTag = selectedTags[0];
        self.calendarViewController.isFilterActive = YES;
        //[self.filterButton setTitle:[NSString stringWithFormat:@"\\ %@", self.filterTag.name] forState:UIControlStateNormal];
    }

    [self reloadData];
}

- (void)kidsChanged:(NSNotification *)notification {

    // if it has received something new, we might have to reload the current stuff
    
    if(self.addedKidDuringOnboarding && !self.processedAddedKidDuringOnboarding){
        
        self.processedAddedKidDuringOnboarding = YES;
        
        // we have created a new kid!
        // let's find the youngest one
        NSArray *myKids = [KPLocalKid fetchMine];
        NSSortDescriptor *ageDescriptor = [[NSSortDescriptor alloc] initWithKey:@"birthdate" ascending:NO]; // we want to sort by descending birth date
        NSArray *ageSortedKids = [myKids sortedArrayUsingDescriptors:@[ageDescriptor]];
        
        KPLocalKid *youngestKidWithoutPhotos;
        
        for(KPLocalKid *currentKid in ageSortedKids){
            
            // let's see how many pictures you have
            NSArray *associatedKeepies = [KPLocalKidItem fetchItemIDsForKid:currentKid];
            
            if(!associatedKeepies || associatedKeepies.count == 0){
                youngestKidWithoutPhotos = currentKid;
                break;
            }
            
        }
        
        
        // experimental reactivation of the everyone feed
        // [[UserManager sharedInstance] setSelectedKid:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:CHANGE_KID_NOTIFICATION object:youngestKidWithoutPhotos];
        
    }
    
    
    
    [self calculateDenormalizedItemInfo];
    
    
    KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:self.kid.imageAssetID];
    if (imageAsset != nil) {
        __weak ItemsViewController *_s = self;
        [self.kidImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            NSLog(@"imageasset completed ====");
            
            _s.kidImageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];

        }];
        self.kidNameLabel.text = [NSString stringWithFormat:@"%@", self.kid.name];
        NSLog(@"imageasset label ====");
    }


}

@end
