//
//  ItemsViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/10/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KPLocalKid;
@class KPLocalItem;

@interface ItemsViewController : UIViewController

@property(nonatomic, strong) KPLocalKid *kid;
@property(readonly) KPLocalItem *currentItem;
@property(nonatomic) BOOL isLockedKeepyFeed;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic) BOOL allowRefrash;

@property BOOL addedKidDuringOnboarding;

@property(nonatomic, strong) NSArray *items;
@property(strong, nonatomic) NSDictionary *denormalizedItems;

@property(nonatomic) BOOL *isFirstTimeLoading;

- (void)reloadData;
- (void)galleryModeTap:(id)sender;
- (void)feedModeTap:(id)sender;

@end
