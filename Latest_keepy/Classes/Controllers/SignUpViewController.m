//
//  SignUpViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/8/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "SignUpViewController.h"
#import "CreateFanViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"
#import "KPSQLiteManager.h"

#import "Keepy-Swift.h"

@interface SignUpViewController () <LoginViewDelegate, UITextFieldDelegate>

@property(nonatomic, weak) IBOutlet UIView *backgroundColor;
@property(nonatomic, weak) IBOutlet UIImageView *background;
@property(nonatomic, weak) IBOutlet UIImageView *gradient;
@property(nonatomic, weak) IBOutlet UIImageView *logo;
@property(nonatomic, weak) IBOutlet UILabel *tagLine;
@property(nonatomic, weak) IBOutlet UIButton *facebook;
@property(nonatomic, weak) IBOutlet UIButton *email;
@property(nonatomic, weak) IBOutlet UILabel *alreadyAMember;
@property(nonatomic, weak) IBOutlet UIButton *loginNow;
@property(nonatomic, weak) IBOutlet UIView *controls;
@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UITextField *emailField;
@property(nonatomic, weak) IBOutlet UIImageView *emailFieldBackground;
@property(nonatomic, weak) IBOutlet UIButton *continueButton;
@property(nonatomic, weak) IBOutlet UIView *emailControls;
@property(nonatomic, assign) SEL selector;
@property(nonatomic, strong) NSDictionary *fan;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property(nonatomic, strong) NSDictionary *facebookData;
@property(nonatomic, strong) NSString *facebookAccessToken;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintOfEmailControl;

@end

@implementation SignUpViewController

@synthesize index = _index;

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"open screen"];
    [[Mixpanel sharedInstance] track:@"onboarding-splash-open"];


    self.title = KPLocalizedString(@"back");
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        
        
        self.logo.center = CGPointMake(self.logo.center.x, self.logo.center.y + 35.0f);
        self.tagLine.center = CGPointMake(self.tagLine.center.x, self.tagLine.center.y + 35.0f);
        
        self.controls.center = CGPointMake(self.controls.center.x, self.controls.center.y + 5.0f);
        self.alreadyAMember.center = CGPointMake(self.alreadyAMember.center.x, self.alreadyAMember.center.y + 5.0f);
        self.loginNow.center = CGPointMake(self.loginNow.center.x, self.loginNow.center.y + 5.0f);
    }
    

    self.cancelButton.alpha = 0.0f;
    [self.facebook setTitle:KPLocalizedString(@"       Sign up with Facebook") forState:UIControlStateNormal];
    
    self.facebook.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:17];
    [self.email setTitle:KPLocalizedString(@"Sign up with email") forState:UIControlStateNormal];
    self.email.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:17];

    [self.email setTitleColor:UIColorFromRGB(0x43B1C0) forState:UIControlStateNormal];
    

    
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:13.0f];
    self.tagLine.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:18.0f];
    self.alreadyAMember.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:13.0f];
    self.loginNow.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:13.0f];
//    self.emailFieldBackground.image = [[UIImage imageNamed:@"input"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 33.0f, 0.0f, 33.0f) resizingMode:UIImageResizingModeStretch];
    self.emailField.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:17];

    UIColor *color = [UIColor whiteColor];
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:KPLocalizedString(@"Enter your email") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:@"ARSMaquettePro-LightItalic" size:16]}];
    
    [self.continueButton setBackgroundImage:[[UIImage imageNamed:@"button"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 33.0f, 0.0f, 33.0f) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    self.continueButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];

    self.emailControls.alpha = 0.0f;
    self.controls.alpha = 0.0f;
    self.controls.center = CGPointMake(self.controls.center.x, self.controls.center.y + 5.0f);
    
    UILongPressGestureRecognizer* gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onViewLongPressed:)];
    gesture.minimumPressDuration = 2.0;
    gesture.numberOfTouchesRequired = 2;
    [self.view addGestureRecognizer:gesture];
}

- (void)revealControls {
    [UIView animateWithDuration:0.6f animations:^{
        self.controls.center = CGPointMake(self.controls.center.x, self.controls.center.y - 5.0f);
        self.controls.alpha = 1.0f;
        self.tagLine.alpha = 1.0f;
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.navigationController setNavigationBarHidden:YES];
    if (self.navigationController.viewControllers.count > 2)//means we came from the welcome sliders
    {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    
    if (self.controls.alpha == 0.0f) {
        [self performSelector:@selector(revealControls) withObject:nil afterDelay:0.3f];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)facebookToken:(NSString*)token {
    self.facebookAccessToken = token;
    
    [[ServerComm instance] fblogin:token
                  withSuccessBlock:^(id result) {
                      if ([[result valueForKey:@"status"] intValue] == 0) {
                          [[Mixpanel sharedInstance] track:@"onboarding-splash-facebook-existing-user"];
                          
                          result = [result valueForKey:@"result"];
                          [GlobalUtils hideAllTips];
                          [self didLogin:result];
                      } else {
                          [self signUpNew];
                      }
                  }
                      andFailBlock:^(NSError *error) {
                          DDLogInfo(@"%@", error.localizedDescription);
                          [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
                      }
     ];
}

- (IBAction)signInWithFacebook:(id)sender {

    [[Mixpanel sharedInstance] track:@"Facebook tap" properties:@{@"source" :@"pre-signup"}];
    [[Mixpanel sharedInstance] track:@"onboarding-splash-facebook-tap"];
    
    
    __weak SignUpViewController *weakSelf = self;
    
    FBSDKLoginManager* fb = [FBSDKLoginManager new];
    [fb logOut];
    [fb logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: %@", error);
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
        } else if (result.isCancelled) {
            DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Cancelled");
        } else {
            DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Logged in");
            [weakSelf facebookToken:result.token.tokenString];
        }
        
    }];
    //    [fb logInWithReadPermissions: @[@"email"]
    //#if FBSDK_46
    //              fromViewController:self
    //#endif
    //                         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
    //                             if (error) {
    //                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: %@", error);
    //                                 [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
    //                             } else if (result.isCancelled) {
    //                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Cancelled");
    //                             } else {
    //                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Logged in");
    //                                 [weakSelf facebookToken:result.token.tokenString];
    //                             }
    //                         }
    //     ];
}

- (IBAction)login:(id)sender {

    [[Mixpanel sharedInstance] track:@"Login tap"];
    [[Mixpanel sharedInstance] track:@"onboarding-splash-login-tap"];

    LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    loginViewController.delegate = self;
    [self.navigationController pushViewController:loginViewController animated:YES];
}

- (void)didLogin:(NSDictionary *)userInfo {



    [GlobalUtils hideAllTips];
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];

    // let's create the SQLite data store
    [appDelegate initializeDataStoreForEmail:userInfo[@"email"]];

    [appDelegate createHome:YES];
    [appDelegate.navController setViewControllers:[NSArray arrayWithObject:appDelegate.viewController] animated:YES];
    [[UserManager sharedInstance] setIsAfterLogin:YES];
    [GlobalUtils saveUser:userInfo];
}

- (void)signUpNew {
    __weak SignUpViewController* weakSelf = self;

    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id fbresult, NSError *error) {
         if (error) {
             [[Mixpanel sharedInstance] track:@"onboarding-splash-facebook-login-error"];
             [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
             DDLogInfo(@"signUpNew error: %@", error);
             return;
         }
         
         DDLogInfo(@"signUpNew me: %@", fbresult);
         weakSelf.facebookData = fbresult;
         
         [[ServerComm instance] getFanByEmail:fbresult[@"email"]
                                 successBlock:^(id result) {
                                     [SVProgressHUD dismiss];
                                     id fan = [result objectForKey:@"result"];
                                     if (fan != [NSNull null]) {
                                         [[Mixpanel sharedInstance] track:@"onboarding-splash-facebook-create-fan"];
                                         
                                         weakSelf.fan = (NSDictionary *) fan;
                                         [weakSelf performSegueWithIdentifier:@"CreateFan" sender:weakSelf];
                                     } else {
                                         [[Mixpanel sharedInstance] track:@"onboarding-splash-facebook-registration"];
                                         
                                         RegisterViewController* vc = [[RegisterViewController alloc] initWithFacebook:fbresult withAccessToken:weakSelf.facebookAccessToken];
                                         [weakSelf.navigationController pushViewController:vc animated:YES];
                                     }
                                 }                          failBlock:^(NSError *error) {
                                     [[Mixpanel sharedInstance] track:@"onboarding-splash-facebook-fan-check-fail"];
                                     
                                     [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy, please try again soon")];
                                 }
          ];
     }];
}

- (IBAction)email:(id)sender {
    [[Mixpanel sharedInstance] track:@"onboarding-splash-email-tap"];

    __weak SignUpViewController *weakSelf = self;
    [UIView animateWithDuration:0.6f animations:^{
        weakSelf.controls.center = CGPointMake(weakSelf.controls.center.x, weakSelf.controls.center.y + 5.0f);
        weakSelf.controls.alpha = 0.0f;
        weakSelf.tagLine.alpha = 0.0f;
    }                completion:^(BOOL finished) {
        if (finished) {
            [[NSNotificationCenter defaultCenter] addObserver:weakSelf selector:@selector(onKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
            [weakSelf.emailField becomeFirstResponder];
        }
    }];
}

- (void)onKeyboardWillShow:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];

    NSDictionary *userInfo = notification.userInfo;
    NSValue *endFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [self.view convertRect:endFrameValue.CGRectValue fromView:nil];
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;

    CGFloat iphone4padding;
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        iphone4padding = 50.0f;
    }
    else{
        iphone4padding = 0.0f;
    }
    
    __weak SignUpViewController *weakSelf = self;
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        //weakSelf.logo.center = CGPointMake(weakSelf.logo.center.x, weakSelf.logo.center.y - 148.0f);
        weakSelf.backgroundColor.center = CGPointMake(weakSelf.backgroundColor.center.x, weakSelf.backgroundColor.center.y - keyboardEndFrame.size.height);
        weakSelf.background.center = CGPointMake(weakSelf.background.center.x, weakSelf.background.center.y - 346.0f);
        weakSelf.backgroundImage.center = CGPointMake(weakSelf.backgroundImage.center.x, weakSelf.backgroundImage.center.y + 346.0f);
        weakSelf.gradient.center = CGPointMake(weakSelf.gradient.center.x, weakSelf.gradient.center.y - 346.0f);
//        weakSelf.emailControls.center = CGPointMake(weakSelf.emailControls.center.x, weakSelf.emailControls.center.y - keyboardEndFrame.size.height - iphone4padding);
//        weakSelf.topConstraintOfEmailControl.constant = (weakSelf.emailControls.center.y - keyboardEndFrame.size.height - iphone4padding) / 2;

        weakSelf.cancelButton.alpha = 1.0f;
        weakSelf.emailControls.alpha = 1.0f;
    }                completion:^(BOOL finished) {
        if (finished) {
            [[Mixpanel sharedInstance] track:@"onboarding-email-open"];

            UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:weakSelf action:@selector(swipe:)];
            swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
            [weakSelf.view addGestureRecognizer:swipeGesture];
        }
    }];
}

- (void)swipe:(UISwipeGestureRecognizer *)recognizer {
    [self.view removeGestureRecognizer:recognizer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self.emailField resignFirstResponder];
}

- (IBAction)signUpExit:(UIStoryboardSegue *)segue {

}

- (IBAction)cancel:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self.emailField resignFirstResponder];
}

- (void)onKeyboardWillHide:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

    NSDictionary *userInfo = notification.userInfo;
    NSValue *beginFrameValue = userInfo[UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardBeginFrame = [self.view convertRect:beginFrameValue.CGRectValue fromView:nil];
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;

    CGFloat iphone4padding;
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        iphone4padding = 50.0f;
    }
    else{
        iphone4padding = 0.0f;
    }
    
    
    __weak SignUpViewController *weakSelf = self;
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        //weakSelf.logo.center = CGPointMake(weakSelf.logo.center.x, weakSelf.logo.center.y + 148.0f);
        weakSelf.backgroundColor.center = CGPointMake(weakSelf.backgroundColor.center.x, weakSelf.backgroundColor.center.y + keyboardBeginFrame.size.height);
        weakSelf.background.center = CGPointMake(weakSelf.background.center.x, weakSelf.background.center.y + 346.0f);
        weakSelf.backgroundImage.center = CGPointMake(weakSelf.backgroundImage.center.x, weakSelf.backgroundImage.center.y - 346.0f);
        weakSelf.gradient.center = CGPointMake(weakSelf.gradient.center.x, weakSelf.gradient.center.y + 346.0f);
//        weakSelf.emailControls.center = CGPointMake(weakSelf.emailControls.center.x, weakSelf.emailControls.center.y + keyboardBeginFrame.size.height + iphone4padding);
//        weakSelf.topConstraintOfEmailControl.constant = ( weakSelf.emailControls.center.y - keyboardBeginFrame.size.height + iphone4padding)/2;
        weakSelf.cancelButton.alpha = 0.0f;
        weakSelf.emailControls.alpha = 0.0f;
    }                completion:^(BOOL finished) {
        if (finished) {
            if (weakSelf.selector) {
                [weakSelf performSelector:weakSelf.selector withObject:nil afterDelay:0.0f];
                weakSelf.selector = nil;
            } else {
                [UIView animateWithDuration:0.6f animations:^{
                    weakSelf.controls.center = CGPointMake(weakSelf.controls.center.x, weakSelf.controls.center.y - 5.0f);
                    weakSelf.controls.alpha = 1.0f;
                    weakSelf.tagLine.alpha = 1.0f;
                }                completion:nil];
            }
        }
    }];
}

- (IBAction)textFieldChanged:(id)sender {


    //Convert Every Char to lower case and then check if valid
    self.emailField.text = [self.emailField.text lowercaseString];
    if (self.emailField.text.isEmailAddress) {
        self.continueButton.enabled = YES;
    } else {
        self.continueButton.enabled = NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newText = [textField.text stringByAppendingString:string];
    if (newText.length > 254 && range.length == 0) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.emailField.text.isEmailAddress) {
        [self next:nil];
        return NO;
    } else {
        return NO;
    }
}

- (IBAction)next:(id)sender {
    if (self.emailField.text.isEmailAddress) {
        __weak SignUpViewController *weakSelf = self;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.cancelButton.enabled = NO;
        self.continueButton.enabled = NO;
        [[ServerComm instance] getFanByEmail:self.emailField.text successBlock:^(id result) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            weakSelf.cancelButton.enabled = YES;
            weakSelf.continueButton.enabled = YES;

            id fan = [result objectForKey:@"result"];
            if (fan != [NSNull null]) {
                weakSelf.fan = (NSDictionary *) fan;
                self.selector = @selector(createFan);
            } else {
                self.selector = @selector(goToRegistration);
            }

            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
            [weakSelf.emailField resignFirstResponder];

        }                          failBlock:^(NSError *error) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            weakSelf.cancelButton.enabled = YES;
            weakSelf.continueButton.enabled = YES;
            [[Mixpanel sharedInstance] track:@"onboarding-email-fan-check-fail"];

            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy, please try again soon")];
        }];
    }
}

- (void)createFan {

    [[Mixpanel sharedInstance] track:@"onboarding-email-create-fan"];

    [self performSegueWithIdentifier:@"CreateFan" sender:self];
}

- (void)goToRegistration {

    [[Mixpanel sharedInstance] track:@"onboarding-email-registration"];

    RegisterViewController *registerViewController = [[RegisterViewController alloc] initWithEmail:self.emailField.text];



    [self.navigationController pushViewController:registerViewController animated:YES];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CreateFan"]) {
        CreateFanViewController *createFanViewController = (CreateFanViewController *) segue.destinationViewController;
        createFanViewController.fan = self.fan;
        if (self.facebookData) {
            createFanViewController.facebookData = self.facebookData;
            createFanViewController.facebookAccessToken = self.facebookAccessToken;
            createFanViewController.email = [self.facebookData valueForKey:@"email"];
        } else {
            createFanViewController.email = self.emailField.text;
        }
    }
}

- (void)onViewLongPressed:(UIGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        NSLog(@"open DevConf");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DevConf" bundle:nil];
        UIViewController* vc = [storyboard instantiateInitialViewController];
        [self presentViewController:vc animated:YES completion:NULL];
    }
}

@end
