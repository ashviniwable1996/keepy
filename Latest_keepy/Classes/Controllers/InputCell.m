//
//  InputCell.m
//  Keepy
//
//  Created by Troy Payne on 2/12/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "InputCell.h"

@interface InputCell ()

@property(nonatomic, weak) IBOutlet UIImageView *backgroundImageView;

@end

@implementation InputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundImageView.image = [[UIImage imageNamed:@"input"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 33.0f, 0.0f, 33.0f) resizingMode:UIImageResizingModeStretch];
    self.input.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
}

+ (CGFloat)height {
    return 44.0f;
}

@end
