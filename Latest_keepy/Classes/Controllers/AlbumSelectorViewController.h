//
//  AlbumSelectorViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//
@import Photos;

@protocol AlbumSelectorViewControllerDelegate;

@interface AlbumSelectorViewController : UIViewController

@property(nonatomic, weak) id <AlbumSelectorViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *albums;

@end

@protocol AlbumSelectorViewControllerDelegate <NSObject>

//- (void)pictures:(ALAssetsGroup *)group;
- (void)pictures:(PHCollection *)collection;

@end

@interface AlbumCell : UITableViewCell

//@property(nonatomic, strong) ALAssetsGroup *group;
@property(nonatomic, strong) PHAssetCollection *collection;

+ (CGFloat)height;

- (void)layout;

@end
