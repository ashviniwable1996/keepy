#import "DatePickerViewController.h"

@interface DatePickerViewController ()
@property(strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property(nonatomic, strong) NSDate *date;
@end

@implementation DatePickerViewController
@synthesize datePicker = _datePicker;
@synthesize delegate = _delegate;
@synthesize date = _date;

+ (DatePickerViewController *)datePickerViewControllerWithDelegate:(id <DatePickerViewControllerDelegate>)delegate date:(NSDate *)date {
    DatePickerViewController *instance = [[DatePickerViewController alloc] initWithNibName:@"DatePickerViewController" bundle:nil];
    instance.delegate = delegate;
    instance.date = date;
    return instance;

}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Maximum date
    NSDateComponents *maximumDateComponent = [[NSDateComponents alloc] init];
    //[maximumDateComponent setYear:-13];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // Minus 13 years from the current date
    self.datePicker.maximumDate = [calendar dateByAddingComponents:maximumDateComponent
                                                            toDate:[NSDate date]
                                                           options:0];

    if (self.date) {
        self.datePicker.date = self.date;
    }
}

- (IBAction)datePickerValueDidChange:(UIDatePicker *)datePicker {
    self.date = datePicker.date;
    [self.delegate datePickerValueDidChange:self.date];
}

- (CGSize)contentSizeForViewInPopover {
    return CGSizeMake(320, 216);
}

- (void)viewDidUnload {
    [self setDatePicker:nil];
    [super viewDidUnload];
}
@end
