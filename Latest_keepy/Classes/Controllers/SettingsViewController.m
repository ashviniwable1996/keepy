//
//  SettingsViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 4/2/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>



#import "SettingsViewController.h"
#import "RegisterViewController.h"
#import "KidsListViewController.h"
#import "FansViewController.h"
#import "UserVoice.h"
#import "SignUpViewController.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>
#import "AccountViewController.h"
#import "KPWebViewController.h"
#import "KPNotification.h"
#import "Fan.h"
#import "Story.h"
#import "Comment.h"
#import "AppDelegate.h"
#import "OnBoardingScrollViewController.h"

#import "Keepy-Swift.h"

@interface SettingsViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *menus;
@property(nonatomic, strong) NSArray *titles;
@property(nonatomic, assign) BOOL isUnlimitedButtonHidden;

@end

@implementation SettingsViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self.navigationController setNavigationBarHidden:NO];
    
    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"settings", @"source", nil]];
    
    
    UIColor *patternColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
    self.view.backgroundColor = patternColor;
    self.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height);

    self.title = KPLocalizedString(@"settings");
    
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"close"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];
    
    self.titles = [NSArray arrayWithObjects:KPLocalizedString(@"my profile"), KPLocalizedString(@"keep up with us on"), KPLocalizedString(@"keepy"), nil];
    
    self.menus = [[NSMutableArray alloc] initWithObjects:
                  [NSArray arrayWithObjects:@"gounlimitedimage", KPLocalizedString(@"my profile"), KPLocalizedString(@"my kids"), KPLocalizedString(@"my fans"), KPLocalizedString(@"my account"),KPLocalizedString(@"logout"),  nil],
                  [NSArray arrayWithObjects:@"facebook", @"pinterest", @"twitter", @"keepy video", nil],
                  [NSArray arrayWithObjects:KPLocalizedString(@"about"), KPLocalizedString(@"faq"), KPLocalizedString(@"terms"), KPLocalizedString(@"privacy policy"), KPLocalizedString(@"send feedback"), nil], nil];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollsToTop = NO;
    self.tableView.backgroundView = nil;
    
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    self.tableView.separatorColor = [UIColor colorWithRed:0.447 green:0.361 blue:0.325 alpha:1];
    [self.view addSubview:self.tableView];
    
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dropBoxRefreshed:) name:DROPBOX_REFRESHED_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(planRefreshed:) name:PLAN_REFRESHED_NOTIFICATION object:nil];
    
    self.isUnlimitedButtonHidden = NO;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (Defaults.planType == 1 || Defaults.planType == 2) {
        [self hideUnlimitedButton];
    } else {
        [self showUnlimitedButton];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if( indexPath.row == 0 && indexPath.section == 0 && !self.isUnlimitedButtonHidden){
        if ([UIScreen mainScreen].bounds.size.width > 320){
            return 90;
        }
        return 76;
    }
    else{
        return 44;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.menus count]; // +1 for the image on top.
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.menus objectAtIndex:section] count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 223, 30)];
    lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    lbl.text = [self.titles objectAtIndex:section];
    
    [aview addSubview:lbl];
    
    return aview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 3) {
        return 30;
    } else {
//        return 0;
        return CGFLOAT_MIN;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section != 3) {
        return nil;
    }
    
    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
    
    NSBundle* mainBundle = [NSBundle mainBundle];
    NSString* appVersion = [mainBundle objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString* buildNumber = [mainBundle objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
#if DEBUG
    NSString* env = @"d";
#elif ADHOC
    NSString* env = @"a";
#else
    NSString* env = @"";
#endif
    lbl.text = [NSString stringWithFormat:@"keepy v%@ (%@%@)%@", appVersion, buildNumber, env, Defaults.isDevMode ? @" dev" : @""];
    
    [aview addSubview:lbl];
    
    // Add DevConf backdoor.
    UILongPressGestureRecognizer* gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onVersionLongPressed:)];
    gesture.minimumPressDuration = 2.0;
    gesture.numberOfTouchesRequired = 2;
    lbl.userInteractionEnabled = YES;
    [lbl addGestureRecognizer:gesture];
    
    return aview;
}

// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.row == 0 && indexPath.section == 0 && !self.isUnlimitedButtonHidden){
        
        NSString *cellIdentifier = @"UTGoUnlimitedCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, cell.frame.size.height);
            
            //full size image for the first cell.
            UIImageView *imgUT = [[UIImageView alloc] init];
            if ([UIScreen mainScreen].bounds.size.width > 320){
                imgUT.frame = CGRectMake(0, 0, cell.bounds.size.width, cell.bounds.size.width * 90 /cell.bounds.size.width);
            } else {
                imgUT.frame = CGRectMake(0, 0, cell.bounds.size.width, cell.bounds.size.width * 74 /cell.bounds.size.width);
            }
           
            imgUT.image = [UIImage imageNamed:@"UTSave"];
            [cell.contentView addSubview:imgUT];
            
        }
        return cell;
        
    }
    else
    {
        
        NSString *cellIdentifier = @"MenuCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, cell.frame.size.height);
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 223, 44)];
            lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
            lbl.textAlignment = NSTextAlignmentLeft;
            lbl.backgroundColor = [UIColor clearColor];
            lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:16];
            [cell.contentView addSubview:lbl];
            
            //arrow
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(cell.bounds.size.width - 40, (cell.bounds.size.height - 14) / 2, 10, 14)];
            img.image = [UIImage imageNamed:@"settings-arrow"];
            [cell.contentView addSubview:img];
            
            //line
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 43, [UIScreen mainScreen].bounds.size.width - 10, 1)];
            //lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
            [cell.contentView addSubview:lineView];
            
            //icon
            img = [[UIImageView alloc] initWithFrame:CGRectMake(10, (cell.bounds.size.height - 25) / 2, 25, 25)];
            [cell.contentView addSubview:img];
            
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSString *bkgImageStr = @"";
        if (indexPath.row == 0) {
            bkgImageStr = @"settings-cell-top";
        } else if (indexPath.row == [[self.menus objectAtIndex:indexPath.section] count] - 1) {
            bkgImageStr = @"settings-cell-bottom";
        }
        
        UIImageView *bkgView = [[UIImageView alloc] initWithFrame:cell.bounds];
        
        if ([bkgImageStr isEqualToString:@""]) {
            bkgView.backgroundColor = [UIColor whiteColor];
        }
        else {
            bkgView.image = [[UIImage imageNamed:bkgImageStr] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
        }
        cell.backgroundView = bkgView;
        
        [self configureCell:cell withIndexPath:indexPath];
        
        return cell;
    }
}

- (void)configureCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    
    
    UILabel *titleLbl = (UILabel *) [cell.contentView.subviews objectAtIndex:0];
    UIImageView *arrowImg = (UIImageView *) [cell.contentView.subviews objectAtIndex:1];
    arrowImg.hidden = ((indexPath.section == 0 && indexPath.row == 3) || (indexPath.section == 1 && indexPath.row == 1));
    UIImageView *lineImage = (UIImageView *) [cell.contentView.subviews objectAtIndex:2];
    lineImage.hidden = (indexPath.row == [[self.menus objectAtIndex:indexPath.section] count] - 1);
    
    
//    
//    if (indexPath.section == 1 && indexPath.row == 1) {
//        if (![[DBSession sharedSession] isLinked]) {
//            titleLbl.text = KPLocalizedString(@"link Dropbox account");
//        } else {
//            titleLbl.text = KPLocalizedString(@"unlink Dropbox account");
//        }
//    }
//    else {
        titleLbl.text = [[self.menus objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
//    }
    
    CGRect aframe = titleLbl.frame;
    aframe.origin.x = 10;
    
    UIImageView *iconImage = (UIImageView *) [cell.contentView.subviews objectAtIndex:3];
    iconImage.hidden = (indexPath.section != 1);
    arrowImg.hidden = YES;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (!iconImage.hidden) //social networks
    {
        //arrowImg.hidden = YES;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (indexPath.row == 3) {
            iconImage.image = [UIImage imageNamed:@"youTube-color"];
        } else {
            iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-color", [[self.menus objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]]];
        }
        
        aframe.origin.x = 50;
    }
    titleLbl.frame = aframe;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = indexPath.row;
    
    if (self.isUnlimitedButtonHidden) {
        row++;
    }
    
    
    if (indexPath.section == 0 && row == 0) {
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"updagrade-settings-btn", @"btn", nil]];
        
        UnlimitedViewController *avc = [[UIStoryboard storyboardWithName:@"Unlimited" bundle:nil] instantiateViewControllerWithIdentifier:@"UnlimitedTable"];
        [self.navigationController pushViewController:avc animated:YES];

    }

    else if (indexPath.section == 0 && row == 1) {
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-profile-settings-btn", @"btn", nil]];
        
        RegisterViewController *avc = [[RegisterViewController alloc] initWithMode:1];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else if (indexPath.section == 0 && row == 2) {
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-kids-settings-btn", @"btn", nil]];
        
        KidsListViewController *avc = [[KidsListViewController alloc] initWithNibName:@"KidsListViewController" bundle:nil];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else if (indexPath.section == 0 && row == 3) {
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fans-settings-btn", @"btn", nil]];
        
        if (![GlobalUtils checkCanInviteFans]) {
            return;
        }
        
        FansViewController *avc = [[FansViewController alloc] initWithNibName:@"FansViewController" bundle:nil];
        [self.navigationController pushViewController:avc animated:YES];
        //[SVProgressHUD showError:@"coming soon"];
    }
    else if (indexPath.section == 0 && row == 4){
        NSMutableDictionary *properties = [NSMutableDictionary dictionary];
        [properties setValue:@"settings" forKey:@"source"];
        [[Mixpanel sharedInstance] track:@"my-keepies-opened" properties:properties];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-keepies-settings-btn", @"btn", nil]];
        
        AccountViewController *avc = [[AccountViewController alloc] initWithNibName:@"AccountViewController" bundle:nil];
        [self.navigationController pushViewController:avc animated:YES];

    }
    else if (indexPath.section == 0 && row == 5) {
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"logout-settings-btn", @"btn", nil]];
        
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"logout", comment: @"")
                                                                    message:NSLocalizedString(@"are you sure?", comment: @"")
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"no" style:UIAlertActionStyleCancel handler:nil]];
        [ac addAction:[UIAlertAction actionWithTitle:@"logout" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self logout];
        }]];
        [self presentViewController:ac animated:YES completion:nil];
    }
//    //dropbox removed
//    else if (indexPath.section == 1 && indexPath.row == 1) {
//        
//        [[Mixpanel sharedInstance] track:@"dropbox tap"];
//        
//        
//        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"dropbox-settings-btn", @"btn", nil]];
//        
//        if (![[DBSession sharedSession] isLinked]) {
//            [[DBSession sharedSession] linkFromController:self];
//        } else {
//            [[DBSession sharedSession] unlinkAll];
//            
//            [[ServerComm instance] updateDropboxCredentials:@"oauth_token= &oauth_token_secret= &uid= " withSuccessBlock:^(id result) {
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:DROPBOX_REFRESHED_NOTIFICATION object:nil];
//                
//            }                                  andFailBlock:^(NSError *error) {
//                
//            }];
//            
//        }
//        
//        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    }
    else if (indexPath.section == 1 && indexPath.row == 0) {
        
        [[Mixpanel sharedInstance] track:@"settings-facebook tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fb-settings-btn", @"btn", nil]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://facebook.com/keepyme"]];
    }
    else if (indexPath.section == 1 && indexPath.row == 1) {
        
        [[Mixpanel sharedInstance] track:@"settings-pinterest tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"pinterest-settings-btn", @"btn", nil]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://pinterest.com/keepyme"]];
    }
    else if (indexPath.section == 1 && indexPath.row == 2) {
        
        [[Mixpanel sharedInstance] track:@"settings-twitter tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"twitter-settings-btn", @"btn", nil]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/keepyme"]];
    }
    else if (indexPath.section == 1 && indexPath.row == 3) {
        
        [[Mixpanel sharedInstance] track:@"settings-keepy-video tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"youtube-settings-btn", @"btn", nil]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.youtube.com/watch?v=-hJyixjhXk0"]];
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        
        [[Mixpanel sharedInstance] track:@"settings-about tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"about-settings-btn", @"btn", nil]];
        
        KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/about.html" andTitle:KPLocalizedString(@"about")];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else if (indexPath.section == 2 && indexPath.row == 1) {
        
        [[Mixpanel sharedInstance] track:@"settings-faq tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"faq-settings-btn", @"btn", nil]];
        
        KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/faqios" andTitle:KPLocalizedString(@"faq")];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else if (indexPath.section == 2 && indexPath.row == 2) {
        
        [[Mixpanel sharedInstance] track:@"settings-terms tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"terms-settings-btn", @"btn", nil]];
        
        KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/terms.html" andTitle:KPLocalizedString(@"terms of use")];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else if (indexPath.section == 2 && indexPath.row == 3) {
        
        [[Mixpanel sharedInstance] track:@"settings-privacy tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"privacy-settings-btn", @"btn", nil]];
        
        
        KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/privacy.html" andTitle:KPLocalizedString(@"privacy policy")];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else if (indexPath.section == 2 && indexPath.row == 4) {
        
        [[Mixpanel sharedInstance] track:@"feedback tap"];
        
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"feedback-settings-btn", @"btn", nil]];
        UVConfig *config = [UVConfig configWithSite:@"keepy.uservoice.com"
                                             andKey:@"xlgORbPAktc4kuuIKvaIA"
                                          andSecret:@"dOMs8y7cMcntxnPyxiJILmGhYZqrJbY7mv5rcfM0" andEmail:[[UserManager sharedInstance] getMe].email andDisplayName:[[UserManager sharedInstance] getMe].name andGUID:[NSString stringWithFormat:@"%d", [[UserManager sharedInstance] getMe].userId.intValue]];
        
        [UserVoice presentUserVoiceInterfaceForParentViewController:self andConfig:config];
        
    }
    
}


- (void)topBtnTap:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"settings", @"source", @"cancel", @"result", nil]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)logout {
    [[Mixpanel sharedInstance] track:@"user logout"];
    
    [SVProgressHUD showWithStatus:KPLocalizedString(@"clearing")];
    
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] clearMemory];
    
    User *me = [[UserManager sharedInstance] getMe];
    [me MR_deleteEntity];
    
    NSArray *kids = [Kid MR_findAll];
    for (Kid *kid in kids) {
        [kid MR_deleteEntity];
    }
    
    NSArray *items = [Item MR_findAll];
    for (Item *item in items) {
        [item MR_deleteEntity];
    }
    
    NSArray *places = [Place MR_findAll];
    for (Place *place in places) {
        [place MR_deleteEntity];
    }
    
    NSArray *assets = [Asset MR_findAll];
    for (Asset *asset in assets) {
        [asset MR_deleteEntity];
    }
    
    NSArray *notifications = [KPNotification MR_findAll];
    for (KPNotification *notification in notifications) {
        [notification MR_deleteEntity];
    }
    
    NSArray *fans = [Fan MR_findAll];
    for (Fan *fan in fans) {
        [fan MR_deleteEntity];
    }
    
    NSArray *comments = [Comment MR_findAll];
    for (Comment *comment in comments) {
        [comment MR_deleteEntity];
    }
    
    NSArray *stories = [Story MR_findAll];
    for (Story *story in stories) {
        [story MR_deleteEntity];
    }
    
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
    
    // Retain some API Endpoints
    NSString* apiEndpoint = Defaults.apiEndpoint;
    NSString* s3URLString = Defaults.s3URLString;
    NSString* assetsURLString = Defaults.assetsURLString;
    
    [Defaults resetStandardUserDefaults];
    
    Defaults.apiEndpoint = apiEndpoint;
    Defaults.s3URLString = s3URLString;
    Defaults.assetsURLString = assetsURLString;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FANS_REFRESHED_NOTIFICATION object:nil];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:CHANGE_KID_NOTIFICATION object:nil];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:KID_SELECT_NOTIFICATION object:nil];
    
    /*
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
     NSString *diskCachePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Assets"];
     
     */
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"firstRun"]){
        [defaults removeObjectForKey:@"firstRun"];
    }
    [[UserManager sharedInstance] logout];
    
    //[[Analytics sharedAnalytics] reset];
    
    [SVProgressHUD dismiss];
    
    [self dismissViewControllerAnimated:YES completion:^{
        //            OpenningViewController *avc = [[OpenningViewController alloc] initWithMode:0];
        //            AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
        //            [d.navController setViewControllers:[NSArray arrayWithObject:avc] animated:YES];
        //            d.viewController = nil;
        
        UIStoryboard *onboardStoryboard = [UIStoryboard storyboardWithName:@"Onboard" bundle:nil];
        OnBoardingScrollViewController *onBoardingScrollViewController = (OnBoardingScrollViewController *) [onboardStoryboard instantiateViewControllerWithIdentifier:@"OnBoardingScrollViewController"];
        AppDelegate *appDelegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
        [appDelegate.navController setViewControllers:[NSArray arrayWithObject:onBoardingScrollViewController] animated:YES];
        appDelegate.viewController = nil;
    }];
}

#pragma mark - Notifications

- (void)dropBoxRefreshed:(NSNotification *)notification {
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)planRefreshed:(NSNotification *)notification {
    if (Defaults.planType == 1 || Defaults.planType == 2) {
        [self hideUnlimitedButton];
    } else {
        [self showUnlimitedButton];
    }
}

#pragma mark -

- (void)onVersionLongPressed:(UIGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        NSLog(@"open DevConf");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DevConf" bundle:nil];
        UIViewController* vc = [storyboard instantiateInitialViewController];
        [self presentViewController:vc animated:YES completion:NULL];
    }
}

#pragma mark - Private functions

- (void)hideUnlimitedButton {
    if (self.isUnlimitedButtonHidden) {
        return;
    }
    
    NSArray *firstSection = [NSArray arrayWithObjects:KPLocalizedString(@"my profile"), KPLocalizedString(@"my kids"), KPLocalizedString(@"my fans"), KPLocalizedString(@"my account"),KPLocalizedString(@"logout"),  nil];
    [self.menus replaceObjectAtIndex:0 withObject:firstSection];
    [self.tableView reloadData];
    
    self.isUnlimitedButtonHidden = YES;
}

- (void)showUnlimitedButton {
    if (!self.isUnlimitedButtonHidden) {
        return;
    }
    
    NSArray *firstSection = [NSArray arrayWithObjects:@"gounlimitedimage", KPLocalizedString(@"my profile"), KPLocalizedString(@"my kids"), KPLocalizedString(@"my fans"), KPLocalizedString(@"my account"),KPLocalizedString(@"logout"),  nil];
    [self.menus replaceObjectAtIndex:0 withObject:firstSection];
    [self.tableView reloadData];
    
    self.isUnlimitedButtonHidden = NO;
}


@end
