//
//  AddStoryViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddStoryViewController.h"

@interface AddStoryViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AddStoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *InfoCellIdentifier = @"InfoCell";
    InfoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InfoCellIdentifier];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [InfoCell height];
}

- (IBAction)save:(id)sender {

}

@end

@implementation InfoCell

+ (CGFloat)height {
    return 75.0f;
}

@end
