//
//  AddParentsViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol AddParentsViewControllerDelegate;

@interface AddParentsViewController : UIViewController

@property(nonatomic, weak) id <AddParentsViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *kids;

- (CGFloat)height;

@end

@protocol AddParentsViewControllerDelegate <NSObject>

- (void)doneAddingParents:(NSArray *)fans;

@end
