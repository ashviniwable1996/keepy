//
//  LoadingViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/11/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "LoadingViewController.h"

@interface LoadingViewController ()

@property(nonatomic, weak) IBOutlet UIImageView *background;

@end

@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        self.background.image = [UIImage imageNamed:@"Default"];
    }
    else {
        self.background.image = [UIImage imageNamed:@"Default-568h"];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"firstRun"])
    {
        // do something or not...
        [self performSelector:@selector(reveal) withObject:nil afterDelay:0.7f];
    }
    else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults objectForKey:@"firstRun"])
            [defaults setObject:[NSDate date] forKey:@"firstRun"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //go to onboarding
        [self performSegueWithIdentifier:@"onboardingSegue1" sender:self];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)reveal {
    [self performSegueWithIdentifier:@"SignUp" sender:self];
}

@end

@implementation KPLoadingNavigationController

@end

@implementation SegueLoading

- (void)perform {
    UIViewController *source = (UIViewController *) self.sourceViewController;
    UIViewController *destination = (UIViewController *) self.destinationViewController;
    [UIView transitionFromView:source.view toView:destination.view duration:0.6 options:UIViewAnimationOptionTransitionCrossDissolve completion:^(BOOL finished) {
        [source.navigationController pushViewController:destination animated:NO];
    }];
}

@end
