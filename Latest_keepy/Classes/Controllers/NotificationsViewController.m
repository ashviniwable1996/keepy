//
//  NotificationsViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 4/21/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//



#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

#import "NotificationsViewController.h"
#import "KPNotification.h"
#import "AppDelegate.h"
#import "PrintIOManager.h"

#import "Keepy-Swift.h"

@interface NotificationsViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSArray *notifications;
@property(nonatomic, strong) KPNotification *currentNotification;
@property(nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property(nonatomic, strong) NSCache *imageCache;
@property(nonatomic, strong) NSIndexPath *currentNotificationIndexPath;

@end

@implementation NotificationsViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source":@"notificationsCenter"}];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4; // many servers limit how many concurrent requests they'll accept from a device, so make sure to set this accordingly

    self.imageCache = [[NSCache alloc] init];

    UIColor *patternColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
    self.view.backgroundColor = patternColor;

    self.title = KPLocalizedString(@"notifications");

    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"close"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollsToTop = NO;
    self.tableView.backgroundView = nil;

    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    self.tableView.separatorColor = [UIColor colorWithRed:0.447 green:0.361 blue:0.325 alpha:1];
    [self.view addSubview:self.tableView];

    [self refresh];

    //Take action to reset the notification after user enter to notification center
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d setCleanTheUnreadNotificationNumber:YES];
    
    // handled notification count lable
    [NSUserDefaults.standardUserDefaults setBool:true forKey:@"isNewNoticationReceived"];
    [NSUserDefaults.standardUserDefaults synchronize];
   
    // added by Ashvini
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATIONS_NEED_TO_BE_RESET object:nil];
    //--------------------------

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationsChanged:) name:NOTIFICATIONS_REFRESHED_NOTIFICATION object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [self.imageCache removeAllObjects];
}

- (void)viewDidLayoutSubviews {
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)refresh {
    NSArray *arr = [KPNotification MR_findAllSortedBy:@"notificationDate" ascending:NO];
    NSUInteger c = [arr count];
    //if (c > 20)
    //    c = 20;

    NSMutableArray *marr = [[NSMutableArray alloc] init];

    for (int i = 0; i < c; i++) {
        [marr addObject:[arr objectAtIndex:i]];
    }
    self.notifications = marr;

    [self.tableView reloadData];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    KPNotification *notification = [self.notifications objectAtIndex:indexPath.row];

    UIFont *afont = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:13];
    if (notification.status.intValue == 1) {
        afont = [UIFont fontWithName:@"ARSMaquettePro-Light" size:13];
    }

    float ax = 10;
    //if (notification.notificationType.intValue != 0)
    ax += 60;

    CGSize size = [notification.subject sizeWithFont:afont thatFitsToSize:CGSizeMake(self.view.frame.size.width - ax, 500) lineBreakMode:NSLineBreakByWordWrapping];

    float h = size.height + 41;
    if (h < 75) {// && notification.notificationType.intValue != 0)
        h = 75;
    }

    return h;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.notifications count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"NotificationCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 44)];
        lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:13];
        lbl.numberOfLines = 10;
        [cell.contentView addSubview:lbl];

        lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 20)];
        lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:13];
        [cell.contentView addSubview:lbl];

        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 53, 53)];
        //imageView.contentMode = UIViewContentModeScaleAspectFill;
        //imageView.clipsToBounds = YES;
        [imageView.layer setBorderColor:[[UIColor whiteColor] CGColor]];
        [imageView.layer setBorderWidth:1.0];
        [cell.contentView addSubview:imageView];

        UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        av.frame = CGRectMake(10 + (53 - 30) / 2, 10 + (53 - 30) / 2, 30, 30);
        av.hidesWhenStopped = YES;
        [cell.contentView addSubview:av];

        //line
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];

    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    [self configureCell:cell withIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    UILabel *titleLbl = (UILabel *) [cell.contentView.subviews objectAtIndex:0];

    KPNotification *notification = [self.notifications objectAtIndex:indexPath.row];
    titleLbl.text = notification.subject;
    if (notification.status.intValue == 0) {
        titleLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:13];
    } else {
        titleLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:13];
    }

    UIImageView *img = (UIImageView *) [cell.contentView.subviews objectAtIndex:2];
    img.image = nil;
    //img.hidden = (notification.notificationType.intValue == 0);

    UIActivityIndicatorView *av = (UIActivityIndicatorView *) [cell.contentView.subviews objectAtIndex:3];
    float ax = 10;
    //if (notification.notification.intValue != 0)
    //{
    ax += 60;
    NSString *urlHash = [[notification.items componentsSeparatedByString:@","] objectAtIndex:0];

    img.backgroundColor = [UIColor blackColor];
    img.contentMode = UIViewContentModeScaleAspectFit;

    if (notification.notificationType.intValue == NotificationTypeItems) {
        [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, urlHash]]];
        img.hidden = NO;
        img.alpha = 1.0;
    } else if (notification.notificationType.intValue == NotificationTypeBookIsReady) {
        [av stopAnimating];
        img.image = [UIImage imageNamed:@"ic_keepy_book"];
    } else if (notification.notificationType.intValue != NotificationTypeFanApproved && notification.notificationType.intValue != NotificationTypeNewFan) {
        [av startAnimating];
        //img.tag = assetId;
        //Asset *asset = [[KPDataParser instance] createAssetById:assetId withType:0];
        __weak UIImageView *wimg = img;
        [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, urlHash]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:0.5 animations:^{
                wimg.alpha = 1.0;
            }];
            [av stopAnimating];
        }];

    } else {
        [av stopAnimating];
        img.image = [UIImage imageNamed:@"app-icon57x57"];
    }
    //}


    CGSize size = [notification.subject sizeWithFont:titleLbl.font thatFitsToSize:CGSizeMake(self.view.frame.size.width - ax, 500) lineBreakMode:NSLineBreakByWordWrapping];

    CGRect aframe = titleLbl.frame;
    aframe.size.height = size.height;
    aframe.size.width = self.view.frame.size.width - ax;
    aframe.origin.x = ax;
    titleLbl.frame = aframe;

    UILabel *timeLbl = (UILabel *) [cell.contentView.subviews objectAtIndex:1];
    aframe = timeLbl.frame;
    aframe.origin.y = size.height + 8;
    aframe.origin.x = ax;
    timeLbl.frame = aframe;
    timeLbl.text = [notification.notificationDate stringOfTimestamp];

    float h = size.height + 41;
    if (h < 75) {// && notification.notification.intValue != 0)
        h = 75;
    }

    UIView *lineView = (UIView *) [cell.contentView.subviews objectAtIndex:4];
    aframe = lineView.frame;
    aframe.origin.y = h - 1;
    lineView.frame = aframe;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [[Mixpanel sharedInstance] track:@"Open Notification from NotificationsCenter"];

    KPNotification* noti = [self.notifications objectAtIndex:indexPath.row];
    
    self.currentNotification = noti;
    self.currentNotificationIndexPath = indexPath;
    
    if (noti.notificationType.intValue == NotificationTypeBookIsReady) {
        [PrintIOManager showPreselectedPrintIODialogInViewController: self];
    } else if (noti.notificationType.intValue == NotificationTypeFanApproved) {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:nil message:noti.subject preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"reject", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self acceptFan:NO];
        }]];
        [ac addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"accept", @"") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self acceptFan:YES];
        }]];
        [self presentViewController:ac animated:YES completion:nil];
        
        if (noti.status.intValue == NotificationStatusUnread) {
            [[ServerComm instance] markNotificationRead:noti.notificationId.intValue
                                       withSuccessBlock:^(id result) {
                                           noti.status = @(NotificationStatusRead);
                                           [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                                           [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATIONS_REFRESHED_NOTIFICATION object:nil];
                                           [self refresh];
                                       }
                                           andFailBlock:^(NSError *error) {}
             ];
        }
    }
    else {
        [self dismissViewControllerAnimated:YES completion:^{
            [GlobalUtils execNotification:noti.notificationId.intValue withType:noti.notificationType.intValue andItems:noti.items];
        }];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        self.currentNotification = [self.notifications objectAtIndex:indexPath.row];
        
        __weak typeof(self) weakSelf = self;
        void (^deleteNotification)() = ^() {
            typeof(self) strongSelf = weakSelf;
            
            NSMutableArray *marr = [[NSMutableArray alloc] initWithArray:strongSelf.notifications];
            [marr removeObjectAtIndex:indexPath.row];
            strongSelf.notifications = marr;
            
            [strongSelf.tableView beginUpdates];
            [strongSelf.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
            [strongSelf.tableView endUpdates];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATIONS_REFRESHED_NOTIFICATION object:@(1)];
        };
        
        KPNotification *notification = [self.notifications objectAtIndex:indexPath.row];
        if (notification.notificationType.intValue == NotificationTypeBookIsReady) {
            deleteNotification();
            return;
        }
        
        [[ServerComm instance] deleteNotification:self.currentNotification.notificationId.intValue withSuccessBlock:^(id result) {
            if ([self.notifications containsObject:self.currentNotification]) {
                [self.currentNotification MR_deleteEntity];
                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                //[self refresh];

                deleteNotification();
            }
        } andFailBlock:^(NSError *error) {

        }];
    }
}

- (void)topBtnTap:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source":@"notificationsCenter", @"result":@"cancel"}];
}

- (void)acceptFan:(BOOL)accept {
    int fanRequestId = [self.currentNotification.items intValue];
    [SVProgressHUD showWithStatus:KPLocalizedString(@"updating")];
    [[ServerComm instance] respondToFanRequest:fanRequestId andAccept:accept withSuccessBlock:^(id result) {

        [[ServerComm instance] deleteNotification:self.currentNotification.notificationId.intValue withSuccessBlock:^(id result2) {

            [self.currentNotification MR_deleteEntity];
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

            //[self refresh];
            NSMutableArray *marr = [[NSMutableArray alloc] initWithArray:self.notifications];
            [marr removeObjectAtIndex:self.currentNotificationIndexPath.row];
            self.notifications = marr;

            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.currentNotificationIndexPath] withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];

            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATIONS_REFRESHED_NOTIFICATION object:@(1)];

            [[UserManager sharedInstance] refreshMyData];

            [GlobalUtils addFanPop:[result valueForKey:@"result"]];

        }                            andFailBlock:^(NSError *error) {

        }];

        [SVProgressHUD dismiss];


    }                             andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
    }];
}

- (void)notificationsChanged:(NSNotification *)notification {
    DDLogInfo(@"%@", notification.userInfo);

    if (notification.object == nil) {
        [self refresh];
    }
}

@end
