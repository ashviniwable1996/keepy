
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "RecordViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Story.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Comment.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ItemShareViewController.h"
#import "DIYCam.h"
#import "KPLocalItem.h"
#import "KPLocalKidItem.h"
#import "KPLocalKid.h"
#import "KPLocalAsset.h"
#import "KPLocalStory.h"
#import "KPLocalComment.h"

#import "Keepy-Swift.h"

@interface RecordViewController () <AVAudioRecorderDelegate, AVAudioPlayerDelegate, /*AVCaptureFileOutputRecordingDelegate,*/ UIActionSheetDelegate, DIYCamDelegate, UITextViewDelegate> {
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}

@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic, strong) NSString *recordFilePath;
@property(nonatomic, strong) NSTimer *blinkTimer;
@property(nonatomic, strong) NSDate *recordStarted;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintOfPreviewButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintOfRecordButton;
@property(nonatomic, strong) IBOutlet UIButton *previewButton;
@property(nonatomic, strong) IBOutlet UIButton *deleteButton;
@property(nonatomic, strong) IBOutlet UIButton *recordButton;
@property(nonatomic, strong) IBOutlet UIImageView *itemImageView;
@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *loadingIV;
@property(nonatomic, strong) IBOutlet UIImageView *bottomPanel;
@property(nonatomic, strong) IBOutlet UILabel *timerLabel;
@property(nonatomic, strong) IBOutlet UIImageView *timerBkg;

@property(nonatomic, strong) UILabel *storyTimeCurrentLabel;
@property(nonatomic, strong) UILabel *storyTimeEndLabel;
@property(nonatomic, strong) UIImageView *progressBkg;
@property(nonatomic, strong) UIImageView *progressFiller;
@property(nonatomic, strong) UIView *playerView;
@property(nonatomic, strong) UIButton *knobButton;
@property(nonatomic, strong) NSTimer *audioTimer;
@property(nonatomic, strong) NSTimer *compressionProgressTimer;
@property(nonatomic, strong) NSDate *comperssionTimerStated;

@property(nonatomic) NSInteger mode;
@property(nonatomic) NSInteger recordingLength;
@property(nonatomic) BOOL isRecording;

@property(strong, nonatomic) AVCaptureVideoPreviewLayer *prevLayer;
@property(strong, nonatomic) AVCaptureSession *session;
@property(strong, nonatomic) AVCaptureDeviceInput *input;
@property(strong, nonatomic) AVCaptureDeviceInput *audioInput;
@property(strong, nonatomic) AVCaptureMovieFileOutput *output;
@property(strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property(strong, nonatomic) UIImage *previewImage;

@property(strong, nonatomic) IBOutlet UIView *container;

@property(nonatomic) BOOL videoRecordingWaitingForUpload;
@property(nonatomic) BOOL isAfterAddItem;
@property(nonatomic) BOOL isGoingToShareScreen;

@property(nonatomic) BOOL isRecordingComment;

@property(nonatomic, strong) DIYCam *cam;

@property(nonatomic, strong) KPTextView *commentTV;
@property (nonatomic,strong) UIImageView *bkgImageView;

- (IBAction)startTap:(id)sender;

- (IBAction)previewTap:(id)sender;

- (IBAction)deleteTap:(id)sender;

- (void)startRecording;

- (void)stopRecording;

@end

@implementation RecordViewController

@synthesize recordFilePath = _recordFilePath;
@synthesize previewButton = _previewButton;

- (instancetype)initWithItem:(KPLocalItem *)item withMode:(NSInteger)mode {
    self = [super initWithNibName:@"RecordViewController" bundle:nil];
    if (self) {
        self.item = (id)item;
        if (mode > 9) {
            self.isRecordingComment = YES;
            mode -= 10;
        }
        self.mode = mode;
    }
    return self;
}

- (instancetype)initWithData:(NSData *)data andLength:(NSInteger)fileLength andMode:(NSInteger)mode {
    self = [super initWithNibName:@"RecordViewController" bundle:nil];
    if (self) {
        self.mode = mode;
        self.recordingLength = fileLength;

        if (mode == 0) {
            self.recordFilePath = [NSString stringWithFormat:@"%@/%@", NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0], [NSString stringWithFormat:@"story.mp4"]];
            [data writeToFile:self.recordFilePath atomically:NO];
        }
        else {
            self.recordFilePath = [NSString stringWithFormat:@"%@/%@", NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0], [NSString stringWithFormat:@"story.mov"]];
            [data writeToFile:self.recordFilePath atomically:NO];

        }
    }
    return self;
}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = KPLocalizedString(@"record story");
    NSString *btnTitle = KPLocalizedString(@"save");
    if (self.isRecordingComment) {
        self.title = KPLocalizedString(@"comment");
        btnTitle = KPLocalizedString(@"post");
    }

    if (self.isAfterAddItem) {
        self.title = KPLocalizedString(@"message");
        [self setButtons:@{@"title" : KPLocalizedString(@"skip"), @"buttonType" : @(BUTTON_TYPE_NORMAL), @"disabled" : @(NO)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"next"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(YES)}];
    }
    else {
        if (self.navigationController != nil && self.navigationController.view.tag != 1) {
            [self setButtons:@{@"title" : KPLocalizedString(@"back"), @"buttonType" : @(BUTTON_TYPE_BACK)} andRightButtonInfo:@{@"title" : btnTitle, @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(YES)}];
        } else {
            [self setButtons:@{@"title" : KPLocalizedString(@"close"), @"buttonType" : @(BUTTON_TYPE_NORMAL)} andRightButtonInfo:@{@"title" : btnTitle, @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(YES)}];
        }

    }

    if (self.isAfterAddItem) {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source" : @"add-video-message"}];
    }
    else {
        if (self.mode == 1) {
            [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source" : @"add-comment"}];

        } else {
            [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source" : @"add-voice-over"}];
        }

    }

    if (self.mode == 2) //text comment
    {
        float dy = 0;
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            dy = 64;
        }

        self.bkgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, dy + 15, self.view.bounds.size.width - 60, 160)];
        if ([UIScreen mainScreen].bounds.size.height >= 812) {
           self.bkgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, dy + 35, self.view.bounds.size.width - 60, 160)];
        }
        [self.bkgImageView.layer setCornerRadius:15.0];
        self.bkgImageView.backgroundColor = UIColorFromRGB(0xffffff);

        self.rightButtonEnabled = NO;
        self.previewButton.hidden = YES;
        self.recordButton.hidden = YES;
        self.bottomPanel.hidden = YES;
        self.timerLabel.hidden = YES;
        self.timerBkg.hidden = YES;
        
        [self.bkgImageView.layer setShadowOffset:CGSizeMake(1, 1)];
        [self.bkgImageView.layer setShadowRadius:100.0];
        [self.bkgImageView.layer setShadowColor:[[UIColor redColor] CGColor]];
        [self.view addSubview:self.bkgImageView];

        self.commentTV = [[KPTextView alloc] initWithFrame:CGRectMake(35, dy + 20, self.view.bounds.size.width - 70, 150)];
        self.commentTV.textColor = UIColorFromRGB(0x523c37);
        self.commentTV.backgroundColor = [UIColor clearColor];
        self.commentTV.textAlignment = NSTextAlignmentLeft;
        self.commentTV.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:18];
        self.commentTV.delegate = self;
        [self.view addSubview:self.commentTV];

        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

        if (self.item && itemKids.count > 1) {
            NSString *kidNames = [GlobalUtils getKidNames:itemKids];


            if (self.item.itemType != 4) { //quote
                self.commentTV.placeholder = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kidNames, KPLocalizedString(@"\'s"), KPLocalizedString(@"photo")];
            } else {
                self.commentTV.placeholder = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kidNames, KPLocalizedString(@"\'s"), KPLocalizedString(@"video")];
            }
        } else {
            KPLocalKid *kid = itemKids[0];
            if (self.item.itemType != 4) { //quote
                self.commentTV.placeholder = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kid.name, KPLocalizedString(@"\'s"), KPLocalizedString(@"photo")];
            } else {
                self.commentTV.placeholder = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kid.name, KPLocalizedString(@"\'s"), KPLocalizedString(@"video")];
            }
        }
        /*
        UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [clearButton setImage:[UIImage imageNamed:@"clear-field-button"] forState:UIControlStateNormal];
        clearButton.frame = CGRectMake(250, dy + 20, 33, 33);
        [clearButton addTarget:self action:@selector(clearTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:clearButton];
        */
        //[self.commentTV becomeFirstResponder];

        return;
    }

    if (self.item != nil) {

        KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];

        //[self.itemImageView setImageWithAsset:self.item.image];
        [self.itemImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_iphone.jpg", Defaults.assetsURLString, itemImage.urlHash]] placeholderImage:nil options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {

        }                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

        }       usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    }
    else if (self.itemImage != nil) {
        self.itemImageView.image = self.itemImage;
    }
    else if (self.isAfterAddItem) {
        self.itemImageView.image = [[[GlobalUtils getCurrentItemData] valueForKey:@"imageInfo"] valueForKey:@"resultImage"];
    }

    self.bottomPanel.backgroundColor = UIColorFromRGB(0x6b5d58);
    self.bottomPanel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    CGRect aframe = self.recordButton.frame;
    aframe.origin.y = self.view.frame.size.height - aframe.size.height - 10;
    self.recordButton.frame = aframe;
    self.recordButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;

    if (self.mode == 0) {
        [self.recordButton setImage:[UIImage imageNamed:@"record-button-vo"] forState:UIControlStateNormal];
    } else {
        [self.recordButton setImage:[UIImage imageNamed:@"record-video-button"] forState:UIControlStateNormal];
    }

    self.previewButton.frame = self.recordButton.frame;

    self.instructionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, aframe.origin.y + 14, self.view.frame.size.width, 20)];
    self.instructionsLabel.textColor = [UIColor whiteColor];
    self.instructionsLabel.textAlignment = NSTextAlignmentCenter;
    self.instructionsLabel.backgroundColor = [UIColor clearColor];
    self.instructionsLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    self.instructionsLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    self.instructionsLabel.numberOfLines = 2;

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

    if (self.isRecordingComment) {
        if (self.item && itemKids.count > 1) {
            NSString *kidNames = [GlobalUtils getKidNames:itemKids];


            if (self.item.itemType != 4) { //quote
                self.instructionsLabel.text = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kidNames, KPLocalizedString(@"\'s"), KPLocalizedString(@"photo")];
            } else {
                self.instructionsLabel.text = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kidNames, KPLocalizedString(@"\'s"), KPLocalizedString(@"video")];
            }
        } else {
            KPLocalKid *kid = itemKids[0];
            if (self.item.itemType != 4) { //quote
                self.instructionsLabel.text = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kid.name, KPLocalizedString(@"\'s"), KPLocalizedString(@"photo")];
            } else {
                self.instructionsLabel.text = [NSString stringWithFormat:@"%@ %@%@ %@", KPLocalizedString(@"gush about"), kid.name, KPLocalizedString(@"\'s"), KPLocalizedString(@"video")];
            }
        }
    }
    else if (self.mode == 0) {
        if (self.item == nil) {
            self.instructionsLabel.text = KPLocalizedString(@"tell us all about this!");
        } else {
            if (itemKids.count > 1) {
                NSString *kidNames = [GlobalUtils getKidNames:itemKids];
                self.instructionsLabel.text = [NSString stringWithFormat:@"%@, %@", kidNames, KPLocalizedString(@"tell us about this!")];
            } else {
                KPLocalKid *kid = itemKids[0];
                self.instructionsLabel.text = [NSString stringWithFormat:@"%@, %@", kid.name, KPLocalizedString(@"tell us about this!")];
            }
        }
    }
    else if (self.isAfterAddItem) {
        if (self.item && itemKids.count > 1) {
            NSString *kidNames = [GlobalUtils getKidNames:itemKids];
            self.instructionsLabel.text = [NSString stringWithFormat:@"%@ %@", KPLocalizedString(@"leave a message for"), kidNames];
        } else {
            KPLocalKid *kid = itemKids[0];
            self.instructionsLabel.text = [NSString stringWithFormat:@"%@ %@", KPLocalizedString(@"leave a message for"), kid.name];
        }
    }

    [self.view addSubview:self.instructionsLabel];

    self.timerLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    self.timerLabel.alpha = 0;
    self.timerBkg.alpha = 0;

}
-(void)viewWillAppear:(BOOL)animated{
   
    
    if (self.mode == 2) //text comment
    {
        float dy = 0;
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            dy = 64;
        }
        
        self.bkgImageView.frame = CGRectMake(30, dy + 15, self.view.bounds.size.width - 60, 160);
        if ([UIScreen mainScreen].bounds.size.height >= 812) {
            self.bkgImageView.frame = CGRectMake(30, dy + 35, self.view.bounds.size.width - 60, 160);
        }
        [self.bkgImageView.layer setCornerRadius:15.0];
        self.bkgImageView.backgroundColor = UIColorFromRGB(0xffffff);
        
        self.rightButtonEnabled = NO;
        self.previewButton.hidden = YES;
        self.recordButton.hidden = YES;
        self.bottomPanel.hidden = YES;
        self.timerLabel.hidden = YES;
        self.timerBkg.hidden = YES;
        
        [self.bkgImageView.layer setShadowOffset:CGSizeMake(1, 1)];
        [self.bkgImageView.layer setShadowRadius:100.0];
        [self.bkgImageView.layer setShadowColor:[[UIColor redColor] CGColor]];
//        [self.view addSubview:self.bkgImageView];
        
        self.commentTV.frame = CGRectMake(35, dy + 20, self.view.bounds.size.width - 70, 150);
        self.commentTV.textColor = UIColorFromRGB(0x523c37);
        self.commentTV.backgroundColor = [UIColor clearColor];
        self.commentTV.textAlignment = NSTextAlignmentLeft;
        self.commentTV.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:18];        
        self.commentTV.delegate = self;
//        [self.view addSubview:self.commentTV];
        
        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];
        
       
        /*
         UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
         [clearButton setImage:[UIImage imageNamed:@"clear-field-button"] forState:UIControlStateNormal];
         clearButton.frame = CGRectMake(250, dy + 20, 33, 33);
         [clearButton addTarget:self action:@selector(clearTap:) forControlEvents:UIControlEventTouchUpInside];
         [self.view addSubview:clearButton];
         */
        //[self.commentTV becomeFirstResponder];
        
        return;
    }
    
    if (self.item != nil) {
        
        KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];
        
        //[self.itemImageView setImageWithAsset:self.item.image];
        [self.itemImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_iphone.jpg", Defaults.assetsURLString, itemImage.urlHash]] placeholderImage:nil options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        }                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }       usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
    }
    else if (self.itemImage != nil) {
        self.itemImageView.image = self.itemImage;
    }
    else if (self.isAfterAddItem) {
        self.itemImageView.image = [[[GlobalUtils getCurrentItemData] valueForKey:@"imageInfo"] valueForKey:@"resultImage"];
    }
    
    self.bottomPanel.backgroundColor = UIColorFromRGB(0x6b5d58);
    self.bottomPanel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    CGRect aframe = self.recordButton.frame;
    aframe.origin.y = self.view.frame.size.height - aframe.size.height - 10;
    self.recordButton.frame = aframe;
    self.recordButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    
    if (self.mode == 0) {
        [self.recordButton setImage:[UIImage imageNamed:@"record-button-vo"] forState:UIControlStateNormal];
    } else {
        [self.recordButton setImage:[UIImage imageNamed:@"record-video-button"] forState:UIControlStateNormal];
    }
    
    self.previewButton.frame = self.recordButton.frame;
    
    self.instructionsLabel.frame = CGRectMake(0, aframe.origin.y + 14, self.view.frame.size.width, 20);
    self.instructionsLabel.textColor = [UIColor whiteColor];
    self.instructionsLabel.textAlignment = NSTextAlignmentCenter;
    self.instructionsLabel.backgroundColor = [UIColor clearColor];
    self.instructionsLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    self.instructionsLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    self.instructionsLabel.numberOfLines = 2;
    

}
- (void)viewDidLayoutSubviews {
    if (self.playerView != nil) {
        return;
    }

    self.instructionsLabel.frame = CGRectMake(0, self.recordButton.frame.origin.y - 25, self.view.frame.size.width, 20);

    self.playerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.recordButton.frame.origin.y - 44, self.view.frame.size.width, 52)];

    self.storyTimeCurrentLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, (52 - 20) / 2, 120, 20)];
    self.storyTimeCurrentLabel.backgroundColor = [UIColor clearColor];
    self.storyTimeCurrentLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.storyTimeCurrentLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
    self.storyTimeCurrentLabel.textAlignment = NSTextAlignmentLeft;
    self.storyTimeCurrentLabel.text = @"0:00";
    [self.playerView addSubview:self.storyTimeCurrentLabel];

    self.storyTimeEndLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 40, (52 - 20) / 2, 40, 20)];
    self.storyTimeEndLabel.backgroundColor = [UIColor clearColor];
    self.storyTimeEndLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.storyTimeEndLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
    self.storyTimeEndLabel.textAlignment = NSTextAlignmentCenter;
    self.storyTimeEndLabel.text = @"0:00";
    [self.playerView addSubview:self.storyTimeEndLabel];
    self.storyTimeEndLabel.alpha = 1.0;

    self.progressBkg = [[UIImageView alloc] initWithFrame:CGRectMake(50, (55 - 3) / 2, self.view.frame.size.width - 100, 3)];
    self.progressBkg.image = [[UIImage imageNamed:@"progress-bar-defaultBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 2)];
    self.progressBkg.alpha = 1.0;
    [self.playerView addSubview:self.progressBkg];

    self.progressFiller = [[UIImageView alloc] initWithFrame:CGRectMake(50, (55 - 3) / 2, 0, 3)];
    self.progressFiller.image = [[UIImage imageNamed:@"progress-bar-filler"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 2)];
    self.progressFiller.alpha = 1.0;
    [self.playerView addSubview:self.progressFiller];

    self.knobButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.knobButton setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
    //[btn addTarget:self action:@selector(closeTap:) forControlEvents:UIControlEventTouchUpInside];
    self.knobButton.frame = CGRectMake(50, (55 - 19) / 2, 17, 19);
    self.knobButton.alpha = 1.0;
    [self.playerView addSubview:self.knobButton];

    self.playerView.alpha = 0;
    [self.view addSubview:self.playerView];


    NSString *deleteTitle = KPLocalizedString(@"retake");
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeSystem];

    [self.deleteButton setTitleColor:UIColorFromRGB(0xd7ff46) forState:UIControlStateNormal];
    self.deleteButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    [self.deleteButton setTitle:deleteTitle forState:UIControlStateNormal];

    CGSize textSize = [deleteTitle sizeWithFont:self.deleteButton.titleLabel.font thatFitsToSize:CGSizeMake(200, 30)];
    self.deleteButton.frame = CGRectMake(13, self.view.frame.size.height - 42, 20 + textSize.width, 30);
    [self.deleteButton addTarget:self action:@selector(deleteTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.deleteButton];
    self.deleteButton.hidden = YES;

    self.itemImageView.frame = CGRectMake((self.view.frame.size.width - 300) / 2, 79, 300, self.view.frame.size.height - 91 - 64 - 30);
    //self.itemImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (self.mode == 1) {
        self.container = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 122, 74, 112, 112)];
        if ([UIScreen mainScreen].bounds.size.height >= 812) {
            [self.container setFrame:CGRectMake(self.view.frame.size.width - 122, self.itemImageView.frame.origin.y + 20, 112, 112)];
        }
        self.container.backgroundColor = [UIColor whiteColor];
        [self.container.layer setBorderWidth:2];
        [self.container.layer setBorderColor:[[UIColor whiteColor] CGColor]];
        self.container.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [self.view addSubview:self.container];

        UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        av.hidesWhenStopped = YES;
        CGRect aframe = av.frame;
        aframe.origin.x = (self.container.frame.size.width - aframe.size.width) / 2;
        aframe.origin.y = (self.container.frame.size.height - aframe.size.height) / 2;
        av.frame = aframe;
        [self.container addSubview:av];
        [av startAnimating];

        self.timerBkg.image = [UIImage imageNamed:@"video-recording-timerBG"];
        float ax = self.container.frame.origin.x + (self.container.frame.size.width - 63) / 2;
        float ay = self.container.frame.origin.y + 96;
        self.timerBkg.frame = CGRectMake(ax, ay, 63, 30);

        self.timerLabel.frame = CGRectMake(ax, ay - 1, 63, 30);
        self.timerLabel.textAlignment = NSTextAlignmentCenter;
        self.timerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        self.timerLabel.textColor = UIColorFromRGB(0x6c5e59);

        [self.view sendSubviewToBack:self.container];
        [self.view sendSubviewToBack:self.itemImageView];

        self.cam = [[DIYCam alloc] initWithFrame:CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)];
        self.cam.delegate = self;
        [self.cam setupWithOptions:@{@"DIYAVSettingCameraPosition" : @(AVCaptureDevicePositionFront), @"DIYAVSettingSaveLibrary" : @(NO), @"DIYAVSettingVideoPreset" : AVCaptureSessionPresetMedium}];

        [self.cam setCamMode:DIYAVModeVideo];
        [self.container addSubview:self.cam];
    }

  //  self.itemImageView.frame = CGRectMake((self.view.frame.size.width - 300) / 2, 79, 300, self.view.frame.size.height - 91 - 64 - 30);
    //self.itemImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];
    KPLocalAsset *itemStoryAsset = [KPLocalAsset fetchByLocalID:itemStory.assetID];

    if (!self.isRecordingComment && (((self.recordFilePath != nil || (self.item != nil && itemStory != nil)) && self.mode == 0) ||
            ((self.recordFilePath != nil || (self.item != nil && [self.item fetchVideoStory] != nil && ![self.delegate videoStoryToDelete])) && self.mode == 1))) {
        self.timerLabel.alpha = 0.0;
        self.timerBkg.alpha = 0.0;
        self.recordButton.hidden = YES;
        self.previewButton.hidden = NO;
        self.deleteButton.hidden = NO;
        self.rightButtonEnabled = YES;
        self.playerView.alpha = 1.0;

        self.instructionsLabel.alpha = 0;

        KPLocalComment *itemVideoComment = [self.item fetchVideoStory];
        KPLocalAsset *itemVideo = [KPLocalAsset fetchByLocalID:itemVideoComment.videoAssetID];

        NSInteger duration = 0;
        if (self.mode == 0) {
            duration = itemStory.storyLength;
            int minutes = floor(duration / 60);
            int seconds = round(duration - minutes * 60);
            self.storyTimeEndLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

            if (self.recordFilePath == nil) {
                [[ServerComm instance] downloadAssetFile:(id)itemStoryAsset withSuccessBlock:^(id result) {
                    self.recordFilePath = result;
                }                           andFailBlock:^(NSError *error) {

                }];
            }
        }
        else {
            if (self.recordFilePath == nil) {
                [[ServerComm instance] downloadAssetFile:(id)itemVideo withSuccessBlock:^(id result) {
                    [self prepareMovie:[NSURL fileURLWithPath:result]];
                }                           andFailBlock:^(NSError *error) {

                }];
            }
            else {
                [self prepareMovie:[NSURL fileURLWithPath:self.recordFilePath]];
            }
        }

        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(self.view.frame.size.width - 54, self.view.frame.size.height - 47, 44, 44);
        [btn setImage:[UIImage imageNamed:@"add-story-delete-photo-icon"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(deleteStory:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    if ([UIScreen mainScreen].bounds.size.height == 812) {
        self.bottomConstraintOfRecordButton.constant = 17.0;
        self.bottomConstraintOfPreviewButton.constant = 17.0;
    }
    if (self.mode == 2) {
       // self.bkgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 64 + 15, self.view.bounds.size.width - 60, 160)];
        //self.commentTV = [[KPTextView alloc] initWithFrame:CGRectMake(35, 64 + 20, self.view.bounds.size.width - 70, 150)];
        
        // This code is modified by Ashvini
        self.bkgImageView.frame = CGRectMake(30, 64 + 15, self.view.bounds.size.width - 60, 160);
        self.commentTV.frame = CGRectMake(35, 64 + 20, self.view.bounds.size.width - 70, 150);
        if ([UIScreen mainScreen].bounds.size.height >= 812) {
            self.bkgImageView.frame = CGRectMake(30, 64 + 35, self.view.bounds.size.width - 60, 160);
            self.commentTV.frame = CGRectMake(35, 64 + 40, self.view.bounds.size.width - 70, 150);
        }
        
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
  
    //Zvika622 05/21/2016 avoid autolock.
    [UIApplication sharedApplication].idleTimerDisabled = NO; //This line is to fix Apple bug
    [UIApplication sharedApplication].idleTimerDisabled = YES; // Turn off sleep mode

    
    
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
//        if (self.mode == 1) {
//            [GlobalUtils showTip:7 withView:nil withCompleteBlock:^{
//            }];
//        }
//        else {
//            [GlobalUtils showTip:9 withView:nil withCompleteBlock:^{
//            }];
//        }
        
        if(self.mode == 2) {
            
            [self.commentTV becomeFirstResponder];
        }

    });
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //Zvika602 05/21/2016 enable autolock back after disabling it on ViewDidLoad
    [UIApplication sharedApplication].idleTimerDisabled = YES; //Apple Bug 
    [UIApplication sharedApplication].idleTimerDisabled = NO; // Turn off

    [self.audioTimer invalidate];
    self.audioTimer = nil;

    if (self.mode == 1) {
        [self.moviePlayer pause];
    }
    else {
        [player pause];
    }
}

- (IBAction)startTap:(id)sender {
    if (!self.isRecording) {
        if (self.mode == 1) {
            [self startVideoRecording];
            self.isRecording = YES;
        }
        else {
            [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                if (!granted) {
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings->Privacy->Microphone")];

                }
                else {
                    [SVProgressHUD showWithStatus:KPLocalizedString(@"preparing")];
                    int64_t delayInSeconds = 0.5;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                        [self startRecording];
                    });
                    self.isRecording = YES;
                }
            }];

        }
    }
    else {
        if (self.mode == 1) {
            [self stopVideoRecording];
        }
        else {
            [self stopRecording];
        }
        self.isRecording = NO;
    }
}

- (IBAction)previewTap:(id)sender {
    if (self.mode == 0 && self.recordFilePath == nil) {
        return;
    }


    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];


    UIButton *btn = (UIButton *) sender;
    if (btn.tag == 0) {
        self.audioTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(audioTimerProc:) userInfo:nil repeats:YES];

        btn.tag = 1;
        [btn setImage:[UIImage imageNamed:@"big-pause-button"] forState:UIControlStateNormal];

        // Commented below line by Ashvini
        [GlobalUtils outputToSpeaker];

        if (self.mode == 1) {
            [self.moviePlayer play];

            BOOL isMyKid = YES;
            if (!self.isAfterAddItem && itemKids.count > 0) {
                
                // itemKids is an empty array
                
                isMyKid = [[UserManager sharedInstance] isMyKid:itemKids[0]];
            }

            [[Mixpanel sharedInstance] track:@"video-record-play" properties:@{@"source" : self.isAfterAddItem ? @"add photo" : @"add comment", @"isFanComment" : @(!isMyKid)}];


        }
        else {
            NSError *error;
            NSURL *url = [NSURL fileURLWithPath:self.recordFilePath];
            player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            player.delegate = self;
            [player play];
        }
    }
    else {
        [self.audioTimer invalidate];
        self.audioTimer = nil;

        if (self.mode == 1) {
            [self.moviePlayer pause];

            BOOL isMyKid = YES;
            if (!self.isAfterAddItem) {
                if (!self.isAfterAddItem && itemKids.count > 0) {
                    isMyKid = [[UserManager sharedInstance] isMyKid:[itemKids objectAtIndex:0]];
                }
            }
            [[Mixpanel sharedInstance] track:@"video-record-pause" properties:@{@"source" : self.isAfterAddItem ? @"add photo" : @"add comment", @"isFanComment" : @(!isMyKid)}];


        }
        else {
            [player pause];
        }
        btn.tag = 0;
        [btn setImage:[UIImage imageNamed:@"big-play-button"] forState:UIControlStateNormal];
    }
}

- (void)deleteTap:(id)sender {

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];
    [self.delegate recordingSuccessful:nil withLength:self.recordingLength andMode:self.mode andPreviewImage:self.previewImage];
    if (self.mode == 1) {
        BOOL isMyKid = YES;
        if (!self.isAfterAddItem && itemKids.count > 0) {
            isMyKid = [[UserManager sharedInstance] isMyKid:[itemKids objectAtIndex:0]];
        }

        [[Mixpanel sharedInstance] track:@"video-record-retake" properties:@{@"source" : self.isAfterAddItem ? @"add photo" : @"add comment", @"isFanComment" : @(!isMyKid)}];

        //[SVProgressHUD showWithStatus:KPLocalizedString(@"preparing")];
        [self.moviePlayer.view removeFromSuperview];
        [self.prevLayer removeFromSuperlayer];
        self.videoRecordingWaitingForUpload = NO;
        [self.cam setCamMode:DIYAVModeVideo];
    }

    [UIView animateWithDuration:0.5 animations:^{
        if (player) {
            [player stop];
        }
        self.recordFilePath = nil;
        self.recordButton.hidden = NO;
        self.previewButton.hidden = YES;
        self.deleteButton.hidden = YES;
        self.rightButtonEnabled = NO;
        self.playerView.alpha = 0;
        self.instructionsLabel.alpha = 1;
        //self.moviePlayer = nil;
    }                completion:^(BOOL finished) {
        //if (self.mode == 1)
        //[self setupCaptureSession];
    }];

}

- (void)topBtnTap:(UIButton *)sender {

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

    if (self.previewButton.tag == 1) {
        [self previewTap:self.previewButton];
    }

    if (sender.tag == 1) //Ok
    {
        if ((self.mode == 0 && self.recordFilePath == nil) || (self.mode == 1 && self.moviePlayer.contentURL == nil)) {
            return;
        }

        if (self.isRecordingComment) {
            [SVProgressHUD showProgress:0 status:KPLocalizedString(@"uploading comment")];

            KPLocalKid *kid = itemKids[0];
            NSInteger relationType = kid.userRelationType;
            if (kid.isMine) {
                relationType = [[UserManager sharedInstance] getMe].parentType.intValue;
            }

            NSData *data = nil;
            NSString *extraData = @"";
            if (self.mode == 0 || self.mode == 2) {
                NSString *relationImageStr = @"comment-mom.png";
                switch (relationType) {
                    case 1:
                        relationImageStr = @"comment-dad.png";
                        break;
                    case 2:
                        relationImageStr = @"comment-grandma.png";
                        break;
                    case 3:
                        relationImageStr = @"comment-grandpa.png";
                        break;
                    case 4:
                        relationImageStr = @"comment-family.png";
                        break;
                    case 5:
                        relationImageStr = @"comment-friend.png";
                        break;
                }
                self.previewImage = [UIImage imageNamed:relationImageStr];
                NSData *tempData = UIImageJPEGRepresentation(self.previewImage, 1.0);
                self.previewImage = [UIImage imageWithData:tempData];

                if (self.mode == 0) {
                    data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:self.recordFilePath]];
                } else {
                    extraData = self.commentTV.text;
                }
            }
            else {
                data = [NSData dataWithContentsOfURL:self.moviePlayer.contentURL];
            }

            NSInteger commentType = 1 - self.mode;
            if (self.mode == 2) {
                commentType = 2;
            }

            [[ServerComm instance] uploadItemComment:self.item.serverID withVideo:data andPreviewImage:self.previewImage andCommentLength:self.recordingLength withType:commentType isStory:NO withExtraData:extraData withSuccessBlock:^(id result) {

                KPLocalComment *comment = [KPLocalComment create];
                comment.serverID = [[result valueForKey:@"commentId"] intValue];
                comment.commentDate = [NSDate date].timeIntervalSince1970;
                comment.commentLength = self.recordingLength;
                comment.wasRead = YES;
                comment.commentType = commentType;
                comment.creatorName = [[UserManager sharedInstance] getMe].name;
                comment.creatorRelationType = relationType;
                comment.extraData = extraData;

                if (commentType != 2) {
                    KPLocalAsset *asset = [KPLocalAsset create];
                    asset.serverID = [[result valueForKey:@"assetId"] intValue];
                    asset.assetHash = [result valueForKey:@"hash"];
                    asset.assetType = (self.mode == 1) ? 1 : 2;

                    [asset saveChanges];

                    comment.videoAssetID = asset.identifier;
                    [[ServerComm instance] saveAssetData:data withFileExt:(self.mode == 1) ? @"mov" : @"mp4" andAssetHash:asset.urlHash];
                }

                KPLocalAsset *asset = [KPLocalAsset create];
                asset.serverID = [[result valueForKey:@"previewAssetId"] intValue];
                asset.assetHash = [result valueForKey:@"previewAssetHash"];
                asset.assetType = 0;
                [asset saveChanges];

                comment.previewImageAssetID = asset.identifier;

                [[SDImageCache sharedImageCache] storeImage:self.previewImage forKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, asset.urlHash]];

                // [self.item addCommentsObject:comment];
                comment.itemID = self.item.identifier;
                [comment saveChanges];

                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

                [self.delegate recordingSuccessful:nil withLength:self.recordingLength andMode:self.mode andPreviewImage:self.previewImage];
                if (self.completion) {
                    self.completion(nil, self.recordingLength, self.mode, self.previewImage);
                }


                int64_t delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    [GlobalUtils fireTrigger:TRIGGER_TYPE_ADDCOMMENT];
                });

                [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:self.item];
                
               // [[NSNotificationCenter defaultCenter] postNotificationName:COMMENT_ADDED_NOTIFICATION object:comment];
                
                [SVProgressHUD dismiss];

                [GlobalUtils updateAnalyticsUser];

                if (self.isAfterAddItem) {
                    [self showShareItem];
                } else if (self.navigationController != nil && self.navigationController.view.tag != 1) {
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }


            }                           andFailBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
            }                       andProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                //DDLogInfo(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
                [SVProgressHUD showProgress:(float) totalBytesWritten / (float) totalBytesExpectedToWrite status:KPLocalizedString(@"uploading comment")];
            }];
        }
        else {
            NSData *fileData = nil;
            if (self.mode == 0) {
                fileData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:self.recordFilePath]];
            } else {
                fileData = [NSData dataWithContentsOfURL:self.moviePlayer.contentURL];
            }

            //if (self.item == nil)
            //{
            [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-voice-over", @"result" : @"ok"}];

            [self.delegate recordingSuccessful:fileData withLength:self.recordingLength andMode:self.mode andPreviewImage:self.previewImage];
            if (self.completion) {
                self.completion(fileData, self.recordingLength, self.mode, self.previewImage);
            }

            
          //  [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:self.item];
            
            if (self.navigationController != nil && self.navigationController.view.tag != 1) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }

            return;
            //}


            // TODO: find out why there is a return statement before this block of code

            /*


            [SVProgressHUD showProgress:0 status:KPLocalizedString(@"uploading story")];
            [[ServerComm instance] uploadStory:self.item.itemId.intValue withStoryLength:self.recordingLength withData:fileData withSuccessBlock:^(id result) {
                int status = [[result valueForKey:@"status"] intValue];
                if (status == 0) {
                    Asset *asset = [Asset MR_createEntity];
                    asset.assetId = @([[result valueForKey:@"assetId"] intValue]);
                    asset.assetHash = [result valueForKey:@"hash"];
                    asset.assetType = @(2);

                    Story *story = [Story MR_createEntity];
                    story.storyId = @([[result valueForKey:@"storyId"] intValue]);
                    story.asset = asset;
                    story.storyLength = @(self.recordingLength);

                    self.item.story = story;

                    [[ServerComm instance] saveAssetData:fileData withFileExt:@"mp4" andAssetHash:asset.urlHash];

                    [[[Mixpanel sharedInstance] people] increment:@"Number of stories" by:@(1)];
                    [[Mixpanel sharedInstance] track:@"story added"];

                    [[Analytics sharedAnalytics] track:@"item-add" properties:@{@"itemType" : @"voice-over"}];

                    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

                    [GlobalUtils updateAnalyticsUser];

                    [self.delegate recordingSuccessful:fileData withLength:self.recordingLength andMode:self.mode andPreviewImage:self.previewImage];

                    int64_t delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                        [GlobalUtils fireTrigger:TRIGGER_TYPE_ADDSTORY];
                    });

                    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:self.item];

                    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-voice-over", @"result" : @"ok"}];

                    [SVProgressHUD dismiss];
                    if (self.navigationController != nil && self.navigationController.view.tag != 1) {
                        [self.navigationController popViewControllerAnimated:YES];
                    } else {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }

                }
                else {
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading story")];
                }
            }                     andFailBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
            }                 andProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                //DDLogInfo(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
                [SVProgressHUD showProgress:(float) totalBytesWritten / (float) totalBytesExpectedToWrite status:KPLocalizedString(@"uploading story")];
            }];



            */



        }
    }
    else {
        if ((self.mode == 0 && self.recordFilePath != nil) || (self.mode == 1 && self.videoRecordingWaitingForUpload)) {
            UIActionSheet *asheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:KPLocalizedString(@"cancel") destructiveButtonTitle:KPLocalizedString(@"delete recording") otherButtonTitles:nil, nil];
            [asheet showInView:self.view];
        }
        else {
            [self cancelRecordingAndQuit];
        }
    }
}

-(void)didReceiveMemoryWarning{
    NSLog(@"RecordViewController - didReceiveMemoryWarning");
}

- (void)cancelRecordingAndQuit {
    [self.blinkTimer invalidate];

    if (self.mode == 1 && self.videoRecordingWaitingForUpload) {
        self.videoRecordingWaitingForUpload = NO;
        [self.output stopRecording];
    }

    [self.delegate recordingCancelled];

    if (self.isAfterAddItem) {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-video-message", @"result" : @"cancel"}];
    }
    else {
        if (self.mode == 1) {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-comment", @"result" : @"cancel"}];

        } else {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-voice-over", @"result" : @"cancel"}];
        }

    }

    if (self.isAfterAddItem) {
        [self showShareItem];
    } else if (self.navigationController != nil && self.navigationController.view.tag != 1) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)startRecording {

    NSError *error;

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];

    self.recordFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:
            [NSString stringWithFormat:@"r%f.mp4", [[NSDate date] timeIntervalSince1970]]];


    NSURL *url = [NSURL fileURLWithPath:self.recordFilePath];

    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];

    [recordSetting setValue:@(kAudioFormatMPEG4AAC) forKey:AVFormatIDKey];
    [recordSetting setValue:@12000.0F forKey:AVSampleRateKey];
    [recordSetting setValue:@2 forKey:AVNumberOfChannelsKey];

    [recordSetting setValue:@16 forKey:AVLinearPCMBitDepthKey];
    [recordSetting setValue:@NO forKey:AVLinearPCMIsBigEndianKey];
    [recordSetting setValue:@NO forKey:AVLinearPCMIsFloatKey];

    recorder = [[AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&error];

    if (recorder) {
        recorder.delegate = self;
        if ([recorder prepareToRecord]) {
            [recorder record];
        }
    } else
        DDLogInfo(@"error starting recorder:%@", [error description]);

    //dispatch_async(dispatch_get_main_queue(), ^{

    self.recordStarted = [NSDate date];
    self.blinkTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(blinkTimerProc:) userInfo:nil repeats:YES];

    self.timerLabel.text = @"0:00";

    [UIView animateWithDuration:0.5 animations:^{
        self.timerLabel.alpha = 1.0;
        self.timerBkg.alpha = 1.0;
        self.instructionsLabel.alpha = 0;
    }                completion:^(BOOL finished) {
    }];


    //});

    [SVProgressHUD dismiss];
}

- (void)stopRecording {
    [recorder stop];
    recorder = nil;

    [self.blinkTimer invalidate];

    [self.recordButton setImage:[UIImage imageNamed:@"record-button-vo"] forState:UIControlStateNormal];

    int minutes = floor(self.recordingLength / 60);
    int seconds = round(self.recordingLength - minutes * 60);
    self.storyTimeEndLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

    [UIView animateWithDuration:0.5 animations:^{
        self.timerLabel.alpha = 0.0;
        self.timerBkg.alpha = 0.0;
        self.recordButton.hidden = YES;
        self.previewButton.hidden = NO;
        self.deleteButton.hidden = NO;
        self.rightButtonEnabled = YES;
        self.playerView.alpha = 1.0;
    }                completion:^(BOOL finished) {
//        [GlobalUtils showTip:10 withView:nil withCompleteBlock:^{
//        }];
    }];
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error {
    DDLogInfo(@"Error:%@", [error description]);
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)arecorder successfully:(BOOL)flag {
    DDLogInfo(@"Finish");
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    self.previewButton.tag = 0;
    [self.previewButton setImage:[UIImage imageNamed:@"big-play-button"] forState:UIControlStateNormal];

    [self.audioTimer invalidate];
    self.audioTimer = nil;

    [self audioTimerProc:nil];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    self.previewButton.tag = 0;
    [self.previewButton setImage:[UIImage imageNamed:@"big-play-button"] forState:UIControlStateNormal];

    [self.audioTimer invalidate];
    self.audioTimer = nil;


}

- (void)blinkTimerProc:(id)sender {
    self.recordButton.tag = 1 - self.recordButton.tag;
    NSString *imageName = @"record-button-blinkColor";
    if (self.recordButton.tag == 0) {
        imageName = @"record-button";
    }

    [self.recordButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];

    NSTimeInterval interval = [self.recordStarted timeIntervalSinceNow];
    double val = fabs(interval);

    int minutes = floor(val / 60);
    int seconds = (val - (minutes * 60));

    self.recordingLength = round(val);
    self.timerLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
}

#pragma mark - AudioTimer

- (void)audioTimerProc:(id)sender {
    __block int minutes = 0;
    __block int seconds = 0;
    NSTimeInterval duration = 1;
    NSTimeInterval currentTime = 0;

    if (self.mode == 1) {
        minutes = floor(self.moviePlayer.currentPlaybackTime / 60);
        seconds = round(self.moviePlayer.currentPlaybackTime - minutes * 60);
        duration = self.moviePlayer.duration;
        currentTime = self.moviePlayer.currentPlaybackTime;
    }
    else {
        minutes = floor(player.currentTime / 60);
        seconds = round(player.currentTime - minutes * 60);
        duration = player.duration;
        currentTime = player.currentTime;
    }

    //if (player == nil)
    //    return;
    if (duration == 0) {
        return;
    }

    if (minutes < 0) {
        minutes = 0;
    }

    if (seconds < 0) {
        seconds = 0;
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        self.storyTimeCurrentLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

        minutes = floor(duration / 60);
        seconds = round(duration - minutes * 60);
        self.storyTimeEndLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

        CGRect aframe = self.progressFiller.frame;
        aframe.size.width = (self.view.frame.size.width - 100) * (currentTime / duration);
        self.progressFiller.frame = aframe;

        float ax = self.progressFiller.frame.origin.x + self.progressFiller.frame.size.width - 10;
        if (ax < 50) {
            ax = 50;
        }

        aframe = self.knobButton.frame;
        aframe.origin.x = ax;
        self.knobButton.frame = aframe;

    });
}

- (void)clearFileIfExists:(NSURL *)filePath {
    NSString *fileStr = [filePath absoluteString];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:fileStr]) {
        [fileManager removeItemAtPath:fileStr error:NULL];
    }
}


#pragma mark - video

/*
- (NSURL *) tempFileURL
{    
    NSString *outputPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"output.mov"];
    //DDLogInfo(@"Saved %@",outputPath);
    NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:outputPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:outputPath]) {
        [fileManager removeItemAtPath:outputPath error:NULL];
    }
    return outputURL;
}
*/
- (void)startVideoRecording {
    @try {
        if (![self.cam getRecordingStatus]) {
            [self.cam captureVideoStart];
        }

        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];


        BOOL isMyKid = YES;
        if (!self.isAfterAddItem && itemKids.count > 0) {
            isMyKid = ((KPLocalKid *)itemKids[0]).isMine;
        }


        [[Mixpanel sharedInstance] track:@"video-record-start" properties:@{@"source" : self.isAfterAddItem ? @"add photo" : @"add comment", @"isFanComment" : @(!isMyKid)}];

        self.videoRecordingWaitingForUpload = YES;
        //dispatch_async(dispatch_get_main_queue(), ^{

        self.recordStarted = [NSDate date];
        self.blinkTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(blinkTimerProc:) userInfo:nil repeats:YES];

        self.timerLabel.text = @"0:00";

        [UIView animateWithDuration:0.5 animations:^{
            self.timerLabel.alpha = 1.0;
            self.timerBkg.alpha = 1.0;
            //self.instructionsLabel.alpha = 0;
        }                completion:^(BOOL finished) {
        }];


        //});
    } @catch(id exeption) {
        
    }
}

- (void)stopVideoRecording {
    DDLogInfo(@"stop recording");

    if ([self.cam getRecordingStatus]) {
        //Zvika to investigate what's DIYAV > Pod
        [self.cam captureVideoStop];
    }

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

    BOOL isMyKid = YES;
    if (!self.isAfterAddItem && itemKids.count > 0) {
        // isMyKid = [[UserManager sharedInstance] isMyKid:[self.item.kids.allObjects objectAtIndex:0]];
        isMyKid = ((KPLocalKid *)itemKids[0]).isMine;
    }

    [[Mixpanel sharedInstance] track:@"video-record-stop" properties:@{@"source" : self.isAfterAddItem ? @"add photo" : @"add comment", @"isFanComment" : @(!isMyKid)}];

    [self.blinkTimer invalidate];

    [self.recordButton setImage:[UIImage imageNamed:@"record-video-button"] forState:UIControlStateNormal];

    int minutes = floor(self.recordingLength / 60);
    int seconds = round(self.recordingLength - minutes * 60);
    self.storyTimeEndLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

    [UIView animateWithDuration:0.5 animations:^{
        self.timerLabel.alpha = 0.0;
        self.timerBkg.alpha = 0.0;
        self.recordButton.hidden = YES;
        self.previewButton.hidden = NO;
        self.deleteButton.hidden = NO;
        self.rightButtonEnabled = YES;
        self.playerView.alpha = 1.0;
    }                completion:^(BOOL finished) {
//        [GlobalUtils showTip:8 withView:nil withCompleteBlock:^{
//        }];
    }];
}

/*
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections
{
    DDLogInfo(@"started captureOutput");
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    DDLogInfo(@"Error %@",error);
    DDLogInfo(@"Ended captureOutput");
    self.deleteButton.hidden = NO;
    self.previewButton.hidden = NO;
    self.recordButton.hidden = YES;
    self.instructionsLabel.alpha = 0;
    
    //[self prepareMovie:outputFileURL];
    if (self.videoRecordingWaitingForUpload)
        [self cropRecordedVideo:outputFileURL];
}
*/

- (void)cropRecordedVideo:(NSURL *)aurl {
    [SVProgressHUD showProgress:0 status:KPLocalizedString(@"compressing video")];
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:aurl options:nil];

    AVAssetTrack *clipVideoTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];

    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.renderSize = CGSizeMake(150, 150);
    videoComposition.frameDuration = CMTimeMake(1, 30); // 30 fps

    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    //Zvika
    //instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30));
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);

    AVMutableVideoCompositionLayerInstruction *transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];

    CGAffineTransform translateToCenter = CGAffineTransformMakeTranslation(-50, -360);
    CGAffineTransform rotateBy90Degrees = CGAffineTransformMakeRotation(M_PI_2);
    //CGAffineTransform moveRight = CGAffineTransformMakeTranslation(0, 0);
    //CGAffineTransform shrinkWidth = CGAffineTransformMakeScale(.99, 1); // needed because Apple does a "stretch" by default
    //CGAffineTransform finalTransform = CGAffineTransformConcat( shrinkWidth, CGAffineTransformConcat(translateToCenter, CGAffineTransformConcat(moveRight, rotateBy90Degrees)));

    //float sx = 150.0/clipVideoTrack.naturalSize.width;
    float sy = 150.0 / clipVideoTrack.naturalSize.height;
    //DDLogInfo(@"SX:%2f %2f %2f %2f", sx, sy, clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
    CGAffineTransform finalTransform = CGAffineTransformConcat(translateToCenter, CGAffineTransformConcat(CGAffineTransformMakeScale(sy, sy), rotateBy90Degrees));
    [transformer setTransform:finalTransform atTime:kCMTimeZero];

    instruction.layerInstructions = @[transformer];
    videoComposition.instructions = @[instruction];

    NSString *outputPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"coutput.mov"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:outputPath]) {
        [fileManager removeItemAtPath:outputPath error:NULL];
    }
    NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:outputPath];

  
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality];
    exporter.videoComposition = videoComposition;
    exporter.outputURL = outputURL;
    exporter.outputFileType = AVFileTypeMPEG4;
    self.compressionProgressTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(compressionProgressTimerProc:) userInfo:exporter repeats:YES];
    self.comperssionTimerStated = [NSDate date];

    [exporter exportAsynchronouslyWithCompletionHandler:^(void) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self prepareMovie:outputURL];
            DDLogInfo(@"Here:%ld ---- %@", (long)exporter.status, [exporter.error localizedDescription]);
        });
    }];
}

#pragma mark - AVSession

/*
- (void)setupCaptureSession
{
    // Create the session
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset =  AVCaptureSessionPresetMedium;
    self.input = [AVCaptureDeviceInput deviceInputWithDevice:[self frontFacingCamera] error:nil];
    
    NSError *setCategoryError = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayAndRecord error: &setCategoryError];
    
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
    if ([devices count] == 0)
    {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"can\'t find audio input device")];
        return;
    }
    self.audioInput = [AVCaptureDeviceInput deviceInputWithDevice:[devices objectAtIndex:0] error:nil];
    if(!self.input || !self.audioInput){
        DDLogInfo(@"Couldn't create input!");
    }
    self.output= [[AVCaptureMovieFileOutput alloc] init] ;
    [self.session addInput:self.input];
    [self.session addInput:self.audioInput];
    [self.session addOutput:self.output];
    [self.session startRunning];
    
    self.prevLayer = [AVCaptureVideoPreviewLayer layerWithSession: self.session];
    self.prevLayer.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height);
    self.prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [self.container.layer addSublayer: self.prevLayer];
    self.container.clipsToBounds = NO;
    
    [SVProgressHUD dismiss];
        
}

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}
- (AVCaptureDevice *) backFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionBack];
}
- (AVCaptureDevice *) frontFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionFront];
}
*/
- (void)prepareMovie:(NSURL *)fileURL {
    if (self.moviePlayer) {
        [self.moviePlayer setContentURL:fileURL];
    } else {
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
        self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    }

    self.moviePlayer.view.clipsToBounds = NO;

    // Register for the playback finished notification.

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerThumbnailImageRequestDidFinishNotification:)
                                                 name:MPMoviePlayerThumbnailImageRequestDidFinishNotification
                                               object:self.moviePlayer];

    // Movie playback is asynchronous, so this method returns immediately.
    self.moviePlayer.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height);
    self.moviePlayer.shouldAutoplay = NO;
    [self.container addSubview:self.moviePlayer.view];

    self.moviePlayer.initialPlaybackTime = -1.0;
    [self.moviePlayer prepareToPlay];

    //if (self.item != nil || self.isAfterAddItem)
    [self.moviePlayer requestThumbnailImagesAtTimes:@[@0.1] timeOption:MPMovieTimeOptionNearestKeyFrame];

    //[self.moviePlayer play];

    if (self.isGoingToShareScreen) {
        [self gotoShareScreen];
    }

}

-(void)moviePlayerThumbnailImageRequestDidFinishNotification:(NSNotification*)note {
    NSDictionary* userInfo = [note userInfo];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.previewImage = (UIImage*)[userInfo objectForKey:MPMoviePlayerThumbnailImageKey];
    });
}

// When the movie is done,release the controller.
- (void)myMovieFinishedCallback:(NSNotification *)aNotification {
    [self.previewButton setImage:[UIImage imageNamed:@"big-play-button"] forState:UIControlStateNormal];
    self.previewButton.tag = 0;

    [self.audioTimer invalidate];
    self.audioTimer = nil;

    [self audioTimerProc:nil];


}

- (void)setItemImage:(UIImage *)itemImage {
    _itemImage = itemImage;
    self.itemImageView.image = itemImage;
}

- (void)compressionProgressTimerProc:(NSTimer *)sender {
   
    AVAssetExportSession *exporter = (AVAssetExportSession *) (sender.userInfo);

    //DDLogInfo(@"compressing... : %f", exporter.progress);
    [SVProgressHUD showProgress:exporter.progress status:KPLocalizedString(@"compressing video")];
    
    //Zvika fix
   // NSLog(@"exporter.progress=%f timePassed=%f", exporter.progress,-self.comperssionTimerStated.timeIntervalSinceNow );
    if (exporter.progress == 1 || (exporter.progress == 0 && -self.comperssionTimerStated.timeIntervalSinceNow > 1)) {
        [SVProgressHUD dismiss];
        [self.compressionProgressTimer invalidate];
    }
    
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1) //delete story
    {
        if (buttonIndex == 0) {
            [self.delegate recordingSuccessful:nil withLength:0 andMode:self.mode andPreviewImage:self.previewImage];
            if (self.completion) {
                self.completion(nil, 0, 0, nil);
            }
            [self.navigationController popViewControllerAnimated:YES];

            return;
        }
    }

    if (buttonIndex == 0) {
        [self cancelRecordingAndQuit];
    }
}

- (void)showShareItem {
    ItemShareViewController *avc = [[ItemShareViewController alloc] initWithItem:(id)self.item isAfterAddItem:YES];
    [self.navigationController pushViewController:avc animated:YES];
}

- (void)gotoShareScreen {
    if (self.moviePlayer.contentURL != nil) {
        NSMutableDictionary *commentData = [[NSMutableDictionary alloc] init];
        [commentData setValue:[NSData dataWithContentsOfURL:self.moviePlayer.contentURL] forKey:@"videoData"];
        [commentData setValue:@(self.recordingLength) forKey:@"recordingLength"];
        commentData[@"previewImage"] = self.previewImage;
        [GlobalUtils storeCurrentCommentData:commentData];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [self showShareItem];
    });
}

#pragma mark - DIYCamDelegate

- (void)camReady:(DIYCam *)cam {
    DDLogInfo(@"Ready");
}

- (void)camDidFail:(DIYCam *)cam withError:(NSError *)error {
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"error"
                                                                message:error.localizedDescription
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:ac animated:YES completion:nil];

    DDLogInfo(@"Fail");
}

- (void)camModeWillChange:(DIYCam *)cam mode:(DIYAVMode)mode {
    DDLogInfo(@"Mode will change");
}

- (void)camModeDidChange:(DIYCam *)cam mode:(DIYAVMode)mode {
    DDLogInfo(@"Mode did change");
}

- (void)camCaptureStarted:(DIYCam *)cam {
    DDLogInfo(@"Capture started");
}

- (void)camCaptureStopped:(DIYCam *)cam {
    DDLogInfo(@"Capture stopped");
}

- (void)camCaptureProcessing:(DIYCam *)cam {
    DDLogInfo(@"Capture processing");
}

- (void)camCaptureComplete:(DIYCam *)cam withAsset:(NSDictionary *)asset {
    DDLogInfo(@"Capture complete. Asset: %@", asset);

    //NSData *pdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:[asset valueForKey:@"thumb"]]];
    //self.previewImage = [UIImage imageWithData:pdata];

    dispatch_async(dispatch_get_main_queue(), ^{
        self.deleteButton.hidden = NO;
        self.previewButton.hidden = NO;
        self.recordButton.hidden = YES;
        self.instructionsLabel.alpha = 0;

        if (self.videoRecordingWaitingForUpload) {
            [self cropRecordedVideo:[asset valueForKey:@"path"]];
        }
    });
}

- (void)camCaptureLibraryOperationComplete:(DIYCam *)cam {
    DDLogInfo(@"Library save complete");
}

- (void)deleteStory:(id)sender {
    UIActionSheet *asheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:KPLocalizedString(@"cancel") destructiveButtonTitle:KPLocalizedString(@"delete recording") otherButtonTitles:nil, nil];
    asheet.tag = 1;
    [asheet showInView:self.view];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    self.rightButtonEnabled = (textView.text.length > 0);
}

- (void)textViewDidEndEditing:(UITextView *)textView {
}

@end
