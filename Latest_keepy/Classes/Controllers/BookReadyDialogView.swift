//
//  BookReadyDialogView.swift
//  Keepy
//
//  Copyright (c) 2017 Keepy. All rights reserved.
//

import Foundation
import UIKit

final class BookReadyDialogView: AlertView {
    @IBOutlet weak var cancelButton: UIButton?
    @IBOutlet weak var bookImageView: UIImageView?
    
    var actionCancel: () -> () = {}
    var bookImage: UIImage? {
        didSet {
            bookImageView?.image = bookImage
        }
    }
    
    override class func create(_ nibName: String = "BookReadyDialogView") -> BookReadyDialogView {
        let alert = UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BookReadyDialogView
        alert.useHapticFeedback = false
        alert.confirmButton?.layer.cornerRadius = 12
        alert.container?.layer.cornerRadius = 5
        
        let shadowPath = UIBezierPath(rect: alert.container!.bounds)
        alert.container?.layer.masksToBounds = false
        alert.container?.layer.shadowColor = UIColor.black.cgColor
        alert.container?.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        alert.container?.layer.shadowOpacity = 0.5
        alert.container?.layer.shadowPath = shadowPath.cgPath
        
        return alert
    }

    @IBAction func onCancelClick(_ sender: AnyObject) {
        if hidesAfterConfirmClick {
            hide()
        }
        actionCancel()
    }
}
