//
//  TitleCell.m
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "TitleCell.h"

@implementation TitleCell

+ (CGFloat)height {
    return 21.0f;
}

@end
