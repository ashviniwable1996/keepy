//
//  DKPlaybackPosition.m

//
//  Created by Daniel Karsh on 4/16/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "DKPlaybackPosition.h"

@implementation DKPlaybackPosition

@synthesize loading, slider, currentTime, remainingTime;


- (void)drawRect:(CGRect)rect {
    [self setBackgroundColor:[UIColor clearColor]];
    [self setClipsToBounds:YES];
    [self.slider setThumbImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
    [self.slider setThumbImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateSelected];
    [self.slider setThumbImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateHighlighted];
    [self.slider setThumbImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateDisabled];
    [self.slider setThumbImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateReserved];
    self.loading.center = CGPointMake(self.loading.center.x, self.loading.center.y + 5);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
