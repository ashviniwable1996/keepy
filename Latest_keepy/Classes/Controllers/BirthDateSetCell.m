//
//  BirthDateSetCell.m
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "BirthDateSetCell.h"

@interface BirthDateSetCell ()

@property(nonatomic, weak) IBOutlet UILabel *birthDateLabel;
@property(nonatomic, weak) IBOutlet UILabel *month;
@property(nonatomic, weak) IBOutlet UILabel *day;
@property(nonatomic, weak) IBOutlet UILabel *year;

@end

@implementation BirthDateSetCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.birthDateLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    self.month.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
    self.day.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16];
    self.year.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
}

+ (CGFloat)height {
    return 85.0f;
}

- (void)setBirthDateDate:(NSDate *)birthDateDate {
    if (_birthDateDate != birthDateDate) {
        _birthDateDate = birthDateDate;
        [self setNeedsLayout];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    [formatter setDateFormat:@"MMM"];
    self.month.text = [[formatter stringFromDate:self.birthDateDate] uppercaseString];
    [formatter setDateFormat:@"dd"];
    self.day.text = [formatter stringFromDate:self.birthDateDate];
    [formatter setDateFormat:@"yyyy"];
    self.year.text = [formatter stringFromDate:self.birthDateDate];
}

- (IBAction)birthDate:(id)sender {
    [self.delegate birthDate];
}

@end
