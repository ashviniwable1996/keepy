//
//  KPWebViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 5/22/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KPWebViewController.h"

@interface KPWebViewController () <UIWebViewDelegate>

@property(nonatomic, strong) NSURL *url;
@property(nonatomic, strong) UIWebView *webView;
@property(nonatomic, strong) UIActivityIndicatorView *av;
@property(nonatomic, strong) NSString *titleStr;

@end

@implementation KPWebViewController

- (instancetype)initWithUrl:(NSString *)aURL andTitle:(NSString *)title {
    self = [super initWithNibName:@"KPWebViewController" bundle:nil];
    if (self) {

        DDLogInfo(@"%@", aURL);

        self.titleStr = title;
        self.url = [NSURL URLWithString:aURL];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = self.titleStr;

    if (self.navigationController.view.tag == 1) {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:@"close", @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];
    } else {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:@"settings", @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:nil];
    }

    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.webView.delegate = self;
    self.webView.backgroundColor = [UIColor clearColor];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    [self.view addSubview:self.webView];

    self.av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.av.hidesWhenStopped = YES;
    [self.av startAnimating];

    CGRect aframe = self.av.frame;
    aframe.origin.x = (self.view.frame.size.width - aframe.size.width) / 2;
    aframe.origin.y = (self.view.frame.size.height - aframe.size.height) / 2;
    self.av.frame = aframe;

    [self.view addSubview:self.av];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    self.webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)topBtnTap:(UIButton *)sender {
    self.webView.delegate = nil;

    if (self.navigationController.view.tag == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }

}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.av stopAnimating];
}

@end
