//
//  DKMultyUploaderManger.h
//  Keepy
//
//  Created by Daniel Karsh on 5/1/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DKVideoUploaderManager;
@class KPLocalItem;

@interface DKMultyUploaderManger : NSObject {

}
@property NSMutableArray *allVideosUploadingProccess;

+ (DKMultyUploaderManger *)sharedClient;

- (void)addUploaderWithMovieURL:(NSURL *)myMovieFileURL
                  andThumbImage:(UIImage *)thumbnailImage;

- (void)addUploaderWithItem:(KPLocalItem *)item;
@end
