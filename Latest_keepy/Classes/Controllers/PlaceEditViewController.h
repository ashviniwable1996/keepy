//
//  PlaceEditViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/25/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@class KPLocalPlace;

@protocol PlaceEditDelegate

- (void)placeAdded:(KPLocalPlace *)place;

@end

@interface PlaceEditViewController : KPPopViewController

@property(nonatomic, weak) id <PlaceEditDelegate> delegate;

- (instancetype)initWithPlace:(Place *)place;

@end
