//
//  FullScreenItemCell.h
//  Keepy
//
//  Created by Yaniv Solnik on 4/9/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KPLocalItem;

@interface FullScreenItemCell : UITableViewCell

@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic) BOOL isActive;
@property(nonatomic) NSInteger commentIndex;
@property(nonatomic, strong) NSTimer *audioTimer;

- (void)clearPreviousContentArtifacts;

@end
