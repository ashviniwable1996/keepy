//
//  KidSelectViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/19/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KidSelectViewController.h"

@interface KidSelectViewController ()

@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end

@implementation KidSelectViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData {
    for (UIView *aview in self.scrollView.subviews) {
        [aview removeFromSuperview];
    }

    float ax = 5;
    float ay = 5;
    NSArray *kids = [[UserManager sharedInstance] myKids];
    for (int i = 0; i < [kids count]; i++) {
        Kid *kid = (Kid *) [kids objectAtIndex:i];
        UIView *kidView = [self createKidView:kid];
        kidView.tag = i;
        kidView.frame = CGRectMake(ax, ay, 50, 50);

        ax += 55;
        if (ax > 145) {
            ax = 5;
            ay += 55;
        }
    }

    UIView *kidView = [self createKidView:nil];
    kidView.tag = -1;
    kidView.frame = CGRectMake(ax, ay, 50, 50);

    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, ay + 55);
}

- (UIView *)createKidView:(Kid *)kid {
    UIView *kidsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];

    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    img.image = [UIImage imageNamed:@"kidPicBg"];
    [kidsView addSubview:img];

    if (kid != nil) {
        img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        if (kid.image == nil) {
            img.image = [GlobalUtils getKidDefaultImage:kid.gender.intValue];
        }
        else {
            [img setImageWithAsset:kid.image];
        }
        [kidsView addSubview:img];
    }

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont boldSystemFontOfSize:12];
    lbl.textAlignment = NSTextAlignmentCenter;
    if (kid == nil) {
        lbl.text = @"Add Kid";
    } else {
        lbl.text = kid.name;
    }

    [kidsView addSubview:lbl];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(kidTap:)];

    [kidsView addGestureRecognizer:gesture];


    [self.scrollView addSubview:kidsView];

    return kidsView;
}

- (void)kidTap:(UITapGestureRecognizer *)gesture {
    Kid *kid = nil;
    if (gesture.view.tag > -1) {
        NSArray *kids = [[UserManager sharedInstance] myKids];
        kid = (Kid *) [kids objectAtIndex:gesture.view.tag];
    }
    [self.delegate kidSelected:kid];
}

- (CGSize)contentSizeForViewInPopover {
    return CGSizeMake(200, 200);
}

@end
