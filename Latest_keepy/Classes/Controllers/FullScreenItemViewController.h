//
//  FullScreenItemViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/24/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

@interface FullScreenItemViewController : UIViewController

- (instancetype)initWithItem:(Item *)item withMode:(int)mode andCommentIndex:(int)commentIndex;

- (instancetype)initWithItem:(Item *)item withMode:(int)mode;

@property(nonatomic) float initialY;
@property(nonatomic, strong) Item *item;
@property(nonatomic) BOOL isActive;
@property(nonatomic) int commentIndex;

@end
