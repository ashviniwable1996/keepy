//
//  ItemShareViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 7/2/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@interface ItemShareViewController : KPPopViewController

@property(copy) NSString *short_url;

- (instancetype)initWithItem:(KPLocalItem *)item;

- (instancetype)initWithItem:(KPLocalItem *)item isAfterAddItem:(BOOL)isAfterAddItem;

@end
