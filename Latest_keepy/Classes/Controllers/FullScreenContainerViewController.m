//
//  FullScreenContainerViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 4/9/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "FullScreenContainerViewController.h"
#import "FullScreenItemCell.h"
#import "AppDelegate.h"
#import "ChildViewController.h"
@interface FullScreenContainerViewController () <UITableViewDataSource, UITableViewDelegate,DetailViewOfKids, UIPageViewControllerDataSource,UIPageViewControllerDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSArray *items;
@property(nonatomic) NSInteger currentIndex;
@property(nonatomic) NSInteger commentIndex;
@property(nonatomic, strong) FullScreenItemCell *currentCell;
@property (nonatomic) BOOL isLastCellVisible;
@property (strong, nonatomic) UIPageViewController *pageController;

@end

@implementation FullScreenContainerViewController

#pragma mark - View Life Cycle

- (instancetype)initWithItems:(NSArray *)items andIndex:(NSInteger)index withCommentIndex:(NSInteger)commentIndex {
    self = [super initWithNibName:@"FullScreenContainerViewController" bundle:nil];
    if (self) {
       // self.view.frame = UIScreen.mainScreen.bounds;
        [self.view setFrame:UIScreen.mainScreen.bounds];
        self.items = items;
        self.currentIndex = index;
        self.commentIndex = commentIndex;
    }
    NSLog(@"FNTRACE FSCVC1-1 %ld", self.currentIndex);
    NSLog(@"FNTRACE FSCVC1-2 %ld", commentIndex);
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       // self.view.frame = UIScreen.mainScreen.bounds;
        [self.view setFrame:UIScreen.mainScreen.bounds];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setFrame:UIScreen.mainScreen.bounds];
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // added by Ashvini
   // [self pageSetup];
 
}

- (void)viewWillAppear:(BOOL)animated {
     [super viewWillAppear:animated];
    [self pageSetup];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fullscreen", @"source", nil]];
    
//    float w = self.view.bounds.size.width;
//    float h = self.view.bounds.size.height;
//    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, w, h)];
//    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//    self.tableView.showsVerticalScrollIndicator = NO;
//    self.tableView.showsHorizontalScrollIndicator = NO;
//    self.tableView.transform = CGAffineTransformMakeRotation(-M_PI * 0.5);
//    [self.tableView setFrame:CGRectMake(0, 0, w, h)];
//    self.tableView.backgroundColor = [UIColor clearColor];
//
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    self.tableView.separatorColor = [UIColor clearColor];
//    self.tableView.bounces = YES;
//    self.tableView.dataSource = self;
//    self.tableView.delegate = self;
//    self.tableView.pagingEnabled = YES;
//    self.tableView.autoresizesSubviews = YES;
//    [self.view addSubview:self.tableView];
   
    // by Ashvini
    //[self pageSetup];

    [GlobalUtils clearStoriesPlayed];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(itemDeleted:)
     name:ITEM_DELETED_NOTIFICATION
     object:nil];
    
    if (self.currentIndex == -1) {
        NSLog(@"Return if current index is %ld",(long)self.currentIndex);
    } else {
        if (self.currentIndex > 0) {
            NSLog(@"current index is %ld",(long)self.currentIndex);
            
//            double delayInSeconds = 0.1;
//            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW + delayInSeconds, 0);
//            dispatch_after(delayTime, dispatch_get_main_queue(), ^(void){
//                UITableViewScrollPosition sp = UITableViewScrollPositionNone;
//                if (self.currentIndex != NSNotFound){
//                    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0] atScrollPosition:sp animated:NO];
//                }
//            });
            
        }
        
    }
}

- (void)pageSetup {
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    self.pageController.delegate   = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    UIViewController *initialViewController = [self viewControllerAtIndex:self.currentIndex];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.pageController setViewControllers:@[initialViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [self addChildViewController:self.pageController];
        [[self view] addSubview:[self.pageController view]];
        [self.pageController didMoveToParentViewController:self];
    });
}


-(void)closeTab {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"fullscreen", @"result" : @"cancel"}];
    
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)fullScreenContainerExit:(UIStoryboardSegue *)segue {
    
}

- (void)viewDidLayoutSubviews {
    //[super viewDidLayoutSubviews];
    
    /*
     float w = self.view.bounds.size.width;
     float h = self.view.bounds.size.height;
     [self.tableView setFrame:CGRectMake(0, 0, w, h)];
     DDLogInfo(@"zzzz %2f %2f", w, h);
     [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
     */
    
    //[self scrollViewDidEndDecelerating:self.tableView];
//    if (_isLastCellVisible) {
    NSLog(@"currentIndex:%ld", (long)self.currentIndex);
    NSLog(@"FNTRACE FSCVC2-1 %ld", self.currentIndex);
    
//    if (self.currentIndex > 0) {
////        [self.tableView setContentOffset:CGPointMake(0, 375 * self.currentIndex) animated:YES];
//        double delayInSeconds = 1.0;
//        dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//        dispatch_after(delayTime, dispatch_get_main_queue(), ^(void){
//            UITableViewScrollPosition sp = UITableViewScrollPositionNone;
//            if (self.currentIndex != NSNotFound){
//         [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0] atScrollPosition:sp animated:NO];
//            }
//        });
//
//    }
//    }
}


- (void) viewWillDisappear:(BOOL)animated{
    self.currentCell.isActive = NO;
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
//-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //float w = self.view.bounds.size.width;
    //float h = self.view.bounds.size.height;
    //DDLogInfo(@"yyyy %2f %2f", w, h);
    //if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
    //[self.tableView setFrame:CGRectMake(0, 0, w, h)];
    //else
    //    [self.tableView setFrame:CGRectMake(0, 0, w, h)];
    
    /*
     for (UIView *aview in self.tableView.subviews)
     {
     if ([aview isKindOfClass:[UITableViewCell class]])
     {
     CGRect aframe = aview.frame;
     aframe.origin.y = 0;
     aframe.size.height = (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) ? h:w;
     aframe.size.width = (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) ? w:h;
     
     aview.frame = aframe;
     }
     }
     */
    //[self.tableView reloadData];
    /*
     self.tableView.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
     if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
     [self.tableView setBounds:CGRectMake(0, 0, w, h)];
     else
     [self.tableView setBounds:CGRectMake(0, 0, h, w)];
     
     self.tableView.transform = CGAffineTransformMakeRotation(-M_PI * 0.5);
     DDLogInfo(@"w:%2f h:%2f", w, h);
     [self.tableView beginUpdates];
     [self.tableView endUpdates];
     */
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.view.bounds.size.width;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    KPLocalItem *currentItem = [self.items objectAtIndex:indexPath.row];
    [((FullScreenItemCell *) cell) clearPreviousContentArtifacts];
    [(FullScreenItemCell *) cell setIsActive:NO];
    [(FullScreenItemCell *) cell setItem:currentItem];

}

// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *kCellIdentifier = @"FullScreenItemCell";
    FullScreenItemCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[FullScreenItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
        //cell.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // Set up the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        if (indexPath.row == self.currentIndex) {
            KPLocalItem *currentItem = [self.items objectAtIndex:indexPath.row];

            NSLog(@"FNTRACE FullScreenContainerViewController: Item %@",currentItem);
            
            [(FullScreenItemCell *) cell setCommentIndex:self.commentIndex];
            [(FullScreenItemCell *) cell setIsActive:YES];
            
            self.commentIndex = 0;
            self.currentIndex = -1;
            self.currentCell = (FullScreenItemCell *) cell;
            _isLastCellVisible = YES;
//            [self viewDidLayoutSubviews];
        }
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSIndexPath *firstVisibleIndexPath = [[self.tableView indexPathsForVisibleRows] objectAtIndex:0];
    
    NSInteger currentIndex = self.currentIndex;
    NSInteger newIndex = firstVisibleIndexPath.row;
    
    if (currentIndex == newIndex) {
        
        //self.currentCell.isActive = !self.currentCell.isActive;
        
    } else {
        
        //self.currentCell.isActive = NO;
        FullScreenItemCell *cell = (FullScreenItemCell *) [self.tableView cellForRowAtIndexPath:firstVisibleIndexPath];
        if(cell){
            cell.isActive = YES;
            self.currentCell = cell;
            
        }
        
    }
    
    
    self.currentIndex = newIndex;
    //DDLogInfo(@"first visible cell's section: %i, row: %i", firstVisibleIndexPath.section, firstVisibleIndexPath.row);
}

- (void)itemDeleted:(NSNotification *)notification {
    //IF this come from edit "add story"
    if ([notification.object boolValue]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObjectsFromArray:self.items];
    for (NSInteger i = [arr count] - 1; i > -1; i--) {
        Item *item = (Item *) [arr objectAtIndex:i];
        if (item.itemId.intValue == 0 || item.itemId.intValue == [notification.object intValue]) {
            [arr removeObjectAtIndex:i];
            //break;
        }
    }
    
    self.items = arr;
    
    if ([self.items count] == 0) {
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        return;
    }
    
    [self.tableView reloadData];
    
    if (self.currentIndex > [self.items count] - 1) {
        self.currentIndex--;
    }
    
    if (self.currentIndex < [self.tableView numberOfRowsInSection:0] && self.currentIndex > -1) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    self.currentIndex = [(ChildViewController *)viewController currentIndex];
    if (self.currentIndex == 0) {
        return nil;
    }
    self.currentIndex--;
    return [self viewControllerAtIndex:self.currentIndex];
}

//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
//    return [self.items count];
//}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    self.currentIndex = [(ChildViewController *)viewController currentIndex];
    NSInteger localIndex = self.currentIndex;
    self.currentIndex++;
    if (self.currentIndex >= self.items.count) {
        self.currentIndex = localIndex;
        return nil;
    }
    return [self viewControllerAtIndex:self.currentIndex];
    
}

- (ChildViewController *)viewControllerAtIndex:(NSUInteger)index {
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboard2" bundle:nil];
//    ChildViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChildViewController"];
//    vc.commentIndex = self.commentIndex;
//    vc.currentIndex = self.currentIndex;
//    vc.item = self.items[self.currentIndex];
//    [vc clearPreviousContentArtifacts];
//    [vc setIsActive:YES];
//    vc.delegate = self;
//    return vc;
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboard2" bundle:nil];
    ChildViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChildViewController"];
    dispatch_async(dispatch_get_main_queue(), ^{
        vc.commentIndex = self.commentIndex;
        vc.currentIndex = index;
        vc.item = self.items[index];
        [vc clearPreviousContentArtifacts];
        [vc setIsActive:YES];
        vc.delegate = self;
    });
    return vc;
}

@end
