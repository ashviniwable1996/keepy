//
//  EditKidViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/10/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>
#import <Photos/Photos.h>



#import "EditKidViewController.h"
#import "KPDataParser.h"
#import "ChooseMemoryViewController.h"
#import "CircleCropViewController.h"
#import "ChoosePictureViewController.h"
#import "BirthDate2ViewController.h"
#import "KPLocalKid.h"
#import "KPLocalAsset.h"

#import "Keepy-Swift.h"

@interface EditKidViewController () <UITextFieldDelegate, ChoosePictureViewControllerDelegate, CircleCropViewControllerDelegate>

@property(nonatomic, strong) KPLocalKid *kid;
@property(nonatomic, strong) NSDate *birthdate;
@property(nonatomic) BOOL photoChanged;
@property(nonatomic, strong) UIImage *kidImage;
@property(nonatomic, strong) UIImage *originalKidImage;
@property(nonatomic) float photo_scale;
@property(nonatomic) float photo_x;
@property(nonatomic) float photo_y;
@property (strong, nonatomic) NSDictionary<NSString *, id> *tweakValues;
@property (nonatomic) BOOL isFirstTime;
@property(nonatomic, strong) IBOutlet UIImageView *profileImageView;
@property(nonatomic, strong) IBOutlet UITextField *nameTF;
@property(nonatomic, strong) IBOutlet UIButton *boyBtn;
@property(nonatomic, strong) IBOutlet UIButton *girlBtn;
@property(nonatomic, strong) IBOutlet UIButton *birthDateButton;

@property(nonatomic, strong) IBOutlet UILabel *yearLbl;
@property(nonatomic, strong) IBOutlet UILabel *monthLbl;
@property(nonatomic, strong) IBOutlet UILabel *dayLbl;

@property(nonatomic, strong) IBOutlet UILabel *addPhotoLabel;
@property(nonatomic, strong) IBOutlet UILabel *birthDateLabel;
@property(nonatomic, strong) IBOutlet UILabel *boyLabel;
@property(nonatomic, strong) IBOutlet UILabel *girlLabel;

@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property(nonatomic, strong) IBOutlet UIView *container;

@property(nonatomic, strong) NSString *noteText;
@property(nonatomic) int noteAssetId;
@property(nonatomic, strong) UIImage *noteImage;

@property(nonatomic, strong) UIButton *deleteButton;

@property(nonatomic, strong) UINavigationController *photoNavController;
@property(nonatomic) BOOL isNew;
@property (nonatomic) float ax;
@property (nonatomic) float ay;
- (IBAction)imageTap:(id)sender;

- (IBAction)dateTap:(id)sender;

- (IBAction)boyGirlTap:(id)sender;

@end

@implementation EditKidViewController
@synthesize ax,ay;
- (instancetype)initWithKid:(KPLocalKid *)akid {
    self = [super initWithNibName:@"EditKidViewController" bundle:nil];
    if (self) {
        self.kid = akid;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if ([[UserManager sharedInstance] isAfterRegistration]) {
        [[Mixpanel sharedInstance] track:@"onboarding-add-kid-open"];
        self.title = KPLocalizedString(@"add a child");
        
        // If we're not mom or dad, don't show unlimitedScreen.
        User *me = [[UserManager sharedInstance] getMe];
        
        BOOL showUnlimitedScreen = [me.parentType intValue] <= 1;
        if (showUnlimitedScreen)
        {
            AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [d reloadTweaks];
            [[Mixpanel sharedInstance] track:TweakPromoUnlimitedAfterSignUP properties:@{@"source": TweakPromoUnlimitedAfterSignUP}];

            long tweakValue = [d.tweakValues[TweakPromoUnlimitedAfterSignUP] integerValue];
            if (tweakValue == 1){
                [GlobalUtils showUpgradeScreenWithParameters:nil fromViewController:@"After SignUP"];
            }
        }
    } else {
        self.title = KPLocalizedString(@"add kid");
    }

    self.isNew = (self.kid == nil);

    if (self.kid == nil) {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-kid", @"source", nil]];
    } else {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-kid", @"source", nil]];
    }

    if (self.navigationController == nil) {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];
    }
    else {
        if ([[[UserManager sharedInstance] myKids] count] == 0) {
            [self setButtons:nil andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];

            if ([[UserManager sharedInstance] isAfterRegistration]) {
                self.navigationItem.hidesBackButton = YES;
            }
        }
        else {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];
        }
    }

    for (UIControl *control in self.container.subviews) {
        if ([control isKindOfClass:[UILabel class]]) {
            [(UILabel *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:14]];
        } else if ([control isKindOfClass:[UITextField class]]) {
            [(UITextField *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:17]];
            [(UITextField *) control setTextColor:UIColorFromRGB(0x4d3c36)];
        }
    }


    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardDidShow:)
                   name:UIKeyboardDidShowNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];


}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    float ax = 5 + self.birthDateButton.frame.origin.x;
//    float ay = 7 + self.birthDateButton.frame.origin.y;
//
//    //Month
//    self.monthLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay, 53, 15)];
//    self.monthLbl.backgroundColor = [UIColor clearColor];
//    self.monthLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
//    self.monthLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
//    self.monthLbl.textAlignment = NSTextAlignmentCenter;
//    [self.container addSubview:self.monthLbl];
//
//    //Day
//    self.dayLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay + 15, 53, 15)];
//    self.dayLbl.backgroundColor = [UIColor clearColor];
//    self.dayLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
//    self.dayLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16];
//    self.dayLbl.textAlignment = NSTextAlignmentCenter;
//    [self.container addSubview:self.dayLbl];
//
//    //Year
//    self.yearLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay + 30, 53, 15)];
//    self.yearLbl.backgroundColor = [UIColor clearColor];
//    self.yearLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
//    self.yearLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
//    self.yearLbl.textAlignment = NSTextAlignmentCenter;
//    self.yearLbl.alpha = 0.8;
//    [self.container addSubview:self.yearLbl];
//
//    self.birthDateLabel.text = KPLocalizedString(@"birth date");
//    self.boyLabel.text = KPLocalizedString(@"boy");
//    self.girlLabel.text = KPLocalizedString(@"girl");
//    self.nameTF.placeholder = KPLocalizedString(@"name");
//    self.nameTF.autocorrectionType = UITextAutocorrectionTypeNo;
//    if (self.kid != nil) {
//        self.title = KPLocalizedString(@"update kid");
//        self.nameTF.text = self.kid.name;
//        //[self.genderSC setSelectedSegmentIndex:self.kid.gender.intValue];
//        if (self.kid.gender == 1) {
//            self.girlBtn.selected = YES;
//        } else if (self.kid.gender == 2) {
//            self.boyBtn.selected = YES;
//        }
//
//
//        KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:self.kid.imageAssetID];
//
//        if (imageAsset != nil) {
//            self.addPhotoLabel.text = KPLocalizedString(@"update photo");
//
//            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//
//                self.profileImageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];
//                [self.profileImageView setContentMode:UIViewContentModeScaleAspectFit];
//            }];
//
//        }
//        else {
//            self.addPhotoLabel.text = KPLocalizedString(@"add photo");
//        }
//
//        if (self.kid.birthdate > 0) {
//            self.birthdate = [NSDate dateWithTimeIntervalSince1970:self.kid.birthdate];
//        }
//    }
//    else {
//        self.addPhotoLabel.text = KPLocalizedString(@"add photo");
//    }
//
//    self.nameTF.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:18];
//
//    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
//    gesture.cancelsTouchesInView = NO;
//    [self.view addGestureRecognizer:gesture];
   
    [self viewDidLayoutSubviews];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    ax = 5 + self.birthDateButton.frame.origin.x;
    ay = 7 + self.birthDateButton.frame.origin.y;
    [self viewDidLayoutSubviews];

}
- (void)viewDidLayoutSubviews {
    if (self.deleteButton != nil) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        // make some UI changes
        // ...
        // show actionSheet for example
   
//    if (!_isFirstTime) {
        
//        _isFirstTime = YES;
    
    
        
//        float ax = -5 + self.view.center.x;
//        float ay = -7 + self.view.center.y;
        ax = 5 + self.birthDateButton.frame.origin.x;
        ay = 7 + self.birthDateButton.frame.origin.y;
    //Month
    self.monthLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay, 53, 15)];
    self.monthLbl.backgroundColor = [UIColor clearColor];
    self.monthLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
    self.monthLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
    self.monthLbl.textAlignment = NSTextAlignmentCenter;
    [self.container addSubview:self.monthLbl];
    
    //Day
    self.dayLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay + 15, 53, 15)];
    self.dayLbl.backgroundColor = [UIColor clearColor];
    self.dayLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
    self.dayLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16];
    self.dayLbl.textAlignment = NSTextAlignmentCenter;
    [self.container addSubview:self.dayLbl];
    
    //Year
    self.yearLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay + 30, 53, 15)];
    self.yearLbl.backgroundColor = [UIColor clearColor];
    self.yearLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
    self.yearLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
    self.yearLbl.textAlignment = NSTextAlignmentCenter;
    self.yearLbl.alpha = 0.8;
    [self.container addSubview:self.yearLbl];
    
    self.birthDateLabel.text = KPLocalizedString(@"birth date");
    self.boyLabel.text = KPLocalizedString(@"boy");
    self.girlLabel.text = KPLocalizedString(@"girl");
    self.nameTF.placeholder = KPLocalizedString(@"name");
    self.nameTF.autocorrectionType = UITextAutocorrectionTypeNo;
    if (self.kid != nil) {
        self.title = KPLocalizedString(@"update kid");
        self.nameTF.text = self.kid.name;
        //[self.genderSC setSelectedSegmentIndex:self.kid.gender.intValue];
        if (self.kid.gender == 1) {
            self.girlBtn.selected = YES;
        } else if (self.kid.gender == 2) {
            self.boyBtn.selected = YES;
        }
        
        
        KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:self.kid.imageAssetID];
        
        if (imageAsset != nil) {
            self.addPhotoLabel.text = KPLocalizedString(@"update photo");
            
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
                self.profileImageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];
                [self.profileImageView setContentMode:UIViewContentModeScaleAspectFit];
            }];
            
        }
        else {
            self.addPhotoLabel.text = KPLocalizedString(@"add photo");
        }
        
        if (self.kid.birthdate > 0) {
            self.birthdate = [NSDate dateWithTimeIntervalSince1970:self.kid.birthdate];
        }
    }
    else {
        self.addPhotoLabel.text = KPLocalizedString(@"add photo");
    }
    
    self.nameTF.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:18];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gesture];
    
         });
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setBackgroundImage:[[UIImage imageNamed:@"delete-photo-buttonBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];

    [self.deleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    self.deleteButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    [self.deleteButton setTitle:KPLocalizedString(@"delete kid") forState:UIControlStateNormal];

    self.deleteButton.frame = CGRectMake((self.view.frame.size.width - 156) / 2, 460, 156, 44);
    [self.deleteButton addTarget:self action:@selector(deleteTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.container addSubview:self.deleteButton];

    self.deleteButton.hidden = (self.kid == nil);

    self.scrollView.contentSize = self.container.frame.size;
    self.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

-(void)getMediaApproval{
    //zvika160810
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        // Access has been granted.
        [self navigateAfterAproval];
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        // Access has been denied.
        [self navigateAfterAproval];
    }
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                [self navigateAfterAproval];
                // Access has been granted.
            }
            
            else {
                // Access has been denied.
                [self navigateAfterAproval];
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
    }
}



#pragma mark - Actions

- (IBAction)imageTap:(id)sender {
    
    [self getMediaApproval];
    }


-(void)navigateAfterAproval{
  dispatch_async(dispatch_get_main_queue(), ^{
    [self.nameTF resignFirstResponder];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"ChoosePicture" bundle:nil];
    
    UINavigationController *nc = (UINavigationController *) [sb instantiateViewControllerWithIdentifier:@"ChoosePictureNavigationController"];
    ChoosePictureViewController *vc = [nc.viewControllers firstObject];
    vc.delegate = self;
    if (self.originalKidImage != nil) {
        [vc startWithPhoto:self.originalKidImage scale:self.photo_scale point:CGPointMake(self.photo_x, self.photo_y)];
    }
    [self.navigationController presentViewController:nc animated:YES completion:nil];
  });
}


- (void)cancelCircleCropping {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

}


- (void)doneCircleCropping:(NSDictionary *)selectedPhotosDict {
    [self doneChoosingPicture:selectedPhotosDict];
}

- (void)doneChoosingPicture:(NSDictionary *)selectedPhotosDict {
    if (selectedPhotosDict) {
        UIImage *image = selectedPhotosDict[@"croppedImage"];
        self.originalKidImage = selectedPhotosDict[@"originalImage"];

        self.photo_scale = [selectedPhotosDict[@"photo_scale"] floatValue];
        self.photo_x = [selectedPhotosDict[@"photo_x"] floatValue];
        self.photo_y = [selectedPhotosDict[@"photo_y"] floatValue];

        self.kidImage = image;
        self.profileImageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];
        self.addPhotoLabel.text = KPLocalizedString(@"update photo");
        self.photoChanged = YES;
    }
    [self checkEnableSaveButton];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)addNewKidsFirstMemory{
    if (self.kid == nil) {
        
        // self.kid = [Kid MR_createEntity];
        self.kid = [KPLocalKid create];
        self.kid.name = self.nameTF.text;
        self.kid.gender = 0;
        if (self.boyBtn.selected) {
            self.kid.gender = 2;
        } else if (self.girlBtn.selected) {
            self.kid.gender = 1;
        }
        
        self.kid.isMine = YES;
        if (self.birthdate != nil) {
            self.kid.birthdate = self.birthdate.timeIntervalSince1970;
        }
        [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"adding %@", self.kid.name]];
        
        [[ServerComm instance] addKid:self.kid.name withGender:self.kid.gender andBirthdate:self.birthdate withSuccessBlock:^(id result) {
            [self afterKidAddedToServer:result];
        }                andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"error adding %@", self.kid.name]];
        }];
        
        return;
    }
    
    self.kid.name = self.nameTF.text;
    self.kid.gender = 0;
    if (self.boyBtn.selected) {
        self.kid.gender = 2;
    } else if (self.girlBtn.selected) {
        self.kid.gender = 1;
    }
    
    self.kid.isMine = YES;
    if (self.birthdate != nil) {
        self.kid.birthdate = self.birthdate.timeIntervalSince1970;
    }
    
    [self.kid saveChanges];
    
    RequestSuccessBlock successBlock = ^(id result) {
        [self afterKidAddedToServer:result];
    };
    [SVProgressHUD showWithStatus:KPLocalizedString(@"processing")];
    [[ServerComm instance] updateKid:self.kid.serverID withName:self.kid.name andGender:self.kid.gender andBirthdate:self.birthdate withSuccessBlock:successBlock andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error updating kid")];
    }];
}

- (void)topBtnTap:(UIButton *)sender {
    [self.nameTF resignFirstResponder];

    if (sender.tag == 1) //Ok
    {
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        
        if (status == PHAuthorizationStatusAuthorized) {
            // Access has been granted.
            [self addNewKidsFirstMemory];
        }
        
        else if (status == PHAuthorizationStatusDenied) {
            // Access has been denied.
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];

        }
        
        else if (status == PHAuthorizationStatusNotDetermined) {
            
            // Access has not been determined.
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                
                if (status == PHAuthorizationStatusAuthorized) {
                    // Access has been granted.
                    [self addNewKidsFirstMemory];
                }
                
                else {
                    // Access has been denied.
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
                }
            }];
        }
        
        else if (status == PHAuthorizationStatusRestricted) {
            // Restricted access - normally won't happen.
             [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        }

    }
    else {
        if (self.kid == nil) {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-kid", @"source", @"cancel", @"result", nil]];
        } else {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-kid", @"source", @"cancel", @"result", nil]];
        }

        if (self.navigationController == nil) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (IBAction)birthDate2Exit:(UIStoryboardSegue *)segue {
    BirthDate2ViewController *birthDate2ViewController = (BirthDate2ViewController *) segue.sourceViewController;
    if (birthDate2ViewController.birthDateDate) {
        self.birthdate = birthDate2ViewController.birthDateDate;
    }
}

- (IBAction)dateTap:(id)sender {
    [self.nameTF resignFirstResponder];
    UIStoryboard *onboardStoryboard = [UIStoryboard storyboardWithName:@"Onboard2" bundle:nil];
    
    
    BirthDate2ViewController *birthDate2ViewController = (BirthDate2ViewController *) [onboardStoryboard instantiateViewControllerWithIdentifier:@"BirthDate2ViewController"];
    birthDate2ViewController.birthDateDate = self.birthdate;
    [self.navigationController presentViewController:birthDate2ViewController animated:YES completion:nil];
    
    
    /*
    UIViewController *birthDate2ViewController = [onboardStoryboard instantiateViewControllerWithIdentifier:@"BirthDate3ViewController"];
    [self.navigationController presentViewController:birthDate2ViewController animated:YES completion:nil];
    */
    
}

- (void)setBirthdate:(NSDate *)birthdate {
    _birthdate = birthdate;
    [self.birthDateButton setImage:[UIImage imageNamed:@"kidPicBg"] forState:UIControlStateNormal];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"MMM"];
    self.monthLbl.text = [[formatter stringFromDate:birthdate] uppercaseString];
    [formatter setDateFormat:@"dd"];
    self.dayLbl.text = [formatter stringFromDate:birthdate];
    [formatter setDateFormat:@"yyyy"];
    self.yearLbl.text = [formatter stringFromDate:birthdate];
    [self checkEnableSaveButton];
}

- (IBAction)boyGirlTap:(id)sender {
    [self.nameTF resignFirstResponder];

    self.boyBtn.selected = NO;
    self.girlBtn.selected = NO;
    [(UIButton *) sender setSelected:YES];

    [self checkEnableSaveButton];
}

- (void)deleteTap:(id)sender {
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"delete kid", comment: @"")
                                                                message:NSLocalizedString(@"are you sure?", comment: @"")
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"no" style:UIAlertActionStyleCancel handler:nil]];
    [ac addAction:[UIAlertAction actionWithTitle:@"delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction* action) {
        [self deleteKid];
    }]];
    [ac presentWithAnimated:YES completion:nil];
}

- (void)deleteKid {
        [SVProgressHUD showWithStatus:KPLocalizedString(@"deleting")];
        [[ServerComm instance] deleteKid:self.kid.serverID withSuccessBlock:^(id result) {

            if ([[result valueForKey:@"status"] intValue] == 0) {
                [[Mixpanel sharedInstance] track:@"delete kid"];

                [[Analytics sharedAnalytics] track:@"item-delete" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"kid", @"itemType", nil]];

                NSArray *items = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"kids contains %@", self.kid]];

                for (Item *item in items) {
                    [item.image MR_deleteEntity];
                    [item.originalImage MR_deleteEntity];
                    [item.story MR_deleteEntity];

                    NSArray *comments = [Comment MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"item == %@", item]];
                    for (Comment *comment in comments) {
                        [comment MR_deleteEntity];
                    }

                    [item MR_deleteEntity];
                }

                if ([[UserManager sharedInstance] selectedKid] == self.kid) {
                    [[UserManager sharedInstance] setSelectedKid:nil];
                }

                NSUInteger kidId = self.kid.serverID;

                [self.kid remove];
                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {


                    [SVProgressHUD dismiss];

                    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_DELETED_NOTIFICATION object:@(kidId)];

                    [[NSNotificationCenter defaultCenter] postNotificationName:DELETE_KID_NOTIFICATION object:@(kidId)];

                    [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];

                    [GlobalUtils updateAnalyticsUser];

                    if (self.navigationController == nil) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    } else {
                        [self.navigationController popViewControllerAnimated:YES];
                    }

                }];

            }
            else {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error deleteing kid")];
            }

        }                   andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];

        }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self checkEnableSaveButton:[textField.text stringByReplacingCharactersInRange:range withString:string]];
    return YES;
}

- (void)checkEnableSaveButton {
    [self checkEnableSaveButton:nil];
}

- (void)checkEnableSaveButton:(NSString *)string {
    self.rightButtonEnabled = NO;

    if (string == nil) {
        string = self.nameTF.text;
    }

    if (string.length == 0) {
        return;
    }

    if (self.birthdate == nil) {
        return;
    }

    if (!self.boyBtn.selected && !self.girlBtn.selected) {
        return;
    }

    self.rightButtonEnabled = YES;

}

- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self.nameTF resignFirstResponder];
}

- (void)afterKidAddedToServer:(NSDictionary *)result {
    if ([[result valueForKey:@"status"] intValue] != 0 || [[[result valueForKey:@"result"] valueForKey:@"id"] isEqual:[NSNull null]]) {
        if (self.kid.serverID == 0) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error adding kid")];
        } else {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error updating kid")];
        }

        return;
    }

    self.kid.serverID = [[[result valueForKey:@"result"] valueForKey:@"id"] intValue];
    [self.kid saveChanges];
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

    if (self.photoChanged && self.kidImage != nil) {
        [SVProgressHUD showWithStatus:KPLocalizedString(@"uploading Photo")];
        [[ServerComm instance] uploadKidPhoto:self.kid.serverID withImage:self.kidImage withSuccessBlock:^(id result) {
            [SVProgressHUD dismiss];

            int rstatus = [[result valueForKey:@"status"] intValue];
            if (rstatus == 0) {
                KPLocalAsset *asset = [KPLocalAsset create];
                asset.serverID = [[result valueForKey:@"assetId"] intValue];
                asset.assetHash = [result valueForKey:@"hash"];
                self.kid.imageAssetID = asset.identifier;
                [asset saveChanges];
                [self.kid saveChanges];

                [[SDImageCache sharedImageCache] storeImage:self.kidImage forKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, asset.assetHash]];

                [self afterPhotoUploaded];
            }
            else {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"Error uploading photo")];
            }

        }                        andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"Error uploading photo")];
        }];
    }
    else {
        [self afterPhotoUploaded];
    }
}

- (void)afterPhotoUploaded {
    if (self.noteText != nil) {
        NSDate *noteDate = [NSDate date];
        [[ServerComm instance]
                addItem:@""
           selectedKids:[NSSet setWithArray:@[@(self.kid.serverID)]]
                andDate:noteDate
               andPlace:0 andCropRect:@"" andOriginalAssetId:self.noteAssetId andRotation:0 andSat:0 andCon:0
                 andBri:0
       andAutoEnhanceOn:false
            andItemType:1 andExtraData:self.noteText andShareWithFans:NO andTags:nil
         andExtraParams:nil
       withSuccessBlock:^(id result) {

           if ([[result valueForKey:@"status"] intValue] == 0) {
               //Save local files with assets ids
               
               NSString* assetsURLString = Defaults.assetsURLString;

               [[SDImageCache sharedImageCache] storeImage:self.noteImage forKey:[NSString stringWithFormat:@"%@%@.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

               [[SDImageCache sharedImageCache] storeImage:self.noteImage forKey:[NSString stringWithFormat:@"%@%@_iphone.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];


               //save thumbnails
               UIImage *smallImage = [self.noteImage imageScaledToFitSize:CGSizeMake(192, 192)];

               [[SDImageCache sharedImageCache] storeImage:smallImage forKey:[NSString stringWithFormat:@"%@%@_small.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

               UIImage *mediumImage = [self.noteImage imageScaledToFitSize:CGSizeMake(600, 600)];
               [[SDImageCache sharedImageCache] storeImage:mediumImage forKey:[NSString stringWithFormat:@"%@%@_medium.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

               //Add new Item to local database
               NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
               [itemData setValue:noteDate forKey:@"itemDate"];
               [itemData setValue:@"" forKey:@"cropRect"];
               [itemData setValue:[NSString stringWithFormat:@"%lu", (unsigned long)self.kid.serverID] forKey:@"kidsIds"];
               [itemData setValue:@"" forKey:@"title"];
               [itemData setValue:@(0) forKey:@"sat"];
               [itemData setValue:@(0) forKey:@"con"];
               [itemData setValue:@(0) forKey:@"rotation"];
               [itemData setValue:@(1) forKey:@"itemType"];
               [itemData setValue:self.noteText forKey:@"extraData"];

               KPLocalItem *item = [[KPDataParser instance] addNewItem:[result valueForKey:@"result"] andData:itemData];
               [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

               [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:item];

           }
           [self updateFinished];

       } andFailBlock:^(NSError *error) {
                    //TODO:...
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"Error uploading note")];
                }];
    }
    else {
        [self updateFinished];
    }
}

- (void)updateFinished {
    [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];

    [SVProgressHUD dismiss];

    //if ([[[UserManager sharedInstance] myKids] count] == 1)
    //    [TapjoyConnect actionComplete:@"8765eb11-70ce-4948-aca3-5b2f9a7e5d4d"];

    if (self.delegate != nil) {
        [self.delegate kidAdded:self.kid];
    }
    
    [[Analytics sharedAnalytics] track:(self.isNew) ? @"item-add" : @"item-update" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"kid", @"itemType", nil]];

    [GlobalUtils updateAnalyticsUser];

    if (self.noteAssetId > 0) //came from adding kid and note view still there...
    {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-kid", @"source", @"ok", @"result", nil]];

        if (self.navigationController != nil) {
            NSUInteger c = [self.navigationController.viewControllers count];

            if (c < 3) {
                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:c - 3] animated:YES];
            }
        }
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }

        return;
    }

    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-kid", @"source", @"ok", @"result", nil]];

    if (self.navigationController != nil) {
        if (self.isNew) {
            
                UIStoryboard *onboardStoryboard = [UIStoryboard storyboardWithName:@"Onboard" bundle:nil];
                ChooseMemoryViewController *chooseMemoryViewController = (ChooseMemoryViewController *) [onboardStoryboard instantiateViewControllerWithIdentifier:@"ChooseMemoryViewController"];
                UINavigationController *ncController = self.navigationController;
                //[ncController popViewControllerAnimated:NO];
                [ncController pushViewController:chooseMemoryViewController animated:YES];
           
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }

}

#pragma mark - Keyboard hide/show

- (void)keyboardDidShow:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(44.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    CGPoint point = self.nameTF.frame.origin;
    point = [self.scrollView convertPoint:point fromView:(UIView *) [self.scrollView.subviews objectAtIndex:0]];
    CGRect aRect = CGRectMake(point.x, point.y + 40, CGRectGetWidth(self.nameTF.frame), CGRectGetHeight(self.nameTF.frame));
    [self.scrollView scrollRectToVisible:aRect animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(44.0, 0.0, 0.0, 0.0);
    self.scrollView.contentInset = contentInsets;
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 10, 10) animated:YES];
    /*
     UIEdgeInsets contentInsets = UIEdgeInsetsZero;
     self.scrollView.contentInset = contentInsets;
     self.scrollView.scrollIndicatorInsets = contentInsets;
     */
}

@end
