//
//  ContactsSelectorViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 6/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <CoreText/CoreText.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#import "ContactsSelectorViewController.h"
#import <AddressBook/AddressBook.h>
#import "EditFanViewController.h"

@interface ContactsSelectorViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property(nonatomic) NSInteger mode;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *friendsList;
@property(nonatomic, strong) NSMutableArray *originalFriendsList;
@property(nonatomic, strong) NSArray *orgArr;
@property(nonatomic, strong) UITextField *txtField;
@property(nonatomic) BOOL isSpreadTheLove;

@property(nonatomic) NSInteger relationMode;

@end

@implementation ContactsSelectorViewController

- (instancetype)initWithMode:(NSInteger)mode {
    self = [super initWithNibName:@"ContactsSelectorViewController" bundle:nil];
    if (self) {
        self.isSpreadTheLove = (mode > 9);
        if (mode > 9) {
            mode -= 10;
        }

        self.mode = mode;
    }
    return self;
}

- (instancetype)initWithMode:(NSInteger)mode andRelationMode:(NSInteger)relationMode {
    self = [super initWithNibName:@"ContactsSelectorViewController" bundle:nil];
    if (self) {
        self.isSpreadTheLove = (mode > 9);
        if (mode > 9) {
            mode -= 10;
        }

        self.mode = mode;
        self.relationMode = relationMode;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];

    self.title = KPLocalizedString(@"contacts");
    if (self.isSpreadTheLove) {
        self.title = KPLocalizedString(@"     contacts");
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"spread the love"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"next"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil]];
    }
    else {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add fan"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:nil];
    }

    

    if (self.mode == 1) {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"contacts-list", @"source", nil]];

        CFErrorRef error = nil;
        ABAddressBookRef m_addressbook = ABAddressBookCreateWithOptions(NULL, &error);
        ABAddressBookRequestAccessWithCompletion(m_addressbook, ^(bool granted, CFErrorRef error) {
            if (!granted) {
                //DDLogInfo(@"permission denied");

                double delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"permission needed", comment: @"")
                                                                                message:NSLocalizedString(@"you need to grant keepy access to your address book on your device settings", comment: @"")
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                    [ac addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:ac animated:YES completion:nil];
                    /*
                    NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:@(-1), @"id", KPLocalizedString(@"you need to grant keepy access to your address book on your device settings"), @"body", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"ok"), @"title", @"", @"link", nil], nil], @"buttons", @"no-contacts", @"name", nil];
                    [[GlobalUtils sharedInstance] showKeepyPopup:d];
                    */

                });

                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });

                return;
            }
            else {
                [self loadFromAddressbook:m_addressbook];
            }
        });
    }
    else {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"facebook-list", @"source", nil]];

        self.title = @"facebook";
        if (self.isSpreadTheLove) {
            self.title = @"      facebook";
        }

        NSMutableDictionary *existingFBIds = [[NSMutableDictionary alloc] init];
        if (!self.isSpreadTheLove) {
            for (Fan *fan in [[UserManager sharedInstance] myFans]) {
                if (fan.facebookId != nil) {
                    [existingFBIds setObject:fan forKey:fan.facebookId];
                }
            }
        }

        NSMutableArray *arr = [[NSMutableArray alloc] init];
        [[UserManager sharedInstance] requireFacebookFromViewController:self
                                                                success:^(id result) {
                                                                    if (result != nil && [result objectForKey:@"significant_other"] != nil) {
                                                                        NSString *s = @"husband";
                                                                        if ([[UserManager sharedInstance] getMe].parentType.intValue == 1) {
                                                                            s = @"wife";
                                                                        }
                                                                        
                                                                        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
                                                                        [d addEntriesFromDictionary:[result objectForKey:@"significant_other"]];
                                                                        [d setObject:s forKey:@"relationship"];
                                                                        
                                                                        Fan *fan = [existingFBIds objectForKey:[[result objectForKey:@"significant_other"] objectForKey:@"id"]];
                                                                        if (fan != nil) {
                                                                            [d setObject:fan forKey:@"fan"];
                                                                        }
                                                                        
                                                                        [arr addObject:d];
                                                                    }
                                                                    [[UserManager sharedInstance] getFBFamily:^(id result2) {
                                                                        [arr addObjectsFromArray:[result2 objectForKey:@"data"]];
                                                                        [[UserManager sharedInstance] getFBFriends:^(id result3) {
                                                                            [arr addObjectsFromArray:[result3 objectForKey:@"data"]];
                                                                            
                                                                            for (NSInteger i = [arr count] - 1; i > -1; i--) {
                                                                                NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithDictionary:[arr objectAtIndex:i]];
                                                                                Fan *fan = [existingFBIds objectForKey:[d objectForKey:@"id"]];
                                                                                if (fan != nil) {
                                                                                    [d setObject:fan forKey:@"fan"];
                                                                                    [arr replaceObjectAtIndex:i withObject:d];
                                                                                }
                                                                            }
                                                                            [self arrangeFriendsLists:arr];
                                                                        }];
                                                                    }];
                                                                }
                                                           failure:^(id result) {
                                                               [SVProgressHUD dismiss];
                                                           }
         ];
    }

}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.allowsMultipleSelection = self.isSpreadTheLove;
    self.tableView.scrollsToTop = YES;
    self.tableView.backgroundView = nil;
    
    self.tableView.separatorColor = UIColorFromRGB(0xe0e0e0);
    [self.view addSubview:self.tableView];
    
    [self addSearchHeader];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadFromAddressbook:(ABAddressBookRef)m_addressbook {

    NSMutableDictionary *existingEmails = [[NSMutableDictionary alloc] init];
    if (!self.isSpreadTheLove) {
        for (Fan *fan in [[UserManager sharedInstance] myFans]) {
            if (fan.email != nil) {
                [existingEmails setObject:fan forKey:fan.email];
            }
        }
    }
    NSMutableArray *contactsArray = [NSMutableArray new];
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(m_addressbook);
    CFIndex nPeople = ABAddressBookGetPersonCount(m_addressbook);
    for (int i = 0; i < nPeople; i++) {
        NSMutableDictionary *tempContactDic = [NSMutableDictionary new];
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople, i);
        CFStringRef firstName, lastName;
        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        lastName = ABRecordCopyValue(ref, kABPersonLastNameProperty);

        NSString *strEmail;
        ABMultiValueRef email = ABRecordCopyValue(ref, kABPersonEmailProperty);
        CFStringRef tempEmailref = ABMultiValueCopyValueAtIndex(email, 0);
        strEmail = (__bridge NSString *) tempEmailref;

        if (strEmail && (strEmail.length > 0 || !self.isSpreadTheLove) && ([strEmail isEqualToString:@""] || [tempContactDic objectForKey:strEmail] == nil) && (firstName != NULL || lastName != NULL)) {
            [tempContactDic setObject:strEmail forKey:@"email"];
            
            UIImage *pImage = nil;
            CFDataRef imageData = ABPersonCopyImageData(ref);
            if (imageData) {
                pImage = [UIImage imageWithData:(__bridge NSData *) imageData];
                CFRelease(imageData);
            }

            if (firstName == NULL) {
                firstName = (CFStringRef) @"";
            }

            if (lastName == NULL) {
                lastName = (CFStringRef) @"";
            }

            NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
            [d setObject:strEmail forKey:@"email"];
            [d setObject:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"name"];
            if (pImage != nil) {
                [d setObject:pImage forKey:@"image"];
            }

            if ([existingEmails objectForKey:strEmail] != nil) {
                [d setObject:[existingEmails objectForKey:strEmail] forKey:@"fan"];
            }

            [contactsArray addObject:d];
        }
    }

    [self arrangeFriendsLists:contactsArray];

    CFRelease(m_addressbook);
}

- (void)topBtnTap:(UIButton *)sender {
    if (self.mode == 1) {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"contacts-list", @"source", @"cancel", @"result", nil]];
    } else {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"facebook-list", @"source", @"cancel", @"result", nil]];
    }


    if (sender.tag == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (int i = 0; i < [[self.tableView indexPathsForSelectedRows] count]; i++) {
            NSIndexPath *indexPath = [[self.tableView indexPathsForSelectedRows] objectAtIndex:i];

            NSMutableArray *sarr = nil;
            if ((self.friendsList != nil) && ([self.friendsList count] > indexPath.section)) {
                sarr = (NSMutableArray *) [self.friendsList objectAtIndex:indexPath.section];
            }

            NSDictionary *userDict = (NSDictionary *) [sarr objectAtIndex:indexPath.row];
            if (self.mode == 1) {
                [arr addObject:[userDict objectForKey:@"email"]];
            } else {
                [arr addObject:[userDict objectForKey:@"fbId"]];
            }
        }

        if (self.mode == 1) {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"contacts-list", @"source", @"ok", @"result", nil]];
            [GlobalUtils openSpreadLoveEmail:arr];
        }
        else {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"facebook-list", @"source", @"ok", @"result", nil]];
            [[UserManager sharedInstance] appInviteFromViewController:self];
        }
    }
}

static NSInteger compareFriends(id left, id right, void *context)
//- (NSComparisonResult) compareFriends:(NSDictionary *)dict2
{
    NSDictionary *dict1 = (NSDictionary *) left;
    NSDictionary *dict2 = (NSDictionary *) right;

    if ([dict1 objectForKey:@"relationship"] != nil && [dict2 objectForKey:@"relationship"] == nil) {
        return NSOrderedAscending;
    } else if ([dict2 objectForKey:@"relationship"] != nil && [dict1 objectForKey:@"relationship"] == nil) {
        return NSOrderedDescending;
    }

    NSString *name1 = @"";
    if ([dict1 objectForKey:@"name"] != nil && [dict1 objectForKey:@"name"] != [NSNull null]) {
        name1 = [(NSString *) [dict1 objectForKey:@"name"] lowercaseString];
    }

    NSString *cmd;
    NSScanner *scanner = [NSScanner scannerWithString:name1];
    [scanner scanUpToString:@" " intoString:&cmd];
    name1 = [[scanner string] substringToIndex:[scanner scanLocation]];

    NSString *name2 = @"";
    if ([dict2 objectForKey:@"name"] != nil && [dict2 objectForKey:@"name"] != [NSNull null]) {
        name2 = [(NSString *) [dict2 objectForKey:@"name"] lowercaseString];
    }

    scanner = [NSScanner scannerWithString:name2];
    [scanner scanUpToString:@" " intoString:&cmd];
    name2 = [[scanner string] substringToIndex:[scanner scanLocation]];

    return [name1 compare:name2];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.friendsList == nil) {
        return 0;
    }
    else {
        return [self.friendsList count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger c = 0;

    if (self.friendsList != nil) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:section];
        if (sarr != nil) {
            c = [sarr count];
        }
    }

    return c;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *cellIdentifier = @"userCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.frame = CGRectMake(0.0, 0.0, 320, 44);

        if (!self.isSpreadTheLove) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;

            UIView *aview = [[UIView alloc] initWithFrame:cell.frame];
            aview.backgroundColor = [UIColor clearColor];//UIColorFromRGB(0xe2dcda);

            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(275, (44 - 15) / 2, 13, 15)];
            imgView.image = [UIImage imageNamed:@"v"];
            [aview addSubview:imgView];

            cell.selectedBackgroundView = aview;

        }

        cell.backgroundColor = [UIColor whiteColor];

        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
        [cell.contentView addSubview:imgView];

        float dx = 40;
        if (self.mode == 1) {
            dx = 7;
        }

        float w = 270;
        if (self.isSpreadTheLove) {
            w = 267 - dx;
        }

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(dx, 0, w, 44)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.opaque = NO;
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
        lbl.tag = 2;
        lbl.textColor = UIColorFromRGB(0x523c37);
        [cell.contentView addSubview:lbl];
    }

    NSMutableArray *sarr = nil;
    if ((self.friendsList != nil) && ([self.friendsList count] > indexPath.section)) {
        sarr = (NSMutableArray *) [self.friendsList objectAtIndex:indexPath.section];
    }

    if (sarr == nil) {
        return cell;
    }

    cell.tag = indexPath.row;
    NSDictionary *userDict = (NSDictionary *) [sarr objectAtIndex:indexPath.row];


    __weak UIImageView *imgView = (UIImageView *) [cell.contentView.subviews objectAtIndex:0];
    if ([userDict objectForKey:@"fbId"] != nil) {
        NSString *imageUrlStr = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", [userDict objectForKey:@"fbId"]];
        [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]];
        [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

            UIImage *img = [UIImage imageWithCGImage:[image imageScaledToFitSize:CGSizeMake(60, 60)].CGImage];
            imgView.image = [img imageMaskedWithElipse:CGSizeMake(60, 60)];

        }];

    }
        /*
        else if ([userDict objectForKey:@"image"] != nil)
        {
            //imgView.image = (UIImage*)[userDict objectForKey:@"image"];
            UIImage *img = [UIImage imageWithCGImage:[(UIImage*)[userDict objectForKey:@"image"] imageScaledToFitSize:CGSizeMake(60, 60)].CGImage];
            imgView.image = [img imageMaskedWithElipse:CGSizeMake(60, 60)];
        }*/
    else {
        imgView.image = nil;
    }

    NSString *s = @"";
    if ([userDict objectForKey:@"fan"] != nil) {
        s = KPLocalizedString(@" (invited)");

        /*Fan *fan = [userDict objectForKey:@"fan"];
        
        if (fan.didApproveValue)
            s = KPLocalizedString(@" (accepted)");
        else
            s = KPLocalizedString(@" (pending)");
         */
    }
    UILabel *lbl = (UILabel *) [cell.contentView viewWithTag:2];
    
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", [userDict objectForKey:@"name"], s]];
    
    NSRange range1 = [[mutableAttributedString string] rangeOfString:@" " options:NSCaseInsensitiveSearch];
    range1.length = mutableAttributedString.length - range1.location;

    UIFont *afont = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef) afont.fontName, afont.pointSize, NULL);
    if (font) {
        if (range1.length > 0) {
            [mutableAttributedString addAttribute:(NSString *) kCTFontAttributeName value:(__bridge id) font range:range1];
        }
        CFRelease(font);
    }
    lbl.attributedText = mutableAttributedString;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (self.friendsList != nil) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:section];
        if (sarr != nil) {
            NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:0];
            if ([d objectForKey:@"relationship"] != nil) {
                return KPLocalizedString(@"family");
            }
            else {
                NSString *name = [d objectForKey:@"name"];
                return [[name substringToIndex:1] uppercaseString];
            }
        }
    }

    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22.5)];
    aview.backgroundColor = UIColorFromRGB(0x897e7a);
    aview.alpha = 0.8;

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 320, 22.5)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:15];
    [aview addSubview:lbl];

    if (self.friendsList != nil) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:section];
        if (sarr != nil) {
            NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:0];
            if ([d objectForKey:@"relationship"] != nil) {
                lbl.text = KPLocalizedString(@"family");
            }
            else {
                NSString *name = [d objectForKey:@"name"];
                lbl.text = [[name substringToIndex:1] uppercaseString];
            }
        }
    }


    return aview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22.5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.txtField resignFirstResponder];
    NSMutableArray *sarr = nil;
    if ((self.friendsList != nil) && ([self.friendsList count] > indexPath.section)) {
        sarr = (NSMutableArray *) [self.friendsList objectAtIndex:indexPath.section];
    }

    if (sarr == nil) {
        return;
    }

    if (self.isSpreadTheLove) {
        return;
    }

    if (self.mode == 1) {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"contacts-list", @"source", @"ok", @"result", nil]];
    } else {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"facebook-list", @"source", @"ok", @"result", nil]];
    }

    if ([sarr count] > indexPath.row) {
        NSDictionary *userDict = (NSDictionary *) [sarr objectAtIndex:indexPath.row];

        EditFanViewController *avc = nil;
        if ([userDict objectForKey:@"fan"] == nil) {
            avc = [[EditFanViewController alloc] initWithUserData:userDict andRelationMode:self.relationMode];
        } else {
            avc = [[EditFanViewController alloc] initWithFan:[userDict objectForKey:@"fan"]];
        }

        [self.navigationController pushViewController:avc animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i = 0; i < [self.friendsList count]; i++) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:i];
        NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:0];
        if ([d objectForKey:@"relationship"] != nil) {
            [arr addObject:@"*"];
        }
        else {
            NSString *name = [d objectForKey:@"name"];
            NSString *s = [[name substringToIndex:1] uppercaseString];
            [arr addObject:s];
        }
    }

    return arr;
}


- (void)arrangeFriendsLists:(NSArray *)list; {
    NSString *lastChar = @"";
    NSMutableArray *sarr = nil;

    NSMutableDictionary *existingIds = [[NSMutableDictionary alloc] init];

    self.orgArr = list;

    if (self.originalFriendsList == nil) {
        self.originalFriendsList = [[NSMutableArray alloc] init];
    }

    [self.originalFriendsList removeAllObjects];

    if (self.friendsList == nil) {
        self.friendsList = [[NSMutableArray alloc] init];
    }

    [self.friendsList removeAllObjects];

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObjectsFromArray:list];
    NSMutableArray *discardedItems = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in arr) {
        if ([dict objectForKey:@"name"] == [NSNull null]) {
            [discardedItems addObject:dict];
        }
    }
    [arr removeObjectsInArray:discardedItems];

    [arr sortUsingFunction:compareFriends context:nil];

    for (int i = 0; i < [arr count]; i++) {
        NSDictionary *item = [arr objectAtIndex:i];

        NSString *aid = @"";
        if ([item objectForKey:@"id"] != nil) {
            aid = [item objectForKey:@"id"];

        } else if ([item objectForKey:@"email"] != nil) {
            aid = [item objectForKey:@"email"];
        }

        if ([existingIds objectForKey:aid] != nil && ![aid isEqualToString:@""]) {
            continue;
        }
        [existingIds setObject:@(1) forKey:aid];

        NSString *aname = [item objectForKey:@"name"];
        NSString *s = @"";
        if ([item objectForKey:@"relationship"] != nil) {
            s = @"*";
        } else if ([aname length] > 0) {
            s = [[aname substringToIndex:1] lowercaseString];
        }

        if (![s isEqualToString:lastChar]) {
            if (sarr != nil) {
                [self.originalFriendsList addObject:sarr];
            }

            sarr = [[NSMutableArray alloc] init];
            lastChar = s;
        }

        if (sarr != nil) {
            NSMutableDictionary *newItem = [[NSMutableDictionary alloc] init];
            if ([item objectForKey:@"id"] != nil) {
                [newItem setObject:[item objectForKey:@"id"] forKey:@"fbId"];
            }

            if ([item objectForKey:@"email"] != nil) {
                [newItem setObject:[item objectForKey:@"email"] forKey:@"email"];
            }

            [newItem setObject:[item objectForKey:@"name"] forKey:@"name"];
            if ([item objectForKey:@"relationship"]) {
                [newItem setObject:[item objectForKey:@"relationship"] forKey:@"relationship"];
            }

            if ([item objectForKey:@"image"]) {
                [newItem setObject:[item objectForKey:@"image"] forKey:@"image"];
            }

            if ([item objectForKey:@"fan"] != nil) {
                [newItem setObject:[item objectForKey:@"fan"] forKey:@"fan"];
            }

            [sarr addObject:newItem];

            //DDLogInfo(@"%@", newItem);
        }
    }
    if (sarr != nil) {
        [self.originalFriendsList addObject:sarr];
    }


    [self.friendsList addObjectsFromArray:self.originalFriendsList];

    dispatch_async(dispatch_get_main_queue(), ^{

        [SVProgressHUD dismiss];
        [self.tableView reloadData];
    });
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return YES;
    }

    [self.friendsList removeAllObjects];
    NSString *searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([searchText isEqualToString:@""]) {
        [self.originalFriendsList removeAllObjects];
        [self arrangeFriendsLists:self.orgArr];
        return YES;
    }

    NSMutableArray *newsarr;
    for (int i = 0; i < [self.originalFriendsList count]; i++) {
        newsarr = nil;
        NSMutableArray *sarr = (NSMutableArray *) [self.originalFriendsList objectAtIndex:i];
        for (int k = 0; k < [sarr count]; k++) {
            NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:k];
            NSString *name = [d objectForKey:@"name"];

            NSRange aRange = [[name lowercaseString] rangeOfString:[searchText lowercaseString]];
            if (aRange.location != NSNotFound) {
                if (newsarr == nil) {
                    newsarr = [[NSMutableArray alloc] init];
                    [self.friendsList addObject:newsarr];
                }
                [newsarr addObject:d];
            }
        }
    }

    [self.tableView reloadData];
    return YES;
}

- (void)addSearchHeader {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -45, self.view.frame.size.width, 45)];
    view.backgroundColor = UIColorFromRGB(0xe2dcda);

    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(7, 7, self.view.frame.size.width - 14, 30)];
    img.image = [[UIImage imageNamed:@"search-field"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 28, 0, 54 - 28)];
    [view addSubview:img];

    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(40, 12.5, self.view.frame.size.width - 80, 17)];
    tf.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
    tf.textColor = [UIColor blackColor];
    tf.placeholder = KPLocalizedString(@"search");
    tf.delegate = self;
    [view addSubview:tf];
    self.txtField = tf;

    self.tableView.tableHeaderView = nil;
    self.tableView.tableHeaderView = view;

    self.tableView.alwaysBounceVertical = YES;
}

#if TO_BE_DELETED
- (void)sendFBSpreadTheLove:(NSArray *)ids {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[ids componentsJoinedByString:@","], @"to", nil];

    //[FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
    [[UserManager sharedInstance] requireFacebook:^(id result) {
        // Display the requests dialog
        [FBWebDialogs
                presentRequestsDialogModallyWithSession:[FBSession activeSession]
                                                message:KPLocalizedString(@"Hey!\n\nI\'m using the keepy app to organize, enhance, share, admire and save my kids\' art, schoolwork, certificates, lego creations, mementos and MORE!\n\nYou should check it out!  I really think you'll dig it!")
                                                  title:KPLocalizedString(@"join keepy")
                                             parameters:params
                                                handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                    if (error) {
                                                        // Error launching the dialog or sending request.
                                                        DDLogInfo(@"Error sending request.");
                                                    } else {
                                                        if (result == FBWebDialogResultDialogNotCompleted) {
                                                            // User clicked the "x" icon
                                                            DDLogInfo(@"User canceled request.");
                                                        } else {
                                                            // Handle the send request callback
                                                            NSDictionary *urlParams = [GlobalUtils parseURLParams:[resultURL query]];
                                                            if (![urlParams objectForKey:@"request"]) {
                                                                // User clicked the Cancel button
                                                                DDLogInfo(@"User canceled request.");
                                                            } else {
                                                                // User clicked the Send button
                                                                NSString *requestID = [urlParams objectForKey:@"request"];
                                                                DDLogInfo(@"Request ID: %@", requestID);

                                                                [[[Mixpanel sharedInstance] people] increment:@"spread-the-love-invite-counter" by:@([ids count])];


                                                                int64_t delayInSeconds = 1.0;
                                                                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                                                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                                                                    [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
                                                                });

                                                                [[ServerComm instance] addFacebookInviteRequest:requestID withFBIds:[ids componentsJoinedByString:@","] andSuccessBlock:^(id result) {

                                                                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                                                }                                  andFailBlock:^(NSError *error) {
                                                                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

                                                                }];
                                                            }
                                                        }
                                                    }
                                                }];
    }];
}
#endif

@end
