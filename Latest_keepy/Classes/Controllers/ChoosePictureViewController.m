//
//  ChoosePictureViewController.m
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@import Photos;

#import "ChoosePictureViewController.h"
#import "PictureSelector2ViewController.h"
#import "AlbumSelector2ViewController.h"
#import "CircleCropViewController.h"
#import "KPPhotoLibrary.h"

@interface ChoosePictureViewController () <PictureSelector2ViewControllerDelegate, AlbumSelector2ViewControllerDelegate, CircleCropViewControllerDelegate>

@property(nonatomic, strong) NSMutableDictionary *viewControllersByIdentifier;
@property(nonatomic, strong) NSArray <PHAsset*> *pictures;
@property(nonatomic, strong) NSArray *albums;

@end

@implementation ChoosePictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewControllersByIdentifier = [NSMutableDictionary dictionary];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SVProgressHUDDidDisappear:) name:SVProgressHUDDidDisappearNotification object:nil];
    
    
    if ([KPPhotoLibrary authorizationIsDenied]) {
        return;
    }
    
    __weak ChoosePictureViewController *weakSelf = self;
    NSMutableArray *assets = [[NSMutableArray alloc] init];
    
    PHFetchOptions* options = [PHFetchOptions new];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult* images = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
    
    [images enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
        if (images.count == 0) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"no photos found")];
            
        }
        [assets insertObject:asset atIndex:0];
        
        if (idx + 1 == images.count) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.pictures = assets;
                [weakSelf performSegueWithIdentifier:@"PictureSelector2" sender:self];
                *stop = YES;
            });
        }
    }];
    
//    PHFetchOptions* options = [PHFetchOptions new];
//    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
//    PHFetchResult* images = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
//    
//    if (images.count == 0) {
//        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"no photos found")];
//        return;
//    }
//    
//    NSMutableArray* arr = [NSMutableArray array];
//    
//    [images enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
//        [arr addObject:asset];
//        
//        if (images.count-1 == idx) {
//            weakSelf.pictures = arr;
//            [weakSelf performSegueWithIdentifier:@"PictureSelector2" sender:self];
//        }
//    }];
#if 0
    
    [[GlobalUtils defaultAssetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            if (group.numberOfAssets > 0) {
                NSMutableArray *pictures = [[NSMutableArray alloc] initWithCapacity:1024];
                [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                    if (result) {
                        [pictures insertObject:result atIndex:0];
                    }
                }];
                //[pictures insertObject:[NSNull null] atIndex:0];
                weakSelf.pictures = pictures;
                *stop = YES;
                [weakSelf performSegueWithIdentifier:@"PictureSelector2" sender:weakSelf];
            } else {
                *stop = YES;
                // @TODO: NO PICTURES!
            }
        }
    }                                               failureBlock:^(NSError *error) {
        if (error.code == ALAssetsLibraryAccessUserDeniedError) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        }
    }];
#endif
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([KPPhotoLibrary authorizationIsDenied]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        return;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

//will close this viewController when SVProgress disappear
- (void)SVProgressHUDDidDisappear:(NSNotification *)notification {
    
    NSString *infoKey = notification.userInfo[SVProgressHUDStatusUserInfoKey];
    
    if([infoKey isEqualToString:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")]) {
         [self dismissViewControllerAnimated:YES completion:nil];
    }
   
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue isKindOfClass:[ChoosePictureSegue class]]) {
        self.previousViewController = self.nextViewController;
        if (![self.viewControllersByIdentifier objectForKey:segue.identifier]) {
            [self.viewControllersByIdentifier setObject:segue.destinationViewController forKey:segue.identifier];
        }
        
        self.nextIdentifier = segue.identifier;
        self.nextViewController = [self.viewControllersByIdentifier objectForKey:self.nextIdentifier];
        if ([self.nextIdentifier isEqualToString:@"PictureSelector2"]) {
            PictureSelector2ViewController *pictureSelector2ViewController = (PictureSelector2ViewController *) self.nextViewController;
            pictureSelector2ViewController.delegate = self;
            pictureSelector2ViewController.pictures = self.pictures;
            [(ChoosePictureSegue *) segue setSubtype:kCATransitionFromRight];
        } else if ([self.nextIdentifier isEqualToString:@"AlbumSelector2"]) {
            AlbumSelector2ViewController *albumSelector2ViewController = (AlbumSelector2ViewController *) self.nextViewController;
            albumSelector2ViewController.delegate = self;
            albumSelector2ViewController.albums = self.albums;
            [(ChoosePictureSegue *) segue setSubtype:kCATransitionFromLeft];
        }
    } else {
        
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([self.nextIdentifier isEqualToString:identifier]) {
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [self.viewControllersByIdentifier.allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
        if (![self.nextIdentifier isEqualToString:key]) {
            [self.viewControllersByIdentifier removeObjectForKey:key];
        }
    }];
}

- (void)albums:(id)sender {
    if ([KPPhotoLibrary authorizationIsDenied]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        return;
    }
    
    NSMutableArray *albums = [[NSMutableArray array] init];
    __weak ChoosePictureViewController *weakSelf = self;
    
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    
    [smartAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        
        PHFetchResult *result = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
        NSLog(@"album title: %@ (%ld)", collection.localizedTitle, (long)result.count);
        if (result.count > 0) {
            [albums addObject:collection];
        }
        
        if (idx+1 == smartAlbums.count) {
            self.albums = albums;
            [self performSegueWithIdentifier:@"AlbumSelector2" sender:weakSelf];
        }
    }];
#if 0
    
    [[GlobalUtils defaultAssetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [albums insertObject:group atIndex:0];
        } else {
            weakSelf.albums = albums;
            [weakSelf performSegueWithIdentifier:@"AlbumSelector2" sender:weakSelf];
            *stop = YES;
        }
    }                                               failureBlock:^(NSError *error) {
        // Do nothing
    }];
#endif
}

- (void)pictureSelected:(PHAsset*)asset {
    if (!asset) {
        NSLog(@"WTF");
        return;
    }
    
    PHImageManager* im = [PHImageManager defaultManager];
    PHImageRequestOptions* options = [PHImageRequestOptions new];
    options.synchronous = YES;
    options.networkAccessAllowed = YES;
    options.version = PHImageRequestOptionsVersionCurrent;
    
    [im requestImageDataForAsset:asset options:options resultHandler:^(NSData * imageData, NSString * dataUTI, UIImageOrientation orientation, NSDictionary * info) {
        UIImage* image = [UIImage imageWithData:imageData];
        [self goToCircleCrop:[image fixOrientation]
                       scale:1.0
                       point:CGPointMake(0, 0)];
    }];
}

#pragma mark - CircleCropViewControllerDelegate

- (void)doneCircleCropping:(NSDictionary *)selectedPhotosDict {
    [self.delegate doneChoosingPicture:selectedPhotosDict];
}

- (void)cancelCircleCropping {
    NSAssert(NO, @"cancelCircleCropping");
}

#pragma mark -

- (void)startWithPhoto:(UIImage *)image scale:(float)scale point:(CGPoint)point {
    [self goToCircleCrop:image scale:scale point:point];
}

- (void)goToCircleCrop:(UIImage *)image scale:(float)scale point:(CGPoint)point {
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setSubtype:kCATransitionFromBottom];
    [animation setDuration:0.3f];
    [self.navigationController.view.layer addAnimation:animation forKey:kCATransition];
    UIStoryboard *choosePictureStoryboard = [UIStoryboard storyboardWithName:@"ChoosePicture" bundle:nil];
    CircleCropViewController *circleCropViewController = (CircleCropViewController *) [choosePictureStoryboard instantiateViewControllerWithIdentifier:@"CircleCropViewController"];
    circleCropViewController.delegate = self;
    circleCropViewController.image = image;
    circleCropViewController.scale = scale;
    circleCropViewController.originPoint = point;
    [self.navigationController pushViewController:circleCropViewController animated:NO];
    [self.navigationController.view.layer removeAnimationForKey:@"pushIn"];
}

- (void)pictures:(PHAssetCollection *)collection {
    __weak ChoosePictureViewController *weakSelf = self;
    NSMutableArray *pictures = [[NSMutableArray alloc] initWithCapacity:1024];
    
    PHFetchOptions* options = [PHFetchOptions new];
    options.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES] ];
    PHFetchResult<PHAsset *> * result = [PHAsset fetchAssetsInAssetCollection:collection options:options];
    
    [result enumerateObjectsUsingBlock:^(PHAsset* obj, NSUInteger idx, BOOL *stop) {
        if (obj) {
            [pictures insertObject:obj atIndex:0];
        }
        
        if (idx+1 == result.count) {
            weakSelf.pictures = pictures;
            [weakSelf performSegueWithIdentifier:@"PictureSelector2" sender:weakSelf];
            *stop = YES;
        }
    }];
}

- (IBAction)cancel:(id)sender {
    [self.delegate doneChoosingPicture:nil];
}

@end

@implementation ChoosePictureSegue

- (void)perform {
    ChoosePictureViewController *choosePictureViewController = (ChoosePictureViewController *) self.sourceViewController;
    UIViewController *nextViewController = (UIViewController *) choosePictureViewController.nextViewController;
    if (choosePictureViewController.previousViewController) {
        CATransition *animation = [CATransition animation];
        [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:self.subtype];
        [animation setDuration:0.3f];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[choosePictureViewController.containerView layer] addAnimation:animation forKey:@"pushIn"];
        
        [choosePictureViewController.previousViewController willMoveToParentViewController:nil];
        [choosePictureViewController.previousViewController.view removeFromSuperview];
        [choosePictureViewController.previousViewController removeFromParentViewController];
        
        nextViewController.view.frame = choosePictureViewController.containerView.bounds;
        [choosePictureViewController addChildViewController:nextViewController];
        [choosePictureViewController.containerView addSubview:nextViewController.view];
        [choosePictureViewController didMoveToParentViewController:choosePictureViewController];
        
        [[choosePictureViewController.containerView layer] removeAnimationForKey:@"pushIn"];
    } else {
        nextViewController.view.frame = choosePictureViewController.containerView.bounds;
        [choosePictureViewController addChildViewController:nextViewController];
        [choosePictureViewController.containerView addSubview:nextViewController.view];
        [choosePictureViewController didMoveToParentViewController:choosePictureViewController];
    }
}

@end
