//
//  PlaceSelectViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/25/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "PlaceSelectViewController.h"

@interface PlaceSelectViewController ()

@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end

@implementation PlaceSelectViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData {
    for (UIView *aview in self.scrollView.subviews) {
        [aview removeFromSuperview];
    }

    float ax = 5;
    float ay = 5;
    NSArray *places = [[UserManager sharedInstance] myPlaces];
    for (int i = 0; i < [places count]; i++) {
        Place *place = (Place *) [places objectAtIndex:i];
        UIView *placeView = [self createPlaceView:place];
        placeView.tag = i;
        placeView.frame = CGRectMake(ax, ay, 50, 50);

        ax += 55;
        if (ax > 145) {
            ax = 5;
            ay += 55;
        }
    }

    UIView *placeView = [self createPlaceView:nil];
    placeView.tag = -1;
    placeView.frame = CGRectMake(ax, ay, 50, 50);

    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, ay + 55);
}

- (UIView *)createPlaceView:(Place *)place {
    UIView *placeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];

    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    img.image = [UIImage imageNamed:@"kidPicBg"];
    [placeView addSubview:img];

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont boldSystemFontOfSize:12];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.shadowOffset = CGSizeMake(1.0, 1.0);
    lbl.shadowColor = [UIColor blackColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:12];

    if (place == nil) {
        lbl.text = @"Add Place";
    } else {
        lbl.text = place.title;
    }

    [placeView addSubview:lbl];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(placeTap:)];

    [placeView addGestureRecognizer:gesture];


    [self.scrollView addSubview:placeView];

    return placeView;
}

- (void)placeTap:(UITapGestureRecognizer *)gesture {
    Place *place = nil;
    if (gesture.view.tag > -1) {
        NSArray *places = [[UserManager sharedInstance] myPlaces];
        place = (Place *) [places objectAtIndex:gesture.view.tag];
    }
    [self.delegate placeSelected:place];
}

- (CGSize)contentSizeForViewInPopover {
    return CGSizeMake(200, 200);
}

@end
