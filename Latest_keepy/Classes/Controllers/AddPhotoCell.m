//
//  AddPhotoCell.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddPhotoCell.h"

@interface AddPhotoCell ()

@property(nonatomic, weak) IBOutlet UILabel *addPhotoLabel;
@property(nonatomic, assign) BOOL originalClipsToBounds;
@property(nonatomic, assign) UIViewContentMode originalContentMode;

@end

@implementation AddPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.addPhotoLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    self.originalClipsToBounds = self.addPhotoButton.imageView.clipsToBounds;
    self.originalContentMode = self.addPhotoButton.imageView.contentMode;
}

+ (CGFloat)height {
    return 109.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    if (self.picture) {
        self.addPhotoButton.imageView.clipsToBounds = self.originalClipsToBounds;
        self.addPhotoButton.imageView.contentMode = self.originalContentMode;
        [self.addPhotoButton setImage:self.picture forState:UIControlStateNormal];
    } else {
        self.addPhotoButton.imageView.contentMode = UIViewContentModeCenter;
        self.addPhotoButton.imageView.clipsToBounds = NO;
        switch (self.addPhotoCellType) {
            default:
            case AddPhotoCellTypeBoy: {
                [self.addPhotoButton setImage:[UIImage imageNamed:@"blue-boy"] forState:UIControlStateNormal];
                break;
            }

            case AddPhotoCellTypeGirl: {
                [self.addPhotoButton setImage:[UIImage imageNamed:@"blue-girl"] forState:UIControlStateNormal];
                break;
            }
        }
    }
}

- (IBAction)addPhoto:(id)sender {
    [self.delegate addPhoto:sender];
}

@end
