//
//  KidsCell.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "KidsCell.h"
#import "KidView.h"

#import "Keepy-Swift.h"

@interface KidsCell ()

@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end

@implementation KidsCell

+ (CGFloat)height {
    return 103.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    CGFloat width = fminf([KidView width] * self.kids.count, self.frame.size.width);
    self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, self.scrollView.frame.origin.y, width, self.scrollView.frame.size.height);
    self.scrollView.center = self.contentView.center;

    self.scrollView.contentSize = CGSizeMake([KidView width] * self.kids.count, self.scrollView.contentSize.height);
    for (int i = 0; i < self.kids.count; i++) {
        NSDictionary *kid = [self.kids objectAtIndex:i];

        // DDLogInfo(@"%@",kid);

        KidView *kidView = [[KidView alloc] init];
        kidView.frame = CGRectMake([KidView width] * i, kidView.frame.origin.y, kidView.frame.size.width, kidView.frame.size.height);
        kidView.name.text = [kid objectForKey:@"name"];
        id hash = [kid objectForKey:@"hash"];
        if (hash != [NSNull null]) {
            NSString *url = [NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, (NSString *) hash];
            kidView.imageView.image = nil;
            [kidView.activityIndicatorView startAnimating];
            [kidView.imageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [kidView.activityIndicatorView stopAnimating];
                kidView.imageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];
            }];
        } else {
            int gender = [[kid objectForKey:@"gender"] intValue];
            kidView.imageView.image = [GlobalUtils getKidDefaultImage:gender];
        }
        [self.scrollView addSubview:kidView];
    }
}

@end