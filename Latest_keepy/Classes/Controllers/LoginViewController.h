//
//  LoginViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 3/17/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@protocol LoginViewDelegate

- (void)didLogin:(NSDictionary *)userInfo;

@end

@interface LoginViewController : KPPopViewController

@property(nonatomic, assign) id <LoginViewDelegate> delegate;

@end
