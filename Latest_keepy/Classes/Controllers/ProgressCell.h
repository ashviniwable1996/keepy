//
//  ProgressCell.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    ProgressCellStateOne,
    ProgressCellStateTwo,
    ProgressCellStateThree,
    ProgressCellStateFour
} ProgressCellState;

@protocol ProgressCellDelegate;

@interface ProgressCell : UITableViewCell

@property(nonatomic, weak) id <ProgressCellDelegate> delegate;
@property(nonatomic, assign) ProgressCellState progressCellState;
@property(nonatomic, weak) IBOutlet UIImageView *progressImageView;
@property(nonatomic, weak) IBOutlet UIButton *skipButton;
@property(nonatomic, assign) BOOL bold;

+ (CGFloat)height;

- (void)layout;

@end

@protocol ProgressCellDelegate <NSObject>

- (void)skip:(id)sender;

@end