//
//  DKMultyUploaderManger.m
//  Keepy
//
//  Created by Daniel Karsh on 5/1/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "DKMultyUploaderManger.h"
#import "DKVideoUploaderManager.h"
#import "KPLocalItem.h"

@implementation DKMultyUploaderManger
@synthesize allVideosUploadingProccess;

+ (DKMultyUploaderManger *)sharedClient {
    static DKMultyUploaderManger *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[DKMultyUploaderManger alloc] init];
    });

    return _sharedClient;
}


- (instancetype)init {

    self = [super init];
    if (self) {
        self.allVideosUploadingProccess = [NSMutableArray array];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(killMe:) name:@"VideoUploaderDone" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelAllUploads:) name:@"cancelAllUploadsNotification" object:nil];
    }
    return self;
}

- (void)addUploaderWithMovieURL:(NSURL *)myMovieFileURL
                  andThumbImage:(UIImage *)thumbnailImage {
    DKVideoUploaderManager *uploadManager = [[DKVideoUploaderManager alloc] init];
    [uploadManager setThumbnail:thumbnailImage];
    [uploadManager setMyMovieFileURL:myMovieFileURL];

    [allVideosUploadingProccess addObject:uploadManager];
    [uploadManager prepareAssetOriginalImage];
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:nil];

}

- (void)cancelAllUploads:(NSNotification *)notification
{
    for (DKVideoUploaderManager * uploader in allVideosUploadingProccess) {
        [uploader killMe];
        [[NSNotificationCenter defaultCenter] removeObserver:uploader];
    }
}


- (void)addUploaderWithItem:(KPLocalItem *)item {
    
    DKVideoUploaderManager *uploadManager = [[DKVideoUploaderManager alloc] init];
    
    // TODO: re-enable only process view for item
    
    [uploadManager setOnlyProccessViewForItem:item];
    
    [allVideosUploadingProccess addObject:uploadManager];
    
    // this notification was sending nil
    // now we replaced it with the actual item that is being sent
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:nil userInfo:@{@"itemID": @(item.identifier)}];

}


- (void)killMe:(NSNotification *)note {

    DKVideoUploaderManager *uploader = note.object;
    [uploader killMe];
    
    [[NSNotificationCenter defaultCenter] removeObserver:uploader];
    NSLog(@"how many videos upload processes %lu", (unsigned long)[self.allVideosUploadingProccess count]);
    [self.allVideosUploadingProccess removeObject:uploader];
    uploader = nil;


    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:nil userInfo:note.userInfo];
//    if (self.allVideosUploadingProccess.count>0) {
//        [(DKVideoUploaderManager*)[self.allVideosUploadingProccess firstObject] uploadOriginalImage];
//    }
}

@end
