//
//  CommentsViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/18/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

@protocol CommentsViewDelegate

- (void)commentSelected:(Comment *)comment;

@end

@interface CommentsViewController : UIViewController

- (instancetype)initWithItem:(Item *)item;

@property(nonatomic, assign) id <CommentsViewDelegate> delegate;

@end
