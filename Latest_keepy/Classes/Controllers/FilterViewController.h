//
//  FilterViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 3/12/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterViewDelegate

- (void)filterSelected:(NSInteger)filterType withValue:(NSString *)filterValue;

@end

@interface FilterViewController : UIViewController

@property(nonatomic, assign) id <FilterViewDelegate> delegate;

@end
