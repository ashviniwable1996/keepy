//
//  AddSiblingsViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddSiblingsViewController.h"
#import "AddSiblingViewController.h"
#import "KidsCell.h"
#import "FamilySelectorCell.h"
#import "ProgressCell.h"
#import "AppDelegate.h"
@interface AddSiblingsViewController () <UITableViewDataSource, UITableViewDelegate, FamilySelectorCellDelegate, ProgressCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AddSiblingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-open"];


    [self.tableView registerNib:[UINib nibWithNibName:@"KidsCell" bundle:nil] forCellReuseIdentifier:@"KidsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilyCell" bundle:nil] forCellReuseIdentifier:@"FamilyCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilySelectorCell" bundle:nil] forCellReuseIdentifier:@"FamilySelectorCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProgressCell" bundle:nil] forCellReuseIdentifier:@"ProgressCell"];
    AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [d reloadTweaks];
    long tweakValue = [d.tweakValues[TweakPromoUnlimitedAfterUpload] integerValue];
    [[Mixpanel sharedInstance] track:TweakPromoUnlimitedAfterUpload properties:@{@"source": TweakPromoUnlimitedAfterUpload}];
    if (tweakValue == 1){
        [GlobalUtils showUpgradeScreenWithParameters:nil fromViewController:@"After Upload"];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        static NSString *KidsCellIdentifier = @"KidsCell";
        KidsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KidsCellIdentifier];
        cell.kids = self.kids;
        [cell layout];
        return cell;
    } else if (indexPath.row == 1) {
        static NSString *FamilySelectorCellIdentifier = @"FamilySelectorCell";
        FamilySelectorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FamilySelectorCellIdentifier];
        cell.delegate = self;
        cell.familySelectorCellType = FamilySelectorCellTypeSiblings;
        cell.title.text = [self title];
        [cell layout];
        return cell;
    } else if (indexPath.row == 2) {
        static NSString *ProgressCellIdentifier = @"ProgressCell";
        ProgressCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ProgressCellIdentifier];
        cell.delegate = self;
        cell.progressCellState = ProgressCellStateOne;
        [cell layout];
        return cell;
    }
    return nil;
}

- (NSString *)title {
    switch (self.kids.count) {
        case 1: {
            NSDictionary *kid = [self.kids objectAtIndex:0];
            return [NSString stringWithFormat:@"Does %@ have any siblings?", [kid objectForKey:@"name"]];
        }

        case 2: {
            NSDictionary *kid1 = [self.kids objectAtIndex:0];
            NSDictionary *kid2 = [self.kids objectAtIndex:1];
            return [NSString stringWithFormat:@"Do %@ & %@ have any other siblings?", [kid1 objectForKey:@"name"], [kid2 objectForKey:@"name"]];
        }

        case 3: {
            NSDictionary *kid1 = [self.kids objectAtIndex:0];
            NSDictionary *kid2 = [self.kids objectAtIndex:1];
            NSDictionary *kid3 = [self.kids objectAtIndex:2];
            return [NSString stringWithFormat:@"Do %@, %@, & %@ have any other siblings?", [kid1 objectForKey:@"name"], [kid2 objectForKey:@"name"], [kid3 objectForKey:@"name"]];
        }

        default: {
            return @"Do your kids have any more siblings?";
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [KidsCell height];
    } else if (indexPath.row == 1) {
        return [FamilySelectorCell height:[self title]];
    } else if (indexPath.row == 2) {
        return [ProgressCell height];
    }
    return 0.0f;
}

- (CGFloat)height {
    return [KidsCell height] + [FamilySelectorCell height:[self title]] + [ProgressCell height];
}

- (IBAction)addSiblingsExit:(UIStoryboardSegue *)segue {
    AddSiblingViewController *addSiblingViewController = (AddSiblingViewController *) segue.sourceViewController;
    if (addSiblingViewController.kids) {
        self.kids = addSiblingViewController.kids;
        [self.tableView reloadData];
    }
}

- (void)leftSelected:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-add-sister-tap"];

    [self performSegueWithIdentifier:@"AddSister" sender:self];
}

- (void)rightSelected:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-add-brother-tap"];

    [self performSegueWithIdentifier:@"AddBrother" sender:self];
}

- (void)skip:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-skip-tap"];

    [self.delegate doneAddingSiblings:self.kids];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddBrother"]) {
        AddSiblingViewController *addSiblingViewController = (AddSiblingViewController *) segue.destinationViewController;
        addSiblingViewController.addSiblingViewControllerType = AddSiblingViewControllerTypeBrother;
    } else if ([segue.identifier isEqualToString:@"AddSister"]) {
        AddSiblingViewController *addSiblingViewController = (AddSiblingViewController *) segue.destinationViewController;
        addSiblingViewController.addSiblingViewControllerType = AddSiblingViewControllerTypeSister;
    }
}

@end
