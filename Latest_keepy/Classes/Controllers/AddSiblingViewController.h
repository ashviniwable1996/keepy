//
//  AddSiblingViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "BirthDateViewController.h"


typedef enum {
    AddSiblingViewControllerTypeSister,
    AddSiblingViewControllerTypeBrother
} AddSiblingViewControllerType;

@interface AddSiblingViewController : UIViewController <BirthDateViewControllerDelegate>

@property(nonatomic, assign) AddSiblingViewControllerType addSiblingViewControllerType;
@property(nonatomic, strong) NSArray *kids;

- (CGFloat)height;

@end
