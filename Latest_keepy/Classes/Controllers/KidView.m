//
//  KidView.m
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "KidView.h"

@implementation KidView

- (instancetype)init {
    if ((self = [[[NSBundle mainBundle] loadNibNamed:@"KidView" owner:self options:nil] firstObject])) {
        self.name.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    }
    return self;
}

+ (CGFloat)width {
    return 77.0f;
}

@end
