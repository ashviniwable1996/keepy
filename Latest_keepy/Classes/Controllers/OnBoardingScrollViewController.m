//
//  OnBoardingScrollViewController.m
//  Keepy
//
//  Created by Joao Barbosa on 05/08/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnBoardingScrollViewController.h"
#import "OnBoardingScreenView.h"

@interface OnBoardingScrollViewController ()<UIScrollViewDelegate>
{
    BOOL didPush;
    CGFloat phoneScrollToViewScrollRatio;
    BOOL isFirstTime;
}

@property (strong, nonatomic) NSArray *images;
@property (strong, nonatomic) NSArray *actions;
@property (strong, nonatomic) NSArray *descriptions;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *iphoneScrollView;

@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIImageView *iphoneImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iphoneScrollViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageIphoneWidth;

@end

@implementation OnBoardingScrollViewController

@synthesize images = _images;
@synthesize descriptions = _descriptions;
@synthesize actions = _actions;

-(void) viewDidLoad{
    
    [super viewDidLoad];
    self.scrollView.delegate = self;
   
}
-(void)viewDidLayoutSubviews{
    if ([UIScreen mainScreen].bounds.size.height == 812) {
//        self.iphoneScrollViewWidth.constant = 40;
//        self.imageIphoneWidth.constant      = 35.0;
        self.iphoneScrollView.contentSize = CGSizeMake((self.iphoneScrollView.frame.size.width) * 3, self.iphoneScrollView.frame.size.height);
//        NSLog(@"Gtttttttttt CS : %@",self.iphoneScrollView.contentSize);
    } else {
        self.iphoneScrollView.contentSize = CGSizeMake(self.iphoneScrollView.frame.size.width*3, self.iphoneScrollView.frame.size.height);
    }
    if(!isFirstTime){
        isFirstTime = true;
              self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 4, self.scrollView.frame.size.height);
        CGFloat pageWidth = self.scrollView.frame.size.width;
        
        phoneScrollToViewScrollRatio = self.iphoneScrollView.frame.size.width/pageWidth;
        
        
        for(int i = 0; i<3; i++){
            CGRect phoneScreenFrame;
            phoneScreenFrame.origin.x = self.iphoneScrollView.frame.size.width  * i ;
            //        phoneScreenFrame.size.width
            phoneScreenFrame.origin.y = 0.0;
            phoneScreenFrame.size = self.iphoneScrollView.frame.size;
//            phoneScreenFrame.size = self.iphoneScrollView.contentSize;
            if ([UIScreen mainScreen].bounds.size.height == 812) {
//                phoneScreenFrame.size.width = self.iphoneScrollView.contentSize.width / 3;
//                phoneScreenFrame.size.width =  phoneScreenFrame.size.width + 30.0;
//                phoneScreenFrame.size.width = self.iphoneScrollView.contentSize.width / 0.8125;
            }
            
            UIImageView* screenImageView = [[UIImageView alloc] initWithFrame:phoneScreenFrame];
            [screenImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:i]]];
            [self.iphoneScrollView addSubview:screenImageView];
        }
      
    }
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.images = [[NSArray alloc] initWithObjects:@"screen_01.png", @"screen_02.png", @"screen_03.png", nil];
    self.actions = [[NSArray alloc] initWithObjects:@"Welcome to Keepy", @"Add a video story", @"Share with Loved Ones", nil];
    
    self.descriptions = [[NSArray alloc] initWithObjects:@"All your kids' memories in one place. Organized, saved and privately shared.", @"Describe the moment with a video story.", @"Invite only those who care about your kids' memories (all they need is an email).", nil];
    
    if([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        
        [self.iphoneImageView setFrame:CGRectMake(self.view.frame.origin.x, self.iphoneImageView.frame.origin.y, 124, 243)];
        [self.iphoneScrollView setFrame:CGRectMake(self.view.frame.origin.x, self.iphoneScrollView.frame.origin.y, 102, 175)];
        
        [self.iphoneImageView setCenter:CGPointMake(self.view.center.x, self.iphoneImageView.center.y)];
        [self.iphoneScrollView setCenter:CGPointMake(self.view.center.x, self.iphoneImageView.center.y - 1.0)];
    }
//    self.iphoneScrollView.contentSize = CGSizeMake(self.iphoneScrollView.frame.size.width*3, self.iphoneScrollView.frame.size.height);
//    for(int i = 0; i<3; i++){
//        CGRect phoneScreenFrame;
//        phoneScreenFrame.origin.x = self.iphoneScrollView.frame.size.width * i ;
//        
//        phoneScreenFrame.origin.y = 0.0;
//        phoneScreenFrame.size = self.iphoneScrollView.frame.size;
//        
//        
//        UIImageView* screenImageView = [[UIImageView alloc] initWithFrame:phoneScreenFrame];
//        [screenImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:i]]];
//        [self.iphoneScrollView addSubview:screenImageView];
//    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 4, self.scrollView.frame.size.height);
    CGRect cloudsFrame;
    cloudsFrame.origin.x = -320.0;
    cloudsFrame.origin.y = self.view.frame.size.height - 244;
    cloudsFrame.size = CGSizeMake(1600, 244);
    
    UIImageView *clouds = [[UIImageView alloc] initWithFrame:cloudsFrame];
    
    [clouds setImage:[UIImage imageNamed:@"clouds_scroll.png"]];
    [self.scrollView addSubview:clouds];
    
    
    
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        [self.pageControl setCenter:CGPointMake(self.view.center.x, self.view.frame.size.height - 20.0f)];
        
    }
    
    [self.skipButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:13]];
    [self.skipButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.pageControl setNumberOfPages:4];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
   
    NSLog(@"Phone Ratio%f",phoneScrollToViewScrollRatio);
    [[Mixpanel sharedInstance] track:@"tutorial_landing_page_start"];
    
    if (!self.isMovingToParentViewController)//coming back from previous view controller
    {
        CGFloat pageWidth = self.scrollView.frame.size.width;
        int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if(page == 3)
        {
            self.pageControl.currentPage = 2;
            self.scrollView.contentOffset = CGPointMake(2*pageWidth, 0);
        }
    }
    didPush = NO;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self viewDidLayoutSubviews];
    for(int i = 0; i< 3; i++){
        CGRect screenFrame;
        screenFrame.origin.x = self.view.frame.origin.x + self.view.frame.size.width * i;
        
        screenFrame.origin.y = self.view.frame.origin.y;
        screenFrame.size = self.view.frame.size;
        
        OnBoardingScreenView *screenView = [OnBoardingScreenView myView];
        [screenView initScreenWithAction:[self.actions objectAtIndex:i] description:[self.descriptions objectAtIndex:i] andImage:[UIImage imageNamed:[self.images objectAtIndex:i]]];
        
        [screenView setFrame:screenFrame];
        [self.scrollView addSubview:screenView];
        
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{   
    CGFloat pageWidth = self.scrollView.frame.size.width;

    CGFloat contentOffsetAmount = phoneScrollToViewScrollRatio*  self.scrollView.contentOffset.x;
    if (contentOffsetAmount >= 0 && contentOffsetAmount <= (_iphoneScrollView.contentSize.width - _iphoneScrollView.frame.size.width))
    {
        
        [self.iphoneScrollView setContentOffset:CGPointMake(contentOffsetAmount, 0.0) animated:NO];
    }
    
    // Update the page when more than 50% of the previous/next page is visible
        
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    int previousPage = (int)self.pageControl.currentPage;
    
    if(previousPage != page)
    {
        if(page == 3){
            self.pageControl.currentPage = page;
            if(!didPush)
            {
                didPush = YES;
                [[Mixpanel sharedInstance] track:@"tutorial_landing_page_cta"];
                
                [self performSegueWithIdentifier:@"onboardingScrollToSignUp" sender:self];
            }
        }
        else{
            
            if(previousPage > page)
            {
                [[Mixpanel sharedInstance] track:@"tutorial_landing_page_back" properties:@{@"tutorial_page": @(page)}];
            }
            else
            {
                [[Mixpanel sharedInstance] track:@"tutorial_landing_page_next" properties:@{@"tutorial_page": @(page)}];
            }
            self.pageControl.currentPage = page;
            //            [self.iphoneScrollView setContentOffset:CGPointMake(self.iphoneScrollView.frame.size.width * page, 0.0) animated:YES];
        }
    }
    
}
- (IBAction)skipAction:(id)sender {
    
    [self performSegueWithIdentifier:@"onboardingScrollToSignUp" sender:self];
}

@end
