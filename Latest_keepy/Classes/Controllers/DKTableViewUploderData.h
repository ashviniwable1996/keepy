//
//  DKTableViewUploderData.h
//  Keepy
//
//  Created by Daniel Karsh on 5/9/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DKTableViewUploderData : NSObject <UITableViewDataSource, UITableViewDelegate>

@end
