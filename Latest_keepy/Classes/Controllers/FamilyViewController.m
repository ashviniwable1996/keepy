//
//  FamilyViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilyViewController.h"
#import "AddSiblingsViewController.h"
#import "AddParentsViewController.h"
#import "AddParentViewController.h"
#import "AddGrandParentsViewController.h"
#import "AddRelativesViewController.h"
#import "FamilySuccessViewController.h"
#import "FamilyFailViewController.h"
#import "AddGrandParentViewController.h"
#import "AddRelativeViewController.h"

//#import "GPUImage.h"

@interface FamilyViewController () <AddSiblingsViewControllerDelegate, AddParentsViewControllerDelegate, AddGrandParentsViewControllerDelegate, AddRelativesViewControllerDelegate, FamilySuccessViewControllerDelegate, FamilyFailViewControllerDelegate>

@property(nonatomic, strong) NSMutableDictionary *viewControllersByIdentifier;
@property(nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property(nonatomic, weak) IBOutlet UIView *backgroundView;
@property(nonatomic, strong) NSValue *originalContainerViewFrame;
@property(nonatomic, strong) NSArray *kids;
@property(nonatomic, strong) NSArray *family;

@end

@implementation FamilyViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    self.viewControllersByIdentifier = [NSMutableDictionary dictionary];

    self.backgroundView.alpha = 0.0f;
    //self.backgroundImageView.image = self.backgroundImage;

    CGRect containerViewFrame = self.containerView.frame;
    self.originalContainerViewFrame = [NSValue valueWithCGRect:self.containerView.frame];
    containerViewFrame.origin.y = -containerViewFrame.size.height;
    self.containerView.frame = containerViewFrame;

    __weak FamilyViewController *weakSelf = self;
    [[ServerComm instance] getKids:YES withSuccessBlock:^(NSDictionary *result) {
        weakSelf.kids = [result objectForKey:@"result"];
        [[ServerComm instance] getFans:^(id result) {
            weakSelf.family = [result objectForKey:@"result"];
            [weakSelf performSelector:@selector(start) withObject:nil afterDelay:0.5f];
        }                 andFailBlock:^(NSError *error) {
            // Do nothing if we can't load fans
        }];
    }                 andFailBlock:^(NSError *error) {
        // If we failed to load kids, forget onboarding.
        [weakSelf dismissViewControllerAnimated:NO completion:nil];
    }];
}

- (void)start {
    [self performSegueWithIdentifier:@"AddSiblings" sender:self];
}

- (void)prepareForSegue:(FamilySegue *)segue sender:(id)sender {
    self.previousViewController = self.nextViewController;
    if (![self.viewControllersByIdentifier objectForKey:segue.identifier]) {
        [self.viewControllersByIdentifier setObject:segue.destinationViewController forKey:segue.identifier];
    }

    self.nextIdentifier = segue.identifier;
    self.nextViewController = [self.viewControllersByIdentifier objectForKey:self.nextIdentifier];


    if ([self.nextIdentifier isEqualToString:@"AddSiblings"]) {
        UINavigationController *addSiblingsNavigationController = (UINavigationController *) self.nextViewController;
        AddSiblingsViewController *addSiblingsViewController = (AddSiblingsViewController *) [addSiblingsNavigationController.viewControllers firstObject];
        addSiblingsViewController.delegate = self;
        addSiblingsViewController.kids = self.kids;
        [self reveal];
    } else if ([self.nextIdentifier isEqualToString:@"AddParents"]) {
        UINavigationController *addParentsNavigationController = (UINavigationController *) self.nextViewController;
        AddParentsViewController *addParentsViewController = (AddParentsViewController *) [addParentsNavigationController.viewControllers firstObject];
        addParentsViewController.delegate = self;
        addParentsViewController.kids = self.kids;
    } else if ([self.nextIdentifier isEqualToString:@"AddGrandParents"]) {
        UINavigationController *addGrandParentsNavigationController = (UINavigationController *) self.nextViewController;
        AddGrandParentsViewController *addGrandParentsViewController = (AddGrandParentsViewController *) [addGrandParentsNavigationController.viewControllers firstObject];
        addGrandParentsViewController.delegate = self;
        addGrandParentsViewController.kids = self.kids;
        addGrandParentsViewController.family = self.family;
    } else if ([self.nextIdentifier isEqualToString:@"AddRelatives"]) {
        UINavigationController *addRelativesNavigationController = (UINavigationController *) self.nextViewController;
        AddRelativesViewController *addRelativesViewController = (AddRelativesViewController *) [addRelativesNavigationController.viewControllers firstObject];
        addRelativesViewController.delegate = self;
        addRelativesViewController.kids = self.kids;
        addRelativesViewController.family = self.family;
    } else if ([self.nextIdentifier isEqualToString:@"FamilySuccess"]) {

        CGRect rect = self.view.frame;

        self.containerView.frame = CGRectMake(self.containerView.frame.origin.x, rect.size.height / 2 - 150, self.containerView.frame.size.width, 300);
        FamilySuccessViewController *familySuccessViewController = (FamilySuccessViewController *) self.nextViewController;
        familySuccessViewController.delegate = self;
        familySuccessViewController.kids = self.kids;
        familySuccessViewController.family = self.family;
        familySuccessViewController.containerViewFrame = self.containerView.frame;
    } /*else if ([self.nextIdentifier isEqualToString:@"FamilyFail"]) {
        FamilyFailViewController *familyFailViewController = (FamilyFailViewController *)self.nextViewController;
        familyFailViewController.delegate = self;
        familyFailViewController.kids = self.kids;
        familyFailViewController.family = self.family;
    }*/
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([self.nextIdentifier isEqualToString:identifier]) {
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [self.viewControllersByIdentifier.allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
        if (![self.nextIdentifier isEqualToString:key]) {
            [self.viewControllersByIdentifier removeObjectForKey:key];
        }
    }];
}

- (void)reveal {
//    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:self.backgroundImage];
//    GPUImageiOSBlurFilter *stillImageFilter = [[GPUImageiOSBlurFilter alloc] init];
//    [stillImageSource addTarget:stillImageFilter];
//    [stillImageSource processImage];
//    self.backgroundImageView.image = [stillImageFilter imageFromCurrentlyProcessedOutput];

    __weak FamilyViewController *weakSelf = self;
    [UIView animateWithDuration:0.3f animations:^{
        weakSelf.backgroundView.alpha = 0.85f;
        weakSelf.containerView.frame = [self.originalContainerViewFrame CGRectValue];
    }];
}

- (void)doneAddingSiblings:(NSArray *)kids {
    if (self.kids != nil) {
        self.kids = kids;
    }
    if (self.family) {
        // If another parent is found, skip to grandparents.
        BOOL foundOtherParent = NO;
        for (NSDictionary *familyMember in self.family) {
            int relationshipTypeId = [[familyMember objectForKey:@"fanRelationType"] intValue];
            if (relationshipTypeId == AddParentViewControllerTypeMom || relationshipTypeId == AddParentViewControllerTypeDad) {
                foundOtherParent = YES;
                break;
            }
        }
        if (foundOtherParent) {
            [self performSegueWithIdentifier:@"AddGrandParents" sender:self];
        } else {
            [self performSegueWithIdentifier:@"AddParents" sender:self];
        }
    } else {
        [self performSegueWithIdentifier:@"AddParents" sender:self];
    }
}

- (void)doneAddingParents:(NSArray *)family {
    if (family != nil) {
        self.family = family;
    }
    [self performSegueWithIdentifier:@"AddGrandParents" sender:self];
}

- (void)doneAddingGrandParents:(NSArray *)family {
    if (family != nil) {
        self.family = family;
    }
    [self performSegueWithIdentifier:@"AddRelatives" sender:self];
}

- (void)doneAddingRelatives:(NSArray *)family {
    if (family != nil) {
        self.family = family;
    }

    BOOL success = NO;
    for (NSDictionary *familyMember in self.family) {
        int relationshipTypeId = [[familyMember objectForKey:@"fanRelationType"] intValue];
        if (relationshipTypeId == AddGrandParentViewControllerTypeGrandma || relationshipTypeId == AddGrandParentViewControllerTypeGrandpa || relationshipTypeId == AddRelativeViewControllerTypeAunt || relationshipTypeId == AddRelativeViewControllerTypeUncle) {
            success = YES;
            break;
        }
    }

    /*
    if (success) {
        [self performSegueWithIdentifier:@"FamilySuccess" sender:self];
    } else {
        [self performSegueWithIdentifier:@"FamilyFail" sender:self];
    }*/

    // let us not show the family thing
    // [self performSegueWithIdentifier:@"FamilySuccess" sender:self];

    // instead, let's just say we are done
    [self doneAddingFamily]; // this should close everything
}

- (void)doneAddingFamily {
    CGRect newContainerViewFrame = CGRectMake(self.containerView.frame.origin.x, -self.containerView.frame.size.height, self.containerView.frame.size.width, self.containerView.frame.size.height);
    __weak FamilyViewController *weakSelf = self;
    [UIView animateWithDuration:0.3f animations:^{
        weakSelf.backgroundView.alpha = 0.0f;
        weakSelf.containerView.frame = newContainerViewFrame;
    }                completion:^(BOOL finished) {
        
        [[UserManager sharedInstance] setIsAfterRegistration:NO];
        [[UserManager sharedInstance] setIsAfterOnBoardProccess:YES];
        
        // TODO: this has to be re-enabled
        [[UserManager sharedInstance] refreshMyData];
        
        
        // and then, after refresh my data is complete, the proper kid will have to be chosen. It is of crucial importance
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_UNLIMITED_AFTER_ONBOARD object:nil];

        [weakSelf dismissViewControllerAnimated:NO completion:nil];
        
    }];
}


- (void)onKeyboardWillShow:(NSNotification *)notification {

    // @HACK: If the view is add siblings, don't adjust because the input
    // goes off screen for this particular screen. The correct way to do this
    // is to get the bounds of the active responder associated with this
    // keyboard notification, and use it to adjust the screen.
    if ([self.nextIdentifier isEqualToString:@"AddSiblings"]) {
        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
        swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer:swipeGesture];
        return;
    }

    NSDictionary *userInfo = notification.userInfo;
    NSValue *endFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [self.view convertRect:endFrameValue.CGRectValue fromView:nil];
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;

    self.originalContainerViewFrame = [NSValue valueWithCGRect:self.containerView.frame];
    CGFloat newCenter = ((([UIScreen mainScreen].bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height) - keyboardEndFrame.size.height) / 2.0f) + ([UIApplication sharedApplication].statusBarFrame.size.height * 2.0f);
    __weak FamilyViewController *weakSelf = self;
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        weakSelf.containerView.center = CGPointMake(weakSelf.containerView.center.x, newCenter);
    }                completion:^(BOOL finished) {
        if (finished) {
            UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:weakSelf action:@selector(swipe:)];
            swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
            [weakSelf.view addGestureRecognizer:swipeGesture];
        }
    }];
}

- (void)swipe:(UISwipeGestureRecognizer *)recognizer {
    [self.view removeGestureRecognizer:recognizer];
    [self.view endEditing:YES];
}

- (void)onKeyboardWillHide:(NSNotification *)notification {
    // @HACK: If the view is add siblings, don't adjust because the input
    // goes off screen for this particular screen. The correct way to do this
    // is to get the bounds of the active responder associated with this
    // keyboard notification, and use it to adjust the screen.
    if ([self.nextIdentifier isEqualToString:@"AddSiblings"]) {
        return;
    }

    NSDictionary *userInfo = notification.userInfo;
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;

    __weak FamilyViewController *weakSelf = self;
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        weakSelf.containerView.frame = [self.originalContainerViewFrame CGRectValue];
    }                completion:nil];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    CGFloat height = 400.0f;
    if ([viewController respondsToSelector:@selector(height)]) {
        height = [(AddSiblingsViewController *) viewController height];
    }

    CGRect newBounds = self.containerView.bounds;
    newBounds.size.height = height;
    __weak FamilyViewController *weakSelf = self;
    [UIView animateWithDuration:0.3 delay:0.3 options:0 animations:^{
        weakSelf.containerView.bounds = newBounds;
    }                completion:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

@end

@implementation FamilySegue

- (void)perform {
    FamilyViewController *familyViewController = (FamilyViewController *) self.sourceViewController;
    UIViewController *nextViewController = (UIViewController *) familyViewController.nextViewController;

    CGFloat height = 400.0f;
    if ([nextViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *) nextViewController;
        navigationController.delegate = familyViewController;
        UIViewController *rootViewController = navigationController.viewControllers[0];
        if ([rootViewController respondsToSelector:@selector(height)]) {
            height = [(AddSiblingsViewController *) rootViewController height];
        }
    } else if ([nextViewController respondsToSelector:@selector(height)]) {
        height = [(AddSiblingsViewController *) nextViewController height];
    }

    CGRect newBounds = familyViewController.containerView.bounds;
    newBounds.size.height = height;

    if (familyViewController.previousViewController) {
        [UIView animateWithDuration:0.3f delay:0.3f options:0 animations:^{
            familyViewController.containerView.bounds = newBounds;
        }                completion:nil];

        CATransition *animation = [CATransition animation];
        [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setDuration:0.3f];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[familyViewController.containerView layer] addAnimation:animation forKey:@"pushIn"];

        [familyViewController.previousViewController willMoveToParentViewController:nil];
        [familyViewController.previousViewController.view removeFromSuperview];
        [familyViewController.previousViewController removeFromParentViewController];

        nextViewController.view.frame = newBounds;
        [familyViewController addChildViewController:nextViewController];
        [familyViewController.containerView addSubview:nextViewController.view];
        [familyViewController didMoveToParentViewController:familyViewController];

    } else {
        familyViewController.containerView.bounds = newBounds;
        nextViewController.view.frame = familyViewController.containerView.bounds;
        [familyViewController addChildViewController:nextViewController];
        [familyViewController.containerView addSubview:nextViewController.view];
        [familyViewController didMoveToParentViewController:familyViewController];
    }
}

@end
