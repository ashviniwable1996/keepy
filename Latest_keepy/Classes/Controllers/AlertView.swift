//
//  AlertView.swift
//  Keepy
//
//  Copyright (c) 2015 Keepy. All rights reserved.
//

import Foundation
import UIKit
import Crashlytics

public extension UIView {
    /**
     Add subview to the reciever and install constraints to fill parent size
     
     - parameter contentView: subview
     */
    public func addSubviewOnEntireSize(_ contentView: UIView) {
        addSubview(contentView, inset: .zero)
    }
    
    /**
     Add subview to the reciever and install constraints to fill parent size with inset
     
     - parameter contentView: subview
     - parameter inset: view inset
     */
    public func addSubview(_ contentView: UIView, inset: UIEdgeInsets) {
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        let views = [ "contentView": contentView ]
        let metrics = [
            "top" : inset.top,
            "left" : inset.left,
            "right" : inset.right,
            "bottom" : inset.bottom,
            ]
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(left)-[contentView]-(right)-|", options: [], metrics: metrics, views: views))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(top)-[contentView]-(bottom)-|", options: [], metrics: metrics, views: views))
        setNeedsLayout()
    }
}

// MARK: - Load from nib
extension UIView {
    class func loadFromNib(owner: AnyObject, nibName: String? = nil) -> Self? {
        let nib: String = nibName ?? String(describing: self)
        let view = instantiateFromNib(owner, nibName: nib)
        view?.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private class func instantiateFromNib(_ owner: AnyObject, nibName: String) -> Self? {
        do {
            return try instantiateFromNibHelper(owner, nibName: nibName)
        } catch {
            //Log.error?.message("Error loading nib: \(nibName)")
            return nil
        }
    }
    
    private class func instantiateFromNibHelper<T>(_ owner: AnyObject, nibName: String)  throws -> T? {
        return  Bundle.main.loadNibNamed(nibName, owner: owner, options: [:])?.filter { $0 is T}.first  as? T
    }
}

class AlertView: UIView {
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var container: UIView?
    @IBOutlet weak var confirmButton: UIButton?
    
    var useHapticFeedback = true

    static var numberMessagesOnScreen: Int = 0

    var title: String = "" {
        didSet {
            titleLabel?.text = title
            titleLabel?.accessibilityIdentifier = "alert_title_label"
        }
    }
    var message: String = "" {
        didSet {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5
            paragraphStyle.alignment = .center
            
            let attrString = NSMutableAttributedString(string: message)
            attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
            messageLabel?.attributedText = attrString
            
            messageLabel?.accessibilityIdentifier = "alert_message_label"
        }
    }

    var confirmButtonTitle: String = "" {
        didSet {
            let height = confirmButton?.frame.size.height ?? 0
            confirmButton?.setTitle(confirmButtonTitle, for: UIControlState())
            //confirmButton?.layer.borderColor = UIColor.gray225Color().cgColor
            //confirmButton?.layer.cornerRadius = AppTheme.current.advise.roundingCorners ? height / 2 : 0
            confirmButton?.sizeToFit()
            confirmButton?.accessibilityIdentifier = "alert_confirm_button"
        }
    }

    var hidesAfterConfirmClick: Bool = true
    var hidesOnTap: Bool = false

    var actionConfirm: () -> () = {}

    class func create(_ nibName: String = "AlertView") -> AlertView {
        guard let alert = AlertView.loadFromNib(owner: self) else {
            fatalError("Cant load from nib")
        }
        return alert
    }
    
    // MARK: Actions
    @IBAction func onViewTap(_ sender: AnyObject) {
        if hidesOnTap {
            hide()
        }
    }

    @IBAction func onOkClick(_ sender: AnyObject) {
        if hidesAfterConfirmClick {
            hide()
        }
        actionConfirm()
    }

    // MARK: Internal functions
    internal func show() {
        if #available(iOS 10.0, *), useHapticFeedback {
            let feedBackGenerator = UINotificationFeedbackGenerator()
            feedBackGenerator.prepare()
            feedBackGenerator.notificationOccurred(.error)
        }

        if let lastObject: AnyObject = UIApplication.shared.keyWindow?.subviews.first,
            let currentView = lastObject as? UIView, AlertView.numberMessagesOnScreen < 1 {
                AlertView.numberMessagesOnScreen += 1
                currentView.endEditing(true)
                addToView(currentView)
                alpha = 0
                UIView.animate(withDuration: 0.5, animations: { [weak self] in
                    self?.alpha = 1.0
                })
        } else {
              actionConfirm()
        }
    }

    private func addToView(_ parentView: UIView) {
        parentView.addSubviewOnEntireSize(self)
        parentView.setNeedsLayout()
    }

    internal func hide() {
        AlertView.numberMessagesOnScreen -= 1
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.alpha = 0.0
            }, completion: { [weak self] _ in
    self?.removeFromSuperview()
})
}
}
