//
//  ContactsSelectorViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 6/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <CoreText/CoreText.h>

#import "TFAContactsSelectorViewController.h"
#import <AddressBook/AddressBook.h>
#import "EditFanViewController.h"

#import "AFURLSessionManager.h"

@interface TFAContactsSelectorViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property(nonatomic) NSInteger mode;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *friendsList;
@property(nonatomic, strong) NSMutableArray *originalFriendsList;
@property(nonatomic, strong) NSMutableArray *marksItems;
@property(nonatomic, strong) NSArray *orgArr;
@property(nonatomic, strong) UITextField *txtField;
@property(nonatomic) BOOL isSpreadTheLove;
//@property (nonatomic) BOOL selectAll;

@property(nonatomic) NSInteger relationMode;

@end

@implementation TFAContactsSelectorViewController

- (instancetype)initWithMode:(NSInteger)mode {
    self = [super initWithNibName:@"TFAContactsSelectorViewController" bundle:nil];
    if (self) {
        self.isSpreadTheLove = (mode > 9);
        if (mode > 9) {
            mode -= 10;
        }

        self.mode = mode;
    }
    return self;
}

- (BOOL)isCheckedByDefault{

    return NO;
    
}

- (instancetype)initWithMode:(NSInteger)mode andRelationMode:(NSInteger)relationMode {
    self = [super initWithNibName:@"TFAContactsSelectorViewController" bundle:nil];
    if (self) {
        self.isSpreadTheLove = (mode > 9);
        if (mode > 9) {
            mode -= 10;
        }

        self.mode = mode;
        self.relationMode = relationMode;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];

    self.title = KPLocalizedString(@"contacts");
    if (self.isSpreadTheLove) {
        self.title = KPLocalizedString(@"     contacts");
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"spread the love"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"send"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil]];
    }
    else {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add fan"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:nil];
    }


    self.rightButtonEnabled = NO;

    if ([self.comeFromView isEqualToString:@"on_board"]) {
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:KPLocalizedString(@"close") style:UIBarButtonItemStylePlain
                                                                         target:self action:@selector(close:)];
        self.navigationItem.leftBarButtonItem = anotherButton;
    }

    _marksItems = [[NSMutableArray alloc] init];
    arrayData = [[NSMutableArray alloc] init];


    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.allowsMultipleSelection = self.isSpreadTheLove;
    self.tableView.scrollsToTop = YES;
    self.tableView.backgroundView = nil;

    self.tableView.separatorColor = UIColorFromRGB(0xe0e0e0);
    [self.view addSubview:self.tableView];

    [self addSearchHeader];

    if (self.mode == 1) {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"contacts-list", @"source", nil]];

        CFErrorRef error = nil;
        ABAddressBookRef m_addressbook = ABAddressBookCreateWithOptions(NULL, &error);
        ABAddressBookRequestAccessWithCompletion(m_addressbook, ^(bool granted, CFErrorRef error) {
            if (!granted) {
                //DDLogInfo(@"permission denied");

                double delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"permission needed", comment: @"")
                                                                                message:NSLocalizedString(@"you need to grant keepy access to your address book on your device settings", comment: @"")
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                    [ac addAction:[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:ac animated:YES completion:nil];
                });

                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });

                return;
            }
            else {
                [self loadFromAddressbook:m_addressbook];


            }
        });
    }
    else {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"facebook-list", @"source", nil]];

        self.title = @"facebook";
        if (self.isSpreadTheLove) {
            self.title = @"      facebook";
        }

        NSMutableDictionary *existingFBIds = [[NSMutableDictionary alloc] init];
        if (!self.isSpreadTheLove) {
            for (Fan *fan in [[UserManager sharedInstance] myFans]) {
                if (fan.facebookId != nil) {
                    [existingFBIds setValue:fan forKey:fan.facebookId];
                }
            }
        }

        NSMutableArray *arr = [[NSMutableArray alloc] init];
        [[UserManager sharedInstance] requireFacebookFromViewController:self
                                                                success:^(id result) {
                                                                    if (result != nil && [result valueForKey:@"significant_other"] != nil) {
                                                                        NSString *s = @"husband";
                                                                        if ([[UserManager sharedInstance] getMe].parentType.intValue == 1) {
                                                                            s = @"wife";
                                                                        }
                                                                        
                                                                        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
                                                                        [d addEntriesFromDictionary:[result valueForKey:@"significant_other"]];
                                                                        [d setValue:s forKey:@"relationship"];
                                                                        
                                                                        Fan *fan = [existingFBIds valueForKey:[[result valueForKey:@"significant_other"] valueForKey:@"id"]];
                                                                        if (fan != nil) {
                                                                            [d setValue:fan forKey:@"fan"];
                                                                        }
                                                                        
                                                                        [arr addObject:d];
                                                                    }
                                                                    [[UserManager sharedInstance] getFBFamily:^(id result2) {
                                                                        [arr addObjectsFromArray:[result2 valueForKey:@"data"]];
                                                                        [[UserManager sharedInstance] getFBFriends:^(id result3) {
                                                                            [arr addObjectsFromArray:[result3 valueForKey:@"data"]];
                                                                            
                                                                            for (NSInteger i = [arr count] - 1; i > -1; i--) {
                                                                                NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithDictionary:[arr objectAtIndex:i]];
                                                                                Fan *fan = [existingFBIds valueForKey:[d valueForKey:@"id"]];
                                                                                if (fan != nil) {
                                                                                    [d setValue:fan forKey:@"fan"];
                                                                                    [arr replaceObjectAtIndex:i withObject:d];
                                                                                }
                                                                            }
                                                                            [self arrangeFriendsLists:arr];
                                                                        }];
                                                                    }];
                                                                }
                                                           failure:^(id result) {
                                                               [SVProgressHUD dismiss];
                                                           }
         ];
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadFromAddressbook:(ABAddressBookRef)m_addressbook {

    NSMutableDictionary *existingEmails = [[NSMutableDictionary alloc] init];
    if (!self.isSpreadTheLove) {
        for (Fan *fan in [[UserManager sharedInstance] myFans]) {
            if (fan.email != nil) {
                [existingEmails setValue:fan forKey:fan.email];
            }
        }
    }
    NSMutableArray *contactsArray = [NSMutableArray new];
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(m_addressbook);
    CFIndex nPeople = ABAddressBookGetPersonCount(m_addressbook);

    for (int i = 0; i < nPeople; i++) {
        NSMutableDictionary *tempContactDic = [NSMutableDictionary new];
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople, i);
        CFStringRef firstName, lastName;
        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        lastName = ABRecordCopyValue(ref, kABPersonLastNameProperty);

        NSString *strEmail;
        ABMultiValueRef email = ABRecordCopyValue(ref, kABPersonEmailProperty);
        CFStringRef tempEmailref = ABMultiValueCopyValueAtIndex(email, 0);
        strEmail = (__bridge NSString *) tempEmailref;

        if ((strEmail.length > 0 || !self.isSpreadTheLove) && ([strEmail isEqualToString:@""] || [tempContactDic valueForKey:strEmail] == nil) && (firstName != NULL || lastName != NULL)) {
            [tempContactDic setValue:strEmail forKey:@"email"];

            UIImage *pImage = nil;
            CFDataRef imageData = ABPersonCopyImageData(ref);
            if (imageData) {
                pImage = [UIImage imageWithData:(__bridge NSData *) imageData];
                CFRelease(imageData);
            }

            if (firstName == NULL) {
                firstName = (CFStringRef) @"";
            }

            if (lastName == NULL) {
                lastName = (CFStringRef) @"";
            }

            NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
            [d setValue:strEmail forKey:@"email"];

            [d setValue:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"name"];
            // if (pImage != nil)
            // [d setValue:pImage forKey:@"image"];

            if ([existingEmails valueForKey:strEmail] != nil) {
                [d setValue:[existingEmails valueForKey:strEmail] forKey:@"fan"];
            }

            [contactsArray addObject:d];
        }
    }

    self.friendsCount = contactsArray.count;

    [self arrangeFriendsLists:contactsArray];

    CFRelease(m_addressbook);
}

- (void)close:(id)sender {


    [[Mixpanel sharedInstance] track:@"activator-select-medium" properties:[NSDictionary dictionaryWithObject:@"f" forKey:@"launched"]];

    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)backTofeed {

    // [SVProgressHUD  dismiss];

    if (self.campaignId == nil || arrayData.count == 0) {return;}

    if ([self.comeFromView isEqualToString:@"feed"] || [self.comeFromView isEqualToString:@"triggerd"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TLAMassage" object:arrayData userInfo:[NSDictionary dictionaryWithObject:self.campaignId forKey:@"campaignId"]];
        return;

    }

    if ([self.comeFromView isEqualToString:@"on_board"]) {

        [self.navigationController dismissViewControllerAnimated:YES completion:^{

            if (self.delegate != nil) {
                [self.delegate contactShareFinish];
            }
            NSTimeInterval delayInSeconds = 0.50;

            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                [[NSNotificationCenter defaultCenter] postNotificationName:@"TLAMassage" object:arrayData userInfo:[NSDictionary dictionaryWithObject:self.campaignId forKey:@"campaignId"]];
            });

        }];


        return;

    }


    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TLAMassage" object:arrayData userInfo:[NSDictionary dictionaryWithObject:self.campaignId forKey:@"campaignId"]];

    }];
}

- (NSInteger)numberOfRowsInTotal {
    NSInteger sections = [self.tableView numberOfSections];
    NSInteger cellCount = 0;
    for (NSInteger i = 0; i < sections; i++) {
        cellCount += [self.tableView numberOfRowsInSection:i];
    }

    return cellCount;
}

- (void)topBtnTap:(UIButton *)sender {

    /*
    
    NSArray *filteredarray = [[[UserManager sharedInstance] dynamicMessages] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", @"TAF"]];
    
    NSDictionary *dic = nil;
    if ([filteredarray count] > 0) {
        dic = [filteredarray firstObject];
    }*/


    // DDLogInfo(@"%d",self.friendsList.count);

    // NSArray *listtosend = self.selectAll ? (NSArray*)self.friendsList : nil;


    if (onoff.on) {
        for (int i = 0; i < self.friendsList.count; i++) {

            NSArray *item = (NSArray *) [self.friendsList objectAtIndex:i];

            for (NSDictionary *DicItem in item) {
                [arrayData addObject:DicItem];
            }
        }
    }

    else {
        for (int i = 0; i < [self.tableView numberOfSections]; i++) {

            NSArray *item = (NSArray *) [self.friendsList objectAtIndex:i];

            for (int j = 0; j < [self.tableView numberOfRowsInSection:i]; j++) {
                NSUInteger ints[2] = {i, j};
                NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
                UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];

                if (cell.selected) {
                    [arrayData addObject:[item objectAtIndex:j]];
                }
            }
        }
    }


    if ([arrayData count] == 0) {
        return;
    }




    // DDLogInfo(@"%@",[NSString stringWithFormat:@"%d",(int)(((float)arrayData.count/(float)self.friendsCount)*100)]);


    NSInteger totalNumberofRow = [self numberOfRowsInTotal];


    [[Mixpanel sharedInstance] track:@"activator-setup-medium" properties:[NSDictionary dictionaryWithObjectsAndKeys:
            [NSString stringWithFormat:@"%lu", (unsigned long)arrayData.count], @"count-of-contacts-picked",
            [NSString stringWithFormat:@"%ld", (long)totalNumberofRow], @"count-of-contacts-avail",
            [NSString stringWithFormat:@"%ld", (long) (((float) arrayData.count / (float) totalNumberofRow) * 100)], @"ratio-of-contacts-picked-to-avail",
            onoff.on ? @"t" : @"f", @"select-all",
            @"t", @"launched", nil]];


    [self backTofeed];


    //[SVProgressHUD showWithStatus:KPLocalizedString(@"sharing...")];

    /*

    [[ServerComm instance] addContactList:(NSArray*)arrayData withCampaignId:self.campaignId withSuccessBlock:^(id result) {
        
       // DDLogInfo(@"%@",result);
        
        if ([[result objectForKey:@"status"] intValue] == 0) {
            

 [[[Mixpanel sharedInstance] people] increment:@"activator-count-of-emails" by:[NSNumber numberWithInt:arrayData.count]];
            [[Mixpanel sharedInstance] track:@"activator-completed" properties:[NSDictionary dictionaryWithObject:@"t" forKey:@"success"]];

            [self backTofeed];
        }
        
        else{
 [[Mixpanel sharedInstance] track:@"activator-completed" properties:[NSDictionary dictionaryWithObject:@"f" forKey:@"success"]];

        }
        
    } andFailBlock:^(NSError *error) {
       

        [[Mixpanel sharedInstance] track:@"activator-completed" properties:[NSDictionary dictionaryWithObject:@"f" forKey:@"success"]];

        
    }];
    
    
    
*/

}

static NSInteger compareFriends(id left, id right, void *context)
//- (NSComparisonResult) compareFriends:(NSDictionary *)dict2
{
    NSDictionary *dict1 = (NSDictionary *) left;
    NSDictionary *dict2 = (NSDictionary *) right;

    if ([dict1 valueForKey:@"relationship"] != nil && [dict2 valueForKey:@"relationship"] == nil) {
        return NSOrderedAscending;
    } else if ([dict2 valueForKey:@"relationship"] != nil && [dict1 valueForKey:@"relationship"] == nil) {
        return NSOrderedDescending;
    }

    NSString *name1 = @"";
    if ([dict1 valueForKey:@"name"] != nil && [dict1 valueForKey:@"name"] != [NSNull null]) {
        name1 = [(NSString *) [dict1 valueForKey:@"name"] lowercaseString];
    }

    NSString *cmd;
    NSScanner *scanner = [NSScanner scannerWithString:name1];
    [scanner scanUpToString:@" " intoString:&cmd];
    name1 = [[scanner string] substringToIndex:[scanner scanLocation]];

    NSString *name2 = @"";
    if ([dict2 valueForKey:@"name"] != nil && [dict2 valueForKey:@"name"] != [NSNull null]) {
        name2 = [(NSString *) [dict2 valueForKey:@"name"] lowercaseString];
    }

    scanner = [NSScanner scannerWithString:name2];
    [scanner scanUpToString:@" " intoString:&cmd];
    name2 = [[scanner string] substringToIndex:[scanner scanLocation]];

    return [name1 compare:name2];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.friendsList == nil) {
        return 0;
    }
    else {
        return [self.friendsList count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger c = 0;


    if (self.friendsList != nil) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:section];
        if (sarr != nil) {
            c = [sarr count];
        }
    }

    return c;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *cellIdentifier = @"userCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.frame = CGRectMake(0.0, 0.0, 320, 44);


        if (!self.isSpreadTheLove) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

        } else {
            
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;

            UIView *aview = [[UIView alloc] initWithFrame:cell.frame];
            aview.backgroundColor = [UIColor whiteColor];//UIColorFromRGB(0xe2dcda);


            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(265, (44 - 28) / 2, 28, 28)];
            imgView.image = [UIImage imageNamed:@"check_box_checked"];
            imgView.tag = 111;
            [aview addSubview:imgView];

            cell.selectedBackgroundView = aview;


        }


        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
        [cell.contentView addSubview:imgView];


        UIImageView *uncheck = [[UIImageView alloc] initWithFrame:CGRectMake(265, (44 - 28) / 2, 28, 28)];
        uncheck.image = [UIImage imageNamed:@"check_box_unchecked"];
        uncheck.tag = 222;
        [cell.contentView addSubview:uncheck];
        [cell sendSubviewToBack:uncheck];

        float dx = 40;
        if (self.mode == 1) {
            dx = 7;
        }

        float w = 270;
        if (self.isSpreadTheLove) {
            w = 267 - dx;
        }

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(dx, 0, w, 44)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.opaque = NO;
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        lbl.tag = 2;
        lbl.textColor = UIColorFromRGB(0x523c37);
        [cell.contentView addSubview:lbl];
    }

    NSMutableArray *sarr = nil;
    if ((self.friendsList != nil) && ([self.friendsList count] > indexPath.section)) {
        sarr = (NSMutableArray *) [self.friendsList objectAtIndex:indexPath.section];
    }

    if (sarr == nil) {
        return cell;
    }

    cell.tag = indexPath.row;
    NSDictionary *userDict = (NSDictionary *) [sarr objectAtIndex:indexPath.row];


    NSString *s = @"";
    if ([userDict valueForKey:@"fan"] != nil) {
        s = KPLocalizedString(@" (invited)");

    }

    UILabel *lbl = (UILabel *) [cell.contentView viewWithTag:2];
    NSMutableAttributedString* mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", [userDict valueForKey:@"name"], s]];
    
    NSRange range1 = [[mutableAttributedString string] rangeOfString:@" " options:NSCaseInsensitiveSearch];
    range1.length = mutableAttributedString.length - range1.location;
    
    UIFont *afont = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef) afont.fontName, afont.pointSize, NULL);
    if (font && range1.length > 0) {
        [mutableAttributedString addAttribute:(NSString *) kCTFontAttributeName value:(__bridge id) font range:range1];
        
        CFRelease(font);
    }
    
    lbl.attributedText = mutableAttributedString;

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (self.friendsList != nil) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:section];
        if (sarr != nil) {
            NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:0];
            if ([d valueForKey:@"relationship"] != nil) {
                return KPLocalizedString(@"family");
            }
            else {
                NSString *name = [d valueForKey:@"name"];
                return [[name substringToIndex:1] uppercaseString];
            }
        }
    }

    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22.5)];
    aview.backgroundColor = [UIColor colorWithRed:241.0 / 255.0 green:240.0 / 255.0 blue:239.0 / 255.0 alpha:1.0];//UIColorFromRGB(0x897e7a);
    aview.alpha = 1.0;

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 320, 22.5)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = UIColorFromRGB(0x523c37);//[UIColor whiteColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:15];
    [aview addSubview:lbl];

    if (self.friendsList != nil) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:section];
        if (sarr != nil) {
            NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:0];
            if ([d valueForKey:@"relationship"] != nil) {
                lbl.text = KPLocalizedString(@"family");
            }
            else {
                NSString *name = [d valueForKey:@"name"];
                lbl.text = [[name substringToIndex:1] uppercaseString];
            }
        }
    }


    return aview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22.5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];

    UIImageView *imgView = (UIImageView *) [cell viewWithTag:222];

    imgView.alpha = 1;

    [self setTheRightButton];
    [onoff setOn:NO animated:YES];
    // [self setSelectAll:NO];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // [self.txtField resignFirstResponder];
    NSMutableArray *sarr = nil;
    if ((self.friendsList != nil) && ([self.friendsList count] > indexPath.section)) {
        sarr = (NSMutableArray *) [self.friendsList objectAtIndex:indexPath.section];
    }

    UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];

    UIImageView *imgView = (UIImageView *) [cell viewWithTag:222];

    imgView.alpha = 0;;

    [self setTheRightButton];

    if (sarr == nil) {
        return;
    }

    if (self.isSpreadTheLove) {
        return;
    }

    if (self.mode == 1) {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"contacts-list", @"source", @"ok", @"result", nil]];
    } else {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"facebook-list", @"source", @"ok", @"result", nil]];
    }

    if ([sarr count] > indexPath.row) {
        NSDictionary *userDict = (NSDictionary *) [sarr objectAtIndex:indexPath.row];

        EditFanViewController *avc = nil;
        if ([userDict valueForKey:@"fan"] == nil) {
            avc = [[EditFanViewController alloc] initWithUserData:userDict andRelationMode:self.relationMode];
        } else {
            avc = [[EditFanViewController alloc] initWithFan:[userDict valueForKey:@"fan"]];
        }

        [self.navigationController pushViewController:avc animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i = 0; i < [self.friendsList count]; i++) {
        NSMutableArray *sarr = (NSMutableArray *) [self.friendsList objectAtIndex:i];
        NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:0];
        if ([d valueForKey:@"relationship"] != nil) {
            [arr addObject:@"*"];
        }
        else {
            NSString *name = [d valueForKey:@"name"];
            NSString *s = [[name substringToIndex:1] uppercaseString];
            [arr addObject:s];
        }
    }

    return arr;
}


- (void)arrangeFriendsLists:(NSArray *)list; {
    NSString *lastChar = @"";
    NSMutableArray *sarr = nil;

    NSMutableDictionary *existingIds = [[NSMutableDictionary alloc] init];

    self.orgArr = list;

    if (self.originalFriendsList == nil) {
        self.originalFriendsList = [[NSMutableArray alloc] init];
    }

    [self.originalFriendsList removeAllObjects];

    if (self.friendsList == nil) {
        self.friendsList = [[NSMutableArray alloc] init];
    }

    [self.friendsList removeAllObjects];

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObjectsFromArray:list];
    NSMutableArray *discardedItems = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in arr) {
        if ([dict valueForKey:@"name"] == [NSNull null]) {
            [discardedItems addObject:dict];
        }
    }
    [arr removeObjectsInArray:discardedItems];

    [arr sortUsingFunction:compareFriends context:nil];

    for (int i = 0; i < [arr count]; i++) {
        NSDictionary *item = [arr objectAtIndex:i];

        NSString *aid = @"";
        if ([item valueForKey:@"id"] != nil) {
            aid = [item valueForKey:@"id"];

        } else if ([item valueForKey:@"email"] != nil) {
            aid = [item valueForKey:@"email"];
        }

        if ([existingIds valueForKey:aid] != nil && ![aid isEqualToString:@""]) {
            continue;
        }
        [existingIds setValue:@(1) forKey:aid];

        NSString *aname = [item valueForKey:@"name"];
        NSString *s = @"";
        if ([item valueForKey:@"relationship"] != nil) {
            s = @"*";
        } else if ([aname length] > 0) {
            s = [[aname substringToIndex:1] lowercaseString];
        }

        if (![s isEqualToString:lastChar]) {
            if (sarr != nil) {
                [self.originalFriendsList addObject:sarr];
            }

            sarr = [[NSMutableArray alloc] init];
            lastChar = s;
        }

        if (sarr != nil) {
            NSMutableDictionary *newItem = [[NSMutableDictionary alloc] init];
            if ([item valueForKey:@"id"] != nil) {
                [newItem setValue:[item valueForKey:@"id"] forKey:@"fbId"];
            }

            if ([item valueForKey:@"email"] != nil) {
                [newItem setValue:[item valueForKey:@"email"] forKey:@"email"];
            }

            [newItem setValue:[item valueForKey:@"name"] forKey:@"name"];
            if ([item valueForKey:@"relationship"]) {
                [newItem setValue:[item valueForKey:@"relationship"] forKey:@"relationship"];
            }

            if ([item valueForKey:@"image"]) {
                [newItem setValue:[item valueForKey:@"image"] forKey:@"image"];
            }

            if ([item valueForKey:@"fan"] != nil) {
                [newItem setValue:[item valueForKey:@"fan"] forKey:@"fan"];
            }

            [sarr addObject:newItem];

            //DDLogInfo(@"%@", newItem);
        }
    }
    if (sarr != nil) {
        [self.originalFriendsList addObject:sarr];
    }
    
    [self.friendsList addObjectsFromArray:self.originalFriendsList];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [self.tableView reloadData];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [self toggleAllContacts:[self isCheckedByDefault]];
        });
    });
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return YES;
    }

    [self.friendsList removeAllObjects];
    NSString *searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([searchText isEqualToString:@""]) {
        [self.originalFriendsList removeAllObjects];
        [self arrangeFriendsLists:self.orgArr];
        return YES;
    }

    NSMutableArray *newsarr;
    for (int i = 0; i < [self.originalFriendsList count]; i++) {
        newsarr = nil;
        NSMutableArray *sarr = (NSMutableArray *) [self.originalFriendsList objectAtIndex:i];
        for (int k = 0; k < [sarr count]; k++) {
            NSDictionary *d = (NSDictionary *) [sarr objectAtIndex:k];
            NSString *name = [d valueForKey:@"name"];

            NSRange aRange = [[name lowercaseString] rangeOfString:[searchText lowercaseString]];
            if (aRange.location != NSNotFound) {
                if (newsarr == nil) {
                    newsarr = [[NSMutableArray alloc] init];
                    [self.friendsList addObject:newsarr];
                }
                [newsarr addObject:d];
            }
        }
    }

    [self.tableView reloadData];
    return YES;
}

- (void)addSearchHeader {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -45, 320, 44)];
    view.backgroundColor = [UIColor whiteColor];

    UILabel *sTitel = [[UILabel alloc] initWithFrame:CGRectMake(178, 0, 100, 44)];
    sTitel.backgroundColor = [UIColor clearColor];
    sTitel.textColor = [UIColor lightGrayColor];
    sTitel.textAlignment = NSTextAlignmentLeft;
    sTitel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:13];
    sTitel.text = KPLocalizedString(@"select all");

    [view addSubview:sTitel];


    onoff = [[UISwitch alloc] initWithFrame:CGRectMake(240, 7.f, 79, 20)];
    [onoff addTarget:self action:@selector(markall:) forControlEvents:UIControlEventValueChanged];
    
    // TODO: set the default to off
    [onoff setOn:[self isCheckedByDefault]];
    
    
    self.rightButtonEnabled = YES;
    // Set the desired frame location of onoff here
    [view addSubview:onoff];

    /*
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(7, 7, 280.5, 30)];
    img.image = [[UIImage imageNamed:@"search-field"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 28, 0, 54-28)];
    [view addSubview:img];
    
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(40, 12.5, 235, 17)];
    tf.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
    tf.textColor = [UIColor blackColor];
    tf.placeholder = KPLocalizedString(@"search");
    tf.delegate = self;
    [view addSubview:tf];
    self.txtField = tf;*/

    self.tableView.tableHeaderView = nil;
    self.tableView.tableHeaderView = view;

    self.tableView.alwaysBounceVertical = YES;
}

#if TO_BE_DELETED
- (void)sendFBSpreadTheLove:(NSArray *)ids {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[ids componentsJoinedByString:@","], @"to", nil];

    //[FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
    [[UserManager sharedInstance] requireFacebook:^(id result) {
        // Display the requests dialog
        [FBWebDialogs
                presentRequestsDialogModallyWithSession:[FBSession activeSession]
                                                message:KPLocalizedString(@"Hey!\n\nI\'m using the keepy app to organize, enhance, share, admire and save my kids\' art, schoolwork, certificates, lego creations, mementos and MORE!\n\nYou should check it out!  I really think you'll dig it!")
                                                  title:KPLocalizedString(@"join keepy")
                                             parameters:params
                                                handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                    if (error) {
                                                        // Error launching the dialog or sending request.
                                                        DDLogInfo(@"Error sending request.");
                                                    } else {
                                                        if (result == FBWebDialogResultDialogNotCompleted) {
                                                            // User clicked the "x" icon
                                                            DDLogInfo(@"User canceled request.");
                                                        } else {
                                                            // Handle the send request callback
                                                            NSDictionary *urlParams = [GlobalUtils parseURLParams:[resultURL query]];
                                                            if (![urlParams valueForKey:@"request"]) {
                                                                // User clicked the Cancel button
                                                                DDLogInfo(@"User canceled request.");
                                                            } else {
                                                                // User clicked the Send button
                                                                NSString *requestID = [urlParams valueForKey:@"request"];
                                                                DDLogInfo(@"Request ID: %@", requestID);

                                                                [[[Mixpanel sharedInstance] people] increment:@"spread-the-love-invite-counter" by:@([ids count])];


                                                                int64_t delayInSeconds = 1.0;
                                                                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                                                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                                                                    [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
                                                                });

                                                                [[ServerComm instance] addFacebookInviteRequest:requestID withFBIds:[ids componentsJoinedByString:@","] andSuccessBlock:^(id result) {

                                                                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                                                }                                  andFailBlock:^(NSError *error) {
                                                                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

                                                                }];
                                                            }
                                                        }
                                                    }
                                                }];
    }];
}
#endif

- (void)markall:(id)sender {

    UISwitch *sw = (UISwitch *) sender;
    
    [self toggleAllContacts:sw.on];

}


- (void)toggleAllContacts:(BOOL)on{
    
    if (on) {
        DDLogInfo(@"On");
        //[self setSelectAll:YES];
        for (int i = 0; i < [self.tableView numberOfSections]; i++) {
            for (int j = 0; j < [self.tableView numberOfRowsInSection:i]; j++) {
                NSUInteger ints[2] = {i, j};
                NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
                UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
                UIImageView *imgView = (UIImageView *) [cell viewWithTag:222];
                imgView.alpha = 0;
                [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                
            }
        }
        
        [self.tableView scrollsToTop];
        
        
    }
    else {
        DDLogInfo(@"Off");
        // [self setSelectAll:NO];
        for (int i = 0; i < [self.tableView numberOfSections]; i++) {
            for (int j = 0; j < [self.tableView numberOfRowsInSection:i]; j++) {
                NSUInteger ints[2] = {i, j};
                NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
                [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
                UIImageView *imgView = (UIImageView *) [cell viewWithTag:222];
                
                imgView.alpha = 1;
            }
        }
        
        [self.tableView scrollsToTop];
        
    }
    
    
    [self setTheRightButton];
    
}

- (void)setTheRightButton {

    self.rightButtonEnabled = NO;

    for (int i = 0; i < [self.tableView numberOfSections]; i++) {
        for (int j = 0; j < [self.tableView numberOfRowsInSection:i]; j++) {
            NSUInteger ints[2] = {i, j};
            NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
            UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];

            if (cell.selected) {
                self.rightButtonEnabled = YES;
                return;
            }
        }
    }

}

@end
