//
//  FamilySuccessViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//


@protocol TFAContactsSelectorDelegate <NSObject>

@optional
- (void)contactShareFinish;
@end

@protocol FamilySuccessViewControllerDelegate;

@interface FamilySuccessViewController : UIViewController <TFAContactsSelectorDelegate>

@property(nonatomic, weak) id <FamilySuccessViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *kids;
@property(nonatomic, strong) NSArray *family;
@property(nonatomic, assign) CGRect containerViewFrame;

//- (CGFloat)height;

@end

@protocol FamilySuccessViewControllerDelegate <NSObject>

@optional
- (void)doneAddingFamily;

@end

