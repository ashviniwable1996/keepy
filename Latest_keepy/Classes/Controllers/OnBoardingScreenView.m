//
//  OnBoardingScreenView.m
//  Keepy
//
//  Created by Joao Barbosa on 05/08/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnBoardingScreenView.h"

@interface OnBoardingScreenView()
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iphoneWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iphoneHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintForActionLabel;

@end


@implementation OnBoardingScreenView

+ (OnBoardingScreenView*) myView
{
    NSArray* array = [[NSBundle mainBundle] loadNibNamed:@"OnBoardingScreenView" owner:nil options:nil];
    return [array objectAtIndex:0]; // assume that MyView is the only object in the xib
}

- (void) initScreenWithAction:(NSString *) action description:(NSString*) description andImage:(UIImage*) image{
    
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        
        self.iphoneHeightConstraint.constant = 729.0/3;
        self.iphoneWidthConstraint.constant = 373.0/3;
        [self setNeedsDisplay];
        
        
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f ) {
        self.topConstraintForActionLabel.constant = 70;
        [self setNeedsDisplay];
    }
    else if ( [[UIScreen mainScreen] bounds].size.height == 736){
        self.topConstraintForActionLabel.constant = 100;
        [self setNeedsDisplay];
    }
   else if ([[UIScreen mainScreen] bounds].size.height == 812.0f) {
        self.topConstraintForActionLabel.constant = 120;
        [self setNeedsDisplay];
    }
    
    self.actionLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:18];
    self.descriptionLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:14];
    [self.actionLabel setTextColor:UIColorFromRGB(0x43B1C0)];
    
    [self.actionLabel setText:action];
    [self.descriptionLabel setText:description];
    [self.imageView setImage:image];
    [self.imageView setHidden:YES];
}

@end
