//
//  CreateAccountViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/12/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface CreateAccountViewController : UIViewController

@end

@interface WhoCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UISegmentedControl *who;

+ (CGFloat)height;

@end