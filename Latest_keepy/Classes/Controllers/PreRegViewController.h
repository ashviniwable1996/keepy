//
//  PreRegViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 1/14/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@interface PreRegViewController : KPPopViewController

@end
