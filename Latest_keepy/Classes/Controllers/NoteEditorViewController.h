//
//  NoteEditorViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 8/4/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@protocol NoteEditorDelegate

- (void)noteEditorFinished:(NSString *)noteText withNoteImage:(UIImage *)noteImage andAssetId:(NSInteger)assetId;

@end

@interface NoteEditorViewController : KPPopViewController

@property(nonatomic, assign) id <NoteEditorDelegate> delegate;

- (instancetype)initWithKidName:(NSString *)kidName;

- (instancetype)initWithItem:(Item *)item;

- (instancetype)initWithMode:(NSInteger)mode;

@end
