#import <UIKit/UIKit.h>
#import "Item.h"
#import "KPPopViewController.h"
#import "KPLocalComment.h"

@protocol RecordViewControllerDelegate

- (void)recordingSuccessful:(NSData *)fileData withLength:(NSInteger)recordingLength andMode:(NSInteger)mode andPreviewImage:(UIImage *)previewImage;

- (void)recordingCancelled;

- (KPLocalComment*)videoStoryToDelete;

@end

@interface RecordViewController : KPPopViewController

@property(nonatomic, assign) id <RecordViewControllerDelegate> delegate;
@property(nonatomic, assign) UIImage *itemImage;
@property (nonatomic, copy) void (^completion)(NSData* data, NSInteger length, NSInteger mode, UIImage* image);
@property(nonatomic, strong) IBOutlet UILabel *instructionsLabel;

- (instancetype)initWithItem:(KPLocalItem *)item withMode:(NSInteger)mode;

- (instancetype)initWithData:(NSData *)data andLength:(NSInteger)fileLength andMode:(NSInteger)mode;

@end
