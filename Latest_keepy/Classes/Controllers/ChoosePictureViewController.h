//
//  ChoosePictureViewController.h
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol ChoosePictureViewControllerDelegate;

@interface ChoosePictureViewController : UIViewController

@property(nonatomic, weak) id <ChoosePictureViewControllerDelegate> delegate;
@property(nonatomic, weak) IBOutlet UIView *containerView;
@property(nonatomic, weak) UIViewController *previousViewController;
@property(nonatomic, weak) UIViewController *nextViewController;
@property(nonatomic, copy) NSString *nextIdentifier;

- (void)startWithPhoto:(UIImage *)image scale:(float)scale point:(CGPoint)point;

@end

@protocol ChoosePictureViewControllerDelegate <NSObject>

- (void)doneChoosingPicture:(NSDictionary *)selectedPhotosDict;

@end


@interface ChoosePictureSegue : UIStoryboardSegue

@property(nonatomic, copy) NSString *subtype;

@end
