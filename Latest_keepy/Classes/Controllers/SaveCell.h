//
//  SaveCell.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol SaveCellDelegate;

@interface SaveCell : UITableViewCell

@property(nonatomic, weak) id <SaveCellDelegate> delegate;

+ (CGFloat)height;

- (void)success;

- (void)failed;

- (void)disable;

- (void)enable;

@end

@protocol SaveCellDelegate <NSObject>

- (void)cancelled;

- (void)saving;

- (void)saved;

- (void)failed;

@end