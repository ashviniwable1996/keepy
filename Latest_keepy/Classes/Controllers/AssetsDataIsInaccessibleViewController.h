#import <UIKit/UIKit.h>

@interface AssetsDataIsInaccessibleViewController : UIViewController

@property(nonatomic, strong) NSString *explanation;

@end
