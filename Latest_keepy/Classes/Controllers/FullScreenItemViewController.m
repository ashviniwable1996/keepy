//
//  FullScreenItemViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/24/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "FullScreenItemViewController.h"
#import "Story.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Comment.h"
#import "AppDelegate.h"
#import "RecordViewController.h"

@interface FullScreenItemViewController () <AVAudioPlayerDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, RecordViewControllerDelegate>

@property(nonatomic, strong) UIView *topView;
@property(nonatomic, strong) UIView *bottomView;
@property(nonatomic, strong) UIButton *playCommentButton;
@property(nonatomic, strong) UILabel *commentTimelineLabel;
@property(nonatomic, strong) AVAudioPlayer *player;
@property(nonatomic, strong) UIActivityIndicatorView *commentLoadingIndicator;
@property(nonatomic, strong) MPMoviePlayerController *moviePlayer;
@property(nonatomic, strong) UIImageView *itemImageView;
@property(nonatomic, strong) UIView *backgroundView;
@property(nonatomic, strong) UILabel *commentTimeCurrentLabel;
@property(nonatomic, strong) UILabel *commentTimeEndLabel;
@property(nonatomic, strong) UIImageView *progressFiller;
@property(nonatomic, strong) UIButton *knobButton;
@property(nonatomic, strong) NSTimer *audioTimer;
@property(nonatomic, strong) UIImageView *progressBkg;
@property(nonatomic, strong) UIView *videoContainer;
@property(nonatomic, strong) UIButton *thankyouButton;
@property(nonatomic, strong) UIScrollView *commentsScroll;
@property(nonatomic, strong) NSArray *comments;
@property(nonatomic, strong) UIImageView *kidImg;
@property(nonatomic, strong) UILabel *kidNameLabel;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) UILabel *dateLabel;

@property(nonatomic) BOOL infoHidden;
@property(nonatomic) NSInteger mode;
@property(nonatomic) NSInteger currentCommentIndex;
@property(nonatomic) BOOL isClosing;

@end

@implementation FullScreenItemViewController

- (instancetype)initWithItem:(Item *)item withMode:(int)mode {
    return [self initWithItem:item withMode:mode andCommentIndex:-1];
}

- (instancetype)initWithItem:(Item *)item withMode:(int)mode andCommentIndex:(int)commentIndex {
    self = [super initWithNibName:@"FullScreenItemViewController" bundle:nil];
    if (self) {
        self.item = item;
        self.mode = mode;
        //self.currentCommentIndex = commentIndex;
    }
    NSLog(@"FNTRACE FullScreenContainerViewController0 : %@",self.item);

    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.infoHidden = YES;
}

- (void)viewDidLayoutSubviews {

    if (self.backgroundView != nil) {
        return;
    }

    UIWindow *w = [[[UIApplication sharedApplication] delegate] window];
    self.view.frame = CGRectMake(0, 0, w.frame.size.width, w.frame.size.height);

    self.backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    self.backgroundView.backgroundColor = [UIColor blackColor];
    self.backgroundView.alpha = 0;
    self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.backgroundView];

    //image
    self.itemImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, self.initialY, self.view.frame.size.width, self.view.frame.size.height)];
    self.itemImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.itemImageView.backgroundColor = [UIColor clearColor];
    self.itemImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    UIScrollView *imgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    imgScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    imgScrollView.backgroundColor = [UIColor clearColor];
    [imgScrollView setScrollEnabled:YES];
    [imgScrollView setUserInteractionEnabled:YES];
    [imgScrollView addSubview:self.itemImageView];
    [imgScrollView setMinimumZoomScale:1.0f];
    [imgScrollView setMaximumZoomScale:3.0f];
    [imgScrollView setDelegate:self];
    [self.view addSubview:imgScrollView];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap:)];
    gesture.numberOfTapsRequired = 1;
    [imgScrollView addGestureRecognizer:gesture];

    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeTap:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [imgScrollView addGestureRecognizer:swipeGesture];

    swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeTap:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [imgScrollView addGestureRecognizer:swipeGesture];


    //top view
    float ah = 75;

    self.topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, ah)];
    self.topView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, ah)];
    img.backgroundColor = [UIColor blackColor];
    img.alpha = 0.5;
    img.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.topView addSubview:img];

    img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 53, 53)];
    img.image = [UIImage imageNamed:@"kidPicBg"];
    img.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.topView addSubview:img];

    self.kidImg = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13, 47, 47)];
    self.kidImg.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.topView addSubview:self.kidImg];

    self.kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(24 + 72, 15, 230, 15)];
    self.kidNameLabel.backgroundColor = [UIColor clearColor];
    self.kidNameLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.kidNameLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:13];
    self.kidNameLabel.textAlignment = NSTextAlignmentLeft;
    self.kidNameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.topView addSubview:self.kidNameLabel];

    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(24 + 72, 30, 230, 15)];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:13];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.titleLabel.text = self.item.title;
    [self.topView addSubview:self.titleLabel];

    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(24 + 72, 45, 230, 15)];
    self.dateLabel.backgroundColor = [UIColor clearColor];
    self.dateLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.dateLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:11];
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    self.dateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.topView addSubview:self.dateLabel];

    //close button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"close-button"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(closeTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(self.view.frame.size.width - 27 - 17, -4, 44, 44);
    btn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.topView addSubview:btn];

    [self.view addSubview:self.topView];

    //bottomView

    float dh = 0;
    //if (self.item.story != nil || self.mode == 1)
    dh = 52;

    //if ([self.comments count] > 1)
    //{
    dh += 85;
    //}

    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 44 - dh, self.view.frame.size.width, 44 + dh)];
    self.bottomView.autoresizingMask = UIViewAutoresizingFlexibleWidth;// | UIViewAutoresizingFlexibleTopMargin;

    img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44 + dh)];
    img.backgroundColor = [UIColor blackColor];
    img.alpha = 0.5;
    img.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.bottomView addSubview:img];

    //video player
    self.playCommentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
    self.playCommentButton.frame = CGRectMake(10, (52 - 32) / 2, 32, 32);
    [self.playCommentButton addTarget:self action:@selector(playButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.playCommentButton.alpha = 0;
    self.playCommentButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.bottomView addSubview:self.playCommentButton];

    self.commentLoadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.commentLoadingIndicator.hidesWhenStopped = YES;
    self.commentLoadingIndicator.frame = self.playCommentButton.frame;
    //[self.commentLoadingIndicator startAnimating];
    self.commentLoadingIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.bottomView addSubview:self.commentLoadingIndicator];

    self.commentTimeCurrentLabel = [[UILabel alloc] initWithFrame:CGRectMake(43, (52 - 20) / 2, 120, 20)];
    self.commentTimeCurrentLabel.backgroundColor = [UIColor clearColor];
    self.commentTimeCurrentLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.commentTimeCurrentLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
    self.commentTimeCurrentLabel.textAlignment = NSTextAlignmentLeft;
    self.commentTimeCurrentLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    self.commentTimeCurrentLabel.text = @"loading video...";

    [self.bottomView addSubview:self.commentTimeCurrentLabel];

    self.commentTimeEndLabel = [[UILabel alloc] initWithFrame:CGRectMake(275, (52 - 20) / 2, 40, 20)];
    self.commentTimeEndLabel.backgroundColor = [UIColor clearColor];
    self.commentTimeEndLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
    self.commentTimeEndLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
    self.commentTimeEndLabel.textAlignment = NSTextAlignmentCenter;
    self.commentTimeEndLabel.text = @"0:00";
    self.commentTimeEndLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.bottomView addSubview:self.commentTimeEndLabel];
    self.commentTimeEndLabel.alpha = 0;

    self.progressBkg = [[UIImageView alloc] initWithFrame:CGRectMake(85, (52 - 4) / 2, 188, 4)];
    self.progressBkg.image = [[UIImage imageNamed:@"progress-bar-defaultBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 2)];
    self.progressBkg.alpha = 0;
    self.progressBkg.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.bottomView addSubview:self.progressBkg];

    self.progressFiller = [[UIImageView alloc] initWithFrame:CGRectMake(85, (52 - 4) / 2, 0, 4)];
    self.progressFiller.image = [[UIImage imageNamed:@"progress-bar-filler"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 2)];
    self.progressFiller.alpha = 0;
    self.progressBkg.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.bottomView addSubview:self.progressFiller];

    self.knobButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.knobButton setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
    //[btn addTarget:self action:@selector(closeTap:) forControlEvents:UIControlEventTouchUpInside];
    self.knobButton.frame = CGRectMake(85, (52 - 20) / 2, 20, 20);
    self.knobButton.alpha = 0;
    self.knobButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.bottomView addSubview:self.knobButton];

    self.commentsScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 5, self.view.frame.size.width - 20, 80)];
    [self.bottomView addSubview:self.commentsScroll];


    //comment button
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"comment-button-full-screen"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake((160 - 85) / 2, dh + 3 + (44 - 14) / 2, 85, 14);
    btn.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    [self.bottomView addSubview:btn];

    //share button
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"share-button-full-screen"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(160 + (160 - 62) / 2, dh + 3 + (44 - 16) / 2, 62, 16);
    btn.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    [self.bottomView addSubview:btn];

    img = [[UIImageView alloc] initWithFrame:CGRectMake(0, dh, self.view.frame.size.width, 1)];
    img.backgroundColor = UIColorFromRGB(0xffffff);
    img.alpha = 0.7;
    img.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.bottomView addSubview:img];

    img = [[UIImageView alloc] initWithFrame:CGRectMake(159, dh, 1, 44)];
    img.backgroundColor = UIColorFromRGB(0xffffff);
    img.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    img.alpha = 0.7;
    [self.bottomView addSubview:img];

    [self.view addSubview:self.bottomView];

    //Video Container

    self.videoContainer = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 122, 60, 112, 112)];
    self.videoContainer.backgroundColor = [UIColor whiteColor];
    [self.videoContainer.layer setBorderWidth:3];
    [self.videoContainer.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    self.videoContainer.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    self.videoContainer.alpha = 0;
    [self.view addSubview:self.videoContainer];

    UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    av.hidesWhenStopped = YES;
    CGRect aframe = av.frame;
    aframe.origin.x = (self.videoContainer.frame.size.width - aframe.size.width) / 2;
    aframe.origin.y = (self.videoContainer.frame.size.height - aframe.size.height) / 2;
    av.frame = aframe;
    [self.videoContainer addSubview:av];
    [av startAnimating];

    float ax = self.videoContainer.frame.origin.x + (self.videoContainer.frame.size.width - 113) / 2;
    float ay = self.videoContainer.frame.origin.y + 93;
    self.thankyouButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.thankyouButton.frame = CGRectMake(ax, ay, 113, 29);
    [self.thankyouButton setImage:[UIImage imageNamed:@"say-thanks"] forState:UIControlStateNormal];
    self.thankyouButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    self.thankyouButton.hidden = YES;
    [self.view addSubview:self.thankyouButton];

    self.item = self.item;

    if (self.isActive) {
        [self didBecomeActive];
    }

    self.audioTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(audioTimerProc:) userInfo:nil repeats:YES];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)setItem:(Item *)item {
    _item = item;
    NSLog(@"FNTRACE FullScreenContainerViewController1: %@",item);

    if (_item != item) {
        self.itemImageView.image = nil;
    }

    if (item == nil) {
        return;
    }

    int currentItemId = self.item.itemId.intValue;
    NSLog(@"FNTRACE FullScreenContainerViewController2: %@",self.item);

    [[ServerComm instance] downloadAssetFile:self.item.image withSuccessBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.isClosing) {
                return;
            }

            if (currentItemId != self.item.itemId.intValue) {
                return;
            }


            self.itemImageView.image = [UIImage imageWithContentsOfFile:result];
            [UIView animateWithDuration:0.5 animations:^{
                self.itemImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                self.backgroundView.alpha = 1.0;
                self.itemImageView.contentMode = UIViewContentModeScaleAspectFit;
            }                completion:^(BOOL finished) {
            }];
        });
    }                           andFailBlock:^(NSError *error) {

    }];

    Kid *kid = self.item.kids.anyObject;

    if (kid.image == nil) {
        self.kidImg.image = [GlobalUtils getKidDefaultImage:kid.gender.intValue];
    } else {
        int currentItemId = self.item.itemId.intValue;

        [[ServerComm instance] downloadAssetFile:kid.image withSuccessBlock:^(id result) {

            if (currentItemId != self.item.itemId.intValue) {
                return;
            }

            self.kidImg.image = [[UIImage imageWithContentsOfFile:result] imageMaskedWithElipse:CGSizeMake(290, 290)];
        }                           andFailBlock:^(NSError *error) {

        }];
    }

    //Set Texts
    self.kidNameLabel.text = kid.name;
    self.titleLabel.text = self.item.title;
    if (!self.item.title.length) {
        self.titleLabel.text = @"untitled";
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M.d.YY"];
    self.dateLabel.text = [formatter stringFromDate:self.item.itemDate];


    //Comments

    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"commentDate" ascending:NO]];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObjectsFromArray:[self.item.comments.allObjects sortedArrayUsingDescriptors:sortDescriptors]];
    if (self.item.story != nil) {
        [arr insertObject:self.item.story atIndex:0];
    }
    self.comments = arr;

    for (UIControl *control in self.commentsScroll.subviews) {
        [control removeFromSuperview];
    }

    float dh = 0;
    if ([self.comments count] > 0) {
        dh = 85;
    }// + 52;

    self.bottomView.frame = CGRectMake(0, self.view.frame.size.height - 44 - dh, self.view.frame.size.width, 44 + dh);
    self.commentsScroll.frame = CGRectMake(10, 5, self.view.frame.size.width - 20, 80);

    self.playCommentButton.alpha = 0;
    self.commentLoadingIndicator.alpha = 0;
    self.commentTimeCurrentLabel.alpha = 0;
    self.commentTimeEndLabel.alpha = 0;
    self.progressBkg.alpha = 0;
    self.progressFiller.alpha = 0;
    self.knobButton.alpha = 0;
    self.videoContainer.alpha = 0;

    float ax = 0;
    float ay = 0;
    int i = 0;
    for (Comment *comment in self.comments) {
        UIImageView *pimg = [[UIImageView alloc] initWithFrame:CGRectMake(ax, ay, 70, 70)];
        if ([comment isKindOfClass:[Story class]]) {
            int currentItemId = self.item.itemId.intValue;

            [[ServerComm instance] downloadAssetFile:kid.image withSuccessBlock:^(id result) {

                if (currentItemId != self.item.itemId.intValue) {
                    return;
                }

                pimg.image = [UIImage imageWithContentsOfFile:result];
            }                           andFailBlock:^(NSError *error) {

            }];

            //panel
            UIImageView *panelImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 53, 70, 17)];
            panelImg.image = [UIImage imageNamed:@"VO-thumbnail-bar"];
            panelImg.alpha = 0.8;
            [pimg addSubview:panelImg];

        }
        else {
            int currentItemId = self.item.itemId.intValue;
            [[ServerComm instance] downloadAssetFile:comment.previewImage withSuccessBlock:^(id result) {

                if (currentItemId != self.item.itemId.intValue) {
                    return;
                }

                pimg.image = [UIImage imageWithContentsOfFile:result];
            }                           andFailBlock:^(NSError *error) {

            }];

            //panel
            UIImageView *panelImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 53, 70, 17)];
            panelImg.image = [UIImage imageNamed:@"video-thumbnail-bar"];
            panelImg.alpha = 0.8;
            [pimg addSubview:panelImg];

            //new ribbon
            UIImageView *ribbonImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34, 34)];
            ribbonImg.image = [UIImage imageNamed:@"new-video-ribbon"];
            [pimg addSubview:ribbonImg];

            //new label
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(-5.5, -5.5, 34, 34)];
            lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
            lbl.textAlignment = NSTextAlignmentCenter;
            lbl.backgroundColor = [UIColor clearColor];
            lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
            lbl.text = @"new";
            lbl.transform = CGAffineTransformMakeRotation(-M_PI / 4);
            //lbl.hidden = (newItemsCount == 0);
            [pimg addSubview:lbl];

        }
        [self.commentsScroll addSubview:pimg];

        pimg.clipsToBounds = NO;
        pimg.tag = 100 + i;
        [pimg.layer setBorderWidth:2];
        /*
        if (self.currentCommentIndex == i)
            [pimg.layer setBorderColor:[UIColorFromRGB(0xd7ff46) CGColor]];
        else
            [pimg.layer setBorderColor:[[UIColor clearColor] CGColor]];
        */
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentThumbTap:)];
        [pimg addGestureRecognizer:gesture];
        pimg.userInteractionEnabled = YES;

        ax += 76;
        i++;
    }

    self.commentsScroll.contentSize = CGSizeMake([self.comments count] * 76, 80);

}

- (void)setIsActive:(BOOL)isActive {
    if (isActive == _isActive) {
        return;
    }

    _isActive = isActive;

    if (self.isActive) {
        if (self.backgroundView != nil) {
            [self didBecomeActive];
        }
    }
    else {
        [self.player stop];
        [self.moviePlayer stop];

        self.player = nil;

        [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
        self.playCommentButton.tag = 0;
    }

}

- (void)didBecomeActive {
    if (self.commentIndex > 0) {
        int i = self.commentIndex;
        if (self.item.story == nil) {
            i--;
        }

        [[Mixpanel sharedInstance] track:@"Watch Comment" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Feed", @"Source", @(self.commentIndex), @"Number of Video", nil]];

        [self showComment:i];

        self.commentIndex = 0;
    }
    else if (self.item.story != nil) {
        [self showComment:0];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    /*
    [UIView animateWithDuration:0.5 animations:^{
        DDLogInfo(@"%2f %2f %2f %2f", [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.origin.y, [UIScreen mainScreen].bounds.origin.x);
        self.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
        
        self.topView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.topView.frame.size.height);
        //self.itemImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        
    }];
     */

}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {

    /*
    if ([self.comments count] > 1)
    {
        [UIView animateWithDuration:0.5 animations:^{
            CGRect aframe = self.bottomView.frame;
            aframe.size.height += (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))?-85:85;
            aframe.origin.y += (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))?85:-85;
            self.bottomView.frame = aframe;
            self.commentsScroll.alpha = (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))?0:1;
            
        }];
    }
    */
    /*
    float angle = 0;
    
    switch (toInterfaceOrientation) {
        case UIInterfaceOrientationPortrait:
            angle = 0;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            angle = -90;
            break;
        case UIInterfaceOrientationLandscapeRight:
            angle = -270;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            angle = -180;
            break;
    }
    
    [[ServerComm instance] downloadAssetFile:self.item.image withSuccessBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.itemImageView.image = [[UIImage imageWithContentsOfFile:result] imageRotatedByDegrees:angle];
        });
    } andFailBlock:^(NSError *error) {
        
    }];*/
}


#pragma mark - Actions

- (void)closeTap:(id)sender {
    self.isClosing = YES;
    [self.player stop];
    [self.moviePlayer stop];

    //AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //[d.window setRootViewController:d.navController];

    [UIView animateWithDuration:0.5 animations:^{
        self.itemImageView.frame = CGRectMake(10, self.initialY, self.view.frame.size.width, self.view.frame.size.height);
        self.topView.alpha = 0;
        self.backgroundView.alpha = 0;
        self.view.alpha = 0.2;
    }                completion:^(BOOL finished) {

        AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
        [d.window setRootViewController:d.navController];

        //[self.view removeFromSuperview];        
    }];

}

- (void)shareTap:(id)sender {
    [GlobalUtils shareItem:(id)self.item withSource:@"Full Screen" sender:sender];
}

- (void)likeTap:(id)sender {

}

- (void)commentTap:(id)sender {
    RecordViewController *avc = [[RecordViewController alloc] initWithItem:(id)self.item withMode:1];
    avc.delegate = self;
    [self presentViewController:avc animated:YES completion:NULL];
}

- (void)playButtonTap:(id)sender {
    if (self.playCommentButton.tag == 0) {
        
        // Commented below line by Ashvini
        [GlobalUtils outputToSpeaker];

        if (self.item.story == nil || self.currentCommentIndex > 0) {
            if (self.moviePlayer != nil) {
                [self.moviePlayer play];
                self.playCommentButton.tag = 1;
                [self.playCommentButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];
                return;
            }

            //int currentItemId = self.item.itemId.intValue;
            self.playCommentButton.enabled = NO;
            Comment *comment = [self.comments objectAtIndex:self.currentCommentIndex];
            //[[ServerComm instance] downloadAssetFile:comment.video withSuccessBlock:^(id result) {

            //    if (currentItemId != self.item.itemId.intValue)
            //        return;

            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.isClosing || !self.isActive) {
                    return;
                }

                NSURL *aurl = [NSURL URLWithString:[NSString stringWithFormat:@"https://d38jjpbgzdt7c0.cloudfront.net/assets/%d.mov", comment.video.assetId.intValue]];
                [self prepareMovie:aurl];
                [self.commentLoadingIndicator stopAnimating];
                self.playCommentButton.enabled = YES;

                [self.playCommentButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];
                self.playCommentButton.tag = 1;


                [UIView animateWithDuration:0.5 animations:^{
                    self.knobButton.alpha = 1;
                    self.progressFiller.alpha = 1;
                    self.progressBkg.alpha = 1;
                    self.commentTimeEndLabel.alpha = 1;
                    self.playCommentButton.alpha = 1;
                    self.commentTimeCurrentLabel.frame = CGRectMake(43, (52 - 20) / 2, 40, 20);
                    self.commentTimeCurrentLabel.textAlignment = NSTextAlignmentCenter;
                    self.commentTimeCurrentLabel.text = @"0:00";

                }                completion:^(BOOL finished) {

                }];

            });
            //} andFailBlock:^(NSError *error) {

            //}];
        }
        else {
            if (self.player != nil) {
                [self.player play];
                self.playCommentButton.tag = 1;
                [self.playCommentButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];
                return;
            }
            self.playCommentButton.enabled = NO;
            int currentItemId = self.item.itemId.intValue;

            [[ServerComm instance] downloadAssetFile:self.item.story.asset withSuccessBlock:^(id result) {

                if (currentItemId != self.item.itemId.intValue) {
                    return;
                }

                if (self.isClosing || !self.isActive) {
                    return;
                }

                NSError *error;
                self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:result] error:&error];
                self.player.delegate = self;
                [self.player play];

                [self.commentLoadingIndicator stopAnimating];
                self.playCommentButton.enabled = YES;

                [self.playCommentButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];
                self.playCommentButton.tag = 1;

                [UIView animateWithDuration:0.5 animations:^{
                    self.knobButton.alpha = 1;
                    self.progressFiller.alpha = 1;
                    self.progressBkg.alpha = 1;
                    self.commentTimeEndLabel.alpha = 1;
                    self.playCommentButton.alpha = 1;
                    self.commentTimeCurrentLabel.frame = CGRectMake(43, (52 - 20) / 2, 40, 20);
                    self.commentTimeCurrentLabel.textAlignment = NSTextAlignmentCenter;
                    self.commentTimeCurrentLabel.text = @"0:00";

                }                completion:^(BOOL finished) {

                }];

            }                           andFailBlock:^(NSError *error) {

            }];
        }
    }
    else {
        if (self.item.story == nil || self.currentCommentIndex > 0) {
            [self.moviePlayer pause];
        } else {
            [self.player pause];
        }

        [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
        self.playCommentButton.tag = 0;
    }
}

- (void)imageTap:(UITapGestureRecognizer *)gesture {
    self.infoHidden = (!self.infoHidden);
}

- (void)setInfoHidden:(BOOL)infoHidden {
    if (_infoHidden == infoHidden) {
        return;
    }

    _infoHidden = infoHidden;
    [UIView animateWithDuration:0.5 animations:^{
        self.topView.alpha = (self.infoHidden) ? 0 : 1;
        self.bottomView.alpha = (self.infoHidden) ? 0 : 1;

    }                completion:^(BOOL finished) {

    }];

}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
    self.playCommentButton.tag = 0;
}


#pragma mark - gesture delegate

// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}

// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.itemImageView;
}

#pragma mark - AudioTimer

- (void)audioTimerProc:(id)sender {
    int minutes = 0;
    int seconds = 0;
    NSTimeInterval duration = 1;
    NSTimeInterval currentTime = 0;

    if (self.item.story == nil || self.currentCommentIndex > 0) {
        minutes = floor(self.moviePlayer.currentPlaybackTime / 60);
        seconds = round(self.moviePlayer.currentPlaybackTime - minutes * 60);
        duration = self.moviePlayer.duration;

        if (self.moviePlayer.loadState == MPMovieLoadStateUnknown) {
            currentTime = 0;
        }
        else {
            currentTime = self.moviePlayer.currentPlaybackTime;
        }
    }
    else {
        minutes = floor(self.player.currentTime / 60);
        seconds = round(self.player.currentTime - minutes * 60);
        duration = self.player.duration;
        currentTime = self.player.currentTime;
    }

    //if (self.player == nil)
    //    return;    

    if (minutes < 0) {
        minutes = 0;
    }

    if (seconds < 0) {
        seconds = 0;
    }

    if (duration == 0) {
        duration = 1;
    }

    if (currentTime == NAN) {
        currentTime = 0;
    }

    self.commentTimeCurrentLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

    minutes = floor(duration / 60);
    seconds = round(duration - minutes * 60);
    self.commentTimeEndLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

    CGRect aframe = self.progressFiller.frame;
    aframe.size.width = self.progressBkg.frame.size.width * (currentTime / duration);
    self.progressFiller.frame = aframe;

    float ax = self.progressFiller.frame.origin.x + self.progressFiller.frame.size.width - 10;
    if (ax < 85) {
        ax = 85;
    }

    aframe = self.knobButton.frame;
    aframe.origin.x = ax;
    self.knobButton.frame = aframe;
}

- (void)prepareMovie:(NSURL *)fileURL {
    if (self.moviePlayer) {
        [self.moviePlayer setContentURL:fileURL];
    } else {
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
        self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    }

    self.moviePlayer.view.clipsToBounds = NO;

    // Register for the playback finished notification.

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];

    // Movie playback is asynchronous, so this method returns immediately.
    self.moviePlayer.view.frame = CGRectMake(0, 0, self.videoContainer.frame.size.width, self.videoContainer.frame.size.height);
    self.moviePlayer.shouldAutoplay = YES;
    [self.videoContainer addSubview:self.moviePlayer.view];

    self.moviePlayer.initialPlaybackTime = -1.0;
    [self.moviePlayer prepareToPlay];

    [self.moviePlayer play];
}

// When the movie is done,release the controller.
- (void)myMovieFinishedCallback:(NSNotification *)aNotification {
    [UIView animateWithDuration:0.5 animations:^{
        self.videoContainer.alpha = 0;
        float dh = 85;
        self.bottomView.frame = CGRectMake(0, self.view.frame.size.height - 44 - dh, self.view.frame.size.width, 44 + dh);
        self.commentsScroll.frame = CGRectMake(10, 5, self.view.frame.size.width - 20, 80);
        self.playCommentButton.alpha = 0;
        self.commentLoadingIndicator.alpha = 0;
        self.commentTimeCurrentLabel.alpha = 0;
        self.commentTimeEndLabel.alpha = 0;
        self.progressBkg.alpha = 0;
        self.progressFiller.alpha = 0;
        self.knobButton.alpha = 0;
    }                completion:^(BOOL finished) {
        UIImageView *pimg = (UIImageView *) [self.commentsScroll viewWithTag:100 + self.currentCommentIndex];
        [pimg.layer setBorderColor:[[UIColor clearColor] CGColor]];
        self.currentCommentIndex = -1;
    }];


    /*
     //MPMoviePlayerController* moviePlayer=[aNotification object];
     if (self.deleteButton.tag == 0)
     {
     dispatch_async(dispatch_get_main_queue(), ^{
     
     [[NSNotificationCenter defaultCenter] removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:self.moviePlayer];
     [self.moviePlayer setContentURL:nil];
     [self.moviePlayer.view removeFromSuperview];
     self.moviePlayer = nil;
     });
     }
     else
     {
     //[self.recordButton setTitle:@"Play" forState:UIControlStateNormal];
     //self.recordButton.tag = 3;
     
     }
     */
    //[self.previewButton setImage:[UIImage imageNamed:@"big-play-button"] forState:UIControlStateNormal];
    //self.previewButton.tag = 0;

}

- (void)commentThumbTap:(UITapGestureRecognizer *)gesture {
    if (self.item.story == nil || gesture.view.tag > 100) {
        [[Mixpanel sharedInstance] track:@"Watch Comment" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Full Screen", @"Source", @(self.currentCommentIndex), @"Number of Video", nil]];
    } else {
        [[Mixpanel sharedInstance] track:@"Play Story VO"];
    }


    [self showComment:gesture.view.tag - 100];
}

- (void)showComment:(NSUInteger)aindex {
    self.playCommentButton.tag = 0;
    [self.moviePlayer stop];
    [self.moviePlayer.view removeFromSuperview];
    self.moviePlayer = nil;
    [self.player stop];
    self.player = nil;

    UIImageView *pimg = (UIImageView *) [self.commentsScroll viewWithTag:100 + self.currentCommentIndex];
    [pimg.layer setBorderColor:[[UIColor clearColor] CGColor]];

    self.currentCommentIndex = aindex;

    pimg = (UIImageView *) [self.commentsScroll viewWithTag:100 + self.currentCommentIndex];
    [pimg.layer setBorderColor:[UIColorFromRGB(0xd7ff46) CGColor]];

    float dh = 85 + 52;

    [self.commentLoadingIndicator startAnimating];

    [UIView animateWithDuration:0.5 animations:^{
        self.bottomView.frame = CGRectMake(0, self.view.frame.size.height - 44 - dh, self.view.frame.size.width, 44 + dh);
        self.commentsScroll.frame = CGRectMake(10, 53, self.view.frame.size.width - 20, 80);
        //self.playCommentButton.alpha = 1;
        self.commentLoadingIndicator.alpha = 1;
        self.commentTimeCurrentLabel.alpha = 1;
        self.commentTimeCurrentLabel.text = @"loading";
        self.commentTimeEndLabel.alpha = 1;
        self.progressBkg.alpha = 1;
        self.progressFiller.alpha = 1;
        self.knobButton.alpha = 1;
        if (self.item.story == nil || self.currentCommentIndex > 0) {
            self.videoContainer.alpha = 1;
        } else {
            self.videoContainer.alpha = 0;
        }

    }                completion:^(BOOL finished) {

    }];

    [self playButtonTap:self.playCommentButton];
}

#pragma mark - RecordViewControllerDelegate

- (void)recordingSuccessful:(NSData *)fileData {
    NSInteger c = 0;
    if (self.item.comments != nil) {
        c = [self.item.comments.allObjects count];
    }

    self.item = self.item;  //refresh

    [[Mixpanel sharedInstance] track:@"Add Comment" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Full Screen", @"Source", @(c), @"Number of Video", nil]];
}

- (void)recordingCancelled {

}

- (void)recordingSuccessful:(NSData *)fileData withLength:(NSInteger)recordingLength andMode:(NSInteger)mode andPreviewImage:(UIImage *)previewImage {
    NSAssert(NO, @"recordingSuccessful:withLength:andMode:andPreviewImage:");
}

- (KPLocalComment*)videoStoryToDelete {
    NSAssert(NO, @"videoStoryToDelete is not implemented");
    return nil;
}

@end
