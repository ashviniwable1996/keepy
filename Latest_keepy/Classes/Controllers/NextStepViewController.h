//
//  NextStepViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 8/11/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@protocol NextStepDelegate <NSObject>

- (void)nextStepSelected:(NSUInteger)atype;

- (void)nextStepClosed;

@end

@interface NextStepViewController : KPPopViewController

@property(nonatomic, assign) id <NextStepDelegate> delegate;

@end
