//
//  TitlePictureCell.m
//  Keepy
//
//  Created by Troy Payne on 2/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "TitlePictureCell.h"

@interface TitlePictureCell ()

@property(nonatomic, weak) IBOutlet UIButton *imageViewButton;

@end

@implementation TitlePictureCell

+ (CGFloat)height {
    return 81.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    switch (self.titlePictureCellType) {
        default:
        case TitlePictureCellTypeMom: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-mom"] forState:UIControlStateNormal];
            break;
        }

        case TitlePictureCellTypeDad: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-dad"] forState:UIControlStateNormal];
            break;
        }

        case TitlePictureCellTypeGrandma: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-grandma"] forState:UIControlStateNormal];
            break;
        }

        case TitlePictureCellTypeGrandpa: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-grandpa"] forState:UIControlStateNormal];
            break;
        }

        case TitlePictureCellTypeAunt: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-aunt"] forState:UIControlStateNormal];
            break;
        }

        case TitlePictureCellTypeUncle: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-uncle"] forState:UIControlStateNormal];
            break;
        }
    }
}

@end
