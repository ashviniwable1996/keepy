//
//  TagsViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 10/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "KPLocalTag.h"

#import "TagsViewController.h"
#import "Tag.h"

@interface TagsViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSArray *tags;
@property(nonatomic) int mode;
@property(nonatomic, strong) NSArray *selectedTags;

@end

@implementation TagsViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithMode:(int)mode andSelectedTags:(NSArray *)selectedTags {
    self = [super initWithNibName:@"TagsViewController" bundle:nil];
    if (self) {
        self.mode = mode;
        self.selectedTags = selectedTags;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = KPLocalizedString(@"categories");
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:(self.mode == 0) ? [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] : nil];

//    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
//    self.tableView.backgroundColor = [UIColor whiteColor];
//    self.tableView.delegate = self;
//    self.tableView.dataSource = self;
//    self.tableView.showsVerticalScrollIndicator = NO;
//    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    self.tableView.allowsMultipleSelection = (self.mode == 0);
//    self.tableView.scrollsToTop = YES;
//    self.tableView.backgroundView = nil;
//
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    self.tableView.separatorColor = UIColorFromRGB(0xe0e0e0);
//    [self.view addSubview:self.tableView];
//
//    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
//    aview.backgroundColor = UIColorFromRGB(0xe6e4e3);
//    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width -20, 60)];
//    lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
//    lbl.textAlignment = NSTextAlignmentLeft;
//    lbl.backgroundColor = [UIColor clearColor];
//    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
//    if (self.mode == 0) {
//        lbl.text = KPLocalizedString(@"tag your keepies now, so later you can filter them");
//    } else {
//        lbl.text = KPLocalizedString(@"filter by");
//    }
//
//    lbl.numberOfLines = 2;
//    [aview addSubview:lbl];
//    self.tableView.tableHeaderView = aview;
//
//    
//    if (self.mode == 0) {
//        // self.tags = [Tag MR_findAllSortedBy:@"name" ascending:YES];
//        
//        NSArray *allTags = [KPLocalTag fetchAll];
//        NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
//        self.tags = [allTags sortedArrayUsingDescriptors:@[nameSortDescriptor]];
//        
//        
//        
//    } else {
//        KPLocalKid *akid = [[UserManager sharedInstance] selectedKid];
//        if (akid == nil) {
//            
//            NSArray *allTags = [KPLocalTag fetchUsed];
//            NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
//            self.tags = [allTags sortedArrayUsingDescriptors:@[nameSortDescriptor]];
//            
//            // self.tags = [Tag MR_findAllSortedBy:@"name" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"items.@count > 0"]];
//        }
//        else {
//            
//            // NSArray *items = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"kids contains %@", akid]];
//            // self.tags = [Tag MR_findAllSortedBy:@"name" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"ANY items IN %@", items]];
//            
//            NSArray *relevantTags = [KPLocalTag fetchForKid:akid];
//            NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
//            self.tags = [relevantTags sortedArrayUsingDescriptors:@[nameSortDescriptor]];
//        }
//
//    }
//
//    [self.tableView reloadData];
//
//    for (int i = 0; i < [self.tags count]; i++) {
//        KPLocalTag *tag = (KPLocalTag *) [self.tags objectAtIndex:i];
//        if ([self.selectedTags containsObject:@(tag.serverID)]) {
//            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i + self.mode inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
//        }
//    }
//
//    if ([self.selectedTags count] == 0 && self.mode == 1) {
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)topBtnTap:(UIButton *)sender {
    if (sender.tag == 1) {
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (NSIndexPath *indexPath in [self.tableView indexPathsForSelectedRows]) {
            KPLocalObject *tag = [self.tags objectAtIndex:indexPath.row];
            [arr addObject:tag];
        }

        [self.delegate tagViewClosed:arr];

        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.allowsMultipleSelection = (self.mode == 0);
    self.tableView.scrollsToTop = YES;
    self.tableView.backgroundView = nil;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = UIColorFromRGB(0xe0e0e0);
    [self.view addSubview:self.tableView];
    
    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    aview.backgroundColor = UIColorFromRGB(0xe6e4e3);
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width -20, 60)];
    lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    if (self.mode == 0) {
        lbl.text = KPLocalizedString(@"tag your keepies now, so later you can filter them");
    } else {
        lbl.text = KPLocalizedString(@"filter by");
    }
    
    lbl.numberOfLines = 2;
    [aview addSubview:lbl];
    self.tableView.tableHeaderView = aview;
    
    
    if (self.mode == 0) {
        // self.tags = [Tag MR_findAllSortedBy:@"name" ascending:YES];
        
        NSArray *allTags = [KPLocalTag fetchAll];
        NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        self.tags = [allTags sortedArrayUsingDescriptors:@[nameSortDescriptor]];
        
        
        
    } else {
        KPLocalKid *akid = [[UserManager sharedInstance] selectedKid];
        if (akid == nil) {
            
            NSArray *allTags = [KPLocalTag fetchUsed];
            NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            self.tags = [allTags sortedArrayUsingDescriptors:@[nameSortDescriptor]];
            
            // self.tags = [Tag MR_findAllSortedBy:@"name" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"items.@count > 0"]];
        }
        else {
            
            // NSArray *items = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"kids contains %@", akid]];
            // self.tags = [Tag MR_findAllSortedBy:@"name" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"ANY items IN %@", items]];
            
            NSArray *relevantTags = [KPLocalTag fetchForKid:akid];
            NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            self.tags = [relevantTags sortedArrayUsingDescriptors:@[nameSortDescriptor]];
        }
        
    }
    
    [self.tableView reloadData];
    
    for (int i = 0; i < [self.tags count]; i++) {
        KPLocalTag *tag = (KPLocalTag *) [self.tags objectAtIndex:i];
        if ([self.selectedTags containsObject:@(tag.serverID)]) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i + self.mode inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
    
    if ([self.selectedTags count] == 0 && self.mode == 1) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}
#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tags count] + self.mode;
}


// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"TagCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 223, 44)];
        lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:16];
        [cell.contentView addSubview:lbl];

        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        

        UIView *aview = [[UIView alloc] initWithFrame:cell.frame];
        aview.backgroundColor = [UIColor clearColor];//UIColorFromRGB(0xe2dcda);

        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 40, (44 - 15) / 2, 13, 15)];
        imgView.image = [UIImage imageNamed:@"v"];
        [aview addSubview:imgView];

        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(15, cell.contentView.frame.size.height, cell.contentView.frame.size.width, 0.5)];
        lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [aview addSubview:lineView];
        
       cell.selectedBackgroundView = aview;

    }

    [self configureCell:cell withIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    UILabel *titleLbl = (UILabel *) [cell.contentView.subviews objectAtIndex:0];
    if (self.mode == 1 && indexPath.row == 0) {
        titleLbl.text = KPLocalizedString(@"show all");
    } else {
        titleLbl.text = [(Tag *) [self.tags objectAtIndex:indexPath.row - self.mode] name];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[(Tag*)[self.tags objectAtIndex:indexPath.row] name]
    if (self.mode == 1) {
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        if (indexPath.row > 0) {
            [arr addObject:[self.tags objectAtIndex:indexPath.row - self.mode]];
        }

        [self.delegate tagViewClosed:arr];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}


@end
