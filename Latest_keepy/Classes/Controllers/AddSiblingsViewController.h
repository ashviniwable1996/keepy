//
//  AddSiblingsViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol AddSiblingsViewControllerDelegate;

@interface AddSiblingsViewController : UIViewController

@property(nonatomic, weak) id <AddSiblingsViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *kids;

- (CGFloat)height;

@end

@protocol AddSiblingsViewControllerDelegate <NSObject>

- (void)doneAddingSiblings:(NSArray *)kids;

@end