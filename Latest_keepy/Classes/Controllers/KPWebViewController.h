//
//  KPWebViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 5/22/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@interface KPWebViewController : KPPopViewController

- (instancetype)initWithUrl:(NSString *)aURL andTitle:(NSString *)title;

@end
