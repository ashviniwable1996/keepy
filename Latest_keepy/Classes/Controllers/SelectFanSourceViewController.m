//
//  SelectFanSourceViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 6/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "SelectFanSourceViewController.h"
#import "TFAContactsSelectorViewController.h"
#import "EditFanViewController.h"
#import "ContactsSelectorViewController.h"

#import <Twitter/Twitter.h>

#import "Keepy-Swift.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface SelectFanSourceViewController () {

    UIButton *fbShare;
    UIButton *twitterShare;
    UIButton *msgShare;
    UITextView *incentiveText;
    NSDictionary *serverData;
}

@property(nonatomic) NSInteger mode;
@property(nonatomic) NSInteger relationMode;

@end

@implementation SelectFanSourceViewController

- (instancetype)initWithMode:(NSInteger)mode {
    self = [super initWithNibName:@"SelectFanSourceViewController" bundle:nil];
    if (self) {
        self.mode = mode;
    }
    return self;
}

- (instancetype)initWithRelationMode:(NSInteger)mode withRelationMode:(NSInteger)relationMode {
    self = [super initWithNibName:@"SelectFanSourceViewController" bundle:nil];
    if (self) {
        self.mode = mode;
        self.relationMode = relationMode;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"taf right side bar open"];


    if (self.mode == 1) {
        self.title = KPLocalizedString(@"spread the love");
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"close"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];

        // [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"spread-love", @"source", nil]];

    }
    else {
        //self.title = KPLocalizedString(@"add a fan");

        self.title = KPLocalizedString(@"add fan");
        switch (self.relationMode) {
            case 1:
                self.title = KPLocalizedString(@"add partner");
                break;
            case 2:
                self.title = KPLocalizedString(@"add grandparent");
                break;
            case 3:
                self.title = KPLocalizedString(@"add family member");
                break;
            case 4:
                self.title = KPLocalizedString(@"add friend");
                break;
            case 5:
                self.title = KPLocalizedString(@"add aunt or uncle");
                break;
        }

        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"fans"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:nil];

        //  [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"select-fans-source", @"source", nil]];

    }


}

- (void)viewDidLayoutSubviews {
    if ([self.view.subviews count] > 3) {
        return;
    }


    if (self.mode == 1) {


        NSArray *filteredarray = [[[UserManager sharedInstance] dynamicMessages] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(source == %@)", @"TAF"]];

        if ([filteredarray count] > 0) {
            serverData = (NSDictionary *) [filteredarray firstObject];
        }


        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                [serverData objectForKey:@"id"], @"activator-id",
                [serverData objectForKey:@"name"], @"activator-name",
                [serverData objectForKey:@"source"], @"activator-source",
                [serverData objectForKey:@"category"], @"activator-category",
                [[serverData objectForKey:@"callToAction"] objectForKey:@"id"], @"call-to-action-id",
                [[serverData objectForKey:@"campaign"] objectForKey:@"id"], @"campaign-id",
                        nil];


        [[Mixpanel sharedInstance] track:@"activator-call-to-action" properties:params];


        // DDLogInfo(@"%@",serverData);

        //DDLogInfo(@"%@",[serverData objectForKey:@"campaignId"]);


        NSDictionary *callToAction = (NSDictionary *) [serverData objectForKey:@"callToAction"];

        NSString *titleString = [[callToAction objectForKey:@"subject"] objectForKey:@"text"];

        CGSize titleheight = [self text:titleString sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17] thatFitsToSize:CGSizeMake(self.view.frame.size.width, 100)];


        float xpos = (self.view.frame.size.width / 2.0f) - ((self.view.frame.size.width - 30) / 2.0f);


        //sub title
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, 100, self.view.frame.size.width - 30, 50)];
        titleLabel.textColor = [self colorFromHexString:[[callToAction objectForKey:@"subject"] objectForKey:@"color"]];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
        titleLabel.numberOfLines = 2;
        titleLabel.text = titleString;
        [self.view addSubview:titleLabel];

        //sub text
        UILabel *alabel_ = [[UILabel alloc] initWithFrame:CGRectMake(xpos, titleLabel.frame.origin.y + titleheight.height, self.view.frame.size.width - 30, 60)];
        alabel_.textColor = [self colorFromHexString:[[callToAction objectForKey:@"message"] objectForKey:@"color"]];
        alabel_.textAlignment = NSTextAlignmentCenter;
        alabel_.backgroundColor = [UIColor clearColor];
        alabel_.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        alabel_.numberOfLines = 1;
        alabel_.text = [[callToAction objectForKey:@"message"] objectForKey:@"text"];
        [self.view addSubview:alabel_];


        fbShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [fbShare setBackgroundImage:[UIImage imageNamed:@"facebook-on"] forState:UIControlStateNormal];
        fbShare.frame = CGRectMake(self.view.frame.size.width / 2 - 26.5 - 90, 235, 53.0f, 53.0f);
        [fbShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
        fbShare.tag = 0;
        [self.view addSubview:fbShare];


        UILabel *fbShareText = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 26.5 - 90, 290, 53.0f, 20.0f)];
        fbShareText.textAlignment = NSTextAlignmentCenter;
        fbShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
        fbShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
        fbShareText.text = KPLocalizedString(@"Share");
        [self.view addSubview:fbShareText];


        twitterShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [twitterShare setBackgroundImage:[UIImage imageNamed:@"twitter-on"] forState:UIControlStateNormal];
        twitterShare.frame = CGRectMake(self.view.frame.size.width / 2 - 26.5, 235, 53.0f, 53.0f);
        [twitterShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
        twitterShare.tag = 1;
        [self.view addSubview:twitterShare];

        UILabel *tweetShareText = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 26.5, 290, 53.0f, 20.0f)];
        tweetShareText.textAlignment = NSTextAlignmentCenter;
        tweetShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
        tweetShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
        tweetShareText.text = KPLocalizedString(@"Tweet");
        [self.view addSubview:tweetShareText];


        msgShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [msgShare setBackgroundImage:[UIImage imageNamed:@"email-on"] forState:UIControlStateNormal];
        msgShare.frame = CGRectMake(self.view.frame.size.width / 2 - 26.5 + 90, 235, 53.0f, 53.0f);
        [msgShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
        msgShare.tag = 2;
        [self.view addSubview:msgShare];


        UILabel *msgShareText = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 26.5 + 80, 290, 73.0f, 20.0f)];
        msgShareText.textAlignment = NSTextAlignmentCenter;
        msgShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
        msgShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
        msgShareText.text = KPLocalizedString(@"Message");
        [self.view addSubview:msgShareText];


        incentiveText = [[UITextView alloc] initWithFrame:CGRectMake(0, 380, self.view.frame.size.width, 60)];
        incentiveText.editable = NO;
        incentiveText.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        incentiveText.textAlignment = NSTextAlignmentCenter;
        incentiveText.textColor = [self colorFromHexString:[[callToAction objectForKey:@"disclaimer"] objectForKey:@"color"]];
        incentiveText.text = [[callToAction objectForKey:@"disclaimer"] objectForKey:@"text"];
        incentiveText.backgroundColor = [UIColor clearColor];
        [self.view addSubview:incentiveText];


        return;

    }


    float ay = 5;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        ay += 50;
    }


    //text
    UILabel *alabel = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 250) / 2, ay, 250, 180)];
    alabel.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
    alabel.textAlignment = NSTextAlignmentCenter;
    alabel.backgroundColor = [UIColor clearColor];
    alabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    alabel.numberOfLines = 7;
    [self.view addSubview:alabel];

    if (self.mode == 0) {
        ay += 20;
        if (self.relationMode == 1) //partner
        {
            alabel.text = KPLocalizedString(@"add your partner so they can add keepies to your child\'s timeline from their own keepy account.");
        }
        else {
            if ([[[UserManager sharedInstance] myKids] count] > 1) {
                alabel.text = KPLocalizedString(@"Your loved ones will love your keepies. Invite them to be your kids\' “fans” so they can see all your awesome uploads.");
            } else {
                alabel.text = KPLocalizedString(@"Your loved ones will love your keepies. Invite them to be your kid\'s “fans” so they can see all your awesome uploads.");
            }
        }
    }
    else {
        //alabel.text = KPLocalizedString(@"Got friends who keep everything their kids make? Tell them about  keepy (please!)");
        alabel.text = KPLocalizedString(@"Every time you get someone to sign up and upload one keepy, you both get a full month of unlimited keepies for FREE. (For up to 12 people.)");
    }


    UIButton *btn = nil;

    ay += 95;

    //address book
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:self action:@selector(btnTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = 1;
    [btn setTitleColor:UIColorFromRGB(0x4BD64D) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    btn.frame = CGRectMake((self.view.frame.size.width - 280) / 2, ay + 44 + 15, 280, 44);
    [btn setBackgroundImage:[[UIImage imageNamed:@"green-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
    [btn setTitle:KPLocalizedString(@"from my contacts") forState:UIControlStateNormal];
    [self.view addSubview:btn];

    //email
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:self action:@selector(btnTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = 2;
    [btn setTitleColor:UIColorFromRGB(0xFD8F45) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    btn.frame = CGRectMake((self.view.frame.size.width - 280) / 2, ay + 88 + 30, 280, 44);
    [btn setBackgroundImage:[[UIImage imageNamed:@"orange-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
    [btn setTitle:KPLocalizedString(@"enter email address") forState:UIControlStateNormal];
    [self.view addSubview:btn];


    alabel = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 270) / 2, ay + 215, 270, 80)];
    alabel.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
    alabel.textAlignment = NSTextAlignmentCenter;
    alabel.backgroundColor = [UIColor clearColor];
    alabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:13];
    alabel.numberOfLines = 3;
    [self.view addSubview:alabel];

    if (self.mode == 1) {
        alabel.text = KPLocalizedString(@"we do not store or upload your contacts\nwe will never post anything to Facebook unless you ask us to.");
    }
    else {
        alabel.text = KPLocalizedString(@"we do not store or upload your contacts.");
    }

    if (self.mode == 1) {
        //facebook
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn addTarget:self action:@selector(btnTap:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 0;
        [btn setTitleColor:UIColorFromRGB(0x3C5B93) forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:18];
        btn.frame = CGRectMake((self.view.frame.size.width - 260) / 2, ay + 180, 260, 44);
        [btn setBackgroundImage:[[UIImage imageNamed:@"blue-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"f-icon"] forState:UIControlStateNormal];
        [btn setTitle:KPLocalizedString(@"share on") forState:UIControlStateNormal];
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, 150, 0, 0);
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, -40, 0, 0);
        [self.view addSubview:btn];
    }


}

- (void)shareViaFacebook {

    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];
    NSArray *fb = (NSArray *) [campaign objectForKey:@"mediums"];

    NSArray *filteredarray = [fb filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", @"fb"]];
    if ([filteredarray count] > 0) {
        [[UserManager sharedInstance] appInviteFromViewController:self];
    }
}

- (void)shareViaTwitter {


    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];
    NSArray *fb = (NSArray *) [campaign objectForKey:@"mediums"];

    NSArray *filteredarray = [fb filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", @"twitter"]];
    NSDictionary *sdata = nil;
    if ([filteredarray count] > 0) {
        sdata = [(NSDictionary *) [filteredarray firstObject] objectForKey:@"params"];


        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[sdata objectForKey:@"picture"]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {

            [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"preparing")];

        }                                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {

            [SVProgressHUD dismiss];


            if (finished) {
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    [tweetSheet setInitialText:[sdata objectForKey:@"status"]];


                    [tweetSheet addImage:image];

                    if ([sdata objectForKey:@"link"]) {
                        [tweetSheet addURL:[NSURL URLWithString:[sdata objectForKey:@"link"]]];
                    }

                    [[[Mixpanel sharedInstance] people] increment:@"activator-count-of-twitter" by:@(1)];


                    [self presentViewController:tweetSheet animated:YES completion:nil];
                }
                else {
                    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Sorry"
                                                                                message:NSLocalizedString(@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup", comment: @"")
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                    [ac addAction:[UIAlertAction actionWithTitle:@"no" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:ac animated:YES completion:nil];
                }
            }
        }];
    }
}

- (void)shareViaContacts {


    TFAContactsSelectorViewController *avc = [[TFAContactsSelectorViewController alloc] initWithMode:10 * self.mode + 1 andRelationMode:self.relationMode];

//    NSDictionary *localServerData = serverData;
    
    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];
    avc.campaignId = [campaign objectForKey:@"id"];


    if (avc != nil) {
        [self.navigationController pushViewController:avc animated:YES];
    }
    
}

- (void)shareActionClick:(UIButton*)sender {

    NSString *type = nil;

    switch ([sender tag]) {
        case 0:
            type = @"fb";
            break;
        case 1:
            type = @"twitter";
            break;
        case 2:
            type = @"email";
            break;
        default:
            break;
    }

    NSDictionary *params = nil;
    if ([sender tag] == 0 || [sender tag] == 1) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:type, @"medium", @"launched", @"t", nil];
    } else {
        params = [NSDictionary dictionaryWithObjectsAndKeys:type, @"medium", nil];
    }


    [[Mixpanel sharedInstance] track:@"activator-select-medium" properties:params];


    switch ([sender tag]) {
        case 0:
            [self shareViaFacebook];
            break;
        case 1:
            [self shareViaTwitter];
            break;
        case 2:
            [self shareViaContacts];
            break;
        default:
            break;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)topBtnTap:(UIButton *)sender {
    if (self.mode == 1) {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"select-fans-source", @"source", @"cancel", @"result", nil]];

        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"spread-love", @"source", @"cancel", @"result", nil]];

        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)btnTap:(UIButton *)sender {
    if (self.mode == 1) {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"select-fans-source", @"source", @"ok", @"result", nil]];

    }
    else {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"spread-love", @"source", @"ok", @"result", nil]];

    }

    UIViewController *avc = nil;
    switch (sender.tag) {
        case 0:
            if (self.mode == 1) {
                [[Mixpanel sharedInstance] track:@"add spread tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"facebook", @"source", nil]];
                [[UserManager sharedInstance] appInviteFromViewController:self];
            }

            break;
        case 1:
            if (self.mode == 1) {
                [[Mixpanel sharedInstance] track:@"add spread tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"contacts", @"source", nil]];
            }


            avc = [[ContactsSelectorViewController alloc] initWithMode:10 * self.mode + 1 andRelationMode:self.relationMode];
            break;
        case 2:
            if (self.mode == 1) {
                [[Mixpanel sharedInstance] track:@"add spread tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"email", @"source", nil]];

                [GlobalUtils openSpreadLoveEmail:nil];
            }
            else {
                avc = [[EditFanViewController alloc] initWithFan:nil andRelationMode:self.relationMode];
            }

            break;
    }

    if (avc != nil) {
        [self.navigationController pushViewController:avc animated:YES];
    }


}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if ([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                                                 [cleanString substringWithRange:NSMakeRange(0, 1)], [cleanString substringWithRange:NSMakeRange(0, 1)],
                                                 [cleanString substringWithRange:NSMakeRange(1, 1)], [cleanString substringWithRange:NSMakeRange(1, 1)],
                                                 [cleanString substringWithRange:NSMakeRange(2, 1)], [cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if ([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }

    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];

    float red = ((baseValue >> 24) & 0xFF) / 255.0f;
    float green = ((baseValue >> 16) & 0xFF) / 255.0f;
    float blue = ((baseValue >> 8) & 0xFF) / 255.0f;
    float alpha = ((baseValue >> 0) & 0xFF) / 255.0f;

    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

- (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width {
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height;
}

- (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font thatFitsToSize:(CGSize)size {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        CGRect frame = [text boundingRectWithSize:size
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:@{NSFontAttributeName : font}
                                          context:nil];
        return frame.size;
    }
    else {
        return [text sizeWithFont:font thatFitsToSize:size];
    }
}

@end
