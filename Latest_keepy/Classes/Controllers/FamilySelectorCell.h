//
//  FamilySelectorCell.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    FamilySelectorCellTypeSiblings,
    FamilySelectorCellTypeParents,
    FamilySelectorCellTypeGrandParents,
    FamilySelectorCellTypeRelatives
} FamilySelectorCellType;

@protocol FamilySelectorCellDelegate;

@interface FamilySelectorCell : UITableViewCell

@property(nonatomic, weak) id <FamilySelectorCellDelegate> delegate;
@property(nonatomic, assign) FamilySelectorCellType familySelectorCellType;
@property(nonatomic, weak) IBOutlet UILabel *title;

+ (CGFloat)height:(NSString *)title;

- (void)layout;

@end

@protocol FamilySelectorCellDelegate <NSObject>

- (void)leftSelected:(id)sender;

- (void)rightSelected:(id)sender;

@end