//
//  SettingsMenuViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/18/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsMenuViewController : UIViewController
-(void)openPhotoBook:(UIViewController*)viewcontroller isfromdeeplink:(BOOL)isdeeplink;
@property(nonatomic, strong) NSMutableArray *allKidNamesArray;
@property(nonatomic, strong) NSMutableDictionary *allKidOLAssetDictionary;

@end
