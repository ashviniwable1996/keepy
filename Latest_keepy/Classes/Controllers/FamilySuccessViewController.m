//
//  FamilySuccessViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilySuccessViewController.h"


#import "ContactsSelectorViewController.h"
#import "TFAContactsSelectorViewController.h"
#import <Twitter/Twitter.h>
#import "Keepy-Swift.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface FamilySuccessViewController () {
    UIButton *fbShare;
    UIButton *twitterShare;
    UIButton *msgShare;
    UITextView *incentiveText;
    NSDictionary *serverData;
}

@end

@implementation FamilySuccessViewController
- (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width {
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height;
}

- (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font thatFitsToSize:(CGSize)size {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        CGRect frame = [text boundingRectWithSize:size
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:@{NSFontAttributeName : font}
                                          context:nil];
        return frame.size;
    }
    else {
        return [text sizeWithFont:font thatFitsToSize:size];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];


    self.view.backgroundColor = [UIColor colorWithRed:230.0 / 255.0 green:228.0 / 255.0 blue:227.0 / 255.0 alpha:1.0];

    NSArray *filteredarray = [[[UserManager sharedInstance] dynamicMessages] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(source == %@)", @"onboard"]];

    if ([filteredarray count] > 0) {
        serverData = [filteredarray firstObject];
    }

    //DDLogInfo(@"%@",serverData);




    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
            [serverData objectForKey:@"id"], @"activator-id",
            [serverData objectForKey:@"name"], @"activator-name",
            [serverData objectForKey:@"source"], @"activator-source",
            [serverData objectForKey:@"category"], @"activator-category",
            [[serverData objectForKey:@"callToAction"] objectForKey:@"id"], @"call-to-action-id",
            [[serverData objectForKey:@"campaign"] objectForKey:@"id"], @"campaign-id",
                    nil];


    [[Mixpanel sharedInstance] track:@"activator-call-to-action" properties:params];


    NSDictionary *callToAction = (NSDictionary *) [serverData objectForKey:@"callToAction"];


    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    close.frame = CGRectMake(self.containerViewFrame.size.width - 25, 10, 16, 15);
    [close setBackgroundImage:[UIImage imageNamed:@"close-button"] forState:UIControlStateNormal];
    [close addTarget:self action:@selector(doneAddingFamily) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:close];


    NSString *titleString = [[callToAction objectForKey:@"subject"] objectForKey:@"text"];

    CGSize titleheight = [self text:titleString sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17] thatFitsToSize:CGSizeMake(self.containerViewFrame.size.width, 100)];


    float xpos = (self.containerViewFrame.size.width / 2.0f) - ((self.containerViewFrame.size.width - 30) / 2.0f);


    //sub title
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, 50, self.containerViewFrame.size.width - 30, titleheight.height)];
    titleLabel.textColor = [self colorFromHexString:[[callToAction objectForKey:@"subject"] objectForKey:@"color"]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    titleLabel.numberOfLines = 2;
    titleLabel.text = titleString;
    [self.view addSubview:titleLabel];




    //sub text
    UILabel *alabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos, titleLabel.frame.origin.y + titleheight.height, titleLabel.frame.size.width, 60)];
    alabel.textColor = [self colorFromHexString:[[callToAction objectForKey:@"message"] objectForKey:@"color"]];
    alabel.textAlignment = NSTextAlignmentCenter;
    alabel.backgroundColor = [UIColor clearColor];
    alabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    alabel.numberOfLines = 2;
    alabel.text = [[callToAction objectForKey:@"message"] objectForKey:@"text"];
    [self.view addSubview:alabel];


    fbShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [fbShare setBackgroundImage:[UIImage imageNamed:@"facebook-on"] forState:UIControlStateNormal];
    fbShare.frame = CGRectMake(self.containerViewFrame.size.width / 2 - 26.5 - 90, 180, 53.0f, 53.0f);
    [fbShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
    fbShare.tag = 0;
    [self.view addSubview:fbShare];


    UILabel *fbShareText = [[UILabel alloc] initWithFrame:CGRectMake(self.containerViewFrame.size.width / 2 - 26.5 - 90, 235, 53.0f, 20.0f)];
    fbShareText.textAlignment = NSTextAlignmentCenter;
    fbShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
    fbShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    fbShareText.text = KPLocalizedString(@"Share");
    [self.view addSubview:fbShareText];


    twitterShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [twitterShare setBackgroundImage:[UIImage imageNamed:@"twitter-on"] forState:UIControlStateNormal];
    twitterShare.frame = CGRectMake(self.containerViewFrame.size.width / 2 - 26.5, 180, 53.0f, 53.0f);
    [twitterShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
    twitterShare.tag = 1;
    [self.view addSubview:twitterShare];

    UILabel *tweetShareText = [[UILabel alloc] initWithFrame:CGRectMake(self.containerViewFrame.size.width / 2 - 26.5, 235, 53.0f, 20.0f)];
    tweetShareText.textAlignment = NSTextAlignmentCenter;
    tweetShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
    tweetShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    tweetShareText.text = KPLocalizedString(@"Tweet");
    [self.view addSubview:tweetShareText];


    msgShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [msgShare setBackgroundImage:[UIImage imageNamed:@"email-on"] forState:UIControlStateNormal];
    msgShare.frame = CGRectMake(self.containerViewFrame.size.width / 2 - 26.5 + 90, 180, 53.0f, 53.0f);
    [msgShare addTarget:self action:@selector(shareActionClick:) forControlEvents:UIControlEventTouchUpInside];
    msgShare.tag = 2;
    [self.view addSubview:msgShare];


    UILabel *msgShareText = [[UILabel alloc] initWithFrame:CGRectMake(self.containerViewFrame.size.width / 2 - 26.5 + 80, 235, 73.0f, 20.0f)];
    msgShareText.textAlignment = NSTextAlignmentCenter;
    msgShareText.textColor = [UIColor colorWithRed:87.0 / 255.0 green:66.0 / 255.0 blue:63.0 / 255.0 alpha:1];
    msgShareText.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    msgShareText.text = KPLocalizedString(@"Message");
    [self.view addSubview:msgShareText];


    incentiveText = [[UITextView alloc] initWithFrame:CGRectMake(0, 280, self.containerViewFrame.size.width, 60)];
    incentiveText.editable = NO;
    incentiveText.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    incentiveText.textAlignment = NSTextAlignmentCenter;
    incentiveText.textColor = [self colorFromHexString:[[callToAction objectForKey:@"disclaimer"] objectForKey:@"color"]];
    incentiveText.text = [[callToAction objectForKey:@"disclaimer"] objectForKey:@"text"];
    incentiveText.backgroundColor = [UIColor clearColor];
    [self.view addSubview:incentiveText];


}

- (void)contactShareFinish {
    [self doneAddingFamily];
}


- (void)shareViaContacts {

    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];

    TFAContactsSelectorViewController *taf = [[TFAContactsSelectorViewController alloc] initWithMode:11 andRelationMode:0];
    taf.comeFromView = @"on_board";
    taf.delegate = self;
    taf.campaignId = [campaign objectForKey:@"id"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:taf];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)shareViaFacebook {

    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];
    NSArray *fb = (NSArray *) [campaign objectForKey:@"mediums"];


    NSArray *filteredarray = [fb filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", @"fb"]];
    if ([filteredarray count] > 0) {
        [[UserManager sharedInstance] appInviteFromViewController:self];
    }
}

- (void)shareViaTwitter {

    NSDictionary *campaign = (NSDictionary *) [serverData objectForKey:@"campaign"];
    NSArray *fb = (NSArray *) [campaign objectForKey:@"mediums"];

    NSArray *filteredarray = [fb filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", @"twitter"]];
    NSDictionary *sdata = nil;
    if ([filteredarray count] > 0) {
        sdata = [(NSDictionary *) [filteredarray firstObject] objectForKey:@"params"];

        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[sdata objectForKey:@"picture"]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {

            [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"preparing")];

        }                                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {

            [SVProgressHUD dismiss];


            if (finished) {
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    [tweetSheet setInitialText:[sdata objectForKey:@"status"]];


                    [tweetSheet addImage:image];

                    if ([sdata objectForKey:@"link"]) {
                        [tweetSheet addURL:[NSURL URLWithString:[sdata objectForKey:@"link"]]];
                    }

                    [[[Mixpanel sharedInstance] people] increment:@"activator-count-of-twitter" by:@(1)];


                    [self presentViewController:tweetSheet animated:YES completion:nil];
                }
                else {
                    UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Sorry", comment: @"")
                                                                                message:NSLocalizedString(@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup", comment: @"")
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                    [ac addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:ac animated:YES completion:nil];
                }

            }


        }];


    }

}

- (void)shareActionClick:(UIButton*)sender {


    NSString *type = nil;

    switch ([sender tag]) {
        case 0:
            type = @"fb";
            break;
        case 1:
            type = @"twitter";
            break;
        case 2:
            type = @"email";
            break;
        default:
            break;
    }

    NSDictionary *params = nil;
    if ([sender tag] == 0 || [sender tag] == 1) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:type, @"medium", @"launched", @"t", nil];
    } else {
        params = [NSDictionary dictionaryWithObjectsAndKeys:type, @"medium", nil];
    }


    [[Mixpanel sharedInstance] track:@"activator-select-medium" properties:params];


    switch ([sender tag]) {
        case 0:
            [self shareViaFacebook];
            break;
        case 1:
            [self shareViaTwitter];
            break;
        case 2:
            [self shareViaContacts];
            break;
        default:
            break;
    }

}

- (void)doneAddingFamily {
    [self.delegate doneAddingFamily];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if ([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                                                 [cleanString substringWithRange:NSMakeRange(0, 1)], [cleanString substringWithRange:NSMakeRange(0, 1)],
                                                 [cleanString substringWithRange:NSMakeRange(1, 1)], [cleanString substringWithRange:NSMakeRange(1, 1)],
                                                 [cleanString substringWithRange:NSMakeRange(2, 1)], [cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if ([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }

    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];

    float red = ((baseValue >> 24) & 0xFF) / 255.0f;
    float green = ((baseValue >> 16) & 0xFF) / 255.0f;
    float blue = ((baseValue >> 8) & 0xFF) / 255.0f;
    float alpha = ((baseValue >> 0) & 0xFF) / 255.0f;

    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
