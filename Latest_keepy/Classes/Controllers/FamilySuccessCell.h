//
//  FamilySuccessCell.h
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface FamilySuccessCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *body;

+ (CGFloat)height:(NSString *)body;

- (void)layout;

@end
