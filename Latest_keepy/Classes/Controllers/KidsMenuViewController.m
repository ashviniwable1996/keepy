//
//  KidsMenuViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/14/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "CoreData+MagicalRecord.h"
#import "KidsMenuViewController.h"
#import "Album.h"
#import "EditKidViewController.h"
#import "AppDelegate.h"
#import "Comment.h"
#import "Story.h"
#import "KPLocalKid.h"
#import "KPLocalAsset.h"
#import "KPLocalItem.h"
#import "KPLocalKidItem.h"

#import "Keepy-Swift.h"

@interface KidsMenuViewController () <UITableViewDataSource, UITableViewDelegate, EditKidDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSArray *kids;
@property(nonatomic, strong) KPLocalKid *selectedKid;
@property(nonatomic, strong) Album *selectedAlbum;
@property(nonatomic, strong) KPLocalKid *kidToStopFollow;

@end

@implementation KidsMenuViewController

+ (BOOL)isKeepyFeedActive {
    return NO;
}

+ (BOOL)isEveryoneFeedActive {
    return YES;
}

+ (BOOL)isAddChildFeedActive {
    return YES;
}

+ (int)getNonChildrenFeedCount {
    if ([self isEveryoneFeedActive]) {
        return 1;
    } else {
        return 0;
    }
}

+ (int)getNonChildrenRowCount {

    int rowCount = 0;
    if ([self isEveryoneFeedActive]) {
        rowCount++;
    }
    if ([self isAddChildFeedActive]) {
        rowCount++;
    }
    return rowCount;
}

+ (KPLocalKid *)getKidForIndex:(NSIndexPath *)index fromKids:(NSArray *)kids {

    // first of all, see if there are at least two children

    int twoChildrenThreshold = 2;

    if ([self isKeepyFeedActive]) { // the Keepy feed is another child
        twoChildrenThreshold++;
    }

    // we always show the everyone feed, regardless of the number of children
    BOOL isShowingEveryoneFeed = YES; // [self isEveryoneFeedActive] && kids.count >= twoChildrenThreshold;

    // if the everyone feed is active, there is no child at index path zero
    NSInteger childIndex = index.row;
    if (isShowingEveryoneFeed) {
        childIndex--; // because the 0th-row is there
    }

    if (childIndex >= 0 && childIndex < kids.count) {
        return kids[childIndex];
    }

    return nil;

}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kidsRefreshed:) name:KIDS_REFRESHED_NOTIFICATION object:nil];

        [[NSNotificationCenter defaultCenter]
                addObserver:self
                   selector:@selector(itemAdded:)
                       name:ITEM_ADDED_NOTIFICATION
                     object:nil];

        [[NSNotificationCenter defaultCenter]
                addObserver:self
                   selector:@selector(changeKid:)
                       name:CHANGE_KID_NOTIFICATION
                     object:nil];

        [[NSNotificationCenter defaultCenter]
                addObserver:self
                   selector:@selector(kidDeleted:)
                       name:DELETE_KID_NOTIFICATION
                     object:nil];

        if ([[UserManager sharedInstance] isLoggedIn]) {
            [self reloadData];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = UIColorFromRGB(0xE4E2E1);

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 1, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.scrollsToTop = NO;
    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);

    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    self.tableView.separatorColor = [UIColor clearColor];//[UIColor colorWithRed:0.447 green:0.361 blue:0.325 alpha:1];
    [self.view addSubview:self.tableView];

    [self reloadData];

    [self.revealController setMinimumWidth:123.0f maximumWidth:123.0f forViewController:self];

    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
            initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 2.0; //seconds
    [self.tableView addGestureRecognizer:lpgr];

    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    bar.barTintColor = UIColorFromRGB(0xE4E2E1);
    [self.view addSubview:bar];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadData]; // TODO: re-enable data reload on view appearance
}

- (void)viewDidLayoutSubviews {
    //self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData {
    if (self.specificKids == nil) {
        // NSMutableArray *tkids = [[[NSMutableArray alloc] initWithArray:[Kid MR_findAllSortedBy:@"isMine,kidId" ascending:NO]] autorelease];
        NSArray *unsortedKids = [KPLocalKid fetchAll];
        
        NSSortDescriptor *ageSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"birthdate" ascending:NO];
        NSSortDescriptor *isMineSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isMine" ascending:NO];
        NSSortDescriptor *serverIDSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"serverID" ascending:NO];
        NSSortDescriptor *localIDSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"identifier" ascending:NO];

        NSMutableArray *tkids = [unsortedKids sortedArrayUsingDescriptors:@[isMineSortDescriptor, ageSortDescriptor, serverIDSortDescriptor, localIDSortDescriptor]].mutableCopy;

        [tkids removeObject:[[UserManager sharedInstance] keepyKid]];

        if ([self.class isKeepyFeedActive]) {
            [tkids addObject:[[UserManager sharedInstance] keepyKid]];
        } else {

            for (KPLocalKid *currentKid in tkids.copy) {
                if (currentKid.serverID == [KPLocalKid getKeepyKidID]) {
                    [tkids removeObject:currentKid];
                    break;
                }
            }

        }

        self.kids = tkids;
    } else {
        self.kids = self.specificKids;
    }

    if (self.selectedKid == nil) {

        // self.selectedKid = [self.]


        // no clue what the code below was supposed to do


        /* if ([self.kids count] > 0 && [self.kids count] < 3) {
            self.selectedKid = (KPLocalKid *) self.kids[0];
        }
        else if ([[UserManager sharedInstance] isAfterLogin]) {
            if ([self.kids count] > 2) {
                self.selectedKid = nil;
            }
        }*/


    }
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 123;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger kidsCount = self.kids.count;
    NSInteger additionalRowCount = [self.class getNonChildrenRowCount];
    NSInteger rowCount = kidsCount + additionalRowCount;
    NSLog(@"row count: %ld", (long)rowCount);
    return rowCount;

    if (self.specificKids == nil) {
        if ([self.kids count] > 2) {
            return [self.kids count] + 2;
        } else {
            return [self.kids count] + 1;
        }
    } else {
        return self.kids.count;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}


// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"KidCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

        UIImageView *bkgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 123, 123)];
        bkgImg.backgroundColor = UIColorFromRGB(0x0cc3d9);
        [cell.contentView addSubview:bkgImg];

        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 112.5, 112.5)];
        img.contentMode = UIViewContentModeScaleAspectFill;
        img.clipsToBounds = YES;
        [cell.contentView addSubview:img];

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, 123, 20)];
        lbl.textColor = [UIColor whiteColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.shadowOffset = CGSizeMake(1.0, 1.0);
        lbl.shadowColor = [UIColor blackColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
        [cell.contentView addSubview:lbl];

        img = [[UIImageView alloc] initWithFrame:CGRectMake(7, 7, 31, 30)];
        img.image = [UIImage imageNamed:@"button-prime-actionBG"];
        img.hidden = YES;
        [cell.contentView addSubview:img];

        lbl = [[UILabel alloc] initWithFrame:CGRectMake(7, 7, 31, 30)];
        lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
        lbl.text = @"";
        [cell.contentView addSubview:lbl];

        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 122, 123, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xc8beba);
        [cell.contentView addSubview:lineView];

        UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        av.frame = CGRectMake(5 + (112.5 - 20) / 2, 5 + (112.5 - 20) / 2, 20, 20);
        av.hidesWhenStopped = YES;
        [cell.contentView addSubview:av];

    }

    cell.backgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    cell.backgroundView.backgroundColor = UIColorFromRGB(0xE4E2E1);

    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    [self configureCell:cell withIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    UILabel *titleLbl = (UILabel *) cell.contentView.subviews[2];
    UIImageView *img = (UIImageView *) cell.contentView.subviews[1];
    UIImageView *bkgImg = (UIImageView *) cell.contentView.subviews[0];
    UIImageView *notificationImg = (UIImageView *) cell.contentView.subviews[3];
    UILabel *notificationLbl = (UILabel *) cell.contentView.subviews[4];
    UIActivityIndicatorView *av = (UIActivityIndicatorView *) cell.contentView.subviews[6];

    bkgImg.hidden = YES;
    notificationImg.hidden = YES;
    notificationLbl.hidden = YES;

    KPLocalKid *kid = nil;
//    int nonChildrenFeedCount = [self.class getNonChildrenFeedCount];
//    int childrenCount = self.kids.count - nonChildrenFeedCount;
    NSInteger rowCount = self.kids.count + [self.class getNonChildrenRowCount];
    NSInteger lastRowIndex = rowCount - 1;

    // check if it is the last row to show the add button
    if (indexPath.row == lastRowIndex) {
        titleLbl.text = KPLocalizedString(@"+add");
        titleLbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        titleLbl.shadowColor = [UIColor whiteColor];
        img.image = [UIImage imageNamed:@"add-kid-kidsNav"];
        titleLbl.alpha = 1.0;
        img.alpha = 1.0;
        return;
    }

        // other rows, including "everyone" and "Keepy feed"

    else /*if (indexPath.row > 0 )*/
    {

        kid = [self.class getKidForIndex:indexPath fromKids:self.kids];



        // the index of the current child is the row index
        // but if there are more than two children, the index is one less


        /* 
        // the index of the child to be shown here
        NSUInteger aindex = indexPath.row;
        
        // if there are more than two kids in the array, including either keepy or everybody (TODO: find out which!)
        if ([self.kids count] > 2){
            aindex--; // we decreate the index of the kid to be references here
        }
        
        if (aindex < [self.kids count]){
            kid = (Kid*)[self.kids objectAtIndex:aindex];
        }
         */
    }

    img.image = nil;
    if (kid == nil) {
        titleLbl.text = KPLocalizedString(@"everyone");
        //titleLbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        //titleLbl.shadowColor = [UIColor whiteColor];
        titleLbl.textColor = [UIColor whiteColor];
        titleLbl.shadowColor = [UIColor blackColor];

        img.image = [UIImage imageNamed:@"everyone-album"];
    }
    else {
        titleLbl.text = [NSString stringWithFormat:@"%@", kid.name];
        titleLbl.textColor = [UIColor whiteColor];
        titleLbl.shadowColor = [UIColor blackColor];

        KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:kid.imageAssetID];

        if (imageAsset == nil) {
            img.image = [GlobalUtils getKidDefaultImage:kid.gender];
        }
        else {
            [av startAnimating];
            //[img setImageWithAsset:kid.image];

            /*
            [[ServerComm instance] downloadAssetFile:kid.image withSuccessBlock:^(id result) {
                //img.image = [[[UIImage imageWithContentsOfFile:result] resizedImage:CGSizeMake(124, 124) interpolationQuality:kCGInterpolationDefault] imageMaskedWithElipse:CGSizeMake(123, 123)];
                img.image = [[UIImage imageWithContentsOfFile:result] imageMaskedWithElipse:CGSizeMake(290, 290)];
                
                [av stopAnimating];
            } andFailBlock:^(NSError *error) {
                
            }];*/
            [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

                img.image = [img.image imageMaskedWithElipse:CGSizeMake(290, 290)];

                [av stopAnimating];

            }];
        }

        /*
        int c = [kid unreadItemsCount];
        if (c > 0)
        {
            notificationLbl.text = [NSString stringWithFormat:@"%d", c];
            notificationLbl.hidden = NO;
            notificationImg.hidden = NO;
        }*/
    }

    bkgImg.hidden = YES;
    if (self.selectedKid == kid || self.selectedKid.identifier == kid.identifier) {
        bkgImg.hidden = NO;
    }
    // bkgImg.hidden = (self.selectedKid != kid);

    float alpha = 1.0;
    titleLbl.alpha = alpha;
    img.alpha = alpha;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // [SVProgressHUD showWithStatus:@"loading kid…"];
    
    

//    int nonChildrenFeedCount = [self.class getNonChildrenFeedCount];
//    int childrenCount = self.kids.count - nonChildrenFeedCount;
    NSInteger rowCount = self.kids.count + [self.class getNonChildrenRowCount];
    NSInteger lastRowIndex = rowCount - 1;

    if (indexPath.row == lastRowIndex) {
        [self addkidTap:nil];
        return;
    } else {

        // this takes some time

        self.selectedKid = [self.class getKidForIndex:indexPath fromKids:self.kids];

    }

    /* else if (indexPath.row == 0 && [self.kids count] > 2)
    {
        self.selectedKid = nil;
    }
    else
    {
        NSUInteger aindex = indexPath.row;
        if ([self.kids count] > 2)
            aindex--;
        
        self.selectedKid = (Kid*)[self.kids objectAtIndex:aindex];
    } */

    [self.tableView reloadData];

//    UIViewController *fvc = self.revealController.frontViewController;
    [self.revealController showViewController:self.revealController.frontViewController];



    // [SVProgressHUD dismiss];

}

#pragma mark - Actions

- (void)setSelectedKid:(KPLocalKid *)selectedKid {

    // temporarily forget about the necessity of only listening to changes

    if (YES || _selectedKid != selectedKid || [[UserManager sharedInstance] isAfterLogin]) {
        [[UserManager sharedInstance] setIsAfterLogin:NO];

        _selectedKid = selectedKid;
        [[UserManager sharedInstance] setSelectedKid:selectedKid];
        [self.tableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:KID_SELECT_NOTIFICATION object:self.selectedKid];
    }
}

- (void)addkidTap:(UIButton *)btn {
    EditKidViewController *avc = [[EditKidViewController alloc] initWithKid:nil];
    avc.delegate = self;

    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController pushViewController:avc animated:YES];
    //presentModalViewController:avc animated:YES];
    //[self.revealController presentModalViewController:avc animated:YES];
}

#pragma mark - EditKidDelegate

- (void)kidAdded:(KPLocalKid *)kid {
    /*
    int age = 0;
    if (kid.birthdate > 0) {
        NSDate *birthday = [NSDate dateWithTimeIntervalSince1970:kid.birthdate];
        NSDate *today = [NSDate date];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear fromDate:birthday toDate:today options:0];
        age = components.year;
    }
    */

    self.selectedKid = kid;

    [GlobalUtils fireTrigger:TRIGGER_TYPE_ADDKID];

    [[NSNotificationCenter defaultCenter] postNotificationName:KID_ADDED_NOTIFICATION object:nil];

    [self reloadData];
}

#pragma mark - KidsRefreshedNotification

- (void)kidsRefreshed:(NSNotification *)notification {
    [self reloadData];


    // this is where we notify the ItemsViewController that new information is there
    // so we say
    self.selectedKid = self.selectedKid; // this sends a notification that the ItemsViewController listens to

    // that way, without changing the active kid, the data are still reloaded



    [[Mixpanel sharedInstance] registerSuperProperties:@{@"number of kids" : @([[[UserManager sharedInstance] myKids] count])}];


}

- (void)itemAdded:(NSNotification *)notification {
    KPLocalItem *item = (KPLocalItem *) notification.object;

    if (item == nil) {
        return;
    }

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:item];

    if(itemKidIDs.count > 0) {

        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];
        KPLocalKid *firstKid = itemKids[0];

        if (self.selectedKid != nil) {
            self.selectedKid = firstKid;
        }

    }


}

- (void)changeKid:(NSNotification *)notification {
    self.selectedKid = (KPLocalKid *) notification.object;
}

- (void)kidDeleted:(NSNotification *)notification {
    if (self.selectedKid.serverID == [notification.object intValue]) {
        self.selectedKid = nil;

        [self reloadData];


        [[Mixpanel sharedInstance] registerSuperProperties:@{@"number of kids" : @([[[UserManager sharedInstance] myKids] count])}];


    }
}

#pragma mark - tableview long press

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (self.kidToStopFollow != nil) {
        return;
    }

    CGPoint p = [gestureRecognizer locationInView:self.tableView];

    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath == nil) {
        return;
    }

    if (indexPath.row == (([self.kids count] > 2) ? [self.kids count] + 1 : [self.kids count])) {
        return;
    }
    else if (indexPath.row == 0 && [self.kids count] > 2) {
        return;
    }
    else {
        NSUInteger aindex = indexPath.row;
        if ([self.kids count] > 2) {
            aindex--;
        }

        self.kidToStopFollow = (KPLocalKid *) self.kids[aindex];
        if (self.kidToStopFollow.isMine) {
            self.kidToStopFollow = nil;
            return;
        }

        UIAlertController* ac = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@ %@", KPLocalizedString(@"stop following"), self.kidToStopFollow.name]
                                                                    message:NSLocalizedString(@"are you sure?", comment: @"")
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"no" style:UIAlertActionStyleCancel handler:nil]];
        [ac addAction:[UIAlertAction actionWithTitle:@"unfollow" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self unfollow];
        }]];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

- (void)unfollow {
    if (self.kidToStopFollow == nil) {
        return;
    }

        [SVProgressHUD showWithStatus:KPLocalizedString(@"deleting")];
        [[ServerComm instance] stopFollowKid:self.kidToStopFollow.serverID withSuccessBlock:^(id result) {

            if ([[result valueForKey:@"status"] intValue] == 0) {
                [[Mixpanel sharedInstance] track:@"stop follow kid"];


                NSArray *items = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"kids contains %@", self.kidToStopFollow]];

                for (Item *item in items) {
                    [item.image MR_deleteEntity];
                    [item.originalImage MR_deleteEntity];
                    [item.story MR_deleteEntity];

                    NSArray *comments = [Comment MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"item == %@", item]];
                    for (Comment *comment in comments) {
                        [comment MR_deleteEntity];
                    }

                    [item MR_deleteEntity];
                }

                if ([[UserManager sharedInstance] selectedKid] == self.kidToStopFollow) {
                    [[UserManager sharedInstance] setSelectedKid:nil];
                }

                NSInteger kidId = self.kidToStopFollow.serverID;

                [self.kidToStopFollow remove];
                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {


                    [SVProgressHUD dismiss];

                    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_DELETED_NOTIFICATION object:@(kidId)];

                    [[NSNotificationCenter defaultCenter] postNotificationName:DELETE_KID_NOTIFICATION object:nil];

                    [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];

                    self.kidToStopFollow = nil;

                }];

            }
            else {
                self.kidToStopFollow = nil;
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error deleteing kid")];
            }

        }                       andFailBlock:^(NSError *error) {
            self.kidToStopFollow = nil;
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];

        }];
}


@end
