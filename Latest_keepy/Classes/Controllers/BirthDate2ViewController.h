//
//  BirthDate2ViewController.h
//  Keepy
//
//  Created by Troy Payne on 3/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface BirthDate2ViewController : UIViewController

@property(nonatomic, strong) NSString *theTitleString;
@property(nonatomic, strong) NSDate *birthDateDate;

@end
