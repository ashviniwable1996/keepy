//
//  AddKidViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/12/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddKidViewController.h"
#import "InputCell.h"
#import "AddPhotoCell.h"
#import "BirthDateCell.h"

@interface AddKidViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AddKidViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    [self.tableView registerNib:[UINib nibWithNibName:@"InputCell" bundle:nil] forCellReuseIdentifier:@"InputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AddPhotoCell" bundle:nil] forCellReuseIdentifier:@"AddPhotoCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BirthDateCell" bundle:nil] forCellReuseIdentifier:@"BirthDateCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *InputCellIdentifier = @"InputCell";
    static NSString *AddPhotoCellIdentifier = @"AddPhotoCell";
    static NSString *BirthDateCellIdentifier = @"BirthDateCell";
    if (indexPath.row == 0) {
        InputCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
        cell.input.placeholder = @"name";
        return cell;
    } else if (indexPath.row == 1) {
        AddPhotoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:AddPhotoCellIdentifier];
        return cell;
    } else if (indexPath.row == 2) {
        BirthDateCell *cell = [self.tableView dequeueReusableCellWithIdentifier:BirthDateCellIdentifier];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [InputCell height];
    } else if (indexPath.row == 1) {
        return [AddPhotoCell height];
    } else if (indexPath.row == 1) {
        return [BirthDateCell height];
    }
    return 0.0f;
}

- (IBAction)next:(id)sender {
    [self performSegueWithIdentifier:@"ChooseMemory" sender:self];
}

@end