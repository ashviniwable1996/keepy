//
//  AccountViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 5/19/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KPPopViewController.h"
#import "KPPlansView.h"

@interface AccountViewController : KPPopViewController <KPPlansViewDelegate>

- (void)enterCouponCode:(NSString *)couponCode;

@end
