//
//  AddSiblingViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddSiblingViewController.h"
#import "TitleCell.h"
#import "InputCell.h"
#import "AddPhotoCell.h"
#import "BirthDateCell.h"
#import "BirthDateSetCell.h"
#import "SaveCell.h"
#import "ChoosePictureViewController.h"
#import "KPLocalKid.h"
#import "KPLocalKidItem.h"
#import "ItemsViewController.h"

@interface AddSiblingViewController () <UITableViewDataSource, UITableViewDelegate, AddPhotoCellDelegate, UITextFieldDelegate, BirthDateCellDelegate, BirthDateSetCellDelegate, SaveCellDelegate, ChoosePictureViewControllerDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, weak) UITextField *nameField;
@property(nonatomic, strong) NSDate *birthDateDate;
@property(nonatomic, weak) SaveCell *saveCell;
@property(nonatomic, weak) AddPhotoCell *addPhotoCell;
@property(nonatomic, strong) UIImage *picture;
@property(nonatomic, strong) UIImage *originalKidImage;
@property(nonatomic) float photo_scale;
@property(nonatomic) float photo_x;
@property(nonatomic) float photo_y;

@end

@implementation AddSiblingViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-add-sibling-open"];


    self.kids = nil;
    [self.tableView registerNib:[UINib nibWithNibName:@"SpacerCell" bundle:nil] forCellReuseIdentifier:@"SpacerCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TitleCell" bundle:nil] forCellReuseIdentifier:@"TitleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"InputCell" bundle:nil] forCellReuseIdentifier:@"InputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AddPhotoCell" bundle:nil] forCellReuseIdentifier:@"AddPhotoCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BirthDateCell" bundle:nil] forCellReuseIdentifier:@"BirthDateCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BirthDateSetCell" bundle:nil] forCellReuseIdentifier:@"BirthDateSetCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SaveCell" bundle:nil] forCellReuseIdentifier:@"SaveCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *SpacerCellIdentifier = @"SpacerCell";
    static NSString *TitleCellIdentifier = @"TitleCell";
    static NSString *InputCellIdentifier = @"InputCell";
    static NSString *AddPhotoCellIdentifier = @"AddPhotoCell";
    static NSString *BirthDateSetCellIdentifier = @"BirthDateSetCell";
    static NSString *BirthDateCellIdentifier = @"BirthDateCell";
    static NSString *SaveCellIdentifier = @"SaveCell";

    if (indexPath.row == 0) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 1) {
        TitleCell *cell = [self.tableView dequeueReusableCellWithIdentifier:TitleCellIdentifier];
        if (self.addSiblingViewControllerType == AddSiblingViewControllerTypeBrother) {
            cell.title.text = @"add brother";
        } else if (self.addSiblingViewControllerType == AddSiblingViewControllerTypeSister) {
            cell.title.text = @"add sister";
        }
        return cell;
    } else if (indexPath.row == 2) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 3) {
        InputCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
        cell.input.placeholder = @"name";
        self.nameField = cell.input;
        self.nameField.delegate = self;
        self.nameField.returnKeyType = UIReturnKeyDone;
        self.nameField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        self.nameField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.nameField.spellCheckingType = UITextSpellCheckingTypeNo;
        return cell;
    } else if (indexPath.row == 4) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 5) {
        AddPhotoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:AddPhotoCellIdentifier];
        cell.delegate = self;
        if (self.addSiblingViewControllerType == AddSiblingViewControllerTypeBrother) {
            cell.addPhotoCellType = AddPhotoCellTypeBoy;
        } else if (self.addSiblingViewControllerType == AddSiblingViewControllerTypeSister) {
            cell.addPhotoCellType = AddPhotoCellTypeGirl;
        }
        cell.picture = [self.picture imageMaskedWithElipse:CGSizeMake(290, 290)];
        self.addPhotoCell = cell;
        [cell layout];
        return cell;
    } else if (indexPath.row == 6) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 7) {
        if (self.birthDateDate) {
            BirthDateSetCell *cell = [self.tableView dequeueReusableCellWithIdentifier:BirthDateSetCellIdentifier];
            cell.delegate = self;
            cell.birthDateDate = self.birthDateDate;
            return cell;
        } else {
            BirthDateCell *cell = [self.tableView dequeueReusableCellWithIdentifier:BirthDateCellIdentifier];
            cell.delegate = self;
            return cell;
        }
    } else if (indexPath.row == 8) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 9) {
        SaveCell *cell = [self.tableView dequeueReusableCellWithIdentifier:SaveCellIdentifier];
        cell.delegate = self;
        self.saveCell = cell;
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 13.0f;
    } else if (indexPath.row == 1) {
        return [TitleCell height];
    } else if (indexPath.row == 2) {
        return 23.0f;
    } else if (indexPath.row == 3) {
        return [InputCell height];
    } else if (indexPath.row == 4) {
        return 25.0f;
    } else if (indexPath.row == 5) {
        return [AddPhotoCell height];
    } else if (indexPath.row == 6) {
        return 18.0f;
    } else if (indexPath.row == 7) {
        return [BirthDateCell height];
    } else if (indexPath.row == 8) {
        return 19.0f;
    } else if (indexPath.row == 9) {
        return [SaveCell height];
    }
    return 0.0f;
}

- (CGFloat)height {
    CGFloat height = 0;
    for (int i = 0; i < [self tableView:self.tableView numberOfRowsInSection:0]; ++i) {
        height += [self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    return height;
}

- (void)birthDateViewControllerDidCancel:(BirthDateViewController *)birthDateViewController {
    [birthDateViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)birthDateViewControllerDidSave:(BirthDateViewController *)birthDateViewController {
    self.birthDateDate = birthDateViewController.birthDateDate;
    [self.tableView reloadData];
    [self enableSaveCheck];
    [birthDateViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cancelled {

    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-add-sibling-cancel-tap"];

    [self performSegueWithIdentifier:@"AddSiblingsExit" sender:self];
}

- (void)saving {

    // DDLogInfo(@"%d",self.addSiblingViewControllerType);

    int gender = self.addSiblingViewControllerType == AddSiblingViewControllerTypeBrother ? 2 : self.addSiblingViewControllerType;

    self.view.userInteractionEnabled = NO;
    __weak AddSiblingViewController *weakSelf = self;
    [[ServerComm instance] addKid:self.nameField.text withGender:gender andBirthdate:self.birthDateDate withSuccessBlock:^(id result) {
        NSDictionary *kid = [result objectForKey:@"result"];
        if (weakSelf.picture) {
            [[ServerComm instance] uploadKidPhoto:[[kid objectForKey:@"id"] intValue] withImage:weakSelf.picture withSuccessBlock:^(id result) {
                [[ServerComm instance] getKids:YES withSuccessBlock:^(NSDictionary *result) {
                    weakSelf.kids = [result objectForKey:@"result"];
                    [weakSelf.saveCell success];
                }                 andFailBlock:^(NSError *error) {
                    [weakSelf.saveCell success];
                }];
            }                        andFailBlock:^(NSError *error) {
                [weakSelf.saveCell success];
            }];
        } else {
            [[ServerComm instance] getKids:YES withSuccessBlock:^(NSDictionary *result) {

                // DDLogInfo(@"%@",result);

                weakSelf.kids = [result objectForKey:@"result"];
                [weakSelf.saveCell success];
            }                 andFailBlock:^(NSError *error) {
                [weakSelf.saveCell success];
            }];
        }
    }                andFailBlock:^(NSError *error) {
        [weakSelf.saveCell failed];
    }];
}

- (void)saved {

    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-add-sibling-save-tap"];

    // when a new sibling was saved, we need to switch to the everyone view
    // RIGHT HERE
    
    // [[UserManager sharedInstance] setSelectedKid:nil];
    // [[NSNotificationCenter defaultCenter] postNotificationName:KID_SELECT_NOTIFICATION object:nil];
    
    
    
    // UIViewController *topViewController = [GlobalUtils topMostController];
    
    GlobalUtils *globalUtils = [GlobalUtils sharedInstance];
    ItemsViewController *globalItemsVC = globalUtils.itemsVC;
    globalItemsVC.addedKidDuringOnboarding = YES;
    
    // ItemsViewController *parentVC = (ItemsViewController *)self.parentViewController.parentViewController.parentViewController;
    // parentVC.addedKidDuringOnboarding = YES;
    
    
    self.view.userInteractionEnabled = YES;
    [self performSegueWithIdentifier:@"AddSiblingsExit" sender:self];
}

- (void)failed {
    self.view.userInteractionEnabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self enableSaveCheck];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.nameField.text.length > 32 && range.length == 0) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.nameField endEditing:YES];
    return NO;
}

- (void)enableSaveCheck {
    [self.saveCell disable];
    if (self.nameField.text.length == 0 || self.nameField.text.length > 32) {
        return;
    }

    if (self.birthDateDate == nil) {
        return;
    }
    [self.saveCell enable];
}

- (void)birthDate {

    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-add-sibling-birthdate-tap"];

    [self performSegueWithIdentifier:@"BirthDate" sender:self];
}

- (void)addPhoto:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-siblings-add-sibling-add-photo-tap"];

    UIStoryboard *choosePictureStoryboard = [UIStoryboard storyboardWithName:@"ChoosePicture" bundle:nil];
    UINavigationController *choosePictureNavigationController = (UINavigationController *) [choosePictureStoryboard instantiateViewControllerWithIdentifier:@"ChoosePictureNavigationController"];
    ChoosePictureViewController *choosePictureViewController = [choosePictureNavigationController.viewControllers firstObject];
    choosePictureViewController.delegate = self;
    if (self.originalKidImage != nil) {
        [choosePictureViewController startWithPhoto:self.originalKidImage scale:self.photo_scale point:CGPointMake(self.photo_x, self.photo_y)];
    }
    [self.navigationController presentViewController:choosePictureNavigationController animated:YES completion:nil];
}

- (void)doneChoosingPicture:(NSDictionary *)selectedPhotosDict {
    if (selectedPhotosDict) {
        UIImage *image = selectedPhotosDict[@"croppedImage"];
        self.originalKidImage = selectedPhotosDict[@"originalImage"];
        self.photo_scale = [selectedPhotosDict[@"photo_scale"] floatValue];
        self.photo_x = [selectedPhotosDict[@"photo_x"] floatValue];
        self.photo_y = [selectedPhotosDict[@"photo_y"] floatValue];
        self.picture = image;
        [self.tableView reloadData];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)pictureSelected:(UIImage *)picture {
    if (picture != nil) {
        self.picture = picture;
        [self.tableView reloadData];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"BirthDate"]) {
        BirthDateViewController *birthDateViewController = (BirthDateViewController *) segue.destinationViewController;
        birthDateViewController.delegate = self;
        birthDateViewController.birthDateDate = self.birthDateDate;
    }
}

@end
