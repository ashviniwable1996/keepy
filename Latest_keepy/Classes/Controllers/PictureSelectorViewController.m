//
//  PictureSelectorViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "PictureSelectorViewController.h"
#import <Photos/Photos.h>
#import "Keepy-Swift.h"

@interface PictureSelectorViewController () <UITableViewDataSource, UITableViewDelegate, PictureCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *unseenView;

@end

@implementation PictureSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"onboarding-picture-selector-open"];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    _unseenView.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ceil(self.pictures.count / 4.0f);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *PictureCellIdentifier = @"PictureCell";
    PictureCell *cell = [self.tableView dequeueReusableCellWithIdentifier:PictureCellIdentifier];
    cell.delegate = self;
    NSInteger index = indexPath.row * 4;
    cell.pictures = [self.pictures subarrayWithRange:NSMakeRange(index, MIN(4, (self.pictures.count - index)))];
    [cell layout];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"Height: %f",((self.view.frame.size.width - 22)/ 4 ) + 4);
    return ((self.view.bounds.size.width - 22)/ 4 ) + 4;//[PictureCell height];
}

- (IBAction)albums:(id)sender {
    [self.delegate albums:sender];
}

- (void)pictureSelected:(UIImage *)image {
    [self.delegate pictureSelected:image];
}

@end

@interface PictureCell ()

@property(nonatomic, strong) IBOutletCollection (UIButton) NSArray *buttons;

@end

@implementation PictureCell

+ (CGFloat)height {
    return 79.0f;
}

- (IBAction)pictureSelected:(id)sender {
    NSInteger index = [self.buttons indexOfObject:sender];
    [self.delegate pictureSelected:[self.pictures objectAtIndex:index]];
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    dispatch_async(dispatch_get_main_queue(), ^{
    
    for (UIButton *button in self.buttons) {
        [button setHidden:YES];
    }
        for (id picture in self.pictures) {
            if (picture == [NSNull null]) {
                NSUInteger index = [self.pictures indexOfObject:picture];
                UIButton *button = [self.buttons objectAtIndex:index];
                UIImage *picture = [UIImage imageNamed:@"camera-photo"];
                [button setBackgroundColor:[UIColor whiteColor]];
                [button setImage:picture forState:UIControlStateNormal];
                button.hidden = NO;
            } else {
                PHAsset *asset = picture;
                NSUInteger index = [self.pictures indexOfObject:asset];
                UIButton *button = [self.buttons objectAtIndex:index];
                
                UIImage *picture = asset.thumbnail;
                [button setImage:picture forState:UIControlStateNormal];
                button.hidden = NO;
            }
        }
    });
//    for (ALAsset *asset in self.pictures) {
//        NSInteger index = [self.pictures indexOfObject:asset];
//        UIButton *button = [self.buttons objectAtIndex:index];
//        UIImage *picture = [UIImage imageWithCGImage:asset.thumbnail];
//        [button setImage:picture forState:UIControlStateNormal];
//        button.hidden = NO;
//    }
}
@end
