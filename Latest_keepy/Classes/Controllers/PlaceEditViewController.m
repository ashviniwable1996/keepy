//
//  PlaceEditViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/25/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "PlaceEditViewController.h"
#import "KPLocalPlace.h"

@interface PlaceEditViewController () <UITextFieldDelegate>

@property(nonatomic, strong) KPLocalPlace *place;

@property(nonatomic, strong) IBOutlet UITextField *titleTF;

@end

@implementation PlaceEditViewController

- (instancetype)initWithPlace:(Place *)place {
    self = [super initWithNibName:@"PlaceEditViewController" bundle:nil];
    if (self) {
        self.place = (id)place;
    }

    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source" : @"edit-place"}];

    self.title = KPLocalizedString(@"add place");
    if (self.navigationController == nil) {
        [self setButtons:@{@"title" : KPLocalizedString(@"cancel"), @"buttonType" : @(BUTTON_TYPE_NORMAL)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"save"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(YES)}];
    }
    else {
        [self setButtons:@{@"title" : KPLocalizedString(@"back"), @"buttonType" : @(BUTTON_TYPE_BACK)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"save"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(YES)}];
    }

    self.titleTF.placeholder = KPLocalizedString(@"name");

    [self.titleTF setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:17]];
    [self.titleTF setTextColor:UIColorFromRGB(0x4d3c36)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)topBtnTap:(UIButton *)sender {
    if (sender.tag == 1) //Save
    {
        if (!self.titleTF.text.length) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enter a name")];/* onDismiss:^{
                [self.titleTF becomeFirstResponder];
            }];*/
            return;
        }

        BOOL isNew = NO;
        if (self.place == nil) {
            self.place = [KPLocalPlace create];
            isNew = YES;
        }
        self.place.title = self.titleTF.text;

        RequestSuccessBlock successBlock = ^(id result) {
            if ([[result valueForKey:@"status"] intValue] != 0) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error updating place")];
                return;
            }

            if ([[[result valueForKey:@"result"] valueForKey:@"id"] isEqual:[NSNull null]]) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error updating place")];
                return;
            }

            self.place.serverID = [[[result valueForKey:@"result"] valueForKey:@"id"] intValue];
            
            [self.place saveChanges];
            
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [SVProgressHUD dismiss];

            [[Analytics sharedAnalytics] track:(isNew) ? @"item-add" : @"item-update" properties:@{@"itemType" : @"kid"}];


            [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"edit-place", @"result" : @"ok"}];

            if (self.navigationController == nil) {
                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
            }

            [self.delegate placeAdded:self.place];
        };

        if (isNew) {
            [[ServerComm instance] addPlace:self.place.title withLon:0 andLat:0 withSuccessBlock:successBlock andFailBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
            }];
        }
        else {
            [[ServerComm instance] editPlace:self.place.serverID withTitle:self.place.title andLon:0 andLat:0 withSuccessBlock:successBlock andFailBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
            }];
        }
    }
    else {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"edit-place", @"result" : @"cancel"}];

        if (self.navigationController == nil) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }

    }

}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self checkEnableSaveButton];
    return YES;
}

- (void)checkEnableSaveButton {
    self.rightButtonEnabled = NO;


    if (self.titleTF.text.length == 0) {
        return;
    }

    self.rightButtonEnabled = YES;
}

@end
