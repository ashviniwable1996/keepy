//
//  AccountViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 5/19/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <CoreText/CoreText.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>

#import "AccountViewController.h"
#import "KPPlan+API.h"

#import "Keepy-Swift.h"

@interface AccountViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSArray *plans;
@property NSUInteger selectedPlanIndex;
@property BOOL couponApplied;
@property(nonatomic, strong) NSString *couponError;
@property(nonatomic, strong) NSString *couponCode;
@property(nonatomic, strong) NSString *couponSource;
@property(nonatomic, assign) BOOL isUnlimitedButtonHidden;

@end

@implementation AccountViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        self.view.frame = CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, self.view.frame.size.height);
//        [self.view layoutIfNeeded];
    }
    return self;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, self.view.frame.size.height);
    [self.view layoutIfNeeded];
    
    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-keepies", @"source", nil]];
    
    UIColor *patternColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
    self.view.backgroundColor = patternColor;
    
    self.title = KPLocalizedString(@"my keepies");
    
    if (self.navigationController == nil || self.navigationController.view.tag == 1) {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"close"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];
    } else {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"settings"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:nil];
    }
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,  [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor clearColor];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset: UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorStyle:)])
    { [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; }
    
    
    
    self.tableView.scrollsToTop = NO;
    self.tableView.backgroundView = nil;
    self.tableView.separatorColor = [UIColor colorWithRed:0.447 green:0.361 blue:0.325 alpha:1];
    [self.view addSubview:self.tableView];
    
   
    
    [self getPlans];
    
    [[Mixpanel sharedInstance] track:@"receipt-debugging-show-account" properties:@{}];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(planRefreshed:) name:PLAN_REFRESHED_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dropBoxRefreshed:) name:DROPBOX_REFRESHED_NOTIFICATION object:nil];
    
    self.isUnlimitedButtonHidden = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (Defaults.planType == 1 || Defaults.planType == 2) {
        [self hideUnlimitedButton];
    } else {
        [self showUnlimitedButton];
    }
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 64)];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[[UIImage imageNamed:@"start-keepying-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)] forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGB(0x0BC5DB) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:15];
    [btn setTitle:KPLocalizedString(@"restore purchases") forState:UIControlStateNormal];
    if ([UIScreen mainScreen].bounds.size.width == 320){
        btn.frame = CGRectMake(15, 0, 290, 44);
    }else {
        btn.frame = CGRectMake(15, 0, self.view.bounds.size.width - 30, 44);
        
    }
    btn.center = CGPointMake(tableFooterView.bounds.size.width / 2.0f, btn.center.y +10);
    [btn addTarget:self action:@selector(restoreTap:) forControlEvents:UIControlEventTouchUpInside];
    [tableFooterView addSubview:btn];
    
    self.tableView.tableFooterView = tableFooterView;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)enterCouponCode:(NSString *)couponCode {
    self.couponSource = @"deeplink";
    self.couponCode = couponCode;
}

#pragma mark -


- (void)dropBoxRefreshed:(NSNotification *)notification {
    [self.tableView reloadData];
}

#pragma mark Notifications

- (void)planRefreshed:(NSNotification *)notification {
    [self.tableView reloadData];
    if (Defaults.planType == 1 || Defaults.planType == 2) {
        [self hideUnlimitedButton];
    } else {
        [self showUnlimitedButton];
    }
}

- (void)keyboardWillShow:(NSNotification *)sender {
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGFloat height = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]) ? kbSize.height : kbSize.width;
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = [[self tableView] contentInset];
        edgeInsets.bottom = height;
        [[self tableView] setContentInset:edgeInsets];
        edgeInsets = [[self tableView] scrollIndicatorInsets];
        edgeInsets.bottom = height;
        [[self tableView] setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender {
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = [[self tableView] contentInset];
        edgeInsets.bottom = 0;
        [[self tableView] setContentInset:edgeInsets];
        edgeInsets = [[self tableView] scrollIndicatorInsets];
        edgeInsets.bottom = 0;
        [[self tableView] setScrollIndicatorInsets:edgeInsets];
    }];
}

- (NSString *)simplifiedSKUStringForPlan:(KPPlan *)plan {
    NSString *planType = @"orange";
    NSArray *components = [plan.sku componentsSeparatedByString:@"."];
    if (components.count > 2) {
        planType = [components objectAtIndex:2];
    }
    return planType;
}

- (void)getPlans {
    self.couponError = nil;
    [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];
    [KPPlan getPlansForCouponCode:self.couponCode resultBlock:^(NSArray *plans, NSError *error) {
        //        //zvika Decipher
        //        NSLog(@"self.couponCode=%@",self.couponCode);
        //        NSLog(@"plans = %@",[plans description]);
        if (error) {
            [SVProgressHUD dismiss];
            UIAlertController* ac = [UIAlertController alertControllerWithTitle:nil message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            [ac addAction:[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:NULL]];
            [self presentViewController:ac animated:YES completion:NULL];
            return;
        }
        
        if (self.couponCode != nil)
        {
            self.couponApplied = plans.count > 0;
            if (!self.couponApplied) {
                self.couponError = error.localizedDescription; //NSLocalizedString(@"Invalid code. Please try again", @"")
                [self.tableView reloadData];
            }
            
            NSString *planType = @"orange";
            if (plans.count > 0) {
                KPPlan *plan = plans[0];
                planType = [self simplifiedSKUStringForPlan:plan];
            }
            
            NSMutableDictionary *properties = [NSMutableDictionary dictionary];
            [properties setValue:planType forKey:@"plan-type"];
            [properties setValue:self.couponCode forKey:@"coupon-code"];
            [properties setValue:self.couponSource forKey:@"coupon-source"];
            [properties setValue:@((self.couponCode != nil) && self.couponApplied) forKey:@"coupon-valid"];
            [[Mixpanel sharedInstance] track:@"my-keepies-coupon-entered" properties:properties];
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.layoutId != 0"];
        NSArray *filteredArray = [plans filteredArrayUsingPredicate:predicate];
        
        
        if ([plans count] > [filteredArray count]) {
            [[UserManager sharedInstance] refreshMyData];
        }
        self.plans = filteredArray;
        [self.tableView reloadData];
        
        NSArray *skus = [self.plans valueForKey:@"sku"];
        DDLogInfo(@"skus: %@", [skus componentsJoinedByString:@", "]);
        
        [[Mixpanel sharedInstance] track:@"receipt-debugging-loaded-skus" properties:@{@"skus": skus}];
        
        [[StoreKitManager sharedManager] productsWithIdentifiers:[NSSet setWithArray:skus]
                                                      completion:^(SKProductsResponse* response, NSError* error) {
                                                          [SVProgressHUD dismiss];
                                                          
                                                          if (error) {
                                                              UIAlertController* ac = [UIAlertController alertWithError:error];
                                                              [self presentViewController:ac animated:YES completion:NULL];
                                                              DDLogInfo(@"Error : %@", error);
                                                          } else {
                                                              [SVProgressHUD dismiss];
                                                              for (KPPlan *plan in self.plans) {
                                                                  plan.product = [response productWithIdentifier:plan.sku];
                                                                  DDLogInfo(@"sku(%@) price(%@)", plan.sku, plan.product.localizedPrice);
                                                              }
                                                              
                                                              if (response.invalidProductIdentifiers.count) {
                                                                  DDLogInfo(@"Invalid Identifiers: %@", response.invalidProductIdentifiers);
                                                              }
                                                          }
                                                          
                                                          [self.tableView reloadData];
                                                      }
         ];
    }];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isUnlimitedButtonHidden) {
        return 4;
    } else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 104 / 2.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return KPLocalizedString(@"account information");
    }
//    if (section == 1) {
//        return KPLocalizedString(@"go unlimited");
//    }
    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForHeaderInSection:section])];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 200, 33)];
    lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    lbl.text = [self tableView:tableView titleForHeaderInSection:section];
    [aview addSubview:lbl];
    
    return aview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3 && indexPath.section == 0 && !self.isUnlimitedButtonHidden){
        
        NSString *cellIdentifier = @"UTGoUnlimitedCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
            //full size image for the first cell.
            UIImageView *imgUT = [[UIImageView alloc] init];
            if ([UIScreen mainScreen].bounds.size.width > 320){
                imgUT.frame = CGRectMake(0, 0, self.view.bounds.size.width, cell.bounds.size.width * 90 /cell.bounds.size.width);
            } else {
            imgUT.frame = CGRectMake(0,0,self.view.bounds.size.width, cell.bounds.size.width * 74 /320);
            }
     
            imgUT.image = [UIImage imageNamed:@"UTSave"];
            [cell.contentView addSubview:imgUT];
            
        }
        return cell;
        
    }
    else
    {
        
        NSString *cellIdentifier = @"InfoCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
            if (indexPath.section == 0) {
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 260, 44)];
                lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
                lbl.textAlignment = NSTextAlignmentLeft;
                lbl.backgroundColor = [UIColor clearColor];
                lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
                [cell.contentView addSubview:lbl];
                
                //line
                UIImageView *lineView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 43, self.view.bounds.size.width - 20, 1)];
                //lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
                [cell.contentView addSubview:lineView];
            }
//            else if (indexPath.section == 1) {
//                // This view should be refactored to comply with Auto Layout.
//                KPPlansView *view = [[KPPlansView alloc] initWithFrame:CGRectMake(0, 0, 320, 190)];
//                view.delegate = self;
//                view.tag = 100;
//                [cell.contentView addSubview:view];
//            }
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImageView *bkgView = [[UIImageView alloc] initWithFrame:cell.bounds];
        
        if (indexPath.section != 0) {
            bkgView.backgroundColor = [UIColor clearColor];
        }
        else {
            bkgView.backgroundColor = [UIColor whiteColor];
        }
        
        cell.backgroundView = bkgView;
        cell.backgroundColor = [UIColor clearColor];
        
        [self configureCell:cell withIndexPath:indexPath];
        
        return cell;
    }
}

- (void)configureCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UILabel *titleLbl = (UILabel*) [cell.contentView.subviews objectAtIndex:0];
        UIImageView *lineImage = (UIImageView *) [cell.contentView.subviews objectAtIndex:1];
        lineImage.hidden = (indexPath.row == 2);
        
        NSString *atitle = @"";
        NSString *itemTitleStr = @"";
        
        if (indexPath.row == 0) {
            atitle = KPLocalizedString(@"plan type:");
            itemTitleStr = KPLocalizedString(@"free");
            if (Defaults.planType == 1) {
                itemTitleStr = KPLocalizedString(@"premium");
            } else if (Defaults.planType == 2) {
                itemTitleStr = KPLocalizedString(@"ultimate");
            }
            
            itemTitleStr = [NSString stringWithFormat:@"%@ %@", atitle, itemTitleStr];
        }
        else if (indexPath.row == 1) {
            
            atitle = KPLocalizedString(@"keepies used this month:");
            itemTitleStr = [NSString stringWithFormat:@"%@ %ld", atitle, (long)Defaults.keepiesUsedThisMonth];
            
            if (Defaults.planType == 2) {
                
                NSInteger kl = Defaults.keepyMonthlySpace - Defaults.keepiesUsedThisMonth;
                if (kl < 0) {
                    kl = 0;
                }
                atitle = KPLocalizedString(@"keepies left:");
                
                NSString *klStr = [NSString stringWithFormat:@"%ld", (long)kl];
                
                if (Defaults.planType == 2) {
                    klStr = KPLocalizedString(@"unlimited");
                }
                
                itemTitleStr = [NSString stringWithFormat:@"%@ %@", atitle, klStr];
            }
        }
        else if (indexPath.row == 2) {
            NSInteger kl = Defaults.keepyMonthlySpace - Defaults.keepiesUsedThisMonth;
            if (kl < 0) {
                kl = 0;
            }
            atitle = KPLocalizedString(@"keepies left:");
            
            NSString *klStr = [NSString stringWithFormat:@"%ld", (long)kl];
            
            if (Defaults.planType == 2) {
                klStr = KPLocalizedString(@"unlimited");
            }
            
            itemTitleStr = [NSString stringWithFormat:@"%@ %@", atitle, klStr];
        }
        
        
        else if ((indexPath.row == 4 && !self.isUnlimitedButtonHidden) ||
                 (indexPath.row == 3 && self.isUnlimitedButtonHidden)) {
            
            
            if (!(DBClientsManager.authorizedClient || DBClientsManager.authorizedTeamClient)) {
                        itemTitleStr = KPLocalizedString(@"link Dropbox account");
                
                    } else {
                        itemTitleStr = KPLocalizedString(@"unlink Dropbox account");
                        
                    }
            atitle = itemTitleStr;
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator]; 
        }
        
        
        NSMutableAttributedString* mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:itemTitleStr];
        
        NSRange noboldRange = {NSNotFound, 0};
        //NSRange strikeRange = {NSNotFound, 0};
        if (atitle.length > 0) {
            noboldRange = [[mutableAttributedString string] rangeOfString:atitle options:NSCaseInsensitiveSearch];
        }
        
        UIFont *noboldSystemFont = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef) noboldSystemFont.fontName, noboldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:(NSString *) kCTFontAttributeName value:(__bridge id) font range:noboldRange];
            
            CFRelease(font);
        }
        
        titleLbl.attributedText = mutableAttributedString;
        
    }
    //    else if (indexPath.section == 1) {
    //        KPPlan *plan1 = [self.plans objectAtIndex:0];
    //        KPPlan *plan2 = [self.plans objectAtIndex:1];
    //        KPPlansView *view = (KPPlansView *) [cell.contentView viewWithTag:100];
    //        [view.leftPriceButton setTitle:plan1.product.localizedPrice forState:UIControlStateNormal];
    //        [view.rightPriceButton setTitle:plan2.product.localizedPrice forState:UIControlStateNormal];
    //        view.leftTitleLabel.text = plan1.title;
    //        view.rightTitleLabel.text = plan2.title;
    //
    //        if (self.couponApplied) {
    //            [view onCouponApplied];
    //        }
    //        if (self.couponCode.length > 0) {
    //            view.couponTextField.text = self.couponCode;
    //        }
    //        view.errorLabel.text = self.couponError;
    //
    //    }
}

#pragma mark -
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3 && !self.isUnlimitedButtonHidden) {
        if ([UIScreen mainScreen].bounds.size.width > 320){
            return 90;
        }
        return 76;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 3 && !self.isUnlimitedButtonHidden){
        [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"updagrade-settings-btn", @"btn", nil]];
        
        UnlimitedViewController *avc = [[UIStoryboard storyboardWithName:@"Unlimited" bundle:nil] instantiateViewControllerWithIdentifier:@"UnlimitedTable"];
        [self.navigationController pushViewController:avc animated:YES];
    }else if ((indexPath.row == 4 && !self.isUnlimitedButtonHidden) ||
              (indexPath.row == 3 && self.isUnlimitedButtonHidden)){
        
                    [[Mixpanel sharedInstance] track:@"dropbox tap"];
        
                    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"dropbox-settings-btn", @"btn", nil]];
        
        
        
        
//                    if (![[DBSession sharedSession] isLinked]) {
//                        [[DBSession sharedSession] linkFromController:self];
//                    } else {
//                        [[DBSession sharedSession] unlinkAll];
//
//                        [[ServerComm instance] updateDropboxCredentials:@"oauth_token= &oauth_token_secret= &uid= " withSuccessBlock:^(id result) {
//
//                            [[NSNotificationCenter defaultCenter] postNotificationName:DROPBOX_REFRESHED_NOTIFICATION object:nil];
//
//                        }                                  andFailBlock:^(NSError *error) {
//
//                        }];
//
//                    }
        
        
        //UIApplication *application = [UIApplication sharedApplication];
        //DBClientsManager authorizeFromController:application controller:self openURL:<#^(NSURL * _Nonnull)openURL#>
        //DBClientsManager isli
        
        
        if (!(DBClientsManager.authorizedClient || DBClientsManager.authorizedTeamClient)) {
            [DBClientsManager authorizeFromController:[UIApplication sharedApplication]
                                           controller:self
                                              openURL:^(NSURL *url) {
                                                  [[UIApplication sharedApplication] openURL:url];
                                              }];
        } else {
            [DBClientsManager unlinkAndResetClients];
            
            [[ServerComm instance] updateDropboxCredentials:@"oauth_token= &oauth_token_secret= &uid= " withSuccessBlock:^(id result) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:DROPBOX_REFRESHED_NOTIFICATION object:nil];
                
            }                                  andFailBlock:^(NSError *error) {
                
            }];
            
        }
        
        [tableView reloadData];
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark -
#pragma mark Buttons

- (void)topBtnTap:(UIButton *)sender {
    
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-keepies", @"source", @"cancel", @"result", nil]];
    
    if (self.navigationController == nil || self.navigationController.view.tag == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)restoreTap:(UIButton *)sender {
    
    [[Mixpanel sharedInstance] track:@"receipt-debugging-pressed-restore-button" properties:@{}];
    
    [SVProgressHUD showWithStatus:KPLocalizedString(@"verifying")];
    [[StoreKitManager sharedManager] restorePurchases:^(NSDictionary* response, NSError* error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        } else {
            [SVProgressHUD dismiss];
            
            UIAlertController* ac = [UIAlertController alertWithMessage:NSLocalizedString(@"your previous purchases have been successfully restored", comment: @"")];
            [self presentViewController:ac animated:YES completion:NULL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSManagedObjectContext MR_contextForCurrentThread ] MR_saveOnlySelfAndWait];
            });
        }
    }];
}

- (NSString *)encodeReceipt:(const uint8_t *)input length:(NSInteger)length {
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData *data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t *output = (uint8_t *) data.mutableBytes;
    
    for (NSInteger i = 0; i < length; i += 3) {
        NSInteger value = 0;
        for (NSInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger index = (i / 3) * 4;
        output[index + 0] = table[(value >> 18) & 0x3F];
        output[index + 1] = table[(value >> 12) & 0x3F];
        output[index + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[index + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

#pragma mark -
#pragma mark KPPlansViewDelegate methods

- (void)plansView:(KPPlansView *)view didSelectReferFriends:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-keepies", @"source", @"ok", @"result", nil]];
    
    [[Mixpanel sharedInstance] track:@"tell-friend-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my keepies", @"source", nil]];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"kpyme://spreadlove"]];
    }];
}

- (void)plansView:(KPPlansView *)view didSelectLeftButton:(UIButton *)sender {
    self.selectedPlanIndex = 0; // monthly
    [self requestPayment];
}

- (void)plansView:(KPPlansView *)view didSelectRightButton:(UIButton *)sender {
    self.selectedPlanIndex = 1; // yearly
    [self requestPayment];
}

- (void)plansView:(KPPlansView *)view didEnterCouponCode:(NSString *)couponCode {
    
    [[Mixpanel sharedInstance] track:@"receipt-debugging-entered-coupon-code" properties:@{@"coupon": couponCode}];
    
    self.couponSource = @"user";
    self.couponCode = couponCode;
    [self getPlans];
}

- (void)requestPayment {
    KPPlan *plan = [self.plans objectAtIndex:self.selectedPlanIndex];
    if (plan.product == nil) { return; }
    
    NSString* type = [self simplifiedSKUStringForPlan:plan];  // "orange" or something else.
    NSString* period = plan.title ?: @"";
    NSString* coupon = self.couponCode ?: @"";
    
    [[Mixpanel sharedInstance] track:@"my-keepies-choose-plan" properties:@{@"plan-type" : type,
                                                                            @"plan-period" : period,
                                                                            @"coupon-code" : coupon}];
    
    [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];
    
    [[StoreKitManager sharedManager] buy:plan.product planType:type couponCode:coupon completion:^(NSDictionary* response, NSError* error) {
        if (error) {
            if (error.code == SKErrorPaymentCancelled) {
                [SVProgressHUD dismiss];
            } else {
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            }
        } else {
            [SVProgressHUD dismiss];
            
            UIAlertController* ac = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"thank you!", comment: @"") preferredStyle:UIAlertControllerStyleAlert];
            [ac addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", comment: @"") style:UIAlertActionStyleDefault handler:NULL]];
            [self presentViewController:ac animated:YES completion:NULL];
            
            [[Mixpanel sharedInstance] track:@"my-keepies-purchase" properties:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSManagedObjectContext MR_contextForCurrentThread ] MR_saveOnlySelfAndWait];
            });
        }
    }];
}

#pragma mark - Private functions

- (void)hideUnlimitedButton {
    if (self.isUnlimitedButtonHidden) {
        return;
    }
    
//    NSArray *firstSection = [NSArray arrayWithObjects:KPLocalizedString(@"my profile"), KPLocalizedString(@"my kids"), KPLocalizedString(@"my fans"), KPLocalizedString(@"my account"),KPLocalizedString(@"logout"),  nil];
//    [self.menus replaceObjectAtIndex:0 withObject:firstSection];
//    [self.tableView reloadData];
    
    self.isUnlimitedButtonHidden = YES;
}

- (void)showUnlimitedButton {
    if (!self.isUnlimitedButtonHidden) {
        return;
    }
    
//    NSArray *firstSection = [NSArray arrayWithObjects:@"gounlimitedimage", KPLocalizedString(@"my profile"), KPLocalizedString(@"my kids"), KPLocalizedString(@"my fans"), KPLocalizedString(@"my account"),KPLocalizedString(@"logout"),  nil];
//    [self.menus replaceObjectAtIndex:0 withObject:firstSection];
//    [self.tableView reloadData];
    
    self.isUnlimitedButtonHidden = NO;
}


@end
