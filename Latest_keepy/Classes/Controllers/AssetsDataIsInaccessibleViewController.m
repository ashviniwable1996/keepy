#import "AssetsDataIsInaccessibleViewController.h"

@interface AssetsDataIsInaccessibleViewController ()
@property(nonatomic, strong) IBOutlet UITextView *explanationTextView;
@end

@implementation AssetsDataIsInaccessibleViewController

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    self.explanationTextView.text = self.explanation;
}

@end
