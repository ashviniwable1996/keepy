//
//  ChildViewController.h
//  Keepy
//
//  Created by Aakash Wadhwa on 08/08/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol DetailViewOfKids<NSObject>
- (void)closeTab;
@end
@interface ChildViewController : UIViewController
@property (assign, nonatomic) NSInteger index;
@property(nonatomic) NSInteger currentIndex;
@property(nonatomic) NSInteger commentIndex;
@property (nonatomic, weak) id <DetailViewOfKids> delegate;
@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic) BOOL isActive;

@property(nonatomic, strong) NSTimer *audioTimer;

- (void)clearPreviousContentArtifacts;
@end
