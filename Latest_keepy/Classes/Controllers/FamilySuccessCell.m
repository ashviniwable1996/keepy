//
//  FamilySuccessCell.m
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilySuccessCell.h"

@interface FamilySuccessCell ()

@property(nonatomic, weak) IBOutlet UILabel *title;

@end

@implementation FamilySuccessCell

#define DEFAULT_BODY_WIDTH 215.0f
#define DEFAULT_BODY_HEIGHT 61.0f
#define CELL_HEIGHT 164.0f

- (void)awakeFromNib {
    [super awakeFromNib];
    self.title.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:21];
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.body.attributedText = [FamilySuccessCell attribuedMessage:self.body.text];
    CGFloat bodyHeight = [FamilySuccessCell bodyHeight:self.body.text];
    CGFloat height = [FamilySuccessCell height:self.body.text];
    self.body.frame = CGRectMake(self.body.frame.origin.x, self.body.frame.origin.y, self.body.frame.size.width, bodyHeight);
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, height);
}

+ (CGFloat)bodyHeight:(NSString *)body {
    CGRect rect = [[FamilySuccessCell attribuedMessage:body] boundingRectWithSize:CGSizeMake(DEFAULT_BODY_WIDTH, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return fmaxf(DEFAULT_BODY_HEIGHT, ceilf(rect.size.height));
}

+ (CGFloat)height:(NSString *)body {
    return (CELL_HEIGHT - DEFAULT_BODY_HEIGHT) + [FamilySuccessCell bodyHeight:body];
}

+ (NSMutableAttributedString *)attribuedMessage:(NSString *)body {
    NSMutableAttributedString *bodyAttributedString = [[NSMutableAttributedString alloc] initWithString:body];
    [bodyAttributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Light" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:(87.0f / 255.0f) green:(66.0f / 255.0f) blue:(63.0f / 255.0f) alpha:1.0f]} range:NSMakeRange(0, body.length)];
    return bodyAttributedString;
}

@end
