//
//  CalendarSectionHeaderReusableView.h
//  Keepy
//
//  Created by Arik Sosman on 6/8/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarSectionHeaderReusableView : UICollectionReusableView

@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) NSInteger count;

@end
