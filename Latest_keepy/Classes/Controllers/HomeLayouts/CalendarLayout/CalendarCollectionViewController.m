    //
//  CalendarCollectionViewController.m
//  Keepy
//
//  Created by Arik Sosman on 6/2/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//


// #import <PrintIO/PrintIO.h>
// #import <PrintIO/PIOSideMenuButton.h>


#import "CalendarCollectionViewController.h"
#import "KPLocalKid.h"
#import "KPLocalItem.h"
#import "KPLocalAsset.h"
#import "KPLocalComment.h"
#import "KPSQLiteManager.h"

#import "CalendarSectionHeaderReusableView.h"
#import "CalendarItemCell.h"
#import "CalendarSectionDescriptor.h"
#import "AgeCalculator.h"

#import "FullScreenContainerViewController.h"
#import "AppDelegate.h"
#import "PseudoRandomnessAssistant.h"
#import "CachingAssistant.h"

#import "Keepy-Swift.h"
#import "TestFairy.h"

// #import "PrintIOPhotoSourceKeepy.h"


@interface CalendarCollectionViewController ()

@property int numberOfPicturesPerRow;
@property CGFloat precisePictureWidth;
@property CGFloat actualPictureWidth;

@property (strong, nonatomic) NSMutableDictionary *temporaryImageCache;
@property (strong, nonatomic) NSMutableDictionary *downloadTasks;
@property (strong, nonatomic) NSURLSession *imageDownloadSession;
@property (strong, nonatomic) NSOperationQueue *imageDownloadQueue;

// @property (strong, nonatomic) NSMutableArray *imageURLsToDownload;
@property (strong, nonatomic) NSMutableArray *indexPathsToDownloadImages;
@property (strong, nonatomic) NSMutableDictionary *imageURLsByIndexPath;
@property (strong, nonatomic) NSArray *sortedItems;
@property (strong, nonatomic) NSArray *sections;

//@property UIEdgeInsets *collectionViewInsets;
//@property ()

@end

@implementation CalendarCollectionViewController

static NSString * const reuseIdentifier = @"Cell";
static NSString * const sectionHeaderReuseIdentifier = @"SectionHeaderView";

static NSString * const ACTIVE_SORTING_COLUMN = @"itemDate";
static id extracted() {
    return @{}.mutableCopy;
}

//static BOOL const USE_RAM_IMAGE_CACHE = NO;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if(([UIScreen mainScreen].bounds.size.height == 812) || ([UIScreen mainScreen].bounds.size.height > 812)) {
       // self.topInset = 45;
      //  self.topInset = 90;
        self.topInset = 90 - 25;
    } else if ([UIScreen mainScreen].bounds.size.height == 568) {
       // self.topInset = 70;
        self.topInset = 90;
    }
//    else if ([UIScreen mainScreen].bounds.size.height == 667) { // for 6 gold
//        //self.topInset = 90;
//    }
    else {
        self.topInset = 70;
    }
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes

    NSLog(@"Email: %@",[UserManager sharedInstance].getMe.email);
    [TestFairy setUserId:[UserManager sharedInstance].getMe.email];
    [TestFairy begin:@"27edb8827b24acb13e21575ff94973a5b30ac61e"];

    [self.collectionView registerClass:[CalendarItemCell class] forCellWithReuseIdentifier:reuseIdentifier];
    [self.collectionView registerClass:[CalendarSectionHeaderReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:sectionHeaderReuseIdentifier];

    self.collectionView.backgroundColor = [UIColor colorWithRed:228/255.0 green:226/255.0 blue:225/255.0 alpha:1];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    self.collectionView.contentOffset = CGPointMake(0, self.topInset);
    
//    self.collectionView.contentInset = UIEdgeInsetsMake(self.topInset, 0, 60, 0);
//    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topInset, 0, 60, 0);;
//    self.collectionView.scrollsToTop = YES;
    
    // modified by Ashvini
  //  self.lastContentOffset = self.topInset;
    
    CGFloat screenWidth = self.view.frame.size.width;
    int screenScale = [UIScreen mainScreen].scale;
    
    int numberOfPicturesPerRow = floor((screenWidth - 12) / 77);
//    self.numberOfPicturesPerRow = numberOfPicturesPerRow;
    self.numberOfPicturesPerRow = 4;

    CGFloat precisePictureWidth = (screenWidth - 4 * numberOfPicturesPerRow - 12) / numberOfPicturesPerRow;
    CGFloat actualPictureWidth = floor(precisePictureWidth * screenScale) / screenScale;
    
    self.precisePictureWidth = precisePictureWidth;
    self.actualPictureWidth = actualPictureWidth;
    
    // CGFloat overflowingPixels = (precisePictureWidth - actualPictureWidth) * numberOfPicturesPerRow;
    
    self.temporaryImageCache = extracted();
    self.downloadTasks = @{}.mutableCopy;
    
    FSQCollectionViewAlignedLayout *layout = (id)self.collectionViewLayout;
    if([layout isKindOfClass:[FSQCollectionViewAlignedLayout class]]){
        // layout.defaultCellSize = CGSizeMake(actualPictureWidth, actualPictureWidth);
    }
    
    self.collectionView.allowsSelection = YES;
    // self.collectionView.allowsMultipleSelection = YES;

    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressAlbumCreationButton:) name:@"AlbumCreationButtonTappedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(lockedKeepyFeed:)
     name:@"isLockedKeepyFeed"
     object:nil];
    
    self.imageDownloadQueue = [NSOperationQueue new];
    self.imageDownloadQueue.maxConcurrentOperationCount = 30;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.imageDownloadSession = [NSURLSession sessionWithConfiguration:configuration delegate:(id)self delegateQueue:self.imageDownloadQueue];
    

    self.indexPathsToDownloadImages = @[].mutableCopy;
    self.imageURLsByIndexPath = @{}.mutableCopy;
    
    
    [self calculateSections];
    
//    ProductCollectionView * collection = [[ProductCollectionView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
//    [collection getThePriceForTheProducts];
    
}

// added this currently by Ashvini
// current by Ashvini
- (void)viewDidLayoutSubviews{
//    if (self.isFirstTimeLoading) {
//        self.isFirstTimeLoading =  NO;
//    } else {
//        self.collectionView.contentInset = UIEdgeInsetsMake(self.topInset, 0, 60, 0);
//        self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topInset, 0, 60, 0);;
//        self.collectionView.scrollsToTop = YES;
//    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self downloadPendingImages];
    
}

- (void)downloadPendingImages{
    
    NSArray *indexPathsCopy = self.indexPathsToDownloadImages.copy;
    for(NSIndexPath *currentIndexPath in indexPathsCopy){
        
        [self downloadPendingImageForIndexPath:currentIndexPath];
        
    }
    if(self.collectionView != nil){
        [self.collectionView reloadData];    
    }
}


- (void)downloadPendingImageForCell:(CalendarItemCell *)cell atIndexPath:(NSIndexPath *)currentIndexPath{
    
    NSURL *imageURL = self.imageURLsByIndexPath[currentIndexPath];
    
    if(!imageURL){ return; }
    
    // NSDictionary *downloadTaskDetails = self.downloadTasks[imageURL];
    // NSURLSessionDownloadTask *imageDownload;
    
    NSLog(@"downloading %@", imageURL.path);
    
    NSURLSessionDownloadTask *imageDownload = self.downloadTasks[imageURL];
    if(imageDownload && imageDownload.state == NSURLSessionTaskStateRunning){ 
        return; // this image download already exists, and we don't wanna do anything about it
    } // we don't wanna do anything with it just yet 
    
    // if(YES || !imageDownload){
    
    imageDownload = [self.imageDownloadSession downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        
        if(!error){
            
            [self.indexPathsToDownloadImages removeObject:currentIndexPath];
            
            NSString *localImagePath = [CachingAssistant cacheLocationForExternalURL:imageURL];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[NSFileManager defaultManager] moveItemAtPath:location.path toPath:localImagePath error:nil];
                UIImage *localImage = [[UIImage alloc] initWithContentsOfFile:localImagePath];
                
                if([self.collectionView.indexPathsForVisibleItems containsObject:currentIndexPath]){
                    // [self.collectionView reloadItemsAtIndexPaths:@[currentIndexPath]];
                    cell.imageView.image = localImage;
                }
                
            });
            
            
        }else{
            
            if(error.code == -999){ // it was cancelled from the outside, we don't care
                return;
            }
            
            // this could be entered because the download was cancelled, or because it simply failed
            
            // to determine whether this thing should be removed, we'll do the following:
            
            
            
            /*if([self.collectionView.indexPathsForVisibleItems containsObject:currentIndexPath] && !self.collectionView.isDragging && !self.collectionView.isDecelerating){
             
             // it is still relevant
             NSLog(@"Failed to download image for a cell that is still visible. Unsure whether to retry or abort");
             
             // either way, we need to get rid of this download task, cause it's not useful for us
             
             // let's try to restart it
             // [imageDownload resume];
             
             
             
             }*/
            
            if([self.indexPathsToDownloadImages containsObject:currentIndexPath]){
                
                // failed to download an image that is still relevant
                
                // maybe let's wait a second 
                [self downloadPendingImageForCell:cell atIndexPath:currentIndexPath];
                
            }else{
                
                NSLog(@"failed to download an image for an index path that is still relevant. restarting it");
                
                [imageDownload cancel];
                [self.downloadTasks removeObjectForKey:imageURL];
                
            }
            
        }
        
    }];
    
    self.downloadTasks[imageURL] = imageDownload;
    [imageDownload resume];

    
}

- (void)downloadPendingImageForIndexPath:(NSIndexPath *)currentIndexPath{
    
    return;
    
    NSURL *imageURL = self.imageURLsByIndexPath[currentIndexPath];
    
    if(!imageURL){ return; }
    
    // NSDictionary *downloadTaskDetails = self.downloadTasks[imageURL];
    // NSURLSessionDownloadTask *imageDownload;
    
    NSLog(@"downloading %@", imageURL.path);
    
    NSURLSessionDownloadTask *imageDownload = self.downloadTasks[imageURL];
    if(imageDownload && imageDownload.state == NSURLSessionTaskStateRunning){ 
        return; // this image download already exists, and we don't wanna do anything about it
    } // we don't wanna do anything with it just yet 
    
    // if(YES || !imageDownload){
    
    imageDownload = [self.imageDownloadSession downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        
        if(!error){
            
            [self.indexPathsToDownloadImages removeObject:currentIndexPath];
            
            NSString *localImagePath = [CachingAssistant cacheLocationForExternalURL:imageURL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[NSFileManager defaultManager] moveItemAtPath:location.path toPath:localImagePath error:nil];
                
                if([self.collectionView.indexPathsForVisibleItems containsObject:currentIndexPath]){
                    [self.collectionView reloadItemsAtIndexPaths:@[currentIndexPath]];
                }
                
            });
            
            
        }else{
            
            if(error.code == -999){ // it was cancelled from the outside, we don't care
                return;
            }
            
            // this could be entered because the download was cancelled, or because it simply failed
            
            // to determine whether this thing should be removed, we'll do the following:
            
            
            
            /*if([self.collectionView.indexPathsForVisibleItems containsObject:currentIndexPath] && !self.collectionView.isDragging && !self.collectionView.isDecelerating){
                
                // it is still relevant
                NSLog(@"Failed to download image for a cell that is still visible. Unsure whether to retry or abort");
                
                // either way, we need to get rid of this download task, cause it's not useful for us
                
                // let's try to restart it
                // [imageDownload resume];
                
                
                
            }*/
             
            if([self.indexPathsToDownloadImages containsObject:currentIndexPath]){
             
                // failed to download an image that is still relevant
                
                // maybe let's wait a second 
                [self downloadPendingImageForIndexPath:currentIndexPath];
                
            }else{
                
                NSLog(@"failed to download an image for an index path that is still relevant. restarting it");
                
                [imageDownload cancel];
                [self.downloadTasks removeObjectForKey:imageURL];
                
            }
            
            // we could resume it here after the failure
            // but let's not at first
            // [imageDownload resume];
            
            // NSLog(@"failed %@ | %li", sectionDescriptor.description, (long)indexPath.item);
            
        }
        
    }];
    
    self.downloadTasks[imageURL] = imageDownload;
    [imageDownload resume];
    
    /* 
     } else{
     
     [imageDownload resume];
     
     // imageDownload = downloadTaskDetails[@"task"];
     
     }
     */
    
}

- (void)cancelAllDownloads{
    
//    NSLog(@"cancelling everything");
    
    // return;
    
    NSDictionary *downloadTasks = self.downloadTasks.copy;
    for(NSURL *imageURL in downloadTasks){
        
        [self cancelDownloadForURL:imageURL];
        
    }
    
}

- (void)cancelDownloadForURL:(NSURL *)imageURL{
    
    NSLog(@"cancelling %@", imageURL.path);
    
    NSURLSessionDownloadTask *currentTask = self.downloadTasks[imageURL];
    if(!currentTask){
        return;
    }
    
    [self.downloadTasks removeObjectForKey:currentTask]; // it is crucial that this thing first be removed to eliminate blockage, and THEN be cancelled
    
    [currentTask cancel];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.collectionView.contentInset = UIEdgeInsetsMake(self.topInset, 0, 60, 0);
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topInset, 0, 60, 0);
    [self.view layoutIfNeeded];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // [self downloadPendingImages];
//    NSArray *pendingIndexPaths = self.indexPathsToDownloadImages;
//    NSArray *relevantIndexPaths = self.collectionView.indexPathsForVisibleItems;
    NSLog(@"here");
}

- (void)setSelectedKid:(KPLocalKid *)selectedKid{
    _selectedKid = selectedKid;
    // [self reloadEverything];
}

- (void)reloadEverything{
    [self cancelAllDownloads];
    self.imageURLsByIndexPath = @{}.mutableCopy;
    [self calculateSections];
    
    /*
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.collectionView reloadData];
        
    });
    */
    
}

- (void)completedRecalculatingSections{
    
    [self downloadPendingImages];
    
}

- (void)calculateSections{
    
//    if([NSThread isMainThread]){
//        
//        self.sections = @[];
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [self calculateSections];
//        });
//        
//        // [self performSelectorInBackground:@selector(calculateSections) withObject:nil];
//        
//        return; // we do not want to perform this calculation in the main thread
//    }
    
    
    
    // we have the items
    // now, let's sort them
    
    NSSortDescriptor *dateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:ACTIVE_SORTING_COLUMN ascending:NO];
    self.sortedItems = [self.unsortedItems sortedArrayUsingDescriptors:@[dateSortDescriptor]];
    
    
    KPLocalItem *newestItem = self.sortedItems.firstObject;
    KPLocalItem *oldestItem = self.sortedItems.lastObject;
    
    
    
    
    // NSString *itemTable = [KPLocalItem getDatabaseTable];
    
    NSDate *presentDate = [AgeCalculator removeHourAndMinutes:[NSDate date]];
    NSDate *latestDate = presentDate;
    if(newestItem.itemDate > presentDate.timeIntervalSince1970){
        latestDate = [AgeCalculator removeHourAndMinutes:[NSDate dateWithTimeIntervalSince1970:newestItem.itemDate]];
    }
    
    NSDate *oldestDate = [AgeCalculator removeHourAndMinutes:[NSDate dateWithTimeIntervalSince1970:oldestItem.itemDate]];
    
    // __block NSDate *oldestDate;
    
    NSMutableArray *sections = @[].mutableCopy;
    
    // in order to properly determine the sections, we need to find the time we want to span
    
    if(!self.selectedKid){ // this is the everyone mode, so let's first take the oldest photo we have  
        
        // in the everyone mode, we are interested in the years. This is also what we're gonna use for the titles.
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *todayComponents = [calendar components:NSCalendarUnitYear fromDate:presentDate];
        NSDateComponents *oldestComponents = [calendar components:NSCalendarUnitYear fromDate:oldestDate];
        
        NSInteger oldestYear = oldestComponents.year;
        NSInteger presentYear = todayComponents.year;
        
        for(NSInteger currentYear = presentYear; currentYear >= oldestYear; currentYear--){
            
            NSDateComponents *components = [NSDateComponents new];
            components.year = currentYear;
            
            NSDate *currentYearStartDate = [calendar dateFromComponents:components];
            
            
            CalendarSectionDescriptor *descriptor = [CalendarSectionDescriptor new];
            descriptor.startDate = currentYearStartDate;
            
            
            // now, to calculate how many items there are in this section
            
            /*
            NSString *itemCountQueryString = [NSString stringWithFormat:@"SELECT COUNT(*) AS itemCount FROM %@ WHERE %@ >= :startDate AND %@ < :endDate", itemTable, ACTIVE_SORTING_COLUMN, ACTIVE_SORTING_COLUMN];
            NSDictionary *params = @{@"startDate": @(currentYearStartDate.timeIntervalSince1970), @"endDate": @(descriptor.nextYearStartDate.timeIntervalSince1970)};
            
            [[KPSQLiteManager getManager].dbQueue inDatabase:^(FMDatabase *db) {
            
                FMResultSet *resultSet = [db executeQuery:itemCountQueryString withParameterDictionary:params];
                
                if(resultSet.next){
                    
                    NSDictionary *resultDictionary = resultSet.resultDictionary;
                    NSNumber *itemCount = resultDictionary[@"itemCount"];
                    descriptor.itemCount = itemCount.integerValue;
                    
                }
                
                [resultSet close];
            
            }];
            */
            
            
            // NSDictionary *denormalizedItems = [self calculateDenormalizedItemInfoForSectionDetails:descriptor];
            // NSArray *itemOrder = denormalizedItems[@"localItemOrder"];
            
            // descriptor.denormalizedItems = denormalizedItems;
            // descriptor.itemCount = itemOrder.count;
            
            
            [sections addObject:descriptor];
            
        }
        
        self.sections = sections;
        

    }else{
        
//        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        // there is a kid selected
        // let's select the older picture
        // NSString *oldestItemQueryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id IN (SELECT itemID from kid_items WHERE kidID = :kidID) ORDER BY %@ ASC LIMIT 0,1", itemTable, ACTIVE_SORTING_COLUMN];
        
        
        NSDate *birthdate = [AgeCalculator removeHourAndMinutes:[NSDate dateWithTimeIntervalSince1970:self.selectedKid.birthdate]];
        if(birthdate.timeIntervalSince1970 < oldestDate.timeIntervalSince1970){
            oldestDate = birthdate;
        }
        
        NSDate *threshold = birthdate;
        
        if(oldestDate.timeIntervalSince1970 < presentDate.timeIntervalSince1970){
        
            while(true){
                
                CalendarSectionDescriptor *descriptor = [CalendarSectionDescriptor new];
                descriptor.linkedChild = self.selectedKid;
                descriptor.startDate = threshold;
                
                
                // NSDictionary *denormalizedItems = [self calculateDenormalizedItemInfoForSectionDetails:descriptor];
                // NSArray *itemOrder = denormalizedItems[@"localItemOrder"];
                
                // descriptor.denormalizedItems = denormalizedItems;
                // descriptor.itemCount = itemOrder.count;

                
                
                threshold = descriptor.nextYearStartDate;
                
                [sections addObject:descriptor];
                
                if(threshold.timeIntervalSince1970 > presentDate.timeIntervalSince1970){
                    break;
                }
                
            }
            
        }
        
        sections = sections.reverseObjectEnumerator.allObjects.mutableCopy;
        self.sections = sections;
        
        
        
        // NSDictionary *params = @{@"kidID": @(self.selectedKid.identifier)};
        
    }
    
    
    // NSMutableArray *unassignedItems = self.sortedItems.mutableCopy;
    int i = 0;
    
    for(CalendarSectionDescriptor *currentDescriptor in self.sections){
        
        // it goes from the top to the bottom, so from the newest to the oldest
        // and so do the sections
        
        NSMutableArray *currentItemIDs = @[].mutableCopy;
        
        int j = i;
        for(j = i; j < self.sortedItems.count; j++){
            
            KPLocalItem *currentItem = self.sortedItems[j];
            NSDate *currentItemDate = [AgeCalculator removeHourAndMinutes:[NSDate dateWithTimeIntervalSince1970:currentItem.itemDate]];
            if(currentItemDate.timeIntervalSince1970 < currentDescriptor.startDate.timeIntervalSince1970){
                break;
            }
            
            [currentItemIDs addObject:@(currentItem.identifier)];
            
        }
        
        currentDescriptor.itemIDs = currentItemIDs.copy;
        currentDescriptor.itemCount = currentDescriptor.itemIDs.count;
        
        i = j; // the next round starts with newer indices
        
    }

    [self completedRecalculatingSections];
    
}

/*
- (NSDictionary *)calculateDenormalizedItemInfoForSectionDetails:(CalendarSectionDescriptor *)sectionDescriptor{
    
    // this query links the item to all its comments and its originalImage
    
    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    
    NSString *itemsTable = [KPLocalItem getDatabaseTable];
    NSString *commentsTable = [KPLocalComment getDatabaseTable];
    NSString *assetsTable = [KPLocalAsset getDatabaseTable];
    
    NSString *kidsCondition = @" ";
    if(sectionDescriptor.linkedChild){
        kidsCondition = @" AND items.id IN (SELECT itemID from kid_items WHERE kidID = :kidID) ";
    }
    
    NSString *selectorString = [NSString stringWithFormat:@"SELECT *, \
    items.id AS localItemID, items.serverID AS serverItemID, items.extraData AS extraItemData, \
    comments.id AS localCommentID, comments.serverID AS serverCommentID, comments.extraData AS extraCommentData, \
    assets.id AS localAssetID, assets.serverID AS serverAssetID \
    FROM items \
    LEFT JOIN comments ON items.id = comments.itemID \
    LEFT JOIN assets ON items.imageAssetID = assets.id \
    WHERE items.%@ >= :startDate AND items.%@ < :endDate \
    %@ \
    ORDER BY items.%@ DESC  \
    ", ACTIVE_SORTING_COLUMN, ACTIVE_SORTING_COLUMN, kidsCondition, ACTIVE_SORTING_COLUMN];
    
    NSMutableDictionary *params = @{}.mutableCopy;
    params[@"startDate"] = @(sectionDescriptor.startDate.timeIntervalSince1970);
    params[@"endDate"] = @(sectionDescriptor.nextYearStartDate.timeIntervalSince1970);
    if(sectionDescriptor.linkedChild){
        params[@"kidID"] = @(sectionDescriptor.linkedChild.identifier);
    }
    
    NSMutableDictionary *denormalizedDetails = @{}.mutableCopy;
    
    
    NSMutableDictionary *denormalizedItems = @{}.mutableCopy;
    NSMutableDictionary *denormalizedComments = @{}.mutableCopy;
    NSMutableDictionary *denormalizedOriginalImages = @{}.mutableCopy;
    
    NSMutableArray *itemIDOrder = @[].mutableCopy;
    
    
    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *results = [db executeQuery:selectorString withParameterDictionary:params];
        while([results next]){
            
            NSDictionary *currentResult = results.resultDictionary;
            
            // [denormalizedResults addObject:currentResult];
            
            NSNumber *localItemID = currentResult[@"localItemID"];
            NSNumber *serverItemID = currentResult[@"serverItemID"];
            id extraItemData = currentResult[@"extraItemData"];
            KPLocalItem *currentItem = denormalizedItems[localItemID];
            
            NSNumber *localCommentID = currentResult[@"localCommentID"];
            NSNumber *serverCommentID = currentResult[@"serverCommentID"];
            id extraCommentData = currentResult[@"extraCommentData"];
            KPLocalComment *currentComment = denormalizedComments[localCommentID];
            
            NSNumber *localAssetID = currentResult[@"localAssetID"];
            NSNumber *serverAssetID = currentResult[@"serverAssetID"];
            KPLocalAsset *currentOriginalImage = denormalizedOriginalImages[localAssetID];
            
            // first, let's configure the item
            if(!currentItem){
                NSMutableDictionary *itemInfo = currentResult.mutableCopy;
                itemInfo[@"id"] = localItemID;
                itemInfo[@"serverID"] = serverItemID;
                
                if(extraItemData){
                    itemInfo[@"extraData"] = extraItemData;
                }
                
                currentItem = [KPLocalItem initializeFromFetchResponse:itemInfo];
                denormalizedItems[localItemID] = currentItem;
                
                [itemIDOrder addObject:localItemID];
                
            }
            
            // second, let's configure the original image
            if(!currentOriginalImage){
                NSMutableDictionary *assetInfo = currentResult.mutableCopy;
                assetInfo[@"id"] = localAssetID;
                assetInfo[@"serverID"] = serverAssetID;
                
                currentOriginalImage = [KPLocalAsset initializeFromFetchResponse:assetInfo];
                denormalizedOriginalImages[localAssetID] = currentOriginalImage;
            }
            
            // and lastly, let's do the comment
            if(!currentComment && localCommentID != [NSNull null]){
                NSMutableDictionary *commentInfo = currentResult.mutableCopy;
                commentInfo[@"id"] = localCommentID;
                commentInfo[@"serverID"] = serverCommentID;
                
                if(extraCommentData){
                    commentInfo[@"extraData"] = extraCommentData;
                }
                
                currentComment = [KPLocalComment initializeFromFetchResponse:commentInfo];
                denormalizedComments[localCommentID] = currentComment;
            }
            
            
            // finally, let's do the linking
            if(!denormalizedDetails[localItemID]){
                denormalizedDetails[localItemID] = @{}.mutableCopy;
                denormalizedDetails[localItemID][@"item"] = currentItem;
                denormalizedDetails[localItemID][@"image"] = currentOriginalImage;
                denormalizedDetails[localItemID][@"comments"] = @[].mutableCopy;
            }
            
            if(currentComment){
                [denormalizedDetails[localItemID][@"comments"] addObject:currentComment];
            }
            
            
            
        }
        [results close];
        
    }];
    
    denormalizedDetails[@"localItemOrder"] = itemIDOrder;
    
    return denormalizedDetails.copy;
    
}
*/

- (void)didPressAlbumCreationButton:(NSNotification *)notification{
    
}

- (void)lockedKeepyFeed:(NSNotification *)notification{
   // BOOL *ishide = (BOOL)notification.object;
   //  self.collectionView.hidden = notification.object;
    BOOL ishide = [notification.object boolValue];
    self.collectionView.hidden = ishide;
    [self.view layoutIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.sections.count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    CalendarSectionDescriptor *currentSectionDescriptor = self.sections[section];
    NSInteger theoreticalItemCount = MAX(currentSectionDescriptor.itemCount, 1);
    // int theoreticalItemCount = 1 + (arc4random() % 20);
    
    int rowCount = ceil(theoreticalItemCount * 1.0/self.numberOfPicturesPerRow);
    int actualItemCount = self.numberOfPicturesPerRow * rowCount;
    
    currentSectionDescriptor.rowCount = rowCount;
    
    return actualItemCount;
    
}

- (KPLocalItem *)itemForIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section >= self.sections.count){
        return nil;
    }
    
    CalendarSectionDescriptor *currentSectionDescriptor = self.sections[indexPath.section];

    if(indexPath.row >= currentSectionDescriptor.itemIDs.count){
        return nil;
    }
    
    
    NSNumber *localItemID = currentSectionDescriptor.itemIDs[indexPath.row];
    KPLocalItem *item = self.denormalizedItems[localItemID][@"item"];
    
    return item;
    
    
    /* NSArray *itemOrder = denormalizedDetails[@"localItemOrder"];
    
    if(!itemOrder || indexPath.item >= itemOrder.count){
        return nil;
    }
    
    NSNumber *localItemID = itemOrder[indexPath.item];
    item = denormalizedDetails[localItemID][@"item"];
    
    return item; */

    
    
    
    /*
    
    NSMutableDictionary *params = @{}.mutableCopy;
    params[@"startDate"] = @(currentSectionDescriptor.startDate.timeIntervalSince1970);
    params[@"endDate"] = @(currentSectionDescriptor.nextYearStartDate.timeIntervalSince1970);
    params[@"itemIndex"] = @(indexPath.item);
    
    NSString *currentItemQueryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ >= :startDate AND %@ < :endDate ORDER BY %@ DESC LIMIT :itemIndex,1", [KPLocalItem getDatabaseTable], ACTIVE_SORTING_COLUMN, ACTIVE_SORTING_COLUMN, ACTIVE_SORTING_COLUMN];
    
    [[KPSQLiteManager getManager].dbQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *resultSet = [db executeQuery:currentItemQueryString withParameterDictionary:params];
        
        if(resultSet.next){
            
            NSDictionary *resultDictionary = resultSet.resultDictionary;
            // item = [KPLocalItem initializeFromFetchResponse:resultDictionary];
            
        }
        
        [resultSet close];
        
    }];
    
    return item;
     
    */
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width - 28) / 4, ([UIScreen mainScreen].bounds.size.width - 28) / 4);
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.indexPathsToDownloadImages removeObject:indexPath];
    
    NSURL *imageURL = self.imageURLsByIndexPath[indexPath];
    if(!imageURL){ return; }
    
    [self cancelDownloadForURL:imageURL];
    
    return;
    
    
    // return;
    if(indexPath.section >= self.sections.count){
        return;
    }
    
    
    
    
    
    
    // configure the cell
//    CalendarSectionDescriptor *sectionDescriptor = self.sections[indexPath.section];
    
    
//    KPLocalItem *item = [self itemForIndexPath:indexPath];
//    KPLocalAsset *itemImage = sectionDescriptor.denormalizedItems[@(item.identifier)][@"image"];
    // NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@_medium.jpg", [[KPDefaults sharedDefaults] assetsURLString], itemImage.urlHash]];

    
    
    
    
    NSDictionary *downloadTaskDetails = self.downloadTasks[imageURL];
    if(!downloadTaskDetails){
        return;
    }
    
    
    if([self.collectionView.indexPathsForVisibleItems containsObject:indexPath]){
        return;
    }
    
    
    NSURLSessionDownloadTask *imageDownload = downloadTaskDetails[@"task"];
    [imageDownload cancel];
    [self.downloadTasks removeObjectForKey:imageURL];
        
         
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CalendarItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // configure the cell
    CalendarSectionDescriptor *sectionDescriptor = nil;
    if(indexPath.section < self.sections.count){
        sectionDescriptor = self.sections[indexPath.section];
    }
    
    cell.imageView.image = nil;
    cell.showPlayButton = NO;
    cell.showFooter = NO;
    cell.showAddPrompt = NO;
    
    // return cell;
    
        
    // let's fetch the item
    KPLocalItem *item = [self itemForIndexPath:indexPath];
    cell.item = item;

    if (cell.item) {
        cell.showLoadIndicator = YES;

        // check if it's a video
        BOOL isVideo = (cell.item.itemType == 4);
        cell.showPlayButton = isVideo;
        
        // check comment count
        NSArray *comments = [KPLocalComment fetchCommentsForItem:cell.item];
        // NSArray *comments = sectionDescriptor.denormalizedItems[@(cell.item.identifier)][@"comments"];
        if(comments && comments.count > 0){
            cell.showFooter = YES;
            cell.footerText = @(comments.count).stringValue;
        }else{
            cell.showFooter = NO;
        }

        KPImageProviderWrapper *imageProvider = [KPImageProviderWrapper instance];
        id<SDWebImageOperation> imageOperation = [imageProvider loadImageWithLocalItem:cell.item imageSize:[KPImageProviderWrapper ImageSizeMedium] completion:^(UIImage * _Nullable image) {
            cell.imageView.image = image;
            cell.showLoadIndicator = NO;
        }];
        cell.imageOperation = imageOperation;
    } else {
        cell.showLoadIndicator = NO;
        BOOL showPrompt = NO;
        
        if (!self.isFilterActive) {
            if (sectionDescriptor.itemCount == 0) {
                NSMutableDictionary *seedGenerationDictionary = @{}.mutableCopy;
                seedGenerationDictionary[@"startTime"] = @(sectionDescriptor.startDate.timeIntervalSince1970).stringValue;
                if(self.selectedKid){
                    seedGenerationDictionary[@"child"] = @(self.selectedKid.serverID).stringValue;
                }

                // we can have it anywhere
                NSInteger visibilityIndex = [PseudoRandomnessAssistant obtainPseudoRandomValueForDictionary:seedGenerationDictionary withinMinimum:0 andMaximum:self.numberOfPicturesPerRow-1];
                if(visibilityIndex == indexPath.item){
                    
                    showPrompt = YES;
                }
            }
            
            // we temporarily disable the display of brown icons in sections where there is at least one item
            if (NO && sectionDescriptor.itemCount > 0 && sectionDescriptor.rowCount == 1 && indexPath.item == sectionDescriptor.itemCount) {
                showPrompt = YES;
            }
        }

        if(showPrompt) {
            UIImage *promptImage = sectionDescriptor.addPromptImage;
            if (promptImage) {
                cell.showAddPrompt = YES;
                cell.addPromptImageView.image = sectionDescriptor.addPromptImage;
                [cell setAddPromptText:sectionDescriptor.addPromptText];
            }
        }
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    // now let's check what's going on in print.io
    if(buttonIndex == 0){ // they just pressed the print.io button
        [self showPrintIODialog];
    }
    
}

- (void)showPrintIODialog{
    
    
    
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    dispatch_async(dispatch_get_main_queue(), ^{
        // do work here
   
    KPLocalItem *currentItem = [self itemForIndexPath:indexPath];
    if(!currentItem){
        
        // do the green button
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationTriggerKeepyButtonTap" object:nil];
        
        /*
        
        
        
        // let's show an action sheet
        
        UIActionSheet *sheet = [UIActionSheet new];
        sheet.title = @"Create Album";
        sheet.delegate = self;
        // sheet.cancelButtonIndex = @"Cancel";
        
        [sheet addButtonWithTitle:@"Print.io"];
        [sheet addButtonWithTitle:@"Cancel"];
        
        sheet.cancelButtonIndex = 1;
        
        // [[UIActionSheet alloc] initWithTitle:<#(NSString *)#> delegate:<#(id<UIActionSheetDelegate>)#> cancelButtonTitle:<#(NSString *)#> destructiveButtonTitle:<#(NSString *)#> otherButtonTitles:<#(NSString *), ...#>, nil];
        [sheet showInView:self.collectionView];
        
         */

        

        return;
         

        
    }
    NSLog(@"FNTRACE CAlendarCollectionViewController: Current selected Item ID %ld",(long)currentItem.imageAssetID);
    
    CalendarSectionDescriptor *currentSectionDescriptor = self.sections[indexPath.section];
    
    NSMutableArray *currentSectionItems = @[].mutableCopy;
    for(int i = 0; i < currentSectionDescriptor.itemCount; i++){
        NSNumber *localItemID = currentSectionDescriptor.itemIDs[i];
        KPLocalItem *item = self.denormalizedItems[localItemID][@"item"];
        [currentSectionItems addObject:item];
    }
    
    NSLog(@"FNTRACE CAlendarCollectionViewController: %@", indexPath);
    KPLocalItem *itemInCOll = [currentSectionItems objectAtIndex:indexPath.item];
    NSLog(@"FNTRACE CAlendarCollectionViewController: Item ID %ld",(long)itemInCOll.imageAssetID);
    FullScreenContainerViewController *avc = [[FullScreenContainerViewController alloc] initWithItems:currentSectionItems andIndex:indexPath.item withCommentIndex:-1];
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController presentViewController:avc animated:YES completion:nil];
    
//    NSLog(@"it is selecting");
     });
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceHeightForHeaderInSection:(NSInteger)section{
    if ([UIScreen mainScreen].bounds.size.width > 320) {
        return 25;
    }
    return 20;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableView = nil;
    
    if([kind isEqualToString:UICollectionElementKindSectionHeader]){
        
        CalendarSectionHeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:sectionHeaderReuseIdentifier forIndexPath:indexPath];
        
        CalendarSectionDescriptor *currentSectionDescriptor = self.sections[indexPath.section];
        headerView.text = currentSectionDescriptor.description;
        headerView.count = currentSectionDescriptor.itemCount;
        
        reusableView = headerView;
        
        
    }
    
    return reusableView;
    
}



- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    return NO;
}


/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/




@end
