//
// Created by Arik Sosman on 6/4/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FSQCollectionViewAlignedLayout.h"

@interface CalendarCollectionViewLayout : FSQCollectionViewAlignedLayout
@end