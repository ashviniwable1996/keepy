//
//  CalendarCollectionViewController.h
//  Keepy
//
//  Created by Arik Sosman on 6/2/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSQCollectionViewAlignedLayout.h"
// #import <PrintIO/PrintIO.h>

@protocol FSQCollectionViewDelegateAlignedLayout;
@class KPLocalKid;

@interface CalendarCollectionViewController : UICollectionViewController <FSQCollectionViewDelegateAlignedLayout, UIActionSheetDelegate>

@property (strong, nonatomic) KPLocalKid *selectedKid;
@property (strong, nonatomic) NSDictionary *denormalizedItems;
@property (strong, nonatomic) NSArray *unsortedItems;

@property BOOL isFilterActive;
//@property UIEdgeInsets *collectionViewInsets;

@property(nonatomic) CGFloat topInset;
@property(nonatomic) float lastContentOffset;
@property(nonatomic) BOOL *isFirstTimeLoading;
@property(nonatomic) BOOL *isLockedKeepyFeed;

@property (strong, nonatomic) ItemsViewController *itemsVC;

- (void)reloadEverything;

@end
