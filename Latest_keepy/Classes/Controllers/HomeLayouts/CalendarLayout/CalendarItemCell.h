//
//  CalendarItemCell.h
//  Keepy
//
//  Created by Arik Sosman on 6/8/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SDWebImageOperation;

@interface CalendarItemCell : UICollectionViewCell

@property (strong, nonatomic) KPLocalItem *item;
@property (assign, nonatomic) BOOL showFooter;
@property (assign, nonatomic) BOOL showPlayButton;
@property (assign, nonatomic) BOOL showLoadIndicator;

@property (assign, nonatomic) BOOL showAddPrompt;

@property (strong, nonatomic) NSString *footerText;

@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (strong, nonatomic, readonly) UIImageView *addPromptImageView;
@property (weak, nonatomic) id<SDWebImageOperation> imageOperation;

- (void)setAddPromptText:(NSString *)text;

@end
