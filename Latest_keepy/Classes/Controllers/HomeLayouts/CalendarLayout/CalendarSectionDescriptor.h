//
//  CalendarSectionDescriptor.h
//  Keepy
//
//  Created by Arik Sosman on 6/9/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarSectionDescriptor : NSObject

@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic, readonly) NSDate *nextYearStartDate;

@property (strong, nonatomic) KPLocalKid *linkedChild;
@property (strong, nonatomic) NSDictionary *denormalizedItems;

@property (strong, nonatomic) NSArray *itemIDs;

@property NSInteger itemCount;
@property NSInteger rowCount;

@property (strong, nonatomic, readonly) NSString *description; 

- (UIImage *)addPromptImage;
- (NSString *)addPromptText;

@end
