//
//  CalendarSectionDescriptor.m
//  Keepy
//
//  Created by Arik Sosman on 6/9/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "CalendarSectionDescriptor.h"

#import "KPLocalKid.h"
#import "AgeCalculator.h"

@implementation CalendarSectionDescriptor

- (NSString *)description{
    
    if(!self.linkedChild){ // this is based on the Gregorian year
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger year = 0;
        
        NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self.startDate]; // Get necessary date components
        
        year = [components year]; // gives you year
        
        return @(year).stringValue;
        
    }
    
    NSDate *birthdate = [NSDate dateWithTimeIntervalSince1970:self.linkedChild.birthdate];

    NSLog(@"description, today: %@", self.startDate);
    NSInteger age = [AgeCalculator calculateAgeForBirthday:birthdate atDate:self.startDate];
    
    return [NSString stringWithFormat:@"AGE %li", (long)age];
    
    // return @"This is some interesting text";
    
}

- (NSDate *)nextYearStartDate{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *nextYearStartDate;
    [calendar setTimeZone:[NSTimeZone defaultTimeZone]];
    
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self.startDate];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    [components setYear:year + 1];
    [components setMonth:month];
    [components setDay:day];
    
    
   

    
    nextYearStartDate = [calendar dateFromComponents:components];
    NSLog(@"next year start date %@", nextYearStartDate);
    return nextYearStartDate;
    
}

- (UIImage *)addPromptImage{
    
    /*
    if(!self.linkedChild){
        
        return [UIImage imageNamed:@"calendar-add-age-any"];
        
    }
    */
    
    NSDate *birthdate = [NSDate dateWithTimeIntervalSince1970:self.linkedChild.birthdate];
    NSLog(@"add prompt image call, today: %@", self.startDate);
    NSInteger age = [AgeCalculator calculateAgeForBirthday:birthdate atDate:self.startDate];
    
    NSString *imageName = [NSString stringWithFormat:@"calendar-add-age-%li-brown", (long)age];
    UIImage *preliminaryImage = [UIImage imageNamed:imageName];
    
    return preliminaryImage;
    
    /*
    if(preliminaryImage){
        return preliminaryImage;
    }
    
    return [UIImage imageNamed:@"calendar-add-age-any"];
    */
    
}

- (NSString *)addPromptText{
    
    NSLog(@"add prompt text, today: %@", self.startDate);
    NSDate *birthdate = [NSDate dateWithTimeIntervalSince1970:self.linkedChild.birthdate];
    NSInteger age = [AgeCalculator calculateAgeForBirthday:birthdate atDate:self.startDate];
    
    switch (age) {
        case 0:
            return @"birthday";
            
        case 1:
            return @"playground";
            
        case 2:
            return @"artwork";
            
        case 3:
            return @"sports";
            
        case 4:
            return @"outdoors";
            
        case 5:
            return @"achievement";
        
        default:
            return nil;
    }
    
}

@end
