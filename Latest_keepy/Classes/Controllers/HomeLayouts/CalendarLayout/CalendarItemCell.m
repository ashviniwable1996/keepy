//
//  CalendarItemCell.m
//  Keepy
//
//  Created by Arik Sosman on 6/8/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <SDWebImage/SDWebImageOperation.h>
#import "CalendarItemCell.h"

@interface CalendarItemCell()

@property (strong, nonatomic, readwrite) UIImageView *imageView;

@property (strong, nonatomic) UIImageView *playButtonImageView;

@property (strong, nonatomic) UIImageView *footerImageView;
@property (strong, nonatomic) UILabel *footerLabel;
@property (strong, nonatomic) UILabel *promptLabel;

@property (strong, nonatomic, readwrite) UIImageView *addPromptImageView;

@property (strong, nonatomic) UIActivityIndicatorView *loadIndicator;

@end

@implementation CalendarItemCell

- (instancetype)initWithFrame:(CGRect)frame{
    
    if(self = [super initWithFrame:frame]) {
        /*
        CGFloat hue = (arc4random() % 360) / 360.0;
        UIColor *bgColor = [UIColor colorWithHue:hue saturation:1 brightness:0.5 alpha:1];
        self.backgroundColor = bgColor;
        */
        
        UIImage *backgroundImage = [UIImage imageNamed:@"calendar-image-placeholder"];
        UIImageView *bgImageView = [[UIImageView alloc] initWithImage:backgroundImage];
        
        self.backgroundView = bgImageView;
        self.userInteractionEnabled = YES;
        
        CGRect contentFrame = frame;
        contentFrame.origin = CGPointZero;
        
        self.loadIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGSize indicatorSize = self.loadIndicator.frame.size;
        CGFloat indicatorX = (contentFrame.size.width - indicatorSize.width) * 0.5;
        CGFloat indicatorY = (contentFrame.size.height - indicatorSize.height) * 0.5;
        CGRect indicatorFrame = CGRectMake(indicatorX, indicatorY, indicatorSize.width, indicatorSize.height);
        self.loadIndicator.frame = indicatorFrame;
        
        [self.loadIndicator startAnimating];
        [self.contentView addSubview:self.loadIndicator];

        self.imageView = [[UIImageView alloc] initWithFrame:contentFrame];
        [self.contentView addSubview:self.imageView];

        self.playButtonImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play-video-small-gallery"]];
        CGSize playButtonSize = self.playButtonImageView.frame.size;
        CGFloat playButtonX = (contentFrame.size.width - playButtonSize.width) * 0.5;
        CGFloat playButtonY = (contentFrame.size.height - playButtonSize.height) * 0.5;
        CGRect playButtonFrame = CGRectMake(playButtonX, playButtonY, playButtonSize.width, playButtonSize.height);
        
        self.playButtonImageView.userInteractionEnabled = NO;
        self.playButtonImageView.frame = playButtonFrame;
        [self.contentView addSubview:self.playButtonImageView];
        
        
        CGSize footerSize = CGSizeMake(frame.size.width, 17);
        CGPoint footerPosition = CGPointMake(0, contentFrame.size.height - footerSize.height);
        CGRect footerFrame = CGRectMake(footerPosition.x, footerPosition.y, footerSize.width, footerSize.height);
        
        self.footerImageView = [[UIImageView alloc] initWithFrame:footerFrame];
        self.footerImageView.image = [UIImage imageNamed:@"new-thumb-footer-video"];
        self.footerImageView.userInteractionEnabled = NO;
        
        [self.contentView addSubview:self.footerImageView];

        self.footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 55, 50, 17)];
        self.footerLabel.textColor = [UIColor whiteColor];
        self.footerLabel.textAlignment = NSTextAlignmentRight;
        self.footerLabel.backgroundColor = [UIColor clearColor];
        self.footerLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
        self.footerLabel.userInteractionEnabled = NO;
        [self.contentView addSubview:self.footerLabel];

        self.addPromptImageView = [[UIImageView alloc] initWithFrame:contentFrame];
        self.addPromptImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.addPromptImageView];

        self.promptLabel = [UILabel new];
        self.promptLabel.textAlignment = NSTextAlignmentCenter;
        self.promptLabel.textColor = [UIColor whiteColor];
        self.promptLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
        self.promptLabel.userInteractionEnabled = NO;
        [self.contentView addSubview:self.promptLabel];
    }
    
    return self;
}

- (void)setShowAddPrompt:(BOOL)showAddPrompt {
    _showAddPrompt = showAddPrompt;
    
    self.backgroundView.hidden = showAddPrompt;
    self.imageView.hidden = showAddPrompt;
    self.addPromptImageView.hidden = !showAddPrompt;
    self.promptLabel.hidden = !showAddPrompt;
    
    self.backgroundColor = [self determineBackgroundColor];
    self.footerLabel.hidden = ![self determineShouldShowFooter];
    self.footerImageView.hidden = ![self determineShouldShowFooter];

    if (showAddPrompt) {
        // self.footerLabel.hidden = YES;
        // self.footerImageView.hidden = YES;
        self.playButtonImageView.hidden = YES;
    } else {
        
        // self.footerLabel.hidden = !self.showFooter || self.showLoadIndicator;
        // self.footerImageView.hidden = !self.showFooter || self.showLoadIndicator;
        self.playButtonImageView.hidden = !self.showPlayButton || self.showLoadIndicator;
    }
}

- (void)setAddPromptText:(NSString *)text{
    self.promptLabel.text = text;
    [self.promptLabel sizeToFit];
    
    CGRect promptFrame = self.promptLabel.frame;
    CGRect contentFrame = self.frame;
    
    promptFrame.origin.x = 0;
    promptFrame.size.width = contentFrame.size.width;
    promptFrame.origin.y = contentFrame.size.height - promptFrame.size.height - 5;
    
    self.promptLabel.frame = promptFrame;
}

- (void)setShowLoadIndicator:(BOOL)showLoadIndicator {
    _showLoadIndicator = showLoadIndicator;
    
    self.backgroundColor = [self determineBackgroundColor];
    self.footerLabel.hidden = ![self determineShouldShowFooter];
    self.footerImageView.hidden = ![self determineShouldShowFooter];
    
    if (showLoadIndicator) {
        
        self.backgroundView.hidden = YES;
        
        // self.footerLabel.hidden = YES;
        // self.footerImageView.hidden = YES;
        self.playButtonImageView.hidden = YES;
        
        self.loadIndicator.hidden = NO;
        [self.loadIndicator startAnimating];
        
    } else {
        [self.loadIndicator stopAnimating];
        self.loadIndicator.hidden = YES;
        
        self.backgroundView.hidden = NO;
        
        // self.footerLabel.hidden = !self.showFooter || self.showAddPrompt;
        // self.footerImageView.hidden = !self.showFooter || self.showAddPrompt;
        self.playButtonImageView.hidden = !self.showPlayButton || self.showAddPrompt;
    }
}

- (void)setShowFooter:(BOOL)showFooter {
    _showFooter = showFooter;
    
    self.footerImageView.hidden = ![self determineShouldShowFooter];
    self.footerLabel.hidden = ![self determineShouldShowFooter];
    
}

- (void)setShowPlayButton:(BOOL)showPlayButton {
    _showPlayButton = showPlayButton;
    
    self.playButtonImageView.hidden = ![self determineShouldShowPlayButton];
}

- (void)setFooterText:(NSString *)footerText{
    _footerText = footerText;
    
    self.footerLabel.text = footerText;
}

- (UIColor *)determineBackgroundColor{
    /* if(self.showLoadIndicator){
        return [UIColor clearColor];
    } */
    
    if(self.showAddPrompt){
        return [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1];
    } else {
        return [UIColor colorWithRed:231/255.0 green:230/255.0 blue:230/255.0 alpha:1];
    }
}

- (BOOL)determineShouldShowFooter{
    if(!self.showFooter){ return NO; }
    
    if(self.showLoadIndicator || self.showAddPrompt){
        return NO;
    }
    
    return YES;
}

- (BOOL)determineShouldShowPlayButton{
    if(!self.showPlayButton){ return NO; }
    
    if(self.showLoadIndicator || self.showAddPrompt){
        return NO;
    }
    
    return YES;
}

- (void) prepareForReuse {
    [super prepareForReuse];

    if (self.imageOperation) {
        [self.imageOperation cancel];
    }

    self.imageOperation = nil;
}

@end
