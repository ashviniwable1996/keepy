//
//  CalendarSectionHeaderReusableView.m
//  Keepy
//
//  Created by Arik Sosman on 6/8/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "CalendarSectionHeaderReusableView.h"

@interface CalendarSectionHeaderReusableView()

@property (strong, nonatomic) UILabel *textLabel;
@property (strong, nonatomic) UILabel *countLabel;

@end

@implementation CalendarSectionHeaderReusableView

// this is the method that is called when something is dequeued
- (instancetype)initWithFrame:(CGRect)frame{
    
    if(self = [super initWithFrame:frame]){
        
        UIColor *headerBGColor = [UIColor colorWithRed:239/255.0 green:238/255.0 blue:238/255.0 alpha:1];
        UIColor *headerTranslucentBGColor = [UIColor colorWithRed:239/255.0 green:238/255.0 blue:238/255.0 alpha:0.5];
        UIColor *headerTextColor = [UIColor colorWithRed:149/255.0 green:157/255.0 blue:159/255.0 alpha:1];
        
        
        self.backgroundColor = headerTranslucentBGColor;
        
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
        
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        UIVisualEffectView *vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
        
        blurEffectView.tintColor = headerBGColor;
        vibrancyEffectView.tintColor = headerTextColor;
        
        
        
        CGSize headerSize = frame.size;
        blurEffectView.frame = CGRectMake(0, 0, headerSize.width, headerSize.height);
        vibrancyEffectView.frame = CGRectMake(0, 0, headerSize.width, headerSize.height);
        
        blurEffectView.alpha = 1;
        
        [self addSubview:blurEffectView];
        // [headerView addSubview:vibrancyEffectView];
        
        
        self.textLabel = [UILabel new];
        if ([UIScreen mainScreen].bounds.size.width > 320){
            self.textLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:15];

        } else {
            self.textLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];

        }
//        self.textLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
        self.textLabel.textColor = [UIColor colorWithRed:149/255.0 green:157/255.0 blue:159/255.0 alpha:1];

        [self addSubview:self.textLabel];
        
        
        
        self.countLabel = [UILabel new];
        if ([UIScreen mainScreen].bounds.size.width > 320){
            self.countLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:15];

        } else {
            self.countLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
        }
//        self.countLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
        self.countLabel.textColor = [UIColor colorWithRed:149/255.0 green:157/255.0 blue:159/255.0 alpha:1];
        
        [self addSubview:self.countLabel];
        
        
        // [vibrancyEffectView.contentView addSubview:lastSubview];
        
        
        
    }
    
    return self;
    
}

- (void)setText:(NSString *)text{
    
    _text = text;
    
    self.textLabel.text = text;
    [self.textLabel sizeToFit];
    
    
    CGRect textFrame = self.textLabel.frame;
    CGFloat textHeight = textFrame.size.height;
    CGFloat headerHeight = self.frame.size.height;
    
    textFrame.origin.x = 8 + 5; // 8px for the default spacing (1 in Dean's PDF), and 5px additional distance
    textFrame.origin.y = (headerHeight - textHeight) * 0.5;
    
    self.textLabel.frame = textFrame;
    
}

- (void)setCount:(NSInteger)count{
    
    _count = count;
    
    self.countLabel.text = @(count).stringValue;
    [self.countLabel sizeToFit];
    
    
    
    CGRect textFrame = self.countLabel.frame;
    CGFloat textHeight = textFrame.size.height;
    CGFloat textWidth = textFrame.size.width;
    
    CGFloat headerHeight = self.frame.size.height;
    CGFloat headerWidth = self.frame.size.width;
    
    textFrame.origin.x = headerWidth - textWidth - (8 + 5); // 8px for the default spacing (1 in Dean's PDF), and 5px additional distance
    textFrame.origin.y = (headerHeight - textHeight) * 0.5;
    
    self.countLabel.frame = textFrame;
    
    
}

@end
