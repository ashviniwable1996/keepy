//
// Created by Arik Sosman on 6/4/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "CalendarCollectionViewLayout.h"


@implementation CalendarCollectionViewLayout {


    
}

- (instancetype)init {

    self = [super init];
    if (self) {

        self.sectionSpacing = 0;
        self.contentInsets = UIEdgeInsetsZero;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        contentInsets.left = 8;
        contentInsets.right = 8;
        contentInsets.top = 6;
        contentInsets.bottom = 6;
        
      //  contentInsets = UIEdgeInsetsZero;
        
        
//        self.defaultCellSize = CGSizeMake(73, 73);
        self.defaultCellSize = CGSizeMake(([UIScreen mainScreen].bounds.size.width - 28) / 4, ([UIScreen mainScreen].bounds.size.width - 28) / 4);
        FSQCollectionViewAlignedLayoutCellAttributes *attributes = [FSQCollectionViewAlignedLayoutCellAttributes withInsets:UIEdgeInsetsZero shouldBeginLine:NO shouldEndLine:NO startLineIndentation:NO];
        
        self.defaultCellAttributes = attributes;
        
        self.defaultSectionAttributes = [FSQCollectionViewAlignedLayoutSectionAttributes withHorizontalAlignment:FSQCollectionViewHorizontalAlignmentLeft verticalAlignment:FSQCollectionViewVerticalAlignmentTop itemSpacing:4 lineSpacing:4 insets:contentInsets];
        
        self.shouldPinSectionHeadersToTop = YES;
        
    }

    return self;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}


@end
