//
//  FamilyViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface FamilyViewController : UIViewController <UINavigationControllerDelegate>

@property(nonatomic, strong) UIImage *backgroundImage;
@property(nonatomic, weak) IBOutlet UIView *containerView;
@property(nonatomic, weak) UIViewController *previousViewController;
@property(nonatomic, weak) UIViewController *nextViewController;
@property(nonatomic, copy) NSString *nextIdentifier;

@end

@interface FamilySegue : UIStoryboardSegue

@end
