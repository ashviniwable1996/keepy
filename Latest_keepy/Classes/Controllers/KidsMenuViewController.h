//
//  KidsMenuViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/14/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KidsMenuViewController : UIViewController

@property(nonatomic, strong) NSArray *specificKids;

+ (BOOL)isKeepyFeedActive;
@end
