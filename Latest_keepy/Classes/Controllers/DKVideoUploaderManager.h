//
//  DKVideoUploaderManager.h
//  Keepy
//
//  Created by Daniel Karsh on 4/25/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AppDelegate;
@class KPLocalItem;
@class KPTextView;

@interface DKVideoUploaderManager : NSObject {
    AppDelegate *appDelegate;
}

@property(nonatomic, strong) NSURL *myMovieFileURL;
@property(nonatomic, strong) UIImage *thumbnail;
@property(nonatomic, strong) UIView *uploadProgressView;
@property (nonatomic, strong) UIViewController* viewController;
@property(nonatomic, strong) NSString *myID;
@property(copy) NSString *duration;
@property(copy) NSString *short_url;
@property(assign) BOOL isValidItem;

- (void)setItem:(Item *)titem
         setKid:(Kid *)tkid
       setPlace:(Place *)tplace
     setTitleTF:(KPTextView *)ttitleTF
setSelectedKids:(NSMutableArray *)tselectedKids
   setVideoInfo:(NSDictionary *)tvideoInfo
    setItemDate:(NSDate *)titemDate
      setSwitch:(BOOL)tshareSwitch
      setTagIds:(NSArray *)ttagIds
withSuccessItem:(APISuccessBlock)apiB;

- (void)setOnlyProccessViewForItem:(KPLocalItem *)item;

- (void)startToCheckVideoStatusAssetID:(NSString *)assetId;

- (void)prepareAssetOriginalImage;

- (void)convertVideoToLowQualityWithInputURL;

- (void)zeroOutTheProgressBar;

- (void)killMe;

enum {
    DKVideoStatusPrepareVideoAsset,
    DKVideoStatusPrepareVideoAssetFail,
    DKVideoStatusPrepareVideoAssetDone,
    DKVideoStatusCompressed,
    DKVideoStatusCompressedFail,
    DKVideoStatusUploadingToServer,
    DKVideoStatusUploaded,
    DKVideoStatusUploadingFail,
    DKVideoStatusUploadingCommited,
    DKVideoStatusUploadingCommitedFail,
    DKVideoStatusAdded,
    DKVideoStatusAddedFail,
    DKVideoStatusSDReady
};

typedef NSInteger DKVideoStatus;

@property(nonatomic) DKVideoStatus videoStat;
@property(nonatomic) float startFromHere;


@end
