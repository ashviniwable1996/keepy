//
//  CircleCropViewController.h
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol CircleCropViewControllerDelegate;

@interface CircleCropViewController : UIViewController

@property(nonatomic, weak) id <CircleCropViewControllerDelegate> delegate;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic) BOOL parentIsEditKidViewController;
@property(nonatomic, assign) CGFloat scale;
@property(nonatomic, assign) CGPoint originPoint;

@end

@protocol CircleCropViewControllerDelegate <NSObject>


- (void)doneCircleCropping:(NSDictionary *)selectedPhotosDict;

- (void)cancelCircleCropping;
@end