//
//  ItemEditViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "ItemEditViewController.h"

@import Photos;
@import AVFoundation;

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "CameraViewController.h"
#import "RecordViewController.h"
#import "PlaceEditViewController.h"
#import "ImageEditorViewController.h"
#import "KPDataParser.h"
#import "EditKidViewController.h"
#import "KPSelectionPopViewController.h"
#import "ItemShareViewController.h"
#import "NoteEditorViewController.h"
#import "SettingsViewController.h"

#import "GalleryPickerViewController.h"
#import "ELCAlbumPickerController.h"
#import "TagsViewController.h"

#import "BirthDate2ViewController.h"
#import "PlacesViewController.h"
#import "DKVideoUploaderManager.h"
#import "DKMoviePlayerViewController.h"
#import "DKMultyUploaderManger.h"
#import "KPPhotoLibrary.h"

#import "KPLocalItem.h"
#import "KPLocalKid.h"
#import "KPLocalPlace.h"
#import "KPLocalKidItem.h"
#import "KPLocalItemTag.h"
#import "KPLocalTag.h"
#import "KPLocalStory.h"
#import "KPLocalComment.h"
#import "KPLocalAsset.h"
#import "AgeCalculator.h"

#import "CachingAssistant.h"

#import "Keepy-Swift.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface ItemEditViewController () <RecordViewControllerDelegate, AVAudioPlayerDelegate, PlaceEditDelegate, UITextFieldDelegate, ImageEditorDelegate, EditKidDelegate, KPSelectionDelegate, UIActionSheetDelegate, NoteEditorDelegate, GalleryPickerDelegate, UITableViewDataSource, UITableViewDelegate, TagViewDelegate, UITextViewDelegate>

@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic, strong) KPLocalKid *kid;
@property(nonatomic, strong) KPLocalPlace *place;
@property(nonatomic, strong) NSDate *itemDate;
@property(nonatomic, strong) AVAudioPlayer *player;
@property(nonatomic) BOOL kidChanged;
@property(nonatomic, strong) UIImage *changedImage;
@property(nonatomic, strong) NSDate *oldDate;
@property(nonatomic, strong) NSDictionary *imageInfo;
@property(nonatomic, strong) NSDictionary *videoInfo;
@property(nonatomic, strong) NSNumber *videoId;

@property(nonatomic, strong) NSData *storyRecordingData;

@property(nonatomic, strong) IBOutlet UIButton *recordStoryButton;
@property(nonatomic, strong) IBOutlet UIButton *deleteStoryButton;
@property(nonatomic, strong) IBOutlet UIButton *kidButton;
@property(nonatomic, strong) IBOutlet UIButton *dateButton;
@property(nonatomic, strong) IBOutlet UIButton *placeButton;
@property(nonatomic, strong) IBOutlet KPTextView *titleTF;
@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *storyLoadingIndicator;
@property(nonatomic, strong) IBOutlet UILabel *kidNameLabel;
@property(nonatomic, strong) IBOutlet UILabel *editPhotoLabel;
@property(nonatomic, strong) IBOutlet UIImageView *placeMarker;
@property(nonatomic, strong) IBOutlet UILabel *placeNearLabel;
@property(nonatomic, strong) IBOutlet UIImageView *photoImageView;

@property(nonatomic, strong) UILabel *storyTimeCurrentLabel;
@property(nonatomic, strong) UILabel *storyTimeEndLabel;
@property(nonatomic, strong) UIImageView *progressBkg;
@property(nonatomic, strong) UIImageView *progressFiller;
@property(nonatomic, strong) UIView *playerView;
@property(nonatomic, strong) UIButton *knobButton;
@property(nonatomic, strong) NSTimer *audioTimer;
@property(nonatomic, strong) UIButton *playButton;
@property(nonatomic, strong) UISwitch *shareSwitch;
@property(nonatomic, strong) UISwitch *HDSwitch;

@property(nonatomic) BOOL storyChanged;
@property(nonatomic) BOOL textNeedUpdate;

@property(nonatomic, strong) DKVideoUploaderManager *uploadVideo;
@property(nonatomic, strong) KPSelectionPopViewController *selectionPopVC;
@property(nonatomic, strong) UIScrollView *thumbsScrollView;
@property(nonatomic, strong) UINavigationController *addPhotoNavController;

@property(nonatomic, strong) NSMutableArray *imagesInfos;

@property(nonatomic) NSInteger mode;
@property(assign) BOOL comeFromEdit;
@property(nonatomic) float TextcellHeight;
@property(nonatomic) NSInteger recordingLength;

@property(nonatomic) BOOL shareSwitchWaitingForFans;
@property(nonatomic, strong) NSArray *tagIds;
@property(nonatomic, strong) NSMutableArray *kidsArray;
@property(nonatomic) BOOL tagsChanged;

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) UILabel *tagsStrLabel;
@property(nonatomic, strong) NSString *tagsStr;

@property(nonatomic, strong) NSData *videoStoryRecordingData;
@property(nonatomic, strong) UIButton *recordVideoStoryButton;
@property(nonatomic) NSInteger videoRecordingLength;
@property(nonatomic) BOOL videoStoryChanged;
@property(nonatomic, strong) UIImage *videoStoryPreviewImage;

@property(nonatomic, strong) KPLocalStory *storyToDelete;
@property(nonatomic, strong) KPLocalComment *videoStoryToDelete;

@property(nonatomic) NSInteger itemAddStatus;
@property(nonatomic) BOOL isNewItem;
@property(nonatomic) BOOL videoIsReady;
@property(nonatomic) BOOL isAddVideo;

@property(nonatomic, strong) IBOutlet NSMutableArray *kidButtons;
@property(nonatomic, strong) IBOutlet NSMutableArray *kidNameLabels;

// local IDs of the selected children
@property(nonatomic, strong) IBOutlet NSMutableArray *selectedKidIDs;

@property(strong, nonatomic) UIView *uploadProgressView;

@property(nonatomic, strong) NSMutableDictionary *editedImageInfoExtraParams;


- (IBAction)recordStoryTap:(id)sender;

- (IBAction)deleteStoryTap:(id)sender;

- (IBAction)kidTap:(id)sender;

- (IBAction)dateTap:(id)sender;

- (IBAction)placeTap:(id)sender;

- (IBAction)editTap:(id)sender;

@end

@implementation ItemEditViewController


- (instancetype)initWithVideoInfo:(NSDictionary *)videoInfo {
    self = [super initWithNibName:@"ItemEditViewController" bundle:nil];
    if (self) {
        self.isNewKid = NO;
        self.videoInfo = videoInfo;
        self.imageInfo = nil;
    }
    return self;
}

- (instancetype)initWithItem:(KPLocalItem *)item andMode:(NSInteger)mode {
    self = [super initWithNibName:@"ItemEditViewController" bundle:nil];
    if (self) {
        
        // the item might have to be reloaded
        [item reload];
        self.isNewKid = NO;
        self.item = item;

        // DDLogInfo(@"%@",self.item.place);

        //              DLog(@">>>>>>>>> item: %@ cropRect %@", self.item.itemId,  self.item.cropRect);

        //DDLogInfo(@"%d",item.kids.count);
        self.mode = mode;
        [self setComeFromEdit:YES];

    }
    return self;
}

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo {
    self = [super initWithNibName:@"ItemEditViewController" bundle:nil];
    if (self) {
        self.imageInfo = imageInfo;
        self.videoInfo = nil;
        self.isNewKid = NO;
    }
    return self;
}

- (instancetype)initWithImagesInfos:(NSArray *)imagesInfos {
    self = [super initWithNibName:@"ItemEditViewController" bundle:nil];
    if (self) {
        self.imagesInfos = [[NSMutableArray alloc] initWithArray:imagesInfos];
        self.videoInfo = nil;
        self.isNewKid = NO;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isNewKid = NO;
        // Custom initialization
    }
    return self;
}

- (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width {
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height;
}

- (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font thatFitsToSize:(CGSize)size {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        CGRect frame = [text boundingRectWithSize:size
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:@{NSFontAttributeName : font}
                                          context:nil];
        return frame.size;
    }
    else {
        return [text sizeWithFont:font thatFitsToSize:size];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];


    if ([[UserManager sharedInstance] isAfterRegistration]) {
        [[Mixpanel sharedInstance] track:@"onboarding-item-edit-open"];
    }


    self.title = KPLocalizedString(@"add story");
    if (self.item != nil) {
        self.title = KPLocalizedString(@"edit story");
        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
//        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

        [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source" : @"edit-story"}];
        self.selectedKidIDs = (id)itemKidIDs;
    }
    else {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:@{@"source" : @"add-story"}];


        
        
        // self.selectedKids = [[NSMutableArray alloc] initWithCapacity:[[[UserManager sharedInstance] myKids] count]];
        self.selectedKidIDs = @[].mutableCopy;

        
        NSArray *myKids = [KPLocalKid fetchMine];
        if(myKids.count == 1){ // if there's only one kid
            KPLocalKid *firstKid = myKids[0];
            [self.selectedKidIDs addObject:@(firstKid.identifier)];
        }
        
        
        // [self.selectedKids addObject:[[[UserManager sharedInstance] myKids] firstObject]];
    }

    //DDLogInfo(@"%@",self.selectedKids);

    if (self.mode == 1) {
        [self setButtons:@{@"title" : KPLocalizedString(@"cancel"), @"buttonType" : @(BUTTON_TYPE_NORMAL)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"save"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(self.item == nil)}];
    } else {
        [self setButtons:@{@"title" : KPLocalizedString(@"back"), @"buttonType" : @(BUTTON_TYPE_BACK)} andRightButtonInfo:@{@"title" : KPLocalizedString(@"save"), @"buttonType" : @(BUTTON_TYPE_PRIME), @"disabled" : @(self.item == nil)}];
    }

    //Array of sorted kids by birthdays
    // _kidsArray =  (NSMutableArray*)[[UserManager sharedInstance] myKids];
    self.kidsArray = (id)[KPLocalKid fetchMine];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"birthdate" ascending:NO];
    NSArray *orderedArray = [self.kidsArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    self.kidsArray = orderedArray.mutableCopy;
    
    
    if (self.item == nil) {
        
        //if fan want to load image but not have kid yet
        if ([_kidsArray count] == 0) {
            [self dismissViewControllerAnimated:NO completion:nil];
            return;
        }
        
        // we do not want the first kid selected by default now that multitagging is enabled
        // [self.selectedKids addObject:_kidsArray.firstObject];
    }
    
    CGSize size = [self text:self.item.title sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17] thatFitsToSize:CGSizeMake(310, 999)];
    
    
    _TextcellHeight = size.height >= 44.0f ? size.height + 42 : 44.0f;
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollsToTop = YES;
    self.tableView.backgroundView = nil;
    
    self.shareSwitch = [[UISwitch alloc] init];
    self.photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 82, 82)];
    
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    self.tableView.separatorColor = [UIColor colorWithRed:0.447 green:0.361 blue:0.325 alpha:1];
    [self.view addSubview:self.tableView];
    
    [self.tableView reloadData];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gesture];
    
    if (self.item != nil) {
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        NSMutableArray *tagsNames = [[NSMutableArray alloc] init];
        
        NSArray *tagIDs = [KPLocalItemTag fetchTagIDsForItem:self.item];
        NSArray *itemTags = [KPLocalTag idsToObjects:tagIDs];
        
        for (KPLocalTag *tag in itemTags) {
            [arr addObject:@(tag.serverID)];
            [tagsNames addObject:tag.name];
        }
        
        self.tagIds = arr;
        
        self.tagsStr = [tagsNames componentsJoinedByString:@", "];
        
        if ([tagsNames count] == 0) {
            self.tagsStr = KPLocalizedString(@"select");
        }
        
        
    }
    
    
    
    // properly initialize place
    if(self.item){
        self.place = [KPLocalPlace fetchByLocalID:self.item.placeID];
    }
    
    
    
    // DDLogInfo(@"%@",self.item);
    
    if (self.item != nil) {
        BOOL share = (self.item.shareStatus == 1);
        [self.shareSwitch setOn:share];
    }
    else {
        [self.shareSwitch setOn:YES];
    }

    [self.navigationController setNavigationBarHidden:NO];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardDidShow:)
                   name:UIKeyboardDidShowNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];



}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    // if (self.shareSwitchWaitingForFans)
    //   self.shareSwitch.on = ([[[UserManager sharedInstance] myFans] count] > 0);

    self.navigationController.view.tag = 0;
   
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    if (self.storyToDelete != nil) {
//        self.item.storyID = self.storyToDelete.identifier;
//    }

    if (self.videoStoryToDelete != nil) {
        self.videoStoryToDelete.isStory = YES;
    }
}

- (void)viewDidLayoutSubviews {
    self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    self.uploadProgressView.frame = CGRectMake(9, 64, 302, 35);
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)refreshImageInfo:(NSDictionary *)imageInfo {
    self.imageInfo = imageInfo;
    self.videoInfo = nil;
    self.changedImage = [self.imageInfo valueForKey:@"resultImage"];
    self.photoImageView.image = self.changedImage;
}


- (void)refreshVideoInfo:(NSDictionary *)videoInfo {
    self.imageInfo = nil;
    self.videoInfo = videoInfo;
    self.changedImage = [self.videoInfo valueForKey:@"thumbImage"];
    self.photoImageView.image = self.changedImage;
}


- (void)reloadData {
    if (self.item == nil) {
        if (self.imageInfo) {
            self.photoImageView.image = [self.imageInfo valueForKey:@"resultImage"];
            if ([self.imageInfo valueForKey:@"extraData"] != nil) {
                NSError *e;
                NSDictionary *d = [NSJSONSerialization JSONObjectWithData:[[self.imageInfo valueForKey:@"extraData"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];


                if ([d valueForKey:@"{Exif}"] != nil && [[d valueForKey:@"{Exif}"] valueForKey:@"DateTimeOriginal"] != nil) {
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                    dateFormatter.dateFormat = @"y:MM:dd HH:mm:ss";
                    self.itemDate = [dateFormatter dateFromString:[[d valueForKey:@"{Exif}"] valueForKey:@"DateTimeOriginal"]];

                    //
                }
            }

            if (self.imageInfo[@"originalMetadata"]) {

                NSDictionary *originalMetaData = self.imageInfo[@"originalMetadata"];

                NSDictionary *exifData = originalMetaData[@"{Exif}"];
                NSString *exifCreationDateString = exifData[@"DateTimeOriginal"];
                if (exifCreationDateString) {
                    NSDateFormatter *dateFormatter = [NSDateFormatter new];
                    dateFormatter.dateFormat = @"y:MM:dd HH:mm:ss";
                    NSDate *creationDate = [dateFormatter dateFromString:exifCreationDateString];
                    self.itemDate = creationDate;
                }

            }

        }
        else {

            // DDLogInfo(@"%@",self.videoInfo);

            if ([self.videoInfo valueForKey:@"VideoDateCreated"] != nil) {
                self.itemDate = (NSDate *) [self.videoInfo valueForKey:@"VideoDateCreated"];
            }


            self.photoImageView.image = [self.videoInfo valueForKey:@"thumbImage"];
        }


        self.itemDate = self.itemDate == nil ? [NSDate date] : self.itemDate;

        //DDLogInfo(@"%@",self.itemDate);
        //JIRA IPHONE-52
        //The date should always revert to the current date
        // self.itemDate = [NSDate date];

        /* if ([GlobalUtils getLastSelectedDate] == nil) {
             self.itemDate = [NSDate date];
         }
         else {
             self.itemDate = [GlobalUtils getLastSelectedDate];
         }*/



        /*
        Kid *k = [GlobalUtils getLastSelectedKid];
        if (k && k.name && k.name.length > 0) {
            self.kid = k;
        } else {
            NSArray *kids = [[UserManager sharedInstance] myKids];
            if ([kids count] == 1) {
                k = (Kid*)[kids objectAtIndex:0];
            }
        }
        
        if (!(k && k.name && k.name.length > 0)) {
            [self.kidButton setImage:[UIImage imageNamed:@"add-kid-kidsNav"] forState:UIControlStateNormal];
            self.kidNameLabel.text = KPLocalizedString(@"kid");
        } else {
            self.kid = k;
        }
        
        Place *p = nil;
        p = [GlobalUtils getLastSelectedPlace];
        if (k != nil)
        {
            self.place = p;
        }
        */

        for (UIButton *kidButton in self.kidButtons) {
            kidButton.alpha = 0.3f;
        }
        for (UILabel *kidNameLabel in self.kidNameLabels) {
            kidNameLabel.alpha = 0.3f;
        }

        if (self.selectedKidIDs.count > 0) {
            NSUInteger itemKidsCount = self.selectedKidIDs.count;
            NSArray *selectedKids = [KPLocalKid idsToObjects:self.selectedKidIDs];

            for (int i = 0; i < itemKidsCount; i++) {

                KPLocalKid *kid = selectedKids[i];

                for (UIButton *kidButton in self.kidButtons) {
                    if (kidButton.tag == kid.serverID) {
                        kidButton.alpha = 1.0f;
                    }
                }
                for (UILabel *kidNameLabel in self.kidNameLabels) {
                    if (kidNameLabel.tag == kid.serverID) {
                        kidNameLabel.alpha = 1.0f;
                    }
                }
            }
        }

        if (self.selectedKidIDs.count > 0) {
            self.rightButtonEnabled = YES;

        } else {
            self.rightButtonEnabled = NO;
        }

        return;
    }


    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [self.item.title stringByTrimmingCharactersInSet:charSet];
    if (![trimmedString isEqualToString:@""]) {
        // it's empty or contains only white spaces
        self.titleTF.text = self.item.title;
    }


    if (self.item.itemDate) {
        self.itemDate = [NSDate dateWithTimeIntervalSince1970:self.item.itemDate];
    }

    for (UIButton *kidButton in self.kidButtons) {
        kidButton.alpha = 0.3f;
    }
    for (UILabel *kidNameLabel in self.kidNameLabels) {
        kidNameLabel.alpha = 0.3f;
    }

    if (self.selectedKidIDs.count > 0) {
        NSUInteger itemKidsCount = self.selectedKidIDs.count;
        NSArray *selectedKids = [KPLocalKid idsToObjects:self.selectedKidIDs];

        for (int i = 0; i < itemKidsCount; i++) {
            KPLocalKid *kid = selectedKids[i];
            for (UIButton *kidButton in self.kidButtons) {
                if (kidButton.tag == kid.serverID) {
                    kidButton.alpha = 1.0f;
                }
            }
            for (UILabel *kidNameLabel in self.kidNameLabels) {
                if (kidNameLabel.tag == kid.serverID) {
                    kidNameLabel.alpha = 1.0f;
                }
            }
        }

        if (self.selectedKidIDs.count > 0) {


            self.rightButtonEnabled = YES;

        } else {
            self.rightButtonEnabled = NO;
        }

//        self.kid = [self.item.kids.allObjects objectAtIndex:0];
//        if (self.kid.image == nil)
//            [self.kidButton setImage:[GlobalUtils getKidDefaultImage:self.kid.gender.intValue] forState:UIControlStateNormal];
//        else
//            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], self.kid.image.urlHash]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                
//                [self.kidButton setImage:[image imageMaskedWithElipse:CGSizeMake(290, 290)] forState:UIControlStateNormal];
//            }];
//        
//        
//        self.kidNameLabel.text = self.kid.name;

    } else {
        self.rightButtonEnabled = NO;
    }

    KPLocalPlace *itemPlace = [KPLocalPlace fetchByLocalID:self.item.placeID];

    if (itemPlace != nil || self.place != nil) {
        [self.placeButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]];
        self.placeButton.titleLabel.alpha = 1.0;
        if(!self.place) {
            
            [self.placeButton setTitle:itemPlace.title forState:UIControlStateNormal];
        } else {
            
            [self.placeButton setTitle:self.place.title forState:UIControlStateNormal];
        }
        


    }

    if (self.changedImage) {
        self.photoImageView.image = self.changedImage;
    }
    else {

        KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];

        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_medium.jpg", Defaults.assetsURLString, itemImage.urlHash]]];
        DDLogInfo(@"%@", [NSString stringWithFormat:@"%@%@_medium.jpg", Defaults.assetsURLString, itemImage.urlHash]);
    }


    self.tagsStrLabel.text = self.tagsStr;

    if (![self.tagsStrLabel.text isEqualToString:@"select"] && self.tagsStrLabel != nil) {
        self.tagsStrLabel.alpha = 1.0f;
        [self.tagsStrLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]];
    }



    // DDLogInfo(@"%d",self.item.sharestatus.intValue);
    // self.shareSwitch.on = self.item.sharestatus.intValue == 1 ? YES : NO;

    [self setVoiceRecordingButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)recordStoryTap:(id)sender {
    RecordViewController *rvc = nil;

    if (self.storyChanged && self.storyRecordingData != nil) {
        rvc = [[RecordViewController alloc] initWithData:self.storyRecordingData andLength:self.recordingLength andMode:0];
    } else {
        rvc = [[RecordViewController alloc] initWithItem:(id)self.item withMode:0];
    }

    rvc.delegate = self;
    rvc.itemImage = self.photoImageView.image;

    [self.navigationController pushViewController:rvc animated:YES];
}

- (void)recordVideoStoryTap:(id)sender {
    RecordViewController *rvc = nil;

    if (self.videoStoryChanged && self.videoStoryRecordingData != nil) {
        rvc = [[RecordViewController alloc] initWithData:self.videoStoryRecordingData andLength:self.videoRecordingLength andMode:1];
    } else {
        rvc = [[RecordViewController alloc] initWithItem:(id)self.item withMode:1];
    }

    rvc.delegate = self;
    //if (self.item == nil)
    rvc.itemImage = self.photoImageView.image;

    [self.navigationController pushViewController:rvc animated:YES];
}

- (IBAction)deleteStoryTap:(id)sender {
    UIActionSheet *asheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:KPLocalizedString(@"cancel") destructiveButtonTitle:KPLocalizedString(@"delete recording") otherButtonTitles:nil, nil];
    [asheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != 0) {
        return;
    }

    if (self.item == nil) {
        self.storyChanged = YES;
        self.storyRecordingData = nil;
        [self setVoiceRecordingButtons];
        return;
    }

    KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];

    [SVProgressHUD showWithStatus:KPLocalizedString(@"deleting")];
    [[ServerComm instance] deleteStory:itemStory.serverID withSuccessBlock:^(id result) {
        [SVProgressHUD dismiss];
        self.item.storyID = 0;
        self.storyRecordingData = nil;
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
        [self setVoiceRecordingButtons];
        self.storyChanged = YES;

        [[Analytics sharedAnalytics] track:@"item-delete" properties:@{@"itemType" : @"voice-over"}];

        [GlobalUtils updateAnalyticsUser];

    }                     andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
    }];
}

- (IBAction)placeTap:(id)sender {
    
    NSArray *places = [KPLocalPlace fetchAll];
    
    // if ([[[UserManager sharedInstance] myPlaces] count] == 0) {
    if(places.count < 1){
        PlaceEditViewController *avc = [[PlaceEditViewController alloc] initWithPlace:nil];
        avc.delegate = self;
        [self.navigationController pushViewController:avc animated:YES];
        return;
    }

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PlacesViewController *placesViewController = (PlacesViewController *) [mainStoryboard instantiateViewControllerWithIdentifier:@"PlacesViewController"];
    placesViewController.places = places;
    placesViewController.selectedPlace = self.place;
    [self.navigationController presentViewController:placesViewController animated:YES completion:nil];

    /*
    CGPoint p = CGPointMake(self.placeButton.center.x-30, self.placeButton.frame.origin.y+self.placeButton.frame.size.height);
    p = [self.tableView convertPoint:p toView:self.view];
    
    self.selectionPopVC = [[KPSelectionPopViewController alloc] initWithItems:[[UserManager sharedInstance] myPlaces] withSelectedIndex:-1 andPoint:p];
    self.selectionPopVC.delegate = self;
    self.selectionPopVC.view.tag = 1;
    
    
    [self.view addSubview:self.selectionPopVC.view];
    */
}

- (IBAction)places2Exit:(UIStoryboardSegue *)segue {
    PlacesViewController *placesViewController = (PlacesViewController *) segue.sourceViewController;
    if (self.item.placeID == placesViewController.selectedPlace.identifier) {
        return;
    }

    [[Mixpanel sharedInstance] track:@"Change Place"];

    self.place = placesViewController.selectedPlace;
}

- (IBAction)places2ExitThenAdd:(UIStoryboardSegue *)segue {
    [self performSelector:@selector(waitForIt) withObject:nil afterDelay:0.5f];
}

- (void)waitForIt {

    [[Mixpanel sharedInstance] track:@"add place tap" properties:@{@"source" : @"add story"}];

    PlaceEditViewController *avc = [[PlaceEditViewController alloc] initWithPlace:nil];
    avc.delegate = self;
    [self.navigationController pushViewController:avc animated:YES];
}

- (void)topBtnTap:(UIButton *)sender {

    self.rightButtonEnabled = NO;

    if (sender.tag == 1) //Ok
    {
        if (self.itemAddStatus > 0) {
            if (self.storyChanged && self.itemAddStatus == 1) {
                [self uploadStory:self.isNewItem];
            }
            else if (self.videoStoryChanged) {
                [self uploadVideoStory:self.isNewItem];
            }
        }
        else {
            if (self.item == nil) {
                if (self.videoInfo) {
                    self.rightButtonEnabled = NO;

                    DKVideoUploaderManager *uploader =
                            (DKVideoUploaderManager *) [[[DKMultyUploaderManger sharedClient] allVideosUploadingProccess] lastObject];

                    __weak DKVideoUploaderManager *_uploader = uploader;


                    [SVProgressHUD show];

                    BOOL shareSwitchValue = NO;

                    if (self.shareSwitch && self.shareSwitch.on) {
                        shareSwitchValue = YES;
                    }

                    NSArray *selectedKids = [KPLocalKid idsToObjects:self.selectedKidIDs];
                    
                    [uploader setViewController:self];

                    [uploader setItem:(id)self.item
                               setKid:(id)self.kid
                             setPlace:(id)self.place
                           setTitleTF:self.titleTF
                      setSelectedKids:(id)selectedKids
                         setVideoInfo:self.videoInfo
                          setItemDate:self.itemDate
                            setSwitch:shareSwitchValue
                            setTagIds:self.tagIds
                      withSuccessItem:^(id result) {
                          [SVProgressHUD dismiss];
                          if (result) {
                              ItemShareViewController *avc =
                                      [[ItemShareViewController alloc] initWithItem:result isAfterAddItem:YES];
                              avc.short_url = _uploader.short_url;
                              [self.navigationController pushViewController:avc animated:YES];


                          } else {
                              //error no item;///
                              self.rightButtonEnabled = YES;
                          }

                      }];


                } else {
                    [self addNewItem];
                }
            }
            else {
                [self updateItem];
            }
        }
    }
    else //Cancel
    {
        if (self.storyChanged) {
            [self.delegate itemEditFinished];
        } else {
            [self.delegate itemEditCancelled];
        }

        if (self.item == nil) {

            [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-story", @"result" : @"cancel"}];
        } else {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"edit-story", @"result" : @"cancel"}];
        }

        if (self.mode == 1) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
   
}


- (void)viewVideo:(id)sender {
    if (self.item) {

        KPLocalAsset *itemOriginalImage = [KPLocalAsset fetchByLocalID:self.item.originalImageAssetID];


        NSString *fileName = itemOriginalImage.urlHash;
        NSURL *streamURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.mp4", Defaults.assetsURLString, fileName]];
        DKMoviePlayerViewController *avc = [DKMoviePlayerViewController sharedClient];
        [[DKMoviePlayerViewController sharedClient] playMovieStream:streamURL bufferReady:^(BOOL ready) {

        }];
        [self.navigationController pushViewController:avc animated:YES];
    } else {
        if (self.videoInfo) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


- (IBAction)editTap:(id)sender {
    [self.titleTF resignFirstResponder];

    if (self.item == nil) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }

    if (self.item.itemType == 4 || self.videoInfo) {
        [self viewVideo:sender];
        return;
    }

    if (self.item.itemType == 1 || self.item.itemType == 2) //note
    {
        NoteEditorViewController *avc = [[NoteEditorViewController alloc] initWithItem:(id)self.item];
        avc.delegate = self;
        [self.navigationController pushViewController:avc animated:YES];
    }
    else {
        ImageEditorViewController *avc = [[ImageEditorViewController alloc] initWithItem:(id)self.item];
        avc.delegate = self;
        [self.navigationController pushViewController:avc animated:YES];
    }

}

- (void)photoImageTap:(UITapGestureRecognizer *)gesture {
    [self editTap:nil];
}

- (IBAction)kidTap:(UIButton *)kidButton {
    [self.titleTF resignFirstResponder];
    NSArray *myKids = self.kidsArray;
    for (KPLocalKid *kid in myKids) {
        if (kid.serverID == kidButton.tag) {
            // @HACK to make multi tagging children a toggle. One kid only for now

            if ([self.selectedKidIDs containsObject:@(kid.identifier)]) {
                [self.selectedKidIDs removeObject:@(kid.identifier)];
            } else {
                [self.selectedKidIDs addObject:@(kid.identifier)];
            }

            // [self.selectedKids removeAllObjects];
            // [self.selectedKids addObject:kid];
            break;
        }
    }
    [self reloadData];

    /*
    [self.titleTF resignFirstResponder];
    NSArray *myKids = [[UserManager sharedInstance] myKids];
    for (Kid *kid in myKids) {
        if (kid.kidId.intValue == kidButton.tag) {
            // @HACK to make multi tagging children a toggle. One kid only for now
            //[self.selectedKids removeAllObjects];
            
            if (![self.selectedKids containsObject:kid]) {
                 [self.selectedKids addObject:kid];
            }
            else{
                [self.selectedKids removeObject:kid];
            }
           
            break;
        }
    }
    [self reloadData];*/


}

- (IBAction)dateTap:(id)sender {
    [self.titleTF resignFirstResponder];
    UIStoryboard *onboardStoryboard = [UIStoryboard storyboardWithName:@"Onboard2" bundle:nil];
    BirthDate2ViewController *birthDate2ViewController = (BirthDate2ViewController *) [onboardStoryboard instantiateViewControllerWithIdentifier:@"BirthDate2ViewController"];

    if (self.itemDate == nil) {
        self.itemDate = [NSDate date];
    }
    birthDate2ViewController.theTitleString = KPLocalizedString(@"when");
    birthDate2ViewController.birthDateDate = self.itemDate;
    [self.navigationController presentViewController:birthDate2ViewController animated:YES completion:nil];
}

- (IBAction)birthDate2Exit:(UIStoryboardSegue *)segue {
    BirthDate2ViewController *birthDate2ViewController = (BirthDate2ViewController *) segue.sourceViewController;
    if (birthDate2ViewController.birthDateDate) {
        self.itemDate = birthDate2ViewController.birthDateDate;
    }
}

- (void)deleteTap:(id)sender {
    NSString *title = (self.videoInfo || self.item.itemType == 4) ? KPLocalizedString(@"delete video") : KPLocalizedString(@"delete photo");

    UIAlertController* ac = [UIAlertController alertControllerWithTitle:title
                                                                message:NSLocalizedString(@"are you sure?", comment: @"")
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"no" style:UIAlertActionStyleCancel handler:nil]];
    [ac addAction:[UIAlertAction actionWithTitle:@"delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self deleteItem];
    }]];
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)deleteItem {
        if (self.item == nil) {

            if ([[UserManager sharedInstance] isAfterRegistration]) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
                // [[NSNotificationCenter defaultCenter] postNotificationName:CANCEL_CURRENT_UPLOAD_NOTIFICATION object:nil];
                [self dismissViewControllerAnimated:YES completion:^{

                }];
            }

            return;
        }

        NSInteger itemId = self.item.serverID;
        [[ServerComm instance] deleteItem:itemId withSuccessBlock:^(id result) {

            [[Mixpanel sharedInstance] track:@"delete photo"];

            // KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];

            if (self.item.storyID > 0) {
                [[[Mixpanel sharedInstance] people] increment:@"Number of stories" by:@(-1)];
            }


            [self.item remove];
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

            [[[Mixpanel sharedInstance] people] increment:@"Number of photos" by:@(-1)];

            [[Analytics sharedAnalytics] track:@"item-delete" properties:@{@"itemType" : (self.item.itemType == 0) ? @"photo" : @"quote"}];

            [GlobalUtils updateAnalyticsUser];

            if (self.mode == 1) {
                [self dismissViewControllerAnimated:YES completion:^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_DELETED_NOTIFICATION object:@(itemId)];
                }];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_DELETED_NOTIFICATION object:@(itemId)];
                [self.navigationController popViewControllerAnimated:YES];
            }

        }                    andFailBlock:^(NSError *error) {

        }];
}

- (void)previewTap:(id)sender {
    if (self.recordStoryButton.tag == 0) {
        [self.storyLoadingIndicator startAnimating];
        self.playButton.tag = 1;
        [self.playButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];


        if (self.item == nil) {
            [self startPlayer];
        }
        else {

            KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];
            KPLocalAsset *itemStoryAsset = [KPLocalAsset fetchByLocalID:itemStory.assetID];

            [[ServerComm instance] downloadAssetFile:(id)itemStoryAsset withSuccessBlock:^(id result) {
                self.storyRecordingData = [NSData dataWithContentsOfFile:result];
                [self startPlayer];
            }                           andFailBlock:^(NSError *error) {

            }];
        }
    }
    else {
        [self.player pause];

        self.playButton.tag = 0;
        [self.playButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];

    }
}

- (void)HDSwitchChange:(id)sender {

}

- (void)shareSwitchChange:(id)sender {
    return;
    /*
    if (self.shareSwitch.on)
    {
        if ([[[UserManager sharedInstance] myFans] count] == 0)
        {
            //add fans!
            FansViewController *avc = [[FansViewController alloc] initWithNibName:@"FansViewController" bundle:nil];
            self.navigationController.view.tag = 1;
            [self.navigationController pushViewController:avc animated:YES];
        
            self.shareSwitch.on = NO;
            self.shareSwitchWaitingForFans = YES;
        }
    }
    
    [[UserManager sharedInstance] setShareWithFans:self.shareSwitch.on];*/
}

- (void)setItemDate:(NSDate *)itemDate {
    _itemDate = itemDate;
    self.oldDate = [NSDate dateWithTimeIntervalSince1970:self.item.itemDate];
    self.item.itemDate = itemDate.timeIntervalSince1970;

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    //[formatter setDateFormat:@"MMMM dd, yyyy"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateButton setTitle:[formatter stringFromDate:itemDate] forState:UIControlStateNormal];
}

#pragma mark - Voice Recording Buttons

- (void)setVoiceRecordingButtons {
    BOOL hasStory = (self.storyRecordingData != nil || (self.item != nil && self.item.storyID > 0));

    if (hasStory) {
        [self.recordStoryButton setImage:[UIImage imageNamed:@"microphone-icon-for-button"] forState:UIControlStateNormal];
        [self.recordStoryButton setTitle:KPLocalizedString(@"edit") forState:UIControlStateNormal];
    }
    else {
        [self.recordStoryButton setImage:[UIImage imageNamed:@"microphone-icon-for-button"] forState:UIControlStateNormal];
        [self.recordStoryButton setTitle:KPLocalizedString(@"add voice") forState:UIControlStateNormal];

    }

    BOOL hasVideoStory = (self.videoStoryRecordingData != nil || (self.item != nil && [self.item fetchVideoStory] != nil && !self.videoStoryToDelete));
    if (hasVideoStory) {
        [self.recordVideoStoryButton setImage:[UIImage imageNamed:@"record-video-icon"] forState:UIControlStateNormal];
        [self.recordVideoStoryButton setTitle:KPLocalizedString(@"edit") forState:UIControlStateNormal];
    }
    else {
        [self.recordVideoStoryButton setImage:[UIImage imageNamed:@"record-video-icon"] forState:UIControlStateNormal];
        [self.recordVideoStoryButton setTitle:KPLocalizedString(@"add video") forState:UIControlStateNormal];
    }

}

#pragma mark - RecordViewControllerDelegate

- (void)recordingSuccessful:(NSData *)fileData withLength:(NSInteger)recordingLength andMode:(NSInteger)amode andPreviewImage:(UIImage *)previewImage {
    if (amode == 0) {
        self.storyChanged = YES;
    } else if (amode == 1) {
        self.videoStoryChanged = YES;
    }

    if (fileData == nil) {
        if (amode == 0) {
            self.storyToDelete = [KPLocalStory fetchByLocalID:self.item.storyID];
            self.item.storyID = 0;

            self.storyRecordingData = nil;
        }
        else {

            KPLocalComment *itemVideoStory = [self.item fetchVideoStory];

            if (itemVideoStory) {
                self.videoStoryToDelete = itemVideoStory;
                self.videoStoryToDelete.isStory = NO;
                
            }

            self.videoStoryRecordingData = nil;
        }
    }
    else {
        if (amode == 0) {
            self.storyRecordingData = fileData;
            self.recordingLength = recordingLength;
        }
        else {
            self.videoStoryRecordingData = fileData;
            self.videoRecordingLength = recordingLength;
            self.videoStoryPreviewImage = previewImage;
        }
    }
    [self setVoiceRecordingButtons];
}

- (void)recordingCancelled {

}



#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    self.playButton.tag = 0;
    [self.playButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
}

- (void)startPlayer {
    self.audioTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(audioTimerProc:) userInfo:nil repeats:YES];

    // Commented below line by Ashvini
    [GlobalUtils outputToSpeaker];
    
    NSError *error;
    self.player = [[AVAudioPlayer alloc] initWithData:self.storyRecordingData error:&error];
    self.player.delegate = self;
    [self.player play];

    [self.storyLoadingIndicator stopAnimating];
    self.recordStoryButton.enabled = YES;
}

#pragma mark - KidSelectDelegate

- (void)itemSelected:(NSUInteger)aindex {
    if (self.selectionPopVC.view.tag == 0) {

        if (aindex == -1) {

            EditKidViewController *avc = [[EditKidViewController alloc] initWithKid:nil];
            avc.delegate = self;
            [self.navigationController pushViewController:avc animated:YES];
            return;
            //[self presentModalViewController:avc animated:YES];
        }

//        NSArray *myKids = [KPLocalKid fetchAll];
        KPLocalKid *particularKid = [KPLocalKid fetchByLocalID:aindex];

        self.kid = particularKid;

    }
    else {
        if (aindex == -1) {
            [[Mixpanel sharedInstance] track:@"add place tap" properties:@{@"source" : @"add story"}];


            PlaceEditViewController *avc = [[PlaceEditViewController alloc] initWithPlace:nil];
            avc.delegate = self;
            [self.navigationController pushViewController:avc animated:YES];
            return;
        }

        NSArray *allMyPlaces = [KPLocalPlace fetchAll];
        KPLocalPlace *particularPlace = allMyPlaces[aindex];

        // Place *place = [[[UserManager sharedInstance] myPlaces] objectAtIndex:aindex];
        if (self.item.placeID == particularPlace.identifier) {
            return;
        }


        [[Mixpanel sharedInstance] track:@"Change Place"];


        self.place = particularPlace;
        //self.item.place = place;
    }
}

- (void)itemSelectionCancelled {

}

- (void)kidAdded:(KPLocalKid *)kid {
    NSInteger age = 0;
    if (kid.birthdate > 0) {
        age = [AgeCalculator calculateAgeForBirthday:[NSDate dateWithTimeIntervalSince1970:kid.birthdate]];
    }

    NSString *sourceStr = @"Adding a picture";
    if (self.item != nil) {
        sourceStr = @"Updating a picture";
    }


    [[Mixpanel sharedInstance] track:@"Add Kid" properties:@{@"Gender" : [GlobalUtils getGenderStr:kid.gender], @"Age" : @(age), @"Kid photo" : @(kid.imageAssetID > 0), @"Add Kid Source" : sourceStr, @"has-birthdate" : @(kid.birthdate > 0)}];


    self.kid = kid;
}

- (void)setKid:(KPLocalKid *)kid {
    _kid = kid;

    KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];

    if (kidImage == nil) {
        [self.kidButton setImage:[GlobalUtils getKidDefaultImage:kid.gender] forState:UIControlStateNormal];
    } else {
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, kidImage.urlHash]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {

            [self.kidButton setImage:[image imageMaskedWithElipse:CGSizeMake(290, 290)] forState:UIControlStateNormal];
        }];
    }

    self.kidNameLabel.text = self.kid.name;
    self.rightButtonEnabled = YES;
    [GlobalUtils setCurrentSelectedKid:kid];

}

- (void)setPlace:(KPLocalPlace *)place {
    _place = place;
    if (place != nil) {
        [self.placeButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]];
        self.placeButton.titleLabel.alpha = 1.0;
        [self.placeButton setTitle:self.place.title forState:UIControlStateNormal];
    }

    if (self.kid != nil) {
        self.rightButtonEnabled = YES;
    }

    [GlobalUtils setCurrentSelectedPlace:(id)place];

}


#pragma mark - PlaceEditDelegate

- (void)placeAdded:(KPLocalPlace *)place {

    [[Mixpanel sharedInstance] track:@"place added" properties:@{@"source" : @"add story"}];


    self.place = place;
    [self.placeButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]];
    self.placeButton.titleLabel.alpha = 1.0;
    [self.placeButton setTitle:place.title forState:UIControlStateNormal];
}

#pragma mark - UITextFieldDelegate


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    /*
    if ([text isEqualToString:@"\n"]) {
        
        [self.titleTF setFrame:CGRectMake(self.titleTF.frame.origin.x, self.titleTF.frame.origin.y, self.titleTF.frame.size.width, _TextcellHeight)];
        [self tableViewNeedsToUpdateHeight];
        
        [self.titleTF endEditing:YES];
    }*/
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    //[self.tableView endUpdates];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)tableViewNeedsToUpdateHeight {
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    [UIView setAnimationsEnabled:animationsEnabled];
}

- (void)textViewDidChange:(UITextView *)textView {
    //CGFloat newTextHeight = [textView contentSize].height;

    // DDLogInfo(@"%lf",newTextHeight);
    /*
    if ((newTextHeight != _TextcellHeight && newTextHeight >= 44.0f))
    {
        _TextcellHeight = newTextHeight;
        [self.titleTF setFrame:CGRectMake(self.titleTF.frame.origin.x, self.titleTF.frame.origin.y, self.titleTF.frame.size.width, _TextcellHeight)];
        [self tableViewNeedsToUpdateHeight];
    }*/


    CGFloat maxHeight = 312.0f;
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), fminf(newSize.height, maxHeight));
    _TextcellHeight = newFrame.size.height;
    self.titleTF.frame = newFrame;
    // DDLogInfo(@"Pretty printed size: %lf", _TextcellHeight);

    if (_TextcellHeight >= 44.0f) {
        [self tableViewNeedsToUpdateHeight];
    }

    //[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
}


- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self.titleTF resignFirstResponder];
}

#pragma mark - Keyboard hide/show

- (void)keyboardDidShow:(NSNotification *)aNotification {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:4] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
}

#pragma mark - API Calls


- (void)addNewItem {
    [SVProgressHUD showWithStatus:KPLocalizedString(@"adding item")];


    self.isNewItem = YES;

    NSInteger placeId = 0;
    if (self.place != nil) {
        placeId = self.place.serverID;
    }

    NSString *extraData = @"";

    int itemType = [[self.imageInfo valueForKey:@"itemType"] intValue];

    if ([self.imageInfo valueForKey:@"extraData"] != nil) {
        extraData = [self.imageInfo valueForKey:@"extraData"];
    }

    NSString *titleText = @"";
    if (![self.titleTF.text isEqualToString:KPLocalizedString(@"add title or description...")]) {
        titleText = self.titleTF.text;
    }

    if (self.shareSwitch.on) {
        DDLogInfo(@"-----ON----");
    }
    else {
        DDLogInfo(@"----OFF------");
    }


    // DDLogInfo(@"%@",self.imageInfo);
    NSMutableSet<NSNumber*>* selectedKids = [NSMutableSet set];
    for (KPLocalKid* i in [KPLocalKid idsToObjects:self.selectedKidIDs]) {
        [selectedKids addObject:@(i.serverID)];
    }

    // __block NSArray *selectedKidsCopy = selectedKids.copy;

    [[ServerComm instance] addItem:titleText//self.titleTF.text
                      selectedKids:selectedKids
                           andDate:self.itemDate
                          andPlace:placeId
                       andCropRect:[GlobalUtils createCropRectStr:[self.imageInfo valueForKey:@"points"]]
                andOriginalAssetId:[[self.imageInfo valueForKey:@"assetId"] intValue]
                       andRotation:[[self.imageInfo valueForKey:@"rotation"] floatValue]
                            andSat:[[self.imageInfo valueForKey:@"saturation"] floatValue]
                            andCon:[[self.imageInfo valueForKey:@"contrast"] floatValue]
                            andBri:[[self.imageInfo valueForKey:@"brightness"] floatValue]
                  andAutoEnhanceOn:[[self.imageInfo valueForKey:@"autoEnhanceOn"] boolValue]
                       andItemType:itemType
                      andExtraData:extraData
                  andShareWithFans:(self.shareSwitch ? self.shareSwitch.on : YES)
                           andTags:self.tagIds
                    andExtraParams:[self.imageInfo valueForKey:@"extraParams"]
                  withSuccessBlock:^(id result) {
                      if ([[result valueForKey:@"status"] intValue] == 0) {

                          //  DDLogInfo(@"%@",result);
                          //Save local files with assets ids

                          //[[SDImageCache sharedImageCache] storeImage:(UIImage*)[self.imageInfo valueForKey:@"originalImage"] forKey:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"originalAssetHash"] ]];

                          // as it happens, we need to fetch the stuff here again
                          NSArray *selectedKids = [KPLocalKid idsToObjects:self.selectedKidIDs];

                          [[SDImageCache sharedImageCache] storeImage:(UIImage *) [self.imageInfo valueForKey:@"resultImage"] forKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

                          [[SDImageCache sharedImageCache] storeImage:(UIImage *) [self.imageInfo valueForKey:@"resultImage"] forKey:[NSString stringWithFormat:@"%@%@_iphone.jpg", Defaults.assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];


                          
                          
                          //save thumbnails
                          NSString *assetsURLString = Defaults.assetsURLString;
                          NSString *assetHash = [[result valueForKey:@"result"] valueForKey:@"assetHash"];
                          
                          
                          UIImage *smallImage = [(UIImage *) [self.imageInfo valueForKey:@"resultImage"] imageScaledToFitSize:CGSizeMake(192, 192)];

                          [[SDImageCache sharedImageCache] storeImage:smallImage forKey:[NSString stringWithFormat:@"%@%@_small.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

                          UIImage *mediumImage = [(UIImage *) [self.imageInfo valueForKey:@"resultImage"] imageScaledToFitSize:CGSizeMake(600, 600)];
                          [[SDImageCache sharedImageCache] storeImage:mediumImage forKey:[NSString stringWithFormat:@"%@%@_medium.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
                          
                          
                          // this is the alternative image cache
                          NSString *mediumURLString = [NSString stringWithFormat:@"%@%@_medium.jpg", assetsURLString, assetHash];
                          NSURL *mediumImageURL = [NSURL URLWithString:mediumURLString];
                          NSString *localMediumImagePath = [CachingAssistant cacheLocationForExternalURL:mediumImageURL];
                          [UIImageJPEGRepresentation(mediumImage, 1.0) writeToFile:localMediumImagePath atomically:YES];
                          
                          // this is where the local image cache is made
                          
                          
                          
                          


                          //Add new Item to local database
                          NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
                          [itemData setValue:self.itemDate forKey:@"itemDate"];
                          [itemData setValue:[GlobalUtils createCropRectStr:[self.imageInfo valueForKey:@"points"]] forKey:@"cropRect"];

                          NSSet *selectedKidsSet = [NSSet setWithArray:selectedKids];
                          NSString *kidsIds = @"";
                          for (KPLocalKid *kid in selectedKidsSet.allObjects) {
                              if ([kidsIds isEqualToString:@""]) {
                                  kidsIds = [kidsIds stringByAppendingFormat:@"%lu", (unsigned long)kid.serverID];
                              } else {
                                  kidsIds = [kidsIds stringByAppendingFormat:@"|%lu", (unsigned long)kid.serverID];
                              }
                          }
                          [itemData setValue:kidsIds forKey:@"kidsIds"];


                          [itemData setValue:(self.shareSwitch.on) ? @(1) : @(0) forKey:@"shareStatus"];
                          if (self.titleTF.text.length) {
                              [itemData setValue:self.titleTF.text forKey:@"title"];
                          }

                          [itemData setValue:[self.imageInfo valueForKey:@"saturation"] forKey:@"sat"];
                          [itemData setValue:[self.imageInfo valueForKey:@"contrast"] forKey:@"con"];
                          [itemData setValue:[self.imageInfo valueForKey:@"brightness"] forKey:@"bri"];
                          [itemData setValue:[self.imageInfo valueForKey:@"rotation"] forKey:@"rotation"];

                          [itemData setValue:[self.imageInfo valueForKey:@"filterId"] forKey:@"filterId"];
                          [itemData setValue:[self.imageInfo valueForKey:@"autoEnhanceOn"] forKey:@"autoEnhanceOn"];


                          if ([self.imageInfo valueForKey:@"itemType"] != nil) {
                              [itemData setValue:[self.imageInfo valueForKey:@"itemType"] forKey:@"itemType"];
                          }
                          if ([self.imageInfo valueForKey:@"extraData"] != nil) {
                              [itemData setValue:[self.imageInfo valueForKey:@"extraData"] forKey:@"extraData"];
                          }

                          //TODO: add filter, autoenhance and crop to local save
                          self.item = [[KPDataParser instance] addNewItem:[result valueForKey:@"result"] andData:itemData];
                          self.item.placeID = self.place.identifier;

                          NSArray *itemTagIDs = [KPLocalItemTag fetchTagIDsForItem:self.item];

                          for (NSNumber *tagId in self.tagIds) {

                              KPLocalTag *tag = [KPLocalTag fetchByServerID:tagId.intValue];

                              if (tag && ![itemTagIDs containsObject:tagId]) {

                                  KPLocalItemTag *itemTagRelation = [KPLocalItemTag create];
                                  itemTagRelation.tagID = tag.identifier;
                                  itemTagRelation.itemID = self.item.identifier;
                                  [itemTagRelation saveChanges];

                              }
                          }

                          [self.item saveChanges];
                          [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

                          /*[[MobileAppTracker sharedManager] trackActionForEventIdOrName:@"Add Keepy"
                                                                              eventIsId:NO];*/
                          @try {
                              NSMutableDictionary *params = [NSMutableDictionary dictionary];
                              [params setValue:@(4) forKey:@"itemType"];
                              NSString *order = @"Other keepy";
                              NSUInteger count = [Item MR_countOfEntitiesWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]];
                              if (count == 1) {
                                  order = @"1st keepy";
                              }
                              if (count == 2) {
                                  order = @"2nd keepy";
                              }

                              if (count == 5) { // 5th keepy
                                  [FBSDKAppEvents logEvent:@"Fifth Keepy"];
                                  order = @"Fifth Keepy";
                              }


                              [params setValue:order forKey:@"order"];
                              [FBSDKAppEvents logEvent:@"add keepy" parameters:params];

                          } @catch (NSException *e) {}

                          if (self.storyChanged) {
                              [self uploadStory:YES];
                          }
                          else if (self.videoStoryChanged) {
                              [self uploadVideoStory:YES];
                          }
                          else {
                              [self itemAddedSuccessfuly];
                          }
                      }
                      else if ([[result valueForKey:@"status"] intValue] == 2) //no more space...
                      {
                          [SVProgressHUD dismiss];
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [GlobalUtils showNoSpace:self];
                          });
                      }
                      else {
                          [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error adding item")];
                      }

                  } andFailBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
                      self.rightButtonEnabled = YES;
            }];
}

- (void)updateItem {
    if (self.titleTF.text.length > 0) {
        self.item.title = self.titleTF.text;
    } else {
        self.item.title = @" ";
    }

    NSArray *selectedKids = [KPLocalKid idsToObjects:self.selectedKidIDs];
    NSSet *selectedKidsSet = [NSSet setWithArray:selectedKids];

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];
    NSSet *itemKidSet = [NSSet setWithArray:itemKids];

    // inverse of equality between selected kids and the item's associated kids
    self.kidChanged = ![itemKidSet isEqualToSet:selectedKidsSet];
    [KPLocalKidItem setLinkedKids:selectedKidsSet.allObjects forItem:self.item];


    /*
    NSMutableArray *ids = [NSMutableArray new];
    
    for (Kid *obj in self.item.kids) {
        [ids addObject:[obj.kidId stringValue]];
    }
    
    NSString *idsStr = [ids componentsJoinedByString:@"|"];
    self.item.kidsIds = idsStr;
    */




    self.item.placeID = self.place.identifier;

    //DDLogInfo(@"%d",self.shareSwitch.on);

    self.item.shareStatus = self.shareSwitch.on ? YES : NO;//(self.shareSwitch.on)?1:0;

    [SVProgressHUD showWithStatus:KPLocalizedString(@"updating")];
    [[ServerComm instance] updateItem:self.item imageChanged:(self.changedImage != nil) withTags:(self.tagsChanged) ? self.tagIds : nil andExtraParams:self.editedImageInfoExtraParams withSuccessBlock:^(id result) {


        //  DDLogInfo(@"%@",result);
        
        if (self.changedImage) {




            //TODO: update foto properties

            int newAssetId = [[[result valueForKey:@"result"] valueForKey:@"assetId"] intValue];
            KPLocalAsset *imageAsset = [[KPDataParser instance] createAssetById:newAssetId withType:0 withHash:[[result valueForKey:@"result"] valueForKey:@"assetHash"]];
            self.item.imageAssetID = imageAsset.identifier;


            //Save local files with assets ids
            NSString* assetsURLString = Defaults.assetsURLString;

            [[SDImageCache sharedImageCache] storeImage:self.changedImage forKey:[NSString stringWithFormat:@"%@%@.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

            [[SDImageCache sharedImageCache] storeImage:self.changedImage forKey:[NSString stringWithFormat:@"%@%@_iphone.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];


            //save thumbnails
            UIImage *smallImage = [self.changedImage imageScaledToFitSize:CGSizeMake(192, 192)];

            [[SDImageCache sharedImageCache] storeImage:smallImage forKey:[NSString stringWithFormat:@"%@%@_small.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

            UIImage *mediumImage = [self.changedImage imageScaledToFitSize:CGSizeMake(600, 600)];
            [[SDImageCache sharedImageCache] storeImage:mediumImage forKey:[NSString stringWithFormat:@"%@%@_medium.jpg", assetsURLString, [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];

        }

        if (self.tagsChanged) {

            NSArray *tagIDs = self.tagIds;
            
            // NSArray *newItemTags = [KPLocalTag idsToObjects:self.tagIds];
            NSMutableArray *newItemTags = @[].mutableCopy;
            for(NSNumber *currentTagID in tagIDs){
                
                int currentServerID = currentTagID.intValue;
                KPLocalTag *currentTag = [KPLocalTag fetchByServerID:currentServerID];
                
                [newItemTags addObject:currentTag];
            }
            
            NSArray *originalItemTagIDs = [KPLocalItemTag fetchTagIDsForItem:self.item];
            NSMutableArray *originalItemTags = [KPLocalTag idsToObjects:originalItemTagIDs].mutableCopy;
            
            [newItemTags addObjectsFromArray:originalItemTags];
            [KPLocalItemTag setLinkedTags:newItemTags forItem:self.item];

        }

        if (self.storyChanged) {
            [self uploadStory:NO];
        }
        else if (self.videoStoryChanged) {
            [self uploadVideoStory:NO];
        }
        else {
            [self updateItemSuccess];
        }
        
        [self.item saveChanges];
        

    }                    andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
        self.rightButtonEnabled = YES;
    }];
}

- (void)updateItemSuccess {

    //DDLogInfo(@"%@",self.item.image);

//    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
//        
//    }];

    [[Analytics sharedAnalytics] track:@"item-update" properties:@{@"itemType" : (self.item.itemType == 0) ? @"photo" : @"quote"}];


    [[Mixpanel sharedInstance] track:@"update item success"];

    [SVProgressHUD dismiss];
    
    [self.item saveChanges];
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];


    if (self.kidChanged) {

        [self.delegate itemEditFinished];
        [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_DELETED_NOTIFICATION object:@(self.item.serverID)];
    }
    else {
        [self.delegate itemEditFinished];
    }

    /*
    if (self.kidChanged) {
        [self.delegate itemEditFinished];
        [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_DELETED_NOTIFICATION object:[NSNumber numberWithBool:YES]];
    }*/

    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"edit-story", @"result" : @"ok"}];

    if (self.mode == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)uploadStory:(BOOL)isNewItem {
    //  DDLogInfo(@"%d",self.item.story.storyId.intValue);
    // DDLogInfo(@"%d",self.storyToDelete.storyId.intValue);

    if (self.storyRecordingData == nil) {
        [SVProgressHUD showWithStatus:KPLocalizedString(@"deleting")];
        [[ServerComm instance] deleteStory:self.storyToDelete.serverID withSuccessBlock:^(id result) {
            [SVProgressHUD dismiss];

            self.item.storyID = 0;
            [self.storyToDelete remove];
            self.storyToDelete = nil;

            self.storyRecordingData = nil;
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

            if (self.videoStoryChanged) {
                [self uploadVideoStory:isNewItem];
            }
            else {
                if (isNewItem) {
                    [self itemAddedSuccessfuly];
                } else {
                    [self updateItemSuccess];
                }
            }

        }                     andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem deleting audio story, please mare sure you are connected to the internet and try again")];

            self.itemAddStatus = 1;
        }];
    }
    else {
        [[ServerComm instance] uploadStory:self.item.serverID withStoryLength:self.recordingLength withData:self.storyRecordingData withSuccessBlock:^(id result) {
            int status = [[result valueForKey:@"status"] intValue];
            if (status == 0) {
                KPLocalAsset *asset = [KPLocalAsset create];
                asset.serverID = [[result valueForKey:@"assetId"] intValue];
                asset.assetHash = [result valueForKey:@"hash"];
                asset.assetType = 2;

                KPLocalStory *story = [KPLocalStory create];
                story.serverID = [[result valueForKey:@"storyId"] intValue];
                story.storyLength = self.recordingLength;
                story.assetID = asset.identifier;

                self.item.storyID = story.identifier;

                [[ServerComm instance] saveAssetData:self.storyRecordingData withFileExt:@"mp4" andAssetHash:asset.urlHash];

                [asset saveChanges];
                [story saveChanges];
                [self.item saveChanges];
                
                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

                if (self.videoStoryChanged) {
                    [self uploadVideoStory:isNewItem];
                }
                else {
                    if (isNewItem) {
                        [self itemAddedSuccessfuly];
                    } else {
                        [self updateItemSuccess];
                    }
                }
            }
            else {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading story")];
            }
        }                     andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem uploading audio story, please mare sure you are connected to the internet and try again")];

            self.itemAddStatus = 1;

        }                 andProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            //DDLogInfo(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
            [SVProgressHUD showProgress:(float) totalBytesWritten / (float) totalBytesExpectedToWrite status:KPLocalizedString(@"uploading story")];
        }];
    }
}

- (void)uploadVideoStory:(BOOL)isNewItem {
    if (self.videoStoryRecordingData != nil) {
  
        [[ServerComm instance] uploadItemComment:self.item.serverID
                                       withVideo:self.videoStoryRecordingData
                                 andPreviewImage:self.videoStoryPreviewImage
                                andCommentLength:self.videoRecordingLength
                                        withType:0
                                         isStory:YES
                                   withExtraData:@""
                                withSuccessBlock:^(id result) {

                                    KPLocalComment *comment = [KPLocalComment create];
                                    comment.serverID = [[result valueForKey:@"commentId"] intValue];
                                    comment.commentDate = [NSDate date].timeIntervalSince1970;
                                    comment.commentLength = self.videoRecordingLength;
                                    comment.wasRead = YES;
                                    comment.isStory = YES;
                                    comment.commentType = 0;

                                    KPLocalAsset *asset = [KPLocalAsset create];
                                    asset.serverID = [[result valueForKey:@"assetId"] intValue];
                                    asset.assetHash = [result valueForKey:@"hash"];
                                    asset.assetType = 1;
                                    [asset saveChanges];

                                    comment.videoAssetID = asset.identifier;
                                    [[ServerComm instance] saveAssetData:self.videoStoryRecordingData withFileExt:@"mov" andAssetHash:asset.urlHash];

                                    asset = [KPLocalAsset create];
                                    asset.serverID = [[result valueForKey:@"previewAssetId"] intValue];
                                    asset.assetHash = [result valueForKey:@"previewAssetHash"];
                                    asset.assetType = 0;
                                    [asset saveChanges];
                                    comment.previewImageAssetID = asset.identifier;


                                    [[SDImageCache sharedImageCache] storeImage:self.videoStoryPreviewImage forKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, asset.urlHash]];


                                    comment.itemID = self.item.identifier;
                                    [comment saveChanges];

                                    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

                                    [SVProgressHUD dismiss];

                                    if (isNewItem) {
                                        [self itemAddedSuccessfuly];
                                    } else {
                                        [self updateItemSuccess];
                                    }


                                } andFailBlock:^(NSError *error) {
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem uploading video story, please mare sure you are connected to the internet and try again")];
                    self.itemAddStatus = 2;
                }               andProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                    //DDLogInfo(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
                    [SVProgressHUD showProgress:(float) totalBytesWritten / (float) totalBytesExpectedToWrite status:KPLocalizedString(@"uploading video story")];
                }];
    }
    
    [SVProgressHUD showWithStatus:KPLocalizedString(@"deleting")];
    [[ServerComm instance] deleteComment:self.videoStoryToDelete.serverID withSuccessBlock:^(id result) {
        
        [SVProgressHUD dismiss];
        
        [self.videoStoryToDelete remove];
        self.videoStoryToDelete = nil;
        
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
        
        if (isNewItem) {
            [self itemAddedSuccessfuly];
        } else {
            [self updateItemSuccess];
        }
        
    }                       andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem deleting video story, please mare sure you are connected to the internet and try again")];
        
        self.itemAddStatus = 2;
        
    }];
}


- (void)itemAddedSuccessfuly {


    //Fix in 1.95.3
    if (!self.videoInfo) {
        [[Mixpanel sharedInstance] track:@"add photo"];
        [[[Mixpanel sharedInstance] people] increment:@"Number of photos" by:@(1)];
    }


    [[Analytics sharedAnalytics] track:@"item-add" properties:@{@"itemType" : @"photo"}];

    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-story", @"result" : @"ok"}];

    [GlobalUtils updateAnalyticsUser];

    self.imageInfo = nil;

    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:self.item];

    [SVProgressHUD dismiss];
    //[SVProgressHUD showSuccessWithStatus:@"photo was added"];

    //RecordViewController *avc = [[RecordViewController alloc] initWithItem:self.item withMode:11];
    
    if (self.isNewKid) {
         
         UIViewController *viewController = self.navigationController.childViewControllers[0];
         
         if([viewController isKindOfClass:[SettingsViewController class]]){
             
             [self.navigationController popToViewController:self.navigationController.childViewControllers[1] animated:YES];
         } else {
 
             [self.navigationController popToRootViewControllerAnimated:YES];
         }
         
    } else {

#if 0
        [[ServerComm instance] getPrivateLink:(id)self.item aSuccessBlock:^(id result) {

            // DDLogInfo(@"%@",result);

            ItemShareViewController *avc = [[ItemShareViewController alloc] initWithItem:(id)self.item isAfterAddItem:YES];
            avc.short_url = [[result objectForKey:@"result"] objectForKey:@"short_url"];
            [self.navigationController pushViewController:avc animated:YES];

        }                        andFailBlock:^(NSError *error) {
            ItemShareViewController *avc = [[ItemShareViewController alloc] initWithItem:(id)self.item isAfterAddItem:YES];
            avc.short_url = nil;
            [self.navigationController pushViewController:avc animated:YES];
        }];
#else
        [self dismissViewControllerAnimated:YES completion:NULL];
#endif

    }
    [GlobalUtils postNotificationForNewItem:self.item];
}

#pragma mark - ImageEditorDelegate

- (void)imageEditorFinishedWithResult:(NSDictionary *)result {
    if ([result valueForKey:@"indexInGroup"] != nil) {
        if (self.imagesInfos == nil) {
            self.imagesInfos = [[NSMutableArray alloc] init];
        }

        NSUInteger aindex = [[result valueForKey:@"indexInGroup"] integerValue];

        if (aindex == -1) {
            aindex = ([self.thumbsScrollView.subviews count] - 1);
            [self.imagesInfos addObject:result];
            float ax = 100 * aindex;
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(ax, 5, 90, 90)];
            img.image = (UIImage *) [result valueForKey:@"resultImage"];
            img.tag = 100 + aindex;
            [self.thumbsScrollView insertSubview:img atIndex:aindex];
            self.thumbsScrollView.contentSize = CGSizeMake(100 * (aindex + 2), 100);
        }
        else {
            self.imagesInfos[aindex] = result;

            UIImageView *img = (UIImageView *) self.thumbsScrollView.subviews[aindex];
            img.image = (UIImage *) [result valueForKey:@"resultImage"];
        }

        UIButton *btn = (UIButton *) self.thumbsScrollView.subviews[[self.thumbsScrollView.subviews count] - 1];
        CGRect aframe = btn.frame;
        aframe.origin.x = 100 * ([self.thumbsScrollView.subviews count] - 1);
        btn.frame = aframe;

        return;
    }

    NSArray *points = (NSArray *) [result valueForKey:@"points"];
    self.changedImage = (UIImage *) [result valueForKey:@"resultImage"];
    self.photoImageView.image = self.changedImage;
    self.item.cropRect = [GlobalUtils createCropRectStr:points];
    self.item.rotation = [[result valueForKey:@"rotation"] floatValue];
    self.item.saturation = [[result valueForKey:@"saturation"] floatValue];
    self.item.contrast = [[result valueForKey:@"contrast"] floatValue];
    self.item.brightness = [[result valueForKey:@"brightness"] floatValue];

    self.item.filterID = [[result valueForKey:@"filterId"] intValue];
    self.item.autoEnhanceOn = [[result valueForKey:@"autoEnhanceOn"] boolValue];

    self.editedImageInfoExtraParams = [result valueForKey:@"extraParams"];
    // DLog(@" self.item.filterId.intValue %d",  self.item.filterId.intValue );
    // DLog(@" self.item.autoEnhanceOnValue %d",  self.item.autoEnhanceOnValue );
}

#pragma mark - AudioTimer

- (void)audioTimerProc:(id)sender {
    int minutes = 0;
    int seconds = 0;
    NSTimeInterval duration = 1;
    NSTimeInterval currentTime = 0;

    minutes = floor(self.player.currentTime / 60);
    seconds = round(self.player.currentTime - minutes * 60);
    duration = self.player.duration;
    currentTime = self.player.currentTime;


    if (minutes < 0) {
        minutes = 0;
    }

    if (seconds < 0) {
        seconds = 0;
    }


    self.storyTimeCurrentLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

    minutes = floor(duration / 60);
    seconds = round(duration - minutes * 60);
    self.storyTimeEndLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

    CGRect aframe = self.progressFiller.frame;
    aframe.size.width = (170 - 32) * (currentTime / duration);
    self.progressFiller.frame = aframe;

    float ax = self.progressFiller.frame.origin.x + self.progressFiller.frame.size.width - 10;
    if (ax < 50) {
        ax = 50;
    }

    aframe = self.knobButton.frame;
    aframe.origin.x = ax;
    self.knobButton.frame = aframe;
}

#pragma mark - NoteEditorDelegate

- (void)noteEditorFinished:(NSString *)noteText withNoteImage:(UIImage *)noteImage andAssetId:(NSInteger)assetId {
    if (self.imageInfo == nil) {
        self.imageInfo = [[NSDictionary alloc] init];
    }

    NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithDictionary:self.imageInfo];
    [d setValue:noteText forKey:@"extraData"];
    [d setValue:@(2) forKey:@"itemType"];
    [d setValue:noteImage forKey:@"resultImage"];


    KPLocalAsset *referencedAsset = [KPLocalAsset fetchByServerID:assetId];
    KPLocalAsset *originalAsset = [KPLocalAsset fetchByLocalID:self.item.originalImageAssetID];


    if (originalAsset.identifier != referencedAsset.identifier) {
        KPLocalAsset *newOriginalImage = [[KPDataParser instance] createAssetById:assetId withType:0 withHash:nil];
        self.item.originalImageAssetID = newOriginalImage.identifier;
    }

    self.item.extraData = noteText;

    self.photoImageView.image = noteImage;
    self.changedImage = noteImage;
}

- (void)addPhoto {
    UIViewController *pvc = self;
    //if (self.revealController.presentedViewController)
    //    pvc = self.revealController.presentedViewController;

    int cameraMode = [[UserManager sharedInstance] cameraMode];

    if (cameraMode == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {


        [[Mixpanel sharedInstance] registerSuperProperties:@{@"PhotoSource" : @"Camera"}];

        CameraViewController *avc = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
        avc.delegate = self;
        self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:avc];
        [pvc presentViewController:self.addPhotoNavController animated:YES completion:nil];
    }
    else {

        if ([KPPhotoLibrary authorizationIsDenied]) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
            return;
        }

        [[Mixpanel sharedInstance] registerSuperProperties:@{@"PhotoSource" : @"Camera Roll"}];

        PHFetchOptions* options = [PHFetchOptions new];
        PHFetchResult* images = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
        
        NSMutableArray* arr = [NSMutableArray array];
        
        [images enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
            [arr addObject:[[KPAsset alloc] initWithAsset:asset]];
            
            if (idx + 1 == images.count) {
                ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName:nil bundle:nil];
                albumController.delegate = self;
                albumController.title = KPLocalizedString(@"albums");
                albumController.filter = (id)[ALAssetsFilter allVideos];

                GalleryPickerViewController *avc = [[GalleryPickerViewController alloc] initWithAssets:arr];
                avc.showToolbar = YES;
                avc.delegate = self;
                self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:albumController];
                [self.addPhotoNavController pushViewController:avc animated:NO];
                self.addPhotoNavController.view.bounds = CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height);
                [pvc presentViewController:self.addPhotoNavController animated:YES completion:nil];

                *stop = YES;
            }
        }];
        
        if (images.count == 0) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings->Privacy->Photos")];
        }
    }
}

#pragma mark - GalleryPickerDelegate

- (void)galleryPickerController:(GalleryPickerViewController *)picker didFinishPickingMediaWithInfo:(NSArray *)info {
    [self showFinalView:info[0] applyCrop:YES];
}

- (void)galleryPickerControllerDidCancel:(GalleryPickerViewController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
        self.addPhotoNavController = nil;
    }];
}

- (void)showFinalView:(NSDictionary *)info applyCrop:(BOOL)applyCrop {
    UIImage *aimage = [info valueForKey:UIImagePickerControllerOriginalImage];
    if (aimage.size.width > 2000 || aimage.size.height > 2000) {
        aimage = [aimage scaleProportionalToSize:CGSizeMake(2000, 2000)];
    }

    aimage = [aimage fixOrientation];

    NSMutableDictionary *newInfo = [[NSMutableDictionary alloc] initWithDictionary:info];
    [newInfo setValue:aimage forKey:UIImagePickerControllerOriginalImage];

    ImageEditorViewController *avc = [[ImageEditorViewController alloc] initWithImageInfo:info andPoints:nil];
    avc.delegate = self;
    [self.addPhotoNavController pushViewController:avc animated:YES];

    self.addPhotoNavController.view.tag = 9999;
}

- (void)editImageTap:(UITapGestureRecognizer *)gesture {
    UIView *sender = gesture.view;

    NSDictionary *info = self.imagesInfos[sender.tag - 100];
    ImageEditorViewController *avc = [[ImageEditorViewController alloc] initWithImageInfo:info andPoints:[info valueForKey:@"points"]];
    avc.delegate = self;

    self.addPhotoNavController = [[UINavigationController alloc] initWithRootViewController:avc];
    [self presentViewController:self.addPhotoNavController animated:YES completion:nil];


    self.addPhotoNavController.view.tag = 10000 + sender.tag - 100;
}

#pragma mark -
#pragma mark UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 10;
    /*
    if ([[UserManager sharedInstance] isAfterRegistration]) {
     
        return 7;
    } else {
        if (self.videoInfo) {
            return 10;
        }
        return 10;
    }*/
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) { // kids / add title or description
        if (indexPath.row == 0) {
            return 100.0f;
        } else if (indexPath.row == 1) {
            return 88.0f;
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 107.0f;
        }
        else if (indexPath.row == 1) {
            return _TextcellHeight;

            //return 44.0f;
        }


    } else if (indexPath.section == 2) { // add voice / add video
        return 44.0f;
    } else if (indexPath.section == 3) { // spacer
        return 30.0f;
    } else if (indexPath.section == 4) { // add tags
        return 44.0f;
    } else if (indexPath.section == 5) { // spacer
        return 30.0f;
    } else if (indexPath.section == 6) { // share with fans
        return 44.0f;
    } else if (indexPath.section == 7) { // info
        return 30.0f;
    } else if (indexPath.section == 8) { // image
        return 44.0f;
    } else if (indexPath.section == 9) {
        return 30.0f;
    }


    /*
    if ([[UserManager sharedInstance] isAfterRegistration]) {

        if (indexPath.section == 0) { // kids / add title or description
            if (indexPath.row == 0) {
                return 135.0f;
            } else if (indexPath.row == 1) {
                return 88.0f;
            }
        } else if (indexPath.section == 1) { // spacer
            return 30.0f;
        } else if (indexPath.section == 2) { // add voice / add video
            return 44.0f;
        } else if (indexPath.section == 3) { // spacer
            return 30.0f;
        } else if (indexPath.section == 4) { // add tags
            return 44.0f;
        } else if (indexPath.section == 5) { // info
            return 60.0f;
        } else if (indexPath.section == 6) { // image
            return 330.0f;
        }
        
    } else {
        if (self.videoInfo) {
            if (indexPath.section == 0) { // kids / add title or description
                if (indexPath.row == 0) {
                    return 100.0f;
                } else if (indexPath.row == 1) {
                    return 88.0f;
                }
            } else if (indexPath.section == 1) {
                if (indexPath.row == 0) {
                    return 92.0f;
                }
                else if(indexPath.row==1){
                    return 44.0f;
                }
                
                
            } else if (indexPath.section == 2) { // add voice / add video
                return 44.0f;
            } else if (indexPath.section == 3) { // spacer
                return 30.0f;
            } else if (indexPath.section == 4) { // add tags
                return 44.0f;
            } else if (indexPath.section == 5) { // spacer
                return 30.0f;
            } else if (indexPath.section == 6) { // share with fans
                return 44.0f;
            } else if (indexPath.section == 7) { // info
                return 30.0f;
            } else if (indexPath.section == 8) { // image
                return 44.0f;
            } else if (indexPath.section == 9){
                return 30.0f;
            }

        }
        
        //---------------------------------
        else{
            if (indexPath.section == 0) { // kids / add title or description
                if (indexPath.row == 0) {
                    return 100.0f;
                } else if (indexPath.row == 1) {
                    return 88.0f;
                }
            } else if (indexPath.section == 1) {
                if (indexPath.row == 0) {
                    return 92.0f;
                }
                else if(indexPath.row==1){
                    return 44.0f;
                }
             
                
            } else if (indexPath.section == 2) { // add voice / add video
                return 44.0f;
            } else if (indexPath.section == 3) { // spacer
                return 30.0f;
            } else if (indexPath.section == 4) { // add tags
                return 44.0f;
            } else if (indexPath.section == 5) { // spacer
                return 30.0f;
            } else if (indexPath.section == 6) { // share with fans
                return 44.0f;
            } else if (indexPath.section == 7) { // info
                return 30.0f;
            } else if (indexPath.section == 8) { // image
                return 44.0f;
            } else if (indexPath.section == 9){
                return 30.0f;
            }
            
        }
    }*/
    return 0.0f;
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    //Kids scroller
    if (section == 0) { // kids / add title or description
        return 1;
    }
    //Image and description
    if (section == 1) {
        return 2;
    }

    //INFO when && where
    if (section == 4) {
        return 2;
    }


    else {
        return 1;
    }


    /*
    if ([[UserManager sharedInstance] isAfterRegistration]) {
        if (section == 0) { // kids / add title or description
            return 1;
        } else if (section == 1) { // spacer
            return 1;
        } else if (section == 2) { // add voice / add video
            return 1;
        } else if (section == 3) { // spacer
            return 1;
        } else if (section == 4) { // add tags
            return 1;
        } else if (section == 5) { // info
            return 1;
        } else if (section == 6) { // image
            return 1;
        }
        else if (section == 7) { // image
            return 1;
        }
    }
    
    else {
        //Kids scroller
        if (section == 0) { // kids / add title or description
            return 1;
        }
        //Image and description
        if (section == 1) {
            return 2;
        }
        
        //INFO when && where
        if (section == 4) {
            return 2;
        }
        
        
        else{
            return 1;
        }
    }
    return 0;*/
}


- (UITableViewCell *)getCellForCellIdentifier:(NSString *)cellIdentifier indexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

    dispatch_async(dispatch_get_main_queue(), ^{
   
    if ([cellIdentifier isEqualToString:@"kidsAndTitleOrDescriptionCell"]) {

        if (indexPath.row == 0) { // kids
            cell.contentView.backgroundColor = [UIColor whiteColor];

            // NSUInteger kidsCount = [[UserManager sharedInstance] myKids].count;
            NSUInteger kidsCount = self.kidsArray.count;


            self.kidButtons = [[NSMutableArray alloc] initWithCapacity:kidsCount];
            self.kidNameLabels = [[NSMutableArray alloc] initWithCapacity:kidsCount];

            UILabel *tagYourKidsLabel = [[UILabel alloc] initWithFrame:CGRectMake(17.0f, 7.0f, cell.contentView.frame.size.width - 17.0f, 15.0f)];
            tagYourKidsLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14.0f];

            if (self.comeFromEdit) {
                tagYourKidsLabel.text = self.item.itemType == 4 ? @"who is in the video?" : @"who is in the picture?";
            }
            else {
                tagYourKidsLabel.text = self.videoInfo ? @"who is in the video?" : @"who is in the picture?";
            }


            tagYourKidsLabel.textColor = UIColorFromRGB(0x57423f);
            [cell.contentView addSubview:tagYourKidsLabel];

            UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 99, self.view.frame.size.width, 1)];
            v.backgroundColor = UIColorFromRGB(0xdcd6d4);
            [cell.contentView addSubview:v];

            //-------Kids

            UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 15.0f, self.view.bounds.size.width, 85.0f)];
            scrollView.contentSize = CGSizeMake(84.0f * kidsCount, scrollView.contentSize.height);
            for (int i = 0; i < kidsCount; i++) {

                KPLocalKid *kid = self.kidsArray[i];//
                KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];

                // DDLogInfo(@"Name = %@\nBirthday = %@",kid.name,kid.birthdate);

                UIView *kidView = [[UIView alloc] initWithFrame:CGRectMake(70.0f * i, 5.0f, 94.0f, 85.0f)];

                UIButton *kidButton = [UIButton buttonWithType:UIButtonTypeCustom];
                kidButton.tag = kid.serverID;
                kidButton.frame = CGRectMake(20.0f, 12.0f, 40.0f, 40.0f);
                [kidButton addTarget:self action:@selector(kidTap:) forControlEvents:UIControlEventTouchUpInside];
                if (kidImage == nil) {
                    [kidButton setImage:[GlobalUtils getKidDefaultImage:kid.gender] forState:UIControlStateNormal];
                } else {
                    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, kidImage.urlHash]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        [kidButton setImage:[image imageMaskedWithElipse:CGSizeMake(290, 290)] forState:UIControlStateNormal];
                    }];
                }
                kidButton.alpha = 0.3f;
                [self.kidButtons addObject:kidButton];
                [kidView addSubview:kidButton];

                UILabel *kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 55.0f, 60.0f, 20.0f)];
                kidNameLabel.tag = kid.serverID;
                kidNameLabel.textColor = [UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1];
                kidNameLabel.textAlignment = NSTextAlignmentCenter;
                kidNameLabel.backgroundColor = [UIColor clearColor];
                kidNameLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
                kidNameLabel.text = kid.name;
                kidNameLabel.alpha = 0.3f;
                [kidView addSubview:kidNameLabel];
                [self.kidNameLabels addObject:kidNameLabel];

                [scrollView addSubview:kidView];
            }
            [cell.contentView addSubview:scrollView];

//            //line
//            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
//            lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
//            [cell.contentView addSubview:lineView];
//            
//            //line
//            lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 100, 320, 1)];
//            lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
//            [cell.contentView addSubview:lineView];



            cell.selectionStyle = UITableViewCellSelectionStyleNone;

        }


    }


    else if ([cellIdentifier isEqualToString:@"recordStoryCell"]) {
        cell.contentView.backgroundColor = [UIColor whiteColor];

        //voice button
        self.recordStoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.recordStoryButton addTarget:self action:@selector(recordStoryTap:) forControlEvents:UIControlEventTouchUpInside];
        self.recordStoryButton.frame = CGRectMake(0, 3, cell.frame.size.width/2, 44);
        [self.recordStoryButton setImage:[UIImage imageNamed:@"microphone-icon-for-button"] forState:UIControlStateNormal];
        [self.recordStoryButton setTitle:KPLocalizedString(@"add voice") forState:UIControlStateNormal];
        self.recordStoryButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
        [self.recordStoryButton setTitleColor:UIColorFromRGB(0x57D62B) forState:UIControlStateNormal];
        self.recordStoryButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 10);
        self.recordStoryButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
        self.recordStoryButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        [cell.contentView addSubview:self.recordStoryButton];

        //video button
        self.recordVideoStoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.recordVideoStoryButton setImage:[UIImage imageNamed:@"record-video-icon"] forState:UIControlStateNormal];
        [self.recordVideoStoryButton setTitle:KPLocalizedString(@"add video") forState:UIControlStateNormal];
        self.recordVideoStoryButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
        [self.recordVideoStoryButton setTitleColor:UIColorFromRGB(0x57D62B) forState:UIControlStateNormal];
        [self.recordVideoStoryButton addTarget:self action:@selector(recordVideoStoryTap:) forControlEvents:UIControlEventTouchUpInside];
        self.recordVideoStoryButton.frame = CGRectMake(cell.frame.size.width/2, 3, cell.frame.size.width/2, 44);
        self.recordVideoStoryButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 8, 10);
        self.recordVideoStoryButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
        self.recordVideoStoryButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        [cell.contentView addSubview:self.recordVideoStoryButton];

        //separator line
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((cell.frame.size.width/2), 0, 1, 44)];
        img.backgroundColor = UIColorFromRGB(0xab9892);
        img.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        img.alpha = 0.5;
        [cell.contentView addSubview:img];

        //line
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, self.view.frame.size.width, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];

        //line
        lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

        //add tags
    else if ([cellIdentifier isEqualToString:@"tagsCell"]) {
        cell.contentView.backgroundColor = [UIColor whiteColor];

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 44)];
        lbl.textColor = UIColorFromRGB(0x57423f);;
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        lbl.text = KPLocalizedString(@"categories");
        [cell.contentView addSubview:lbl];

        self.tagsStrLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 225, 0, 185, 44)];
        self.tagsStrLabel.textColor = UIColorFromRGB(0x57423f);
        self.tagsStrLabel.textAlignment = NSTextAlignmentRight;
        self.tagsStrLabel.backgroundColor = [UIColor clearColor];
        self.tagsStrLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        self.tagsStrLabel.text = KPLocalizedString(@"select");
        self.tagsStrLabel.alpha = 0.4;
        [cell.contentView addSubview:self.tagsStrLabel];

        /*
        //arrow
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(290, (cell.bounds.size.height-14)/2, 10, 15)];
        img.image = [UIImage imageNamed:@"settings-arrow"];
        [cell.contentView addSubview:img];
        
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 43, 320, 1)];
        v.backgroundColor = UIColorFromRGB(0xD3CAC7);
        [cell.contentView addSubview:v];
        
        //line
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];*/

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    } else if ([cellIdentifier isEqualToString:@"privacyCell"]) {


        cell.contentView.backgroundColor = [UIColor whiteColor];

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 200, 20)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textColor = UIColorFromRGB(0x57423f);
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        lbl.text = KPLocalizedString(@"share with fans");
        [cell.contentView addSubview:lbl];

        // self.shareSwitch = [[UISwitch alloc] init];
        CGRect aframe = self.shareSwitch.frame;
        aframe.origin.y = 6;
        aframe.origin.x = self.view.bounds.size.width - 71; //250;
        self.shareSwitch.frame = aframe;
        self.shareSwitch.tintColor = UIColorFromRGB(0xD3CAC7);
        [self.shareSwitch addTarget:self action:@selector(shareSwitchChange:) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:self.shareSwitch];


        // DDLogInfo(@"%d",self.shareSwitch.on);
        // self.shareSwitch.on = self.item == nil ? YES :self.item.sharestatus.intValue;

        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 43, self.view.frame.size.width, 1)];
        v.backgroundColor = UIColorFromRGB(0xD3CAC7);
        [cell.contentView addSubview:v];

        //line
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    } else if ([cellIdentifier isEqualToString:@"HD"]) {
        //hiding as requested at JIRA - iPHONE 63
        /*cell.contentView.backgroundColor = [UIColor whiteColor];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 200, 20)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        lbl.text = KPLocalizedString(@"HD");
        [cell.contentView addSubview:lbl];
        
        self.HDSwitch = [[UISwitch alloc] init];
        CGRect aframe = self.HDSwitch.frame;
        aframe.origin.y = 6;
        aframe.origin.x = 250;
        self.HDSwitch.frame = aframe;
        [self.HDSwitch setOn:YES];
        self.HDSwitch.tintColor = UIColorFromRGB(0xD3CAC7);
        [self.HDSwitch addTarget:self action:@selector(HDSwitchChange:) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:self.HDSwitch];
        
//        self.HDSwitch.on = [[UserManager sharedInstance] shareWithFans];
        
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 43, 320, 1)];
        v.backgroundColor = UIColorFromRGB(0xD3CAC7);
        [cell.contentView addSubview:v];
        
        //line
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;*/
    }

    else if ([cellIdentifier isEqualToString:@"imageCell"]) {

        if (indexPath.row == 0) {
            self.photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 100, 100)];
            self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.photoImageView.clipsToBounds = YES;

            [cell.contentView addSubview:self.photoImageView];

            float w = 300;

            self.photoImageView.tag = 1;
            self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;

            [self.photoImageView.layer setBorderWidth:0.1];
            [self.photoImageView.layer setBorderColor:[UIColorFromRGB(0xffffff) CGColor]];

            [self.photoImageView.layer setShadowColor:[UIColorFromRGB(0x000000) CGColor]];

            CGMutablePathRef path = CGPathCreateMutable();
            CGPathAddRect(path, NULL, CGRectMake(-1, -1, self.photoImageView.frame.size.width + 1, self.photoImageView.frame.size.height + 1));
            self.photoImageView.layer.shadowPath = path;
            self.photoImageView.layer.shadowOpacity = 0.3;
            self.photoImageView.layer.shadowOffset = CGSizeMake(1.0, 1.0);
            self.photoImageView.layer.shadowRadius = 0.5;
            CFRelease(path);


            UIButton *editbtn = [UIButton buttonWithType:UIButtonTypeCustom];
            editbtn.frame = self.photoImageView.frame;
            [editbtn addTarget:self action:@selector(editTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:editbtn];


            if (!self.videoInfo && self.comeFromEdit) {

                // DDLogInfo(@"%@",NSStringFromCGRect(self.photoImageView.frame));

                UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.photoImageView.frame.size.height - 25, self.photoImageView.frame.size.width, 25)];
                [bg setImage:[UIImage imageNamed:@"thumb_gradient"]];
                [self.photoImageView addSubview:bg];

                //delete button
                UIButton *DeleteBtu = [UIButton buttonWithType:UIButtonTypeCustom];
                [DeleteBtu setImage:[UIImage imageNamed:@"add-story-delete-photo-icon"] forState:UIControlStateNormal];
                // btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                DeleteBtu.frame = CGRectMake(self.photoImageView.frame.size.width - 14, self.photoImageView.frame.size.height - 14, 20, 20);
                [DeleteBtu addTarget:self action:@selector(deleteTap:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:DeleteBtu];
            }

            /*
             // play by
             if (self.item.itemType.intValue == 4) {
             UIImageView *playVideoView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"play-video-big"]];
             playVideoView.center = self.photoImageView.center ;
             playVideoView.center = CGPointMake(playVideoView.center.x, playVideoView.center.y - 15.0f);
             
             [cell.contentView addSubview:playVideoView];
             }
             */

            //edit button
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            if (self.item.itemType == 4 || self.videoInfo) {
                [btn setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
            } else {
                [btn setImage:[UIImage imageNamed:@"add-story-edit-photo-icon"] forState:UIControlStateNormal];
            }
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
            btn.frame = CGRectMake(10, 20 + w - 44, 150, 44);
            btn.alpha = 0.9;
            btn.backgroundColor = UIColorFromRGB(0x57423f);
            if (self.item.itemType == 4 || self.videoInfo) {
                [btn setTitle:KPLocalizedString(@"play") forState:UIControlStateNormal];
            } else {

                [btn setTitle:KPLocalizedString(@"edit") forState:UIControlStateNormal];
            }
            [btn setTitleColor:UIColorFromRGB(0xd7ff46) forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:16];
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
            if (self.item.itemType == 4 || self.videoInfo) {
                [btn addTarget:self action:@selector(viewVideo:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                [btn addTarget:self action:@selector(editTap:) forControlEvents:UIControlEventTouchUpInside];
            }

            if (self.videoInfo || (self.comeFromEdit && self.item.itemType == 4)) {
                UIImageView *playImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play-video-small-gallery"]];
                playImage.frame = CGRectMake(self.photoImageView.frame.size.width / 2 - 15.75, self.photoImageView.frame.size.height / 2 - 15.75, 31.5f, 31.5f);
                [self.photoImageView addSubview:playImage];
            }



            // [cell.contentView addSubview:btn];


            /*
            [btn setTitle:KPLocalizedString(@"delete") forState:UIControlStateNormal];
            [btn setTitleColor:UIColorFromRGB(0xd7ff46) forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:16];
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
            [btn addTarget:self action:@selector(deleteTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];*/

            //sep line
            UIView *v = [[UIView alloc] initWithFrame:CGRectMake(159, 20 + w - 44, 1, 44)];
            v.backgroundColor = UIColorFromRGB(0xab9892);
            //[cell.contentView addSubview:v];

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }


        if (indexPath.row == 1) {
            // add title or description
            //cell.contentView.backgroundColor = [UIColor whiteColor];

            self.titleTF = [[KPTextView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width -20, _TextcellHeight)];
            self.titleTF.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
            [self.titleTF setPlaceholder:KPLocalizedString(@"add title or description...")];
            [self.titleTF setTextColor:UIColorFromRGB(0x57423f)];
            self.titleTF.delegate = self;
            if (self.comeFromEdit) {
                self.titleTF.scrollEnabled = NO;
            }

            [cell.contentView addSubview:self.titleTF];

            //line
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 1)];
            lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
            //[cell.contentView addSubview:lineView];

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

    } else if ([cellIdentifier isEqualToString:@"spacerCell"]) {

        //line in the start of the cell
        UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 1)];
        lineView2.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView2];

        if (indexPath.row == 0 && indexPath.section == 9) {

            [lineView2 removeFromSuperview];
        }


        cell.contentView.backgroundColor = UIColorFromRGB(0xE6E4E3);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;


        //line in the end of the cell
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];
    }

    else if ([cellIdentifier isEqualToString:@"infoCell"]) {

        if (indexPath.row == 0) {
            cell.contentView.backgroundColor = [UIColor whiteColor];//UIColorFromRGB(0xE6E4E3);

            cell.textLabel.text = KPLocalizedString(@"when");
            cell.textLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
            cell.textLabel.textColor = UIColorFromRGB(0x57423f);


            self.dateButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [self.dateButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]];
            self.dateButton.frame = CGRectMake(cell.frame.size.width - 190, 0.0f, 150.0f, 44.0f);
            //[self.dateButton.titleLabel setTextColor:UIColorFromRGB(0x57423f)];
            [self.dateButton setTitleColor:UIColorFromRGB(0x57423f) forState:UIControlStateNormal];
            [self.dateButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            [self.dateButton addTarget:self action:@selector(dateTap:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:self.dateButton];


            //line
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 1)];
            lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
            [cell.contentView addSubview:lineView];
        }
        else if (indexPath.row == 1) {


            cell.contentView.backgroundColor = [UIColor whiteColor];//UIColorFromRGB(0xE6E4E3);

            cell.textLabel.text = KPLocalizedString(@"where");
            cell.textLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
            cell.textLabel.textColor = UIColorFromRGB(0x57423f);


            self.placeButton = [UIButton buttonWithType:UIButtonTypeSystem];
            //[self.placeButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]];
            [self.placeButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Light" size:17]];
            self.placeButton.frame = CGRectMake(cell.frame.size.width - 190, 0.0f, 150.0f, 44.0f);
            [self.placeButton setTitleColor:UIColorFromRGB(0x57423f) forState:UIControlStateNormal];
            [self.placeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            [self.placeButton addTarget:self action:@selector(placeTap:) forControlEvents:UIControlEventTouchUpInside];

            [self.placeButton setTitle:@"add" forState:UIControlStateNormal];
            self.placeButton.titleLabel.alpha = 0.4;

            [cell.contentView addSubview:self.placeButton];


        }



        /*
        self.placeMarker = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location-icon"]];
        self.placeMarker.frame = CGRectMake(20.0f, 38.0f, 8.0f, 13.0f);
        [cell.contentView addSubview:self.placeMarker];
        
        self.placeButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.placeButton setTitle:@"Add a location" forState:UIControlStateNormal];
        [self.placeButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:13]];
        self.placeButton.frame = CGRectMake(33.0f, 33.0f, 238.0f, 22.0f);
        [self.placeButton.titleLabel setTextColor:UIColorFromRGB(0x329FC7)];
        [self.placeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.placeButton addTarget:self action:@selector(placeTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:self.placeButton];
         */

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
        [self reloadData];

 });
    return cell;
}

// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    NSString *cellIdentifier = nil;


    if (indexPath.section == 0) {
        cellIdentifier = @"kidsAndTitleOrDescriptionCell";
    } else if (indexPath.section == 1) {
        cellIdentifier = @"imageCell";//@"spacerCell";
    } else if (indexPath.section == 2) {
        cellIdentifier = @"recordStoryCell";
    } else if (indexPath.section == 3) {
        cellIdentifier = @"spacerCell";
    } else if (indexPath.section == 4) {
        cellIdentifier = @"infoCell";//@"tagsCell";
    } else if (indexPath.section == 5) {
        cellIdentifier = @"spacerCell";
    } else if (indexPath.section == 6) {
        cellIdentifier = @"tagsCell";
    } else if (indexPath.section == 7) {
        cellIdentifier = @"spacerCell";//@"infoCell";
    } else if (indexPath.section == 8) {
        cellIdentifier = @"privacyCell";
    } else if (indexPath.section == 8) {
        cellIdentifier = @"spacerCell";
    }


    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];


    if (cell == nil) {
        if (indexPath.section == 0) {
            cell = [self getCellForCellIdentifier:@"kidsAndTitleOrDescriptionCell" indexPath:indexPath];
        } else if (indexPath.section == 1) {
            cell = [self getCellForCellIdentifier:@"imageCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
        } else if (indexPath.section == 2) {
            cell = [self getCellForCellIdentifier:@"recordStoryCell" indexPath:indexPath];
        } else if (indexPath.section == 3) {
            cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
        } else if (indexPath.section == 4) {
            cell = [self getCellForCellIdentifier:@"infoCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"tagsCell" indexPath:indexPath];
        } else if (indexPath.section == 5) {
            cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
        } else if (indexPath.section == 6) {
            cell = [self getCellForCellIdentifier:@"tagsCell" indexPath:indexPath];
        } else if (indexPath.section == 7) {
            cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
        } else if (indexPath.section == 8) {
            cell = [self getCellForCellIdentifier:@"privacyCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
        }
        else if (indexPath.section == 9) {
            cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
        }


    }

    /*
    if ([[UserManager sharedInstance] isAfterRegistration]) {
        NSString *cellIdentifier = nil;
        if (indexPath.section == 0) {
            cellIdentifier = @"kidsAndTitleOrDescriptionCell";
        } else if (indexPath.section == 1) {
            cellIdentifier =  @"spacerCell";
        } else if (indexPath.section == 2) {
            cellIdentifier = @"recordStoryCell";
        } else if (indexPath.section == 3) {
            cellIdentifier = @"spacerCell";
        } else if (indexPath.section == 4) {
            cellIdentifier = @"tagsCell";
        } else if (indexPath.section == 5) {
            cellIdentifier = @"infoCell";
        } else if (indexPath.section == 6) {
            cellIdentifier = @"imageCell";
        }
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            if (indexPath.section == 0) {
                cell = [self getCellForCellIdentifier:@"kidsAndTitleOrDescriptionCell" indexPath:indexPath];
            } else if (indexPath.section == 1) {
                cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
            } else if (indexPath.section == 2) {
                cell = [self getCellForCellIdentifier:@"recordStoryCell" indexPath:indexPath];
            } else if (indexPath.section == 3) {
                cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
            } else if (indexPath.section == 4) {
                cell = [self getCellForCellIdentifier:@"tagsCell" indexPath:indexPath];
            } else if (indexPath.section == 5) {
                cell = [self getCellForCellIdentifier:@"infoCell" indexPath:indexPath];
            } else if (indexPath.section == 6) {
                cell = [self getCellForCellIdentifier:@"imageCell" indexPath:indexPath];
            }
        }
        return cell;
    } else {
        if (self.videoInfo){
            NSString *cellIdentifier = nil;
            if (indexPath.section == 0) {
                cellIdentifier = @"kidsAndTitleOrDescriptionCell";
            } else if (indexPath.section == 1) {
                cellIdentifier = @"imageCell";//@"spacerCell";
            } else if (indexPath.section == 2) {
                cellIdentifier = @"recordStoryCell";
            } else if (indexPath.section == 3) {
                cellIdentifier = @"spacerCell";
            } else if (indexPath.section == 4) {
                cellIdentifier = @"infoCell";//@"tagsCell";
            } else if (indexPath.section == 5) {
                cellIdentifier = @"spacerCell";
            } else if (indexPath.section == 6) {
                cellIdentifier = @"tagsCell";
            } else if (indexPath.section == 7) {
                cellIdentifier = @"spacerCell";//@"infoCell";
            } else if (indexPath.section == 8) {
                cellIdentifier = @"privacyCell";
            }else if (indexPath.section == 8) {
                cellIdentifier = @"spacerCell";
            }
            
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                if (indexPath.section == 0) {
                    cell = [self getCellForCellIdentifier:@"kidsAndTitleOrDescriptionCell" indexPath:indexPath];
                } else if (indexPath.section == 1) {
                    cell = [self getCellForCellIdentifier:@"imageCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 2) {
                    cell = [self getCellForCellIdentifier:@"recordStoryCell" indexPath:indexPath];
                } else if (indexPath.section == 3) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 4) {
                    cell = [self getCellForCellIdentifier:@"infoCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"tagsCell" indexPath:indexPath];
                } else if (indexPath.section == 5) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 6) {
                    cell = [self getCellForCellIdentifier:@"tagsCell" indexPath:indexPath];
                } else if (indexPath.section == 7) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 8) {
                    cell = [self getCellForCellIdentifier:@"privacyCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                }
                else if (indexPath.section == 9) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                }
            }
            return cell;
        }
        else{
            NSString *cellIdentifier = nil;
            if (indexPath.section == 0) {
                cellIdentifier = @"kidsAndTitleOrDescriptionCell";
            } else if (indexPath.section == 1) {
                cellIdentifier = @"imageCell";//@"spacerCell";
            } else if (indexPath.section == 2) {
                cellIdentifier = @"recordStoryCell";
            } else if (indexPath.section == 3) {
                cellIdentifier = @"spacerCell";
            } else if (indexPath.section == 4) {
                cellIdentifier = @"infoCell";//@"tagsCell";
            } else if (indexPath.section == 5) {
                cellIdentifier = @"spacerCell";
            } else if (indexPath.section == 6) {
                cellIdentifier = @"tagsCell";
            } else if (indexPath.section == 7) {
                cellIdentifier = @"spacerCell";//@"infoCell";
            } else if (indexPath.section == 8) {
                cellIdentifier = @"privacyCell";
            }else if (indexPath.section == 8) {
                cellIdentifier = @"spacerCell";
            }

            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                if (indexPath.section == 0) {
                    cell = [self getCellForCellIdentifier:@"kidsAndTitleOrDescriptionCell" indexPath:indexPath];
                } else if (indexPath.section == 1) {
                    cell = [self getCellForCellIdentifier:@"imageCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 2) {
                    cell = [self getCellForCellIdentifier:@"recordStoryCell" indexPath:indexPath];
                } else if (indexPath.section == 3) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 4) {
                    cell = [self getCellForCellIdentifier:@"infoCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"tagsCell" indexPath:indexPath];
                } else if (indexPath.section == 5) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 6) {
                    cell = [self getCellForCellIdentifier:@"tagsCell" indexPath:indexPath];
                } else if (indexPath.section == 7) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                } else if (indexPath.section == 8) {
                    cell = [self getCellForCellIdentifier:@"privacyCell" indexPath:indexPath];//[self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                }
                else if (indexPath.section == 9) {
                    cell = [self getCellForCellIdentifier:@"spacerCell" indexPath:indexPath];
                }
            }
             return cell;
        }
    }*/

    return cell;
}

//-(void) tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if([indexPath row] == 1 && [indexPath section] == 1){
//        //end of loading
//        //for example [activityIndicator stopAnimating];
//        
//        DDLogInfo(@"%lf",[self.titleTF contentSize].height);
//    }
//}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


    if (indexPath.section == 4) {
        if (indexPath.row == 0) {
            [self dateTap:nil];
        }
        else if (indexPath.row == 1) {
            [self placeTap:nil];
        }


    }

    else if (indexPath.section == 6) {
        TagsViewController *avc = [[TagsViewController alloc] initWithMode:0 andSelectedTags:self.tagIds];
        avc.delegate = self;
        [self.navigationController pushViewController:avc animated:YES];
    }
}

#pragma mark - TagsViewDelegate

- (void)tagViewClosed:(NSArray *)selectedTagIds {
    NSMutableArray *tagsNames = [[NSMutableArray alloc] init];
    NSMutableArray *newIds = [[NSMutableArray alloc] init];


    if ([selectedTagIds count] > 0) {
        self.tagsStrLabel.alpha = 1.0f;
        [self.tagsStrLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]];

    }

    for (int i = 0; i < [selectedTagIds count]; i++) {
        KPLocalTag *tag = (KPLocalTag *) selectedTagIds[i];
        [tagsNames addObject:tag.name];
        [newIds addObject:@(tag.serverID)];
    }

    if (![self.tagIds isEqual:newIds]) {
        self.tagsChanged = YES;
        self.tagIds = newIds;
        self.tagsStrLabel.text = [tagsNames componentsJoinedByString:@", "];

        if ([tagsNames count] == 0) {
            self.tagsStrLabel.text = KPLocalizedString(@"select");
        }

        self.tagsStr = self.tagsStrLabel.text;
    }
}

@end
