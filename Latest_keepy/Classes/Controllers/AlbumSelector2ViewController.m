//
//  AlbumSelector2ViewController.m
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AlbumSelector2ViewController.h"

#import "Keepy-Swift.h"

@interface AlbumSelector2ViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AlbumSelector2ViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.albums.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *AlbumCellIdentifier = @"AlbumCell2";
    AlbumCell2 *cell = [self.tableView dequeueReusableCellWithIdentifier:AlbumCellIdentifier];
    cell.collection = [self.albums objectAtIndex:indexPath.row];
    [cell layout];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [AlbumCell2 height];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate pictures:[self.albums objectAtIndex:indexPath.row]];
}

@end

@interface AlbumCell2 ()

@property(nonatomic, weak) IBOutlet UIImageView *albumThumbnail;
@property(nonatomic, weak) IBOutlet UILabel *albumTitle;
@property(nonatomic, weak) IBOutlet UILabel *albumCount;

@end

@implementation AlbumCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    self.albumTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f];
    self.albumCount.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17.0f];
}

+ (CGFloat)height {
    return 64.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.albumTitle.text = _collection.localizedTitle;
    
    PHFetchResult* result = [PHAsset fetchAssetsInAssetCollection:_collection options:nil];

    self.albumCount.text = [NSString stringWithFormat:@"%ld", (long) result.count];
    PHAsset* asset = result.lastObject;
    
    self.albumThumbnail.image = asset.thumbnail;// [UIImage imageWithCGImage:self.group.posterImage];
}

@end
