//
//  ContactsSelectorViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 6/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"
#import "FamilySuccessViewController.h"


@interface TFAContactsSelectorViewController : KPPopViewController {
    UISwitch *onoff;
    NSMutableArray *arrayData;
}
@property(copy) NSString *comeFromView;
@property(copy) NSString *campaignId;
@property(nonatomic, assign) NSInteger friendsCount;
@property(strong) NSObject <TFAContactsSelectorDelegate> *delegate;

- (instancetype)initWithMode:(NSInteger)mode;

- (instancetype)initWithMode:(NSInteger)mode andRelationMode:(NSInteger)relationMode;

@end
