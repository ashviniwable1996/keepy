//
//  TitlePictureCell.h
//  Keepy
//
//  Created by Troy Payne on 2/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    TitlePictureCellTypeMom,
    TitlePictureCellTypeDad,
    TitlePictureCellTypeGrandma,
    TitlePictureCellTypeGrandpa,
    TitlePictureCellTypeAunt,
    TitlePictureCellTypeUncle
} TitlePictureCellType;

@interface TitlePictureCell : UITableViewCell

@property(nonatomic, assign) TitlePictureCellType titlePictureCellType;

+ (CGFloat)height;

- (void)layout;

@end
