//
//  AlbumSelectorViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AlbumSelectorViewController.h"
#import "Keepy-Swift.h"

@interface AlbumSelectorViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AlbumSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"onboarding-choose-album-open"];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.albums.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *AlbumCellIdentifier = @"AlbumCell";
    AlbumCell *cell = [self.tableView dequeueReusableCellWithIdentifier:AlbumCellIdentifier];
//    cell.group = [self.albums objectAtIndex:indexPath.row];
    cell.collection = [self.albums objectAtIndex:indexPath.row];

    [cell layout];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [AlbumCell height];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate pictures:[self.albums objectAtIndex:indexPath.row]];
}

@end

@interface AlbumCell ()

@property(nonatomic, weak) IBOutlet UIImageView *albumThumbnail;
@property(nonatomic, weak) IBOutlet UILabel *albumTitle;
@property(nonatomic, weak) IBOutlet UILabel *albumCount;

@end

@implementation AlbumCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.albumTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f];
    self.albumCount.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17.0f];
}

+ (CGFloat)height {
    return 64.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

//    self.albumTitle.text = (NSString *) [self.group valueForProperty:ALAssetsGroupPropertyName];
//    self.albumCount.text = [NSString stringWithFormat:@"%ld", (long) self.group.numberOfAssets];
//    self.albumThumbnail.image = [UIImage imageWithCGImage:self.group.posterImage];
    
    self.albumTitle.text = _collection.localizedTitle;
    
    PHFetchResult* result = [PHAsset fetchAssetsInAssetCollection:_collection options:nil];
    
    self.albumCount.text = [NSString stringWithFormat:@"%ld", (long) result.count];
    PHAsset* asset = result.lastObject;
    
    self.albumThumbnail.image = asset.thumbnail;
}

@end
