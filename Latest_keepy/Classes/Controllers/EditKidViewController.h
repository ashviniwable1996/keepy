//
//  EditKidViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/10/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Kid.h"
#import "KPPopViewController.h"

@class KPLocalKid;

@protocol EditKidDelegate

- (void)kidAdded:(KPLocalKid *)kid;

@end

@interface EditKidViewController : KPPopViewController

@property(nonatomic, assign) id <EditKidDelegate> delegate;

- (instancetype)initWithKid:(KPLocalKid *)akid;

@end
