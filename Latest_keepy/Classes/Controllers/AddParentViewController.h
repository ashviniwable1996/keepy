//
//  AddParentViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    AddParentViewControllerTypeMom = 0,
    AddParentViewControllerTypeDad = 1
} AddParentViewControllerType;

@interface AddParentViewController : UIViewController

@property(nonatomic, assign) AddParentViewControllerType addParentViewControllerType;
@property(nonatomic, strong) NSArray *family;

- (CGFloat)height;

@end
