//
//  CircleCropViewController.m
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "CircleCropViewController.h"
#import "UIImage+Resize.h"

@interface CircleCropViewController ()

@property(nonatomic, weak) IBOutlet UIView *containerView;
@property(nonatomic, strong) IBOutlet UIImageView *imageView;
@property(nonatomic, strong) UIView *boundingBox;
@property(nonatomic, weak) IBOutlet UIButton *backbutton;

@end

@implementation CircleCropViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //DDLogInfo(@"self.scale %f", self.scale);

    /* if(self.parentIsEditKidViewController) {
         backbutton setTitle:@"Pictures" forState:<#(UIControlState)#>
     }*/


}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.scale = (self.scale == 0) ? 1.0f : self.scale;
    
    
    if (self.scale != 0.0) {
        self.imageView.transform = CGAffineTransformScale(self.imageView.transform, self.scale, self.scale);
    }
    
    self.scale = 1.0f;
    
    /*
     self.imageView = [[UIImageView alloc] initWithImage:self.image];
     //    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
     DDLogInfo(@"contentScaleFactor %f:", [self.imageView contentScaleFactor]);
     [self.containerView addSubview:self.imageView];
     
     CGFloat screenScale = [[UIScreen mainScreen] scale];
     DDLogInfo(@"screenScale %f:", screenScale);
     //self.imageView.bounds = CGRectMake(0.0f, 0.0f, (self.image.size.width / screenScale), (self.image.size.height / screenScale));
     DDLogInfo(@"self.image.size.width %f", self.image.size.width );
     DDLogInfo(@"self.image.size.height %f", self.image.size.height );
     self.imageView.center = self.containerView.center;
     
     */
    
    self.imageView.image = self.image;
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    self.imageView.bounds = CGRectMake(0, 0, (self.image.size.width / screenScale), (self.image.size.height / screenScale));
    self.imageView.center = self.containerView.center;
    //self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, 20.0f, self.imageView.frame.size.width, self.imageView.frame.size.height);
    
    
    self.boundingBox = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 240.0f, 240.0f)];
    self.boundingBox.center = self.imageView.center;
    self.boundingBox.frame = CGRectMake(self.boundingBox.frame.origin.x, 20.0f, self.boundingBox.frame.size.width, self.boundingBox.frame.size.height);
    self.boundingBox.userInteractionEnabled = NO;
    self.boundingBox.opaque = NO;
    self.boundingBox.alpha = 0;
    //self.boundingBox.backgroundColor = [UIColor redColor];
    [self.containerView addSubview:self.boundingBox];
    self.imageView.center = self.boundingBox.center;
    
    
    // DDLogInfo(@"self.originPoint.x %f", self.originPoint.x );
    // DDLogInfo(@"self.originPoint.y %f", self.originPoint.y );
    if (self.originPoint.x == 0 && self.originPoint.y == 0) {
        self.originPoint = self.imageView.frame.origin;
    }
    
    CGRect frame = self.imageView.frame;
    frame.origin.x = self.originPoint.x;
    frame.origin.y = self.originPoint.y;
    self.imageView.frame = frame;
    
    
    CALayer *containerLayer = [CALayer layer];
    containerLayer.bounds = self.containerView.bounds;
    containerLayer.position = self.containerView.layer.position;
    containerLayer.frame = CGRectMake(containerLayer.frame.origin.x, 0.0f, containerLayer.frame.size.width, containerLayer.frame.size.height);
    
    CALayer *whiteLayer = [CALayer layer];
    whiteLayer.bounds = containerLayer.bounds;
    whiteLayer.position = CGPointMake((containerLayer.bounds.size.width / 2.0f), (containerLayer.bounds.size.height / 2.0f));
    whiteLayer.backgroundColor = [UIColor colorWithRed:(230.0f / 255.0f) green:(228.0f / 255.0f) blue:(227.0f / 255.0f) alpha:1.0f].CGColor;
    [containerLayer addSublayer:whiteLayer];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.bounds = containerLayer.bounds;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(((whiteLayer.bounds.size.width / 2.0f) - 120.0f), 20.0f, 240.0f, 240.0f)];
    [path appendPath:[UIBezierPath bezierPathWithRect:maskLayer.bounds]];
    
    maskLayer.fillColor = [UIColor blackColor].CGColor;
    maskLayer.path = path.CGPath;
    maskLayer.position = CGPointMake((whiteLayer.bounds.size.width / 2.0f), (whiteLayer.bounds.size.height / 2.0f));
    maskLayer.fillRule = kCAFillRuleEvenOdd;
    
    whiteLayer.mask = maskLayer;
    [self.containerView.layer addSublayer:containerLayer];
    
    // DDLogInfo(@"viewDidLoad");
    
    [self ensureScale];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self.imageView addGestureRecognizer:panGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
    [self.imageView addGestureRecognizer:pinchGesture];
}
- (void)ensureScale {
    UIView *box = [[UIView alloc] initWithFrame:CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y, self.imageView.frame.size.width, self.imageView.frame.size.height)];

    BOOL hasChangedTransform = false;
    while (!CGRectContainsRect(box.frame, self.boundingBox.frame)) {
        self.scale = self.scale + 0.1f;
        box.transform = CGAffineTransformScale(box.transform, self.scale, self.scale);

        hasChangedTransform = true;
    }
    if (hasChangedTransform) {
        self.imageView.transform = box.transform;
    }
}

- (void)pan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:self.containerView];
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.containerView];

    CGPoint center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
    CGRect box = CGRectMake((center.x - (self.imageView.frame.size.width / 2.0f)), (center.y - (self.imageView.frame.size.height / 2.0f)), self.imageView.frame.size.width, self.imageView.frame.size.height);

    if (CGRectContainsRect(box, self.boundingBox.frame)) {
        recognizer.view.frame = box;
    }
}

- (void)pinch:(UIPinchGestureRecognizer *)recognizer {
    UIView *box = [[UIView alloc] initWithFrame:CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y, self.imageView.frame.size.width, self.imageView.frame.size.height)];
    box.transform = CGAffineTransformScale(box.transform, recognizer.scale, recognizer.scale);
    if (CGRectContainsRect(box.frame, self.boundingBox.frame)) {
        self.imageView.transform = CGAffineTransformScale(self.imageView.transform, recognizer.scale, recognizer.scale);
        self.scale = recognizer.scale;
        recognizer.scale = 1.0f;
    }
}


- (IBAction)cancel:(id)sender {
//    Handled crash if the imageView Contains the NaN Values.
//    if ((isnan(self.imageView.frame.size.height)) || (isnan(self.imageView.frame.size.width)) || (isnan(self.imageView.frame.origin.y))) {
//        [self.navigationController popViewControllerAnimated:YES];
//
//    } else {
//        if ((self.imageView.frame.size.height != self.imageView.frame.size.height) || (self.imageView.frame.size.width != self.imageView.frame.size.width) || (self.imageView.frame.origin.y != self.imageView.frame.origin.y)){
//            NSLog(@"NaN on cancel.");
//        } else {
//            self.imageView.frame = CGRectMake(0, self.imageView.frame.origin.y, self.imageView.frame.size.width, self.imageView.frame.size.height);
//            [self.navigationController popViewControllerAnimated:YES];
//        }
//    }
    
    // commented above and added below by Ashvini
    if ((isnan(self.imageView.frame.size.height)) || (isnan(self.imageView.frame.size.width)) || (isnan(self.imageView.frame.origin.y)) || (self.imageView.frame.size.height != self.imageView.frame.size.height) || (self.imageView.frame.size.width != self.imageView.frame.size.width) || (self.imageView.frame.origin.y != self.imageView.frame.origin.y)) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        self.imageView.frame = CGRectMake(0, self.imageView.frame.origin.y, self.imageView.frame.size.width, self.imageView.frame.size.height);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
- (IBAction)cancel:(id)sender {
    [self.delegate doneCircleCropping:nil];
}
*/

- (IBAction)done:(id)sender {

    UIImage *originalImage = self.imageView.image;

    UIImage *scaledImage = [self.imageView.image resizedImage:CGSizeMake(self.imageView.image.size.width * self.scale, self.imageView.image.size.height * self.scale) interpolationQuality:kCGInterpolationHigh];


    CGFloat screenScale = [[UIScreen mainScreen] scale];
    CGRect boundingBoxFrame = [self.containerView convertRect:self.boundingBox.frame toView:self.imageView];


    CGRect doubledUp = CGRectMake(boundingBoxFrame.origin.x * screenScale, (boundingBoxFrame.origin.y) * screenScale, (boundingBoxFrame.size.width * screenScale), (boundingBoxFrame.size.height * screenScale));

    /*UIView *tmpView = [[UIView alloc] initWithFrame:boundingBoxFrame];
    tmpView.backgroundColor = [UIColor blueColor];
    [self.imageView addSubview:tmpView];
    return;*/


    // self.imageView.image = scaledImage; return;

    CGImageRef imageRef = CGImageCreateWithImageInRect(self.imageView.image.CGImage, doubledUp);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef scale:scaledImage.scale orientation:scaledImage.imageOrientation];
    CGImageRelease(imageRef);


    croppedImage = [croppedImage resizedImage:CGSizeMake(240.0f, 240.0f) interpolationQuality:kCGInterpolationHigh];

    //DDLogInfo(@"croppedImage.size.width %f:", croppedImage.size.width );
    //DDLogInfo(@"croppedImage.size.height %f:", croppedImage.size.height);

    //self.imageView.image = croppedImage; return;


    //DDLogInfo(@"imageView.frame.origin.x %f", self.imageView.frame.origin.x );
    //DDLogInfo(@"imageView.frame.origin.y %f", self.imageView.frame.origin.y );
    // DDLogInfo(@"self.imageView.transform.a %f", self.imageView.transform.a);
    
    // Handled condition if original & croppedImages are nil then do not perform the task.
    if (originalImage != nil ) {
        if (croppedImage == nil) {
            croppedImage = originalImage;
        }
        NSDictionary *selectedPhotosDict = @{@"originalImage" : originalImage, @"croppedImage" : croppedImage, @"photo_scale" : [NSNumber numberWithFloat:self.imageView.transform.a], @"photo_x" : [NSNumber numberWithFloat:self.imageView.frame.origin.x], @"photo_y" : [NSNumber numberWithFloat:self.imageView.frame.origin.y]};
        [self.delegate doneCircleCropping:selectedPhotosDict];
    }
   
}


@end
