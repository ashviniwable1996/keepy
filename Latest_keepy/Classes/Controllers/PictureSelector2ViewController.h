//
//  PictureSelector2ViewController.h
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@import Photos;

@protocol PictureSelector2ViewControllerDelegate;

@interface PictureSelector2ViewController : UIViewController

@property(nonatomic, weak) id <PictureSelector2ViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *pictures;

@end

@protocol PictureSelector2ViewControllerDelegate <NSObject>

- (void)albums:(id)sender;

- (void)pictureSelected:(id)sender;

@end

@protocol PictureCell2Delegate;

@interface PictureCell2 : UITableViewCell

@property(nonatomic, weak) id <PictureCell2Delegate> delegate;
@property(nonatomic, strong) NSArray <PHAsset*> *pictures;

+ (CGFloat)height;

- (void)layout;

@end

@protocol PictureCell2Delegate <NSObject>

- (void)pictureSelected:(PHAsset *)asset;

@end
