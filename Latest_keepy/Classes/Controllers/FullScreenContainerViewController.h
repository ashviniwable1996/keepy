//
//  FullScreenContainerViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 4/9/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullScreenContainerViewController : UIViewController

- (instancetype)initWithItems:(NSArray *)items andIndex:(NSInteger)index withCommentIndex:(NSInteger)commentIndex;

@end
