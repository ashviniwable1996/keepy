//
//  ContactsSelectorViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 6/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@interface ContactsSelectorViewController : KPPopViewController

- (instancetype)initWithMode:(NSInteger)mode;

- (instancetype)initWithMode:(NSInteger)mode andRelationMode:(NSInteger)relationMode;

@end
