//
//  FullScreenItemCell.m
//  Keepy
//
//  Created by Yaniv Solnik on 4/9/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <MagicalRecord/CoreData+MagicalRecord.h>


#import "FullScreenItemCell.h"
#import "KPLocalItem.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AppDelegate.h"
#import "RecordViewController.h"
#import "ItemEditViewController.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "NoteEditorViewController.h"
#import "DKMoviePlayerViewController.h"
#import "KPLocalAsset.h"
#import "KPLocalKidItem.h"
#import "KPLocalKid.h"
#import "KPLocalStory.h"

#import "Keepy-Swift.h"

@interface FullScreenItemCell () <AVAudioPlayerDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, RecordViewControllerDelegate, ItemEditDelegate, NoteEditorDelegate, UIActionSheetDelegate>

@property(nonatomic, strong) UIView *topView;
@property(nonatomic, strong) UIView *bottomView;
@property(nonatomic, strong) UIButton *playCommentButton;
@property(nonatomic, strong) UILabel *commentTimelineLabel;
@property(nonatomic, strong) AVAudioPlayer *player;
@property(nonatomic, strong) UIActivityIndicatorView *commentLoadingIndicator;
@property(nonatomic, strong) MPMoviePlayerController *moviePlayer;
@property(nonatomic, strong) UIImageView *itemImageView;
//@property(nonatomic, strong) UIView *backgroundView;
@property(nonatomic, strong) UILabel *commentTimeCurrentLabel;
@property(nonatomic, strong) UILabel *commentTimeEndLabel;
@property(nonatomic, strong) UIImageView *progressFiller;
@property(nonatomic, strong) UIButton *knobButton;

@property(nonatomic, strong) UIImageView *progressBkg;
@property(nonatomic, strong) UIView *videoContainer;
@property(nonatomic, strong) UIButton *thankyouButton;
@property(nonatomic, strong) UIScrollView *commentsScroll;
@property(nonatomic, strong) NSArray *comments;
@property(nonatomic, strong) UIImageView *kidImg;
@property(nonatomic, strong) UILabel *kidNameLabel;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) UIButton *editButton;
@property(nonatomic, strong) UIActivityIndicatorView *imageActivityIndicator;
@property(nonatomic, strong) UILabel *txtLabel;
@property(nonatomic, strong) UIImageView *quoteImageView;
@property(nonatomic, strong) UIScrollView *txtScrollView;
@property(nonatomic, strong) UIImageView *txtBkg;
@property(nonatomic, strong) UIButton *playVideoButton;
@property(nonatomic, strong) UIScrollView *imgScrollView;
@property(nonatomic, strong) DKMoviePlayerViewController *playView;

@property(nonatomic, strong) UIImageView *pauseBG;
@property(nonatomic, strong) UIImageView *playBG;


@property(nonatomic) BOOL infoHidden;
@property(nonatomic) BOOL MoviePlayerChangeURL;
@property(nonatomic) NSInteger currentCommentIndex;
@property(nonatomic) NSInteger lastCommentPlay;
@property(nonatomic) BOOL isClosing;

@property(nonatomic, strong) UIView *txtView;
@property(nonatomic, assign) CGRect originalTopViewFrame;
@property(nonatomic, assign) CGRect originalBottomViewFrame;

@end

@implementation FullScreenItemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //This will make sure that the observer is added only once.
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"stopPlayVoice" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"hideWindowChrome" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showWindowChrome" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayVoice:) name:@"stopPlayVoice" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideWindowChrome:) name:@"hideWindowChrome" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showWindowChrome:) name:@"showWindowChrome" object:nil];
        //DDLogInfo(@"xxx:%2f %2f", self.bounds.size.width, self.bounds.size.height);
        [self drawUI];
        self.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
        self.originalTopViewFrame = self.topView.frame;
        self.originalBottomViewFrame = self.bottomView.frame;
    }
    return self;
}

- (void)layoutSubviews {
    //DDLogInfo(@"here:%2f %2f", self.bounds.size.width, self.bounds.size.height);
    //if (self.backgroundView == nil)
    //    [self drawUI];
    
    float ay = self.topView.frame.origin.y + self.topView.frame.size.height + 10;
    self.txtScrollView.frame = CGRectMake(0, ay, self.bounds.size.width, self.bounds.size.height - ay - 10);
}

- (void)drawUI {
    //self.view.frame = CGRectMake(0, 0,  w.frame.size.width, w.frame.size.height);
    float h = self.frame.size.height;
    float w = self.frame.size.width;
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, h)];
    //    self.backgroundView.backgroundColor = UIColorFromRGB(0xe6e5e3);
    self.backgroundView.alpha = 0;
    self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:self.backgroundView];
    
    //image
    self.itemImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, w - 20, h)];
    self.itemImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.itemImageView.backgroundColor = [UIColor clearColor];
    self.itemImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.imgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, w, h)];
    self.imgScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //    self.imgScrollView.backgroundColor = UIColorFromRGB(0xe6e5e3);
    
    [self.imgScrollView setScrollEnabled:YES];
    [self.imgScrollView setUserInteractionEnabled:YES];
    [self.imgScrollView addSubview:self.itemImageView];
    [self.imgScrollView setMinimumZoomScale:1.0f];
    [self.imgScrollView setMaximumZoomScale:3.0f];
    [self.imgScrollView setDelegate:self];
    [self addSubview:self.imgScrollView];
    
    UIImage *playVideoBig = [UIImage imageNamed:@"play-video-big"];
    self.playVideoButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, playVideoBig.size.width, playVideoBig.size.height)];
    [self.playVideoButton setImage:playVideoBig forState:UIControlStateNormal];
    self.playVideoButton.center = self.imgScrollView.center;
    [self.playVideoButton addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.playVideoButton];
    
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap:)];
    gesture.numberOfTapsRequired = 1;
    [self addGestureRecognizer:gesture];
    
    UITapGestureRecognizer *doubleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageDoubleTap:)];
    doubleGesture.numberOfTapsRequired = 2;
    [self.imgScrollView addGestureRecognizer:doubleGesture];
    [gesture requireGestureRecognizerToFail:doubleGesture];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.imgScrollView addGestureRecognizer:swipeGesture];
    swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.imgScrollView addGestureRecognizer:swipeGesture];
    
    //top view
    float ah = 101;
    
    self.topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, ah)];
    self.topView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, ah)];
    v.backgroundColor = UIColorFromRGB(0xe6e4e3);
    v.alpha = 0.5;
    v.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.topView addSubview:v];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, w, ah)];
    bar.barTintColor = UIColorFromRGB(0xe6e4e3);
    bar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.topView addSubview:bar];
    
    v = [[UIView alloc] initWithFrame:CGRectMake(0, ah - 1, w, 1)];
    v.backgroundColor = UIColorFromRGB(0xab9892);
    v.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.topView addSubview:v];
    
    //img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 53, 53)];
    //img.image = [UIImage imageNamed:@"kidPicBg"];
    //img.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    //[self.topView addSubview:img];
    
    self.kidImg = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13 + 20, 47, 47)];
    self.kidImg.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.topView addSubview:self.kidImg];
    
    self.kidNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(68, 15 + 20, 230, 15)];
    self.kidNameLabel.backgroundColor = [UIColor clearColor];
    self.kidNameLabel.textColor = UIColorFromRGB(0x523c37);
    self.kidNameLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:14];
    self.kidNameLabel.textAlignment = NSTextAlignmentLeft;
    self.kidNameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.topView addSubview:self.kidNameLabel];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(68, 30 + 20, 220, 15)];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = UIColorFromRGB(0x523c37);
    self.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    //self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.titleLabel.numberOfLines = 10;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.topView addSubview:self.titleLabel];
    
    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(68, 45 + 20, 230, 15)];
    // self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 45 + 20, 230, 15)];
    self.dateLabel.backgroundColor = [UIColor clearColor];
    self.dateLabel.textColor = UIColorFromRGB(0x523c37);
    self.dateLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    self.dateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.topView addSubview:self.dateLabel];
    
    //close button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"close-button"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(closeTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(w - 27 - 17, -4 + 20, 44, 44);
    btn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.topView addSubview:btn];
    
    //edit button
    self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.editButton setImage:[UIImage imageNamed:@"edit-button-full-screen"] forState:UIControlStateNormal];
    [self.editButton addTarget:self action:@selector(editTap:) forControlEvents:UIControlEventTouchUpInside];
    self.editButton.frame = CGRectMake(w - 27 - 17, 52, 44, 44);
    btn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.topView addSubview:self.editButton];
    
    [self addSubview:self.topView];
    
    //Quote UI
    self.txtScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, w, h - 160)];
    self.txtScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap:)];
    gesture.numberOfTapsRequired = 1;
    [self.txtScrollView addGestureRecognizer:gesture];
    
    [self addSubview:self.txtScrollView];
    
    self.txtBkg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 300, 10)];
    self.txtBkg.backgroundColor = [UIColor whiteColor];
    [self.txtScrollView addSubview:self.txtBkg];
    
    self.txtLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 50, w - 30, h)];
    self.txtLabel.textColor = UIColorFromRGB(0x523c37);
    self.txtLabel.textAlignment = NSTextAlignmentLeft;
    self.txtLabel.backgroundColor = [UIColor clearColor];
    self.txtLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:16];
    self.txtLabel.numberOfLines = 100;
    [self.txtScrollView addSubview:self.txtLabel];
    
    self.quoteImageView = [[UIImageView alloc] initWithFrame:CGRectMake((w - 32) / 2, 16, 32, 24)];
    self.quoteImageView.image = [UIImage imageNamed:@"quote-icon"];
    [self.txtScrollView addSubview:self.quoteImageView];
    
    
    
    
    
    
    //bottomView
    
    float dh = 0;
    //if (self.item.story != nil || self.mode == 1)
    dh = 52;
    
    //if ([self.comments count] > 1)
    //{
    dh += 85;
    //}
    
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, h - 44 - dh, w, 44 + dh)];
    self.bottomView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, 44 + dh)];
    v.backgroundColor = UIColorFromRGB(0xe6e4e3);
    v.alpha = 0.5;
    v.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.bottomView addSubview:v];
    
    bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, w, 44 + dh)];
    bar.barTintColor = UIColorFromRGB(0xe6e4e3);
    [self.bottomView addSubview:bar];
    
    //video player
    self.playCommentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
    self.playCommentButton.frame = CGRectMake(10, (52 - 32) / 2, 32, 32);
    [self.playCommentButton addTarget:self action:@selector(playButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    self.playCommentButton.alpha = 0;
    self.playCommentButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    //[self.bottomView addSubview:self.playCommentButton];
    
    self.commentLoadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.commentLoadingIndicator.hidesWhenStopped = YES;
    self.commentLoadingIndicator.frame = self.playCommentButton.frame;
    //[self.commentLoadingIndicator startAnimating];
    self.commentLoadingIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.bottomView addSubview:self.commentLoadingIndicator];
    
    self.commentTimeCurrentLabel = [[UILabel alloc] initWithFrame:CGRectMake(3, (52 - 20) / 2, 120, 20)];
    self.commentTimeCurrentLabel.backgroundColor = [UIColor clearColor];
    self.commentTimeCurrentLabel.textColor = UIColorFromRGB(0x523c37);
    self.commentTimeCurrentLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
    self.commentTimeCurrentLabel.textAlignment = NSTextAlignmentLeft;
    self.commentTimeCurrentLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    self.commentTimeCurrentLabel.text = @"loading video...";
    
    [self.bottomView addSubview:self.commentTimeCurrentLabel];
    
    self.commentTimeEndLabel = [[UILabel alloc] initWithFrame:CGRectMake(275, (52 - 20) / 2, 40, 20)];
    self.commentTimeEndLabel.backgroundColor = [UIColor clearColor];
    self.commentTimeEndLabel.textColor = UIColorFromRGB(0x523c37);
    self.commentTimeEndLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
    self.commentTimeEndLabel.textAlignment = NSTextAlignmentCenter;
    self.commentTimeEndLabel.text = @"0:00";
    self.commentTimeEndLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.bottomView addSubview:self.commentTimeEndLabel];
    self.commentTimeEndLabel.alpha = 0;
    
    self.progressBkg = [[UIImageView alloc] initWithFrame:CGRectMake(40, (55 - 3) / 2, 235, 3)];
    self.progressBkg.image = [[UIImage imageNamed:@"progress-bar-defaultBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 2)];
    self.progressBkg.alpha = 0;
    self.progressBkg.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.bottomView addSubview:self.progressBkg];
    
    self.progressFiller = [[UIImageView alloc] initWithFrame:CGRectMake(40, (55 - 3) / 2, 0, 3)];
    self.progressFiller.image = [[UIImage imageNamed:@"progress-bar-filler"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 2, 0, 2)];
    self.progressFiller.alpha = 0;
    self.progressBkg.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.bottomView addSubview:self.progressFiller];
    
    self.knobButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.knobButton setImage:[UIImage imageNamed:@"circle-for-play-bar"] forState:UIControlStateNormal];
    //[btn addTarget:self action:@selector(closeTap:) forControlEvents:UIControlEventTouchUpInside];
    self.knobButton.frame = CGRectMake(40, (55 - 19) / 2, 17, 19);
    self.knobButton.alpha = 0;
    self.knobButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.bottomView addSubview:self.knobButton];
    
    
    self.commentsScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 15, w - 20, 80)];
    [self.bottomView addSubview:self.commentsScroll];
    
    _pauseBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pause-layer-on-thumbnails"]];
    [_pauseBG setFrame:CGRectMake(0, 0, 70, 70)];
    [self.commentsScroll addSubview:_pauseBG];
    
    _playBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play-layer-on-thumbnails"]];
    [_playBG setFrame:CGRectMake(0, 0, 70, 70)];
    [self.commentsScroll addSubview:_playBG];
    
    //comment button
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 300;
    //[btn setImage:[UIImage imageNamed:@"comment-button-full-screen"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(commentTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(0, dh + 3, 138, 44);
    [btn setImage:[UIImage imageNamed:@"comment-icon-"] forState:UIControlStateNormal];
    [btn setTitle:KPLocalizedString(@"comment") forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
    [btn setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 10);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
    btn.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    [self.bottomView addSubview:btn];
    
    //share button
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 400;
    [btn setImage:[UIImage imageNamed:@"feed-image-share-icon"] forState:UIControlStateNormal];
    [btn setTitle:KPLocalizedString(@"share") forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16];
    [btn setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(shareTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(138, dh + 3, 138, 44);
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 8, 10);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
    btn.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    [self.bottomView addSubview:btn];
    
    //buy button
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 500;
    
    
    // Item *item__ = (Item*)[self.items objectAtIndex:indexPath.row];
    // DDLogInfo(@"%hd",item__.itemType.intValue);
    
    
    
    
    //[btn setImage:[UIImage imageNamed:@"cart-icon"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"album-icon"] forState:UIControlStateNormal];
    btn.frame = CGRectMake(320 - 42, dh , 44, 44);
    [btn addTarget:self action:@selector(buyTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    [self.bottomView addSubview:btn];
    
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, dh, w, 1)];
    img.backgroundColor = UIColorFromRGB(0xab9892);
    img.alpha = 0.5;
    img.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.bottomView addSubview:img];
    
    //Seperator Line
    img = [[UIImageView alloc] initWithFrame:CGRectMake(139, dh, 1, 44)];
    img.backgroundColor = UIColorFromRGB(0xab9892);
    img.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    img.alpha = 0.5;
    img.tag = 1001;
    [self.bottomView addSubview:img];
    
    img = [[UIImageView alloc] initWithFrame:CGRectMake(2 * 139, dh, 1, 44)];
    img.backgroundColor = UIColorFromRGB(0xab9892);
    img.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    img.alpha = 0.5;
    img.tag = 1002;
    [self.bottomView addSubview:img];
    
    
    [self addSubview:self.bottomView];
    
    //Video Container
    
    self.videoContainer = [[UIView alloc] initWithFrame:CGRectMake(w - 122, 70, 112, 112)];
    self.videoContainer.backgroundColor = [UIColor whiteColor];
    [self.videoContainer.layer setBorderWidth:2];
    [self.videoContainer.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    self.videoContainer.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    self.videoContainer.alpha = 0;
    [self addSubview:self.videoContainer];
    
    UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    av.hidesWhenStopped = YES;
    CGRect aframe = av.frame;
    aframe.origin.x = (self.videoContainer.frame.size.width - aframe.size.width) / 2;
    aframe.origin.y = (self.videoContainer.frame.size.height - aframe.size.height) / 2;
    av.frame = aframe;
    [self.videoContainer addSubview:av];
    [av startAnimating];
    
    float ax = self.videoContainer.frame.origin.x + (self.videoContainer.frame.size.width - 113) / 2;
    float ay = self.videoContainer.frame.origin.y + 93;
    self.thankyouButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.thankyouButton.frame = CGRectMake(ax, ay, 113, 29);
    [self.thankyouButton setImage:[UIImage imageNamed:@"say-thanks"] forState:UIControlStateNormal];
    self.thankyouButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    self.thankyouButton.hidden = YES;
    [self addSubview:self.thankyouButton];
    
    self.imageActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.imageActivityIndicator.frame = CGRectMake((w - 50) / 2, (h - 50) / 2, 50, 50);
    self.imageActivityIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.imageActivityIndicator.hidesWhenStopped = YES;
    [self addSubview:self.imageActivityIndicator];
    
    
    self.item = self.item;
    
    //if (self.isActive)
    //    [self didBecomeActive];
}


- (void)playTheVideo:(id)sender {
    
    KPLocalAsset *itemOriginalImage = [KPLocalAsset fetchByLocalID:self.item.originalImageAssetID];
    
    self.playVideoButton.hidden = YES;
    NSURL *streamURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.mp4", Defaults.assetsURLString, itemOriginalImage.urlHash]];
    self.playView = [DKMoviePlayerViewController sharedClient];
    
    self.playView.uploadVideoMode = NO;
    
    
    [self.playView playMovieStream:streamURL bufferReady:^(BOOL ready) {
        if (![self viewWithTag:10210]) {
            self.playView.view.tag = 10210;
            [self performSelector:@selector(action) withObject:nil afterDelay:0.1f];
        }
    }];
}

- (void)playVideo:(id)sender {
    
    if (self.player.playing) {
        _pauseBG.hidden = YES;
        [self.player stop];
        self.player = nil;
    }
    
    //[self.playVideoButton setImage:[UIImage imageNamed:@"pause-video-big"] fBGorState:UIControlStateNormal];
    
    [self performSelector:@selector(playTheVideo:) withObject:sender afterDelay:0.3];
    
}


- (void)action {
    //Remove the pause image from comments because video are going to play
    [_pauseBG removeFromSuperview];
    [_playBG removeFromSuperview];
    
    
    [self.playView.view setAlpha:0.f];
    [self addSubview:self.playView.view];
    
    [UIView animateWithDuration:0.5f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.playView.view setAlpha:1.f];
    }                completion:^(BOOL finished) {
        
        // DDLogInfo(@"START PLAY");
        [self.playView tapPlay:nil];
    }];
    
    //[self bringSubviewToFront:self.topView];
    //[self bringSubviewToFront:self.bottomView];
    // [self.playView tapPlay:nil];
}

- (void)hideWindowChrome:(NSNotification *)notification {
    
    self.topView.hidden = YES;
    self.bottomView.hidden = YES;
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.infoHidden = YES;
}

- (void)showWindowChrome:(NSNotification *)notification {
    
    [self bringSubviewToFront:self.topView];
    [self bringSubviewToFront:self.bottomView];
    [self.topView setAlpha:0.f];
    [self.bottomView setAlpha:0.f];
    
    [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self.topView setAlpha:1.f];
        [self.bottomView setAlpha:1.f];
    }                completion:^(BOOL finished) {
        
    }];
    
    
    self.infoHidden = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setItem:(KPLocalItem *)item {
    _item = item;
    if (_item != item) {
        self.itemImageView.image = nil;
    }
    
    if (item == nil) {
        return;
    }
    
    
    self.playView = [DKMoviePlayerViewController sharedClient];
    [self.playView tapPause:nil];
    
    if ([self viewWithTag:10210]) {
        [[self viewWithTag:10210] removeFromSuperview];
    }
    
    self.backgroundView.backgroundColor = [UIColor blackColor];
    [(UIView *) self.topView.subviews[0] setHidden:NO];
    [(UIView *) self.bottomView.subviews[0] setHidden:NO];
    self.quoteImageView.hidden = YES;
    self.itemImageView.hidden = NO;
    self.txtScrollView.hidden = YES;
    
    /*
     self.kidNameLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
     self.titleLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
     self.dateLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
     
     [(UIButton*)[self.bottomView.subviews objectAtIndex:[self.bottomView.subviews count]-3] setImage:[UIImage imageNamed:@"share-button-full-screen"] forState:UIControlStateNormal];
     [(UIButton*)[self.bottomView.subviews objectAtIndex:[self.bottomView.subviews count]-4] setImage:[UIImage imageNamed:@"comment-button-full-screen"] forState:UIControlStateNormal];
     */
    
    //Title
    
    CGRect aframe = self.titleLabel.frame;
    float originalHeight = aframe.size.height;
    CGSize titlTextSize = [item.title sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:12] thatFitsToSize:CGSizeMake(220, 100) lineBreakMode:NSLineBreakByWordWrapping];
    
    aframe.size.height = (titlTextSize.height > originalHeight) ? titlTextSize.height : originalHeight;
    self.titleLabel.frame = aframe;
    float deltaH = aframe.size.height - originalHeight;
    
    if (deltaH > 0) {
        aframe = self.dateLabel.frame;
        aframe.origin.y += deltaH + 5;
        self.dateLabel.frame = aframe;
    }
    
    aframe = self.topView.frame;
    aframe.size.height += deltaH;
    self.topView.frame = aframe;
    
    aframe = [(UIView *) self.topView.subviews[2] frame];
    aframe.origin.y += deltaH;
    [(UIView *) self.topView.subviews[2] setFrame:aframe];
    
    //self.txtScrollView.frame = CGRectMake(0, deltaH + 115, self.bounds.size.width, self.bounds.size.height-165-deltaH);
    
    if (item.itemType == 2)//quote
    {
        /*
         self.kidNameLabel.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
         self.titleLabel.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
         self.dateLabel.textColor = [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1];
         
         [(UIButton*)[self.bottomView.subviews objectAtIndex:[self.bottomView.subviews count]-3] setImage:[UIImage imageNamed:@"share-button-brown-full-screen"] forState:UIControlStateNormal];
         [(UIButton*)[self.bottomView.subviews objectAtIndex:[self.bottomView.subviews count]-4] setImage:[UIImage imageNamed:@"comment-button-brown-full-screen"] forState:UIControlStateNormal];
         */
        
        //self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
        [(UIView *) self.topView.subviews[0] setHidden:YES];
        [(UIView *) self.bottomView.subviews[0] setHidden:YES];
        
        self.quoteImageView.hidden = NO;
        self.txtScrollView.hidden = NO;
        self.itemImageView.hidden = YES;
        
        
        self.txtLabel.text = item.extraData;
        
        CGRect aframe = self.txtLabel.frame;
        CGSize textSize = [item.extraData sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Light" size:16] thatFitsToSize:CGSizeMake(285, 2000) lineBreakMode:NSLineBreakByWordWrapping];
        aframe.size.height = textSize.height;
        aframe.origin.y = 70;
        aframe.size.width = 285;
        self.txtLabel.frame = aframe;
        
        self.txtBkg.frame = CGRectMake(10, 0, 300, textSize.height + 80);
        self.txtScrollView.contentSize = CGSizeMake(320, textSize.height + 100);
        
        [self.txtBkg.layer setBorderWidth:10.0];
        [self.txtBkg.layer setBorderColor:[UIColorFromRGB(0xffffff) CGColor]];
        [self.txtBkg.layer setShadowColor:[UIColorFromRGB(0x000000) CGColor]];
        
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathAddRect(path, NULL, CGRectMake(0, 0, self.txtBkg.frame.size.width, self.txtBkg.frame.size.height));
        self.txtBkg.layer.shadowPath = path;
        self.txtBkg.layer.shadowOpacity = 0.3;
        self.txtBkg.layer.shadowOffset = CGSizeMake(1.0, 1.0);
        self.txtBkg.layer.shadowRadius = 0.1;
        CFRelease(path);
        
    }
    
    NSInteger currentItemId = self.item.serverID;
    
    self.infoHidden = NO;
    
    
    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];
    
    if (itemKidIDs.count == 0) {
        return;
    }
    
    KPLocalKid *firstKid = itemKids[0];
    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:item.imageAssetID];
    
    BOOL isMyKid = firstKid.isMine;
    self.editButton.hidden = (!isMyKid);
    
    __weak FullScreenItemCell *wself = self;
    
    
    
    // DDLogInfo(@"Pretty printed size: %@", NSStringFromCGRect([self viewWithTag:300].frame));
    
    if (item.itemType == 4) {
        
        //[self viewWithTag:300].frame = CGRectMake(0, 137, 160, 44);
        //[self viewWithTag:400].frame = CGRectMake(160, 137, 160, 44);
        
        [self viewWithTag:1002].hidden = YES;
        
        CGRect lineOrigin = [self viewWithTag:1001].frame;
        
        [self viewWithTag:1001].frame = CGRectMake(160, lineOrigin.origin.y, lineOrigin.size.width, lineOrigin.size.height);
        
        [self viewWithTag:500].hidden = YES;
        
        
        self.playVideoButton.hidden = NO;
        
    }
    else {
        [self viewWithTag:500].hidden = NO;
        self.playVideoButton.hidden = YES;
    }
    
    //not quote
    if (item.itemType != 2) {
        self.itemImageView.image = nil;
        self.itemImageView.alpha = 0;
        [self.itemImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_iphone.jpg", Defaults.assetsURLString, itemImage.urlHash]] placeholderImage:nil options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            if (wself.isActive) {
                [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"loading")];
            } else {
                DDLogInfo(@"not active");
            }
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [SVProgressHUD dismiss];
            if (wself.isClosing) {
                return;
            }
            if (currentItemId != wself.item.serverID) {
                return;
            }
            [wself.imageActivityIndicator stopAnimating];
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        wself.itemImageView.alpha = 1.0;
        wself.itemImageView.frame = CGRectMake(0, 0, wself.frame.size.height, wself.frame.size.width);
        wself.backgroundView.alpha = 1.0;
        wself.itemImageView.contentMode = UIViewContentModeScaleAspectFit;
        if (wself.item.itemType == 4) {
            wself.itemImageView.contentMode = UIViewContentModeScaleAspectFill;
        }
    }                completion:^(BOOL finished) {
        if (wself.isActive) {
            [wself didBecomeActive];
        }
        
    }];
    
    
    if (itemKidIDs.count > 1) {
        self.kidImg.hidden = YES;
    } else {
        
        KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:firstKid.imageAssetID];
        
        if (kidImage == nil) {
            self.kidImg.image = [GlobalUtils getKidDefaultImage:firstKid.gender];
        } else {
            NSInteger currentItemId = self.item.serverID;
            
            [self.kidImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, kidImage.urlHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (currentItemId != wself.item.serverID) {
                    return;
                }
                
                wself.kidImg.image = [wself.kidImg.image
                                      imageMaskedWithElipse:CGSizeMake(290, 290)];
                
            }];
        }
    }
    
    if (itemKidIDs.count > 1) {
        self.kidNameLabel.frame = CGRectMake(68 - self.kidImg.frame.size.width, 15 + 20, 230 + self.kidImg.frame.size.width, 15);
        self.kidNameLabel.textColor = UIColorFromRGB(0x523C37);
        //  self.titleLabel.frame = CGRectMake(68 - self.kidImg.frame.size.width, 30 + 20, 220 + self.kidImg.frame.size.width, 15);
        // self.dateLabel.frame = CGRectMake(68 - self.kidImg.frame.size.width, 45 + 20, 230 + self.kidImg.frame.size.width, 15);
        
        // when multitagging is active, the photo is hidden, and we need to re-align/shift the title and the date somewhat to the left
        CGRect dateFrame = self.dateLabel.frame;
        CGRect titleFrame = self.titleLabel.frame;
        CGRect childrenNameFrame = self.kidNameLabel.frame;
        
        dateFrame.origin.x = childrenNameFrame.origin.x;
        titleFrame.origin.x = childrenNameFrame.origin.x;
        
        self.dateLabel.frame = dateFrame;
        self.titleLabel.frame = titleFrame;
        
        //UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showKids:)];
        //[self.kidNameLabel addGestureRecognizer:gesture];
        //self.kidNameLabel.userInteractionEnabled = YES;
    } else {
        self.kidNameLabel.frame = CGRectMake(68, 15 + 20, 230, 15);
        self.kidNameLabel.textColor = UIColorFromRGB(0x523C37);
        // self.titleLabel.frame = CGRectMake(68, 30 + 20, 220, 15);
        // self.dateLabel.frame = CGRectMake(68, 45 + 20, 230, 15);
    }
    
    
    //Set Texts
    if (itemKidIDs.count > 1) {
        self.kidNameLabel.text = [GlobalUtils getKidNamesv2:itemKids];
    } else {
        self.kidNameLabel.text = firstKid.name;
    }
    
    
    self.titleLabel.text = self.item.title;
    
    
    if (!self.item.title.length) {
        self.titleLabel.text = @""; //untitled
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    /*NSString *s = [NSDateFormatter dateFormatFromTemplate: @"ddMMyyyy"
     options: 0
     locale: [NSLocale currentLocale]];
     [formatter setDateFormat:s];
     */
    self.dateLabel.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.item.itemDate]];
    
    
    //Comments
    
    NSArray *itemComments = [KPLocalComment fetchCommentsForItem:self.item];
    KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];
    
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"isStory" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"commentDate" ascending:YES]];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObjectsFromArray:[itemComments sortedArrayUsingDescriptors:sortDescriptors]];
    
    if (itemStory) {
        // TODO: re-add story
        [arr insertObject:itemStory atIndex:0];
    }
    self.comments = arr;
    
    for (UIControl *control in self.commentsScroll.subviews) {
        [control removeFromSuperview];
    }
    
    float dh = 0;
    if ([self.comments count] > 0) {
        dh = 85;
    }// + 52;
    
    self.bottomView.frame = CGRectMake(0, self.frame.size.width - 44 - dh, self.frame.size.height, 44 + dh);
    self.commentsScroll.frame = CGRectMake(10, 10, self.frame.size.height - 20, 80);
    
    self.playCommentButton.alpha = 0;
    self.commentLoadingIndicator.alpha = 0;
    self.commentTimeCurrentLabel.alpha = 0;
    self.commentTimeEndLabel.alpha = 0;
    self.progressBkg.alpha = 0;
    self.progressFiller.alpha = 0;
    self.knobButton.alpha = 0;
    self.videoContainer.alpha = 0;
    
    float ax = 0;
    float ay = 0;
    int i = 0;
    for (KPLocalComment *comment in self.comments) {
        
        NSInteger clength = 0;
        UIColor *timeLabelColor = [UIColor whiteColor];
        UIImageView *pimg = [[UIImageView alloc] initWithFrame:CGRectMake(ax, ay, 70, 70)];
        pimg.backgroundColor = [UIColor blackColor];
        pimg.contentMode = UIViewContentModeScaleAspectFit;
        
        
        if ([comment isKindOfClass:[KPLocalStory class]]) {
            
            
            timeLabelColor = [UIColor whiteColor];
            
            clength = itemStory.storyLength;
            
            if (itemKidIDs.count > 1) {
                
            } else {
                KPLocalKid *kid = itemKids[0];
                KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];
                if (kidImage == nil) {
                    pimg.image = [GlobalUtils getKidDefaultImage:kid.gender];
                } else {
                    
                    [pimg setImage:[UIImage imageNamed:@"story-thumbnail"]];
                    [pimg setBackgroundColor:[UIColor clearColor]];
                    
                    /*
                     [pimg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], kid.image.urlHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                     
                     }];*/
                }
            }
            
            
            //panel
            UIImageView *panelImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 53, 70, 17)];
            panelImg.image = [UIImage imageNamed:@"VO-thumbnail-bar"];
            panelImg.alpha = 0.8;
            // [pimg addSubview:panelImg];
            
            if ([self.item fetchVideoStory] == nil) {
                //Seperator Line
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(77.5, 0, 1, 70)];
                lineView.backgroundColor = UIColorFromRGB(0xab9892);
                lineView.alpha = 0.8;
                [pimg addSubview:lineView];
                
                ax += 10;
            }
        }
        else {
            clength = comment.commentLength;
            //int currentItemId = self.item.itemId.intValue;
            
            if (comment.isStory) {
                if (itemKidIDs.count > 0) {
                    [pimg setImage:[UIImage imageNamed:@"story-thumbnail"]];
                    [pimg setBackgroundColor:[UIColor clearColor]];
                }
                else {
                    KPLocalKid *kid = itemKids[0];
                    KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];
                    
                    if (kidImage == nil) {
                        pimg.image = [GlobalUtils getKidDefaultImage:kid.gender];
                    } else {
                        
                        //DDLogInfo(@"%@",[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], kid.image.urlHash]);
                        [pimg setImage:[UIImage imageNamed:@"story-thumbnail"]];
                        [pimg setBackgroundColor:[UIColor clearColor]];
                        /*
                         [pimg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], kid.image.urlHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                         
                         }];*/
                    }
                }
                //Seperator Line
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(77.5, 0, 1, 70)];
                lineView.backgroundColor = UIColorFromRGB(0xab9892);
                lineView.alpha = 0.8;
                [pimg addSubview:lineView];
                
                ax += 10;
                
            }
            else {
                
                KPLocalAsset *commentPreviewImage = [KPLocalAsset fetchByLocalID:comment.previewImageAssetID];
                
                // DDLogInfo(@"%@",[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], comment.previewImage.urlHash]);
                [pimg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, commentPreviewImage.urlHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                }];
            }
            
            //panel
            UIImageView *panelImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 53, 70, 17)];
            if (comment.isStory) {
                panelImg.image = nil;//[UIImage imageNamed:@"Video-story-thumbnail-bar"];
            } else if (comment.commentType == 0) {
                panelImg.image = [UIImage imageNamed:@"video-thumbnail-bar"];
            } else if (comment.commentType == 1) {
                panelImg.image = [UIImage imageNamed:@"VO-comment-thumbnail-bar"];
            } else if (comment.commentType == 2) {
                panelImg.image = [UIImage imageNamed:@"text-comment-thumbnail-bar"];
            }
            panelImg.alpha = 0.8;
            [pimg addSubview:panelImg];
            
            //new ribbon
            UIImageView *ribbonImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34, 34)];
            ribbonImg.image = [UIImage imageNamed:@"new-video-ribbon"];
            ribbonImg.hidden = comment.wasRead;
            [pimg addSubview:ribbonImg];
            
            //new label
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(-5.5, -5.5, 34, 34)];
            lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
            lbl.textAlignment = NSTextAlignmentCenter;
            lbl.backgroundColor = [UIColor clearColor];
            lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:10];
            lbl.text = @"new";
            lbl.transform = CGAffineTransformMakeRotation(-M_PI / 4);
            lbl.hidden = comment.wasRead;
            //lbl.hidden = (newItemsCount == 0);
            [pimg addSubview:lbl];
            
        }
        
        
        //______________________
        
        //time label
        UILabel *timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(27, 70 - 17, 40, 17)];
        timeLbl.backgroundColor = [UIColor clearColor];
        timeLbl.textColor = timeLabelColor;
        timeLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:10];
        timeLbl.textAlignment = NSTextAlignmentRight;
        
        BOOL commentIsStory = [comment isKindOfClass:[KPLocalStory class]];
        if(!commentIsStory){
            if([comment isKindOfClass:[KPLocalComment class]]){
                commentIsStory = comment.isStory;
            }
        }
        
        if (commentIsStory) {
            int minutes = floor(clength / 60);
            int seconds = round(clength - minutes * 60);
            
            if (minutes < 0) {
                minutes = 0;
            }
            
            if (seconds < 0) {
                seconds = 0;
            }
            
            timeLbl.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
        }
        else {
            if (comment.creatorUserID == [[UserManager sharedInstance] getMe].userId.intValue) {
                timeLbl.text = [[UserManager sharedInstance] getMe].name;
            } else {
                timeLbl.text = comment.creatorName;
            }
        }
        [pimg addSubview:timeLbl];
        
        [self.commentsScroll addSubview:pimg];
        
        pimg.clipsToBounds = NO;
        pimg.tag = 100 + i;
        [pimg.layer setBorderWidth:0];
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentThumbTap:)];
        [pimg addGestureRecognizer:gesture];
        pimg.userInteractionEnabled = YES;
        
        ax += 76;
        i++;
    }
    
    //_______________________
    for (int i = 0; i < [self.bottomView.subviews count]; i++) {
        UIControl *control = (UIControl *) self.bottomView.subviews[i];
        //control.tag = indexPath.row;
        CGRect aframe = control.frame;
        
        
        if ([control tag] == 1001) {
            //  DDLogInfo(@"Pretty printed size: %@", NSStringFromCGRect(control.frame));
        }
        
        if ([control tag] == 1002) {
            //  DDLogInfo(@"Pretty printed size: %@", NSStringFromCGRect(control.frame));
            // [control setFrame: CGRectMake(278, 0, 1, 44)];
            //DDLogInfo(@"Pretty printed size: %@", NSStringFromCGRect(control.frame));
            
            
            control.hidden = (item.itemType == 4) ? YES : NO;
            [self.bottomView bringSubviewToFront:control];
        }
        
        //DDLogInfo(@"%d",item.itemType.intValue);
        
        //Middel line
        if (i == [self.bottomView.subviews count] - 2) {
            
            // DDLogInfo(@"Pretty printed size: %@", NSStringFromCGRect(control.frame));
            
            if (item.itemType == 4) {
                aframe = CGRectMake(160, aframe.origin.y, 1, 44);
            } else {
                aframe = CGRectMake(144, aframe.origin.y, 1, 44);
            }
            
        }
        //comment *& share - hor sep line
        if (i == [self.bottomView.subviews count] - 3) {
            
            // DDLogInfo(@"Pretty printed size: %@", NSStringFromCGRect(aframe));
            // aframe = CGRectMake(0, 85, 1, 44);
            /*
             if (item.itemType.intValue == 4)
             aframe = CGRectMake(365, aframe.origin.y, 1, 44);
             else{
             aframe = CGRectMake(288, aframe.origin.y, 1, 44);
             }*/
            
            
        }
        
        //comment
        if (i == 9) {
            if (item.itemType == 4) {
                aframe = CGRectMake(0, aframe.origin.y, 150, 44);
            } else {
                aframe = CGRectMake(0, aframe.origin.y, 138, 44);
            }
        }
        //Share
        if (i == 10) {
            if (item.itemType == 4) {
                aframe = CGRectMake(160, aframe.origin.y, 150, 44);
            } else {
                aframe = CGRectMake(138, aframe.origin.y, 138, 44);
            }
        }
        
        //present box
        if (i == 11) {
            /*
             if (item.itemType.intValue == 4)
             aframe = CGRectMake(160, aframe.origin.y, 150, 44);
             else
             aframe = CGRectMake(256, aframe.origin.y, control.frame.size.width, control.frame.size.height);*/
        }
        
        control.frame = aframe;
    }
    
    
    self.commentsScroll.contentSize = CGSizeMake(20 + [self.comments count] * 76, 80);
    
}

- (void)showKids:(id)sender {
    
    
    NSArray *kidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *kids = [KPLocalKid idsToObjects:kidIDs];
    
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_KIDS_MENU_NOTIFICATION object:kids];
    }];
}

- (void)setIsActive:(BOOL)isActive {
    if (isActive == _isActive) {
        return;
    }
    
    _isActive = isActive;
    
    // self.currentCommentIndex = -1;
    
    if (self.isActive) {
        
        self.audioTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(audioTimerProc:) userInfo:nil repeats:YES];
        
        //if (self.backgroundView != nil)
        //    [self didBecomeActive];
        
        //mark item as read
        if (self.item.readAction == 0) {
            self.item.readAction = 1;
            [self.item saveChanges];
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            
            dispatch_async(dispatch_queue_create(nil, nil), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];
            });
            
            
            [[ServerComm instance] markItemRead:self.item.serverID andReadAction:1 withSuccessBlock:^(id result) {
            }                      andFailBlock:^(NSError *error) {
            }];
            
        }
    } else {
        [self clearPreviousContentArtifacts];
    }
}

- (void)clearPreviousContentArtifacts {
    [self.audioTimer invalidate];
    self.audioTimer = nil;
    
    [self.player stop];
    [self.moviePlayer stop];
    
    
    [SVProgressHUD dismiss];
    
    self.infoHidden = NO;
    
    self.player = nil;
    
    [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
    self.playCommentButton.tag = 0;
    
    UIScrollView *scrollView = (UIScrollView *) self.itemImageView.superview;
    if (scrollView.zoomScale == 1.0) {
        [UIView animateWithDuration:0.5 animations:^{
            self.itemImageView.frame = CGRectMake(10, 0, self.frame.size.height - 20, self.frame.size.width);
        }];
    }
    
    if (self.txtView != nil) {
        [self.txtView removeFromSuperview];
        self.txtView = nil;
    }
    
    
}

- (void)setCommentIndex:(NSInteger)commentIndex {
    _commentIndex = commentIndex;
}

- (void)didBecomeActive {
    
    [UIView animateWithDuration:0.5 animations:^{
        if (self.item.itemType != 2) {
            self.itemImageView.frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.width);
        }
        
    }];
    self.playVideoButton.center = self.itemImageView.center;
    if (self.commentIndex > -1) {
        NSInteger i = 0;
        if(self.item.storyID > 0){
            i = self.commentIndex + 1;
        }
        else{
            i = self.commentIndex;
        }
        [[Mixpanel sharedInstance] track:@"Watch Comment" properties:@{@"Source" : @"Feed", @"Number of Video" : @(self.commentIndex)}];
        
        
        [self.commentsScroll scrollRectToVisible:CGRectMake(76 * i, 0, 70, 70) animated:YES];
        
        if(self.comments && self.currentCommentIndex < self.comments.count && self.currentCommentIndex >= 0 && i >= 0){
            
            // we don't wanna show the comment otherwise
            NSLog(@"current: %ld", (long)self.currentCommentIndex);
            [self showComment:i];
            
        }
        
        self.commentIndex = -1;
    }
    else{
        if([self.comments count] > 0 && (self.comments.count > self.currentCommentIndex) /*&& self.currentCommentIndex > -1*/){
            if ([self.comments[self.currentCommentIndex] isKindOfClass:[KPLocalStory class]]) {
                
                //            KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];
                KPLocalStory* story = self.comments[self.currentCommentIndex];
                if (![GlobalUtils wasStoryPlayed:story.serverID]) {
                    [self showComment:0];
                    // }
                }
            }
            
            else{
                KPLocalComment* commentStory = self.comments[self.currentCommentIndex];
                if (commentStory.commentType == 1) //voice
                {
                    KPLocalStory* story = self.comments[self.currentCommentIndex];
                    if (![GlobalUtils wasStoryPlayed:story.serverID]) {
                       
                    }
//                    KPLocalAsset *commentVideo = [KPLocalAsset fetchByLocalID:commentStory.videoAssetID];
//                    [self playAudio:(id)commentVideo];
                }
                if(commentStory.isStory){
                    [self showComment:0];
                }
            }
        }
        
    }
    
}

#pragma mark - Actions

- (void)swipe:(id)gesture {
    if (self.txtView != nil) {
        return;
    }
    
    [self closeTap:nil];
    
}

- (void)closeTap:(id)sender {
    self.infoHidden = NO;
    self.isClosing = YES;
    [self.player stop];
    [self.moviePlayer stop];
    
    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"fullscreen", @"result" : @"cancel"}];
    
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.navController dismissViewControllerAnimated:YES completion:nil];
    /*
     //[d.window setRootViewController:d.navController];
     
     [UIView transitionWithView:self.window
     duration:0.5
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
     [[UIApplication sharedApplication] setStatusBarHidden:NO];
     self.window.rootViewController = d.navController;
     }
     completion:nil];*/
    /*
     [UIView animateWithDuration:0.5 animations:^{
     //self.itemImageView.frame = CGRectMake(10, self.initialY, self.view.frame.size.width, self.view.frame.size.height);
     self.topView.alpha = 0;
     self.backgroundView.alpha = 0;
     //self.view.alpha = 0.2;
     } completion:^(BOOL finished) {
     
     AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
     [d.window setRootViewController:d.navController];
     }];
     */
}


- (void)editTap:(id)sender {
    self.isActive = NO;
#if 1
    UIStoryboard* sb = [UIStoryboard storyboardWithName:@"AssetCollection" bundle:nil];
    UINavigationController* nc = [sb instantiateViewControllerWithIdentifier:@"EditStory"];
    AddStoryViewController2* vc = nc.viewControllers.firstObject;
    vc.localItem = self.item;
    
    // WTF...
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.viewController.presentedViewController presentViewController:nc animated:YES completion:NULL];
#else
    
    id avc = nil;
    //if (self.item.itemType.intValue == 0 || self.item.itemType.intValue == 2) //photo
    //{
    avc = [[ItemEditViewController alloc] initWithItem:(id)self.item andMode:1];
    [(ItemEditViewController *) avc setDelegate:self];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    navController.navigationBarHidden = YES;
    
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.viewController.presentedViewController presentViewController:navController animated:YES completion:nil];
    
    /*}
     else if (self.item.itemType.intValue == 1) //cover-note
     {
     avc = [[NoteEditorViewController alloc] initWithItem:self.item];
     [(NoteEditorViewController*)avc setDelegate:self];
     
     UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
     navController.view.tag = 1;
     AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
     [d.viewController.presentedViewController presentViewController:navController animated:YES completion:nil];
     }*/
    
#endif
}

- (void)shareTap:(id)sender {
    if (!self.isActive) {
        return;
    }
    
    if (self.txtView != nil) {
        [self.txtView removeFromSuperview];
        self.txtView = nil;
        
        return;
    }
    
    
    [GlobalUtils shareItem:self.item withSource:@"Full Screen" sender:sender];
}

- (void)buyTap:(UIButton *)sender {
    
    NSArray *kidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *kids = [KPLocalKid idsToObjects:kidIDs];
    
    if (((KPLocalKid *) kids[0]).serverID == 88) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"can\'t print & give on this feed")];
        return;
    }
    
    
    [[Mixpanel sharedInstance] track:@"Gift Tap"];
    
    
    if (!self.isActive) {
        return;
    }
    
    if (self.txtView != nil) {
        [self.txtView removeFromSuperview];
        self.txtView = nil;
        
        return;
    }
    
    
    [GlobalUtils buyItem:(id)self.item withSource:@"Full Screen"];
}


- (void)commentTap:(id)sender {
    if (!self.isActive) {
        return;
    }
    
    if (self.txtView != nil) {
        [self.txtView removeFromSuperview];
        self.txtView = nil;
    }
    
    NSArray *kidIDs = [KPLocalKidItem fetchKidIDsForItem:self.item];
    NSArray *kids = [KPLocalKid idsToObjects:kidIDs];
    
    if (((KPLocalKid *) kids[0]).serverID == 88) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"can\'t comment on this feed")];
        return;
    }
    
    self.isActive = NO;
    
    UIActionSheet *asheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:KPLocalizedString(@"cancel") destructiveButtonTitle:nil otherButtonTitles:KPLocalizedString(@"video"), KPLocalizedString(@"voice"), KPLocalizedString(@"text"), nil];
    [asheet showInView:[GlobalUtils topMostController].view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 3) {
        return;
    }
    
    RecordViewController *avc = [[RecordViewController alloc] initWithItem:(id)self.item withMode:(buttonIndex < 2) ? (NSInteger)(11 - buttonIndex) : 12];
    avc.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    navController.view.tag = 1;
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [d.viewController.presentedViewController presentViewController:navController animated:YES completion:nil];
    
}


- (void)playButtonTap:(id)sender {
    
    // DDLogInfo(@"%d",self.currentCommentIndex);
    //zvika 160829 to avoid crash  FullScreenItemCell.m line 1469
     if (self.comments.count > self.currentCommentIndex) {
    
    self.isActive = YES;
    if (self.playCommentButton.tag == 0) {
        
        // Commented below line by Ashvini
        [GlobalUtils outputToSpeaker];
        
        NSInteger currentCommentIndex = self.currentCommentIndex; // it HAS to be signed, otherwise, a comparison between 0 > -1 will return false (wtf?!)
        NSInteger storyID = self.item.storyID;
        
        if (storyID == 0 || currentCommentIndex > 0) {
            
            // if self.comments.count > self.currentCommentIndex {
            KPLocalComment *comment = self.comments[self.currentCommentIndex];
            
            
            
            //DDLogInfo(@"%@",comment.commentId);
            
            if (comment.commentType == 1) //voice
            {
                KPLocalAsset *commentVideo = [KPLocalAsset fetchByLocalID:comment.videoAssetID];
                [self.playVideoButton setImage:[UIImage imageNamed:@"play-video-big"] forState:UIControlStateNormal];
                [self playAudio:(id)commentVideo];
            }
            else {
                if (_lastCommentPlay != _currentCommentIndex && self.moviePlayer == nil) {
                    _lastCommentPlay = _currentCommentIndex;
                    
                }
                
                
                if (self.moviePlayer != nil) {
                    
                    BOOL ModeChange = _lastCommentPlay == _currentCommentIndex ? NO : YES;
                    
                    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
                        [self.moviePlayer pause];
                        [self.videoContainer setHidden:YES];
                        _pauseBG.hidden = YES;
                        _playBG.hidden = NO;
                        
                        if (ModeChange) {
                            _lastCommentPlay = _currentCommentIndex;
                            self.moviePlayer = nil;
                            [self playButtonTap:nil];
                        }
                        
                        return;
                    }
                    
                    if (self.moviePlayer.playbackState == MPMoviePlaybackStateStopped) {
                        //DDLogInfo(@"MPMoviePlaybackStateStopped");
                        
                        if (self.player.isPlaying) {
                            [self.player stop];
                            [self.videoContainer setHidden:YES];
                            
                        }
                        
                        if (ModeChange) {
                            _lastCommentPlay = _currentCommentIndex;
                            self.moviePlayer = nil;
                            [self playButtonTap:nil];
                            return;
                        }
                        
                        KPLocalAsset *commentVideo = [KPLocalAsset fetchByLocalID:comment.videoAssetID];
                        
                        NSURL *aurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.mov", Defaults.assetsURLString, commentVideo.urlHash]];
                        [self prepareMovie:aurl];
                        
                        //[self.moviePlayer play];
                        _pauseBG.hidden = NO;
                        _playBG.hidden = YES;
                        return;
                    }
                    
                    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
                        // DDLogInfo(@"MPMoviePlaybackStatePaused");
                        if (self.player.isPlaying) {
                            [self.player pause];
                            [self.videoContainer setHidden:YES];
                        }
                        
                        if (ModeChange) {
                            _lastCommentPlay = _currentCommentIndex;
                            self.moviePlayer = nil;
                            [self playButtonTap:nil];
                            return;
                        }
                        
                        
                        [self.moviePlayer play];
                        [self.videoContainer setHidden:NO];
                        _pauseBG.hidden = NO;
                        _playBG.hidden = YES;
                        //self.infoHidden = YES;
                        
                        
                        return;
                    }
                    // [self.moviePlayer pause];
                    return;
                }
                
                
                //int currentItemId = self.item.itemId.intValue;
                //self.playCommentButton.hidden = YES;
                
                //dispatch_async(dispatch_get_main_queue(), ^{
                if (self.isClosing || !self.isActive) {
                    return;
                }
                
                [self.playVideoButton setImage:[UIImage imageNamed:@"play-video-big"] forState:UIControlStateNormal];
                
                // DDLogInfo(@"%@",[NSString stringWithFormat:@"%@%@.mov", [[KPDefaults sharedDefaults] assetsURLString], comment.video.urlHash]);
                KPLocalAsset *commentVideo = [KPLocalAsset fetchByLocalID:comment.videoAssetID];
                
                NSURL *aurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.mov", Defaults.assetsURLString, commentVideo.urlHash]];
                [self prepareMovie:aurl];
                
                [self.playCommentButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];
                self.playCommentButton.tag = 1;
                
                
                [UIView animateWithDuration:0.5 animations:^{
                    self.knobButton.alpha = 1;
                    self.progressFiller.alpha = 1;
                    self.progressBkg.alpha = 1;
                    self.commentTimeEndLabel.alpha = 1;
                    self.playCommentButton.alpha = 1;
                    self.commentTimeCurrentLabel.frame = CGRectMake(3, (52 - 20) / 2, 40, 20);
                    self.commentTimeCurrentLabel.textAlignment = NSTextAlignmentCenter;
                    self.commentTimeCurrentLabel.text = @"0:00";
                    
                }                completion:^(BOOL finished) {
                    if (_item.itemType == 4) {
                        if ([self viewWithTag:10210]) {
                            [[self viewWithTag:10210] removeFromSuperview];
                            self.playVideoButton.hidden = NO;
                            [self bringSubviewToFront:self.playVideoButton];
                        }
                    }
                }];
            }
            
        } else {
            
            KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];
            KPLocalAsset *itemStoryAsset = [KPLocalAsset fetchByLocalID:itemStory.assetID];
            [self playAudio:(id)itemStoryAsset];
        }
    }
    else {
        self.infoHidden = NO;
        
        
        if (self.item.storyID > 0 || self.currentCommentIndex > 0) {
            KPLocalComment *comment = self.comments[self.currentCommentIndex];
            
            if (comment.commentType == 1) { //voice
                [self.player pause];
            } else {
                [self.moviePlayer pause];
                [self.videoContainer setHidden:YES];
                
            }
        }
        else {
            [self.player pause];
        }
        
        
        [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
        self.playCommentButton.tag = 0;
    }
    
    }
}

- (void)stopPlayVoice:(id)sender {
    if (self.player != nil && self.player.playing) {
        [self.player stop];
        _pauseBG.hidden = YES;
        _playBG.hidden = NO;
        self.player = nil;
    }
}

- (void)playAudio:(Asset *)asset {
//     DDLogInfo(@"%@",asset.assetId);
    
    //self.player = nil;
    
    if (self.moviePlayer != nil) {
        [self.moviePlayer stop];
        //self.moviePlayer = nil;
    }
    
    if (self.playView.playerStatePlaying) {
        [self.playView tapPause:nil];
    }
    
    if (self.player == nil) {
        _lastCommentPlay = _currentCommentIndex;
    }
    
    if (self.currentCommentIndex == 0) {
        //self.player = nil;
    }
    if (self.player != nil) {
        BOOL ModeChange = _lastCommentPlay == _currentCommentIndex ? NO : YES;
        
        if (self.item.itemType != 4) {
            //self.infoHidden = YES;
        }
        
        if (self.player.playing) {
            [self.player pause];
            [self.commentLoadingIndicator stopAnimating];
            _pauseBG.hidden = YES;
            _playBG.hidden = NO;
            if (ModeChange) {
                
                
                [self.player stop];
                self.player = nil;
                
                
                if([self.comments[self.currentCommentIndex] isKindOfClass:[KPLocalStory class]]){
                    KPLocalStory *story = self.comments[self.currentCommentIndex];
                    KPLocalAsset *itemStoryAsset = [KPLocalAsset fetchByLocalID:story.assetID];
                    [self playAudio:(id)itemStoryAsset];
                }
                else{
                    KPLocalComment *comment = self.comments[self.currentCommentIndex];
                    if (comment.commentType == 1) //voice
                    {
                        KPLocalAsset *commentVideo = [KPLocalAsset fetchByLocalID:comment.videoAssetID];
                        //self.player = nil;
                        [self playAudio:(id)commentVideo];
                    }
                }
            }
            
        } else {
            
            if (ModeChange) {
                [self.player stop];
                self.player = nil;
                if([self.comments[self.currentCommentIndex] isKindOfClass:[KPLocalStory class]]){
                    KPLocalStory *story = self.comments[self.currentCommentIndex];
                    KPLocalAsset *itemStoryAsset = [KPLocalAsset fetchByLocalID:story.assetID];
                    [self playAudio:(id)itemStoryAsset];
                }
                else{
                    KPLocalComment *comment = self.comments[self.currentCommentIndex];
                    if (comment.commentType == 1) //voice
                    {
                        KPLocalAsset *commentVideo = [KPLocalAsset fetchByLocalID:comment.videoAssetID];
                        //self.player = nil;
                        [self playAudio:(id)commentVideo];
                    }
                }
            }
            else {
                _lastCommentPlay = self.currentCommentIndex;
                [self.commentLoadingIndicator stopAnimating];
                [self.player play];
                _pauseBG.hidden = NO;
                _playBG.hidden = YES;
                //[self.commentLoadingIndicator startAnimating];
                
            }
            
        }
        
        self.playCommentButton.tag = 1;
        [self.playCommentButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];
        return;
    }
    
    
    _pauseBG.hidden = NO;
    _playBG.hidden = YES;
    self.playCommentButton.hidden = YES;
    NSInteger currentItemId = self.item.serverID;
    
    [[ServerComm instance] downloadAssetFile:asset withSuccessBlock:^(id result) {
        
        NSLog(@"currentID: %ld self.item.serverID: %lu", (long)currentItemId, (unsigned long)self.item.serverID);
        if (currentItemId != self.item.serverID) {
            return;
        }
        
        if (self.isClosing || !self.isActive) {
            return;
        }
        
        NSError *error;
        
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:result] error:&error];
        self.player.delegate = self;
        [self.commentLoadingIndicator stopAnimating];
        [self.player play];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.item.itemType != 4) {
                //self.infoHidden = YES;
            }
            [self.commentLoadingIndicator stopAnimating];
            self.playCommentButton.hidden = NO;
            
            [self.playCommentButton setImage:[UIImage imageNamed:@"pauseVoiceoverButton"] forState:UIControlStateNormal];
            self.playCommentButton.tag = 1;
            
            [UIView animateWithDuration:0.5 animations:^{
                self.knobButton.alpha = 1;
                self.progressFiller.alpha = 1;
                self.progressBkg.alpha = 1;
                self.commentTimeEndLabel.alpha = 1;
                self.playCommentButton.alpha = 1;
                self.commentTimeCurrentLabel.frame = CGRectMake(3, (52 - 20) / 2, 40, 20);
                self.commentTimeCurrentLabel.textAlignment = NSTextAlignmentCenter;
                self.commentTimeCurrentLabel.text = @"0:00";
                
            }                completion:^(BOOL finished) {
                if (_item.itemType == 4) {
                    if ([self viewWithTag:10210]) {
                        [[self viewWithTag:10210] removeFromSuperview];
                        self.playVideoButton.hidden = NO;
                        [self bringSubviewToFront:self.playVideoButton];
                    }
                    
                    //                        [self.itemImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_iphone.jpg", [[KPDefaults sharedDefaults] assetsURLString], self.item.image.urlHash]] placeholderImage:nil];
                }
            }];
        });
        
        
    }                           andFailBlock:^(NSError *error) {
        NSLog(@"failed fetching comment");
        
    }];
}

- (void)imageTap:(UITapGestureRecognizer *)gesture {
    if (self.txtView != nil) {
        [self.txtView removeFromSuperview];
        self.txtView = nil;
        
        return;
    }
    
    self.infoHidden = !self.infoHidden;
}

- (void)imageDoubleTap:(UITapGestureRecognizer *)gesture {
    if (self.item.itemType == 4) {
        return;
    }
    
    if (self.txtView != nil) {
        return;
    }
    
    UIScrollView *scrollView = (UIScrollView *) gesture.view;
    if (scrollView.zoomScale > scrollView.minimumZoomScale) {
        [scrollView setZoomScale:scrollView.minimumZoomScale animated:YES];
    } else {
        [scrollView setZoomScale:scrollView.maximumZoomScale animated:YES];
    }
}

- (void)setInfoHidden:(BOOL)infoHidden {
    if (_infoHidden == infoHidden) {
        return;
    }
    
    if (_infoHidden == NO) {
        self.topView.hidden = YES;
        self.bottomView.hidden = YES;
        //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    } else {
        self.topView.hidden = NO;
        self.bottomView.hidden = NO;
        //[[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
    
    _infoHidden = infoHidden;
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    self.infoHidden = NO;
    [self.playCommentButton setImage:[UIImage imageNamed:@"playVoiceoverButton"] forState:UIControlStateNormal];
    self.playCommentButton.tag = 0;
    _pauseBG.hidden = YES;
    _playBG.hidden = YES;
    [self.commentLoadingIndicator stopAnimating];
    
}


#pragma mark - gesture delegate

// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}

// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.itemImageView;
}

#pragma mark - AudioTimer

- (void)audioTimerProc:(id)sender {
    int minutes = 0;
    int seconds = 0;
    NSTimeInterval duration = 1;
    NSTimeInterval currentTime = 0;
    
    if (self.item.storyID == 0 || self.currentCommentIndex > 0) {
        if (self.comments == nil) {
            return;
        }
        
        if ([self.comments count] == 0) {
            return;
        }
        
        //        if (self.currentCommentIndex == -1 || self.currentCommentIndex == 0) {
        //            return;
        //        }
        
        
        if (!self.comments || self.currentCommentIndex >= self.comments.count) {
            return;
            self.isActive = !self.isActive;
            self.isActive = !self.isActive;
        }
        
        KPLocalComment *comment = self.comments[self.currentCommentIndex];
        if (comment.commentType == 1) //voice
        {
            minutes = floor(self.player.currentTime / 60);
            seconds = round(self.player.currentTime - minutes * 60);
            duration = self.player.duration;
            currentTime = self.player.currentTime;
            
            if(self.player.currentTime > 0 && self.player.duration > 0){
                if(self.player.currentTime + 0.5 > self.player.duration){
                    [self hideProgressBarTimer];
                }
            }
        }
        else {
            minutes = floor(self.moviePlayer.currentPlaybackTime / 60);
            seconds = round(self.moviePlayer.currentPlaybackTime - minutes * 60);
            duration = self.moviePlayer.duration;
            
            if (self.moviePlayer.loadState == MPMovieLoadStateUnknown) {
                currentTime = 0;
            }
            else {
                currentTime = self.moviePlayer.currentPlaybackTime;
            }
            
            if(self.moviePlayer.duration > 0 && self.moviePlayer.currentPlaybackTime > 0){
                if(self.moviePlayer.currentPlaybackTime + 0.5 > self.moviePlayer.duration){
                    NSLog(@"finish");
                    [self hideProgressBarTimer];
                }
            }
            
        }
    }
    else {
        minutes = floor(self.player.currentTime / 60);
        seconds = round(self.player.currentTime - minutes * 60);
        duration = self.player.duration;
        currentTime = self.player.currentTime;
        
        if(self.player.currentTime > 0 && self.player.duration > 0){
            if(self.player.currentTime + 0.5 > self.player.duration){
                [self hideProgressBarTimer];
            }
        }
    }
    
    //if (self.player == nil)
    //    return;
    
    if (minutes < 0) {
        minutes = 0;
    }
    
    if (seconds < 0) {
        seconds = 0;
    }
    
    if (duration == 0) {
        duration = 1;
    }
    
    if (currentTime == NAN) {
        currentTime = 0;
    }
    
    self.commentTimeCurrentLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    
    minutes = floor(duration / 60);
    seconds = round(duration - minutes * 60);
    self.commentTimeEndLabel.text = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    
    CGRect aframe = self.progressFiller.frame;
    aframe.size.width = self.progressBkg.frame.size.width * (currentTime / duration);
    self.progressFiller.frame = aframe;
    
    
    float ax = self.progressFiller.frame.origin.x + self.progressFiller.frame.size.width - 10;
    if (ax < 40) {
        ax = 40;
    }
    
    aframe = self.knobButton.frame;
    aframe.origin.x = ax;
    self.knobButton.frame = aframe;
}

- (void) hideProgressBarTimer{
    
    NSInteger commentIndex = self.currentCommentIndex;
    [UIView animateWithDuration:0.5 animations:^{
        self.videoContainer.alpha = 0;
        float dh = 85;
        self.bottomView.frame = CGRectMake(0, self.frame.size.width - 44 - dh, self.frame.size.height, 44 + dh);
        self.commentsScroll.frame = CGRectMake(10, 10, self.frame.size.height - 20, 80);
        self.playCommentButton.alpha = 0;
        self.commentLoadingIndicator.alpha = 0;
        self.commentTimeCurrentLabel.alpha = 0;
        self.commentTimeEndLabel.alpha = 0;
        self.progressBkg.alpha = 0;
        self.progressFiller.alpha = 0;
        self.knobButton.alpha = 0;
    }                completion:^(BOOL finished) {
        if (commentIndex == self.currentCommentIndex) {
            
            if (self.player == nil || !self.player.playing) {
                _pauseBG.hidden = YES;
                _playBG.hidden = YES;
            }
            
            //self.currentCommentIndex = -1;
        }
    }];
    
    self.infoHidden = NO;
}


- (void)prepareMovie:(NSURL *)fileURL {
    
    
    
    if (self.player.playing) {
        [self.player stop];
    }
    
    _pauseBG.hidden = NO;
    _playBG.hidden = YES;
    //fileURL = [NSURL URLWithString:@"http://keepy.me/xdl2/test.mov"];
    if (self.moviePlayer) {
        [self.moviePlayer setContentURL:fileURL];
    } else {
        
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
        self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    }
    
    self.moviePlayer.view.clipsToBounds = NO;
    
    // Register for the playback finished notification.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieLoadStateChangedCallback:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification object:self.moviePlayer];
    
    // Movie playback is asynchronous, so this method returns immediately.
    self.moviePlayer.view.frame = CGRectMake(0, 0, self.videoContainer.frame.size.width, self.videoContainer.frame.size.height);
    self.moviePlayer.shouldAutoplay = YES;
    [self.videoContainer addSubview:self.moviePlayer.view];
    
    self.moviePlayer.initialPlaybackTime = -1.0;
    
    // this line causes an asynchronous crash on the simulator, but not on the actual device
    [self.moviePlayer prepareToPlay];
    [self.videoContainer setHidden:NO];
    
    [self.moviePlayer play];
}

// When the movie is done,release the controller.
- (void)myMovieFinishedCallback:(NSNotification *)aNotification {
    
    // self.moviePlayer = nil;
    
    if (self.player == nil || !self.player.playing) {
        _pauseBG.hidden = YES;
        _playBG.hidden = YES;
    }
    
    return;
}

- (void)myMovieLoadStateChangedCallback:(NSNotification *)aNotification {
    
    if (self.moviePlayer.loadState & MPMovieLoadStateStalled) {
        [self.commentLoadingIndicator startAnimating];
        
    } else if (self.moviePlayer.loadState & MPMovieLoadStatePlayable) {
        self.infoHidden = YES;
        [self.commentLoadingIndicator stopAnimating];
        
    }
    
}

- (void)commentThumbTap:(UITapGestureRecognizer *)gesture {
    
    if (self.item.storyID > 0 || gesture.view.tag > 100) {
        [[Mixpanel sharedInstance] track:@"Watch Comment" properties:@{@"Source" : @"Full Screen", @"Number of Video" : @(self.currentCommentIndex)}];
    } else {
        [[Mixpanel sharedInstance] track:@"Play Story VO"];
    }
    
    
    [[Analytics sharedAnalytics] track:@"btn-tap" properties:@{@"btn" : @"comment-play"}];
    
    
    if (self.txtView != nil) {
        [self.txtView removeFromSuperview];
        self.txtView = nil;
    }
    NSLog(@"view tag: %ld", (long)gesture.view.tag - 100);
    [self showComment:gesture.view.tag - 100];
}

- (void)showComment:(NSInteger)aindex {
    @try {
        self.playCommentButton.tag = 0;
    
        UIImageView *pimg = (UIImageView *) [self.commentsScroll viewWithTag:100 + self.currentCommentIndex];
        [pimg.layer setBorderColor:[[UIColor clearColor] CGColor]];
        [pimg.layer setBorderWidth:0];
        [_pauseBG removeFromSuperview];
        [_playBG removeFromSuperview];
    
        self.currentCommentIndex = aindex;
    
        BOOL isVideoComment = NO;
        BOOL isTextComment = NO;
    
        KPLocalStory *itemStory = [KPLocalStory fetchByLocalID:self.item.storyID];
    
        if (aindex == 0 && itemStory != nil) {
            [GlobalUtils storyPlayed:itemStory.serverID];
        } else {
            
            if([self.comments count] > 0 && (self.comments.count > self.currentCommentIndex)){
                
                KPLocalComment *comment = (KPLocalComment *) self.comments[self.currentCommentIndex];
                isVideoComment = (comment.commentType == 0);
                
                if (comment.commentType == 2) {
                    isTextComment = YES;
                    
                    _pauseBG.hidden = YES;
                    _playBG.hidden = YES;
                    
                    if([self.player isPlaying]){
                        [self.player stop];
                    }
                    
                    [UIView animateWithDuration:0.5 animations:^{
                        float dh = 85;
                        self.bottomView.frame = CGRectMake(0, self.frame.size.width - 44 - dh, self.frame.size.height, 44 + dh);
                        self.commentsScroll.frame = CGRectMake(10, 10, self.frame.size.height - 20, 80);
                        self.playCommentButton.alpha = 0;
                        self.commentLoadingIndicator.alpha = 0;
                        self.commentTimeCurrentLabel.alpha = 0;
                        self.commentTimeCurrentLabel.text = @"";
                        self.commentTimeEndLabel.alpha = 0;
                        self.progressBkg.alpha = 0;
                        self.progressFiller.alpha = 0;
                        self.knobButton.alpha = 0;
                        self.videoContainer.alpha = 0;
                        
                    }                completion:^(BOOL finished) {
                        [self showTextComment:comment];
                    }];
                    
                }
            }
            
        }
    
        pimg = (UIImageView *) [self.commentsScroll viewWithTag:100 + self.currentCommentIndex];
        [_pauseBG setFrame:pimg.frame];
        [_playBG setFrame:pimg.frame];
        [self.commentsScroll addSubview:_pauseBG];
        [self.commentsScroll addSubview:_playBG];
    
        if ([pimg.subviews count] > 3) {
            [(UIControl *) pimg.subviews[1] setHidden:YES];
            [(UIControl *) pimg.subviews[2] setHidden:YES];
        }
    
        if (!isTextComment) {
            float dh = 85 + 52;
            
            [self.commentLoadingIndicator startAnimating];
            
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame = CGRectMake(0, self.frame.size.width - 44 - dh, self.frame.size.height, 44 + dh);
                self.commentsScroll.frame = CGRectMake(10, 53, self.frame.size.height - 20, 80);
                //self.playCommentButton.alpha = 1;
                self.commentLoadingIndicator.alpha = 1;
                self.commentTimeCurrentLabel.alpha = 1;
                self.commentTimeCurrentLabel.text = @"loading";
                self.commentTimeEndLabel.alpha = 1;
                self.progressBkg.alpha = 1;
                self.progressFiller.alpha = 1;
                self.knobButton.alpha = 1;
                if (isVideoComment) {
                    self.videoContainer.alpha = 1;
                } else {
                    self.videoContainer.alpha = 0;
                }
                
            }                completion:^(BOOL finished) {
                
            }];
            
            
            self.playCommentButton.tag = 0;
            // TODO: re-enable automatic video playback
            [self playButtonTap:self.playCommentButton];
            
            
        }
    
        // if it is not the first comment, or it is the first one but there is no story
        if (aindex > 0 || self.item.storyID == 0) {
            KPLocalComment *comment = (KPLocalComment *) self.comments[self.currentCommentIndex];
            //if (!comment.wasRead) {
            comment.wasRead = YES;
            [comment saveChanges];
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [[ServerComm instance] markCommentRead:self.item.serverID andCommentId:comment.serverID andReadAction:1 withSuccessBlock:nil andFailBlock:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:self.item];
            //}
        }
    } @catch(id exeption) {
        
    }
}

#pragma mark - RecordingViewDelegate

- (void)recordingSuccessful:(NSData *)fileData withLength:(NSInteger)recordingLength andMode:(NSInteger)mode andPreviewImage:(UIImage *)previewImage {
    self.isActive = YES;
    NSArray *itemComments = [KPLocalComment fetchCommentsForItem:self.item];
    
    NSInteger c = 0;
    if (itemComments != nil) {
        c = itemComments.count;
    }
    
    self.item = self.item;  //refresh
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:self.item];
    
    
    [[Mixpanel sharedInstance] track:@"Add Comment" properties:@{@"Source" : @"Full Screen", @"Number of Video" : @(c)}];
    
    
}

- (void)recordingCancelled {
    self.isActive = YES;
}

- (KPLocalComment*)videoStoryToDelete {
    NSAssert(NO, @"videoStoryToDelete is not implemented.");
    return nil;
}

#pragma mark - ItemEditDelegate

- (void)itemEditFinished {
    self.isActive = YES;
    
    self.item = self.item;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:self.item];
}

- (void)itemEditCancelled {
    self.isActive = YES;
}

#pragma mark - NoteEditDelegate

- (void)noteEditorFinished:(NSString *)noteText withNoteImage:(UIImage *)noteImage andAssetId:(NSInteger)assetId {
    self.isActive = YES;
    
    self.item = self.item;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:self.item];
    
    //AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //[d.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - text comment display

- (void)showTextComment:(KPLocalComment *)comment {
    if (self.txtView != nil) {
        [self.txtView removeFromSuperview];
        self.txtView = nil;
    }
    
    //AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.txtView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.txtView.userInteractionEnabled = YES;
    self.txtView.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.txtView];
    
    
    /*
     UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
     btn.frame = CGRectMake(0, 0, self.frame.size.width, 1000);
     btn.backgroundColor = [UIColor blackColor];
     btn.alpha = 0.5;
     [btn addTarget:self action:@selector(textBkgTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.txtView addSubview:btn];
     */
    
    CGSize titlTextSize = [comment.extraData sizeWithFont:[UIFont fontWithName:@"ARSMaquettePro-Light" size:16] thatFitsToSize:CGSizeMake(280, 1000) lineBreakMode:NSLineBreakByWordWrapping];
    
    float ah = (titlTextSize.height > 100) ? 100 : titlTextSize.height + 50;
    
    UIImageView *txtBkgImage = [[UIImageView alloc] initWithFrame:CGRectMake(8.5, self.bottomView.frame.origin.y - ah, 303, ah)];
    txtBkgImage.image = [[UIImage imageNamed:@"tex-comment-bubble-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(17, 0, 17, 0)];
    [self.txtView addSubview:txtBkgImage];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, txtBkgImage.frame.origin.y + 10, 300, txtBkgImage.frame.size.height - 20)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.contentSize = CGSizeMake(300, titlTextSize.height + 40);
    [self.txtView addSubview:scrollView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.bounds.size.width - 40, titlTextSize.height)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = UIColorFromRGB(0x523c37);
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:16];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    lbl.text = comment.extraData;
    lbl.numberOfLines = 100;
    [scrollView addSubview:lbl];
    
    lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 10 + titlTextSize.height, self.bounds.size.width - 40, 20)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = UIColorFromRGB(0xab9892);
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:12];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    lbl.numberOfLines = 10;
    [scrollView addSubview:lbl];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    lbl.text = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:comment.commentDate]], comment.creatorName];
    
    UIView *pimg = (UIImageView *) [self.commentsScroll viewWithTag:100 + self.currentCommentIndex];
    CGPoint p = [self.txtView convertPoint:pimg.center fromView:self.commentsScroll];
    float ax = p.x - 10;
    if (ax < 20) {
        ax = 20;
    } else if (ax > 280) {
        ax = 280;
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(ax, txtBkgImage.frame.origin.y + txtBkgImage.frame.size.height, 20, 10)];
    imgView.image = [UIImage imageNamed:@"tex-comment-bubble-arrow"];
    [self.txtView addSubview:imgView];
    
    
}

- (void)textBkgTap:(id)sender {
    [self.txtView removeFromSuperview];
    
    self.txtView = nil;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"hideWindowChrome" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showWindowChrome" object:nil];
}

@end
