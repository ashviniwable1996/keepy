//
//  SettingsMenuViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/18/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//




#import "SettingsMenuViewController.h"
#import "FansViewController.h"
#import "AppDelegate.h"
#import "UserVoice.h"
#import "SettingsViewController.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>
#import "Keepy-Bridging-Header.h"
#import "Keepy-Swift.h"
#import "OLKitePrintSDK.h"
#import "PrintIOManager.h"
#import "KPSQLiteManager.h"
//#import "KPImageProviderWrapper"

@interface SettingsMenuViewController () <UINavigationControllerDelegate, UIActionSheetDelegate>

@property(nonatomic, strong) IBOutlet UIImageView *planButton;
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property(nonatomic, strong) IBOutlet UIButton *unlimitedButton;
@property(nonatomic, strong) IBOutlet UIButton *fansButton;
@property (strong, nonatomic) IBOutlet UIButton *albumButton;
@property(nonatomic, strong) IBOutlet UIButton *feedbackButton;
@property (weak, nonatomic) IBOutlet UIButton *keepyPhonebookButton;
@property(nonatomic, strong) IBOutlet UIButton *settingsButton;
@property(nonatomic, strong) IBOutlet UIButton *spreadButton;
//@property(nonatomic, strong) IBOutlet UIButton *dropboxButton;
@property(nonatomic, assign) BOOL isUnlimitedButtonHidden;

@property(nonatomic, strong) NSMutableArray *allKidsForPhotoBook;
//@property(nonatomic, strong) NSMutableArray *allKidNamesArray;
////@property(nonatomic, strong) NSMutableArray *allKidOLAssetArrays;
//@property(nonatomic, strong) NSMutableDictionary *allKidOLAssetDictionary;
@property(nonatomic, assign) BOOL isdeeplink;

- (IBAction)fansTap:(id)sender;

- (IBAction)feedbackTap:(id)sender;

- (IBAction)settingsTap:(id)sender;

- (IBAction)spreadTap:(id)sender;

//- (IBAction)dropBoxTap:(id)sender;
- (IBAction)ulimitedTap:(id)sender;

- (void)hideUnlimitedButton;
- (void)showUnlimitedButton;

@end

@implementation SettingsMenuViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.allKidOLAssetDictionary = [[NSMutableDictionary alloc] init];
    self.allKidNamesArray  = [[NSMutableArray alloc] init];
    self.view.backgroundColor = UIColorFromRGB(0xE4E2E1);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fansRefreshed:) name:FANS_REFRESHED_NOTIFICATION object:nil];
    [self fansRefreshed:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dropBoxRefreshed) name:DROPBOX_REFRESHED_NOTIFICATION object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(planRefreshed:) name:PLAN_REFRESHED_NOTIFICATION object:nil];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, 20)];
    bar.barTintColor = UIColorFromRGB(0xE4E2E1);
    [self.view addSubview:bar];
    if ([UIScreen mainScreen].bounds.size.height == 812) {
        self.scrollView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
    }
    [self.revealController setMinimumWidth:113.0f maximumWidth:113.0f forViewController:self];

    for (UIControl *control in self.scrollView.subviews) {
        if ([control isKindOfClass:[UIButton class]]) {
            [(UIButton *) control titleLabel].font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
            [(UIButton *) control setTitleColor:UIColorFromRGB(0xe6e4e3) forState:UIControlStateNormal];
        }
    }
    
    [self.feedbackButton setTitle:KPLocalizedString(@"send\nfeedback") forState:UIControlStateNormal];
    self.feedbackButton.titleLabel.numberOfLines = 2;
    self.feedbackButton.titleLabel.frame = CGRectMake(0, 0, 120, 50);
    self.feedbackButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.feedbackButton.titleEdgeInsets = UIEdgeInsetsMake(45, 0, 0, 0);

    [self.spreadButton setTitle:KPLocalizedString(@"spread\nthe love") forState:UIControlStateNormal];
    self.spreadButton.titleLabel.numberOfLines = 2;
    self.spreadButton.titleLabel.frame = CGRectMake(0, 0, 120, 50);
    self.spreadButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.spreadButton.titleEdgeInsets = UIEdgeInsetsMake(45, 0, 0, 0);
    
   // [self.albumButton setTitle:KPLocalizedString(@"keepy\nstore") forState:UIControlStateNormal];
    [self.albumButton setTitle:KPLocalizedString(@"photo\ngifts") forState:UIControlStateNormal];
    self.albumButton.titleLabel.numberOfLines = 2;
    self.albumButton.titleLabel.frame = CGRectMake(0, 0, 120, 50);
    self.albumButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.albumButton.titleEdgeInsets = UIEdgeInsetsMake(45, 0, 0, 0);
    
    [self.keepyPhonebookButton setTitle:KPLocalizedString(@"keepyphotobook") forState:UIControlStateNormal];
   // [self.keepyPhonebookButton setTitle:KPLocalizedString(@"photobooks") forState:UIControlStateNormal];
    self.keepyPhonebookButton.titleLabel.numberOfLines = 2;
    self.keepyPhonebookButton.titleLabel.frame = CGRectMake(0, 0, 120, 50);
    self.keepyPhonebookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.keepyPhonebookButton.titleEdgeInsets = UIEdgeInsetsMake(45, 0, 0, 0);

    
    [self.fansButton setTitle:KPLocalizedString(@"add fans") forState:UIControlStateNormal];
    [self fansRefreshed:nil];

    [self.settingsButton setTitle:KPLocalizedString(@"settings") forState:UIControlStateNormal];
    
    self.isUnlimitedButtonHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    /*
    if (![[DBSession sharedSession] isLinked]) {
        [self.dropboxButton setTitle:KPLocalizedString(@"link to\nDropbox") forState:UIControlStateNormal];
    } else {
        [self.dropboxButton setTitle:KPLocalizedString(@"unlink from\nDropbox") forState:UIControlStateNormal];
    }

    self.dropboxButton.titleLabel.numberOfLines = 2;
    self.dropboxButton.titleLabel.frame = CGRectMake(0, 0, 120, 50);
    self.dropboxButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.dropboxButton.titleEdgeInsets = UIEdgeInsetsMake(45, 0, 0, 0);
*/
    /*
    self.dropboxButton.titleLabel.numberOfLines = 2;
    self.dropboxButton.titleLabel.frame = CGRectMake(0, 0, 120, 50);
    self.dropboxButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.dropboxButton.titleEdgeInsets = UIEdgeInsetsMake(47, -40, 0, 0);
    
    self.dropboxButton.backgroundColor = UIColorFromRGB(0x2EA3EB);
    self.dropboxButton.imageEdgeInsets = UIEdgeInsetsMake(-35, 30, 0, 0);
    */
  //  [self dropBoxRefreshed];
    
    
    if (Defaults.planType == 1 || Defaults.planType == 2) {
        [self hideUnlimitedButton];
//        [self showUnlimitedButton];
    } else {
        [self showUnlimitedButton];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions

- (IBAction)fansTap:(id)sender {

    
    
    [[Mixpanel sharedInstance] track:@"fans tap"];

    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fans-right-btn", @"btn", nil]];

    if (![GlobalUtils checkCanInviteFans]) {
        return;
    }

    [self.revealController showViewController:self.revealController.frontViewController];

    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        FansViewController *avc = [[FansViewController alloc] initWithNibName:@"FansViewController" bundle:nil];

        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
        //[navController setNavigationBarHidden:YES];
        AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
        [d.viewController presentViewController:navController animated:YES completion:nil];
    });
    
}

#pragma mark - Notifications

- (void)fansRefreshed:(NSNotification *)notification {
    
    NSString *s = KPLocalizedString(@"add fans");
    NSString *sp = KPLocalizedString(@"fan");
    if ([[[UserManager sharedInstance] myFans] count] > 1) {
        sp = KPLocalizedString(@"fans");
    }

    if ([[[UserManager sharedInstance] myFans] count] > 0) {
        s = [NSString stringWithFormat:@"%lu %@", (unsigned long) [[[UserManager sharedInstance] myFans] count], sp];
    }

    [self.fansButton setTitle:s forState:UIControlStateNormal];
    
}

- (void)planRefreshed:(NSNotification *)notification {
    if (Defaults.planType == 1 || Defaults.planType == 2) {
        [self hideUnlimitedButton];
    } else {
        [self showUnlimitedButton];
    }
}


- (IBAction)didPressAlbumCreationButton:(UIButton *)sender {
    
    [PrintIOManager showPrintIODialogInViewController:self.revealController.frontViewController withImage:nil];
}




- (IBAction)feedbackTap:(id)sender {

    [[Mixpanel sharedInstance] track:@"feedback tap"];

    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"feedback-right-btn", @"btn", nil]];

    [self.revealController showViewController:self.revealController.frontViewController];

    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        UVConfig *config = [UVConfig configWithSite:@"keepy.uservoice.com"
                                             andKey:@"xlgORbPAktc4kuuIKvaIA"
                                          andSecret:@"dOMs8y7cMcntxnPyxiJILmGhYZqrJbY7mv5rcfM0" andEmail:[[UserManager sharedInstance] getMe].email andDisplayName:[[UserManager sharedInstance] getMe].name andGUID:[NSString stringWithFormat:@"%d", [[UserManager sharedInstance] getMe].userId.intValue]];

        [UserVoice presentUserVoiceInterfaceForParentViewController:self andConfig:config];
    });
}

- (IBAction)settingsTap:(id)sender {
    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"settings-right-btn", @"btn", nil]];

    [self.revealController showViewController:self.revealController.frontViewController];

    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

        SettingsViewController *avc = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];

        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
        //[navController setNavigationBarHidden:YES];
        AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
        [d.viewController presentViewController:navController animated:YES completion:nil];
    });
}


- (IBAction)ulimitedTap:(id)sender{
//zvika160920
    [[Mixpanel sharedInstance] track:@"ulimited Tap"];
    
    [self.revealController showViewController:self.revealController.frontViewController];
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        
//         SettingsViewController *avc = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        
        UnlimitedViewController *avc = [[UIStoryboard storyboardWithName:@"Unlimited" bundle:nil] instantiateViewControllerWithIdentifier:@"UnlimitedTable"];
//        
//        UnlimitedTableViewController *avc = [[UnlimitedTableViewController alloc] initWithNibName:@"UnlimitedTableViewController" bundle:nil];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
        //[navController setNavigationBarHidden:YES];
        AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
        [d.viewController presentViewController:navController animated:YES completion:nil];
    });

}


- (IBAction)spreadTap:(id)sender {

    [[Mixpanel sharedInstance] track:@"spread tap"];

    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"spread-right-btn", @"btn", nil]];

//    UIViewController *shareViewController = self.revealController.frontViewController;
    
    [self.revealController showViewController:self.revealController.frontViewController];

    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

        [GlobalUtils spreadTheLove];
    });
}

- (IBAction)dropBoxTap:(id)sender; {
    [[Analytics sharedAnalytics] track:@"btn-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"drop-box-right-btn", @"btn", nil]];

    if (!(DBClientsManager.authorizedClient || DBClientsManager.authorizedTeamClient)) {
        [self.revealController showViewController:self.revealController.frontViewController];

        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [DBClientsManager authorizeFromController:[UIApplication sharedApplication]
                                           controller:self
                                              openURL:^(NSURL *url) {
                                                  [[UIApplication sharedApplication] openURL:url];
                                              }];
        });
    } else {
        [DBClientsManager unlinkAndResetClients];

     //   [self.dropboxButton setTitle:KPLocalizedString(@"link to\nDropbox") forState:UIControlStateNormal];

        [[ServerComm instance] updateDropboxCredentials:@"oauth_token= &oauth_token_secret= &uid= " withSuccessBlock:^(id result) {

            [[NSNotificationCenter defaultCenter] postNotificationName:DROPBOX_REFRESHED_NOTIFICATION object:nil];

        }                                  andFailBlock:^(NSError *error) {

        }];

    }
}

-(void)openPhotoBook:(UIViewController*)viewcontroller isfromdeeplink:(BOOL)isdeeplink {
    self.isdeeplink = isdeeplink;
    [self phoneBookTapped:nil];
}

- (IBAction)phoneBookTapped:(id)sender {
    [[Mixpanel sharedInstance] track:@"phoneBookTapped"];

  [self processKidsForPhotoBook];
  dispatch_async(dispatch_get_main_queue(), ^{
      NSString *URLString = [[NSString alloc] init];
//    // IMP - first way to fetch all assets
      NSDictionary *denormalizedItems = [PrintIOManager denormalizedItems];
      NSArray *unsorteditems = [PrintIOManager items];
      NSSortDescriptor *dateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"itemDate" ascending:NO];
      NSArray *sorteditems = [unsorteditems sortedArrayUsingDescriptors:@[dateSortDescriptor]];

      NSMutableArray *olAssets = [[NSMutableArray alloc] init];
      for (KPLocalItem *currentItem in sorteditems) {
//          KPImageProviderWrapper *imageProvider = [KPImageProviderWrapper instance];
//          NSString *urlString = [imageProvider getOriginalImageURLWithLocalItem:currentItem imageSize:KPImageProviderWrapper.ImageSizeOriginal];
//          NSURL *imgurl = [NSURL URLWithString:urlString];
          KPLocalAsset *originalImage = denormalizedItems[@(currentItem.identifier)][@"originalImage"];
          NSURL *imgurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, originalImage.urlHash]];
//          if(URLString.length == 0){
//              URLString[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, originalImage.urlHash]
//          }
          [olAssets addObject:[OLAsset assetWithURL:imgurl]];
      }
//    //
   
      
 // IMP - second way to fetch all assets
//     NSArray *assets = [KPLocalAsset fetchAll];
//     NSLog(@"There are %lu items in assets array at SettingsMenuVC", (unsigned long)[assets count]);
//     NSMutableArray *olAssets = [[NSMutableArray alloc] init];
//     for(KPLocalAsset *imageAsset in assets){
//         NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]];
//         [olAssets addObject:[OLAsset assetWithURL:imageURL]];
//      }
  //
      
    OLKiteViewController *vc = [[OLKiteViewController alloc] initWithAssets:@[[OLAsset assetWithURL:[NSURL URLWithString:@"https://psps.s3.amazonaws.com/sdk_static/4.jpg"]]]];
    vc.filterProducts = @[@"Photobooks"];
    NSMutableArray *collectionArray = [[NSMutableArray alloc] init];
    OLImagePickerProviderCollection *everyOnecollection = [[OLImagePickerProviderCollection alloc] initWithArray:olAssets name:@"everyone"];
//    [collectionArray addObject:everyOnecollection];
    for (NSString *key in self.allKidNamesArray){
        OLImagePickerProviderCollection *collection = [[OLImagePickerProviderCollection alloc] initWithArray:self.allKidOLAssetDictionary[key] name:key];
        [collectionArray addObject:collection];
    }
      [collectionArray addObject:everyOnecollection];
      [[Mixpanel sharedInstance] track:@"addCustomPhotoProviderWithCollections"];

    [vc addCustomPhotoProviderWithCollections:collectionArray name:@"Keepy" icon:[UIImage imageNamed:@"keepyIcon.png"]];
      if (self.isdeeplink == true) {
//          [self.revealController presentViewController:vc animated:YES completion:NULL];
          AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
          [appdelegate.viewController presentViewController:vc animated:YES completion:NULL];
      }else {
          [self presentViewController:vc animated:YES completion:NULL];

      }
 });
}

- (void)processKidsForPhotoBook {
  [self.allKidOLAssetDictionary removeAllObjects];
  dispatch_async(dispatch_get_main_queue(), ^{
    NSDictionary *denormalizedItems = [PrintIOManager denormalizedItems];
    NSArray *unsortedKids = [KPLocalKid fetchAll];
    
    NSSortDescriptor *ageSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"birthdate" ascending:YES];
    NSSortDescriptor *isMineSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isMine" ascending:YES];
    NSSortDescriptor *serverIDSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"serverID" ascending:YES];
    NSSortDescriptor *localIDSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"identifier" ascending:YES];
    
    NSMutableArray *tkids = [unsortedKids sortedArrayUsingDescriptors:@[isMineSortDescriptor, ageSortDescriptor, serverIDSortDescriptor, localIDSortDescriptor]].mutableCopy;
    self.allKidsForPhotoBook = tkids;
    
    for(KPLocalKid *kid in self.allKidsForPhotoBook){
       [self.allKidNamesArray addObject:kid.name];
       NSLog(@"Kid name in processKidsForPhotoBook is %@",kid.name);
        
        // fetching imgurls for particular kid
        NSArray *itemIDs = [KPLocalKidItem fetchItemIDsForKid:kid];
        NSArray *unsortedKeepies = [KPLocalItem idsToObjects:itemIDs];
        
        NSLog(@"There are %lu items in itemIDs array at SettingsMenuVC", (unsigned long)[itemIDs count]);
        NSLog(@"There are %lu items in unsortedKeepies array at SettingsMenuVC", (unsigned long)[unsortedKeepies count]);
        
        NSSortDescriptor *dateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"itemDate" ascending:NO];
        NSArray *allKeepies = [unsortedKeepies sortedArrayUsingDescriptors:@[dateSortDescriptor]];
        NSLog(@"There are %lu items in allKeepies array at SettingsMenuVC", (unsigned long)[allKeepies count]);
        
        NSMutableArray *olAssetsArray = [[NSMutableArray alloc] init];
        for (KPLocalItem *currentKeepy in allKeepies) {
//            KPImageProviderWrapper *imageProvider = [KPImageProviderWrapper instance];
//            NSString *urlString = [imageProvider getOriginalImageURLWithLocalItem:currentKeepy imageSize:KPImageProviderWrapper.ImageSizeOriginal];
//            NSURL *imgurl = [NSURL URLWithString:urlString];
            
            //ABove lines are used to returns image irl with original size
            
            // following This returns default image
            KPLocalAsset *originalImage = denormalizedItems[@(currentKeepy.identifier)][@"originalImage"];
            NSURL *imgurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, originalImage.urlHash]];
            //
            
            [olAssetsArray addObject:[OLAsset assetWithURL:imgurl]];
        }
        NSLog(@"There are %lu items in olAssetsArray array at processKidsImgs (around 29 times)", (unsigned long)[olAssetsArray count]);
        [self.allKidOLAssetDictionary setValue:olAssetsArray forKey:kid.name];
    }
  });
}


- (void)dropBoxRefreshed {
//    CGRect aframe = self.feedbackButton.frame;
//    CGRect bframe = self.dropboxButton.frame;
//    
//    
//    
//    if ([[DBSession sharedSession] isLinked]) {
//        [self.dropboxButton setTitle:KPLocalizedString(@"unlink from\nDropbox") forState:UIControlStateNormal];
//        bframe.origin.y = 548;
//        aframe.origin.y = 439;
//    }
//    else {
//        [self.dropboxButton setTitle:KPLocalizedString(@"link to\nDropbox") forState:UIControlStateNormal];
//        aframe.origin.y = 548;
//        bframe.origin.y = 439;
//
//    }
//
//    [UIView animateWithDuration:0.3 animations:^{
//        // self.feedbackButton.frame = aframe;
//        // self.dropboxButton.frame = bframe;
//    }];
}

#pragma mark - Private functions

- (void)hideUnlimitedButton {
    if (self.isUnlimitedButtonHidden) {
        return;
    }
    
    self.unlimitedButton.hidden = YES;
    
    for (UIControl *control in self.scrollView.subviews) {
        if ([control isKindOfClass:[UIButton class]]) {
            CGRect frame = control.frame;
            frame.origin.y -= 100;
            [(UIButton *) control setFrame:frame];
        }
    }
    
    self.scrollView.contentSize = CGSizeMake(100, 685);
    self.isUnlimitedButtonHidden = YES;
}

- (void)showUnlimitedButton {
    if (!self.isUnlimitedButtonHidden) {
        return;
    }
    
    for (UIControl *control in self.scrollView.subviews) {
        if ([control isKindOfClass:[UIButton class]]) {
            CGRect frame = control.frame;
            frame.origin.y += 100;
            [(UIButton *) control setFrame:frame];
        }
    }
    
    self.scrollView.contentSize = CGSizeMake(100, 785);
    self.isUnlimitedButtonHidden = NO;
}


//- (void)loadImage:(NSNotification *)notification {
//}

// - (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {

//- (void)loadImage:(KPLocalItem *)localItem {
//
//}


//func loadImage(localItem: KPLocalItem, imageSize: ImageSize, completion: @escaping (UIImage?) -> Void) -> SDWebImageOperation? {
//    let originalItemImageKey: String?
//    let itemImageKey : String?
//
//    if let originalItemImage = KPLocalAsset.fetch(byLocalID: UInt(localItem.originalImageAssetID)) {
//        originalItemImageKey = Defaults.assetsURLString + imageSize.compose(name: originalItemImage.urlHash(), suffix: ImageFormat.Jpeg.rawValue)
//    } else {
//        originalItemImageKey = .none
//    }
//
//    if let itemImage = KPLocalAsset.fetch(byLocalID: UInt(localItem.imageAssetID)) {
//        itemImageKey = Defaults.assetsURLString + imageSize.compose(name: itemImage.urlHash(), suffix: ImageFormat.Jpeg.rawValue)
//    } else {
//        itemImageKey = .none
//    }
//
//    let key: String?
//    if let itemImageKey = itemImageKey, self.imageCache.diskImageExists(withKey: itemImageKey) {
//        key = itemImageKey;
//    } else if let originalItemImageKey = originalItemImageKey, self.imageCache.diskImageExists(withKey: originalItemImageKey) {
//        key = originalItemImageKey;
//    } else {
//        key = itemImageKey;
//    }
//
//    if let key = key {
//        let url = URL(string:key)!
//        print(url)
//        return self.webImageManager.downloadImage(with: url, options: [.retryFailed], progress: nil) { (image, error, cacheType, finished, imageURL) in
//            DispatchQueue.main.async {
//                completion(image)
//            }
//        }
//    } else {
//        completion(.none)
//        return .none
//    }
//}

@end
