//
//  BirthDateViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "BirthDateViewController.h"

@interface BirthDateViewController ()

@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UILabel *theTitle;
@property(nonatomic, weak) IBOutlet UIButton *saveButton;
// @property(nonatomic, weak) IBOutlet UIDatePicker *datePicker;


@property(nonatomic, strong) UIDatePicker *datePicker;

@end

@implementation BirthDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    CGRect datePickerFrame;
    datePickerFrame.origin.y = 68;
    datePickerFrame.size.width = 320;
    datePickerFrame.size.height = 162;
    self.datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.timeZone = [NSTimeZone defaultTimeZone];
    
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    self.theTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    if (self.birthDateDate) {
        self.datePicker.date = self.birthDateDate;
    }
}

- (void)viewDidLayoutSubviews{
    
    [self.datePicker removeFromSuperview];
    [self.view addSubview:self.datePicker];
    
}

- (IBAction)save:(id)sender {
    self.birthDateDate = self.datePicker.date;
    if (self.delegate && [self.delegate respondsToSelector:@selector(birthDateViewControllerDidSave:)]) {
        [self.delegate birthDateViewControllerDidSave:self];
    }
    //doesn't work on iOS 8.0 - 8.0.2
    //http://stackoverflow.com/questions/25654941/unwind-segue-not-working-in-ios-8
    //[self performSegueWithIdentifier:@"AddSiblingExit" sender:self];
}

- (IBAction)cancel:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(birthDateViewControllerDidCancel:)]) {
        [self.delegate birthDateViewControllerDidCancel:self];
    }
    //[self performSegueWithIdentifier:@"AddSiblingExit" sender:self];
}

@end
