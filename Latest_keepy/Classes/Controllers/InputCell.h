//
//  InputCell.h
//  Keepy
//
//  Created by Troy Payne on 2/12/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface InputCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UITextField *input;

+ (CGFloat)height;

@end
