//
//  NotificationsViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 4/21/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@interface NotificationsViewController : KPPopViewController

@end
