#import <UIKit/UIKit.h>

@protocol DatePickerViewControllerDelegate;

@interface DatePickerViewController : UIViewController
@property(nonatomic, unsafe_unretained) id <DatePickerViewControllerDelegate> delegate;

+ (DatePickerViewController *)datePickerViewControllerWithDelegate:(id <DatePickerViewControllerDelegate>)delegate date:(NSDate *)date;
@end


@protocol DatePickerViewControllerDelegate <NSObject>
- (void)datePickerValueDidChange:(NSDate *)date;
@end