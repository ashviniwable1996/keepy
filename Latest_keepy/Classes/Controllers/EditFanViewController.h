//
//  EditFanViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Fan.h"
#import "KPPopViewController.h"

@protocol EditFanDelegate

- (void)fanChanged:(Fan *)fan;

@end

@interface EditFanViewController : KPPopViewController

@property(nonatomic, assign) id <EditFanDelegate> delegate;

- (instancetype)initWithFan:(Fan *)fan;

- (instancetype)initWithUserData:(NSDictionary *)userData;

- (instancetype)initWithFan:(Fan *)fan andRelationMode:(NSInteger)relationMode;

- (instancetype)initWithUserData:(NSDictionary *)userData andRelationMode:(NSInteger)relationMode;

- (IBAction)textFieldChanged:(id)sender;

@end
