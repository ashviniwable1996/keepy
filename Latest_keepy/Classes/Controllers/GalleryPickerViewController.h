//
//  GalleryPickerViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 8/6/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KPPopViewController.h"

@class KPAsset;

@class GalleryPickerViewController;

@protocol GalleryPickerDelegate <NSObject>

@optional
- (void)galleryPickerController:(GalleryPickerViewController *)picker didFinishPickingMediaWithInfo:(NSArray *)info;

- (void)galleryPickerControllerDidCancel:(GalleryPickerViewController *)picker;

@end

@interface GalleryPickerViewController : KPPopViewController

@property(nonatomic, assign) id <GalleryPickerDelegate> delegate;
@property (nonatomic, assign) BOOL showToolbar;

- (instancetype)initWithAssets:(NSArray<KPAsset*>*)assets;

@end
