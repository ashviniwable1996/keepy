//
//  EditFanViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

#import "EditFanViewController.h"
#import "KPAddressBookUtils.h"
#import "KPLocalKid.h"

#import "Keepy-Swift.h"

@interface EditFanViewController ()

@property(nonatomic, strong) Fan *fan;
@property(nonatomic, strong) NSDictionary *userData;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dadButtonCenterConstraint;

@property(nonatomic, strong) IBOutlet UITextField *emailTF;
@property(nonatomic, strong) IBOutlet UITextField *nameTF;
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property(nonatomic, strong) UIButton *resendInviteButton;
@property(nonatomic, strong) IBOutlet UILabel *thisisLabel;
@property(nonatomic, strong) IBOutlet UILabel *momLabel;
@property(nonatomic, strong) IBOutlet UILabel *dadLabel;
@property(nonatomic, strong) IBOutlet UILabel *grandpaLabel;
@property(nonatomic, strong) IBOutlet UILabel *grandmaLabel;
@property(nonatomic, strong) IBOutlet UILabel *familyLabel;
@property(nonatomic, strong) IBOutlet UILabel *friendLabel;
@property(nonatomic, strong) IBOutlet UILabel *auntLabel;
@property(nonatomic, strong) IBOutlet UILabel *uncleLabel;

@property(nonatomic, strong) IBOutlet UIView *container;

@property(nonatomic) NSInteger relationMode;

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, weak) NSString *emailAddress;

- (IBAction)relationTap:(id)sender;

@end

@interface EditFanViewController (SuggestingEmail) <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@end

@implementation EditFanViewController (SuggestingEmail)

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.tableView]) {
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.tableView == nil) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(textField.frame.origin.x, 184, textField.frame.size.width, 44 * 2) style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.rowHeight = 44;
        self.tableView.layer.cornerRadius = 10.0;
        self.tableView.layer.masksToBounds = YES;
        self.tableView.separatorColor = [UIColor keepyBrownColor];
        [self.view addSubview:self.tableView];
    }
    NSString *searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [KPAddressBookUtils sharedUtils];

    if (self.emailAddress == nil) {
        [self searchAutocompleteEntriesWithSubstring:searchStr];
    }
    else {
        NSArray *myArray = [searchStr componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];

        [self searchAutocompleteEntriesWithSubstring:[myArray lastObject]];
    }
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[KPAddressBookUtils sharedUtils].searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ABCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }

    cell.textLabel.font = self.emailTF.font;
    cell.textLabel.textColor = self.emailTF.textColor;
    cell.textLabel.text = [[KPAddressBookUtils sharedUtils].searchArray objectAtIndex:indexPath.row];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    self.emailAddress = [[KPAddressBookUtils sharedUtils].searchArray objectAtIndex:indexPath.row];
    self.emailTF.text = self.emailAddress;

    self.tableView.hidden = YES;
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    [[KPAddressBookUtils sharedUtils].searchArray removeAllObjects];

    for (NSString *curString in [KPAddressBookUtils sharedUtils].emailArray) {
        NSRange substringRange = [curString rangeOfString:substring options:NSCaseInsensitiveSearch];
        if (substringRange.location == 0) {
            [[KPAddressBookUtils sharedUtils].searchArray addObject:curString];
        }
    }

    if ([[KPAddressBookUtils sharedUtils].searchArray count] > 0) {
        self.tableView.hidden = NO;
    }

    if ([UIScreen mainScreen].bounds.size.height >= 548) {
        if ([KPAddressBookUtils sharedUtils].searchArray.count > 5) {
            self.tableView.scrollEnabled = YES;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 80);
        }
        else {
            self.tableView.scrollEnabled = NO;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 44 * [KPAddressBookUtils sharedUtils].searchArray.count);
        }
    }
    else {
        if ([KPAddressBookUtils sharedUtils].searchArray.count > 3) {
            self.tableView.scrollEnabled = YES;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, 274, 50);
        }
        else {
            self.tableView.scrollEnabled = NO;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 44 * [KPAddressBookUtils sharedUtils].searchArray.count);
        }
    }

    [self.tableView reloadData];
}

@end

@implementation EditFanViewController

- (instancetype)initWithFan:(Fan *)fan {
    self = [super initWithNibName:@"EditFanViewController" bundle:nil];
    if (self) {
        self.fan = fan;
    }
    return self;
}

- (instancetype)initWithFan:(Fan *)fan andRelationMode:(NSInteger)relationMode {
    self = [super initWithNibName:@"EditFanViewController" bundle:nil];
    if (self) {
        self.fan = fan;
        self.relationMode = relationMode;
    }
    return self;
}

- (instancetype)initWithUserData:(NSDictionary *)userData {
    self = [super initWithNibName:@"EditFanViewController" bundle:nil];
    if (self) {
        self.userData = userData;
    }
    return self;
}

- (instancetype)initWithUserData:(NSDictionary *)userData andRelationMode:(NSInteger)relationMode {
    self = [super initWithNibName:@"EditFanViewController" bundle:nil];
    if (self) {
        self.userData = userData;
        self.relationMode = relationMode;
    }
    return self;

}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = KPLocalizedString(@"add fan");
    switch (self.relationMode) {
        case 1:
            self.title = KPLocalizedString(@"add partner");
            break;
        case 2:
            self.title = KPLocalizedString(@"add grandparent");
            break;
        case 3:
            self.title = KPLocalizedString(@"add family member");
            break;
        case 4:
            self.title = KPLocalizedString(@"add friend");
            break;
        case 5:
            self.title = KPLocalizedString(@"add aunt or uncle");
            break;
    }
    self.emailTF.delegate = self;

    if (self.fan != nil) {
        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-fan", @"source", nil]];

        self.nameTF.text = self.fan.nickname;

        if (self.fan.email != nil) {
            self.emailTF.text = self.fan.email;
        }

        if (self.fan.fanId.intValue < 1) {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];

            self.title = KPLocalizedString(@"add fan");

            return;
        }
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"fans"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];

        self.title = KPLocalizedString(@"edit fan");
        for (UIControl *control in ((UIView *) [self.scrollView.subviews objectAtIndex:0]).subviews) {
            if ([control isKindOfClass:[UIButton class]]) {
                if (control.tag == self.fan.relationType.intValue) {
                    [(UIButton *) control setSelected:YES];
                }
            }
        }


        if (self.fan.facebookId != nil) {
            [self setFacebookUI];
        }
    }
    else {
        if (self.navigationController.view.tag == 1) {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];
        } else {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add fan"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];
        }


        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-fan", @"source", nil]];

        if (self.userData != nil) {
            if ([self.userData valueForKey:@"name"] != nil) {
                self.nameTF.text = [self.userData valueForKey:@"name"];
            }

            if ([self.userData valueForKey:@"email"] != nil) {
                self.emailTF.text = [self.userData valueForKey:@"email"];
            }

            if ([self.userData valueForKey:@"relationship"] != nil) {
                int rvalue = -1;
                NSString *s = [[self.userData valueForKey:@"relationship"] lowercaseString];
                if ([s isEqualToString:@"father"] || [s isEqualToString:@"father-in-law"]) {
                    rvalue = 3;
                } else if ([s isEqualToString:@"mother"] || [s isEqualToString:@"mother-in-law"]) {
                    rvalue = 2;
                } else if ([s isEqualToString:@"wife"]) {
                    rvalue = 0;
                } else if ([s isEqualToString:@"husband"]) {
                    rvalue = 1;
                } else {
                    rvalue = 4;
                }

                for (UIControl *control in ((UIView *) [self.scrollView.subviews objectAtIndex:0]).subviews) {
                    if ([control isKindOfClass:[UIButton class]]) {
                        if (control.tag == rvalue) {
                            [(UIButton *) control setSelected:YES];
                        }
                    }
                }

                if (rvalue > -1) {
                    self.rightButtonEnabled = YES;
                }

            }

            if ([self.userData valueForKey:@"fbId"] != nil) {
                [self setFacebookUI];
            }

            [self checkEnableSaveButton];
        }
    }

    for (UIControl *control in ((UIView *) [self.scrollView.subviews objectAtIndex:0]).subviews) {
        if ([control isKindOfClass:[UILabel class]]) {
            if ([[(UILabel *) control text] isEqualToString:@"this is"]) {
                [(UILabel *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Light" size:21]];
            } else {
                [(UILabel *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Light" size:14]];
            }
        }
        else if ([control isKindOfClass:[UITextField class]]) {
            [(UITextField *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:17]];
            [(UITextField *) control setTextColor:UIColorFromRGB(0x4d3c36)];
        }
    }
    self.nameTF.placeholder = KPLocalizedString(@"name");
    self.emailTF.placeholder = KPLocalizedString(@"email");


    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.delegate = self;
    [gesture setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:gesture];

    self.scrollView.contentSize = ((UIView *) [self.scrollView.subviews objectAtIndex:0]).frame.size;

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardDidShow:)
                   name:UIKeyboardDidShowNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];

    self.thisisLabel.text = KPLocalizedString(@"this is");
    self.momLabel.text = KPLocalizedString(@"mom");
    self.dadLabel.text = KPLocalizedString(@"dad");
    self.grandmaLabel.text = KPLocalizedString(@"grandma");
    self.grandpaLabel.text = KPLocalizedString(@"grandpa");
    self.familyLabel.text = KPLocalizedString(@"family");
    self.friendLabel.text = KPLocalizedString(@"friend");
}

- (void)viewDidLayoutSubviews {
    CGRect aframe = self.view.frame;
    self.scrollView.frame = aframe;

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(54.0, 0.0, 0.0, 0.0);
    self.scrollView.contentInset = contentInsets;

    self.thisisLabel.hidden = (self.relationMode == 3 || self.relationMode == 4);
    for (UIControl *control in ((UIView *) [self.scrollView.subviews objectAtIndex:0]).subviews) {
        if ([control isKindOfClass:[UIButton class]]) {
            if (control.tag < 100) {
                if (self.relationMode == 1) //partner
                {
                    control.hidden = (control.tag > 1);
                    CGRect aframe = control.frame;
                    if (control.tag == 0) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) - 80 ;//100;
                    }
                    else if (control.tag == 1) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) ;//166;
                    }
                    control.frame = aframe;
                }
                else if (self.relationMode == 2) //grandparents
                {
                    control.hidden = (control.tag < 2 || control.tag > 3);
                    CGRect aframe = control.frame;
                    if (control.tag == 2) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) - 80;//100;
                    }
                    else if (control.tag == 3) {
                        aframe.origin.x = aframe.origin.x = (self.view.bounds.size.width / 2) ;//166;
                    }
                    control.frame = aframe;
                }
                else if (self.relationMode == 3 || self.relationMode == 4) //family/friends
                {
                    control.hidden = YES;
                    control.selected = ((self.relationMode == 3 && control.tag == 4) || (self.relationMode == 4 && control.tag == 5));

                }
                else if (self.relationMode == 5) //aunt/uncle
                {
                    control.hidden = (control.tag < 6 || control.tag > 7);
                    CGRect aframe = control.frame;
                    if (control.tag == 6) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) - 80;//100;
                        aframe.origin.y = 203;
                    }
                    else if (control.tag == 7) {
                        aframe.origin.x = (self.view.bounds.size.width / 2);//166;
                        aframe.origin.y = 203;
                    }
                    control.frame = aframe;
                }
            }
        }
        else if ([control isKindOfClass:[UILabel class]]) {
            if (control.tag >= 10) {
                if (self.relationMode == 1) //partner
                {
                    control.hidden = (control.tag > 11);
                    CGRect aframe = control.frame;
                    if (control.tag == 10) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) - 75;//107;
                    }
                    else if (control.tag == 11) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) + 7;//173;
                    }
                    control.frame = aframe;
                }
                else if (self.relationMode == 2) //grandparents
                {
                    control.hidden = (control.tag < 12 || control.tag > 13);
                    CGRect aframe = control.frame;
                    if (control.tag == 12) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) - 85;//100;
                    }
                    else if (control.tag == 13) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) ;//166;
                    }
                    control.frame = aframe;
                }
                else if (self.relationMode == 3 || self.relationMode == 4) //family/friends
                {
                    control.hidden = YES;
                }
                else if (self.relationMode == 5) //aunt/uncle
                {
                    control.hidden = (control.tag < 16 || control.tag > 17);
                    CGRect aframe = control.frame;
                    if (control.tag == 16) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) - 85;//100;
                        aframe.origin.y = 269;
                    }
                    else if (control.tag == 17) {
                        aframe.origin.x = (self.view.bounds.size.width / 2) ;//166;
                        aframe.origin.y = 269;
                    }
                    control.frame = aframe;
                }
            }
        }

    }

}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.fan != nil) {
        UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setBackgroundImage:[[UIImage imageNamed:@"delete-photo-buttonBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
        
        [deleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        deleteButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
        [deleteButton setTitle:KPLocalizedString(@"remove fan") forState:UIControlStateNormal];
        
        deleteButton.frame = CGRectMake((self.view.frame.size.width - 156) / 2, 450, 156, 44);
        [deleteButton addTarget:self action:@selector(deleteTap:) forControlEvents:UIControlEventTouchUpInside];
        deleteButton.tag = 102;
        [[self.scrollView.subviews objectAtIndex:0] addSubview:deleteButton];
        
        if (![self.fan.didApprove boolValue]) {
            self.resendInviteButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.resendInviteButton setBackgroundImage:[[UIImage imageNamed:@"big-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
            
            [self.resendInviteButton setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
            self.resendInviteButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
            [self.resendInviteButton setTitle:KPLocalizedString(@"resend invite") forState:UIControlStateNormal];
            
            self.resendInviteButton.frame = CGRectMake((self.view.frame.size.width - 156) / 2, 510, 156, 44);
            [self.resendInviteButton addTarget:self action:@selector(resendInviteTap:) forControlEvents:UIControlEventTouchUpInside];
            self.resendInviteButton.tag = 103;
            [[self.scrollView.subviews objectAtIndex:0] addSubview:self.resendInviteButton];
            
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setFacebookUI {
    self.emailTF.hidden = YES;
    UIView *aview = (UIView *) [self.scrollView.subviews objectAtIndex:0];
    [[aview.subviews objectAtIndex:1] setHidden:YES];

    for (int i = 4; i < [aview.subviews count]; i++) {
        UIView *v = [aview.subviews objectAtIndex:i];
        CGRect aframe = v.frame;
        aframe.origin.y -= 50;
        v.frame = aframe;
    }
}

#pragma mark - Actions

- (IBAction)relationTap:(id)sender {
    [self.nameTF resignFirstResponder];
    [self.emailTF resignFirstResponder];

    for (UIControl *control in ((UIView *) [self.scrollView.subviews objectAtIndex:0]).subviews) {
        if ([control isKindOfClass:[UIButton class]]) {
            [(UIButton *) control setSelected:NO];
        }
    }

    [(UIButton *) sender setSelected:YES];
    //if (self.fan != nil)
    //    self.fan.relationType.intValue = [(UIButton*)sender tag];

    [self checkEnableSaveButton];
}

- (void)topBtnTap:(UIButton *)sender {
    if (sender.tag == 1) //Save
    {
        [self.emailTF resignFirstResponder];
        [self.nameTF resignFirstResponder];

        NSInteger relationType = -1;
        for (UIControl *control in  ((UIView *) [self.scrollView.subviews objectAtIndex:0]).subviews) {
            if ([control isKindOfClass:[UIButton class]]) {
                if ([(UIButton *) control isSelected]) {
                    relationType = [(UIButton *) control tag];
                    break;
                }
            }
        }

        if (self.fan == nil) {
            NSString *fbId = @"1";
            NSString *aEmail = self.emailTF.text;
            if (self.userData != nil && [self.userData valueForKey:@"fbId"] != nil) {
                fbId = [self.userData valueForKey:@"fbId"];
                aEmail = @"xxx";
            }

            // KPLocalKid *myFirstKid = [KPLocalKid fetchMine][0];

            // now, let's look at the fan

            
           //  Kid *kid = (Kid *) [[[UserManager sharedInstance] myKids] objectAtIndex:0];
            // Fan *afan = [Fan MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"kids contains %@ AND (email=%@ or facebookId=%@)", kid, aEmail, fbId]];

            Fan *afan = [Fan MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"(email=%@ or facebookId=%@)", aEmail, fbId]];

            if (afan != nil) {
                if (self.emailTF.text.length == 0) {
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"You already have a fan with this email")]; /*onDismiss:^{
                        [self.emailTF becomeFirstResponder];
                    }];*/
                    return;
                }
            }
        }


        if (self.userData != nil && [self.userData valueForKey:@"fbId"] != nil) {
            [[UserManager sharedInstance] appInviteFromViewController:self];
            //[self sendFbInvite:[self.userData valueForKey:@"fbId"] andRelationType:relationType];
        }
        else {
            [self updateFan:relationType];
        }
    }
    else {
        if (self.fan != nil && self.fan.fanId.intValue < 1) {
            [self.fan MR_deleteEntity];
            [self dismissViewControllerAnimated:YES completion:nil];

            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-fan", @"source", @"cancel", @"result", nil]];

        }
        else {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-fan", @"source", @"cancel", @"result", nil]];

            if (self.navigationController.view.tag == 1) {
                NSInteger c = [self.navigationController.viewControllers count];

                if (c < 3) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:c - 3] animated:YES];
                }
            }
            else {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

#if TO_BE_DELETED
- (void)sendFbInvite:(NSString *)fbId andRelationType:(NSInteger)relationType {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:fbId, @"to",
                    nil];

    //[FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {

    // Display the requests dialog

    NSMutableString *kidsStr = [[NSMutableString alloc] init];
    NSString *commaStr = @"";
    for (int i = 0; i < [[[UserManager sharedInstance] myKids] count]; i++) {
        Kid *kid = (Kid *) [[[UserManager sharedInstance] myKids] objectAtIndex:i];
        [kidsStr appendFormat:@"%@%@", commaStr, kid.name];
        if (i == [[[UserManager sharedInstance] myKids] count] - 2) {
            commaStr = @" and ";
        } else {
            commaStr = @", ";
        }

    }

    [FBWebDialogs
            presentRequestsDialogModallyWithSession:nil
                                            message:[NSString stringWithFormat:KPLocalizedString(@"I\'d love you to be %@\'s fan on keepy so you can see all of her awesome creations."), kidsStr]
                                              title:nil
                                         parameters:params
                                            handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                if (error) {
                                                    // Error launching the dialog or sending request.
                                                    DDLogInfo(@"Error sending request.");
                                                    [SVProgressHUD dismiss];
                                                } else {
                                                    if (result == FBWebDialogResultDialogNotCompleted) {
                                                        // User clicked the "x" icon
                                                        DDLogInfo(@"User canceled request.");
                                                        [SVProgressHUD dismiss];
                                                    } else {
                                                        // Handle the send request callback
                                                        NSDictionary *urlParams = [GlobalUtils parseURLParams:[resultURL query]];
                                                        if (![urlParams valueForKey:@"request"]) {
                                                            // User clicked the Cancel button
                                                            DDLogInfo(@"User canceled request.");
                                                            [SVProgressHUD dismiss];
                                                        } else {
                                                            // User clicked the Send button
                                                            NSString *requestID = [urlParams valueForKey:@"request"];
                                                            DDLogInfo(@"Request ID: %@", requestID);
                                                            [self updateFan:relationType facebookId:fbId facebookRequestId:requestID];
                                                        }
                                                    }
                                                }
                                            }];

    //}];
}
#endif

- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self.nameTF resignFirstResponder];
    [self.emailTF resignFirstResponder];
}

- (void)deleteTap:(id)sender {
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"remove fan", @"")
                                                                message:NSLocalizedString(@"are you sure?", comment: @"")
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"no" style:UIAlertActionStyleCancel handler:nil]];
    [ac addAction:[UIAlertAction actionWithTitle:@"yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[ServerComm instance] deleteFan:self.fan.fanId.intValue withSuccessBlock:^(id result) {
            if ([[result valueForKey:@"status"] intValue] == 0) {
                [self.fan MR_deleteEntity];
                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                [self.delegate fanChanged:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:FANS_REFRESHED_NOTIFICATION object:nil];
                
                [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fan", @"source", @"delete", @"result", nil]];
                
                [[Analytics sharedAnalytics] track:@"item-delete" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fan", @"itemType", nil]];
                
                [GlobalUtils updateAnalyticsUser];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error removing fan")];
            }
        }                   andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
        }];
    }]];
    
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)resendInviteTap:(id)sender {
    [SVProgressHUD showWithStatus:KPLocalizedString(@"sending")];

    if (self.fan.facebookId != nil) {
        [[UserManager sharedInstance] appInviteFromViewController:self];
        //[self sendFbInvite:self.fan.facebookId andRelationType:self.fan.relationType.intValue];
    }
    else {
        [[ServerComm instance] reinviteFan:self.fan.fanId.intValue withSuccessBlock:^(id result) {
            if ([[result valueForKey:@"status"] intValue] == 0) {
                [SVProgressHUD showSuccessWithStatus:KPLocalizedString(@"invite sent")];
            } else {
                NSString *s = KPLocalizedString(@"there was problem re-inviting fan");
                if ([[result valueForKey:@"error"] valueForKey:@"errors"] != nil) {
                    NSArray *arr = [(NSArray *) [result valueForKey:@"error"] valueForKey:@"errors"];
                    if ([arr count] > 0) {
                        s = [arr objectAtIndex:0];
                    }
                }
                [SVProgressHUD showErrorWithStatus:s];
            }
        }                     andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
        }];
    }
}

#pragma mark - Keyboard hide/show

- (void)keyboardDidShow:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(54.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    /*
    if (self.currentTF != nil)
    {
        CGPoint point = self.currentTF.frame.origin;
        point = [self.scrollView convertPoint:point fromView:(UIView*)[self.scrollView.subviews objectAtIndex:0]];
        CGRect aRect = CGRectMake(point.x, point.y + 40, CGRectGetWidth(self.currentTF.frame), CGRectGetHeight(self.currentTF.frame));
        [self.scrollView scrollRectToVisible:aRect animated:YES];
    }*/
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(54.0, 0.0, 0.0, 0.0);
    self.scrollView.contentInset = contentInsets;

    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 10, 10) animated:YES];
}

- (IBAction)textFieldChanged:(id)sender {
    [self checkEnableSaveButton];
}

- (void)checkEnableSaveButton {
    self.rightButtonEnabled = NO;

    NSInteger relationType = -1;
    for (UIControl *control in  ((UIView *) [self.scrollView.subviews objectAtIndex:0]).subviews) {
        if ([control isKindOfClass:[UIButton class]]) {
            if ([(UIButton *) control isSelected]) {
                relationType = [(UIButton *) control tag];
                break;
            }
        }
    }


    if (relationType == -1 && (self.relationMode <= 2 || self.relationMode == 5)) {
        return;
    }

    if (self.nameTF.text.length == 0) {
        return;
    }

    self.emailTF.text = [self.emailTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];

    BOOL validEmail = self.emailTF.text.length > 0 && self.emailTF.text.isEmailAddress;
    if (!validEmail && !(self.userData != nil && [self.userData valueForKey:@"fbId"] != nil) && !(self.fan != nil && self.fan.facebookId != nil)) {
        return;
    }

    self.rightButtonEnabled = YES;
}

- (void)updateFan:(NSInteger)relationType {
    [self updateFan:relationType facebookId:nil facebookRequestId:nil];
}

- (void)updateFan:(NSInteger)relationType facebookId:(NSString *)facebookId facebookRequestId:(NSString *)facebookRequestId {
    [SVProgressHUD showWithStatus:KPLocalizedString(@"updating")];
    if (self.fan == nil || self.fan.fanId.intValue < 1) {
        [[ServerComm instance] addFan:self.emailTF.text withName:self.nameTF.text andRelationType:relationType andFacebookId:facebookId andFacebookRequestId:facebookRequestId andBirthdate:nil withSuccessBlock:^(id result) {

            int status = [[result valueForKey:@"status"] intValue];
            if (status == 0) {
                [[Analytics sharedAnalytics] track:@"item-add" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fan", @"itemType", nil]];

                dispatch_async(dispatch_get_main_queue(), ^{
                    Fan *fan = [Fan MR_createEntity];
                    fan.fanId = @([[[result valueForKey:@"result"] valueForKey:@"id"] intValue]);
                    fan.userId = @([[[result valueForKey:@"result"] valueForKey:@"userId"] intValue]);
                    if (facebookId != nil) {
                        fan.facebookId = facebookId;
                    } else {
                        fan.email = self.emailTF.text;
                    }

                    fan.nickname = self.nameTF.text;
                    fan.relationType = @(relationType);
                    for (Kid *kid in [[UserManager sharedInstance] myKids]) {
                        [fan addKidsObject:kid];
                    }
                    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                    [SVProgressHUD dismiss];
                    [self.delegate fanChanged:fan];
                    [[NSNotificationCenter defaultCenter] postNotificationName:FANS_REFRESHED_NOTIFICATION object:nil];

                    [GlobalUtils updateAnalyticsUser];

                    @try {
                        [FBSDKAppEvents logEvent:@"add fan"];
                    } @catch (NSException *e) {}

                    int64_t delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                        [GlobalUtils fireTrigger:TRIGGER_TYPE_ADDFAN];
                    });

                    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"add-fan", @"source", @"ok", @"result", nil]];

                    if (self.navigationController == nil) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    } else if (self.navigationController.view.tag == 1) {
                        NSInteger c = [self.navigationController.viewControllers count];

                        if (c < 3) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        } else {
                            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:c - 3] animated:YES];
                        }
                    }
                    else {
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }

                });
            }
            else {
                if ([[[[result valueForKey:@"error"] valueForKey:@"errors"] objectAtIndex:0] isEqualToString:@"already invited"]) {
                    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:KPLocalizedString(@"%@ was already invited by you."), self.emailTF.text]];
                }
                else {
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"There was a problem adding fan")];
                }
            }
        }                andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
        }];
    }
    else {
        self.fan.nickname = self.nameTF.text;
        self.fan.email = self.emailTF.text;
        self.fan.relationType = @(relationType);

        [[ServerComm instance] updateFan:self.fan andFacebookRequestId:facebookRequestId withSuccessBlock:^(id result) {
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [SVProgressHUD dismiss];
            [self.delegate fanChanged:self.fan];

            [[Analytics sharedAnalytics] track:@"item-updated" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fan", @"itemType", nil]];


            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-fan", @"source", @"ok", @"result", nil]];

            [self.navigationController popViewControllerAnimated:YES];

        }                   andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"There was a problem connecting to Keepy\'s servers. Please try again soon."];
        }];
    }
}

@end
