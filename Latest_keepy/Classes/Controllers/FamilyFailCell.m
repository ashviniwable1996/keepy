//
//  FamilyFailCell.m
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilyFailCell.h"

@interface FamilyFailCell ()

@property(nonatomic, weak) IBOutlet UILabel *body;
@property(nonatomic, assign) CGRect originalBodyFrame;

@end

@implementation FamilyFailCell

#define DEFAULT_TITLE_WIDTH 208.0f
#define DEFAULT_TITLE_HEIGHT 41.0f
#define CELL_HEIGHT 150.0f

- (void)awakeFromNib {
    [super awakeFromNib];

    NSString *light1 = @"Go to";
    NSString *medium1 = @"settings";
    NSString *light2 = @"and tap";
    NSString *medium2 = @"“fans.”";
    NSString *theString = [NSString stringWithFormat:@"%@ %@ %@ %@", light1, medium1, light2, medium2];
    NSMutableAttributedString *bodyAttributedString = [[NSMutableAttributedString alloc] initWithString:theString];
    [bodyAttributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Light" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:(87.0f / 255.0f) green:(66.0f / 255.0f) blue:(63.0f / 255.0f) alpha:1.0f]} range:NSMakeRange(0, light1.length)];
    [bodyAttributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:(87.0f / 255.0f) green:(66.0f / 255.0f) blue:(63.0f / 255.0f) alpha:1.0f]} range:NSMakeRange(light1.length + 1, medium1.length)];
    [bodyAttributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Light" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:(87.0f / 255.0f) green:(66.0f / 255.0f) blue:(63.0f / 255.0f) alpha:1.0f]} range:NSMakeRange(light1.length + 1 + medium1.length + 1, light2.length)];
    [bodyAttributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:(87.0f / 255.0f) green:(66.0f / 255.0f) blue:(63.0f / 255.0f) alpha:1.0f]} range:NSMakeRange(light1.length + 1 + medium1.length + 1 + light2.length + 1, medium2.length)];
    self.body.attributedText = bodyAttributedString;
    self.originalBodyFrame = self.body.frame;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.title.attributedText = [FamilyFailCell attribuedMessage:self.title.text];
    CGFloat titleHeight = [FamilyFailCell titleHeight:self.title.text];
    CGFloat height = [FamilyFailCell height:self.title.text];
    CGFloat delta = fabs(titleHeight - DEFAULT_TITLE_HEIGHT);
    self.title.frame = CGRectMake(self.title.frame.origin.x, self.title.frame.origin.y, self.title.frame.size.width, titleHeight);
    self.body.frame = CGRectMake(self.body.frame.origin.x, self.originalBodyFrame.origin.y + delta, self.body.frame.size.width, self.body.frame.size.height);
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, height);
}

+ (CGFloat)titleHeight:(NSString *)title {
    CGRect rect = [[FamilyFailCell attribuedMessage:title] boundingRectWithSize:CGSizeMake(DEFAULT_TITLE_WIDTH, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return fmaxf(DEFAULT_TITLE_HEIGHT, ceilf(rect.size.height));
}

+ (CGFloat)height:(NSString *)title {
    return (CELL_HEIGHT - DEFAULT_TITLE_HEIGHT) + [FamilyFailCell titleHeight:title];
}

+ (NSMutableAttributedString *)attribuedMessage:(NSString *)title {
    NSMutableAttributedString *titleAttributedString = [[NSMutableAttributedString alloc] initWithString:title];
    [titleAttributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:(238.0f / 255.0f) green:(22.0f / 255.0f) blue:(119.0f / 255.0f) alpha:1.0f]} range:NSMakeRange(0, title.length)];
    return titleAttributedString;
}

@end
