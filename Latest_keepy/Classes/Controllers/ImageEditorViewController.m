//
//  ImageEditorViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 3/30/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

//Filters Definitions
#define kTotalFilters 7
#define kAutoEnhanceSaturation 1.1
#define kAutoEnhanceContrast 1.1
#define kAutoEnhanceBrightness 0.1

#import "ImageEditorViewController.h"
#import "KPOpenCV.h"
#import "ImageCropViewController.h"
#import "ItemEditViewController.h"
#import "GPUImage.h"
#import "KPLocalAsset.h"
#import "KPLocalItem.h"

#import "Keepy-Swift.h"

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)
#define float_epsilon 0.00001;
#define float_equal(a, b) (fabs((a) - (b)) < float_epsilon)

static CGRect CGRectSmallestWithCGPoints(NSArray *pointsArray, int numberOfPoints);

@interface ImageEditorViewController () <ImageCropDelegate, UIScrollViewDelegate> {
    ImageCropViewController *avc;
}


@property(nonatomic, strong) UIView *adjustToolBar;
@property(nonatomic, strong) UIImageView *itemImageView;
@property(nonatomic, strong) UIButton *enhanceBtn;
@property(nonatomic, strong) UIButton *contrastBtn;

@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic, strong) UIImage *sourceImage;
@property(nonatomic, strong) UIImage *croppedImage;
@property(nonatomic, strong) UIImage *resultImage;
@property(nonatomic) BOOL applyCrop;
@property(nonatomic, strong) NSDictionary *imageInfo;
@property(nonatomic) float currentRotationAngle;
@property(nonatomic, strong) NSArray *points;
@property(nonatomic, strong) NSArray *originalPoints;
@property(nonatomic, strong) NSMutableArray *rotatedNewPoints;
@property(nonatomic) int originalAssetId;
@property(nonatomic, strong) ServerComm *serverComm;
@property(nonatomic, strong) UIView *enhancementView;
@property(nonatomic, strong) NSArray *tabs;
@property(nonatomic, strong) NSArray *satValues;
@property(nonatomic, strong) NSArray *conValues;
@property(nonatomic) float currentSaturation;
@property(nonatomic) float currentContrast;
@property(nonatomic) float currentBrightness;
@property(nonatomic) float maxContrastGain;
@property(nonatomic, strong) ItemEditViewController *itemEditVC;
@property(nonatomic, strong) GPUImageSaturationFilter *saturationFilter;
@property(nonatomic, strong) GPUImageContrastFilter *contrastFilter;
@property(nonatomic, strong) GPUImageBrightnessFilter *brightnessFilter;
@property(nonatomic, strong) GPUImageFilter *rotationFilter;
@property(nonatomic, strong) GPUImagePicture *gpuImage;
@property(nonatomic, strong) GPUImageView *gpuView;
@property(nonatomic, strong) GPUImageFilterGroup *filter;
@property(nonatomic, strong) NSString *originalImageMetadata;
@property(nonatomic) BOOL isUploadingOriginalImage;
@property(nonatomic) BOOL isPreProcessingImages;

@property(strong, nonatomic) NSDictionary *originalMetaData;


@property(nonatomic, strong) UIButton *resetBtn;

@property(nonatomic) BOOL wasCropped;
@property(nonatomic) BOOL hasAutoEnhance;
@property (nonatomic) BOOL isUpdateFrame;
@end

@implementation ImageEditorViewController

- (instancetype)initWithItem:(KPLocalItem *)item {
    self = [self initWithNibName:@"ImageEditorViewController" bundle:nil];
    if (self) {
        self.isNewKid = NO;
        self.item = item;
        //DLog(@">>>>>>>>> init item: %@ cropRect '%@'", self.item.itemId,  self.item.cropRect);
    }
    return self;
}

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo applyCrop:(BOOL)applyCrop {
    self = [self initWithNibName:@"ImageEditorViewController" bundle:nil];
    if (self) {
        self.isNewKid = NO;
        self.imageInfo = imageInfo;
        self.applyCrop = applyCrop;
        UIImage * originalImage = (UIImage *) [self.imageInfo valueForKey:UIImagePickerControllerOriginalImage];
        self.sourceImage = [[[UIImage alloc] initWithCGImage:originalImage.CGImage scale:1 orientation:originalImage.imageOrientation] normalizedImage];

        if ([self.imageInfo valueForKey:@"metadata"] != nil) {
            self.originalMetaData = self.imageInfo[@"metadata"];

            if ([NSJSONSerialization isValidJSONObject:[self.imageInfo valueForKey:@"metadata"]]) {
                self.originalImageMetadata = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self.imageInfo valueForKey:@"metadata"] options:NSJSONWritingPrettyPrinted error:NULL] encoding:NSUTF8StringEncoding];
            }
        }

        self.imageInfo = nil;
    }
    return self;
}

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo andPoints:(NSArray *)points {
    self = [self initWithNibName:@"ImageEditorViewController" bundle:nil];
    if (self) {
        self.isNewKid = NO;
        self.imageInfo = imageInfo;
        self.points = self.originalPoints = points;
        self.applyCrop = (points != nil);

        UIImage *orgImage = (UIImage *) [self.imageInfo valueForKey:UIImagePickerControllerOriginalImage];
        if (orgImage == nil) {
            orgImage = [self.imageInfo valueForKey:@"originalImage"];
        }

        self.sourceImage = [[UIImage alloc] initWithCGImage:orgImage.CGImage scale:1 orientation:UIImageOrientationUp];

        if ([self.imageInfo valueForKey:@"metadata"] != nil) {
            if ([NSJSONSerialization isValidJSONObject:[self.imageInfo valueForKey:@"metadata"]]) {
                self.originalImageMetadata = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self.imageInfo valueForKey:@"metadata"] options:NSJSONWritingPrettyPrinted error:NULL] encoding:NSUTF8StringEncoding];
            }
        }

        if ([self.imageInfo valueForKey:@"resultImage"] != nil) {
            self.points = [self.imageInfo valueForKey:@"points"];
            if ([self.points count] == 0) {
                self.points = nil;
            }

            self.originalPoints = self.points;

            self.currentRotationAngle = [[self.imageInfo valueForKey:@"rotation"] floatValue];
            self.currentSaturation = [[self.imageInfo valueForKey:@"saturation"] floatValue];
            self.currentContrast = [[self.imageInfo valueForKey:@"contrast"] floatValue];

            if (self.currentContrast == 0) {
                self.currentContrast = 1;
            }

            if (self.currentSaturation == 0) {
                self.currentSaturation = 1;
            }

            self.resultImage = [self.imageInfo valueForKey:@"resultImage"];
        }

        self.imageInfo = nil;
        DDLogInfo(@"XXXX %2f %2f", self.sourceImage.size.width, self.sourceImage.size.height);
    }
    return self;
}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isNewKid = NO;
        // Custom initialization
    }
    return self;
}

#pragma mark - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([[UserManager sharedInstance] isAfterRegistration]) {
        [[Mixpanel sharedInstance] track:@"onboarding-edit-image-open"];
    }
    
    
    self.title = KPLocalizedString(@"edit photo");
    
    if (self.item == nil) {
        if (self.navigationController.view.tag == 9999) {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"done"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];
        }
        else {
            [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"next"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];
        }
    }
    else {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"done"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];
    }
    
    [self.view setBackgroundColor:UIColorFromRGB(0x5e4f4a)];
    
    self.tabs = [[NSArray alloc] init];
    
    self.satValues = [NSArray arrayWithObjects:
                      [NSDictionary dictionaryWithObjectsAndKeys:@"none", @"title", @(0.0), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"low", @"title", @(0.5), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"medium", @"title", @(1.0), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"high", @"title", @(1.5), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"extreme", @"title", @(2.0), @"value", nil],
                      nil];
    
    self.conValues = [NSArray arrayWithObjects:
                      [NSDictionary dictionaryWithObjectsAndKeys:@"none", @"title", @(0.0), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"low", @"title", @(1.0), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"medium", @"title", @(2.0), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"high", @"title", @(3.0), @"value", nil],
                      [NSDictionary dictionaryWithObjectsAndKeys:@"extreme", @"title", @(4.0), @"value", nil],
                      nil];
    
    
    
    
   

   

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelCurrentUpload:) name:CANCEL_CURRENT_UPLOAD_NOTIFICATION object:nil];
}

- (void)viewDidLayoutSubviews {
    if (self.gpuView != nil) {
        return;
    }
    if (self.isUpdateFrame){
        self.isUpdateFrame = NO;
        //Filters
        //adjustToolBar
        self.adjustToolBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 100)];
        self.adjustToolBar.backgroundColor = UIColorFromRGB(0x5e4f4a);//[UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        [self.view addSubview:self.adjustToolBar];
        
        
        //Line divider
        UIView *lineDivider = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 1)];
        lineDivider.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.200];
        [self.adjustToolBar addCenteredSubview:lineDivider];
        //lineDivider.frameY = self.filtersPageControl.frameBottom + 5;
        
        
        
        //Buttons
        //Common Y position for all buttons
        int btnY = lineDivider.frameBottom + 5;
        
        if (!IS_IPHONE5) {
            lineDivider.frameY -= 10;
            btnY -= 20;
        }
        
        //Enhance
        self.enhanceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.enhanceBtn setImage:[UIImage imageNamed:@"newEnhance"] forState:UIControlStateNormal];
        [self.enhanceBtn setImage:[UIImage imageNamed:@"newEnhance-on"] forState:UIControlStateSelected];
        self.enhanceBtn.selected = NO;
        [self.enhanceBtn addTarget:self action:@selector(autoEnhanceTap) forControlEvents:UIControlEventTouchUpInside];
        self.enhanceBtn.frame = CGRectMake(0, btnY, self.view.frame.size.width / 4, 60);
        self.enhanceBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        
        [self.adjustToolBar addSubview:self.enhanceBtn];
        
        //Crop
        UIButton *frameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [frameBtn setImage:[UIImage imageNamed:@"newCrop"] forState:UIControlStateNormal];
        frameBtn.frame = CGRectMake(self.view.frame.size.width / 4, btnY, self.view.frame.size.width / 4, 60);
        [frameBtn addTarget:self action:@selector(frameTap:) forControlEvents:UIControlEventTouchUpInside];
        frameBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        [self.adjustToolBar addSubview:frameBtn];
        
        //Rotate
        UIButton *rotateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        /*UIImage* flippedImage = [UIImage imageWithCGImage:[UIImage imageNamed:@"newRotate"].CGImage
         scale:[UIImage imageNamed:@"newRotate"].scale
         orientation:UIImageOrientationUpMirrored];*/
        [rotateBtn setImage:[UIImage imageNamed:@"newRotate"] forState:UIControlStateNormal];
        rotateBtn.frame = CGRectMake((self.view.frame.size.width / 4) *2 , btnY, self.view.frame.size.width / 4, 60);
        rotateBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        [rotateBtn addTarget:self action:@selector(rotateTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.adjustToolBar addSubview:rotateBtn];
        
        //Reset
        self.resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.resetBtn setTitle:KPLocalizedString(@"reset") forState:UIControlStateNormal];
        self.resetBtn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
        [self.resetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.resetBtn setTitleColor:[UIColor colorWithWhite:1.000 alpha:0.150] forState:UIControlStateDisabled];
        self.resetBtn.frame = CGRectMake((self.view.frame.size.width / 4)*3, btnY, self.view.frame.size.width / 4, 60);
        self.resetBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        [self.resetBtn addTarget:self action:@selector(resetAction) forControlEvents:UIControlEventTouchUpInside];
        [self.adjustToolBar addSubview:self.resetBtn];
        [self updateResetButton];
    }
    //self.itemImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 50, self.view.bounds.size.width - 20, self.view.bounds.size.height - 90/1.5 - 50)];
    //[self.itemImageView setContentMode:UIViewContentModeScaleAspectFit];
    //self.itemImageView.image = self.sourceImage;
    //[self.view addSubview:self.itemImageView];

/////

    self.filter = [[GPUImageFilterGroup alloc] init];

    self.saturationFilter = [[GPUImageSaturationFilter alloc] init];
    self.contrastFilter = [[GPUImageContrastFilter alloc] init];
    self.brightnessFilter = [[GPUImageBrightnessFilter alloc] init];
    self.rotationFilter = [[GPUImageFilter alloc] init];

    [(GPUImageFilterGroup *) self.filter addFilter:self.rotationFilter];
    [(GPUImageFilterGroup *) self.filter addFilter:self.saturationFilter];
    [(GPUImageFilterGroup *) self.filter addFilter:self.contrastFilter];
    [(GPUImageFilterGroup *) self.filter addFilter:self.brightnessFilter];

    [(GPUImageFilterGroup *) self.filter setInitialFilters:[NSArray arrayWithObject:self.rotationFilter]];
    [(GPUImageFilterGroup *) self.filter setTerminalFilter:self.brightnessFilter];

    self.gpuView = [[GPUImageView alloc] initWithFrame:CGRectMake(27, 94, self.view.frameWidth - 54, self.view.frameHeight - 60 - 15 - 94 - 46)];

    //self.view.bounds.size.height - 60 - 74 - 15
    //self.gpuView.backgroundColor = UIColorFromRGB(0x5e4f4a);
    self.gpuView.backgroundColor = [UIColor clearColor];
    [self.gpuView setBackgroundColorRed:0 green:0 blue:0 alpha:0];
    [self.view addSubview:self.gpuView];

    self.adjustToolBar.frameY = self.gpuView.frameBottom;

    if (!IS_IPHONE5) {
        self.adjustToolBar.frameY = self.view.frameHeight - self.adjustToolBar.frameHeight - 2;
    }

    if (self.item != nil && self.resultImage == nil) {

        [SVProgressHUD showWithStatus:KPLocalizedString(@"downloading original image")];

        //KPLocalAsset *originalImage = [KPLocalAsset fetchByLocalID:self.item.originalImageAssetID];
        KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];
        
//        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_medium.jpg", [[KPDefaults sharedDefaults] assetsURLString], itemImage.urlHash]]];
        
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, itemImage.urlHash]] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {

            [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"downloading original image")];
        }                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {


            //We decide to reset to the original when the photo is edited
            /*
            
            self.points = [GlobalUtils getPointsFromCropRectStr:self.item.cropRect];
            
           // DLog(@">>>>>>>>>>> loaded points %@", self.points);
            if ([self.points count] == 0) {
                self.points = nil;
            }
            
            
            
            self.originalPoints = self.points;
            
            self.currentRotationAngle = self.item.rotationValue;
            self.currentSaturation = self.item.satValue;
            self.currentContrast = self.item.conValue;
            self.currentRotationAngle = self.item.rotationValue;
            self.currentBrightness = self.item.briValue;
            
       

            BOOL saturationIsEqual = [[NSString stringWithFormat:@"%.2f", self.currentSaturation] floatValue] == [[NSString stringWithFormat:@"%.2f", kAutoEnhanceSaturation] floatValue];
            BOOL brightnessIsEqual = [[NSString stringWithFormat:@"%.2f", self.currentBrightness] floatValue] == [[NSString stringWithFormat:@"%.2f", kAutoEnhanceBrightness] floatValue];
            BOOL contrastIsEqual = [[NSString stringWithFormat:@"%.2f", self.currentContrast] floatValue] == [[NSString stringWithFormat:@"%.2f", kAutoEnhanceContrast] floatValue];

         
            
            self.enhanceBtn.selected = (saturationIsEqual && contrastIsEqual && brightnessIsEqual);
             
           // [self applyFilter:self.item.filterId.intValue reloadImage:false];
            
          //  self.enhanceBtn.selected = self.item.autoEnhanceOnValue;
            
            if (self.currentContrast == 0)
                self.currentContrast = 1;
            
            if (self.currentSaturation == 0)
                self.currentSaturation = 1;
             
             */


            self.currentSaturation = 1.0;
            self.currentBrightness = 0;
            self.currentContrast = 1.0;
            self.enhanceBtn.selected = NO;
            self.currentRotationAngle = 0;
            self.points = nil;


            if (image == nil) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"could not download original image")];
                return;
            }
            self.sourceImage = image;
            //self.croppedImage = [self.sourceImage copy];
            //[self setupGPUImage];

            self.applyCrop = (self.points != nil);

            [self execCrop];
            [SVProgressHUD dismiss];
        }];
    }
    else {
        if (self.resultImage == nil) {
            self.currentSaturation = 1.0;
            self.currentContrast = 1.0;
            self.currentBrightness = 0.0;
            self.currentRotationAngle = 0.0;

            [self execCrop];
        }
        else {

            [self execCrop];
        }
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    /*
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0xd7ff46);
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    //self.navigationController.navigationBar.translucent = NO;
    */
    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"image-editor", @"source", nil]];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.isUpdateFrame = YES;
    [self viewDidLayoutSubviews];
 
    
    
    if(!self.item) {
        
        self.navigationItem.rightBarButtonItem.enabled = self.rightButtonEnabled;

    } else {
        
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
    if (self.isMovingFromParentViewController) {
        [self cancelCurrentUpload:nil];
    }
}

- (void)willMoveToParentViewController:(UIViewController *)parent {
    if (!parent) {
        [self cancelCurrentUpload:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setResultImage:(UIImage *)resultImage {
    _resultImage = resultImage;
    //self.itemImageView.image = resultImage;


    /*
    [UIView transitionWithView:self.itemImageView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.itemImageView.image = resultImage;
                        
                    } completion:^(BOOL finished) {                        
                    }];
    */
}




- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

#pragma mark - Image Manipulation


- (void)execCrop {
    if (self.applyCrop) {

        [[Mixpanel sharedInstance] registerSuperProperties:[NSDictionary dictionaryWithObjectsAndKeys:@"Auto", @"Crop Type", nil]];

        [[Mixpanel sharedInstance] registerSuperProperties:[NSDictionary dictionaryWithObjectsAndKeys:@"Success", @"Processing Result", nil]];


        //[SVProgressHUD showWithStatus:KPLocalizedString(@"processing")];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{

            // NSDictionary *cropResultData = [KPOpenCV kpCropImage:self.sourceImage withPoints:self.points];

            if (self.croppedImage == nil) {
                self.croppedImage = [self.sourceImage imageRotatedByDegrees:self.currentRotationAngle];
            }

            NSDictionary *cropResultData = [KPOpenCV kpCropImage:self.croppedImage withPoints:self.points];


            self.points = (NSArray *) [cropResultData valueForKey:@"points"];
            self.croppedImage = (UIImage *) [cropResultData valueForKey:@"image"];

            if (self.currentRotationAngle != 0) {
                //we need to rotate the cropped image back to the original position
                self.croppedImage = [self.croppedImage imageRotatedByDegrees:self.currentRotationAngle * -1];
            }

            if (self.item != nil) {
                //      self.sourceImage = nil;
            }

            [self setupGPUImage];

            dispatch_async(dispatch_get_main_queue(), ^{
                // [SVProgressHUD dismiss];
                // self.gpuView.hidden = false;
            });
        });

        if (self.item == nil) {
            [self uploadOriginalImage];
        }

    }
    else {
        self.croppedImage = [UIImage imageWithCGImage:[self.sourceImage CGImage]];
        [self setupGPUImage];
        if (self.item == nil) {
            [self uploadOriginalImage];
        }
        else {
            self.sourceImage = nil;
        }
        // self.gpuView.hidden = false;
    }

}

- (void)setupGPUImage {

    dispatch_async(dispatch_get_main_queue(), ^{
        //[SVProgressHUD showWithStatus:KPLocalizedString(@"processing")];
    });

    //Update the reset button
    [self updateResetButton];

    UIImage *tmpImage;

    if (self.croppedImage) {
        tmpImage = self.croppedImage;
    } else {
        tmpImage = self.sourceImage;
    }


    /*
    //Apply selected filter
    NSString *effectName = nil;
    switch (self.currentFilterIndex) {
        case 1:
            effectName = @"CIPhotoEffectChrome";
            break;
        case 2:
            effectName = @"CIPhotoEffectProcess";
            break;
        case 3:
            effectName = @"CIPhotoEffectTransfer";
            break;
        case 4:
            effectName = @"CIPhotoEffectInstant";
            break;
        case 5:
            effectName = @"CIPhotoEffectFade";
            break;
        case 6:
            effectName = @"CIPhotoEffectTonal";
            break;
        default:
            break;
    }
    
    if(effectName) {
        CIImage *_inputImage = [CIImage imageWithCGImage:[tmpImage CGImage]];
        CIFilter *filter = [CIFilter filterWithName:effectName];
        [filter setValue:_inputImage forKey:kCIInputImageKey];
        
        CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:filter.outputImage fromRect:filter.outputImage.extent];
        
        tmpImage= [UIImage imageWithCGImage:cgImage];
    }
     
     */

    //Auto Enhance
    if (self.hasAutoEnhance) {
        self.currentSaturation = kAutoEnhanceSaturation;
        self.currentContrast = kAutoEnhanceContrast;
        self.currentBrightness = kAutoEnhanceBrightness;
    } else {
        //reset to default values
        self.currentSaturation = 1.0;
        self.currentBrightness = 0;
        self.currentContrast = 1.0;

    }


    [self.saturationFilter setSaturation:self.currentSaturation];
    [self.contrastFilter setContrast:self.currentContrast];
    [self.brightnessFilter setBrightness:self.currentBrightness];

    //DLog(@"self.currentRotationAngle %f", self.currentRotationAngle);
    GPUImageRotationMode m = kGPUImageNoRotation;
    if (self.currentRotationAngle == 270) {
        m = kGPUImageRotateLeft;
    } else if (self.currentRotationAngle == 180) {
        m = kGPUImageRotate180;
    } else if (self.currentRotationAngle == 90) {
        m = kGPUImageRotateRight;
    }


    self.gpuImage = [[GPUImagePicture alloc] initWithImage:[tmpImage scaleProportionalToSize:CGSizeMake(640, 1136)] smoothlyScaleOutput:YES];

    //self.croppedImage = nil;

    tmpImage = nil;

    [self.gpuImage addTarget:self.filter];

    [self.rotationFilter addTarget:self.saturationFilter];
    [self.saturationFilter addTarget:self.contrastFilter];
    [self.contrastFilter addTarget:self.brightnessFilter];
    [self.brightnessFilter addTarget:self.gpuView];

    [self.rotationFilter setInputRotation:m atIndex:0];

    //dispatch_async(dispatch_get_main_queue(), ^{
    [self.gpuImage processImageWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            self.enhanceBtn.selected = self.hasAutoEnhance;
            self.hasAutoEnhance = false;
            [self updateResetButton];
            self.gpuView.hidden = false;
        });
    }];
    //});


    //});
}

#pragma mark - Actions

- (void)topBtnTap:(UIButton *)sender {

    sender.enabled = NO;
    if (sender.tag == 1) //Ok
    {
        if (self.originalAssetId == 0 && self.item == nil && self.navigationController.view.tag != 9999) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];
            sender.enabled = YES;
            return;
        }

        //self.resultImage = [UIImage imageWithCGImage:[[self.brightnessFilter imageFromCurrentlyProcessedOutput] CGImage]];
        //self.resultImage = [self.brightnessFilter imageFromCurrentlyProcessedOutput];
        /*
        NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], [NSString stringWithFormat:@"test.jpg"]];
        NSData* imageData = UIImageJPEGRepresentation(self.resultImage, 0.8);
        [imageData writeToFile:tmpPath atomically:NO];
        */

        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"image-editor", @"source", @"ok", @"result", nil]];

        //[[NSNotificationCenter defaultCenter] postNotificationName:@"MAIPCSuccess" object:d];

        [self.brightnessFilter useNextFrameForImageCapture];
        [self.gpuImage processImage];

        NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
        [result setValue:[self.brightnessFilter imageFromCurrentFramebuffer] forKey:@"resultImage"];

        [result setValue:self.points forKey:@"points"];

        //  DLog(@">>>>>>>>>>>>> saving points %@", self.points);
        //[result setValue:@(self.enhanceBtn.selected) forKey:@"filter"];
        [result setValue:@(self.currentSaturation) forKey:@"saturation"];
        [result setValue:@(self.currentContrast) forKey:@"contrast"];
        [result setValue:@(self.currentBrightness) forKey:@"brightness"];
        [result setValue:@(self.currentRotationAngle) forKey:@"rotation"];
        [result setValue:@(self.originalAssetId) forKey:@"assetId"];
        // [result setValue:[NSNumber numberWithBool:self.hasAutoEnhance] forKeyPath:@"autoEnhanceOn"];

        //save filter
        // [result setValue:@(self.currentFilterIndex) forKey:@"filterId"];
        //save autoenhance
        [result setValue:@(self.enhanceBtn.selected) forKey:@"autoEnhanceOn"];

        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        if (_rotatedNewPoints) {
            [dict setValue:@YES forKey:@"rotateFirst"];
        }
        else {
            [dict setValue:@NO forKey:@"rotateFirst"];
        }
        [dict setValue:@(self.sourceImage.size.width) forKey:@"originalWidth"];
        [dict setValue:@(self.sourceImage.size.height) forKey:@"originalHeight"];
        [result setValue:dict forKey:@"extraParams"];

        if (self.originalMetaData) {
            // NSError *jsonError;
            // NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.originalMetaData options:0 error:&jsonError];
            // result[@"extraData"] = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            result[@"originalMetadata"] = self.originalMetaData;
        }

        if (self.originalImageMetadata != nil) {
            [result setValue:self.originalImageMetadata forKey:@"extraData"];
        }

        if (self.sourceImage != nil) {
            [result setValue:self.sourceImage forKey:@"originalImage"];
        }
        //[result setValue: forKey:@"itemDate"];



        //DDLogInfo(@"%@",result);

        if (self.item != nil) {
            [self.delegate imageEditorFinishedWithResult:result];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            if (self.navigationController.view.tag == 9999) {
                [result setValue:@(self.navigationController.view.tag - 10000) forKey:@"indexInGroup"];
                [self.delegate imageEditorFinishedWithResult:result];
                [self dismissViewControllerAnimated:YES completion:^{

                }];
            }
            else {
                if (self.itemEditVC == nil) {
                    self.itemEditVC = [[ItemEditViewController alloc] initWithImageInfo:result];
                } else {
                    [self.itemEditVC refreshImageInfo:result];
                }
                self.itemEditVC.isNewKid = self.isNewKid;
                [self.navigationController pushViewController:self.itemEditVC animated:YES];
            }
        }
    }
    else {

        [[Mixpanel sharedInstance] track:@"Image editor back tap"];


        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"image-editor", @"source", @"cancel", @"result", nil]];

        if (self.serverComm != nil) {
            [self.serverComm cancelOperation];
            [GlobalUtils updateAssetUpload:self.originalAssetId totalBytesWritten:0 totalBytesExpectedToWrite:0];

        }

        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)enhanceTap:(UIButton *)sender {
    //sender.selected = (!sender.selected);

    self.hasAutoEnhance = (!sender.selected);
    if (sender.selected) {
        if (sender.tag == 0) {
            self.contrastBtn.selected = NO;
        } else {
            self.enhanceBtn.selected = NO;
        }
    }
    int direction = 1;
    float ay = self.view.bounds.size.height - 90 / 1.5 - 65;
    if (sender.tag == 1) {
        ay -= 65;
    }

    if (self.enhancementView.superview != nil && self.enhancementView.tag == sender.tag) {
        direction = -1;
        ay = 500;

        self.itemImageView.hidden = NO;
    }
    else {
        if (self.enhancementView.superview != nil) {
            direction = 0;
        }
        else {
        }

        self.enhancementView.tag = sender.tag;
    }

    float v = self.currentSaturation / 4.0;
    NSString *s = KPLocalizedString(@"saturation");
    if (sender.tag == 1) {
        s = KPLocalizedString(@"contrast");
        v = self.currentContrast / 2.0;
    }

    [(UILabel *) [self.enhancementView.subviews objectAtIndex:1] setText:s];
    [(UISlider *) [self.enhancementView.subviews objectAtIndex:0] setValue:v];
    [(UISlider *) [self.enhancementView.subviews objectAtIndex:2] setValue:(self.currentBrightness + 1) / 2];

    if (self.enhancementView.superview == nil) {
        //[self.view addSubview:self.enhancementView];
        //[self.view sendSubviewToBack:self.enhancementView];
        [self.view insertSubview:self.enhancementView aboveSubview:self.gpuView];
    }

    self.tabs = (sender.tag == 0) ? self.satValues : self.conValues;

    //[self enhancementScaleTap:nil];

    [UIView animateWithDuration:0.5 animations:^{

        CGRect aframe = self.enhancementView.frame;
        aframe.origin.y = ay;
        self.enhancementView.frame = aframe;
        /*
        CGRect aframe = self.gpuView.frame;
        aframe.size.height -= 50 * direction;
        //self.itemImageView.frame = aframe;
        self.gpuView.frame = aframe;*/
    }                completion:^(BOOL finished) {
        if (direction == -1) {
            [self.enhancementView removeFromSuperview];
        }
        else {

        }

    }];


}

- (void)rotateTap:(id)sender {


    self.currentRotationAngle += 270;// 90;

    /*   if (self.currentRotationAngle == 270) {
           self.currentRotationAngle += 270;
       }*/

    if (self.currentRotationAngle >= 360) {
        self.currentRotationAngle = self.currentRotationAngle - 360;
    }

    //DLog(@"self.currentRotationAngle %f", self.currentRotationAngle);
    GPUImageRotationMode m = kGPUImageNoRotation;
    if (self.currentRotationAngle == 270) {
        m = kGPUImageRotateLeft;
    } else if (self.currentRotationAngle == 180) {
        m = kGPUImageRotate180;
    } else if (self.currentRotationAngle == 90) {
        m = kGPUImageRotateRight;
    }

    [self.rotationFilter setInputRotation:m atIndex:0];
    [self.gpuImage processImage];

    [self updateResetButton];


    /*
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self setupGPUImage];
    });*/
}

- (void)frameTap:(id)sender {
    if (self.item != nil && self.sourceImage == nil) {

        [SVProgressHUD showWithStatus:KPLocalizedString(@"downloading original image")];

        KPLocalAsset *originalImage = [KPLocalAsset fetchByLocalID:self.item.originalImageAssetID];

        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, originalImage.urlHash]] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {

            [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"downloading original image")];
        }                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {

            if (image == nil) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"could not download original image")];
                return;
            }

            self.sourceImage = image;
            if (self.points == nil) {
                self.croppedImage = [UIImage imageWithCGImage:[self.sourceImage CGImage]];
            }
            else {
                self.applyCrop = YES;
                [self execCrop];
            }
            [SVProgressHUD dismiss];
            [self frameTap:nil];

        }];

        return;
    }


    [[Mixpanel sharedInstance] track:@"crop tap"];


    //ImageCropViewController *avc = nil;
    //if (self.item != nil || self.navigationController.view.tag >= 9999)
    //{
    //  avc = [[ImageCropViewController alloc] initWithImage:self.sourceImage andPoints:self.points];
    //}
    //else
    //{
    //    avc = [[ImageCropViewController alloc] initWithImage:self.sourceImage andPoints:[NSArray arrayWithObject:@(-1)]];
    //}

    //if(avc == nil) {
    //avc = [[ImageCropViewController alloc] initWithImage:self.sourceImage andPoints:self.points];


    /* UIImage *currentImage = self.sourceImage;
     UIImage * correctImage ;
     DLog(@"self.currentRotationAngle %f",                self.currentRotationAngle);
     //rotate the cropped image back to 0
     if (self.currentRotationAngle == 270) {
         correctImage = [[UIImage alloc] initWithCGImage: currentImage.CGImage scale: 1.0 orientation: UIImageOrientationLeft];
     }
     else if (self.currentRotationAngle == 180) {
         correctImage = [[UIImage alloc] initWithCGImage: currentImage.CGImage scale: 1.0 orientation: UIImageOrientationDown];
     }
     else if (self.currentRotationAngle == 90) {
         correctImage = [[UIImage alloc] initWithCGImage: currentImage.CGImage scale: 1.0 orientation: UIImageOrientationRight];
     }*/


    UIImage *currentImage = [UIImage imageWithCGImage:self.sourceImage.CGImage];
    currentImage = [currentImage imageRotatedByDegrees:self.currentRotationAngle];

    avc = [[ImageCropViewController alloc] initWithImage:currentImage andPoints:self.points];
    avc.delegate = self;
    //}


    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromBottom;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:avc animated:NO];
}

- (CGPoint)translatePoint:(CGFloat)rotatedX :(CGFloat)rotatedY :(CGFloat)ox :(CGFloat)oy :(CGFloat)rotation_ {

    CGFloat theta = DEGREES_TO_RADIANS(rotation_);

    CGFloat newX = cosf(theta) * (rotatedX - ox) - sinf(theta) * (rotatedY - oy) + ox;
    CGFloat newY = sinf(theta) * (rotatedX - ox) + cosf(theta) * (rotatedY - oy) + oy;

    return CGPointMake(newX, newY);
}


- (CGPoint)translateSinglePoint:(CGFloat)rotatedX :(CGFloat)rotatedY :(CGFloat)originalWidth :(CGFloat)originalHeight :(CGFloat)rotation_ {

    CGFloat rotation = DEGREES_TO_RADIANS(rotation_);

    CGFloat originalX = (rotatedX - originalWidth / 2) * cos(rotation) - (rotatedY - originalHeight / 2) * sin(rotation) + originalWidth / 2;
    CGFloat originalY = (rotatedX - originalWidth / 2) * sin(rotation) + (rotatedY - originalHeight / 2) * cos(rotation) + originalHeight / 2;

    CGFloat nearestX = fabsf([[NSString stringWithFormat:@"%.2f", originalX] floatValue]);
    CGFloat nearestY = fabsf([[NSString stringWithFormat:@"%.2f", originalY] floatValue]);


    return CGPointMake(nearestX, nearestY);
}


#pragma mark - ImageCropDelegate

- (void)cropRectSelected:(NSArray *)newPoints withImage:(UIImage *)image {


    //DDLogInfo(@"%@",newPoints);

    self.gpuView.hidden = true;
    int64_t delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        //self.sourceImage = image;


        if (self.currentRotationAngle != 0) {

            if (self.currentRotationAngle == 270) {
                _rotatedNewPoints = [[NSMutableArray alloc] initWithCapacity:3];

                for (int i = 0; i < 4; i++) {
                    [_rotatedNewPoints addObject:[NSNull null]];
                }

                for (int i = 0; i < newPoints.count; i++) {

                    CGPoint originalPoint = [[newPoints objectAtIndex:i] CGPointValue];
                    CGFloat originalWidth = image.size.width;
                    CGFloat originalHeight = image.size.height;

                    CGFloat a = DEGREES_TO_RADIANS(270);
                    CGFloat dx = originalWidth / 2;
                    CGFloat dy = originalHeight / 2;

                    CGAffineTransform transform = CGAffineTransformMakeTranslation(-dx, -dy);
                    transform = CGAffineTransformRotate(transform, a);
                    //transform = CGAffineTransformScale(transform, 1, -1);
                    transform = CGAffineTransformTranslate(transform, dx, dy);

                    /*if (originalPoint.x > originalWidth/2.0f) {
                        originalPoint.x = originalWidth - originalPoint.x;
                    }
                    if (originalPoint.y > originalHeight/2.0f) {
                        originalPoint.y = originalHeight - originalPoint.y;
                    }*/

                    CGAffineTransform t; // = CGAffineTransformMake(cos(a),sin(a),-sin(a),cos(a),dx-dx*cos(a)+dy*sin(a),dy-dx*sin(a)-dy*cos(a));
                    //t = CGAffineTransformInvert(t);
                    //t = CGAffineTransformRotate(t,DEGREES_TO_RADIANS(270));
                    t = CGAffineTransformMakeRotation(a);
                    CGPoint point = CGPointApplyAffineTransform(originalPoint, t);
                    point = CGPointMake(fabs(point.x), fabs(point.y));

                    if (point.x > originalHeight / 2.0f) {
                        point.x = originalHeight - point.x;
                    }
                    if (point.y > originalWidth / 2.0f) {
                        point.y = originalWidth - point.y;
                    }

                    //point = CGPointApplyAffineTransform(originalPoint, CGAffineTransformMakeTranslation(-originalHeight/originalWidth,-originalHeight/originalWidth));
                    //[_rotatedNewPoints replaceObjectAtIndex: newIndex withObject:[NSValue valueWithCGPoint: point]];
                    [_rotatedNewPoints replaceObjectAtIndex:i withObject:[NSValue valueWithCGPoint:point]];
                }

                CGRect originalRect = CGRectSmallestWithCGPoints(newPoints, 4);
                CGRect rect = CGRectSmallestWithCGPoints(_rotatedNewPoints, 4);
                [_rotatedNewPoints replaceObjectAtIndex:0 withObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x, rect.origin.y)]];
                [_rotatedNewPoints replaceObjectAtIndex:3 withObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x + originalRect.size.height, rect.origin.y)]];
                [_rotatedNewPoints replaceObjectAtIndex:2 withObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x + originalRect.size.height, rect.origin.y + originalRect.size.width)]];
                [_rotatedNewPoints replaceObjectAtIndex:1 withObject:[NSValue valueWithCGPoint:CGPointMake(rect.origin.x, rect.origin.y + originalRect.size.width)]];
            }
            else {
                _rotatedNewPoints = [[NSMutableArray alloc] initWithCapacity:3];

                for (int i = 0; i < 4; i++) {
                    [_rotatedNewPoints addObject:[NSNull null]];
                }

                float originalWidth = image.size.width;
                float originalHeight = image.size.height;

                for (int i = 0; i < newPoints.count; i++) {

                    CGPoint originalPoint = [[newPoints objectAtIndex:i] CGPointValue];

                    int indexOffset = self.currentRotationAngle / 90;
                    if (self.currentRotationAngle == 270) {
                        //indexOffset = 1;
                    }

                    int newIndex = (i + indexOffset) % 4;

                    DDLogInfo(@"old point: %@ new point: %@", NSStringFromCGPoint(originalPoint), NSStringFromCGPoint([self translateSinglePoint:originalPoint.x :originalPoint.y :originalWidth :originalHeight :self.currentRotationAngle]));
                    [_rotatedNewPoints replaceObjectAtIndex:newIndex withObject:[NSValue valueWithCGPoint:[self translateSinglePoint:originalPoint.x :originalPoint.y :originalWidth :originalHeight :self.currentRotationAngle]]];
                }
            }

        }


        self.points = newPoints;
        self.croppedImage = image;

        self.applyCrop = (newPoints != nil);
        [self execCrop];


        self.wasCropped = (newPoints != nil);

        [self updateResetButton];
    });
}

#pragma mark - Upload Original Image

- (void)uploadOriginalImage {
    if (self.navigationController.view.tag == 9999) {
        return;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(internetConnectionRevived:) name:INTERNET_CONNECTION_REVIVED_NOTIFICATION object:nil];

    if (self.serverComm == nil) {
        self.serverComm = [ServerComm instance];
    }

    //DLog(@">>>>>>>>> self.isPreProcessingImages %d", self.isPreProcessingImages);
    //if(!self.isPreProcessingImages) [SVProgressHUD showWithStatus:KPLocalizedString(@"connecting to keepy\'s servers")];

    __weak ImageEditorViewController *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

        weakSelf.isUploadingOriginalImage = YES;


        [[ServerComm instance] prepareAsset:^(id result) {
            int assetId = 0;
            if ([[result valueForKey:@"result"] valueForKey:@"id"] != [NSNull null]) {
                assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] intValue];
            }

            if (assetId > 0) {
                [[SDImageCache sharedImageCache] storeImage:weakSelf.sourceImage forKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, [[result valueForKey:@"result"] valueForKey:@"hash"]]];

                dispatch_async(dispatch_get_main_queue(), ^{
                    // if(!weakSelf.isPreProcessingImages )[SVProgressHUD dismiss];
                });

                weakSelf.rightButtonEnabled = YES;
                weakSelf.originalAssetId = assetId;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                
                //TODO: the compression was removed
                NSData *data = UIImageJPEGRepresentation(weakSelf.sourceImage, 1);

                DDLogInfo(@"original image size %@", NSStringFromCGSize(weakSelf.sourceImage.size));

                NSLog(@"FNTRACE ImageEditorViewCOntroller");
                
                [weakSelf.serverComm uploadAsset:[[result valueForKey:@"result"] valueForKey:@"hash"]
                                        withData:data
                                       andPolicy:[[result valueForKey:@"result"] valueForKey:@"policy"] andSignature:[[result valueForKey:@"result"] valueForKey:@"policyHash"] withSuccessBlock:^(id result) {

                            [weakSelf.serverComm commitAsset:assetId andFileSize:data.length withSuccessBlock:^(id result) {

                                weakSelf.isUploadingOriginalImage = NO;
                                DDLogInfo(@"Uploaded...");
                            }                   andFailBlock:^(NSError *error) {

                            }                      onHandler:nil];

                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

                        }           andFailBlock:^(NSError *error) {
                            [GlobalUtils updateAssetUpload:assetId totalBytesWritten:0 totalBytesExpectedToWrite:0];

                            if (error != nil && error.code != NSURLErrorCancelled) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading original image")];
                                });
                            }

                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                        }             onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                            //float p = ((float)totalBytesWritten / (float)totalBytesExpectedToWrite);
                            [GlobalUtils updateAssetUpload:assetId totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
                            //DDLogInfo(@"uploading:%2f", 100 * p);

                        }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];
                });
            }
        }                      andFailBlock:^(NSError *error) {

            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"we can\'t establish connection to keepy\'s servers. are you connected to the internet?")];
            });

            //[SVProgressHUD showError:@"error contacting keepy"];
        }];
    });
}

- (void)enhancementSliderChange:(UISlider *)sender {
    float v = 0;
    if (self.enhancementView.tag == 0) {
        v = sender.value * 4.0;
        if (v == 0) {v = 0.01;}

        self.currentSaturation = v;
    }
    else {
        if (sender.tag == 1) {
            v = -1 + sender.value * 2.0;
            self.currentBrightness = v;
        }
        else {
            v = sender.value * 2.0;
            if (v == 0) {v = 0.01;}

            self.currentContrast = v;
        }
    }

    if (self.currentSaturation > 0) {
        [self.saturationFilter setSaturation:self.currentSaturation];
    }

    if (self.currentContrast > 0) {
        [self.contrastFilter setContrast:self.currentContrast];
    }

    if (self.currentBrightness >= -1) {
        [self.brightnessFilter setBrightness:self.currentBrightness];
    }

    [self.gpuImage processImage];
}


- (void)cancelCurrentUpload:(NSNotification *)notification {
    if (self.serverComm != nil) {
        [self.serverComm cancelOperation];
        [GlobalUtils updateAssetUpload:self.originalAssetId totalBytesWritten:0 totalBytesExpectedToWrite:0];

    }
}

- (void)internetConnectionRevived:(NSNotification *)notification {
    if (self.isUploadingOriginalImage) {
        [self uploadOriginalImage];
    }
}


- (void)autoEnhanceTap {
    self.hasAutoEnhance = (!self.enhanceBtn.selected);
    /*
     if(self.enhanceBtn.selected) {
         self.currentSaturation = 0.5;
         self.currentContrast  = 0.5;
         self.currentBrightness = 0.5;

     } else {
         self.currentSaturation = 1.0;
         self.currentContrast  = 1.0;
         self.currentBrightness = 0;

         //return original image
         self.croppedImage = nil;
     }*/
    [self setupGPUImage];
}

#pragma mark - UI

- (void)updateResetButton {
    // DLog(@"updateResetButton");
    //  BOOL hasFilter = (self.currentFilterIndex != 0);
    BOOL hasAutoEnhance = self.enhanceBtn.selected;
    BOOL hasRotation = self.currentRotationAngle != 0;
    if (hasAutoEnhance || hasRotation || self.wasCropped) {
        self.resetBtn.enabled = true;
    } else {
        self.resetBtn.enabled = false;
    }
}

- (void)resetAction {
    //Filter
    // [self applyFilter:0];

    self.points = self.originalPoints;
    self.wasCropped = false;

    //reset Crop
    self.croppedImage = nil;

    //Auto Enhance
    self.enhanceBtn.selected = false;

    //Rotate
    self.currentRotationAngle = 0;


    //Recreate the Photo
    [self setupGPUImage];
}


@end

CGRect CGRectSmallestWithCGPoints(NSArray *pointsArray, int numberOfPoints) {
    CGFloat greatestXValue = [pointsArray[0] CGPointValue].x;
    CGFloat greatestYValue = [pointsArray[0] CGPointValue].y;
    CGFloat smallestXValue = [pointsArray[0] CGPointValue].x;
    CGFloat smallestYValue = [pointsArray[0] CGPointValue].y;
    
    for (int i = 1; i < numberOfPoints; i++) {
        CGPoint point = [pointsArray[i] CGPointValue];
        greatestXValue = MAX(greatestXValue, point.x);
        greatestYValue = MAX(greatestYValue, point.y);
        smallestXValue = MIN(smallestXValue, point.x);
        smallestYValue = MIN(smallestYValue, point.y);
    }
    
    CGRect rect;
    rect.origin = CGPointMake(smallestXValue, smallestYValue);
    rect.size.width = greatestXValue - smallestXValue;
    rect.size.height = greatestYValue - smallestYValue;
    
    return rect;
}
