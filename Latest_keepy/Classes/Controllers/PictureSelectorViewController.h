//
//  PictureSelectorViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//
@import Photos;
@protocol PictureSelectorViewControllerDelegate;

@interface PictureSelectorViewController : UIViewController

@property(nonatomic, weak) id <PictureSelectorViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *pictures;

@end

@protocol PictureSelectorViewControllerDelegate <NSObject>

- (void)albums:(id)sender;

- (void)pictureSelected:(id)sender;

@end

@protocol PictureCellDelegate;

@interface PictureCell : UITableViewCell

@property(nonatomic, weak) id <PictureCellDelegate> delegate;
@property(nonatomic, strong) NSArray *pictures;

+ (CGFloat)height;

- (void)layout;

@end

@protocol PictureCellDelegate <NSObject>

//- (void)pictureSelected:(ALAsset *)asset;
- (void)pictureSelected:(PHAsset *)asset;

@end
