//
//  AlbumSelector2ViewController.h
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@import Photos;

@protocol AlbumSelector2ViewControllerDelegate;

@interface AlbumSelector2ViewController : UIViewController

@property(nonatomic, weak) id <AlbumSelector2ViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray <PHAssetCollection*> *albums;

@end

@protocol AlbumSelector2ViewControllerDelegate <NSObject>

- (void)pictures:(PHCollection *)collection;

@end

@interface AlbumCell2 : UITableViewCell

@property(nonatomic, strong) PHAssetCollection *collection;

+ (CGFloat)height;

- (void)layout;

@end
