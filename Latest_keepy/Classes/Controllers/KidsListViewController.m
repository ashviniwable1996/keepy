//
//  KidsListViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 4/2/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KidsListViewController.h"
#import "EditKidViewController.h"
#import "KPLocalKid.h"
#import "KPLocalAsset.h"
#import "AgeCalculator.h"

#import "Keepy-Swift.h"

@interface KidsListViewController () <EditKidDelegate>

@property(nonatomic, strong) UIScrollView *scrollView;

@end

@implementation KidsListViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kidsRefreshed:) name:KIDS_REFRESHED_NOTIFICATION object:nil];


    [[Mixpanel sharedInstance] track:@"Kids List"];


    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-kids", @"source", nil]];


    UIColor *patternColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
    self.view.backgroundColor = patternColor;

    self.title = KPLocalizedString(@"my kids");
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:@"settings", @"title", @(BUTTON_TYPE_BACK), @"buttonType", @(NO), @"disabled", nil] andRightButtonInfo:nil];

    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.scrollView.showsVerticalScrollIndicator = NO;
    //self.scrollView.contentInset = UIEdgeInsetsMake(54, 0, 0, 0);
    [self.view addSubview:self.scrollView];

    [self fetchData];
}
-(void)viewDidLayoutSubviews{
    self.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchData {
    for (UIControl *control in self.scrollView.subviews) {
        [control removeFromSuperview];
    }

    __block float ax = 40;
    __block float ay = 10;
    if([UIScreen mainScreen].bounds.size.width == 320){
        ax = 20;
    } else if([UIScreen mainScreen].bounds.size.width == 414){
        ax = 65;
    }
    void(^createButtonBlock)(KPLocalKid *kid) = ^(KPLocalKid *kid) {

        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(ax, ay, 80, 80);
        if (kid != nil) {
            //[btn setBackgroundImage:[UIImage imageNamed:@"kidPicBg"] forState:UIControlStateNormal];
        }
        else {
            [btn setBackgroundImage:[UIImage imageNamed:@"settings-add-kid"] forState:UIControlStateNormal];
        }
        [btn addTarget:self action:@selector(kidTap:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = -1;
        if (kid != nil) {
            btn.tag = kid.serverID;
            [btn setTitle:[NSString stringWithFormat:@"%@", kid.name] forState:UIControlStateNormal];
        }
        else {
            [btn setTitle:KPLocalizedString(@"add") forState:UIControlStateNormal];
        }
        [btn setTitleColor:[UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:15];
        CGRect aframe = btn.titleLabel.frame;
        aframe.size.width = 100;
        btn.titleLabel.frame = aframe;
        btn.titleLabel.numberOfLines = 1;
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn.titleEdgeInsets = UIEdgeInsetsMake(100, 0, 0, 0);

        [self.scrollView addSubview:btn];

        if (kid != nil) {

            UIImageView *kidImg = [[UIImageView alloc] initWithFrame:CGRectMake(ax + 4, ay + 4, 72, 72)];
            [self.scrollView addSubview:kidImg];

            KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:kid.imageAssetID];
            if (imageAsset == nil) {
                kidImg.image = [GlobalUtils getKidDefaultImage:kid.gender];
            } else {
                __weak UIImageView *wkidImg = kidImg;
                [kidImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    wkidImg.image = [wkidImg.image imageMaskedWithElipse:CGSizeMake(290, 290)];

                    //UIImageView *pencilImage = [[UIImageView alloc] initWithFrame:CGRectMake(50, 5, 26, 26)];
                    //pencilImage.image = [UIImage imageNamed:@"edit-artist-camera"];
                    //[wkidImg addSubview:pencilImage];

                }];
            }
        }


        ax += 100;
        if (ax > self.view.frame.size.width - 100) {
            ax = 40;
            if([UIScreen mainScreen].bounds.size.width == 320){
                ax = 20;
            } else if([UIScreen mainScreen].bounds.size.width == 414){
                ax = 65;
            }
            ay += 110;
        }
    };

    for (KPLocalKid *kid in [KPLocalKid fetchMine]) {
        createButtonBlock(kid);
    }
    createButtonBlock(nil);

    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width - 20, ay + 150);

}

#pragma mark - Actions

- (void)kidTap:(UIButton *)sender {
    KPLocalKid *kid = nil;
    if (sender.tag != -1) {
        kid = [KPLocalKid fetchByServerID:sender.tag];
    }

    EditKidViewController *avc = [[EditKidViewController alloc] initWithKid:kid];
    avc.delegate = self;
    [self.navigationController pushViewController:avc animated:YES];
}

- (void)topBtnTap:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-kids", @"source", @"cancel", @"result", nil]];

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - EditKidDelegate

- (void)kidAdded:(KPLocalKid *)kid {

    /*
    int age = 0;
    if (kid.birthdate != 0) {
        
        age = [AgeCalculator calculateAgeForBirthday:[NSDate dateWithTimeIntervalSince1970:kid.birthdate]];
        
        // age = [[NSDate date] timeIntervalSinceDate:kid.birthdate] / 60 / 60 / 24 / 30 / 12;

    }
    */

    [self fetchData];
}

- (void)kidsRefreshed:(NSNotification *)notification {
    [self fetchData];
}

@end
