//
//  ChooseMemoryViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "ChooseMemoryViewController.h"
#import "PictureSelectorViewController.h"
#import "AlbumSelectorViewController.h"
#import "ImageEditorViewController.h"
#import "KPPhotoLibrary.h"
#import "KPLocalKid.h"
#import "KPLocalAsset.h"

#import "Keepy-Swift.h"

@interface ChooseMemoryViewController () <PictureSelectorViewControllerDelegate, AlbumSelectorViewControllerDelegate> {
    KPPhotoLibrary *photoLibrary;
}

@property(nonatomic, strong) NSMutableDictionary *viewControllersByIdentifier;
@property(nonatomic, weak) IBOutlet UIImageView *kidImageView;
@property(nonatomic, weak) IBOutlet UIActivityIndicatorView *kidActivityIndicatorView;
@property(nonatomic, weak) IBOutlet UILabel *welcomeTitle;
@property(nonatomic, strong) NSArray *pictures;
@property(nonatomic, strong) NSArray *albums;
@property(nonatomic, weak) UIActivityIndicatorView *photoLoadingActivityIndicatorView;
@property(nonatomic, strong) NSError *libraryError;
@end

@implementation ChooseMemoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"onboarding-choose-memory-open"];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SVProgressHUDDidDisappear:) name:SVProgressHUDDidDisappearNotification object:nil];

    self.viewControllersByIdentifier = [NSMutableDictionary dictionary];

    NSArray* kids = [[KPLocalKid fetchMine] sortedArrayUsingComparator:^NSComparisonResult(KPLocalKid* kid1, KPLocalKid* kid2) {
        return kid1.imageAssetID < kid2.imageAssetID ? NSOrderedAscending : NSOrderedDescending;
    }];
    
    KPLocalKid *kid = [kids lastObject];
    KPLocalAsset *kidImage = [KPLocalAsset fetchByLocalID:kid.imageAssetID];
    
    __weak ChooseMemoryViewController *weakSelf = self;
    if (kidImage.urlHash) {
        [self.kidActivityIndicatorView startAnimating];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, kidImage.urlHash]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            [weakSelf.kidActivityIndicatorView stopAnimating];
            weakSelf.kidImageView.image = [image imageMaskedWithElipse:CGSizeMake(290, 290)];
        }];
    } else {
        self.kidImageView.image = [GlobalUtils getKidDefaultImage:kid.gender];
    }

    self.welcomeTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:16.0f];
    //self.welcomeTitle.text = [NSString stringWithFormat:@"Pick one of %@'s memories to get started", kid.name];
    self.welcomeTitle.text = [NSString stringWithFormat:@"%@\n%@", @"Pick A Memory", @"Artwork, Schoolwork, Photo"];

    if (photoLibrary == nil) {
        photoLibrary = [[KPPhotoLibrary alloc] init];

        UIActivityIndicatorView *photoLoadingActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        photoLoadingActivityIndicatorView.center = CGPointMake(self.view.bounds.size.width / 2.0f, self.view.bounds.size.height / 2.0f);
        [self.view addSubview:photoLoadingActivityIndicatorView];
        self.photoLoadingActivityIndicatorView = photoLoadingActivityIndicatorView;
    }
   
        
    
    [self.photoLoadingActivityIndicatorView startAnimating];
    [photoLibrary loadPhotosAsynchronously:^(NSArray *assets, NSError *error) {
        [self.photoLoadingActivityIndicatorView stopAnimating];
        if (!error) {
            weakSelf.pictures = assets;
            [weakSelf performSegueWithIdentifier:@"PictureSelector" sender:weakSelf];
        }
        else {
            //error message is show in viewDidAppear
            self.libraryError = error;
        }
    }];
    
    /*if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        NSMutableArray *pictures = [[NSMutableArray alloc] initWithCapacity:1024];

        PHFetchOptions *allPhotosOptions = [PHFetchOptions new];
        allPhotosOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending: NO]];
        PHFetchResult *allPhotosResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:allPhotosOptions];
        
        if ([allPhotosResult count] > 0) {
            [pictures addObjectsFromArray: (NSArray *)allPhotosResult];
            weakSelf.pictures = pictures;
            
            [weakSelf performSegueWithIdentifier:@"PictureSelector" sender:weakSelf];
        }
        else {
            
        }
    }
    else {
        [[GlobalUtils defaultAssetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            if (group) {
                if (group.numberOfAssets > 0) {
                    NSMutableArray *pictures = [[NSMutableArray alloc] initWithCapacity:1024];
                    [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                        if (result) {
                            [pictures insertObject:result atIndex:0];
                        }
                    }];
                    weakSelf.pictures = pictures;
                    *stop  = YES;
                    [weakSelf performSegueWithIdentifier:@"PictureSelector" sender:weakSelf];
                } else {
                    *stop = YES;
                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                }
            }
        } failureBlock:^(NSError *error) {
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }];
    }*/
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    //if    denied access to library
    if(self.libraryError != nil && self.libraryError.code == ALAssetsLibraryAccessUserDeniedError) {
        
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        
    } else if(self.libraryError != nil) {
        //other errors
        UIAlertController* ac = [UIAlertController alertWithMessage:self.libraryError.localizedDescription];
        [ac presentWithAnimated:YES completion:nil];
    }
    //for show only one time
    self.libraryError = nil;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
}

//will close this viewController when SVProgress disappear
- (void)SVProgressHUDDidDisappear:(NSNotification *)notification {
    
    NSString *infoKey = notification.userInfo[SVProgressHUDStatusUserInfoKey];
    
    if([infoKey isEqualToString:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void)prepareForSegue:(ChooseMemorySegue *)segue sender:(id)sender {
    self.previousViewController = self.nextViewController;
    if (![self.viewControllersByIdentifier objectForKey:segue.identifier]) {
        [self.viewControllersByIdentifier setObject:segue.destinationViewController forKey:segue.identifier];
    }

    self.nextIdentifier = segue.identifier;
    self.nextViewController = [self.viewControllersByIdentifier objectForKey:self.nextIdentifier];
    if ([self.nextIdentifier isEqualToString:@"PictureSelector"]) {
        PictureSelectorViewController *pictureSelectorViewController = (PictureSelectorViewController *) self.nextViewController;
        pictureSelectorViewController.delegate = self;
        pictureSelectorViewController.pictures = self.pictures;
        segue.subtype = kCATransitionFromRight;
    } else if ([self.nextIdentifier isEqualToString:@"AlbumSelector"]) {
        AlbumSelectorViewController *albumSelectorViewController = (AlbumSelectorViewController *) self.nextViewController;
        albumSelectorViewController.delegate = self;
        albumSelectorViewController.albums = self.albums;
        segue.subtype = kCATransitionFromLeft;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([self.nextIdentifier isEqualToString:identifier]) {
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [self.viewControllersByIdentifier.allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
        if (![self.nextIdentifier isEqualToString:key]) {
            [self.viewControllersByIdentifier removeObjectForKey:key];
        }
    }];
}

- (IBAction)chooseMemoryExit:(UIStoryboardSegue *)segue {

}

- (void)albums:(id)sender {
//    if ([KPPhotoLibrary authorizationIsDenied]) {
//        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
//        return;
//    }
//    NSMutableArray *albums = [[NSMutableArray array] init];
//    __weak ChooseMemoryViewController *weakSelf = self;
//    [[GlobalUtils defaultAssetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
//        if (group) {
//            [albums insertObject:group atIndex:0];
//        } else {
//            weakSelf.albums = albums;
//            [weakSelf performSegueWithIdentifier:@"AlbumSelector" sender:weakSelf];
//            *stop = YES;
//        }
//    }                                               failureBlock:^(NSError *error) {
//        // Do nothing
//    }];
//
    
    if ([KPPhotoLibrary authorizationIsDenied]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        return;
    }
    
    NSMutableArray *albums = [[NSMutableArray array] init];
    __weak ChooseMemoryViewController *weakSelf = self;
    
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    
    [smartAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        
        PHFetchResult *result = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
        NSLog(@"album title: %@ (%ld)", collection.localizedTitle, (long)result.count);
        if (result.count > 0) {
            [albums addObject:collection];
        }
        
        if (idx+1 == smartAlbums.count) {
            self.albums = albums;
            [self performSegueWithIdentifier:@"AlbumSelector" sender:weakSelf];
        }
    }];
#if 0
    
    [[GlobalUtils defaultAssetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [albums insertObject:group atIndex:0];
        } else {
            weakSelf.albums = albums;
            [weakSelf performSegueWithIdentifier:@"AlbumSelector" sender:weakSelf];
            *stop = YES;
        }
    }                                               failureBlock:^(NSError *error) {
        // Do nothing
    }];
#endif
    
}
- (void)pictureSelected:(PHAsset *)asset {
    @try {
 
        NSMutableDictionary *workingDictionary = [[NSMutableDictionary alloc] init];
        [workingDictionary setObject:[NSString stringWithFormat:@"%ld",(long)asset.mediaType]  forKey:@"UIImagePickerControllerMediaType"];
        
        PHImageManager* im = [PHImageManager defaultManager];
        PHImageRequestOptions* options = [PHImageRequestOptions new];
        options.synchronous = YES;
        options.networkAccessAllowed = YES;
        options.version = PHImageRequestOptionsVersionCurrent;
       
        [im requestImageDataForAsset:asset options:options resultHandler:^(NSData * imageData, NSString * dataUTI, UIImageOrientation orientation, NSDictionary * info) {
            NSLog(@"Dict: %@",info);
            UIImage* image = [UIImage imageWithData:imageData];
            [workingDictionary setObject:image forKey:@"UIImagePickerControllerOriginalImage"];

        }];
        
        ImageEditorViewController *imageEditorViewController = [[ImageEditorViewController alloc] initWithImageInfo:workingDictionary applyCrop:NO];
        imageEditorViewController.isNewKid = YES;
        [self.navigationController pushViewController:imageEditorViewController animated:YES];
    } @catch (NSException *e) {
        // If for whatever reason assets data comes back nil, do nothing so the user can try again instead of crashing.
    }
}

//- (void)pictureSelected:(ALAsset *)asset {
////- (void)pictureSelected:(PHAsset*)asset {
//    @try {
//
//
//        NSMutableDictionary *workingDictionary = [[NSMutableDictionary alloc] init];
//        [workingDictionary setObject:[asset valueForProperty:ALAssetPropertyType] forKey:@"UIImagePickerControllerMediaType"];
//        ALAssetRepresentation *assetRep = [asset defaultRepresentation];
//
//        UIImageOrientation orientation = UIImageOrientationUp;
//        NSNumber *orientationValue = [asset valueForProperty:@"ALAssetPropertyOrientation"];
//        if (orientationValue != nil) {
//            orientation = [orientationValue intValue];
//        }
//
//        CGImageRef imgRef = [assetRep fullResolutionImage];
//        UIImage *img = [UIImage imageWithCGImage:imgRef
//                                           scale:1.0
//                                     orientation:orientation];
//
//        [workingDictionary setObject:img forKey:@"UIImagePickerControllerOriginalImage"];
//        [workingDictionary setObject:[[asset valueForProperty:ALAssetPropertyURLs] valueForKey:[[[asset valueForProperty:ALAssetPropertyURLs] allKeys] objectAtIndex:0]] forKey:@"UIImagePickerControllerReferenceURL"];
//        NSMutableDictionary *metaData = [assetRep metadata].mutableCopy;
//
//        NSDate *creationDate = [asset valueForProperty:ALAssetPropertyDate];
//
//        NSDateFormatter *dateFormatter = [NSDateFormatter new];
//        dateFormatter.dateFormat = @"y:MM:dd HH:mm:ss";
//
//        NSDictionary *exifData = metaData[@"{Exif}"];
//        NSString *exifCreationDateString = exifData[@"DateTimeOriginal"];
//
//
//        if (exifCreationDateString) {
//            creationDate = [dateFormatter dateFromString:exifCreationDateString];
//        } else {
//
//            if (!exifData) {
//                metaData[@"{Exif}"] = @{}.mutableCopy;
//            }
//
//            metaData[@"{Exif}"][@"DateTimeOriginal"] = [dateFormatter stringFromDate:creationDate];
//
//        }
//
//        [workingDictionary setObject:metaData.copy forKey:@"metadata"];
//
//        ImageEditorViewController *imageEditorViewController = [[ImageEditorViewController alloc] initWithImageInfo:workingDictionary applyCrop:NO];
//        imageEditorViewController.isNewKid = YES;
//        [self.navigationController pushViewController:imageEditorViewController animated:YES];
//    } @catch (NSException *e) {
//        // If for whatever reason assets data comes back nil, do nothing so the user can try again instead of crashing.
//    }
//}
- (void)pictures:(PHAssetCollection *)collection {
    __weak ChooseMemoryViewController *weakSelf = self;
    NSMutableArray *pictures = [[NSMutableArray alloc] initWithCapacity:1024];
    
    PHFetchOptions* options = [PHFetchOptions new];
    options.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES] ];
    PHFetchResult<PHAsset *> * result = [PHAsset fetchAssetsInAssetCollection:collection options:options];
    
    [result enumerateObjectsUsingBlock:^(PHAsset* obj, NSUInteger idx, BOOL *stop) {
        if (obj) {
            [pictures insertObject:obj atIndex:0];
        }
        
        if (idx+1 == result.count) {
            weakSelf.pictures = pictures;
            [weakSelf performSegueWithIdentifier:@"PictureSelector" sender:weakSelf];
            *stop = YES;
        }
    }];
}

//- (void)pictures:(ALAssetsGroup *)group {
//    __weak ChooseMemoryViewController *weakSelf = self;
//    NSMutableArray *pictures = [[NSMutableArray alloc] initWithCapacity:1024];
//    [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
//        if (result) {
//            [pictures insertObject:result atIndex:0];
//        } else {
//            weakSelf.pictures = pictures;
//            [weakSelf performSegueWithIdentifier:@"PictureSelector" sender:weakSelf];
//            *stop = YES;
//        }
//    }];
//}

@end

@implementation ChooseMemorySegue

- (void)perform {
    ChooseMemoryViewController *chooseMemoryViewController = (ChooseMemoryViewController *) self.sourceViewController;
    UIViewController *nextViewController = (UIViewController *) chooseMemoryViewController.nextViewController;
    if (chooseMemoryViewController.previousViewController) {
        CATransition *animation = [CATransition animation];
        [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:self.subtype];
        [animation setDuration:0.3f];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[chooseMemoryViewController.containerView layer] addAnimation:animation forKey:@"pushIn"];

        [chooseMemoryViewController.previousViewController willMoveToParentViewController:nil];
        [chooseMemoryViewController.previousViewController.view removeFromSuperview];
        [chooseMemoryViewController.previousViewController removeFromParentViewController];

        nextViewController.view.frame = chooseMemoryViewController.containerView.bounds;
        [chooseMemoryViewController addChildViewController:nextViewController];
        [chooseMemoryViewController.containerView addSubview:nextViewController.view];
        [chooseMemoryViewController didMoveToParentViewController:chooseMemoryViewController];

        [[chooseMemoryViewController.containerView layer] removeAnimationForKey:@"pushIn"];
    } else {
        nextViewController.view.frame = chooseMemoryViewController.containerView.bounds;
        [chooseMemoryViewController addChildViewController:nextViewController];
        [chooseMemoryViewController.containerView addSubview:nextViewController.view];
        [chooseMemoryViewController didMoveToParentViewController:chooseMemoryViewController];
    }
}

@end
