//
//  DKTableViewUploderData.m
//  Keepy
//
//  Created by Daniel Karsh on 5/9/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "DKTableViewUploderData.h"
#import "DKMultyUploaderManger.h"
#import "DKVideoUploaderManager.h"


@implementation DKTableViewUploderData

- (instancetype)init {
    self = [super init];
    if (self) {

    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger a = [[[DKMultyUploaderManger sharedClient] allVideosUploadingProccess] count];
    return a;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"uploaderCELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    [self showUploader:cell atIndexPath:indexPath];
    return cell;
}

- (void)showUploader:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    if (cell.contentView.tag) {

    } else {
        DKVideoUploaderManager *upManager = [[[DKMultyUploaderManger sharedClient] allVideosUploadingProccess] objectAtIndex:indexPath.row];
        UIView *uploadProgressView = upManager.uploadProgressView;
        [cell.contentView addSubview:uploadProgressView];
        uploadProgressView.tag = 102;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

@end
