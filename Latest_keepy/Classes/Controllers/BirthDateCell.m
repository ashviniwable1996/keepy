//
//  BirthDateCell.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "BirthDateCell.h"

@interface BirthDateCell ()

@property(nonatomic, weak) IBOutlet UILabel *birthDateLabel;

@end

@implementation BirthDateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.birthDateLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
}

+ (CGFloat)height {
    return 85.0f;
}

- (IBAction)birthDate:(id)sender {
    [self.delegate birthDate];
}

@end
