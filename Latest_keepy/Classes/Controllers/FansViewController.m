//
//  FansViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>



#import "FansViewController.h"
#import "Fan.h"
#import "EditFanViewController.h"
#import "KPDataParser.h"
#import "SelectFanSourceViewController.h"
#import "RegisterViewController.h"

@interface FansViewController () <EditFanDelegate>

@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) NSArray *fans;

@end

@implementation FansViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fans", @"source", nil]];

    //self.title = KPLocalizedString(@"fans");
    if ([self.navigationController.viewControllers count] == 1) {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"close"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];
    } else {
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"settings"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:nil];
    }


    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.scrollView];

    [self fetchData];
    [self refresh];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fansRefreshed:) name:FANS_REFRESHED_NOTIFICATION object:nil];

    /*
    if ([self.fans count] == 0 && [self.navigationController.viewControllers count] == 1)
    {
        SelectFanSourceViewController *avc = [[SelectFanSourceViewController alloc] initWithNibName:@"SelectFanSourceViewController" bundle:nil];
        [self.navigationController pushViewController:avc animated:NO];
    }*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    self.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)fetchData {
    for (UIControl *control in self.scrollView.subviews) {
        [control removeFromSuperview];
    }

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width - 40, 60)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:16];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = KPLocalizedString(@"Who would love to get updates of your kids?");
    lbl.numberOfLines = 4;
    [self.scrollView addSubview:lbl];

    // Kid *kid = (Kid*)[[[UserManager sharedInstance] myKids] objectAtIndex:0];
    self.fans = [Fan MR_findAllSortedBy:@"relationType,nickname" ascending:YES withPredicate:nil];

    __block float ax = (self.view.bounds.size.width / 2) - 80;// 80;
    __block float ay = 65;
    __block BOOL addedLine;

    void(^createButtonBlock)(Fan *fan, int aindex) = ^(Fan *fan, int aindex) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(ax, ay, 80, 80);
        NSString *imageName = @"small-add-button";
        NSString *relationStr = @"";
        if (fan != nil) {
            switch (fan.relationType.intValue) {
                case RELATION_MOM:
                    imageName = @"mom";
                    //relationStr = @"mom\n";
                    break;
                case RELATION_DAD:
                    imageName = @"dad";
                    //relationStr = @"dad\n";
                    break;
                case RELATION_GRANDMA:
                    imageName = @"grandma";
                    //relationStr = @"grandma\n";
                    break;
                case RELATION_GRANPA:
                    imageName = @"grandpa";
                    //relationStr = @"grandpa\n";
                    break;
                case RELATION_FAMILY:
                    imageName = @"family";
                    //relationStr = @"";
                    break;
                case RELATION_FRIEND:
                    imageName = @"friend";
                    //relationStr = @"";
                    break;
                case RELATION_AUNT:
                    imageName = @"aunt";
                    //relationStr = @"";
                    break;
                case RELATION_UNCLE:
                    imageName = @"uncle";
                    //relationStr = @"";
                    break;
            }
        }

        if (aindex == -900) //user
        {
            if ([[UserManager sharedInstance] getMe].parentType.intValue == 0) {
                imageName = @"mom";
            } else if ([[UserManager sharedInstance] getMe].parentType.intValue == 1) {
                imageName = @"dad";
            } else {
                imageName = @"family";
            }

        }

        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-selected", imageName]] forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(fanTap:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = aindex + 1;

        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay + 70, 80, 40)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textColor = [UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.numberOfLines = 2;
        [self.scrollView addSubview:lbl];


        if (fan != nil) {
            lbl.text = [NSString stringWithFormat:@"%@%@", relationStr, fan.nickname];
        }
        else if (aindex == -900) //user
        {
            lbl.text = KPLocalizedString(@"you");
        }
        else if (aindex == -1) //partner
        {
            lbl.text = KPLocalizedString(@"add partner");
        }
        else {
            lbl.text = KPLocalizedString(@"add");
        }

        CGRect aframe = lbl.frame;
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:lbl.text attributes:@
        {
                NSFontAttributeName : lbl.font
        }];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize) {aframe.size.width, CGFLOAT_MAX}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        aframe.size.height = rect.size.height;
        lbl.frame = aframe;

        //[btn setTitleColor:[UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1] forState:UIControlStateNormal];
        //btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
        /*
        CGRect aframe = btn.titleLabel.frame;
        btn.titleLabel.numberOfLines = 2;
        btn.titleLabel.backgroundColor = [UIColor greenColor];
        if (fan == nil)
            btn.imageEdgeInsets = UIEdgeInsetsMake(6, 14, 0, 0);
        
        btn.titleEdgeInsets = UIEdgeInsetsMake(80, -80, 0, 0);
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        */
        //DDLogInfo(@"%2f", aframe.size.width);
        //aframe.size.width = 100;
        //btn.titleLabel.frame = aframe;


        [self.scrollView addSubview:btn];

        ax += 80;
        int differenceOfScreens = 0;
        if ([UIScreen mainScreen].bounds.size.width == 375){
            differenceOfScreens = 80;
        }
        if (ax >= self.view.frame.size.width - differenceOfScreens) {
            ax = 0;
            if ([UIScreen mainScreen].bounds.size.width == 375){
                ax = 20;
            }
            ay += 100;

            addedLine = YES;
        }
        else {
            addedLine = NO;
        }
    };

    //you
    createButtonBlock(nil, -900);

    //partner
    Fan *apartner = nil;
    int partnerIndex = -1;
    for (int i = 0; i < [self.fans count]; i++) {
        if (((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 0 || ((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 1) {
            apartner = [self.fans objectAtIndex:i];
            partnerIndex = i;
            break;
        }
    }
    createButtonBlock(apartner, partnerIndex);

    //grandparents

    ay += 100;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ay, self.view.frame.size.width, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
    [self.scrollView addSubview:lineView];

    ay += 10;

    lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, ay, 280, 20)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.text = KPLocalizedString(@"grandparents");
    [self.scrollView addSubview:lbl];

    ay += 15;
    ax = 0;
    if ([UIScreen mainScreen].bounds.size.width == 375){
        ax = 20;
    }
    for (int i = 0; i < [self.fans count]; i++) {
        if (((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 2 || ((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 3) {
            createButtonBlock([self.fans objectAtIndex:i], i);
        }
    }
    createButtonBlock(nil, -2);

    //aunts & uncles

    if (addedLine) {
        ay += 20;
    } else {
        ay += 110;
    }

    lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ay, self.view.frame.size.width, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
    [self.scrollView addSubview:lineView];

    ay += 10;

    lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, ay, 280, 20)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.text = KPLocalizedString(@"aunts & uncles");
    [self.scrollView addSubview:lbl];

    ay += 15;
    ax = 0;
    if ([UIScreen mainScreen].bounds.size.width == 375){
        ax = 20;
    }
    for (int i = 0; i < [self.fans count]; i++) {
        if (((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 6 || ((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 7) {
            createButtonBlock([self.fans objectAtIndex:i], i);
        }
    }
    createButtonBlock(nil, -5);

    //family

    if (addedLine) {
        ay += 20;
    } else {
        ay += 110;
    }

    lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ay, self.view.frame.size.width, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
    [self.scrollView addSubview:lineView];

    ay += 10;

    lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, ay, 280, 20)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.text = KPLocalizedString(@"family");
    [self.scrollView addSubview:lbl];

    ay += 15;
    ax = 0;
    if ([UIScreen mainScreen].bounds.size.width == 375){
        ax = 20;
    }
    for (int i = 0; i < [self.fans count]; i++) {
        if (((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 4 || (i != partnerIndex && (((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 0 || ((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 1))) {
            createButtonBlock([self.fans objectAtIndex:i], i);
        }
    }
    createButtonBlock(nil, -3);

    //friends

    if (addedLine) {
        ay += 20;
    } else {
        ay += 110;
    }

    lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ay, self.view.frame.size.width, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
    [self.scrollView addSubview:lineView];

    ay += 10;

    lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, ay, 280, 20)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:14];
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.text = KPLocalizedString(@"friends");
    [self.scrollView addSubview:lbl];

    ay += 15;
    ax = 0;
    if ([UIScreen mainScreen].bounds.size.width == 375){
        ax = 20;
    }
    for (int i = 0; i < [self.fans count]; i++) {
        if (((Fan *) [self.fans objectAtIndex:i]).relationType.intValue == 5) {
            createButtonBlock([self.fans objectAtIndex:i], i);
        }
    }
    createButtonBlock(nil, -4);

    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width - 20, ay + 120);
}

- (void)refresh {
    [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];
    [[ServerComm instance] getFans:^(id result) {
        [[KPDataParser instance] parseFans:[result valueForKey:@"result"]];
        [SVProgressHUD dismiss];
        [self fetchData];
    }                 andFailBlock:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - Actions

- (void)fanTap:(UIButton *)sender {
    Fan *fan = nil;
    if (sender.tag > 0) {
        fan = [self.fans objectAtIndex:sender.tag - 1];
    }
    else if (sender.tag == -899) {
        RegisterViewController *avc = [[RegisterViewController alloc] initWithMode:1];
        [self.navigationController pushViewController:avc animated:YES];
        return;
    }
    else {
        SelectFanSourceViewController *avc = [[SelectFanSourceViewController alloc] initWithRelationMode:0 withRelationMode:(sender.tag - 1) * -1];
        [self.navigationController pushViewController:avc animated:YES];
        return;
    }

    EditFanViewController *avc = [[EditFanViewController alloc] initWithFan:fan];
    avc.delegate = self;
    [self.navigationController pushViewController:avc animated:YES];
}

- (void)topBtnTap:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fans", @"source", @"cancel", @"result", nil]];

    if ([self.navigationController.viewControllers count] == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - EditFanDelegate

- (void)fanChanged:(Fan *)fan {
    [self fetchData];
}

- (void)fansRefreshed:(NSNotification *)notification {
    [self fetchData];
}

@end
