//
//  HomeViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/7/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemsViewController.h"
#import "EditKidViewController.h"


#import "KLCPopup.h"
#import "Keepy-Swift.h"

@interface HomeViewController : UIViewController

@property(nonatomic, strong) KLCPopup *popup;
@property(nonatomic, strong) ItemsViewController *itemsVC;
@property(nonatomic, strong) EditKidViewController *editKidVC;
@property(nonatomic) BOOL isFromLogin;
@property(strong, nonatomic) IBOutlet UIView *OBContainer;
@property(strong, nonatomic) IBOutlet UIView *BGView;
@property(strong,nonatomic) ProductCollectionView * productView;
- (void)showThetreasureView;
@end
