//
//  FansViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@interface FansViewController : KPPopViewController

@end
