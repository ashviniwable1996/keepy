//
//  ImageEditorViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 3/30/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"
#import "Item.h"

@protocol ImageEditorDelegate

- (void)imageEditorFinishedWithResult:(NSDictionary *)result;

@end

@interface ImageEditorViewController : KPPopViewController
@property(nonatomic, strong) id <ImageEditorDelegate> delegate;
@property (nonatomic) BOOL isNewKid;

- (instancetype)initWithItem:(KPLocalItem *)item;

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo applyCrop:(BOOL)applyCrop;

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo andPoints:(NSArray *)points;

- (void)execCrop;

@end
