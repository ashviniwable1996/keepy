//
//  FamilyMemberView.m
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilyMemberView.h"

@implementation FamilyMemberView

- (instancetype)init {
    if ((self = [[[NSBundle mainBundle] loadNibNamed:@"FamilyMemberView" owner:self options:nil] firstObject])) {
        self.name.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    }
    return self;
}

+ (CGFloat)width {
    return 75.0f;
}

@end
