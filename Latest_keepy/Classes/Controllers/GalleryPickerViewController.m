//
//  GalleryPickerViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 8/6/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "GalleryPickerViewController.h"

@import Photos;

#import "ELCAssetTablePicker.h"
#import "CameraViewController.h"
#import "AppDelegate.h"

#import "Keepy-Swift.h"

@interface GalleryPickerViewController () <ELCAssetSelectionDelegate>

@property(nonatomic, strong) ELCAssetTablePicker *tablePicker;
@property(nonatomic, strong) NSArray<KPAsset*>* assets;

@end

@implementation GalleryPickerViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithAssets:(NSArray<KPAsset *> *)assets {
    self = [super initWithNibName:@"GalleryPickerViewController" bundle:nil];
    if (self) {
        self.assets = assets;
        self.title = KPLocalizedString(@"All Photos");
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];


#ifdef TEACHERS
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"albums"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", nil]];
#else
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"albums"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil] andRightButtonInfo:nil];
#endif

    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"gallery-picker", @"source", nil]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0x2CA0C4);
    self.navigationController.navigationBar.barTintColor = nil;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1], NSForegroundColorAttributeName, [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17], NSFontAttributeName, nil];


}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];

    if (self.tablePicker != nil) {
        for (KPAsset* a in self.tablePicker.assets) {
            a.selected = NO;
        }
        [self.tablePicker.tableView reloadData];
    }
}

- (void)viewDidLayoutSubviews {

    if (self.tablePicker != nil) {
        return;
    }

    self.tablePicker = [[ELCAssetTablePicker alloc] initWithNibName:nil bundle:nil];
#ifdef TEACHERS
    self.tablePicker.singleSelection = NO;
    self.tablePicker.immediateReturn = NO;
#else
    self.tablePicker.singleSelection = YES;
    self.tablePicker.immediateReturn = YES;
#endif

    self.tablePicker.assets = self.assets;

    self.tablePicker.parent = self;
    if (_showToolbar) {
        self.tablePicker.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 60);
        [self.view addSubview:self.tablePicker.view];

        UIView *bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 60, self.view.bounds.size.width, 60)];
        bottomBar.backgroundColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        bottomBar.alpha = 0.8;
        [self.view addSubview:bottomBar];

        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIButton *libButton = [UIButton buttonWithType:UIButtonTypeCustom];
            libButton.frame = CGRectMake(20, (60 - 44) / 2, 44, 44);
            [libButton setImage:[UIImage imageNamed:@"add-photo"] forState:UIControlStateNormal];
            [libButton addTarget:self action:@selector(cameraTap:) forControlEvents:UIControlEventTouchUpInside];
            [bottomBar addSubview:libButton];
        }

    } else {
        self.tablePicker.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:self.tablePicker.view];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)topBtnTap:(UIButton *)sender {
    if (sender.tag == 1) {
        NSMutableArray* arr = [NSMutableArray array];
        for (KPAsset* a in self.tablePicker.assets) {
            if (a.selected) {
                [arr addObject:a.asset];
            }
        }

        if (arr.count > 0) {
            [self selectedAssets:arr];
        }
    }
    else {
        [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"gallery-picker", @"source", @"cancel", @"result", nil]];


        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)cameraTap:(id)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"gallery-picker", @"source", @"camera", @"result", nil]];

    [[UserManager sharedInstance] setCameraMode:0];


    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType:completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[Mixpanel sharedInstance] registerSuperProperties:[NSDictionary dictionaryWithObjectsAndKeys:@"Camera", @"PhotoSource", nil]];

                    CameraViewController *avc = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
                    avc.delegate = self.delegate;

                    [UIView transitionFromView:self.view
                                        toView:avc.view
                                      duration:1
                                       options:UIViewAnimationOptionTransitionFlipFromLeft
                                    completion:nil];
                    [self.navigationController setViewControllers:[NSArray arrayWithObject:avc] animated:YES];
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Camera")];
                });
            }
        }];
    }
    else {
        [[Mixpanel sharedInstance] registerSuperProperties:[NSDictionary dictionaryWithObjectsAndKeys:@"Camera", @"PhotoSource", nil]];

        CameraViewController *avc = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
        avc.delegate = self.delegate;

        [UIView transitionFromView:self.view
                            toView:avc.view
                          duration:1
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        completion:nil];
        [self.navigationController setViewControllers:[NSArray arrayWithObject:avc] animated:YES];
    }
}

- (void)selectedAssets:(NSArray *)assets {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];

    NSMutableArray* arr = [NSMutableArray new];

    PHImageManager* im = [PHImageManager defaultManager];
    PHImageRequestOptions* options = [PHImageRequestOptions new];
    options.synchronous = YES;
    options.networkAccessAllowed = YES;
    options.version = PHImageRequestOptionsVersionCurrent;
    
    NSDateFormatter* df = [NSDateFormatter new];
    df.dateFormat = @"y:MM:dd HH:mm:ss";
    
    for (PHAsset* a in assets) {
        NSMutableDictionary* dict = [NSMutableDictionary new];
        
        dict[UIImagePickerControllerMediaType] = @(a.mediaType);
        
        [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];
        
        // Forcibly fetch the video synchronously.
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            if (a.mediaType == PHAssetMediaTypeImage) {
                [im requestImageDataForAsset:a options:options resultHandler:^(NSData * imageData, NSString * dataUTI, UIImageOrientation orientation, NSDictionary * info) {
                    dict[UIImagePickerControllerOriginalImage] = [UIImage imageWithData:imageData];
                    dict[UIImagePickerControllerReferenceURL] = dataUTI;
                    
                    CIImage* ciimage = [CIImage imageWithData:imageData];
                    NSDictionary* metadata = ciimage.properties[@"metadata"];
                    if (metadata) {
                        NSDictionary* exif = metadata[@"{Exif}"];
                        if (exif) {
                            NSLog(@"DateTimeOriginal: %@", exif[@"DateTimeOriginal"]);
                            
                        }
                        dict[@"metadata"] = metadata;
                    } else {
                        dict[@"metadata"] = ciimage.properties;
                    }
                    dispatch_semaphore_signal(semaphore);
                }];
            } else {
                PHVideoRequestOptions* options = [PHVideoRequestOptions new];
                options.networkAccessAllowed = YES;
                [im requestAVAssetForVideo:a options:options resultHandler:^(AVAsset * asset, AVAudioMix * audioMix, NSDictionary * info) {
                    AVURLAsset* urlAsset = (AVURLAsset*)asset;
                    dict[UIImagePickerControllerReferenceURL] = urlAsset.URL;
                    dispatch_semaphore_signal(semaphore);
                }];
            }
        });
            
        while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1f]];
        }
        
        if (dict[UIImagePickerControllerReferenceURL] == nil) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"too large to download. please use wifi.")];
            return;
        }
        [SVProgressHUD dismiss];
        appDelegate.VideoDateCreated = a.creationDate;

        [arr addObject:dict];
    }

    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"gallery-picker", @"source", @"ok", @"result", nil]];

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(galleryPickerController:didFinishPickingMediaWithInfo:)]) {
        [self.delegate performSelector:@selector(galleryPickerController:didFinishPickingMediaWithInfo:) withObject:self withObject:arr];
    }
}

@end
