//
//  CreateAccountViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/12/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "InputCell.h"

@interface CreateAccountViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation CreateAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    [self.tableView registerNib:[UINib nibWithNibName:@"InputCell" bundle:nil] forCellReuseIdentifier:@"InputCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *InputCellIdentifier = @"InputCell";
    static NSString *WhoCellIdentifier = @"WhoCell";
    if (indexPath.row == 0) {
        InputCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
        cell.input.placeholder = @"name";
        return cell;
    } else if (indexPath.row == 1) {
        WhoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:WhoCellIdentifier];
        return cell;
    } else if (indexPath.row == 2) {
        InputCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
        cell.input.placeholder = @"enter email";
        return cell;
    } else if (indexPath.row == 3) {
        InputCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
        cell.input.placeholder = @"create password";
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [InputCell height];
    } else if (indexPath.row == 1) {
        return [WhoCell height];
    } else if (indexPath.row == 2) {
        return [InputCell height];
    } else if (indexPath.row == 3) {
        return [InputCell height];
    }
    return 0.0f;
}

- (IBAction)save:(id)sender {
    [self performSegueWithIdentifier:@"AddKid" sender:self];
}

@end


@implementation WhoCell

+ (CGFloat)height {
    return 100.0f;
}

@end