//
//  AddPhotoCell.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    AddPhotoCellTypeBoy,
    AddPhotoCellTypeGirl
} AddPhotoCellType;

@protocol AddPhotoCellDelegate;

@interface AddPhotoCell : UITableViewCell

@property(nonatomic, weak) id <AddPhotoCellDelegate> delegate;
@property(nonatomic, assign) AddPhotoCellType addPhotoCellType;
@property(nonatomic, weak) IBOutlet UIButton *addPhotoButton;
@property(nonatomic, strong) UIImage *picture;

+ (CGFloat)height;

- (void)layout;

@end

@protocol AddPhotoCellDelegate <NSObject>

- (void)addPhoto:(id)sender;

@end