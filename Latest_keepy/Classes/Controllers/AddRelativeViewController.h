//
//  AddRelativeViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    AddRelativeViewControllerTypeAunt = 6,
    AddRelativeViewControllerTypeUncle = 7
} AddRelativeViewControllerType;

@interface AddRelativeViewController : UIViewController

@property(nonatomic, assign) AddRelativeViewControllerType addRelativeViewControllerType;
@property(nonatomic, strong) NSArray *family;

- (CGFloat)height;

@end
