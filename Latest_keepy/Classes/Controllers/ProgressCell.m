//
//  ProgressCell.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "ProgressCell.h"

@implementation ProgressCell

+ (CGFloat)height {
    return 49.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    if (self.bold == YES) {
        self.skipButton.titleLabel.textColor = [UIColor colorWithRed:(50.0f / 255.0f) green:(159.0f / 255.0f) blue:(199.0f / 255.0f) alpha:1.0f];
        self.skipButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    } else {
        self.skipButton.titleLabel.textColor = [UIColor colorWithRed:(50.0f / 255.0f) green:(159.0f / 255.0f) blue:(199.0f / 255.0f) alpha:1.0f];
        self.skipButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    }

    switch (self.progressCellState) {
        default:
        case ProgressCellStateOne: {
            self.progressImageView.image = [UIImage imageNamed:@"progress-one"];
            break;
        }

        case ProgressCellStateTwo: {
            self.progressImageView.image = [UIImage imageNamed:@"progress-two"];
            break;
        }

        case ProgressCellStateThree: {
            self.progressImageView.image = [UIImage imageNamed:@"progress-three"];
            break;
        }

        case ProgressCellStateFour: {
            self.progressImageView.image = [UIImage imageNamed:@"progress-four"];
            break;
        }
    }
}

- (IBAction)skip:(id)sender {
    [self.delegate skip:sender];
}

@end
