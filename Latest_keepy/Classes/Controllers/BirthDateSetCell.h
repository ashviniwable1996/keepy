//
//  BirthDateSetCell.h
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol BirthDateSetCellDelegate;

@interface BirthDateSetCell : UITableViewCell

@property(nonatomic, weak) id <BirthDateSetCellDelegate> delegate;
@property(nonatomic, strong) NSDate *birthDateDate;

+ (CGFloat)height;

@end

@protocol BirthDateSetCellDelegate <NSObject>

- (void)birthDate;

@end
