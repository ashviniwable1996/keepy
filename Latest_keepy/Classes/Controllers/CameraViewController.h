//
//  CameraViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 8/8/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KPPopViewController.h"
#import "GalleryPickerViewController.h"


@interface CameraViewController : KPPopViewController

@property(nonatomic, assign) id <GalleryPickerDelegate> delegate;

@end
