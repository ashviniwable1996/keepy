//
//  EmailCell.h
//  Keepy
//
//  Created by Troy Payne on 2/16/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol EmailCellDelegate;

@interface EmailCell : UITableViewCell

@property(nonatomic, weak) id <EmailCellDelegate> delegate;
@property(nonatomic, weak) IBOutlet UITextField *input;

+ (CGFloat)height;

@end

@protocol EmailCellDelegate <NSObject>

- (UIViewController *)viewController;

@end