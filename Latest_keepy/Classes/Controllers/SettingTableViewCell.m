//
//  SettingTableViewCell.m
//  Keepy
//
//  Created by Zvika Ashkenazi on 6/27/16.
//  Copyright © 2016 Keepy. All rights reserved.
//

#import "SettingTableViewCell.h"

@implementation SettingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
