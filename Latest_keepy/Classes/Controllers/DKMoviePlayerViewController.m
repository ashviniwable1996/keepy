//
//  DKMoviePlayerViewController.m
//  Keepy
//
//  Created by Daniel Karsh on 4/16/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "DKMoviePlayerViewController.h"
#import "ItemEditViewController.h"
#import "DKVideoUploaderManager.h"
#import "DKMultyUploaderManger.h"
#import "DKPlaybackPosition.h"
#import "AppDelegate.h"

CGFloat kMovieViewOffsetX = 20.0;
CGFloat kMovieViewOffsetY = 20.0;


@interface DKMoviePlayerViewController (MovieControllerInternal)

- (void)updatePlaybackProgressFromTimer:(id)sender;

- (void)createAndPlayMovieForURL:(NSURL *)movieURL sourceType:(MPMovieSourceType)sourceType;

- (void)moviePlayBackDidFinish:(NSNotification *)notification;

- (void)loadStateDidChange:(NSNotification *)notification;

- (void)moviePlayBackStateDidChange:(NSNotification *)notification;

- (void)mediaIsPreparedToPlayDidChange:(NSNotification *)notification;

- (void)installMovieNotificationObservers;

- (void)removeMovieNotificationHandlers;

- (void)deletePlayerAndNotificationObservers;

- (void)uploadOriginalImage;


@end

@interface DKMoviePlayerViewController (ViewController)
- (void)removeMovieViewFromViewHierarchy;

@end

@implementation DKMoviePlayerViewController (ViewController)


#pragma mark View Controller

- (void)willMoveToParentViewController:(UIViewController *)parent {
    /// kill upload
    if (!parent) {
        DKVideoUploaderManager *uploader =
                (DKVideoUploaderManager *) [[[DKMultyUploaderManger sharedClient] allVideosUploadingProccess] lastObject];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"VideoUploaderDone" object:uploader];
    }
}

- (void)cancelCurrentUpload:(NSNotification*)note {
    NSAssert(NO, @"cancelCurrentUpload: is not implemented");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", nil]
  andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"next"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", nil]];


    self.rightButtonEnabled = NO;


    self.playButton.hidden = YES;
    self.pauseButton.hidden = YES;

    self.isWindowChromeOn = NO;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelCurrentUpload:) name:CANCEL_CURRENT_UPLOAD_NOTIFICATION object:nil];

    if ([[UIScreen mainScreen] bounds].size.height != 568) {

        self.playBack.frame = self.playBack.frame;
 self.playBack.frame = CGRectMake(27, [[UIScreen mainScreen] bounds].size.height - self.playBack.frame.size.height,[[UIScreen mainScreen] bounds].size.width - 54, self.playBack.frame.size.height);
    }
    else {
        self.playBack.frame = CGRectMake(self.playBack.frame.origin.x, [[UIScreen mainScreen] bounds].size.height - self.playBack.frame.size.height, self.playBack.frame.size.width, self.playBack.frame.size.height);//self.playBack.frame;
         self.playBack.frame = CGRectMake(27, [[UIScreen mainScreen] bounds].size.height - self.playBack.frame.size.height,[[UIScreen mainScreen] bounds].size.width - 54, self.playBack.frame.size.height);
    }

    self.originalPlayBackFrame = self.playBack.frame;

    DDLogInfo(@"My view frame: %@", NSStringFromCGRect(self.originalPlayBackFrame));


    if (self.uploadVideoMode) {
        //self.pauseButton.hidden= YES;
        self.playBack.hidden = NO;
        self.playButton.hidden = NO;
        self.activeButton.hidden = NO;
        self.isFrameOn = YES;

        self.playBack.frame = CGRectMake(27, 444, [UIScreen mainScreen].bounds.size.width - 54, self.playBack.frame.size.height);
         self.playBack.frame = CGRectMake(27, [[UIScreen mainScreen] bounds].size.height - self.playBack.frame.size.height,[[UIScreen mainScreen] bounds].size.width - 54, self.playBack.frame.size.height);
        return;
    }

    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapWebView:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.delegate = self;
    [self.view addGestureRecognizer:singleTap];

    doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapWebView:)];
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.delegate = self;
    [self.view addGestureRecognizer:doubleTap];

    [singleTap requireGestureRecognizerToFail:doubleTap];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.viewWillAppear = YES;
    self.isWindowChromeOn = NO;

    [[UIApplication sharedApplication] setStatusBarHidden:self.playerStatePlaying];


    // self.playButton.hidden = NO;
    // self.activeButton.hidden = NO;
    /*
    self.playButton.hidden = YES;
    self.pauseButton.hidden= NO;
    self.isWindowChromeOn = NO;*/


}

/* Notifies the view controller that its view is about to be dismissed,
 covered, or otherwise hidden from view. */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.hideWindowChromeTimer invalidate];


    self.playButton.hidden = YES;
    self.activeButton.hidden = YES;

    MPMoviePlayerController *player = [self moviePlayerController];

    [player pause];

    //	[self removeMovieViewFromViewHierarchy];
    //  [self deletePlayerAndNotificationObservers];
}

- (void)doubleTapWebView:(UITapGestureRecognizer *)gesture {
    DDLogInfo(@"double-tap");
    // nothing to do here
    return;
    [self doDoubleTap];
}

- (void)singleTapWebView:(UITapGestureRecognizer *)gesture {
    DDLogInfo(@"single-tap");
    [self doSingleTap];

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)doDoubleTap {
    [self doubleTapPause];
}

- (void)doSingleTap {

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    [self.playButton setCenter:CGPointMake(CGRectGetMidX(screenRect), CGRectGetMidY(screenRect))];
    [self.pauseButton setCenter:CGPointMake(CGRectGetMidX(screenRect), CGRectGetMidY(screenRect))];



    // DDLogInfo(@"%d --- %d",[self getplayerStatePlaying],[self getisWindowChromeOn]);

    if (self.movieEnd) {
        if (!self.isWindowChromeOn) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showWindowChrome" object:nil];
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            self.playBack.hidden = YES;
            self.isWindowChromeOn = YES;
        }
        else if (self.isWindowChromeOn) {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWindowChrome" object:nil];
            [self.hideWindowChromeTimer invalidate];
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            self.playBack.hidden = NO;
            self.isWindowChromeOn = NO;
        }
        return;
    }

    if (self.playerStatePlaying && !self.isWindowChromeOn) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showWindowChrome" object:nil];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        self.playBack.hidden = YES;
        self.pauseButton.hidden = NO;
        self.activeButton.hidden = NO;
        self.isWindowChromeOn = YES;
        return;
    }
    else if (self.playerStatePlaying && self.isWindowChromeOn) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWindowChrome" object:nil];
        [self.hideWindowChromeTimer invalidate];
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        self.playBack.hidden = NO;
        self.activeButton.hidden = YES;
        self.isWindowChromeOn = NO;
        return;
    }

}

- (void)viewDidUnload {
    [self deletePlayerAndNotificationObservers];
    [super viewDidUnload];
}

/* Notifies the view controller that its view is about to be become visible. */







- (void)topBtnTap:(UIButton *)sender {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];


    self.viewWillAppear = NO;
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    [result setValue:self.thumbnailImage forKey:@"thumbImage"];
    if (appDelegate.VideoDateCreated != nil) {
        [result setValue:appDelegate.VideoDateCreated forKey:@"VideoDateCreated"];
    }


    if (self.itemEditVC == nil) {
        self.itemEditVC = [[ItemEditViewController alloc] initWithVideoInfo:result];
        DKVideoUploaderManager *uploader =
                (DKVideoUploaderManager *) [[[DKMultyUploaderManger sharedClient] allVideosUploadingProccess] lastObject];

        uploader.duration = self.duration;

        [uploader convertVideoToLowQualityWithInputURL];
    } else {
        [self.itemEditVC refreshVideoInfo:result];
    }

    [self.navigationController pushViewController:self.itemEditVC animated:YES];
}


- (void)internetConnectionRevived:(NSNotification *)notification {
    [self uploadOriginalImage];
}


- (BOOL)canBecomeFirstResponder {
    return YES;
}


- (void)dealloc {
    [self setMoviePlayerController:nil];
    [self.hideWindowChromeTimer invalidate];
    //    self.imageView = nil;
    //    self.movieBackgroundImageView = nil;
    //    self.backgroundView = nil;
    //    self.overlayController = nil;
}

/* Remove the movie view from the view hierarchy. */
- (void)removeMovieViewFromViewHierarchy {
    MPMoviePlayerController *player = [self moviePlayerController];
    [player.view removeFromSuperview];
}

#pragma mark Error Reporting

- (void)displayError:(NSError *)theError {
    if (theError) {
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Error"
                                                                    message:[theError localizedDescription]
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

@end

#pragma mark -

@implementation DKMoviePlayerViewController (OverlayView)


/* Add an overlay view on top of the movie. This view will display movie
 play states and includes a 'Close Movie' button. */


//


@end

#pragma mark -

@implementation DKMoviePlayerViewController

@synthesize moviePlayerController;
@synthesize timer;
@synthesize myMovieFileURL;
@synthesize thumbnailImage;

+ (DKMoviePlayerViewController *)sharedClient {
    static DKMoviePlayerViewController *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[DKMoviePlayerViewController alloc] initWithNibName:@"DKMoviePlayerViewController" bundle:nil];
        //         _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    });
    return _sharedClient;
}


- (void)showWindowChromeView {

}

- (void)hideWindowChromeView {

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.uploadVideoMode) {return;}

    UITouch *touch = [touches anyObject];
    if (touch.view != self.playButton && touch.view != self.pauseButton && touch.view != self.playBack) {
        [self ControlVideoFrame];
        /*
        if (self.isFrameOn) {
            DDLogInfo(@"isFrameOn");
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showWindowChrome" object:nil];
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            self.isFrameOn = NO;
            self.playBack.hidden = NO;
            
            
            
        }
        
        else {
            
            DDLogInfo(@"isFrameOFF");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWindowChrome" object:nil];
            //[self.hideWindowChromeTimer invalidate];
            [self.navigationController setNavigationBarHidden:YES animated:YES];
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            self.isFrameOn = YES;
            self.playBack.hidden = YES;
           // self.activeButton.hidden = NO;
        }*/
    }
}


- (void)ControlVideoFrame {

    if (self.playerStatePlaying && self.isFrameOn) {
        DDLogInfo(@"isFrameOn");

        [[NSNotificationCenter defaultCenter] postNotificationName:@"showWindowChrome" object:nil];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        self.isFrameOn = NO;
        // self.playBack.hidden = NO;
        self.pauseButton.hidden = NO;


    }

    else if (self.playerStatePlaying && !self.isFrameOn) {

        DDLogInfo(@"isFrameOFF");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWindowChrome" object:nil];
        //[self.hideWindowChromeTimer invalidate];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        self.isFrameOn = YES;
        // self.playBack.hidden = YES;
        self.pauseButton.hidden = YES;
        // self.activeButton.hidden = NO;
    }
}

- (void)hideWindowChrome {

    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWindowChrome" object:nil];
    //[self.hideWindowChromeTimer invalidate];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // self.playBack.hidden = NO;

    // if (self.playBack.hidden == NO) {

    return;
    /*
    self.playBack.hidden = YES;
    //self.activeButton.hidden = YES;

    self.activeButton.hidden = NO;
    self.activeButton.alpha = 1.0f;
    // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
    [UIView animateWithDuration:1.0 delay:0.0 options:0 animations:^{
        // Animate the alpha value of your imageView from 1.0 to 0.0 here
        self.activeButton.alpha = 0.0f;
    } completion:^(BOOL finished) {
        // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
        self.activeButton.hidden = YES;
        self.activeButton.alpha = 1.0f;
    }];*/
    // }
}

- (void)ChangeButtonType {
    return;

    self.pauseButton.hidden = NO;
    self.pauseButton.alpha = 1.0f;
    // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
    [UIView animateWithDuration:1.0 delay:1.0 options:0 animations:^{
        // Animate the alpha value of your imageView from 1.0 to 0.0 here
        self.pauseButton.alpha = 0.0f;
    }                completion:^(BOOL finished) {
        // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
        self.pauseButton.hidden = YES;
        self.pauseButton.alpha = 1.0f;
    }];
}

- (IBAction)tapPlay:(id)sender {

    if ([[UIScreen mainScreen] bounds].size.height != 568) {
        self.playButton.frame = CGRectMake(self.playButton.frame.origin.x, 177.0f, self.playButton.frame.size.width, self.playButton.frame.size.height);
        self.pauseButton.frame = CGRectMake(self.pauseButton.frame.origin.x, 177.0f, self.pauseButton.frame.size.width, self.pauseButton.frame.size.height);
    }


    //[self.playButton setCenter:CGPointMake(CGRectGetMidX([self.view bounds]), CGRectGetMidY([self.view bounds]))];
    //[self.pauseButton setCenter:CGPointMake(CGRectGetMidX([self.view bounds]), CGRectGetMidY([self.view bounds]))];


    self.playButton.hidden = YES;
    self.playBack.hidden = NO;



    // [self performSelector:@selector(ChangeButtonType) withObject:nil afterDelay:0.0];
    /*
    self.playButton.alpha = 1.0f;
    // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
    [UIView animateWithDuration:0.5 delay:0.0 options:0 animations:^{
        // Animate the alpha value of your imageView from 1.0 to 0.0 here
        self.playButton.alpha = 0.0f;
    } completion:^(BOOL finished) {
        // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
        self.playButton.hidden = YES;
        self.playButton.alpha = 1.0f;
    }];*/

    //self.playButton.hidden  = YES;
    // self.pauseButton.hidden = NO;

    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopPlayVoice" object:nil];

    [moviePlayerController play];


    self.activeButton = self.pauseButton;
    [self updatePlaybackProgressFromTimer:nil];

    //

    if (self.isWindowChromeOn) {
        [self performSelector:@selector(doSingleTap) withObject:nil afterDelay:0.0f];
    }

    if (self.playerStatePlaying && self.uploadVideoMode) {
        self.isFrameOn = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWindowChrome" object:nil];
        //[self.hideWindowChromeTimer invalidate];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];


    }
    /*
    if (self.hideWindowChromeTimer == nil && self.uploadVideoMode) {
        self.hideWindowChromeTimer = [NSTimer scheduledTimerWithTimeInterval:0.f target:self selector:@selector(hideWindowChrome) userInfo:nil repeats:NO];
    }*/
}

- (void)hideWindowChromeAfterDelay {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWindowChrome" object:nil];
    [self.hideWindowChromeTimer invalidate];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.playBack.hidden = YES;
    self.activeButton.hidden = YES;
    self.isWindowChromeOn = NO;
}

- (void)doubleTapPause {

    //[self.hideWindowChromeTimer invalidate];
    [moviePlayerController pause];
    self.playButton.hidden = NO;
    self.pauseButton.hidden = YES;
    self.isWindowChromeOn = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showWindowChrome" object:nil];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.activeButton.hidden = NO;
    [self updatePlaybackProgressFromTimer:nil];
}

- (IBAction)tapPause:(id)sender {
    [self.hideWindowChromeTimer invalidate];
    [moviePlayerController pause];
    self.playButton.hidden = NO;
    self.pauseButton.hidden = YES;
    self.isWindowChromeOn = YES;
    /*
    [self.hideWindowChromeTimer invalidate];
    [moviePlayerController pause];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showWindowChrome" object:nil];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.playBack.hidden = NO;
    self.pauseButton.hidden = NO;
    self.activeButton.hidden = NO;
    self.isWindowChromeOn = YES;
    [self updatePlaybackProgressFromTimer:nil];*/

    /*
     [self.hideWindowChromeTimer invalidate];
     [moviePlayerController pause];
     
     
     self.playButton.hidden  = NO;
     self.pauseButton.hidden = YES;
     self.activeButton = self.playButton;
     self.myComplition = nil;
     [self updatePlaybackProgressFromTimer:nil];*/

    /*
    [self.hideWindowChromeTimer invalidate];
    [moviePlayerController pause];
    
    
    self.playButton.hidden  = NO;
    self.pauseButton.hidden = YES;
    self.activeButton = self.playButton;
    self.myComplition = nil;
    [self updatePlaybackProgressFromTimer:nil];*/
}


- (void)viewWillEnterForeground {
    /* Set the movie object settings (control mode, background color, and so on)
     in case these changed. */
    //	[self applyUserSettingsToMoviePlayer];
}

#pragma mark Play Movie Actions

/* Called soon after the Play Movie button is pressed to play the local movie. */
- (void)playMovieFile:(NSURL *)movieFileURL {

    [self createAndPlayMovieForURL:movieFileURL sourceType:MPMovieSourceTypeFile];
}

/* Called soon after the Play Movie button is pressed to play the streaming movie. */
- (void)playMovieStream:(NSURL *)movieFileURL bufferReady:(BOOLBlock)ready {

    self.myComplition = ready;
    MPMovieSourceType movieSourceType = MPMovieSourceTypeUnknown;
    /* If we have a streaming url then specify the movie source type. */
    if ([[movieFileURL pathExtension] compare:@"m3u8" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        movieSourceType = MPMovieSourceTypeStreaming;
    }

    // DDLogInfo(@"%@",movieFileURL);

    [self createAndPlayMovieForURL:movieFileURL sourceType:movieSourceType];
    //[self tapPlay:nil];
}

@end

#pragma mark -
#pragma mark Movie Player Controller Methods
#pragma mark -

@implementation DKMoviePlayerViewController (MovieControllerInternal)

- (void)uploadOriginalImage {
    NSAssert(NO, @"uploadOriginalImage is not implemented");
}

#pragma mark Create and Play Movie URL

- (void)createAndConfigurePlayerWithURL:(NSURL *)movieURL sourceType:(MPMovieSourceType)sourceType {
    /* Create a new movie player object. */
    //    if (!movieURL) {
    //        movieURL = self.myMovieFileURL;
    //        sourceType = MPMovieSourceTypeFile;
    //    }



    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    [self setMoviePlayerController:player];


    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updatePlaybackProgressFromTimer:) userInfo:nil repeats:YES];


    [self installMovieNotificationObservers];

    moviePlayerController.shouldAutoplay = NO;
    NSNumber *time1 = [NSNumber numberWithFloat:0.1f];
    [moviePlayerController requestThumbnailImagesAtTimes:[NSArray arrayWithObject:time1] timeOption:MPMovieTimeOptionNearestKeyFrame];
    [moviePlayerController setMovieSourceType:sourceType];
    moviePlayerController.scalingMode = MPMovieScalingModeAspectFit;
    moviePlayerController.controlStyle = MPMovieControlStyleNone;




    /*
    if ([[UIScreen mainScreen] bounds].size.height != 568) {
        
        DDLogInfo(@"My view frame: %@", NSStringFromCGRect(self.view.frame));
        
        [[moviePlayerController view] setFrame:CGRectMake(0, 0, 320, 480)];
        //vFrame = CGRectMake(self.view.frame.origin.x, 177.0f, self.playButton.frame.size.width, self.playButton.frame.size.height);
    }
    else*/

    CGRect screenRect = [[UIScreen mainScreen] bounds];

    [[moviePlayerController view] setFrame:screenRect];


    self.rightButtonEnabled = NO;

}


- (void)weGotAnImage:(NSNotification *)note {
    UIImage *thumb = [note.userInfo objectForKey:MPMoviePlayerThumbnailImageKey];
    if (thumb) {
        self.thumbnailImage = thumb;
        //self.rightButtonEnabled = YES;
        [self setWeGotAnImage:YES];
        [[DKMultyUploaderManger sharedClient] addUploaderWithMovieURL:myMovieFileURL andThumbImage:self.thumbnailImage];


    }
}


- (void)createAndPlayMovieForURL:(NSURL *)movieURL sourceType:(MPMovieSourceType)sourceType {
    myMovieFileURL = movieURL;
    [self createAndConfigurePlayerWithURL:movieURL sourceType:sourceType];
}

#pragma mark Movie Notification Handlers

/*  Notification called when the movie finished playing. */
- (void)moviePlayBackDidFinish:(NSNotification *)notification {
    NSNumber *reason = [notification userInfo][MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([reason integerValue]) {
        /* The end of the movie was reached. */
        case MPMovieFinishReasonPlaybackEnded:

            [self setMovieEnd:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showWindowChrome" object:nil];
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            self.playBack.hidden = self.uploadVideoMode ? NO : YES;
            self.pauseButton.hidden = YES;
            self.playButton.hidden = NO;
            self.activeButton.hidden = YES;
            self.isWindowChromeOn = YES;
            self.playerStatePlaying = NO;

            CGRect screenRect = [[UIScreen mainScreen] bounds];
            [self.playButton setCenter:CGPointMake(CGRectGetMidX(screenRect), CGRectGetMidY(screenRect))];
            [self.pauseButton setCenter:CGPointMake(CGRectGetMidX(screenRect), CGRectGetMidY(screenRect))];
            //self.uploadVideoMode = YES;
            /*
            self.playButton.hidden  = NO;
            self.pauseButton.hidden = YES;
            self.activeButton = self.playButton;
            [self.hideWindowChromeTimer invalidate];*/
            break;

            /* An error was encountered during playback. */
        case MPMovieFinishReasonPlaybackError:
            DDLogInfo(@"An error was encountered during playback   %@", [notification userInfo]);
            [self performSelectorOnMainThread:@selector(displayError:) withObject:[notification userInfo][@"error"]
                                waitUntilDone:NO];
            [self removeMovieViewFromViewHierarchy];
            //            [self removeOverlayView];
            //            [self.backgroundView removeFromSuperview];
            break;

            /* The user stopped playback. */
        case MPMovieFinishReasonUserExited:
            [self removeMovieViewFromViewHierarchy];
            //            [self removeOverlayView];
            //            [self.backgroundView removeFromSuperview];
            break;

        default:
            break;
    }
}


/* Handle movie load state changes. */
- (void)loadStateDidChange:(NSNotification *)notification {
    MPMoviePlayerController *player = notification.object;
    MPMovieLoadState loadState = player.loadState;


    if (loadState == MPMoviePlaybackStatePlaying) {
        // DDLogInfo(@"00000--000000");
        [self setPlayerStatePlaying:YES];
    }
    if (loadState == MPMoviePlaybackStatePaused) {
        // DDLogInfo(@"00000--000011");
        [self setPlayerStatePlaying:NO];
    }
    if (loadState == MPMoviePlaybackStateStopped) {
        // DDLogInfo(@"00000--000011");
        [self setPlayerStatePlaying:NO];
    }
    /* The load state is not known at this time. */
    if (loadState & MPMovieLoadStateUnknown) {
        // DDLogInfo(@"111111111111");
    }
    //
    //	/* The buffer has enough data that playback can begin, but it
    //	 may run out of data before playback finishes. */
    if (loadState & MPMovieLoadStatePlayable) {
        // DDLogInfo(@"22222222222222");
        if (self.myComplition) {
            self.myComplition(YES);
        }
    }
    //
    //	/* Enough data has been buffered for playback to continue uninterrupted. */
    if (loadState & MPMovieLoadStatePlaythroughOK) {
        //DDLogInfo(@"3333333333333333"); // Add an overlay view on top of the movie view

    }
    //
    //	/* The buffering of data has stalled. */
    if (loadState & MPMovieLoadStateStalled) {
        // DDLogInfo(@"444444444444444");
    }
}

/* Called when the movie playback state has changed. */
- (void)moviePlayBackStateDidChange:(NSNotification *)notification {
    MPMoviePlayerController *player = notification.object;

    // DDLogInfo(@"555555555555");

    if (player.playbackState == MPMoviePlaybackStateStopped) {
        self.movieEnd = NO;
        [self setPlayerStatePlaying:NO];
        DDLogInfo(@"PLAYER STOP");
    }
    else if (player.playbackState == MPMoviePlaybackStatePlaying) {
        DDLogInfo(@"PLAYER PLAY");
        self.movieEnd = NO;
        [self setPlayerStatePlaying:YES];
        if (self.playerWasActive) {
            [self ChangeButtonType];
        }
    }
    else if (player.playbackState == MPMoviePlaybackStatePaused) {
        DDLogInfo(@"PLAYER PAUSE");
        self.movieEnd = NO;
        [self setPlayerStatePlaying:NO];
        [self setPlayerWasActive:YES];

    }
    //	{
    //        [overlayController setPlaybackStateDisplayString:@"playing"];
    //	}
    //	/* Playback is currently stopped. */
    //	if (player.playbackState == MPMoviePlaybackStateStopped)
    //	{
    //        [overlayController setPlaybackStateDisplayString:@"stopped"];
    //	}
    //	/*  Playback is currently under way. */
    //	else if (player.playbackState == MPMoviePlaybackStatePlaying)
    //	{
    //        [overlayController setPlaybackStateDisplayString:@"playing"];
    //	}
    //	/* Playback is currently paused. */
    //	else if (player.playbackState == MPMoviePlaybackStatePaused)
    //	{
    //        [overlayController setPlaybackStateDisplayString:@"paused"];
    //	}
    //	/* Playback is temporarily interrupted, perhaps because the buffer
    //	 ran out of content. */
    //	else if (player.playbackState == MPMoviePlaybackStateInterrupted)
    //	{
    //        [overlayController setPlaybackStateDisplayString:@"interrupted"];
    //	}
}

/* Notifies observers of a change in the prepared-to-play state of an object
 conforming to the MPMediaPlayback protocol. */
- (void)mediaIsPreparedToPlayDidChange:(NSNotification *)notification {
    [self.view addSubview:[moviePlayerController view]];

    if (self.playerStatePlaying) {
        self.playButton.hidden = YES;
    }
    else {self.playButton.hidden = self.uploadVideoMode ? NO : NO;}
    //self.pauseButton.hidden= YES;



    [self.view bringSubviewToFront:self.playButton];
    [self.view bringSubviewToFront:self.pauseButton];
    [self.view bringSubviewToFront:self.playBack];
    [_playBack.slider addTarget:self action:@selector(skipTo:) forControlEvents:UIControlEventTouchUpInside];
    [_playBack.slider addTarget:self action:@selector(skipTo:) forControlEvents:UIControlEventTouchUpOutside];


}

#pragma mark Install Movie Notifications

/* Register observers for the various movie object notifications. */
- (void)installMovieNotificationObservers {
    MPMoviePlayerController *player = [self moviePlayerController];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadStateDidChange:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                 name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                               object:player];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:player];
    if (!self.myComplition) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(weGotAnImage:)
                                                     name:MPMoviePlayerThumbnailImageRequestDidFinishNotification
                                                   object:player];
    }


}

#pragma mark Remove Movie Notification Handlers

/* Remove the movie notification observers from the movie object. */
- (void)removeMovieNotificationHandlers {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }

    MPMoviePlayerController *player = [self moviePlayerController];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:player];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:player];

}

/* Delete the movie player object, and remove the movie notification observers. */
- (void)deletePlayerAndNotificationObservers {
    [self removeMovieNotificationHandlers];
    [self setMoviePlayerController:nil];
}

#pragma mark Movie Settings

/* Apply user movie preference settings (these are set from the Settings: iPhone Settings->Movie Player)
 for scaling mode, control style, background color, repeat mode, application audio session, background
 image and AirPlay mode.
 */

- (void)skipTo:(UISlider *)slider {
    [self.moviePlayerController setCurrentPlaybackTime:slider.value * self.moviePlayerController.duration];
    [self updatePlaybackProgressFromTimer:nil];
}

- (void)updatePlaybackProgressFromTimer:(id)sender {


    if (self.playBack != nil) {


        float sec = round(self.moviePlayerController.duration);
        [self setDuration:[NSString stringWithFormat:@"%d", (int) sec]];

        [_playBack.currentTime setText:[self formatTime:self.moviePlayerController.currentPlaybackTime]];
        [_playBack.remainingTime setText:[self formatTime:self.moviePlayerController.duration]];
        [_playBack.slider setValue:(self.moviePlayerController.currentPlaybackTime / self.moviePlayerController.duration)];
        [_playBack.loading setProgress:(self.moviePlayerController.playableDuration / self.moviePlayerController.duration)];

        if (self.weGotAnImage) {
            self.rightButtonEnabled = YES;
        }

    }
}

- (NSString *)formatTime:(float)seconds {
    int s = seconds;
    int m = floor(s / 60.0);
    int h = floor(m / 60.0);
    if (h >= 1) {
        m = m - (h * 60);
        s = s - (h * 60 * 60);
    }
    if (m >= 1) {
        s = s - (m * 60);
    }
    NSString *sec = [NSString stringWithFormat:@"%d", (int) floor(s)];
    NSString *min = [NSString stringWithFormat:@"%d", (int) floor(m)];
    //    NSString* hour = [NSString stringWithFormat:@"%d",(int)floor(h)];
    if (m < 10) {
        min = [NSString stringWithFormat:@"%@", min];
    }
    if (s < 10) {
        sec = [NSString stringWithFormat:@"%@%@", @"0", sec];
    }
    return [NSString stringWithFormat:@"%@:%@", min, sec];
}

@end



