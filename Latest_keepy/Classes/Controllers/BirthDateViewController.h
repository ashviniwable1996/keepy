//
//  BirthDateViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@class BirthDateViewController;

@protocol BirthDateViewControllerDelegate <NSObject>

- (void)birthDateViewControllerDidCancel:(BirthDateViewController *)vc;

- (void)birthDateViewControllerDidSave:(BirthDateViewController *)vc;

@end

@interface BirthDateViewController : UIViewController

@property(nonatomic, strong) NSDate *birthDateDate;
@property(nonatomic, weak) id <BirthDateViewControllerDelegate> delegate;

@end
