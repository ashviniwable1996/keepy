//
//  PreRegViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 1/14/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "PreRegViewController.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"

@interface PreRegViewController () <UITextFieldDelegate>

@property(nonatomic, strong) UITextField *lEmailTF;
@property(nonatomic, strong) UIScrollView *scrollView;

@end

@implementation PreRegViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"Pre signup screen"];


    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardDidShow:)
                   name:UIKeyboardDidShowNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];

    self.title = KPLocalizedString(@"sign up");
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", @(NO), @"disabled", nil] andRightButtonInfo:nil];
    //self.lEmailTF.placeholder = KPLocalizedString(@"enter email");

    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollView];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gesture];

    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 136, 140, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xC9BFBC);
    [self.scrollView addSubview:lineView];

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(140, 126, 42, 21)];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:14];
    lbl.textColor = UIColorFromRGB(0x523c37);
    lbl.text = KPLocalizedString(@"or");
    lbl.textAlignment = NSTextAlignmentCenter;
    [self.scrollView addSubview:lbl];

    lineView = [[UIView alloc] initWithFrame:CGRectMake(180, 136, 140, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xC9BFBC);
    [self.scrollView addSubview:lineView];

    UIImageView *txtBkg = [[UIImageView alloc] initWithFrame:CGRectMake((320 - 266) / 2, 180, 266, 44)];
    txtBkg.image = [[UIImage imageNamed:@"input-field"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 26, 0, 26)];
    [self.scrollView addSubview:txtBkg];

    self.lEmailTF = [[UITextField alloc] initWithFrame:CGRectMake((320 - 221) / 2, 187, 221, 30)];
    self.lEmailTF.backgroundColor = [UIColor clearColor];
    self.lEmailTF.placeholder = KPLocalizedString(@"enter email");
    self.lEmailTF.keyboardType = UIKeyboardTypeEmailAddress;
    self.lEmailTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.lEmailTF.returnKeyType = UIReturnKeyGo;
    self.lEmailTF.delegate = self;
    [self.scrollView addSubview:self.lEmailTF];

    //login
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[[UIImage imageNamed:@"login-continue-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];

    [btn setTitleColor:UIColorFromRGB(0x3ABEDC) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:18];
    btn.titleEdgeInsets = UIEdgeInsetsMake(-2, 0, 0, 0);
    [btn setTitle:KPLocalizedString(@"continue") forState:UIControlStateNormal];

    btn.frame = CGRectMake((self.view.frame.size.width - 266) / 2, 240, 266, 44);
    [btn addTarget:self action:@selector(continueTap:) forControlEvents:UIControlEventTouchUpInside];

    [self.scrollView addSubview:btn];

    //facebook
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 0;
    [btn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:18];
    btn.frame = CGRectMake((self.view.frame.size.width - 266) / 2, 60, 266, 44);
    [btn setBackgroundImage:[[UIImage imageNamed:@"FB-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
    //[btn setImage:[UIImage imageNamed:@"f-icon"] forState:UIControlStateNormal];
    //btn.imageEdgeInsets = UIEdgeInsetsMake(0, 150, 0, 0);
    //btn.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);
    [btn setTitle:KPLocalizedString(@"facebook") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(fbloginTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:btn];
}

- (void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    self.scrollView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)continueTap:(UIButton *)sender {
    RegisterViewController *avc = [[RegisterViewController alloc] initWithEmail:self.lEmailTF.text];
    [self.navigationController pushViewController:avc animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self continueTap:nil];

    return YES;
}

- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self.lEmailTF resignFirstResponder];
}

- (void)facebookToken:(NSString*)token {
    [[ServerComm instance] fblogin:token
                  withSuccessBlock:^(id result) {
                      DDLogInfo(@"%@", result);
                      [SVProgressHUD dismiss];
                      
                      if ([[result valueForKey:@"status"] intValue] == 0) {
                          result = [result valueForKey:@"result"];
                          [GlobalUtils hideAllTips];
                          
                          AppDelegate *d = (AppDelegate *) [[UIApplication sharedApplication] delegate];
                          [d createHome:YES];
                          [d.navController setViewControllers:[NSArray arrayWithObject:d.viewController] animated:YES];
                          
                          [[UserManager sharedInstance] setIsAfterLogin:YES];
                          
                          [GlobalUtils saveUser:result];
                          
                      } else {
                          [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id fbresult, NSError *error) {
                              if (error) {
                                  [[Mixpanel sharedInstance] track:@"onboarding-splash-facebook-login-error"];
                                  [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
                                  DDLogInfo(@"signUpNew error: %@", error);
                                  return;
                              }
                          }];
                      }
                  }
                      andFailBlock:^(NSError *error) {
                          DDLogInfo(@"%@", error.localizedDescription);
                          [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
                      }
     ];
}

- (void)fbloginTap:(id)sender {

    [[Mixpanel sharedInstance] track:@"Facebook tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"pre-signup", @"source", nil]];


    [SVProgressHUD showWithStatus:KPLocalizedString(@"signing in")];

    __weak PreRegViewController* weakSelf = self;
    
    FBSDKLoginManager* fb = [FBSDKLoginManager new];
    [fb logOut];
    [fb logInWithReadPermissions:@[@"email"]
#if FBSDK_46
              fromViewController:self
#endif
                         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                             if (error) {
                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: %@", error);
                                 [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
                                 
                             } else if (result.isCancelled) {
                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Cancelled");
                                 [SVProgressHUD dismiss];
                             } else {
                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Logged in");
                                 [weakSelf facebookToken:result.token.tokenString];
                             }
                         }
     ];
}

#pragma mark - Keyboard hide/show

- (void)keyboardDidShow:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(54.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(54.0, 0.0, 0.0, 0.0);
    self.scrollView.contentInset = contentInsets;

    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 10, 10) animated:YES];
}

@end