//
//  RegisterViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/7/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@interface RegisterViewController : KPPopViewController

- (instancetype)initWithMode:(int)mode;

- (instancetype)initWithFacebook:(NSDictionary *)fbData withAccessToken:(NSString *)fbAccessToken;

- (instancetype)initWithEmail:(NSString *)email;

@end
