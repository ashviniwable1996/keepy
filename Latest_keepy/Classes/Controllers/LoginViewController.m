//
//  LoginViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 3/17/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "LoginViewController.h"
#import "AppDelegate.h"

@interface LoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintOfBackButton;

@property(nonatomic, strong) IBOutlet UITextField *lEmailTF;
@property(nonatomic, strong) IBOutlet UITextField *lPasswordTF;
@property(nonatomic, strong) IBOutlet UIButton *passwordResetButton;
@property(nonatomic, strong) IBOutlet UIView *container;
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *keepyLogo;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraintOfFbIcon;

- (IBAction)forgotPasswordTap:(id)sender;

@end

@implementation LoginViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"Login screen"];
    [[Mixpanel sharedInstance] track:@"onboarding-login-open"];


    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"login", @"source", nil]];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardDidShow:)
                   name:UIKeyboardDidShowNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];

    self.title = KPLocalizedString(@"login");
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", @(NO), @"disabled", nil] andRightButtonInfo:nil];

    for (UIControl *control in self.container.subviews) {
        if ([control isKindOfClass:[UILabel class]]) {
            [(UILabel *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:14]];
            [(UILabel *) control setTextColor:UIColorFromRGB(0x329fc7)];
            if ([[(UILabel *) control text] isEqualToString:@"or"]) {
                [(UILabel *) control setTextColor:[UIColor whiteColor]];
            }

        }
        if ([control isKindOfClass:[UITextField class]]) {
            [(UITextField *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:17]];
//            [(UITextField *) control setTextColor:UIColorFromRGB(0x4d3c36)];
        }

    }


    UIColor *color = [UIColor whiteColor];
    self.lEmailTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:KPLocalizedString(@"Enter your email") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:@"ARSMaquettePro-LightItalic" size:16]}];

    self.lPasswordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:KPLocalizedString(@"Enter your password") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:@"ARSMaquettePro-LightItalic" size:16]}];
    
    [self.passwordResetButton setTitle:[NSString stringWithFormat:@"> %@", KPLocalizedString(@"Forgot password")] forState:UIControlStateNormal];
    [self.passwordResetButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:14]];
    [self.passwordResetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [self.backButton.titleLabel setFont:[UIFont fontWithName:@"ARSMaquettePro-Medium" size:13]];
    [self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    [self.lEmailTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.lPasswordTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gesture];

    if ([UIScreen mainScreen].bounds.size.height == 812) {
        self.topConstraintOfBackButton.constant = 55;
    }

}


- (void)textFieldDidChange:(UITextField *)textField{
    
    [self checkEnableSaveButton];
}


- (void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.container.frame.size.width, self.container.frame.size.height);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    //login
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [loginButton setBackgroundImage:[[UIImage imageNamed:@"login-continue-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
    [loginButton setBackgroundImage:[[UIImage imageNamed:@"button"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];
    
    [loginButton setTitleColor:UIColorFromRGB(0x3ABEDC) forState:UIControlStateNormal];
    loginButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    loginButton.titleEdgeInsets = UIEdgeInsetsMake(-2, 0, 0, 0);
    [loginButton setTitle:KPLocalizedString(@"Login") forState:UIControlStateNormal];
    
    //loginButton.frame = CGRectMake((self.view.frame.size.width - 266) / 2, self.lPasswordTF.frame.origin.y + self.lPasswordTF.frame.size.height + 40.0, 266, 44);
    //CGPoint center = CGPointMake([self.view bounds].size.width/2.0, [self.view bounds].size.height/2.0);
    //[loginButton setCenter:center];
    
    //266+ 23.5+23.5
    //modified by amitg
    //    loginButton.frame = CGRectMake(([self.view bounds].size.width - 314) /2, self.lPasswordTF.frame.origin.y + self.lPasswordTF.frame.size.height + 40.0, 266, 44);
    loginButton.frame = CGRectMake(40, self.lPasswordTF.frame.origin.y + self.lPasswordTF.frame.size.height + 40.0, self.view.frame.size.width - 80, 44);
    self.leadingConstraintOfFbIcon.constant = 10.0;
    if ([[UIScreen mainScreen] bounds].size.width == 320.0f) {
        loginButton.frame = CGRectMake( 40, self.lPasswordTF.frame.origin.y + self.lPasswordTF.frame.size.height + 40.0, 240, 44);
    }
    
    [loginButton addTarget:self action:@selector(loginTap:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.container addSubview:loginButton];
    
    //facebook
    self.facebookButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:17];
    //266+ 23.5+23.5
    //modified by amitg
    //    self.facebookButton.frame = CGRectMake((self.view.frame.size.width - 314) / 2, self.keepyLogo.frame.origin.y + self.keepyLogo.frame.size.height + 30.0, 266, 44);
    
    //[btn setImage:[UIImage imageNamed:@"f-icon"] forState:UIControlStateNormal];
    //btn.imageEdgeInsets = UIEdgeInsetsMake(0, 150, 0, 0);
    //btn.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);
    [self.facebookButton setTitle:KPLocalizedString(@"Login with Facebook") forState:UIControlStateNormal];
    [self.facebookButton addTarget:self action:@selector(fbloginTap:) forControlEvents:UIControlEventTouchUpInside];
    //[self.container addSubview:btn];
    
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        [self.passwordResetButton setCenter:CGPointMake(self.passwordResetButton.center.x, self.passwordResetButton.center.y - 45.0f)];
    }

    //[self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [super viewWillDisappear:animated];
}



- (void)loginTap:(UIButton *)sender {
    [SVProgressHUD showWithStatus:KPLocalizedString(@"signing in")];
    [[ServerComm instance] login:self.lEmailTF.text withPassword:self.lPasswordTF.text withSuccessBlock:^(id result) {

        int rstatus = [[result valueForKey:@"status"] intValue];
        if (rstatus == 0) {
            [[Mixpanel sharedInstance] track:@"Login Success"];

            // [SVProgressHUD dismiss]; // TODO: re-enable progress dismissal

            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"login", @"source", @"ok", @"result", nil]];
            if ([result valueForKey:@"result"] != nil ){
            [self.delegate didLogin:[result valueForKey:@"result"]];
        }
        }
        else {
            [[Mixpanel sharedInstance] track:@"Login Error"];


            if ([result valueForKey:@"error"] == nil) {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
                return;
            }
            /*
            NSMutableString *errorMsg = [[NSMutableString alloc] init];
            NSArray *errors = [[result valueForKey:@"error"] valueForKey:@"errors"];
            if ([errors count] == 0)
                [errorMsg appendString:@"wrong email or password"];
            else
                [errorMsg appendString:@"please fix the following:\n"];
            for (NSString *errorStr in errors)
            {
                [errorMsg appendFormat:@"%@\n", errorStr];
            }
            
            [SVProgressHUD showError:@"%@", errorMsg];
             */
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"wrong email or password")];
        }

    }               andFailBlock:^(NSError *error) {

        if (![GlobalUtils isConnected]) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"There is a problem with your internet connection")];

        }
        else {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
        }
    }];
}

#pragma mark - UITextFieldDelegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.lEmailTF) {
        [self.lPasswordTF becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self loginTap:nil];
    }

    return YES;
}

/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    self.passwordResetButton.enabled = YES;
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self checkEnableSaveButton];
    return NO;
}
*/

- (void)checkEnableSaveButton {
    self.rightButtonEnabled = (self.lEmailTF.text.length > 0 && self.lPasswordTF.text.length > 1);
}

- (IBAction)forgotPasswordTap:(id)sender {
    [self.lEmailTF resignFirstResponder];
    [self.lPasswordTF resignFirstResponder];
    if (self.lEmailTF.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"Please enter your email"];
    }
    else {
        self.passwordResetButton.enabled = NO;
        [[ServerComm instance] forgotPassword:self.lEmailTF.text withSuccessBlock:^(id result) {
            if ([[result valueForKey:@"status"] intValue] == 0) {
                [SVProgressHUD showSuccessWithStatus:KPLocalizedString(@"a password reset link have been sent to your email.")];
            }
            else {
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"Could not find user with this email")];
            }
        }                        andFailBlock:^(NSError *error) {
            self.passwordResetButton.enabled = YES;
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
        }];
    }

}

- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self.lEmailTF resignFirstResponder];
    [self.lPasswordTF resignFirstResponder];
}

- (void)facebookToken:(NSString*)token {
    [[ServerComm instance] fblogin:token
                  withSuccessBlock:^(id result) {
                      DDLogInfo(@"%@", result);
                      [SVProgressHUD dismiss];
                      
                      if ([[result valueForKey:@"status"] intValue] == 0) {
                          result = [result valueForKey:@"result"];
                          [GlobalUtils hideAllTips];
                          
                          NSString* email = [result valueForKey:@"email"];
                          
                          AppDelegate *d = (AppDelegate *) [[UIApplication sharedApplication] delegate];
                          [d initializeDataStoreForEmail:email];
                          [d createHome:YES];
                          [d.navController setViewControllers:[NSArray arrayWithObject:d.viewController] animated:YES];
                          
                          [[UserManager sharedInstance] setIsAfterLogin:YES];
                          
                          [GlobalUtils saveUser:result];
                      } else {
                          [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"you are not registered to keepy with this facebook account")];
                          
                      }
                  }
                      andFailBlock:^(NSError *error) {
                          DDLogInfo(@"%@", error.localizedDescription);
                          [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
                      }
     ];
}

- (void)fbloginTap:(id)sender {
    [[Mixpanel sharedInstance] track:@"Facebook tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"login", @"source", nil]];
    
    __weak LoginViewController* weakSelf = self;

    FBSDKLoginManager* fb = [FBSDKLoginManager new];
    [fb logOut];
    [fb logInWithReadPermissions: @[@"email"]
#if FBSDK_46
              fromViewController:self
#endif
                         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                             if (error) {
                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: %@", error);
                                 [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting through facebook, please try again soon")];
                                 
                             } else if (result.isCancelled) {
                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Cancelled");
                                 [SVProgressHUD dismiss];
                             } else {
                                 DDLogInfo(@"FBSDKLoginManager -logInWithReadPermissions: Logged in");
                                 [weakSelf facebookToken:result.token.tokenString];
                             }
                         }
     ];
}

#pragma mark - Keyboard hide/show

- (void)keyboardDidShow:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(54.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0.0, 0.0);
    self.scrollView.contentInset = contentInsets;

//    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 10, 10) animated:YES];
}

@end
