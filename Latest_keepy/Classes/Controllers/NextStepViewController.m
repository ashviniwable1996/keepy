//
//  NextStepViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 8/11/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "NextStepViewController.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>

@interface NextStepViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSArray *items;
@property(nonatomic, strong) NSArray *stepsDone;
@property(nonatomic, strong) UIView *firstView;

@end

@implementation NextStepViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"what-now", @"source", nil]];


    self.title = KPLocalizedString(@"what now?");
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"close"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];

    self.view.userInteractionEnabled = YES;

    self.items = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add my first kid"), @"title", KPLocalizedString(@"add your kid"), @"text", UIColorFromRGB(0x66C165), @"color", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add kid"), @"title", @"add-kid-icon", @"image", nil], nil], @"buttons", nil], [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add my first keepy"), @"title", KPLocalizedString(@"take a photo of your kids' drawings, birthday cards, lego creations, face paintings or anything else offline that you want to KEEP online"), @"text", UIColorFromRGB(0x00C0D3), @"color", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add note"), @"title", @"wo-add-note", @"image", nil], [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add/upload photo"), @"title", @"add-keepy-icon", @"image", nil], nil], @"buttons", nil], [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add a fan"), @"title", KPLocalizedString(@"invite grandparents and loved ones to be your kids\' \"fan\" so they can see all your keepies and leave video comments on them."), @"text", UIColorFromRGB(0xF68D43), @"color", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"add a fan"), @"title", @"add-fan-icon", @"image", nil], nil], @"buttons", nil], [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"get free keepies!"), @"title", KPLocalizedString(@"for each friend that signs up and uploads at least one keepy, you both will get 5 free keepies...so spread the word!"), @"text", UIColorFromRGB(0xFA4D7C), @"color", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"tell a friend"), @"title", @"get-5-keepies-icon", @"image", nil], nil], @"buttons", nil], [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"connect to dropbox"), @"title", KPLocalizedString(@"use your dropbox account to have an additional backup and automatically synchronize all your keepies to your computer."), @"text", UIColorFromRGB(0x2EA3EB), @"color", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"connect to dropbox"), @"title", @"db-icon", @"image", nil], nil], @"buttons", nil],
                    nil];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.scrollsToTop = YES;
    self.tableView.backgroundView = nil;

    self.tableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];

    [self refresh];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeNotification) name:CLOSE_NEXT_STEP_NOTIFICATION object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:DROPBOX_REFRESHED_NOTIFICATION object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)topBtnTap:(id)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"what-now", @"source", @"cancel", @"result", nil]];

    [[Mixpanel sharedInstance] track:@"what-now-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"close", @"btn", nil]];


    [self.delegate nextStepClosed];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)viewDidLayoutSubviews {
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    [self.tableView reloadData];
}

- (void)refresh {
    BOOL step1 = YES;
    BOOL step2 = ([[Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]] count] > 0);

    BOOL step3 = ([[[UserManager sharedInstance] myFans] count] > 0);
    BOOL step4 = [[UserManager sharedInstance] didSpreadLove];
    BOOL step5 = (DBClientsManager.authorizedClient || DBClientsManager.authorizedTeamClient);

    self.stepsDone = [NSArray arrayWithObjects:@(step1), @(step2), @(step3), @(step4), @(step5), nil];
    if (step1 && step2 && step3 && step4 && step5) {
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:@(-1), @"id", KPLocalizedString(@"we hope keepy keeps you happy for now, for later, for ever."), @"body", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"ok"), @"title", @"kpyme://closenextstep", @"link", nil], nil], @"buttons", nil];
            [GlobalUtils showKeepyPopup:d];
        });
    }
    else {
        [self.tableView reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *cellIdentifier = @"ACell";
    //if ([[self.stepsDone objectAtIndex:indexPath.row] boolValue])
    //    cellIdentifier = @"BCell";

    float ah = (self.view.frame.size.height - 50) / 5;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.frame = CGRectMake(0.0, 0.0, 320, ah);

        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        //icon
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 36, 42)];
        [cell.contentView addSubview:img];

        //title
        UILabel *alabel = [[UILabel alloc] initWithFrame:CGRectMake(53, 68, 250, 20)];
        alabel.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        alabel.textAlignment = NSTextAlignmentLeft;
        alabel.backgroundColor = [UIColor clearColor];
        alabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
        [self.view addSubview:alabel];
        alabel.text = KPLocalizedString(@"");
        [cell.contentView addSubview:alabel];

        //done
        alabel = [[UILabel alloc] initWithFrame:CGRectMake(240, 68, 250, 20)];
        alabel.textColor = UIColorFromRGB(0x66c64f);
        alabel.textAlignment = NSTextAlignmentLeft;
        alabel.backgroundColor = [UIColor clearColor];
        alabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
        [self.view addSubview:alabel];
        alabel.text = KPLocalizedString(@"done");
        [cell.contentView addSubview:alabel];


        img = [[UIImageView alloc] initWithFrame:CGRectMake(285, 18.5, 20, 21)];
        img.image = [UIImage imageNamed:@"v"];
        [cell.contentView addSubview:img];

        //line
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.bounds.size.height - 1, cell.bounds.size.width, 1)];
        lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        lineView.backgroundColor = UIColorFromRGB(0xdcd6d4);
        [cell.contentView addSubview:lineView];

        //go button
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundImage:[[UIImage imageNamed:@"small-blue-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 16, 0, 16)] forState:UIControlStateNormal];
        [btn setTitleColor:UIColorFromRGB(0x329fc7) forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:15];
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
        [btn setTitle:KPLocalizedString(@"go") forState:UIControlStateNormal];
        btn.frame = CGRectMake(270, 7, 100, 30);
        [btn addTarget:self action:@selector(btnTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btn];
    }

    UIImageView *img = (UIImageView *) [cell.contentView.subviews objectAtIndex:0];
    //img.image = [UIImage imageNamed:@"add-keepy-icon"];
    NSArray *btns = (NSArray *) [[self.items objectAtIndex:indexPath.row] valueForKey:@"buttons"];
    img.image = [UIImage imageNamed:[[btns objectAtIndex:[btns count] - 1] valueForKey:@"image"]];


    UILabel *titleLbl = (UILabel *) [cell.contentView.subviews objectAtIndex:1];
    titleLbl.text = [NSString stringWithFormat:@"%@", [[self.items objectAtIndex:indexPath.row] valueForKey:@"title"]];

    img.frame = CGRectMake(10, (ah - img.frame.size.height) / 2, 36, 42);
    titleLbl.frame = CGRectMake(56, 0, 300, ah);

    UILabel *doneLbl = (UILabel *) [cell.contentView.subviews objectAtIndex:2];
    doneLbl.hidden = (![[self.stepsDone objectAtIndex:indexPath.row] boolValue]);
    doneLbl.frame = CGRectMake(235, 0, 100, ah);

    UIImageView *vimg = (UIImageView *) [cell.contentView.subviews objectAtIndex:3];
    vimg.hidden = (![[self.stepsDone objectAtIndex:indexPath.row] boolValue]);
    vimg.frame = CGRectMake(290, (ah - 21) / 2, 20, 21);

    UIImageView *limg = (UIImageView *) [cell.contentView.subviews objectAtIndex:4];
    limg.frame = CGRectMake(0, ah - 1, cell.bounds.size.width, 1);

    UIButton *gbtn = (UIButton *) [cell.contentView.subviews objectAtIndex:5];
    gbtn.frame = CGRectMake(260, (ah - 30) / 2, 44, 30);
    gbtn.hidden = [[self.stepsDone objectAtIndex:indexPath.row] boolValue];
    gbtn.tag = indexPath.row;

    if ([[self.stepsDone objectAtIndex:indexPath.row] boolValue]) {
        img.alpha = 0.3;
        titleLbl.alpha = 0.3;
    }
    else {
        img.alpha = 1.0;
        titleLbl.alpha = 1.0;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float ah = (self.view.frame.size.height - 50) / 5;
    return ah;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.stepsDone objectAtIndex:indexPath.row] boolValue]) {
        return;
    }

    [[Mixpanel sharedInstance] track:@"what-now-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:[[self.items objectAtIndex:indexPath.row] valueForKey:@"title"], @"btn", nil]];

    [self.delegate nextStepSelected:indexPath.row];
}

- (void)btnTap:(UIButton *)sender {
    [self.delegate nextStepSelected:sender.tag];
}

- (void)closeNotification {
    [self topBtnTap:nil];
}

@end
