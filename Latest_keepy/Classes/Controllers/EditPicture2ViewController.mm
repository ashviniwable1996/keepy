//
//  EditPicture2ViewController.m
//  TalkingCards
//
//  Created by Yaniv Solnik on 9/5/12.
//  Copyright (c) 2012 Jhaniv LTD. All rights reserved.
//

#import "EditPicture2ViewController.h"
#import "UIImage+Resize.h"

@interface EditPicture2ViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    float firstX;
    float firstY;
    float imageScale;
    BOOL goingToNextScreen;
}

@property(nonatomic, strong) UIImageView *originalImageView;
@property(nonatomic, strong) UIImageView *croppedImageView;
@property(nonatomic, strong) UIImage *orgImage;
@property(nonatomic, strong) UIImage *resultImage;

- (void)createCroppedImage;

@end

@implementation EditPicture2ViewController

@synthesize originalImageView = _originalImageView;
@synthesize croppedImageView = _croppedImageView;

- (instancetype)initWithImage:(UIImage *)image {
    self = [super initWithNibName:@"EditPicture2ViewController" bundle:nil];
    if (self) {
        self.orgImage = image;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-kid-photo", @"source", nil]];

    self.title = KPLocalizedString(@"add photo");
    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"cancel"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"done"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(NO), @"disabled", nil]];


    imageScale = 1;

    self.originalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    //self.originalImageView.contentMode = UIViewContentModeCenter;
    self.originalImageView.alpha = 0.1;
    self.originalImageView.userInteractionEnabled = YES;
    [self.view addSubview:self.originalImageView];
    [self.view sendSubviewToBack:self.originalImageView];

    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(imagePan:)];
    [self.originalImageView addGestureRecognizer:panGesture];

    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(imagePinch:)];
    [self.originalImageView addGestureRecognizer:pinchGesture];


    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(16, 75, 290, 290)];
    img.image = [UIImage imageNamed:@"kidPicBg"];
    img.alpha = 0.5;
    [self.view addSubview:img];

    self.croppedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 75, 290, 290)];
    self.croppedImageView.contentMode = UIViewContentModeCenter;
    //self.croppedImageView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.croppedImageView];

    [self loadImage:self.orgImage];


}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (void)topBtnTap:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"edit-kid-photo", @"source", (sender.tag == 1) ? @"ok" : @"cancel", @"result", nil]];

    if (sender.tag == 1) //Ok
    {
        [self.delegate pictureSelected:self.resultImage];
        /*
        NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], @"xxx.jpg"];
        NSData* imageData = UIImageJPEGRepresentation(self.resultImage, 0.8);
        [imageData writeToFile:tmpPath atomically:NO];
         */
    }
    else {
        [self.delegate pictureSelected:nil];
    }

    //[self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = (UIImage *) [info objectForKey:@"UIImagePickerControllerOriginalImage"];

    /*CGRect face = [KPOpenCV openCvFaceDetect:image];
    UIGraphicsBeginImageContext(image.size);
    [image drawAtPoint:CGPointMake(0,0)];
	CGContextRef context = UIGraphicsGetCurrentContext();
	// drawing with a white stroke color
	CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);
	// drawing with a white fill color
	CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
	// Add Filled Rectangle,
	CGContextFillRect(context, face);
	image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    */
    [self loadImage:image];
}

- (void)loadImage:(UIImage *)image {
    image = [image scaleProportionalToSize:CGSizeMake(800, 800)];
    if (image.imageOrientation == UIImageOrientationLeft) {
        image = [image imageRotatedByDegrees:90];
    } else if (image.imageOrientation == UIImageOrientationRight) {
        image = [image imageRotatedByDegrees:-90];
    } else if (image.imageOrientation == UIImageOrientationDown) {
        image = [image imageRotatedByDegrees:-180];
    }

    self.orgImage = [UIImage imageWithCGImage:[image CGImage]];
    self.orgImage = [self.orgImage resizedImage:CGSizeMake(self.orgImage.size.width * 0.95, self.orgImage.size.height * 0.95) interpolationQuality:kCGInterpolationHigh];


    imageScale = 0.95;
    self.originalImageView.transform = CGAffineTransformIdentity;
    float ax = (CGRectGetWidth(self.view.frame) - self.orgImage.size.width) / 2;
    float ay = (CGRectGetHeight(self.view.frame) - self.orgImage.size.height) / 2;
    self.originalImageView.frame = CGRectMake(ax, ay, self.orgImage.size.width, self.orgImage.size.height);
    self.originalImageView.image = self.orgImage;

    self.originalImageView.transform = CGAffineTransformScale(self.originalImageView.transform, 0.95, 0.95);
    [self createCroppedImage];

    self.rightButtonEnabled = YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - image gestures

- (void)imagePan:(UIPanGestureRecognizer *)gesture {
    CGPoint translatedPoint = [(UIPanGestureRecognizer *) gesture translationInView:self.view];

    if ([(UIPanGestureRecognizer *) gesture state] == UIGestureRecognizerStateBegan) {
        [UIView animateWithDuration:0.2 animations:^{
            [[gesture view] setAlpha:1.0];
            self.croppedImageView.alpha = 0;
        }];

        firstX = [[gesture view] center].x;
        firstY = [[gesture view] center].y;
    }

    translatedPoint = CGPointMake(firstX + translatedPoint.x, firstY + translatedPoint.y);

    [[gesture view] setCenter:translatedPoint];

    if ([(UIPanGestureRecognizer *) gesture state] == UIGestureRecognizerStateEnded) {
        CGFloat finalX = translatedPoint.x;// + (.35*[(UIPanGestureRecognizer*)gestureRecognizer velocityInView:self.view].x);
        CGFloat finalY = translatedPoint.y;// + (.35*[(UIPanGestureRecognizer*)gestureRecognizer velocityInView:self.view].y);

        [UIView animateWithDuration:0.2 animations:^{
            [[gesture view] setCenter:CGPointMake(finalX, finalY)];
            [[gesture view] setAlpha:0.1];
        }                completion:^(BOOL finished) {
            [self createCroppedImage];
        }];
    }
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];

        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}


- (void)imagePinch:(UIPinchGestureRecognizer *)gesture {
    [self adjustAnchorPointForGestureRecognizer:gesture];

    if ([(UIPanGestureRecognizer *) gesture state] == UIGestureRecognizerStateBegan) {
        [UIView animateWithDuration:0.2 animations:^{
            [[gesture view] setAlpha:1.0];
            self.croppedImageView.alpha = 0;
        }];
    }


    if ([gesture state] == UIGestureRecognizerStateBegan || [gesture state] == UIGestureRecognizerStateChanged) {

        float w = [gesture scale] * [gesture view].frame.size.width;
        float h = [gesture scale] * [gesture view].frame.size.height;
        if (w < 290 || h < 290) {
            [gesture setScale:1];
            return;
        }

        [gesture view].transform = CGAffineTransformScale([[gesture view] transform], [gesture scale], [gesture scale]);

        imageScale = imageScale * [gesture scale];
        //DDLogInfo(@"Scale:%2f %2f", [gesture scale], imageScale);
        [gesture setScale:1];
    }

    if ([(UIPanGestureRecognizer *) gesture state] == UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:0.2 animations:^{
            [[gesture view] setAlpha:0.1];
        }                completion:^(BOOL finished) {
            [self createCroppedImage];
        }];
    }

}

#pragma mark - crop image with mask
/*
- (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    CGImageRelease(mask);
    UIImage* retImage= [UIImage imageWithCGImage:masked];
    CGImageRelease(masked);
    return retImage;
}
*/
/*
CGImageRef CopyImageAndAddAlphaChannel(CGImageRef sourceImage) {
	CGImageRef retVal = NULL;
	
	size_t width = CGImageGetWidth(sourceImage);
	size_t height = CGImageGetHeight(sourceImage);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
	CGContextRef offscreenContext = CGBitmapContextCreate(NULL, width, height,
                                                          8, 0, colorSpace, kCGImageAlphaPremultipliedFirst);
	
	if (offscreenContext != NULL) {
		CGContextDrawImage(offscreenContext, CGRectMake(0, 0, width, height), sourceImage);
		
		retVal = CGBitmapContextCreateImage(offscreenContext);
		CGContextRelease(offscreenContext);
	}
	
	CGColorSpaceRelease(colorSpace);
	
	return retVal;
}

- (UIImage*)maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    //Create Mask
    UIGraphicsBeginImageContext(CGSizeMake(290, 290));
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextAddRect(context, CGRectMake(0, 0, 290, 290));
    CGContextSetFillColor(context, CGColorGetComponents([[UIColor whiteColor] CGColor]));
    CGContextFillPath(context);

    CGContextAddEllipseInRect(context, CGRectMake(0, 0, 290, 290));
    CGContextSetFillColor(context, CGColorGetComponents([[UIColor blackColor] CGColor]));
    CGContextFillPath(context);
    maskImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], @"mask"];
    NSData* imageData = UIImagePNGRepresentation(maskImage);
    [imageData writeToFile:tmpPath atomically:NO];
    
	CGImageRef maskRef = maskImage.CGImage;
	CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
	
	CGImageRef sourceImage = [image CGImage];
	CGImageRef imageWithAlpha = sourceImage;
	//add alpha channel for images that don't have one (ie GIF, JPEG, etc...)
	//this however has a computational cost
	if (CGImageGetAlphaInfo(sourceImage) == kCGImageAlphaNone) {
		imageWithAlpha = CopyImageAndAddAlphaChannel(sourceImage);
	}
	
	CGImageRef masked = CGImageCreateWithMask(imageWithAlpha, mask);
	CGImageRelease(mask);
	
	//release imageWithAlpha if it was created by CopyImageAndAddAlphaChannel
	if (sourceImage != imageWithAlpha) {
		CGImageRelease(imageWithAlpha);
	}
	
	UIImage* retImage = [UIImage imageWithCGImage:masked];
	CGImageRelease(masked);
	
	return retImage;
}
*/
- (void)createCroppedImage {
    UIImage *image = [self.orgImage resizedImage:CGSizeMake(self.orgImage.size.width * imageScale, self.orgImage.size.height * imageScale) interpolationQuality:kCGInterpolationHigh];

    double ax = self.croppedImageView.frame.origin.x - self.originalImageView.frame.origin.x;
    double ay = self.croppedImageView.frame.origin.y - self.originalImageView.frame.origin.y;

    //DDLogInfo(@"%2f %2f", ax, ay);

    CGRect rect = CGRectMake(ax, ay, 290, 290);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);

    float dx = (290 - cropped.size.width) * -1 * (ax / fabs(ax));
    float dy = (290 - cropped.size.height) * (ay / fabs(ay));


    NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], @"bbb.jpg"];
    NSData *imageData = UIImageJPEGRepresentation(cropped, 0.8);
    [imageData writeToFile:tmpPath atomically:NO];

    UIGraphicsBeginImageContext(CGSizeMake(290, 290));
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextAddRect(context, CGRectMake(0, 0, 290, 290));
    CGContextSetFillColor(context, CGColorGetComponents([[UIColor blackColor] CGColor]));
    CGContextFillPath(context);

    //CGContextRotateCTM(context, M_PI);
    //CGContextTranslateCTM(context, 290, -290);
    CGContextTranslateCTM(context, 0, 290);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, CGRectMake(dx, dy, cropped.size.width, cropped.size.height), cropped.CGImage);
    //[cropped drawInRect:rect];

    cropped = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();


    self.resultImage = cropped;
/*
    tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], @"ccc.jpg"];
    imageData = UIImageJPEGRepresentation(cropped, 0.8);
    [imageData writeToFile:tmpPath atomically:NO];
  */

    cropped = [cropped imageMaskedWithElipse:CGSizeMake(290, 290)];
    /*
    tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], @"ddd.jpg"];
    imageData = UIImageJPEGRepresentation(cropped, 0.8);
    [imageData writeToFile:tmpPath atomically:NO];
*/

    self.croppedImageView.image = cropped;
    self.croppedImageView.alpha = 1.0;
}

- (void)detectFace {
    /*
    detecting      = YES;
    progress       = [ [ UIActivityIndicatorView alloc ] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge ];
    progress.frame = CGRectMake
    (
     ( self.view.frame.size.width / 2 ) - ( progress.frame.size.width / 2 ),
     ( self.view.frame.size.height / 2 ) - ( progress.frame.size.height / 2 ),
     progress.frame.size.width,
     progress.frame.size.height
     );
    
    [ progress startAnimating ];
    
    [ self.view addSubview: progress ];
    [ self performSelectorInBackground: @selector( openCvFaceDetect ) withObject: nil ];
     */
}


@end
