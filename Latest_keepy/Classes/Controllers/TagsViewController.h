//
//  TagsViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 10/27/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@protocol TagViewDelegate

- (void)tagViewClosed:(NSArray *)selectedTags;

@end

@interface TagsViewController : KPPopViewController

- (instancetype)initWithMode:(int)mode andSelectedTags:(NSArray *)selectedTags;

@property(nonatomic, assign) id <TagViewDelegate> delegate;

@end
