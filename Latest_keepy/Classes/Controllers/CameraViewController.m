//
//  CameraViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 8/8/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "CameraViewController.h"

@import Photos;
#import "MACaptureSession.h"
#import "ELCAlbumPickerController.h"
#import "KPPhotoLibrary.h"

#import "Keepy-Swift.h"

@interface CameraViewController ()

@property(strong, nonatomic) MACaptureSession *captureManager;
@property(strong, nonatomic) UIButton *pictureButton;

@end

@implementation CameraViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //[self.navigationController setNavigationBarHidden:YES];

    self.title = KPLocalizedString(@"take photo");

    [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"camera", @"source", nil]];


    [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"close"), @"title", @(BUTTON_TYPE_NORMAL), @"buttonType", nil] andRightButtonInfo:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transitionToMAImagePickerControllerAdjustViewController:) name:kImageCapturedSuccessfully object:@(0)];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0xd7ff46);
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:1 green:1 blue:1 alpha:1], NSForegroundColorAttributeName, nil];


    self.pictureButton.enabled = YES;

    [[_captureManager captureSession] startRunning];
}

- (void)viewDidLayoutSubviews {

    if ([self.view.subviews count] > 2) {
        return;
    }

    if (_captureManager == nil) {
        [self setCaptureManager:[[MACaptureSession alloc] init]];
        [_captureManager addVideoInputFromCamera];
        [_captureManager addStillImageOutput];
        [_captureManager addVideoPreviewLayer];
        _captureManager.mode = 0;

        CGRect layerRect = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        [[_captureManager previewLayer] setBounds:layerRect];
        [[_captureManager previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect))];
    }

    UIView *backView = [[UIView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:backView];
    [backView.layer addSublayer:self.captureManager.previewLayer];
    [self.view sendSubviewToBack:backView];

    [[_captureManager captureSession] startRunning];

    UIView *bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 60, self.view.bounds.size.width, 60)];
    bottomBar.backgroundColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:0.5];
    //bottomBar.alpha = 0.5;

    [self.view addSubview:bottomBar];

    UIButton *libButton = [UIButton buttonWithType:UIButtonTypeCustom];
    libButton.frame = CGRectMake(20, (60 - 44) / 2, 44, 44);
    [libButton setImage:[UIImage imageNamed:@"camera-roll-picture-frame"] forState:UIControlStateNormal];
    [libButton addTarget:self action:@selector(galleryTap:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:libButton];

    if ([KPPhotoLibrary authorizationIsDenied]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        return;
    }

    dispatch_async(dispatch_get_global_queue(0, 0), ^{

        [[GlobalUtils defaultAssetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            if (group) {
                __block ALAsset *lastAsset;
                [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop2) {

                    if (result == nil) {
                        if (lastAsset != nil && [[lastAsset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypePhoto]) {
                            UIImage *image = [UIImage imageWithCGImage:lastAsset.thumbnail];
                            /*
                            CALayer *imageLayer = [CALayer layer];
                            imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                            imageLayer.contents = (id) image.CGImage;
                            
                            imageLayer.masksToBounds = YES;
                            imageLayer.cornerRadius = 15.0;
                            imageLayer.borderColor = [[UIColor whiteColor] CGColor];
                            imageLayer.borderWidth = 5.0;
                            
                            UIGraphicsBeginImageContext(image.size);
                            [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
                            UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
                            UIGraphicsEndImageContext();
                            */
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [libButton setImage:image forState:UIControlStateNormal];
                            });

                            *stop = YES;
                        }
                    }
                    else {
                        lastAsset = result;
                    }
                }];
            }
        }                                               failureBlock:^(NSError *error) {
            DDLogInfo(@"A problem occured %@", [error description]);
            // an error here means that the asset groups were inaccessable.
            // Maybe the user or system preferences refused access.
        }];
    });

    self.pictureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.pictureButton.frame = CGRectMake((self.view.frame.size.width - 47) / 2, (60 - 47) / 2, 47, 47);
    [self.pictureButton setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];

    [self.pictureButton addTarget:self action:@selector(pictureTap:) forControlEvents:UIControlEventTouchUpInside];
    [bottomBar addSubview:self.pictureButton];

    [GlobalUtils showTip:4 withView:nil withCompleteBlock:^{
    }];


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)topBtnTap:(UIButton *)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"camera", @"source", @"cancel", @"result", nil]];

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(galleryPickerControllerDidCancel:)]) {
        [self.delegate performSelector:@selector(galleryPickerControllerDidCancel:) withObject:self];
    }
}

- (void)galleryTap:(id)sender {
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"camera", @"source", @"gallery", @"result", nil]];

    if ([KPPhotoLibrary authorizationIsDenied]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"please enable keepy under your device Settings > Privacy > Photos")];
        return;
    }

    [[UserManager sharedInstance] setCameraMode:1];
    
    PHFetchOptions* options = [PHFetchOptions new];
    PHFetchResult* images = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
    
    NSMutableArray* arr = [NSMutableArray array];
    
    [images enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
        [arr addObject:[[KPAsset alloc] initWithAsset:asset]];
        
        if (idx + 1 == images.count) {
            ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName:nil bundle:nil];
            albumController.delegate = self.delegate;
            albumController.title = KPLocalizedString(@"albums");
            albumController.filter = @"allPhotos";//(NSString*)[ALAssetsFilter allPhotos];
            [[_captureManager captureSession] stopRunning];
            
            GalleryPickerViewController *avc = [[GalleryPickerViewController alloc] initWithAssets:arr];
            avc.showToolbar = YES;
            avc.delegate = self.delegate;
            [UIView transitionFromView:self.view
                                toView:avc.view
                              duration:1
                               options:UIViewAnimationOptionTransitionFlipFromRight
                            completion:nil];
            [self.navigationController setViewControllers:[NSArray arrayWithObjects:albumController, avc, nil] animated:NO];

            *stop = YES;
        }
    }];
}

- (void)pictureTap:(UIButton *)sender {
    [sender setEnabled:NO];
    [self.captureManager captureStillImage];
}

- (void)transitionToMAImagePickerControllerAdjustViewController:(NSNotification *)notification {
    [[self.captureManager captureSession] stopRunning];

    /*
    NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], [NSString stringWithFormat:@"test.jpg"]];
    NSData* imageData = UIImageJPEGRepresentation(self.captureManager.stillImage, 0.8);
    [imageData writeToFile:tmpPath atomically:NO];*/

    //UIImage *img = [self.captureManager.stillImage imageByScalingProportionallyToSize:CGSizeMake(500, 500)];
    UIImage *img = self.captureManager.stillImage;
    /*
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    });
    */
    [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"camera", @"source", @"ok", @"result", nil]];

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(galleryPickerController:didFinishPickingMediaWithInfo:)]) {
        [self.delegate performSelector:@selector(galleryPickerController:didFinishPickingMediaWithInfo:) withObject:self withObject:[NSArray arrayWithObject:[NSDictionary dictionaryWithObjectsAndKeys:img, UIImagePickerControllerOriginalImage, nil]]];
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (error != NULL) {
    }

    DDLogInfo(@"saved image to camera roll");
}

@end
