//
//  FamilyCell.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilyCell.h"
#import "FamilyMemberView.h"
#import "AddParentViewController.h"
#import "AddGrandParentViewController.h"
#import "AddRelativeViewController.h"

@interface FamilyCell ()

@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end

@implementation FamilyCell

+ (CGFloat)height {
    return 73.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    NSMutableArray *family = [[NSMutableArray alloc] initWithCapacity:(self.family.count + 1)];
    NSNumber *parentType = @([[UserManager sharedInstance] getMe].parentType.intValue);
    [family addObject:@{@"fanName" : @"you", @"fanRelationType" : parentType, @"id" : @(0)}];
    [family addObjectsFromArray:self.family];

    NSArray *familySorted = [family sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *first, NSDictionary *second) {
        return [[first objectForKey:@"id"] intValue] > [[second objectForKey:@"id"] intValue];
    }];

    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    self.scrollView.contentSize = CGSizeMake(([FamilyMemberView width] * familySorted.count) + 20.0f, self.scrollView.contentSize.height);
    for (int i = 0; i < familySorted.count; i++) {
        NSDictionary *familyMember = [familySorted objectAtIndex:i];
        FamilyMemberView *familyMemberView = [[FamilyMemberView alloc] init];
        familyMemberView.frame = CGRectMake([FamilyMemberView width] * i, familyMemberView.frame.origin.y, familyMemberView.frame.size.width, familyMemberView.frame.size.height);
        familyMemberView.name.text = [familyMember objectForKey:@"fanName"];
        int fanRelationType = [[familyMember objectForKey:@"fanRelationType"] intValue];
        switch (fanRelationType) {
            default:
            case AddParentViewControllerTypeMom: {
                familyMemberView.imageView.image = [UIImage imageNamed:@"brown-mom-small"];
                break;
            }

            case AddParentViewControllerTypeDad: {
                familyMemberView.imageView.image = [UIImage imageNamed:@"brown-dad-small"];
                break;
            }

            case AddGrandParentViewControllerTypeGrandma: {
                familyMemberView.imageView.image = [UIImage imageNamed:@"brown-grandma-small"];
                break;
            }

            case AddGrandParentViewControllerTypeGrandpa: {
                familyMemberView.imageView.image = [UIImage imageNamed:@"brown-grandpa-small"];
                break;
            }

            case AddRelativeViewControllerTypeAunt: {
                familyMemberView.imageView.image = [UIImage imageNamed:@"brown-aunt-small"];
                break;
            }

            case AddRelativeViewControllerTypeUncle: {
                familyMemberView.imageView.image = [UIImage imageNamed:@"brown-uncle-small"];
                break;
            }
        }
        [self.scrollView addSubview:familyMemberView];

        if (self.scrollView.contentSize.width > self.scrollView.bounds.size.width) {
            CGPoint rightOffset = CGPointMake(self.scrollView.contentSize.width - self.scrollView.bounds.size.width, 0);
            [self.scrollView setContentOffset:rightOffset animated:NO];
        }
    }
}

@end
