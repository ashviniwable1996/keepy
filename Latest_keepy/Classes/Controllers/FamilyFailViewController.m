//
//  FamilyFailViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilyFailViewController.h"
#import "ProgressCell.h"

@interface FamilyFailViewController () //<UITableViewDataSource, UITableViewDelegate, ProgressCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation FamilyFailViewController


- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"onboarding-family-fail-open"];


    /*
    
    [self.tableView registerNib:[UINib nibWithNibName:@"KidsCell" bundle:nil] forCellReuseIdentifier:@"KidsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilyFailCell" bundle:nil] forCellReuseIdentifier:@"FamilyFailCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProgressCell" bundle:nil] forCellReuseIdentifier:@"ProgressCell"];
    
    
    */


}

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        static NSString *KidsCellIdentifier = @"KidsCell";
        KidsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KidsCellIdentifier];
        cell.kids = self.kids;
        return cell;
    } else if (indexPath.row == 1) {
        static NSString *FamilyFailCellIdentifier = @"FamilyFailCell";
        FamilyFailCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FamilyFailCellIdentifier];
        cell.title.text = [self title];
        return cell;
    } else if (indexPath.row == 2) {
        static NSString *ProgressCellIdentifier = @"ProgressCell";
        ProgressCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ProgressCellIdentifier];
        cell.delegate = self;
        cell.progressCellState = ProgressCellStateThree;
        cell.progressImageView.hidden = YES;
        [cell.skipButton setTitle:@"close" forState:UIControlStateNormal];
        [cell layout];
        return cell;
    }
    return nil;
}

- (NSString *)title {
    switch (self.kids.count) {
        case 1: {
            NSDictionary *kid = [self.kids objectAtIndex:0];
            return [NSString stringWithFormat:@"Don't forget to invite %@'s loved ones later!", [kid objectForKey:@"name"]];
            break;
        }
            
        case 2: {
            NSDictionary *kid1 = [self.kids objectAtIndex:0];
            NSDictionary *kid2 = [self.kids objectAtIndex:1];
            return [NSString stringWithFormat:@"Don't forget to invite %@ and %@'s loved ones later!", [kid1 objectForKey:@"name"], [kid2 objectForKey:@"name"]];
        }
            
        case 3: {
            NSDictionary *kid1 = [self.kids objectAtIndex:0];
            NSDictionary *kid2 = [self.kids objectAtIndex:1];
            NSDictionary *kid3 = [self.kids objectAtIndex:2];
            return [NSString stringWithFormat:@"Don't forget to invite %@, %@, and %@'s loved ones later!", [kid1 objectForKey:@"name"], [kid2 objectForKey:@"name"], [kid3 objectForKey:@"name"]];
        }
            
        default: {
            return @"Don't forget to invite your kids' loved ones later!";
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [KidsCell height];
    } else if (indexPath.row == 1) {
        return [FamilyFailCell height:[self title]];
    } else if (indexPath.row == 2) {
        return [ProgressCell height];
    }
    return 0.0f;
}

- (CGFloat)height {
    return [KidsCell height] + [FamilyFailCell height:[self title]] + [ProgressCell height];
}
*/
- (void)skip:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-family-fail-done-tap"];

    [self.delegate doneAddingFamily];
}

@end
