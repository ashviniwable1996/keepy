//
//  ChooseMemoryViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface ChooseMemoryViewController : UIViewController

@property(nonatomic, weak) IBOutlet UIView *containerView;
@property(nonatomic, weak) UIViewController *previousViewController;
@property(nonatomic, weak) UIViewController *nextViewController;
@property(nonatomic, copy) NSString *nextIdentifier;

@end

@interface ChooseMemorySegue : UIStoryboardSegue

@property(nonatomic, copy) NSString *subtype;

@end