//
//  DKMoviePlayerViewController.h
//  Keepy
//
//  Created by Daniel Karsh on 4/16/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "KPPopViewController.h"
#import <AVFoundation/AVFoundation.h>

@class ItemEditViewController;
@class DKMovieOverlayViewController;
@class MyImageView;
@class DKPlaybackPosition;
@class ServerComm;

typedef void (^BOOLBlock)(BOOL ready);

@interface DKMoviePlayerViewController : KPPopViewController <UIGestureRecognizerDelegate> {
    UITapGestureRecognizer *singleTap;
    UITapGestureRecognizer *doubleTap;
}

@property(strong) MPMoviePlayerController *moviePlayerController;
@property(nonatomic, weak) NSTimer *timer;
@property(nonatomic, weak) NSURL *myMovieFileURL;

@property(weak, nonatomic) IBOutlet UIImageView *littleImage;
@property(weak, nonatomic) IBOutlet UIButton *playButton;
@property(weak, nonatomic) IBOutlet UIButton *pauseButton;

@property(strong, nonatomic) UIImage *thumbnailImage;

@property(weak, nonatomic) IBOutlet DKPlaybackPosition *playBack;
@property(nonatomic, strong) ServerComm *serverComm;
@property(nonatomic, strong) ItemEditViewController *itemEditVC;

@property(copy) NSString *duration;
@property(assign) BOOL weGotAnImage;

@property(nonatomic) BOOL viewWillAppear;
@property(nonatomic) BOOL playerStatePlaying;
@property(nonatomic) BOOL playerWasActive;
@property(nonatomic) BOOL movieEnd;

@property(nonatomic) BOOL uploadVideoMode;

@property(nonatomic) BOOL isWindowChromeOn;
@property(nonatomic) BOOL isFrameOn;


@property(nonatomic, copy) BOOLBlock myComplition;
@property(nonatomic, assign) CGRect originalPlayBackFrame;
@property(nonatomic, strong) NSTimer *hideWindowChromeTimer;
@property(nonatomic, assign) UIButton *activeButton;


+ (DKMoviePlayerViewController *)sharedClient;

- (void)doubleTapPause;

- (void)viewWillEnterForeground;

- (void)playMovieFile:(NSURL *)movieFileURL;

- (void)playMovieStream:(NSURL *)movieFileURL bufferReady:(BOOLBlock)ready;

- (IBAction)tapPlay:(id)sender;

- (IBAction)tapPause:(id)sender;


@end
