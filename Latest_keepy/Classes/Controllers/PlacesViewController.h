//
//  PlacesViewController.h
//  Keepy
//
//  Created by Troy Payne on 4/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@class KPLocalPlace;

@interface PlacesViewController : UIViewController

@property(nonatomic, strong) NSArray *places;
@property(nonatomic, strong) KPLocalPlace *selectedPlace;

@end

@interface PlaceViewCell : UITableViewCell

@property(nonatomic, strong) KPLocalPlace *place;

+ (CGFloat)height;

@end

@protocol AddPlaceViewCellDelegate;

@interface AddPlaceViewCell : UITableViewCell

@property(nonatomic, weak) id <AddPlaceViewCellDelegate> delegate;

+ (CGFloat)height;

@end

@protocol AddPlaceViewCellDelegate <NSObject>

- (void)addPlace;

@end