//
//  BirthDate2ViewController.m
//  Keepy
//
//  Created by Troy Payne on 3/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "BirthDate2ViewController.h"
#import "AgeCalculator.h"

@interface BirthDate2ViewController ()

@property(nonatomic, weak) IBOutlet UILabel *theTitle;
@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *saveButton;
// @property(nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property(nonatomic, strong) UIDatePicker *datePicker;

@end

@implementation BirthDate2ViewController

- (NSDate *)dateToGMT:(NSDate *)sourceDate {
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:destinationGMTOffset sinceDate:sourceDate];
    return destinationDate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.theTitleString) {
        self.theTitle.text = self.theTitleString;
    }

    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
    self.theTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17];

    CGRect datePickerFrame;
    datePickerFrame.origin.y = 68;
    datePickerFrame.size.width = self.view.frame.size.width;
    datePickerFrame.size.height = 162;
    self.datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    self.datePicker.timeZone = [NSTimeZone defaultTimeZone];
    self.datePicker.calendar = [NSCalendar currentCalendar];

    if (self.birthDateDate) {
        self.datePicker.date = [self dateToGMT:self.birthDateDate];
    }
    
    
     
}

- (void)viewDidLayoutSubviews{
    
    [self.datePicker removeFromSuperview];
    [self.view addSubview:self.datePicker];
    
}

- (IBAction)save:(id)sender {

    self.birthDateDate = [AgeCalculator removeHourAndMinutes:self.datePicker.date];

    [self performSegueWithIdentifier:@"BirthDate2Exit" sender:self];
}

- (IBAction)cancel:(id)sender {
    [self performSegueWithIdentifier:@"BirthDate2Exit" sender:self];
}


@end
