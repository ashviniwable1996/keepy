//
//  FanCell.m
//  Keepy
//
//  Created by Troy Payne on 2/20/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FanCell.h"

@interface FanCell ()

@property(nonatomic, weak) IBOutlet UIButton *imageViewButton;
@property(nonatomic, weak) IBOutlet UILabel *welcomeLabel;

@end

@implementation FanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.welcomeLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
}

+ (CGFloat)height {
    return 100.0f;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    switch (self.fanCellType) {
        default:
        case FanCellTypeMom: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-mom"] forState:UIControlStateNormal];
            break;
        }

        case FanCellTypeDad: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-dad"] forState:UIControlStateNormal];
            break;
        }

        case FanCellTypeGrandma: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-grandma"] forState:UIControlStateNormal];
            break;
        }

        case FanCellTypeGrandpa: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-grandpa"] forState:UIControlStateNormal];
            break;
        }

        case FanCellTypeAunt: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-aunt"] forState:UIControlStateNormal];
            break;
        }

        case FanCellTypeFamily:
        case FanCellTypeFriend:
        case FanCellTypeUncle: {
            [self.imageViewButton setImage:[UIImage imageNamed:@"brown-uncle"] forState:UIControlStateNormal];
            break;
        }
    }

    self.welcomeLabel.text = [NSString stringWithFormat:@"Welcome %@", [self.fan objectForKey:@"fanName"]];
}

@end

