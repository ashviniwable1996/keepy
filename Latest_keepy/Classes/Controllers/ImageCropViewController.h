//
//  ImageCropViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 3/30/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@protocol ImageCropDelegate

- (void)cropRectSelected:(NSArray *)newPoints withImage:(UIImage *)image;

@end

@interface ImageCropViewController : KPPopViewController

@property(nonatomic, assign) id <ImageCropDelegate> delegate;

- (instancetype)initWithImage:(UIImage *)image andPoints:(NSArray *)points;

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo;

@end
