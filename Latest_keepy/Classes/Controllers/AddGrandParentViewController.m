//
//  AddGrandParentViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AddGrandParentViewController.h"
#import "TitleCell.h"
#import "TitlePictureCell.h"
#import "InputCell.h"
#import "EmailCell.h"
#import "SaveCell.h"
#import "Keepy-Swift.h"

@interface AddGrandParentViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, EmailCellDelegate, SaveCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, weak) UITextField *nameField;
@property(nonatomic, weak) UITextField *emailField;
@property(nonatomic, weak) SaveCell *saveCell;

@end

@implementation AddGrandParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"onboarding-add-grand-parents-add-grand-parent-open"];

    self.family = nil;
    [self.tableView registerNib:[UINib nibWithNibName:@"SpacerCell" bundle:nil] forCellReuseIdentifier:@"SpacerCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TitleCell" bundle:nil] forCellReuseIdentifier:@"TitleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TitlePictureCell" bundle:nil] forCellReuseIdentifier:@"TitlePictureCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"InputCell" bundle:nil] forCellReuseIdentifier:@"InputCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EmailCell" bundle:nil] forCellReuseIdentifier:@"EmailCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SaveCell" bundle:nil] forCellReuseIdentifier:@"SaveCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *SpacerCellIdentifier = @"SpacerCell";
    static NSString *TitleCellIdentifier = @"TitleCell";
    static NSString *TitlePictureCellIdentifier = @"TitlePictureCell";
    static NSString *InputCellIdentifier = @"InputCell";
    static NSString *EmailCellIdentifier = @"EmailCell";
    static NSString *SaveCellIdentifier = @"SaveCell";

    if (indexPath.row == 0) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 1) {
        TitleCell *cell = [self.tableView dequeueReusableCellWithIdentifier:TitleCellIdentifier];
        if (self.addGrandParentViewControllerType == AddGrandParentViewControllerTypeGrandma) {
            cell.title.text = @"add grandma";
        } else if (self.addGrandParentViewControllerType == AddGrandParentViewControllerTypeGrandpa) {
            cell.title.text = @"add grandpa";
        }
        return cell;
    } else if (indexPath.row == 2) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 3) {
        TitlePictureCell *cell = [self.tableView dequeueReusableCellWithIdentifier:TitlePictureCellIdentifier];
        if (self.addGrandParentViewControllerType == AddGrandParentViewControllerTypeGrandma) {
            cell.titlePictureCellType = TitlePictureCellTypeGrandma;
        } else if (self.addGrandParentViewControllerType == AddGrandParentViewControllerTypeGrandpa) {
            cell.titlePictureCellType = TitlePictureCellTypeGrandpa;
        }
        return cell;
    } else if (indexPath.row == 4) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 5) {
        InputCell *cell = [self.tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
        cell.input.placeholder = @"name";
        self.nameField = cell.input;
        self.nameField.delegate = self;
        self.nameField.returnKeyType = UIReturnKeyNext;
        self.nameField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        self.nameField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.nameField.spellCheckingType = UITextSpellCheckingTypeNo;
        return cell;
    } else if (indexPath.row == 6) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 7) {
        EmailCell *cell = [self.tableView dequeueReusableCellWithIdentifier:EmailCellIdentifier];
        cell.delegate = self;
        self.emailField = cell.input;
        self.emailField.delegate = self;
        self.emailField.returnKeyType = UIReturnKeyDone;
        self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
        return cell;
    } else if (indexPath.row == 8) {
        return [self.tableView dequeueReusableCellWithIdentifier:SpacerCellIdentifier];
    } else if (indexPath.row == 9) {
        SaveCell *cell = [self.tableView dequeueReusableCellWithIdentifier:SaveCellIdentifier];
        cell.delegate = self;
        self.saveCell = cell;
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 11.0f;
    } else if (indexPath.row == 1) {
        return [TitleCell height];
    } else if (indexPath.row == 2) {
        return 23.0f;
    } else if (indexPath.row == 3) {
        return [TitlePictureCell height];
    } else if (indexPath.row == 4) {
        return 22.0f;
    } else if (indexPath.row == 5) {
        return [InputCell height];
    } else if (indexPath.row == 6) {
        return 17.0f;
    } else if (indexPath.row == 7) {
        return [EmailCell height];
    } else if (indexPath.row == 8) {
        return 84.0f;
    } else if (indexPath.row == 9) {
        return [SaveCell height];
    }
    return 0.0f;
}

- (CGFloat)height {
    CGFloat height = 0;
    for (int i = 0; i < [self tableView:self.tableView numberOfRowsInSection:0]; ++i) {
        height += [self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    return height;
}

- (void)cancelled {
    [[Mixpanel sharedInstance] track:@"onboarding-add-grand-parents-add-grand-parent-cancel-tap"];

    [self performSegueWithIdentifier:@"AddGrandParentsExit" sender:self];
}

- (void)saving {
    self.view.userInteractionEnabled = NO;
    __weak AddGrandParentViewController *weakSelf = self;
    [[ServerComm instance] addFan:self.emailField.text withName:self.nameField.text andRelationType:self.addGrandParentViewControllerType andFacebookId:nil andFacebookRequestId:nil andBirthdate:nil withSuccessBlock:^(id result) {

        int status = [[result valueForKey:@"status"] intValue];
        if (status == 0) {
            @try {
                [[Analytics sharedAnalytics] track:@"item-add" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"fan", @"itemType", nil]];
                
                [FBSDKAppEvents logEvent:@"add fan"];
            } @catch (NSException *e) {}

            [[ServerComm instance] getFans:^(id result) {
                weakSelf.family = [result objectForKey:@"result"];
                [weakSelf.saveCell success];
            }                 andFailBlock:^(NSError *error) {
                [weakSelf.saveCell success];
            }];
        } else {
            if ([[[[result valueForKey:@"error"] valueForKey:@"errors"] objectAtIndex:0] isEqualToString:@"already invited"]) {
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:KPLocalizedString(@"%@ was already invited by you."), weakSelf.emailField.text]];
                [weakSelf.saveCell failed];
            } else {
                [weakSelf.saveCell failed];
            }
        }

    }                andFailBlock:^(NSError *error) {
        [weakSelf.saveCell failed];
    }];
}

- (void)saved {

    [[Mixpanel sharedInstance] track:@"onboarding-add-grand-parents-add-grand-parent-save-tap"];
    self.view.userInteractionEnabled = YES;
    [self performSegueWithIdentifier:@"AddGrandParentsExit" sender:self];
}

- (void)failed {
    self.view.userInteractionEnabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self enableSaveCheck];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSInteger length = (textField.text.length + string.length);
    if (textField == self.nameField) {
        if (length > 32 && range.length == 0) {
            return NO;
        }
    } else if (textField == self.emailField) {
        if (length > 254 && range.length == 0) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.nameField) {
        [self.emailField becomeFirstResponder];
    } else if (textField == self.emailField) {
        [self.emailField resignFirstResponder];
    }
    return NO;
}

- (void)enableSaveCheck {
    [self.saveCell disable];
    if (self.nameField.text.length == 0 || self.nameField.text.length > 32) {
        return;
    }

    if (!self.emailField.text.isEmailAddress) {
        return;
    }

    [self.saveCell enable];
}

- (UIViewController *)viewController {
    return self;
}

@end
