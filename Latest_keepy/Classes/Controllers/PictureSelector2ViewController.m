//
//  PictureSelector2ViewController.m
//  Keepy
//
//  Created by Troy Payne on 3/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "PictureSelector2ViewController.h"

#import "Keepy-Swift.h"

@interface PictureSelector2ViewController () <UITableViewDataSource, UITableViewDelegate, PictureCell2Delegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *unseenView;

@end

@implementation PictureSelector2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    _unseenView.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ceil(self.pictures.count / 4.0f);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *PictureCell2Identifier = @"PictureCell2";
    PictureCell2 *cell = [self.tableView dequeueReusableCellWithIdentifier:PictureCell2Identifier];
    cell.delegate = self;
    NSUInteger index = indexPath.row * 4;
    cell.pictures = [self.pictures subarrayWithRange:NSMakeRange(index, MIN(4, (self.pictures.count - index)))];
    [cell layout];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ((self.view.frame.size.width - 22)/ 4 ) + 4 ;//[PictureCell2 height];
}

- (IBAction)albums:(id)sender {
    [self.delegate albums:sender];
}

- (void)pictureSelected:(UIImage *)image {
    [self.delegate pictureSelected:image];
}

@end

@interface PictureCell2 ()

@property(nonatomic, strong) IBOutletCollection (UIButton) NSArray *buttons;

@end

@implementation PictureCell2

+ (CGFloat)height {
    return 79.0f;
}

- (IBAction)pictureSelected:(id)sender {
    NSUInteger index = [self.buttons indexOfObject:sender];
    [self.delegate pictureSelected:[self.pictures objectAtIndex:index]];
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    for (UIButton *button in self.buttons) {
        [button setHidden:YES];
    }
    for (id picture in self.pictures) {
        if (picture == [NSNull null]) {
            NSUInteger index = [self.pictures indexOfObject:picture];
            UIButton *button = [self.buttons objectAtIndex:index];
            UIImage *picture = [UIImage imageNamed:@"camera-photo"];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button setImage:picture forState:UIControlStateNormal];
            button.hidden = NO;
        } else {
            PHAsset *asset = picture;
            NSUInteger index = [self.pictures indexOfObject:asset];
            UIButton *button = [self.buttons objectAtIndex:index];
            
            UIImage *picture = asset.thumbnail;
            [button setImage:picture forState:UIControlStateNormal];
            button.hidden = NO;
        }
    }
}

@end
