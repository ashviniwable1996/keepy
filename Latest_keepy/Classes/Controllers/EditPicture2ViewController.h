//
//  EditPicture2ViewController.h
//  TalkingCards
//
//  Created by Yaniv Solnik on 9/5/12.
//  Copyright (c) 2012 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPPopViewController.h"

@protocol EditPictureDelegate <NSObject>

@optional
- (void)pictureSelected:(UIImage *)selectedImage;

@end

@interface EditPicture2ViewController : KPPopViewController

@property(nonatomic, assign) id <EditPictureDelegate> delegate;

- (instancetype)initWithImage:(UIImage *)image;

@end
