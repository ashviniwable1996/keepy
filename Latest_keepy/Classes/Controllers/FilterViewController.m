//
//  FilterViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 3/12/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

#import "FilterViewController.h"
#import "Comment.h"

@interface FilterViewController ()

@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) Kid *kid;

@end

@implementation FilterViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(kidSelected:)
                   name:KID_SELECT_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(itemAdded:)
                   name:ITEM_ADDED_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(itemAdded:)
                   name:LOVE_CHANGED_NOTIFICATION
                 object:nil];


    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(commentAdded:)
                   name:COMMENT_ADDED_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(kidsRefreshed:)
                   name:KIDS_REFRESHED_NOTIFICATION
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(itemDeleted:)
                   name:ITEM_DELETED_NOTIFICATION
                 object:nil];


    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    [self.view addSubview:self.scrollView];

    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData {
    for (UIView *control in self.scrollView.subviews) {
        [control removeFromSuperview];
    }

    __block float ax = 5;
    __block float ay = 5;

    void(^createButtonBlock)(NSInteger filterType, NSString *filterValue, Item *aitem, NSInteger itemsCount, NSInteger newItemsCount) = ^(NSInteger filterType, NSString *filterValue, Item *aitem, NSInteger itemsCount, NSInteger newItemsCount) {

        UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(ax, ay, 152, 100)];

        UIImageView *frontImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 152, 100)];
        frontImage.contentMode = UIViewContentModeScaleAspectFill;
        frontImage.clipsToBounds = YES;
        [aview addSubview:frontImage];
        if (aitem != nil) {
            [[ServerComm instance] downloadAssetFile:aitem.image withSuccessBlock:^(id result) {
                UIImage *img = [UIImage imageWithContentsOfFile:result];
                frontImage.image = img;
            }                           andFailBlock:^(NSError *error) {

            }];
        }
        else if (filterType == FILTER_TYPE_COMMENTS) {
            frontImage.image = [UIImage imageNamed:@"album-comments"];
        } else if (filterType == FILTER_TYPE_LIKES) {
            frontImage.image = [UIImage imageNamed:@"album-loves"];
        } else if (filterType == FILTER_TYPE_CONTACTS) {
            frontImage.image = [UIImage imageNamed:@"album-contacts"];
        }

        //Title Label
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 75, 100, 20)];
        lbl.textColor = [UIColor whiteColor];
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.shadowOffset = CGSizeMake(1.0, 1.0);
        lbl.shadowColor = [UIColor blackColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:16];
        lbl.text = filterValue;
        [aview addSubview:lbl];

        //Count Label
        lbl = [[UILabel alloc] initWithFrame:CGRectMake(47, 75, 100, 20)];
        lbl.textColor = [UIColor whiteColor];
        lbl.textAlignment = NSTextAlignmentRight;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.shadowOffset = CGSizeMake(1.0, 1.0);
        lbl.shadowColor = [UIColor blackColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:12];
        NSString *s = @"pics";
        //if (filterType == FILTER_TYPE_COMMENTS)
        //    s = @"comments";

        lbl.text = [NSString stringWithFormat:@"%ld %@", (long)itemsCount, s];
        [aview addSubview:lbl];

        //New Badge
        UIImageView *notificationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 62, 62)];
        notificationImageView.image = [UIImage imageNamed:@"new-ribbon-BG"];
        [aview addSubview:notificationImageView];
        notificationImageView.hidden = (newItemsCount == 0);

        lbl = [[UILabel alloc] initWithFrame:CGRectMake(-7, -7, 62, 62)];
        lbl.textColor = [UIColor colorWithRed:0.345 green:0.267 blue:0.243 alpha:1];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:10];
        lbl.text = [NSString stringWithFormat:@"%ld new", (long)newItemsCount];
        lbl.transform = CGAffineTransformMakeRotation(-M_PI / 4);
        lbl.hidden = (newItemsCount == 0);
        [aview addSubview:lbl];

        //Border
        UIImageView *borderImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 152, 100)];
        [borderImage.layer setBorderColor:[[UIColor colorWithRed:0.961 green:0.843 blue:0.294 alpha:1] CGColor]];
        [borderImage.layer setBorderWidth:5.0];
        borderImage.hidden = (filterType != FILTER_TYPE_ALL);
        [aview addSubview:borderImage];


        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnTap:)];
        gesture.cancelsTouchesInView = NO;
        [aview addGestureRecognizer:gesture];
        aview.tag = filterType;
        [self.scrollView addSubview:aview];

        ax += 157;
        if (ax > 162) {
            ax = 5;
            ay += 105;
        }

    };


    //all items
    NSArray *items = nil;
    if (self.kid == nil) {
        items = [Item MR_findAllSortedBy:@"itemDate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"not kids contains %@", [[UserManager sharedInstance] keepyKid]]];
    } else {
        items = [Item MR_findAllSortedBy:@"itemDate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"kids contains %@", self.kid]];
    }

    Item *firstItem = nil;
    if ([items count] > 0) {
        firstItem = [items objectAtIndex:0];
    }

    int newItemsAll = 0;
    for (Item *item in items) {
        if (item.readAction.intValue != 2 || [GlobalUtils daysBetweenDate:item.createDate andDate:[NSDate date]] > 1) {
            newItemsAll++;
        }
    }

    //all items with comments
    NSArray *itemsWithComments = nil;
    if (self.kid == nil) {
        itemsWithComments = [Item MR_findAllSortedBy:@"itemDate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"comments.@count>0 AND not kids contains %@", [[UserManager sharedInstance] keepyKid]]];
    } else {
        itemsWithComments = [Item MR_findAllSortedBy:@"itemDate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"comments.@count>0 AND kids contains %@", self.kid]];
    }

    int newComments = 0;
    for (Item *item in itemsWithComments) {
        for (Comment *comment in item.comments) {
            if (!comment.wasRead.boolValue) {
                newComments++;
                break;
            }
        }
    }

    //loved items
    /*
    NSArray *lovedItems = nil;
    if (self.kid == nil)
        lovedItems = [Item MR_findByAttribute:@"readAction" withValue:@(2) andOrderBy:@"itemDate" ascending:NO];
    else
        lovedItems = [Item MR_findAllSortedBy:@"itemDate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"readAction==2 AND kids contains %@", self.kid]];
    
    int newLovedItems = 0;
    for (Item *item in lovedItems)
    {
        if (item.readAction.intValue != 2 || [GlobalUtils daysBetweenDate:item.createDate andDate:[NSDate date]]>1)
        {
            newLovedItems++;
        }
    }
    
    */
    createButtonBlock(FILTER_TYPE_ALL, @"All", firstItem, [items count], newItemsAll);
    if ([itemsWithComments count] > 0) {
        createButtonBlock(FILTER_TYPE_COMMENTS, @"", nil, [itemsWithComments count], newComments);
    }

    //createButtonBlock(FILTER_TYPE_LIKES, @"", nil, [lovedItems count], newLovedItems);
    createButtonBlock(FILTER_TYPE_CONTACTS, @"gallery", nil, [items count], newItemsAll);

    NSInteger currentYear = -1;

    int currentYearItemsCount = 0;
    int currentYearNewItemsCount = 0;
    Item *currentYearFirstItem = nil;
    for (Item *item in items) {
        NSInteger itemYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:item.itemDate].year;
        if (currentYear != itemYear) {
            if (currentYearFirstItem != nil) {
                createButtonBlock(FILTER_TYPE_YEAR, [NSString stringWithFormat:@"%ld", (long)currentYear], currentYearFirstItem, currentYearItemsCount, currentYearNewItemsCount);
            }
            currentYear = itemYear;
            currentYearItemsCount = 0;
            currentYearNewItemsCount = 0;
            currentYearFirstItem = item;
        }

        currentYearItemsCount++;
        if (item.readAction.intValue != 2 || [GlobalUtils daysBetweenDate:item.createDate andDate:[NSDate date]] > 1) {
            currentYearNewItemsCount++;
        }
    }

    if (currentYearFirstItem != nil) {
        createButtonBlock(FILTER_TYPE_YEAR, [NSString stringWithFormat:@"%ld", (long)currentYear], currentYearFirstItem, currentYearItemsCount, currentYearNewItemsCount);
    }


    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, ay + 105);
}

- (void)kidSelected:(NSNotification *)notification {
    self.kid = (Kid *) notification.object;
    [self reloadData];
}

- (void)btnTap:(UITapGestureRecognizer *)gesture; {
    for (UIView *control in self.scrollView.subviews) {
        if ([control isKindOfClass:[UIView class]]) {
            NSInteger i = [control.subviews count];
            if (i > 0) {
                UIImageView *borderView = [control.subviews objectAtIndex:i - 1];
                borderView.hidden = YES;
            }
        }
    }
    UILabel *lbl = (UILabel *) [gesture.view.subviews objectAtIndex:1];
    UIImageView *borderView = [gesture.view.subviews objectAtIndex:[gesture.view.subviews count] - 1];
    borderView.hidden = NO;
    [self.delegate filterSelected:gesture.view.tag withValue:lbl.text];
}

- (void)itemAdded:(NSNotification *)notification {
    
    Item *item = (Item *) notification.object;
    if (item == nil || self.kid == nil || [item.kids containsObject:self.kid]) {
        [self reloadData];
    }
    return;
}

- (void)commentAdded:(NSNotification *)notification {
    return;
    Comment *comment = (Comment *) notification.object;
    if (self.kid == nil || [comment.item.kids containsObject:self.kid]) {
        [self reloadData];
    }

}

- (void)kidsRefreshed:(NSNotification *)notification {
    [self reloadData];
}

- (void)itemDeleted:(NSNotification *)notification {
    [self reloadData];
}


@end
