//
//  RegisterViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/7/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "KPWebViewController.h"
#import "KPSelectionPopViewController.h"
#import "AppDelegate.h"
//#import "TapjoyConnect.h"

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>



@interface RegisterViewController () <UITextFieldDelegate, LoginViewDelegate, KPSelectionDelegate,UITextViewDelegate>

@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property(nonatomic, strong) IBOutlet UITextField *currentTF;
@property(nonatomic, strong) IBOutlet UITextField *rEmailTF;
@property(nonatomic, strong) IBOutlet UITextField *rNameTF;
@property(nonatomic, strong) IBOutlet UITextField *rPasswordTF;
@property(nonatomic, strong) IBOutlet UIButton *momButton;
@property(nonatomic, strong) IBOutlet UIButton *dadButton;
@property(nonatomic, strong) IBOutlet UIButton *loginButton;
@property(nonatomic, strong) IBOutlet UIButton *termsButton;
@property(nonatomic, strong) IBOutlet UIButton *otherButton;
@property(nonatomic, strong) IBOutlet UIImageView *passwordBkg;
@property(nonatomic, strong) IBOutlet UILabel *iamLabel;
@property(nonatomic, strong) IBOutlet UILabel *momLabel;
@property(nonatomic, strong) IBOutlet UILabel *dadLabel;
@property(nonatomic, strong) IBOutlet UILabel *otherLabel;
@property(nonatomic, strong) IBOutlet UILabel *warningLabel;

@property(nonatomic, strong) IBOutlet UIButton *birthDateButton;
@property(nonatomic, strong) IBOutlet UILabel *yearLbl;
@property(nonatomic, strong) IBOutlet UILabel *monthLbl;
@property(nonatomic, strong) IBOutlet UILabel *dayLbl;
@property(nonatomic, strong) IBOutlet UILabel *birthDateLabel;


@property(nonatomic, strong) UITextField *otherTF;
@property(nonatomic, strong) UIImageView *otherTextBkg;

@property(nonatomic) int mode;

@property(nonatomic, strong) NSDictionary *fbData;
@property(nonatomic, strong) NSString *fbAccessToken;

@property(nonatomic, strong) NSDate *birthdate;
@property(nonatomic, strong) KPSelectionPopViewController *selectionPopVC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordTFTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordBgTopConstraint;
@property (weak, nonatomic) IBOutlet UITextView *termsAndConditionsTextView;

@property(nonatomic, strong) NSString *email;

- (IBAction)relationTap:(id)sender;

- (IBAction)loginTap:(id)sender;

- (IBAction)termsTap:(id)sender;

- (IBAction)privacyTap:(id)sender;

- (IBAction)dateTap:(id)sender;

@end

@implementation RegisterViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithMode:(int)mode {
    self = [super initWithNibName:@"RegisterViewController" bundle:nil];
    if (self) {
        self.mode = mode;
    }
    return self;
}

- (instancetype)initWithFacebook:(NSDictionary *)fbData withAccessToken:(NSString *)fbAccessToken {
    self = [super initWithNibName:@"RegisterViewController" bundle:nil];
    if (self) {
        self.fbData = fbData;
        self.fbAccessToken = fbAccessToken;
    }
    return self;
}

- (instancetype)initWithEmail:(NSString *)email {
    self = [super initWithNibName:@"RegisterViewController" bundle:nil];
    if (self) {
        self.email = email;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.mode == 0) {

        [[Mixpanel sharedInstance] track:@"Signup screen"];
        [[Mixpanel sharedInstance] track:@"onboarding-registration-open"];


        self.title = KPLocalizedString(@"create account");
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"back"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", @(NO), @"disabled", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];

        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"sign-up", @"source", nil]];

    }
    else {

        [[Mixpanel sharedInstance] track:@"My Profile"];

        [[Analytics sharedAnalytics] track:@"screen-open" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-profile", @"source", nil]];


        self.title = KPLocalizedString(@"my profile");
        [self setButtons:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"settings"), @"title", @(BUTTON_TYPE_BACK), @"buttonType", @(NO), @"disabled", nil] andRightButtonInfo:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"save"), @"title", @(BUTTON_TYPE_PRIME), @"buttonType", @(YES), @"disabled", nil]];
    }

    self.scrollView.scrollsToTop = NO;

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardDidShow:)
                   name:UIKeyboardDidShowNotification
                 object:nil];

    [[NSNotificationCenter defaultCenter]
            addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:gesture];

    for (UIControl *control in [(UIView *) [self.scrollView.subviews objectAtIndex:0] subviews]) {
        if ([control isKindOfClass:[UILabel class]]) {
            if ([(UILabel *) control tag] == 1) {
                [(UILabel *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:12]];
            } else if ([(UILabel *) control tag] > 1) {
                [(UILabel *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:14]];
            } else {
                [(UILabel *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:17]];
            }

            [(UILabel *) control setTextColor:UIColorFromRGB(0x523c37)];
        }
        else if ([control isKindOfClass:[UITextField class]]) {
            [(UITextField *) control setFont:[UIFont fontWithName:@"ARSMaquettePro-Regular" size:17]];
            [(UITextField *) control setTextColor:UIColorFromRGB(0x4d3c36)];
        }
    }

    if (self.email.length > 0) {
        self.rEmailTF.text = self.email;
    }
   
    self.otherTextBkg = [[UIImageView alloc]init];
    self.otherTF = [[UITextField alloc] init];
    self.otherTextBkg.frame = CGRectMake(22, 226, self.view.frame.size.width + 10 , 44);
    self.otherTF.frame = CGRectMake(40, 233, self.view.frame.size.width + 10 , 30);

    if ([[UIScreen mainScreen] bounds].size.width == 320.0f) {
        self.otherTextBkg.frame = CGRectMake(22, 226, self.view.frame.size.width - 44, 44);
        self.otherTF.frame = CGRectMake(40, 233, self.view.frame.size.width - 40, 30);
    }
    self.otherTextBkg.image = [[UIImage imageNamed:@"input-field"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 25, 0, 25)];
    self.otherTextBkg.alpha = 0;
    [[self.scrollView.subviews objectAtIndex:0] addSubview:self.otherTextBkg];

    self.otherTF.borderStyle = UITextBorderStyleNone;
    self.otherTF.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:18];
    self.otherTF.textColor = UIColorFromRGB(0x523c37);
    self.otherTF.placeholder = KPLocalizedString(@"I am...");
    self.otherTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.otherTF.spellCheckingType = UITextSpellCheckingTypeNo;
    self.otherTF.alpha = 0;
    self.otherTF.delegate = self;
    self.otherTF.returnKeyType = UIReturnKeyNext;
    [[self.scrollView.subviews objectAtIndex:0] addSubview:self.otherTF];

    self.rNameTF.placeholder = KPLocalizedString(@"name");
    self.rEmailTF.placeholder = KPLocalizedString(@"email");
    self.rPasswordTF.placeholder = KPLocalizedString(@"password");

    
    
    
    [self.rEmailTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.rNameTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.rPasswordTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.otherTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    
    [self setAttributedText];
    self.iamLabel.text = KPLocalizedString(@"I am...");
    self.momLabel.text = KPLocalizedString(@"mom");
    self.dadLabel.text = KPLocalizedString(@"dad");
    self.otherLabel.text = KPLocalizedString(@"other");
    self.warningLabel.text = KPLocalizedString(@"By creating a keepy account you acknowledge that you have read, understood and agree to the keepy Terms of Use and Privacy Policy");

    if (self.mode == 0) {
        /*
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            for (int i = 1494; i > 1478; i--)
            {
                Asset *asset = [Asset MR_createEntity];
                asset.assetId.intValue = i;
                asset.assetType.intValue = 0;
                [[ServerComm instance] downloadAssetFile:asset withSuccessBlock:nil andFailBlock:nil];
            }
            
            Asset *asset = [Asset MR_createEntity];
            asset.assetId.intValue = 1639;
            asset.assetType.intValue = 0;
            [[ServerComm instance] downloadAssetFile:asset withSuccessBlock:nil andFailBlock:nil];
            
            asset = [Asset MR_createEntity];
            asset.assetId.intValue = 1634;
            asset.assetType.intValue = 0;
            [[ServerComm instance] downloadAssetFile:asset withSuccessBlock:nil andFailBlock:nil];
            
            
        });
         */
        [self.termsAndConditionsTextView setHidden:NO];
        self.rPasswordTF.hidden = (self.fbData != nil);
        self.passwordBkg.hidden = (self.fbData != nil);
        self.passwordBgTopConstraint.constant = 14;
        self.passwordTFTopConstraint.constant = 21;
        if (self.fbData) {
            self.rNameTF.text = [self.fbData valueForKey:@"first_name"];
            if ([[self.fbData valueForKey:@"email"] rangeOfString:@"e.com"].length < 1) {
                self.rEmailTF.text = [self.fbData valueForKey:@"email"];
            }
        }
    }
    else {
        self.rNameTF.text = [[UserManager sharedInstance] getMe].name;
        self.rEmailTF.text = [[UserManager sharedInstance] getMe].email;
        self.dadButton.selected = ([[UserManager sharedInstance] getMe].parentType.intValue == 1);
        self.momButton.selected = ([[UserManager sharedInstance] getMe].parentType.intValue == 0);
        self.loginButton.hidden = YES;
        if (([[UserManager sharedInstance] getMe].parentType.intValue == 2)) {
            self.otherTF.alpha = 1;
            self.otherTextBkg.alpha = 1;
            self.otherTF.text = [[UserManager sharedInstance] getMe].otherTxt;
            [self relationTap:self.otherButton];
        }

        self.rPasswordTF.placeholder = KPLocalizedString(@"change password");
        CGRect aframe = self.rPasswordTF.frame;
        aframe.origin.y += 100;
        self.rPasswordTF.frame = aframe;

        aframe = self.passwordBkg.frame;
        aframe.origin.y += 100;
        self.passwordBkg.frame = aframe;

        //hide terms & privacy controls
//        for (int i = 13; i < [[[self.scrollView.subviews objectAtIndex:0] subviews] count] - 4; i++) {
//            [[[[self.scrollView.subviews objectAtIndex:0] subviews] objectAtIndex:i] setHidden:YES];
//        }
        [self.termsAndConditionsTextView setHidden:YES];
        self.birthDateButton.hidden = NO;
        self.birthDateLabel.hidden = NO;

        float ax = self.birthDateButton.center.x;//self.birthDateButton.frame.origin.x;
        float ay = 7 + self.birthDateButton.frame.origin.y;

        //Month
        self.monthLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay, 53, 15)];
        self.monthLbl.backgroundColor = [UIColor clearColor];
        self.monthLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
        self.monthLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
        self.monthLbl.textAlignment = NSTextAlignmentCenter;
        [self.scrollView addSubview:self.monthLbl];

        //Day
        self.dayLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay + 15, 53, 15)];
        self.dayLbl.backgroundColor = [UIColor clearColor];
        self.dayLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
        self.dayLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:16];
        self.dayLbl.textAlignment = NSTextAlignmentCenter;
        [self.scrollView addSubview:self.dayLbl];

        //Year
        self.yearLbl = [[UILabel alloc] initWithFrame:CGRectMake(ax, ay + 30, 53, 15)];
        self.yearLbl.backgroundColor = [UIColor clearColor];
        self.yearLbl.textColor = [UIColor colorWithRed:0.318 green:0.227 blue:0.212 alpha:1];
        self.yearLbl.font = [UIFont fontWithName:@"ARSMaquettePro-Regular" size:10];
        self.yearLbl.textAlignment = NSTextAlignmentCenter;
        self.yearLbl.alpha = 0.8;
        [self.scrollView addSubview:self.yearLbl];

        self.birthDateLabel.text = KPLocalizedString(@"birth date");

        self.birthdate = [[UserManager sharedInstance] getMe].birthdate;
        self.passwordBgTopConstraint.constant = 104;
        self.passwordTFTopConstraint.constant = 111;
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
  
    
    if ([[UIScreen mainScreen] bounds].size.width != 320.0f) {
        self.otherTextBkg.frame = CGRectMake(22, 226, self.view.frame.size.width - 44 , 44);
        self.otherTF.frame = CGRectMake(40, 233, self.view.frame.size.width - 80 , 30);
    }
}

- (void)viewDidLayoutSubviews {
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

#pragma mark - Actions

- (void)topBtnTap:(UIButton *)sender {
    if (sender != nil && sender.tag == 0) {
        if (self.mode == 0) {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"sign-up", @"source", @"cancel", @"result", nil]];
        } else {
            [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-profile", @"source", @"cancel", @"result", nil]];
        }


        [self.navigationController popViewControllerAnimated:YES];
        return;
    }

    int parentType = 0;
    if (self.dadButton.selected) {
        parentType = 1;
    } else if (self.otherButton.selected) {
        parentType = 2;
    }

    if (self.mode == 0) {
        [SVProgressHUD showWithStatus:KPLocalizedString(@"processing registration")];
        NSString *fbId = nil;
        if (self.fbData) {
            fbId = [self.fbData valueForKey:@"id"];
        }

        [[ServerComm instance] registerUser:self.rEmailTF.text withName:self.rNameTF.text andPassword:self.rPasswordTF.text andParentType:parentType andOtherTxt:(self.otherButton.selected) ? self.otherTF.text : @"" andFBId:fbId andFBAccessToken:self.fbAccessToken withSuccessBlock:^(id result) {

            int rstatus = [[result valueForKey:@"status"] intValue];
            if (rstatus == 1) {
                NSMutableString *errorMsg = [[NSMutableString alloc] init];
                NSArray *errors = [[result valueForKey:@"error"] valueForKey:@"errors"];
                if ([errors count] == 0) {
                    [errorMsg appendString:@"registration error"];
                } else {
                    [errorMsg appendString:@"Please fix the following:\n"];
                }
                for (NSString *errorStr in errors) {
                    [errorMsg appendFormat:@"%@\n", errorStr];
                }
                [[Mixpanel sharedInstance] track:@"Registration Error" properties:[NSDictionary dictionaryWithObjectsAndKeys:errorMsg, @"error", nil]];

                [SVProgressHUD showErrorWithStatus:errorMsg];
            }
            else {

                //[TapjoyConnect actionComplete:@"8b397510-0874-4929-ab16-9e135b2ffa91"];

                [self saveUser:[result valueForKey:@"result"]];

                [[Mixpanel sharedInstance] track:@"become-active"];

                [FBSDKAppEvents logEvent:FBSDKAppEventNameCompletedRegistration
                           parameters:@{FBSDKAppEventParameterNameRegistrationMethod : (self.fbAccessToken == nil) ? @"email" : @"facebook"}];
                /*[[MobileAppTracker sharedManager] trackActionForEventIdOrName:@"Registration"
                                                                    eventIsId:NO];*/

                [SVProgressHUD dismiss];

                [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"sign-up", @"source", @"ok", @"result", nil]];

            }

        }                      andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error connecting to keepy\'s servers. please try again soon")];
        }];
    }
    else {
        [SVProgressHUD showWithStatus:KPLocalizedString(@"updating")];
        [[ServerComm instance] updateUser:self.rEmailTF.text withName:self.rNameTF.text andPassword:self.rPasswordTF.text andParentType:parentType andOtherTxt:(self.otherButton.selected) ? self.otherTF.text : @"" andBirthdate:self.birthdate withSuccessBlock:^(id result) {

            int rstatus = [[result valueForKey:@"status"] intValue];
            if (rstatus == 1) {
                NSMutableString *errorMsg = [[NSMutableString alloc] init];
                NSArray *errors = [[result valueForKey:@"error"] valueForKey:@"errors"];
                if ([errors count] == 0) {
                    [errorMsg appendString:@"update error"];
                } else {
                    [errorMsg appendString:@"Please fix the following:\n"];
                }
                for (NSString *errorStr in errors) {
                    [errorMsg appendFormat:@"%@\n", errorStr];
                }

                [[Mixpanel sharedInstance] track:@"update user Error" properties:[NSDictionary dictionaryWithObjectsAndKeys:errorMsg, @"error", nil]];

                [SVProgressHUD showErrorWithStatus:errorMsg];
            }
            else {
                [[Mixpanel sharedInstance] track:@"update successful"];


                User *me = [[UserManager sharedInstance] getMe];
                me.name = self.rNameTF.text;
                me.email = self.rEmailTF.text;
                me.parentType = @(parentType);
                me.otherTxt = self.otherTF.text;
                me.birthdate = self.birthdate;
                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];


                [SVProgressHUD dismiss];

                [GlobalUtils updateAnalyticsUser];

                [[Analytics sharedAnalytics] track:@"screen-close" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"my-profile", @"source", @"ok", @"result", nil]];

                [self.navigationController popViewControllerAnimated:YES];
            }

        }                    andFailBlock:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"There was a problem contacting Keepy\'s servers. Please try again soon."];
        }];

    }
}

- (IBAction)loginTap:(id)sender {

    [[Mixpanel sharedInstance] track:@"Sign In Tap"];

    [self viewTap:nil];

    
    LoginViewController *avc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    avc.delegate = self;
    [self.navigationController pushViewController:avc animated:YES];
    
    
}



- (void)viewTap:(id)sender {
    if (self.currentTF != nil) {
        [self.currentTF resignFirstResponder];
    }
}

- (IBAction)relationTap:(id)sender {
    [self viewTap:nil];

    BOOL wasOther = self.otherButton.selected;

    self.momButton.selected = NO;
    self.dadButton.selected = NO;
    self.otherButton.selected = NO;
    [(UIButton *) sender setSelected:YES];

    UIView *container = (UIView *) [self.scrollView.subviews objectAtIndex:0];

    if (self.otherButton.selected && !wasOther) {
        [UIView animateWithDuration:0.3 animations:^{
            for (int i = 9; i < [[container subviews] count] - 2; i++) {
                UIView *v = (UIView *) [container.subviews objectAtIndex:i];
                CGRect aframe = v.frame;
                aframe.origin.y += 60;
                v.frame = aframe;
            }
        }                completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                self.otherTextBkg.alpha = 1.0;
                self.otherTF.alpha = 1.0;

                self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.scrollView.contentSize.height + 60);
            }                completion:^(BOOL finished) {

            }];
        }];
    }
    else if (!self.otherButton.selected && wasOther) {
        [UIView animateWithDuration:0.3 animations:^{
            self.otherTextBkg.alpha = 0;
            self.otherTF.alpha = 0;
        }                completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3 animations:^{
                for (int i = 9; i < [[container subviews] count] - 2; i++) {
                    UIView *v = (UIView *) [container.subviews objectAtIndex:i];
                    CGRect aframe = v.frame;
                    aframe.origin.y -= 60;
                    v.frame = aframe;
                }
            }                completion:^(BOOL finished) {
                self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.scrollView.contentSize.height - 60);
            }];

        }];
    }
    [self checkEnableSaveButton];
}

- (IBAction)termsTap:(id)sender {
    KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/terms.html" andTitle:@"terms of use"];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    navController.view.tag = 1;
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)privacyTap:(id)sender {
    KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/privacy.html" andTitle:@"privacy policy"];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    navController.view.tag = 1;
    [self presentViewController:navController animated:YES completion:nil];
}
-(void)setAttributedText{
    
   
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] init];
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"By creating a keepy account you acknowledge that you have read, understood and agree to the keepy "]];
    
  
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Terms of Use"
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                                                                          @"Terms of Use": @(YES),
                                                                                          NSBackgroundColorAttributeName: [UIColor clearColor]}]];
      [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" and "]];
    
 
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Privacy Policy"
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                                                                          @"Privacy Policy": @(YES),
                                                                                          NSBackgroundColorAttributeName: [UIColor clearColor]}]];
   
    
    self.termsAndConditionsTextView.attributedText = attributedString;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(didTapReadTerm:)];
    [self.termsAndConditionsTextView addGestureRecognizer:tapGesture];
}
- (void)didTapReadTerm:(UITapGestureRecognizer *)sender{
   
    UITextView *textView = (UITextView *)sender.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [sender locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    NSLog(@"location: %@", NSStringFromCGPoint(location));
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        NSDictionary *attributes = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        NSLog(@"%@, %@", attributes, NSStringFromRange(range));
        
        //Based on the attributes, do something
        ///if ([attributes objectForKey:...)] //make a network call, load a cat Pic, etc
        if ([attributes objectForKey:@"Privacy Policy"]) {
            KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/privacy.html" andTitle:@"privacy policy"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
            navController.view.tag = 1;
            [self presentViewController:navController animated:YES completion:nil];
        }
        if ([attributes objectForKey:@"Terms of Use"]) {
            KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:@"http://keepy.me/terms.html" andTitle:@"terms of use"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
            navController.view.tag = 1;
            [self presentViewController:navController animated:YES completion:nil];
        }
    }

}

#pragma mark - store user data

- (void)saveUser:(NSDictionary *)data {

    [self viewTap:nil];

    [[UserManager sharedInstance] setIsAfterRegistration:YES];

    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate initializeDataStoreForEmail:data[@"email"]];
    [GlobalUtils saveUser:data];
}

#pragma mark - Keyboard hide/show

- (void)keyboardDidShow:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(44.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    if (self.currentTF != nil) {
        CGPoint point = self.currentTF.frame.origin;
        point = [self.scrollView convertPoint:point fromView:(UIView *) [self.scrollView.subviews objectAtIndex:0]];
        CGRect aRect = CGRectMake(point.x, point.y, CGRectGetWidth(self.currentTF.frame), CGRectGetHeight(self.currentTF.frame));
        [self.scrollView scrollRectToVisible:aRect animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    self.scrollView.contentInset = UIEdgeInsetsMake(44.0, 0.0, 0, 0.0);;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 10, 10) animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.rNameTF) {
        if (self.otherButton.selected) {
            [self.otherTF becomeFirstResponder];
        } else {
            [self.rEmailTF becomeFirstResponder];
        }
    } else if (textField == self.otherTF) {
        [self.rEmailTF becomeFirstResponder];
    } else if (textField == self.rEmailTF) {
        [self.rPasswordTF becomeFirstResponder];
    } else if (textField == self.rPasswordTF) {
        [textField resignFirstResponder];
        if (self.rightButtonEnabled) {
            [self topBtnTap:nil];
        }
    }

    return YES;
}

/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self checkEnableSaveButton];
    return NO;
}
*/

- (void)textFieldDidChange:(UITextField *)textField{
    [self checkEnableSaveButton];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.currentTF = textField;
}

- (void)checkEnableSaveButton {
    if (self.mode == 1) {
        self.rightButtonEnabled = YES;
        return;
    }

    self.rightButtonEnabled = NO;

    if (self.rNameTF.text.length == 0) {
        return;
    }

    if (self.rEmailTF.text.length == 0) {
        return;
    }

    if (self.rPasswordTF.text.length < 2 && self.fbData == nil) {
        return;
    }

    if (!self.momButton.selected && !self.dadButton.selected && !self.otherButton.selected) {
        return;
    }

    self.rightButtonEnabled = YES;


}

#pragma mark - LoginViewDelegate

- (void)didLogin:(NSDictionary *)userInfo {
    [self saveUser:userInfo];
}

#pragma mark - KPSelectionDelegate

- (void)datePickerValueDidChange:(NSDate *)date {
    self.birthdate = date;
}

- (void)itemSelected:(NSUInteger)aindex {
    ///
}

- (void)itemSelectionCancelled {

}

- (void)setBirthdate:(NSDate *)birthdate {
    _birthdate = birthdate;

    if (_birthdate == nil) {
        return;
    }

    [self.birthDateButton setImage:[UIImage imageNamed:@"kidPicBg"] forState:UIControlStateNormal];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    [formatter setDateFormat:@"MMM"];
    self.monthLbl.text = [[formatter stringFromDate:birthdate] uppercaseString];

    [formatter setDateFormat:@"dd"];
    self.dayLbl.text = [formatter stringFromDate:birthdate];

    [formatter setDateFormat:@"yyyy"];
    self.yearLbl.text = [formatter stringFromDate:birthdate];

    [self checkEnableSaveButton];
}

- (IBAction)dateTap:(id)sender {
    [self.rNameTF resignFirstResponder];
    [self.rPasswordTF resignFirstResponder];
    [self.rEmailTF resignFirstResponder];

    CGPoint p = CGPointMake(self.birthDateButton.center.x - 20, self.birthDateButton.frame.origin.y - 170);

    NSArray *items = nil;
    if (self.birthdate != nil) {
        items = [NSArray arrayWithObject:self.birthdate];
    }

    self.selectionPopVC = [[KPSelectionPopViewController alloc] initWithItems:items withSelectedIndex:-1 andPoint:p];
    self.selectionPopVC.delegate = self;
    self.selectionPopVC.hideArrow = true;
    self.selectionPopVC.view.tag = 0;

    [self.view addSubview:self.selectionPopVC.view];
}


@end
