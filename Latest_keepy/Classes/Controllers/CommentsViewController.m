//
//  CommentsViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/18/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "CommentsViewController.h"
#import "Comment.h"
#import <MediaPlayer/MediaPlayer.h>

@interface CommentsViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) Item *item;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) MPMoviePlayerViewController *moviePlayer;

- (IBAction)closeTap:(id)sender;

@end

@implementation CommentsViewController

- (instancetype)initWithItem:(Item *)item {
    self = [super initWithNibName:@"CommentsViewController" bundle:nil];
    if (self) {
        self.item = item;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, self.view.frame.size.height)];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.transform = CGAffineTransformMakeRotation(-M_PI * 0.5);
    [self.tableView setFrame:CGRectMake(0, 30, self.view.frame.size.width, self.view.frame.size.height)];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    [self.view addSubview:self.tableView];

    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.item.comments.allObjects count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Comment *comment = [self.item.comments.allObjects objectAtIndex:indexPath.row];
    UIImageView *img = (UIImageView *) [cell.contentView.subviews objectAtIndex:0];
    [img setImageWithAsset:comment.previewImage];

    UILabel *lbl = (UILabel *) [cell.contentView.subviews objectAtIndex:1];
    if (comment.commentDate == nil) {
        lbl.text = @"Bahh";
    }
    else {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM dd"];
        lbl.text = [formatter stringFromDate:comment.commentDate];
    }
}

// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *kCellIdentifier = @"CommentCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];

        //Image
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 75, 75)];
        [cell.contentView addSubview:img];

        //Title
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 90, 15)];
        lbl.textColor = UIColorFromRGB(0xFFFFFF);
        lbl.backgroundColor = UIColorFromRGB(0x000000);
        [cell.contentView addSubview:lbl];

        cell.transform = CGAffineTransformMakeRotation(M_PI * 0.5);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    // Set up the cell...
    [self configureCell:cell atIndexPath:indexPath];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *comment = [self.item.comments.allObjects objectAtIndex:indexPath.row];
    [self.delegate commentSelected:comment];

    /*
    [[ServerComm instance] downloadAssetFile:comment.video withSuccessBlock:^(id result) {
        NSURL *fileURL = [NSURL fileURLWithPath:result];
        
        
        self.moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:fileURL];
        self.moviePlayer.moviePlayer.fullscreen = YES;
        
        // Remove the movie player view controller from the "playback did finish" notification observers
        [[NSNotificationCenter defaultCenter] removeObserver:self.moviePlayer
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:self.moviePlayer.moviePlayer];

        
        // Register this class as an observer instead
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerDoneButtonClick:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.moviePlayer.moviePlayer];
        
        AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [((HomeViewController*)d.viewController.frontViewController).itemsVC presentMoviePlayerViewControllerAnimated:self.moviePlayer];
        // Register for the playback finished notification.

     ///[[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(myMovieFinishedCallback:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.moviePlayer];
        //self.moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
        //self.moviePlayer.view.transform = CGAffineTransformConcat(self.moviePlayer.view.transform, CGAffineTransformMakeRotation(M_PI_2));
        
        //UIWindow *backgroundWindow = [[UIApplication sharedApplication] keyWindow];
        //[self.moviePlayer.view setFrame:backgroundWindow.frame];
        //[backgroundWindow addSubview:self.moviePlayer.view];
        //self.moviePlayer.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //self.moviePlayer.shouldAutoplay = YES;
        //[self.view addSubview:self.moviePlayer.view];
       // self.moviePlayer.initialPlaybackTime = -1.0;
        //[self.moviePlayer prepareToPlay];
        //[self.moviePlayer play];
        
    } andFailBlock:^(NSError *error) {
        
    }];
*/
}

/*
-(void)playerDoneButtonClick:(NSNotification*)notification
{
    NSNumber *finishReason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    // Dismiss the view controller ONLY when the reason is not "playback ended"
    if ([finishReason intValue] != MPMovieFinishReasonPlaybackEnded)
    {
        MPMoviePlayerController *moviePlayer = [notification object];
        
        // Remove this class from the observers
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];
        
        // Dismiss the view controller
        [self.moviePlayer dismissModalViewControllerAnimated:YES];
        //self.moviePlayer = nil;
    }
}
*/
#pragma mark - Actions

- (IBAction)closeTap:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect aframe = self.view.frame;
        aframe.origin.y = 500;
        self.view.frame = aframe;
    }                completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

@end
