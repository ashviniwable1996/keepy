//
//  AddGrandParentsViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddGrandParentsViewController.h"
#import "AddGrandParentViewController.h"
#import "KidsCell.h"
#import "FamilyCell.h"
#import "FamilySelectorCell.h"
#import "ProgressCell.h"

@interface AddGrandParentsViewController () <UITableViewDataSource, UITableViewDelegate, FamilySelectorCellDelegate, ProgressCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AddGrandParentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[Mixpanel sharedInstance] track:@"onboarding-add-grand-parents-open"];

    [self.tableView registerNib:[UINib nibWithNibName:@"KidsCell" bundle:nil] forCellReuseIdentifier:@"KidsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilyCell" bundle:nil] forCellReuseIdentifier:@"FamilyCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilySelectorCell" bundle:nil] forCellReuseIdentifier:@"FamilySelectorCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProgressCell" bundle:nil] forCellReuseIdentifier:@"ProgressCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        static NSString *KidsCellIdentifier = @"KidsCell";
        KidsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KidsCellIdentifier];
        cell.kids = self.kids;
        [cell layout];
        return cell;
    } else if (indexPath.row == 1) {
        static NSString *FamilyCellIdentifier = @"FamilyCell";
        FamilyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FamilyCellIdentifier];
        cell.family = self.family;
        [cell layout];
        return cell;
    } else if (indexPath.row == 2) {
        static NSString *FamilySelectorCellIdentifier = @"FamilySelectorCell";
        FamilySelectorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FamilySelectorCellIdentifier];
        cell.delegate = self;
        cell.familySelectorCellType = FamilySelectorCellTypeGrandParents;
        cell.title.text = [self title];
        [cell layout];
        return cell;
    } else if (indexPath.row == 3) {
        static NSString *ProgressCellIdentifier = @"ProgressCell";
        ProgressCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ProgressCellIdentifier];
        cell.delegate = self;
        cell.progressCellState = ProgressCellStateTwo;
        [cell layout];
        return cell;
    }
    return nil;
}

- (NSString *)title {
    BOOL foundGrandParent = NO;
    for (NSDictionary *familyMember in self.family) {
        int relationshipTypeId = [[familyMember objectForKey:@"fanRelationType"] intValue];
        if (relationshipTypeId == AddGrandParentViewControllerTypeGrandma || relationshipTypeId == AddGrandParentViewControllerTypeGrandpa) {
            foundGrandParent = YES;
            break;
        }
    }
    if (foundGrandParent) {
        return @"Any more grandparents?";
    } else {
        return @"Share your memories with grandparents";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [KidsCell height];
    } else if (indexPath.row == 1) {
        return [FamilyCell height];
    } else if (indexPath.row == 2) {
        return [FamilySelectorCell height:[self title]];
    } else if (indexPath.row == 3) {
        return [ProgressCell height];
    }
    return 0.0f;
}

- (CGFloat)height {
    return [KidsCell height] + [FamilyCell height] + [FamilySelectorCell height:[self title]] + [ProgressCell height];
}

- (IBAction)addGrandParentsExit:(UIStoryboardSegue *)segue {
    AddGrandParentViewController *addGrandParentViewController = (AddGrandParentViewController *) segue.sourceViewController;
    if (addGrandParentViewController.family) {
        self.family = addGrandParentViewController.family;
        [self.tableView reloadData];
    }
}

- (void)leftSelected:(id)sender {
    [[Mixpanel sharedInstance] track:@"onboarding-add-grand-parents-add-grandma-tap"];

    [self performSegueWithIdentifier:@"AddGrandma" sender:self];
}

- (void)rightSelected:(id)sender {
    [[Mixpanel sharedInstance] track:@"onboarding-add-grand-parents-add-grandpa-tap"];

    [self performSegueWithIdentifier:@"AddGrandpa" sender:self];
}

- (void)skip:(id)sender {
    [[Mixpanel sharedInstance] track:@"onboarding-add-grand-parents-skip-tap"];

    [self.delegate doneAddingGrandParents:self.family];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddGrandma"]) {
        AddGrandParentViewController *addGrandParentViewController = (AddGrandParentViewController *) segue.destinationViewController;
        addGrandParentViewController.addGrandParentViewControllerType = AddGrandParentViewControllerTypeGrandma;
    } else if ([segue.identifier isEqualToString:@"AddGrandpa"]) {
        AddGrandParentViewController *addGrandParentViewController = (AddGrandParentViewController *) segue.destinationViewController;
        addGrandParentViewController.addGrandParentViewControllerType = AddGrandParentViewControllerTypeGrandpa;
    }
}

@end
