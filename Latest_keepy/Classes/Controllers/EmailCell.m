//
//  EmailCell.m
//  Keepy
//
//  Created by Troy Payne on 2/16/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "EmailCell.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface EmailCell () <ABPeoplePickerNavigationControllerDelegate>

@property(nonatomic, weak) IBOutlet UIImageView *backgroundImageView;

@end

@implementation EmailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundImageView.image = [[UIImage imageNamed:@"input"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 33.0f, 0.0f, 33.0f) resizingMode:UIImageResizingModeStretch];
    self.input.font = [UIFont fontWithName:@"ARSMaquettePro-Light" size:17];
}

+ (CGFloat)height {
    return 44.0f;
}

- (IBAction)selectEmail:(id)sender {
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    [[self.delegate viewController] presentViewController:picker animated:YES completion:nil];
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [[self.delegate viewController] dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    return YES;
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
                         didSelectPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier {
    
    if (property == kABPersonEmailProperty) {
        ABMultiValueRef emailProperty = ABRecordCopyValue(person, property);
        self.input.text = (__bridge NSString *) ABMultiValueCopyValueAtIndex(emailProperty, identifier);
        [[self.delegate viewController] dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
