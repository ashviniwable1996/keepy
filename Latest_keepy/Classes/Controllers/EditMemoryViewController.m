//
//  EditMemoryViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/13/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "EditMemoryViewController.h"

@implementation EditMemoryViewController

- (IBAction)editMemoryExit:(UIStoryboardSegue *)storyboard {

}

- (IBAction)next:(id)sender {
    [self performSegueWithIdentifier:@"AddStory" sender:self];
}

@end
