//
//  DKVideoUploaderManager.m
//  Keepy
//
//  Created by Daniel Karsh on 4/25/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "DKVideoUploaderManager.h"
#import <AVFoundation/AVFoundation.h>
#import "KPDataParser.h"
#import "AppDelegate.h"
#import "KPLocalItem.h"
#import "KPLocalAsset.h"
#import "KPLocalKid.h"
#import "KPLocalPlace.h"
#import "KPLocalItemTag.h"
#import "KPLocalTag.h"

#import "Keepy-Swift.h"

typedef void (^BoolBlock)(BOOL ready);

@interface DKVideoUploaderManager ()

@property(nonatomic, copy) BoolBlock readyAssets;
@property(nonatomic, strong) ServerComm *serverComm;
@property(nonatomic, strong) KPLocalItem *item;
@property(nonatomic, strong) KPLocalKid *kid;
@property(nonatomic, strong) KPLocalPlace *place;
@property(nonatomic, strong) KPTextView *titleTF;

@property(nonatomic, strong) NSDictionary *prepareAssetResult;

@property(nonatomic, strong) AVAssetExportSession *exportSession;
@property(nonatomic, strong) NSTimer *exportProgressBarTimer;

@property(nonatomic, strong) NSMutableArray *selectedKids;
@property(nonatomic, strong) NSDictionary *videoInfo;
@property(nonatomic, strong) NSDate *itemDate;
@property(nonatomic, strong) NSArray *tagIds;

@property(nonatomic, strong) NSDictionary *theResult;
@property(nonatomic, strong) NSDictionary *apiResult;
@property(nonatomic, strong) NSString *sdVideoAssetId;
@property(nonatomic, strong) NSMutableArray *faildVideo;




@property(nonatomic, copy) APISuccessBlock myApiSuccess;
@property(nonatomic) BOOL igotIds;
@property(nonatomic) BOOL shareSwitch;
@property(nonatomic) BOOL sendMe;
@property(nonatomic) BOOL userClickCancel;
@property(nonatomic) int thumbId;
@property(nonatomic) int videoId;
@property(nonatomic) int tryAdding;
@property(nonatomic) int tryToRecover;
@property(assign) BOOL killProcess;
@property(assign) BOOL deleteVItem;
@property(nonatomic, strong) NSDictionary *nresult;


@property(nonatomic) float memSoFar;

@end

@implementation DKVideoUploaderManager


- (instancetype)init {
    self = [super init];
    if (self) {

        appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        if (!self.faildVideo) {
            self.faildVideo = [[NSMutableArray alloc] init];
        }

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cancelCurrentUpload:)
                                                     name:CANCEL_CURRENT_UPLOAD_NOTIFICATION
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(test:)
                                                     name:@"TEST"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadingProgress:) name:@"VideoUploadProgress" object:nil];
    }
    return self;
}

- (void)test:(NSNotification *)userInfo {
    [self startToCheckVideoStatusAssetID:self.sdVideoAssetId];
}

- (void)killMe {
    [self.exportSession cancelExport];
    [self updateAssetUploadTotalBytesWritten:0 totalBytesExpectedToWrite:-1];
    [self.serverComm cancelOperation];

    self.myMovieFileURL = nil;
    self.thumbnail = nil;
    [self.uploadProgressView removeFromSuperview];
    self.uploadProgressView = nil;
    self.myID = nil;
    self.serverComm = nil;
    self.item = nil;
    self.kid = nil;
    self.place = nil;
    self.titleTF = nil;
    self.prepareAssetResult = nil;

    self.exportSession = nil;
    self.exportProgressBarTimer = nil;

    self.selectedKids = nil;
    self.videoInfo = nil;
    self.itemDate = nil;
    self.tagIds = nil;
    self.theResult = nil;
    self.sdVideoAssetId = nil;
    self.myApiSuccess = nil;
    self.nresult = nil;
    self.viewController = nil;

}

- (void)dealloc {
}

- (void)deleteItemFromServer:(NSNumber *)iid {

    int itemId = !iid ? self.myID.intValue : [iid intValue];


    [[ServerComm instance] deleteItem:itemId
                     withSuccessBlock:^(id result) {

                         // DDLogInfo(@"%@",result);

                         if ( [result objectForKey:@"result"] != [NSNull null] && [[result objectForKey:@"result"] objectForKey:@"error"] == [NSNull null]) {
                             [[Mixpanel sharedInstance] track:@"video delete" properties:@{@"assetId" : @(itemId), @"state" : @"success"}];
                             [[[Mixpanel sharedInstance] people] increment:@"Number of video" by:@(-1)];
                         }


                     } andFailBlock:^(NSError *error) {

                [[Mixpanel sharedInstance] track:@"video delete" properties:@{@"assetId" : @(itemId), @"state" : @"fail", @"error" : [error localizedDescription]}];

                [self performSelector:@selector(deleteItemFromServer:) withObject:@(itemId) afterDelay:5.0];
                // DDLogInfo(@"%@",error.description);
            }];
}


- (void)failedProgressBar {
    if (self.uploadProgressView) {

        DDLogInfo(@"Wait for user to delete this faild video");

//        UIImageView *imageView = (UIImageView *) [self.uploadProgressView viewWithTag:2221];
//        imageView.image = [UIImage imageNamed:@"failed-icon"];


        if ([self.uploadProgressView viewWithTag:2222]) {
            UILabel *lbl = (UILabel *) [self.uploadProgressView viewWithTag:2222];
            lbl.text = @"ack! your upload failed";
            lbl.textColor = [UIColor redColor];

        }

    }
}

- (void)weHaveAnError:(NSError *)err {

    // DDLogInfo(@"%@",err.description);



    if (self.uploadProgressView) {

//        UIImageView *imageView = (UIImageView *) [self.uploadProgressView viewWithTag:2221];
//        imageView.image = [UIImage imageNamed:@"failed-icon"];


//        if ([self.uploadProgressView viewWithTag:2222]) {
//            UILabel *lbl = (UILabel *) [self.uploadProgressView viewWithTag:2222];
//            lbl.text = @"ack! your upload failed";
//            lbl.textColor = [UIColor redColor];
//
//        }

        if ([self.uploadProgressView viewWithTag:1315]) {
            //UIButton *retry = (UIButton*)[self.uploadProgressView viewWithTag:1315];
            //retry.hidden = NO;
        }

        /*
        if ([self.apiResult objectForKey:self.myID]) {
            if ([[self.apiResult objectForKey:self.myID] objectForKey:@"error"] == [NSNull null]) {
                DDLogInfo(@"The video item %@ delete already",self.myID);
                return;
            }
           
        }
        else{
            if (self.userClickCancel) {
                return;
            }
            [self deleteItemFromServer];
        }*/

        if (self.userClickCancel) {return;}


        [self deleteItemFromServer:nil];
    }

}

- (void)setMyMovieFileURL:(NSURL *)movieFileURL {
    _myMovieFileURL = movieFileURL;
    [self updateAssetUploadTotalBytesWritten:0 totalBytesExpectedToWrite:0];
}


- (void)setItem:(Item *)titem
         setKid:(Kid *)tkid
       setPlace:(Place *)tplace
     setTitleTF:(KPTextView *)ttitleTF
setSelectedKids:(NSMutableArray *)tselectedKids
   setVideoInfo:(NSDictionary *)tvideoInfo
    setItemDate:(NSDate *)titemDate
      setSwitch:(BOOL)tshareSwitch
      setTagIds:(NSArray *)ttagIds
withSuccessItem:(APISuccessBlock)apiSuccess {
    DDLogInfo(@"////////////////////////////>>>>>>>>>>>>>>>>>>>>  self.videoId = %d  self.thumbId = %d", self.videoId, self.thumbId);

    self.myApiSuccess = apiSuccess;
    self.item = (id)titem;
    self.kid = (id)tkid;
    self.place = (id)tplace;
    self.titleTF = ttitleTF;
    self.selectedKids = tselectedKids;
    self.videoInfo = tvideoInfo;
    self.itemDate = titemDate;
    self.shareSwitch = tshareSwitch;
    self.tagIds = ttagIds;
    self.sendMe = YES;
    self.startFromHere = 0;
    self.tryAdding = 0;



    // __block typeof(DKVideoUploaderManager*) mySelf = self ;
    __block DKVideoUploaderManager *mySelf = self;

    [self addNewVideoWhenAssetsReady:^(BOOL ready) {
        [mySelf addNewVideo];
    }];



//    else [self performSelector:@selector(retryAddingItem) withObject:nil afterDelay:2.0];
}


- (void)addNewVideoWhenAssetsReady:(BoolBlock)ready {
    if (ready) {
        if (self.igotIds) {
            ready(YES);
        }
        self.readyAssets = ready;
    }
}

- (void)retryAddingItem {
    if (self.item.itemType == 3)//is image
    {
        self.tryAdding++;
        if (self.tryAdding > 7) {
            [self weHaveAnError:nil];
        } else {
            [self performSelector:@selector(retryAddingItem) withObject:nil afterDelay:2.0];
        }
    }
    if (self.videoId > 0 && self.thumbId > 0)
    {
        [self addNewVideo];
    }
    else
    {
        [self weHaveAnError:nil];
    }

    return;
    
    if (self.videoId > 0 && self.thumbId > 0) {
        [self addNewVideo];
    } else {
        self.tryAdding++;
        if (self.tryAdding > 7) {
            [self weHaveAnError:nil];
        } else {
            [self performSelector:@selector(retryAddingItem) withObject:nil afterDelay:2.0];
        }
    }
}

//-(void)pauseAllWebActivities{
//    NSArray * allActivities = [[NSOperationQueue mainQueue]operations];
//    [allActivities enumerateObjectsUsingBlock:^(AFHTTPRequestOperation *obj, NSUInteger idx, BOOL *stop) {
//        [obj pause];
//    }];
//}
//
//-(void)resumeAllWebActivities{
//    NSArray * allActivities = [[NSOperationQueue mainQueue]operations];
//    [allActivities enumerateObjectsUsingBlock:^(AFHTTPRequestOperation *obj, NSUInteger idx, BOOL *stop) {
//        [obj resume];
//    }];
//}

- (void)addNewVideo {
    DDLogInfo(@"////////////////////////////>>>>>>>>>>>>>>>>>>>>  addNewVideo");
    NSString *extraData = @"";
    int itemType = 4;
    NSUInteger placeId = 0;

    if (self.place != nil) {
        placeId = self.place.serverID;
    }

//    [self pauseAllWebActivities];

    NSMutableSet<NSNumber*>* kids = [NSMutableSet set];
    for (KPLocalKid* i in self.selectedKids) {
        [kids addObject:@(i.serverID)];
    }
    
    [[ServerComm instance] addVideo:self.kid.serverID
                          withTitle:self.titleTF.text
                       selectedKids:kids
                            andDate:self.itemDate
                           andPlace:placeId
                        andCropRect:@""
                    andVideoAssetId:self.videoId
                    andThumpAssetId:self.thumbId
                   andAutoEnhanceOn:NO
                        andItemType:itemType
                       andExtraData:extraData
                   andShareWithFans:self.shareSwitch
                            andTags:self.tagIds
                        andDuration:self.duration
                   withSuccessBlock:^(id result) {


                       // DDLogInfo(@"%@",result);


//                       [self resumeAllWebActivities];
                       if ([[result valueForKey:@"status"] intValue] == 0) {

                           self.videoStat = DKVideoStatusAdded;
                           NSString *VID = [NSString stringWithFormat:@"%@", [[result valueForKey:@"result"] valueForKey:@"sdVideoAssetId"]];
                           self.sdVideoAssetId = VID;
                           self.myID = [NSString stringWithFormat:@"%@", [[result valueForKey:@"result"] valueForKey:@"id"]];

                           self.theResult = result;

                           if ([[result valueForKey:@"result"] objectForKey:@"short_url"]) {
                               self.short_url = [[result valueForKey:@"result"] objectForKey:@"short_url"];
                           }


                           [self itemVideoReadyOnServer];


                       }
                       else if ([[result valueForKey:@"status"] intValue] == 2) //no more space...
                       {
                           self.videoStat = DKVideoStatusAddedFail;
                           //                           [SVProgressHUD dismiss];
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [SVProgressHUD dismiss];
                               [GlobalUtils showNoSpace:self.viewController];
                           });
                       }
                       else {
                           self.videoStat = DKVideoStatusAddedFail;
                           [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error adding item")];
                       }


                   }

                       andFailBlock:^(NSError *error) {
                           self.videoStat = DKVideoStatusAddedFail;
                           [self weHaveAnError:error];
                           [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
                       }];
}


- (void)itemVideoReadyOnServer {


    [[SDImageCache sharedImageCache] storeImage:(UIImage *) [self.videoInfo valueForKey:@"thumbImage"] forKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, [[self.theResult valueForKey:@"result"] valueForKey:@"assetHash"]]];
    [[SDImageCache sharedImageCache] storeImage:(UIImage *) [self.videoInfo valueForKey:@"thumbImage"] forKey:[NSString stringWithFormat:@"%@%@_iphone.jpg", Defaults.assetsURLString, [[self.theResult valueForKey:@"result"] valueForKey:@"assetHash"]]];


    //save thumbnails
    UIImage *smallImage = [(UIImage *) [self.videoInfo valueForKey:@"thumbImage"] imageScaledToFitSize:CGSizeMake(192, 192)];

    [[SDImageCache sharedImageCache] storeImage:smallImage forKey:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, [[self.theResult valueForKey:@"result"] valueForKey:@"assetHash"]]];

    UIImage *mediumImage = [(UIImage *) [self.videoInfo valueForKey:@"thumbImage"] imageScaledToFitSize:CGSizeMake(600, 600)];
    [[SDImageCache sharedImageCache] storeImage:mediumImage forKey:[NSString stringWithFormat:@"%@%@_medium.jpg", Defaults.assetsURLString, [[self.theResult valueForKey:@"result"] valueForKey:@"assetHash"]]];


    //Add new Item to local database
    NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
    [itemData setValue:self.itemDate forKey:@"itemDate"];
    [itemData setValue:[GlobalUtils createCropRectStr:[self.videoInfo valueForKey:@"points"]] forKey:@"cropRect"];

    NSSet *selectedKids = [NSSet setWithArray:self.selectedKids];
    NSString *kidsIds = @"";
    for (KPLocalKid *kid in selectedKids.allObjects) {
        if ([kidsIds isEqualToString:@""]) {
            kidsIds = [kidsIds stringByAppendingFormat:@"%ld", (long) kid.serverID];
        } else {
            kidsIds = [kidsIds stringByAppendingFormat:@"|%ld", (long) kid.serverID];
        }
    }
    [itemData setValue:kidsIds forKey:@"kidsIds"];

    [itemData setValue:@(self.shareSwitch) forKey:@"shareStatus"];
    if (self.titleTF.text.length) {
        [itemData setValue:self.titleTF.text forKey:@"title"];
    }

    [itemData setValue:@(1) forKey:@"sat"];
    [itemData setValue:@(1) forKey:@"con"];
    [itemData setValue:@(0) forKey:@"bri"];
    [itemData setValue:@(0) forKey:@"rotation"];

    [itemData setValue:@(0) forKey:@"filterId"];
    [itemData setValue:@(0) forKey:@"autoEnhanceOn"];
    [itemData setValue:@(4) forKey:@"itemType"];



    // TODO: FIX Item upload
    self.item = [[KPDataParser instance] addNewItem:[self.theResult valueForKey:@"result"] andData:itemData];

    self.item.placeID = self.place.identifier;
    NSArray *itemTagIDs = [KPLocalItemTag fetchTagIDsForItem:self.item];
    
    
    // KPLocalTag *tag = [KPLocalTag fetchByServerID:tagId.intValue];
    
//    if (tag && ![itemTagIDs containsObject:tagId]) {

    
   // NSArray *itemTags = [KPLocalTag idsToObjects:itemTagIDs];

    for (NSNumber *tagId in self.tagIds) {
        KPLocalTag *tag = [KPLocalTag fetchByServerID:tagId.intValue];
        
        if (tag != nil) {
            
            if (![itemTagIDs containsObject:@(tag.identifier)]) {

                KPLocalItemTag *itemTagRelation = [KPLocalItemTag create];
                itemTagRelation.itemID = self.item.identifier;
                itemTagRelation.tagID = tag.identifier;
                [itemTagRelation saveChanges];
            }
            // [self.item addTagsObject:tag];
        }
    }

    [self.item saveChanges];
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

    /*[[MobileAppTracker sharedManager] trackActionForEventIdOrName:@"Add Keepy"
     eventIsId:NO];*/
    @try {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setValue:@(4) forKey:@"itemType"];
        NSString *order = @"Other keepy";
        NSUInteger count = [Item MR_countOfEntitiesWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]];
        if (count == 1) {
            order = @"1st keepy";
        }
        if (count == 2) {
            order = @"2nd keepy";
        }
        [params setValue:order forKey:@"order"];
        [FBSDKAppEvents logEvent:@"add keepy" parameters:params];

    } @catch (NSException *e) {}
    [self itemAddedSuccessfuly];

    self.myApiSuccess ? self.myApiSuccess(self.item) : (nil);

}

- (void)itemAddedSuccessfuly {


    //[[Mixpanel sharedInstance] track:@"video item save"];


    [[Analytics sharedAnalytics] track:@"item-add" properties:@{@"itemType" : @"video"}];

    [[Analytics sharedAnalytics] track:@"screen-close" properties:@{@"source" : @"add-story", @"result" : @"ok"}];

    [GlobalUtils updateAnalyticsUser];


    [SVProgressHUD dismiss];
    [GlobalUtils postNotificationForNewItem:self.item];
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:nil];
    //    [[NSNotificationCenter defaultCenter]postNotificationName:@"VideoUploaderDone" object:self userInfo:nil];
}


#pragma mark -
#pragma mark Preparing Assets

- (void)prepareAssetOriginalImage {

//    [self pauseAllWebActivities];


    self.videoStat = DKVideoStatusPrepareVideoAsset;
    self.igotIds = NO;

    DDLogInfo(@"////////////////////////////>>>>>>>>>>>>>>>>>>>>  prepareAssetOriginalImage ");

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(internetConnectionRevived:) name:INTERNET_CONNECTION_REVIVED_NOTIFICATION object:nil];

    if (self.serverComm == nil) {
        self.serverComm = [ServerComm instance];
    }

    // __block typeof (DKVideoUploaderManager *) weakSelf = self;
    __block DKVideoUploaderManager *weakSelf = self;

//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

    [[ServerComm instance] prepareAsset:^(id result) {
        DDLogInfo(@"////////////////////////////>>>>>>>>>>>>>>>>>>>>  prepareAssetOriginalImage %@", result);
        int assetId = 0;
        if ([[result valueForKey:@"result"] valueForKey:@"id"] != [NSNull null]) {
            assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] intValue];
            self.prepareAssetResult = result;
            
        }

        if (assetId > 0) {
            weakSelf.thumbId = assetId;
            [self preperVideoAssets];
        }
        else {
            [self weHaveAnError:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the thumbnail to keepy\'s servers. please try again soon.")];
            });
        }
    }                      andFailBlock:^(NSError *error) {
        [self weHaveAnError:error];
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"we can\'t establish connection to keepy\'s servers. are you connected to the internet?")];
        });
    }];
//    });
}

- (void)preperVideoAssets {
    DDLogInfo(@"////////////////////////////>>>>>>>>>>>>>>>>>>>>  preperVideoAssets ");
    __block typeof(DKVideoUploaderManager * ) weakSelf = self;

//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

    [[ServerComm instance] prepareVideoAsset:^(id result) {


        DDLogInfo(@"////////////////////////////>>>>>>>>>>>>>>>>>>>>  preperVideoAssets %@", result);
        int assetId = 0;

        if ([[result valueForKey:@"result"] valueForKey:@"id"] != [NSNull null]) {
            assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] intValue];
        }

        if (assetId > 0) {
            weakSelf.nresult = result;
            weakSelf.videoId = assetId;

            self.readyAssets ? self.readyAssets(YES) : (nil);
            self.igotIds = YES;
            self.videoStat = DKVideoStatusPrepareVideoAssetDone;
//                [self resumeAllWebActivities];
            [self uploadToServerOriginalImage];
        }
        else {
            self.videoStat = DKVideoStatusPrepareVideoAssetFail;
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"seems that there was a problem uploading the image to keepy\'s servers. please try again soon.")];
            });
        }

    }                           andFailBlock:^(NSError *error) {

        self.videoStat = DKVideoStatusPrepareVideoAssetFail;
        [self weHaveAnError:error];
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"we can\'t establish connection to keepy\'s servers. are you connected to the internet?")];
        });

    }];
//    });
}


- (void)uploadToServerOriginalImage {
    NSData *data = UIImageJPEGRepresentation(self.thumbnail, 0.8);
    NSString *hash = [[self.prepareAssetResult valueForKey:@"result"] valueForKey:@"hash"];
    NSString *policy = [[self.prepareAssetResult valueForKey:@"result"] valueForKey:@"policy"];
    NSString *policyHash = [[self.prepareAssetResult valueForKey:@"result"] valueForKey:@"policyHash"];
NSLog(@"FNTRACE uploadToServerOriginalImage");
    
    [self.serverComm uploadAsset:hash
                        withData:data
                       andPolicy:policy
                    andSignature:policyHash
                withSuccessBlock:^(id result) {

                    [self.serverComm commitAsset:self.thumbId
                                     andFileSize:data.length
                                withSuccessBlock:^(id result) {

                                    // DDLogInfo(@"%@",result);

                                    if ([result objectForKey:@"error"] == [NSNull null]) {
                                        [[Mixpanel sharedInstance] track:@"commit video asset" properties:@{@"assetId" : @(self.thumbId), @"videoSize" : [NSByteCountFormatter stringFromByteCount:data.length countStyle:NSByteCountFormatterCountStyleFile], @"status" : @"success"}];


                                    }


                                } andFailBlock:^(NSError *error) {
                                [self weHaveAnError:error];
                            }          onHandler:nil];

                } andFailBlock:^(NSError *error) {

                if(error){
                    [[Mixpanel sharedInstance] track:@"commit video asset" properties:@{@"assetId" : @(self.thumbId), @"videoSize" : [NSByteCountFormatter stringFromByteCount:data.length countStyle:NSByteCountFormatterCountStyleFile], @"status" : @"fail", @"error" : [error localizedDescription]}];
                    [self weHaveAnError:error];
                }


                if (error != nil && error.code != NSURLErrorCancelled) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading thumbnail image")];
                    });
                }
                //                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

            }         onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {

                //                     [GlobalUtils updateAssetUpload:assetId totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];

            }];

}


#pragma mark -
#pragma mark convertVideoToLowQualityWithInputURL = after tapping 'next'


- (void)convertVideoToLowQualityWithInputURL {


    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:self.myMovieFileURL options:nil];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *outputURL = paths[0];
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    outputURL = [outputURL stringByAppendingPathComponent:@"output.mp4"];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];


    self.exportProgressBarTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(updateExportDisplay) userInfo:nil repeats:YES];

    // Step 2
    // Create an export session with the composition and write the exported movie to the photo library
    self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPreset960x540];
    self.exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
    self.exportSession.outputFileType = AVFileTypeMPEG4;

    [self.exportSession exportAsynchronouslyWithCompletionHandler:^(void) {
        switch (self.exportSession.status) {
            case AVAssetExportSessionStatusCompleted: {

                self.videoStat = DKVideoStatusCompressed;
                [self.exportProgressBarTimer invalidate];
                self.exportProgressBarTimer = nil;
                [self uploadOriginalVideo];

            }
                break;
            case AVAssetExportSessionStatusFailed: {
                self.videoStat = DKVideoStatusCompressedFail;
                [self weHaveAnError:nil];
                [self.exportProgressBarTimer invalidate];
                self.exportProgressBarTimer = nil;
            }
                break;
            case AVAssetExportSessionStatusCancelled: {
                // > add resume option.
                [self.exportProgressBarTimer invalidate];
                self.exportProgressBarTimer = nil;
            }
                break;
            default:
                break;
        }
    }];
}

- (void)updateExportDisplay {


    [self updateAssetUploadTotalBytesWritten:self.exportSession.progress * 100 totalBytesExpectedToWrite:200];

}

- (void)uploadOriginalVideo {

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetConnectionRevived:)
                                                 name:INTERNET_CONNECTION_REVIVED_NOTIFICATION
                                               object:nil];


    if (self.serverComm == nil) {
        self.serverComm = [ServerComm instance];
    }


    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{


        //        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *outputURL = paths[0];
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
        outputURL = [outputURL stringByAppendingPathComponent:@"output.mp4"];

        NSData *data = [[NSFileManager defaultManager] contentsAtPath:outputURL];
        NSString *hash = [[self.nresult valueForKey:@"result"] valueForKey:@"hash"];
        NSString *policy = [[self.nresult valueForKey:@"result"] valueForKey:@"policy"];
        NSString *policyHash = [[self.nresult valueForKey:@"result"] valueForKey:@"policyHash"];

        DDLogInfo(@"%@", [NSByteCountFormatter stringFromByteCount:data.length countStyle:NSByteCountFormatterCountStyleFile]);


        [[Mixpanel sharedInstance] track:@"start upload video"];


        [self.serverComm uploadVideoAsset:hash
                                 withData:data
                                andPolicy:policy
                             andSignature:policyHash
                         withSuccessBlock:^(id result) {

                             self.videoStat = DKVideoStatusUploaded;

                             [self.serverComm commitAsset:self.videoId
                                              andFileSize:data.length
                                         withSuccessBlock:^(id result) {

                                             self.videoStat = DKVideoStatusUploadingCommited;

                                             DDLogInfo(@"Uploaded...");


                                             [[Mixpanel sharedInstance] track:@"upload video status" properties:@{@"assetId" : @(self.videoId), @"videoSize" : [NSByteCountFormatter stringFromByteCount:data.length countStyle:NSByteCountFormatterCountStyleFile], @"state" : @"success"}];


                                         } andFailBlock:^(NSError *error) {
                                         DDLogInfo(@"%@", error.description);

                                         if (error != nil) {
                                             self.videoStat = DKVideoStatusUploadingCommitedFail;
                                             [self weHaveAnError:error];
                                         }


                                     }


                                                onHandler:^{

                                                    if (!self.sdVideoAssetId) {


                                                    } else {
                                                        //                                 [self startToCheckVideoStatusAssetID:self.VID];
                                                    }
                                                    DDLogInfo(@"Uploaded...notSureNotSure");
                                                    DDLogInfo(@"Uploaded...notSureNotSure");


                                                }];

                             //             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

                         } andFailBlock:^(NSError *error) {




//             if (self.sdVideoAssetId && !self.killProcess) {
//                 if (![appDelegate.failedVideos containsObject:self.sdVideoAssetId]) {
//                     [self addItemToFaildVideoList:self.sdVideoAssetId];
//                 }
//             }

                    // DDLogInfo(@"%@",error.description);

                    NSMutableDictionary *properties = @{}.mutableCopy;
                    properties[@"assetId"] = @(self.videoId);
                    properties[@"videoSize"] = [NSByteCountFormatter stringFromByteCount:data.length countStyle:NSByteCountFormatterCountStyleFile];
                    properties[@"state"] = @"fail";
                    
                    if(error){
                        properties[@"error"] = [error localizedDescription];
                    }
                             
                    [[Mixpanel sharedInstance] track:@"upload video status" properties:properties];


                    self.videoStat = DKVideoStatusUploadingFail;
                    [self weHaveAnError:error];
                    [self updateAssetUploadTotalBytesWritten:0 totalBytesExpectedToWrite:-1];


                    if (error != nil && error.code != NSURLErrorCancelled) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading original image")];
                        });
                    }

                    //             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                }              onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                    self.videoStat = DKVideoStatusUploadingToServer;
                    [self updateAssetUploadTotalBytesWritten:totalBytesExpectedToWrite + totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite * 2];

                }               onHandler:^{

                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                    DDLogInfo(@"notSureNotSure");
                }];
    });
}

- (void)checkIfVideoReady {
    if (self.videoStat == DKVideoStatusUploadingFail) {return;}


    UIProgressView *progressView = (UIProgressView *) [self.uploadProgressView viewWithTag:111];
    [progressView removeFromSuperview];

    //[[self.uploadProgressView viewWithTag:1215]removeFromSuperview];

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(43, 4, 200, 24)];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
//    lbl.text = @"waiting for upload...";
    lbl.text = @"saving...";
    lbl.textColor = [UIColor blackColor];
    lbl.tag = 2222;
    [self.uploadProgressView addSubview:lbl];

}

- (void)startToCheckVideoStatusAssetID:(NSString *)assetId
{
    
    if (!assetId || self.item.itemType == 3) { return; }
    
    
    // __block typeof (DKVideoUploaderManager*) mySelf = self;
    // __block typeof (KPLocalItem *) myItem = self.item;
    
    __block DKVideoUploaderManager *mySelf = self;
    __block KPLocalItem *myItem = self.item;
    


    

    [[ServerComm instance] getVideoAssetStatus:assetId
                                  successBlock:^(id result) {

                                      
                                      // TODO: actually do stuff in here
                                      NSLog(@"received video asset status");
                                      
                                      // return;
                                      
                                      
                                      //assetStatus = 3

                                      DDLogInfo(@"%@", result);
                                      KPLocalAsset *itemOriginalImage = [KPLocalAsset fetchByLocalID:myItem.originalImageAssetID];


                                      if ([[result objectForKey:@"result"] objectForKey:@"assetStatus"] == [NSNull null] || [[[result objectForKey:@"result"] objectForKey:@"assetStatus"] integerValue] == -1) {
                                          DDLogInfo(@"This video was delete already");
                                          [[UploadTracker sharedInstance] removeItemToWatchList:[NSString stringWithFormat:@"%lu", (unsigned long)assetId]];
                                          [self failedProgressBar];
                                          return;
                                      }

                                      int a = [[[result objectForKey:@"result"] objectForKey:@"assetStatus"] intValue];
                                      DDLogInfo(@"a = %i", a);


                                      if (a == 2) {
                                          DDLogInfo(@"READY!!! (video upload manager)");
                                          
                                          [[UploadTracker sharedInstance] removeItemToWatchList:[NSString stringWithFormat:@"%lu", (unsigned long)assetId]];

                                          [self setIsValidItem:YES];
                                          itemOriginalImage.assetStatus = 2;
                                          KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];
                                          imageAsset.assetStatus = 2;
                                          [imageAsset saveChanges];
                                          [itemOriginalImage saveChanges];
                                          [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                                              if (success) {
                                                  DDLogInfo(@"SAVED");

                                                  [[Mixpanel sharedInstance] track:@"video processing" properties:@{@"assetId" : assetId, @"state" : @"success"}];
                                                  [[[Mixpanel sharedInstance] people] increment:@"Number of video" by:@(1)];
                                                  // [[[Mixpanel sharedInstance] people] increment:@"Number of keepys" by:@(1)];
                                                  [[Mixpanel sharedInstance] track:@"add video" properties:@{@"assetId" : assetId}];

                                              } else {
                                                  DDLogInfo(@"%@", error);
                                              }



                                              // [self SDvideoReadyOnServer];
                                              self.videoStat = DKVideoStatusSDReady;
                                              [[NSNotificationCenter defaultCenter] postNotificationName:@"VideoUploaderDone" object:self userInfo:@{@"itemID": @(myItem.identifier)}];
                                              
                                              // [[NSNotificationCenter defaultCenter] postNotificationName:@"VideoUploaderDone" object:@(myItem.identifier) userInfo:nil];
                                              
                                          }];
                                      } else {
                                          DDLogInfo(@"NOT READY!!!");
                                          //[self setIsValidItem:NO];
                                          //[self setKillProcess:NO];

                                          //IF the video is not sent to the server and try to check if is under prosesing

                                          //    DDLogInfo(@"%@ ==  %@",appDelegate.failedVideos,assetId);

//                                         if ([appDelegate.failedVideos containsObject:assetId]) {
//                                             [self setKillProcess:YES];
//                                             [self weHaveAnError:nil];
//                                         }


                                          [mySelf performSelector:@selector(startToCheckVideoStatusAssetID:) withObject:assetId afterDelay:5.0];
                                      }

                                  } failBlock:^(NSError *error) {
                [self weHaveAnError:error];
                [[Mixpanel sharedInstance] track:@"video processing" properties:@{@"assetId" : assetId, @"state" : @"fail", @"error" : [error localizedDescription]}];
                [mySelf performSelector:@selector(startToCheckVideoStatusAssetID:) withObject:assetId afterDelay:5.0];
            }];
}

- (void)uploadingProgress:(NSNotification*)notification {
    NSUInteger assetId = [notification.userInfo[@"assetId"] unsignedIntegerValue];
    KPLocalAsset* a = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];
    if (a.serverID != assetId) { return; }
    
    long long totalBytesWritten = [notification.userInfo[@"totalBytesWritten"] longLongValue];
    long long totalBytesExpectedToWrite = [notification.userInfo[@"totalBytesExpectedToWrite"] longLongValue];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIActivityIndicatorView * activityIndicator = (UIActivityIndicatorView *) [self.uploadProgressView viewWithTag:888];
        [activityIndicator startAnimating];
        
        self.videoStat = DKVideoStatusUploadingToServer;
        [self updateAssetUploadTotalBytesWritten:totalBytesExpectedToWrite + totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite * 2];
    });
}

- (void)cancelCurrentUpload:(NSNotification *)note {

    [self.exportSession cancelExport];
    [self updateAssetUploadTotalBytesWritten:0 totalBytesExpectedToWrite:-1];
    [self.serverComm cancelOperation];


    self.sendMe = NO;
    int itemId = self.myID.intValue;


    if (note) {
        [self.item remove];
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"VideoUploaderDone" object:self userInfo:nil];
        return;
    }

    [[ServerComm instance] deleteItem:itemId
                     withSuccessBlock:^(id result) {

                         if ([result[@"status"] integerValue] == 0) {
                             [[Mixpanel sharedInstance] track:@"video delete" properties:@{@"state" : @"success"}];
                             [[[Mixpanel sharedInstance] people] increment:@"Number of video" by:@(-1)];
                         }

                         [self.item remove];
                         [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"VideoUploaderDone" object:self userInfo:nil];

                     } andFailBlock:^(NSError *error) {
                [self cancelCurrentUpload:nil];
            }];



//      } andFailBlock:^(NSError *error) {
//          [self cancelCurrentUpload:nil];
//      }];

}

- (void)updateAssetUploadTotalBytesWritten:(long long)bytWRT totalBytesExpectedToWrite:(long long)bytTOW {

    DDLogInfo(@"%lld %lld:::%d", bytWRT, bytTOW, self.videoId);
    
    UIProgressView *progressView = (UIProgressView *) [self.uploadProgressView viewWithTag:111];
    if (bytTOW < 0) {
        [progressView removeFromSuperview];
        return;
    }
    UIActivityIndicatorView * activityIndicator = (UIActivityIndicatorView *) [self.uploadProgressView viewWithTag:888];
    [activityIndicator startAnimating];
    
    if (!progressView) {
        [self createUploadProgressView];
    }
    
    progressView.progress = ((float) bytWRT - self.startFromHere) / ((float) bytTOW - self.startFromHere);
    if (progressView.progress == 1 /*&& self.startFromHere > 0*/) {
        [self checkIfVideoReady];
        [self startToCheckVideoStatusAssetID:self.sdVideoAssetId];
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    } else {
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
    self.memSoFar = bytWRT;
    

}

- (void)createUploadProgressView {
    NSLog(@"createUploadProgressView");

    self.uploadProgressView = [[UIView alloc] initWithFrame:CGRectMake(10, 2, 300, 34)];
    self.uploadProgressView.backgroundColor = [UIColor whiteColor];


    //-------------- Cancel button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 1215;
    btn.frame = CGRectMake(265, 3, 28, 28);

    [btn setImage:[UIImage imageNamed:@"cancel-upload.png"] forState:UIControlStateNormal];

    [btn addTarget:self action:@selector(cancelVideoUpload:) forControlEvents:UIControlEventTouchUpInside];
    [self.uploadProgressView setNeedsDisplay];
    
    [self.uploadProgressView addSubview:btn];

    UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.frame = CGRectMake(self.uploadProgressView.frame.size.width - 60, 7, 20, 20);
    [activityIndicator startAnimating];
    activityIndicator.tag = 888;
    [self.uploadProgressView addSubview:activityIndicator];
    

//    //*
//    //-------------- Retry button
//    UIButton *retry = [UIButton buttonWithType:UIButtonTypeSystem];
//    [retry setTitle:@"RETRY" forState:UIControlStateNormal];
//    retry.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
//    retry.tag = 1315;
//    retry.frame = CGRectMake(170,2,50, 28);
//    [retry addTarget:self action:@selector(retryAddingItem) forControlEvents:UIControlEventTouchUpInside];
//    retry.hidden = YES;
//    [self.uploadProgressView addSubview:retry];
//     // */



    UIImageView *thumb = [[UIImageView alloc] initWithFrame:CGRectMake(5, 3, 28, 28)];

    //    thumb.backgroundColor = [UIColor grayColor];
    if (self.thumbnail) {
        thumb.image = self.thumbnail;
    } else {
        if (self.item) {
            KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];
            [thumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, itemImage.urlHash]] completed:nil];
        }
    }
    
    thumb.tag = 2221;
    [self.uploadProgressView addSubview:thumb];

    UIProgressView *progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(40, 17, 220, 3)];
    progressView.tag = 111;
    progressView.progressTintColor = [UIColor colorWithRed:238 / 256.f green:22 / 256.f blue:119 / 256.f alpha:1];
    [self.uploadProgressView addSubview:progressView];
    [self.uploadProgressView setNeedsDisplay];
//^^^^^^^^^
    [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:nil];

}


- (void)setOnlyProccessViewForItem:(KPLocalItem *)item {

    // TODO: actually do something in this function
    // return; 

    self.item = item;
    
    self.myID = @(item.serverID).stringValue;
    self.uploadProgressView = [[UIView alloc] initWithFrame:CGRectMake(10, 2, [UIScreen mainScreen].bounds.size.width - 20, 34)];
    self.uploadProgressView.backgroundColor = [UIColor whiteColor];

    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:item.imageAssetID];
    KPLocalAsset *itemOriginalImage = [KPLocalAsset fetchByLocalID:item.originalImageAssetID];

    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 1215;
    btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 48, 3, 28, 28);

    [btn setImage:[UIImage imageNamed:@"cancel-upload.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(cancelVideoUpload:) forControlEvents:UIControlEventTouchUpInside];
    [self.uploadProgressView addSubview:btn];
    
   
    UIImageView *thumb = [[UIImageView alloc] initWithFrame:CGRectMake(5, 3, 28, 28)];
    thumb.tag = 2221;
    [self.uploadProgressView addSubview:thumb];
    
    if (thumb.image) {
        thumb.image = self.thumbnail;
    } else {
        [thumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, itemImage.urlHash]] completed:nil];
    }

    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(43, 4, 200, 24)];
    lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
//    lbl.text = @"processing...";
    lbl.text = @"finishing up video processing...";
    lbl.textColor = [UIColor blackColor];
    lbl.tag = 2222;
    
    [self.uploadProgressView addSubview:lbl];
    
    UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] init];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
//    UIView * activityIndicator = [[UIView alloc] init];
    activityIndicator.frame = CGRectMake(self.uploadProgressView.frame.size.width - 60, 7, 20, 20);
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = NO;
    [activityIndicator setNeedsDisplay];
    activityIndicator.tag = 888;
    [self.uploadProgressView addSubview:activityIndicator];
    
    
    [self startToCheckVideoStatusAssetID:@(itemOriginalImage.serverID).stringValue];

}

- (void)cancelVideoUpload:(NSNotification *)note {


    if (self.uploadProgressView) {
        UILabel *lbl = (UILabel *) [self.uploadProgressView viewWithTag:2222];
        if ([lbl.text isEqualToString:@"ack! your upload failed"]) {
            [self setUserClickCancel:NO];
            NSNotification *notif = [NSNotification notificationWithName:@"status" object:@{@"status" : @YES}];
            [self cancelCurrentUpload:notif];
            return;
        }
    }

    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@""
                                                                message:NSLocalizedString(@"Are you super sure you want to cancel?", comment: @"")
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil]];
    [ac addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[Mixpanel sharedInstance] track:@"upload video status" properties:@{@"assetId" : self.myID, @"state" : @"stop by user"}];
        [self setUserClickCancel:YES];
        [self cancelCurrentUpload:nil];
    }]];
    [ac presentWithAnimated:YES completion:nil];
}

//- (void)updateUploadProgressTotalBytesWritten:(long long)bytWRT totalBytesExpectedToWrite:(long long)bytTOW {
//    DDLogInfo(@"%lld %lld:::%d", bytWRT, bytTOW, self.videoId);
//
//    UIProgressView *progressView = (UIProgressView *) [self.uploadProgressView viewWithTag:111];
//    progressView.progress = ((float) bytWRT - self.startFromHere) / ((float) bytTOW - self.startFromHere);
//    if (progressView.progress == 1 && self.startFromHere > 0) {
//        [self checkIfVideoReady];
//        [self startToCheckVideoStatusAssetID:self.sdVideoAssetId];
//    }
//    self.memSoFar = bytWRT;
//
//}
//
//
- (void)zeroOutTheProgressBar {

    //Fix in 1.95.3
    if (self.videoStat == DKVideoStatusUploadingCommited || self.videoStat == DKVideoStatusAdded || self.videoStat == DKVideoStatusUploadingFail) {
        UIProgressView *progressView = (UIProgressView *) [self.uploadProgressView viewWithTag:111];
        dispatch_async(dispatch_get_main_queue(), ^{
            progressView.progress = 0;
        });
        self.startFromHere = 0;
        [self tenMinutesLater];

    } else {
        self.startFromHere = self.memSoFar;
    }

}

- (void)tenMinutesLater {
    UIProgressView *progressView = (UIProgressView *) [self.uploadProgressView viewWithTag:111];
    progressView.progress = self.startFromHere;
    if (self.startFromHere < 1) {
        self.startFromHere = self.startFromHere + 0.05;
        [NSTimer scheduledTimerWithTimeInterval:(0.1)
                                         target:self
                                       selector:@selector(tenMinutesLater)
                                       userInfo:nil
                                        repeats:NO];
    } else {

        if (self.videoStat == DKVideoStatusUploadingFail) {
            [self failedProgressBar];

        }

        [self checkIfVideoReady];
        [self startToCheckVideoStatusAssetID:self.sdVideoAssetId];

    }
}

- (void)internetConnectionRevived:(NSNotification *)notification {
    //    if (self.isUploadingOriginalImage)
    //        [self uploadOriginalImage];
}


@end
