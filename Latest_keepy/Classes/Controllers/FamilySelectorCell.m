//
//  FamilySelectorCell.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "FamilySelectorCell.h"

@interface FamilySelectorCell ()

@property(nonatomic, weak) IBOutlet UIButton *leftButton;
@property(nonatomic, weak) IBOutlet UILabel *leftLabel;
@property(nonatomic, weak) IBOutlet UIButton *rightButton;
@property(nonatomic, weak) IBOutlet UILabel *rightLabel;
@property(nonatomic, weak) IBOutlet UIView *actionsView;
@property(nonatomic, assign) CGRect originalActionsViewFrame;

@end

@implementation FamilySelectorCell

#define DEFAULT_TITLE_WIDTH 215.0f
#define DEFAULT_TITLE_HEIGHT 21.0f
#define CELL_HEIGHT 150.0f

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    self.rightLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:12];
    self.originalActionsViewFrame = self.actionsView.frame;
}

- (void)layout {
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    switch (self.familySelectorCellType) {
        default:
        case FamilySelectorCellTypeSiblings: {
            [self.leftButton setImage:[UIImage imageNamed:@"blue-sister"] forState:UIControlStateNormal];
            self.leftLabel.text = @"add sister";
            [self.rightButton setImage:[UIImage imageNamed:@"blue-brother"] forState:UIControlStateNormal];
            self.rightLabel.text = @"add brother";
            break;
        }

        case FamilySelectorCellTypeParents: {
            [self.leftButton setImage:[UIImage imageNamed:@"blue-mom"] forState:UIControlStateNormal];
            self.leftLabel.text = @"add mom";
            [self.rightButton setImage:[UIImage imageNamed:@"blue-dad"] forState:UIControlStateNormal];
            self.rightLabel.text = @"add dad";
            break;
        }

        case FamilySelectorCellTypeGrandParents: {
            [self.leftButton setImage:[UIImage imageNamed:@"blue-grandma"] forState:UIControlStateNormal];
            self.leftLabel.text = @"add grandma";
            [self.rightButton setImage:[UIImage imageNamed:@"blue-grandpa"] forState:UIControlStateNormal];
            self.rightLabel.text = @"add grandpa";
            break;
        }

        case FamilySelectorCellTypeRelatives: {
            self.leftLabel.text = @"add aunt";
            [self.leftButton setImage:[UIImage imageNamed:@"blue-aunt"] forState:UIControlStateNormal];
            self.rightLabel.text = @"add uncle";
            [self.rightButton setImage:[UIImage imageNamed:@"blue-uncle"] forState:UIControlStateNormal];
            break;
        }
    }

    self.title.attributedText = [FamilySelectorCell attribuedMessage:self.title.text];
    CGFloat titleHeight = [FamilySelectorCell titleHeight:self.title.text];
    CGFloat height = [FamilySelectorCell height:self.title.text];
    CGFloat delta = fabs(titleHeight - DEFAULT_TITLE_HEIGHT);
    self.title.frame = CGRectMake(self.title.frame.origin.x, self.title.frame.origin.y, self.title.frame.size.width, titleHeight);
    self.actionsView.frame = CGRectMake(self.actionsView.frame.origin.x, self.originalActionsViewFrame.origin.y + delta, self.actionsView.frame.size.width, self.actionsView.frame.size.height);
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, height);
}

+ (CGFloat)titleHeight:(NSString *)title {
    CGRect rect = [[FamilySelectorCell attribuedMessage:title] boundingRectWithSize:CGSizeMake(DEFAULT_TITLE_WIDTH, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return fmaxf(DEFAULT_TITLE_HEIGHT, ceilf(rect.size.height));
}

+ (CGFloat)height:(NSString *)title {
    return (CELL_HEIGHT - DEFAULT_TITLE_HEIGHT) + [FamilySelectorCell titleHeight:title];
}

+ (NSMutableAttributedString *)attribuedMessage:(NSString *)title {
    NSMutableAttributedString *titleAttributedString = [[NSMutableAttributedString alloc] initWithString:title];
    [titleAttributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Light" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:(87.0f / 255.0f) green:(66.0f / 255.0f) blue:(63.0f / 255.0f) alpha:1.0f]} range:NSMakeRange(0, title.length)];
    return titleAttributedString;
}

- (IBAction)leftSelected:(id)sender {
    [self.delegate leftSelected:sender];
}

- (IBAction)rightSelected:(id)sender {
    [self.delegate rightSelected:sender];
}

@end
