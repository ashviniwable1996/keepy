//
//  AddRelativesViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol AddRelativesViewControllerDelegate;

@interface AddRelativesViewController : UIViewController

@property(nonatomic, weak) id <AddRelativesViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *kids;
@property(nonatomic, strong) NSArray *family;

- (CGFloat)height;

@end

@protocol AddRelativesViewControllerDelegate <NSObject>

- (void)doneAddingRelatives:(NSArray *)family;

@end
