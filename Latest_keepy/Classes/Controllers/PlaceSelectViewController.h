//
//  PlaceSelectViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/25/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"

@protocol PlaceSelectDelegate

- (void)placeSelected:(Place *)place;

@end


@interface PlaceSelectViewController : UIViewController

@property(nonatomic, assign) id <PlaceSelectDelegate> delegate;

@end
