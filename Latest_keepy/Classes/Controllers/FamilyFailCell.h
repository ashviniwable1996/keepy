//
//  FamilyFailCell.h
//  Keepy
//
//  Created by Troy Payne on 2/17/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface FamilyFailCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *title;

+ (CGFloat)height:(NSString *)title;

- (void)layout;

@end
