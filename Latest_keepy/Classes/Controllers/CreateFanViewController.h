//
//  CreateFanViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/20/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface CreateFanViewController : UIViewController

@property(nonatomic, strong) NSDictionary *facebookData;
@property(nonatomic, strong) NSDictionary *fan;
@property(nonatomic, copy) NSString *facebookAccessToken;
@property(nonatomic, copy) NSString *email;

@end
