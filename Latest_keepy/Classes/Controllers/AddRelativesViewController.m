//
//  AddRelativesViewController.m
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "AddRelativesViewController.h"
#import "AddRelativeViewController.h"
#import "KidsCell.h"
#import "FamilyCell.h"
#import "FamilySelectorCell.h"
#import "ProgressCell.h"

@interface AddRelativesViewController () <UITableViewDataSource, UITableViewDelegate, FamilySelectorCellDelegate, ProgressCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation AddRelativesViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    [[Mixpanel sharedInstance] track:@"onboarding-add-relatives-open"];


    [self.tableView registerNib:[UINib nibWithNibName:@"KidsCell" bundle:nil] forCellReuseIdentifier:@"KidsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilyCell" bundle:nil] forCellReuseIdentifier:@"FamilyCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FamilySelectorCell" bundle:nil] forCellReuseIdentifier:@"FamilySelectorCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProgressCell" bundle:nil] forCellReuseIdentifier:@"ProgressCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        static NSString *KidsCellIdentifier = @"KidsCell";
        KidsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KidsCellIdentifier];
        cell.kids = self.kids;
        return cell;
    } else if (indexPath.row == 1) {
        static NSString *FamilyCellIdentifier = @"FamilyCell";
        FamilyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FamilyCellIdentifier];
        cell.family = self.family;
        return cell;
    } else if (indexPath.row == 2) {
        static NSString *FamilySelectorCellIdentifier = @"FamilySelectorCell";
        FamilySelectorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FamilySelectorCellIdentifier];
        cell.delegate = self;
        cell.familySelectorCellType = FamilySelectorCellTypeRelatives;
        cell.title.text = [self title];
        [cell layout];
        return cell;
    } else if (indexPath.row == 3) {
        static NSString *ProgressCellIdentifier = @"ProgressCell";
        ProgressCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ProgressCellIdentifier];
        cell.delegate = self;
        cell.progressCellState = ProgressCellStateThree;
        cell.bold = YES;
        [cell.skipButton setTitle:@"done" forState:UIControlStateNormal];
        [cell layout];
        return cell;
    }
    return nil;
}

- (NSString *)title {
    BOOL foundRelative = NO;
    for (NSDictionary *familyMember in self.family) {
        int relationshipTypeId = [[familyMember objectForKey:@"fanRelationType"] intValue];
        if (relationshipTypeId == AddRelativeViewControllerTypeAunt || relationshipTypeId == AddRelativeViewControllerTypeUncle) {
            foundRelative = YES;
            break;
        }
    }
    if (foundRelative) {
        return @"Any more aunts & uncles?";
    } else {
        return @"Share your memories with aunts & uncles";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [KidsCell height];
    } else if (indexPath.row == 1) {
        return [FamilyCell height];
    } else if (indexPath.row == 2) {
        return [FamilySelectorCell height:[self title]];
    } else if (indexPath.row == 3) {
        return [ProgressCell height];
    }
    return 0.0f;
}

- (CGFloat)height {
    return [KidsCell height] + [FamilyCell height] + [FamilySelectorCell height:[self title]] + [ProgressCell height];
}

- (IBAction)addRelativesExit:(UIStoryboardSegue *)segue {
    AddRelativeViewController *addRelativeViewController = (AddRelativeViewController *) segue.sourceViewController;
    if (addRelativeViewController.family) {
        self.family = addRelativeViewController.family;
        [self.tableView reloadData];
    }
}

- (void)leftSelected:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-relatives-add-aunt-tap"];

    [self performSegueWithIdentifier:@"AddAunt" sender:self];
}

- (void)rightSelected:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-relatives-add-uncle-tap"];

    [self performSegueWithIdentifier:@"AddUncle" sender:self];
}

- (void)skip:(id)sender {

    [[Mixpanel sharedInstance] track:@"onboarding-add-relatives-skip-tap"];

    [self.delegate doneAddingRelatives:self.family];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddAunt"]) {
        AddRelativeViewController *addRelativeViewController = (AddRelativeViewController *) segue.destinationViewController;
        addRelativeViewController.addRelativeViewControllerType = AddRelativeViewControllerTypeAunt;
    } else if ([segue.identifier isEqualToString:@"AddUncle"]) {
        AddRelativeViewController *addRelativeViewController = (AddRelativeViewController *) segue.destinationViewController;
        addRelativeViewController.addRelativeViewControllerType = AddRelativeViewControllerTypeUncle;
    }
}

@end
