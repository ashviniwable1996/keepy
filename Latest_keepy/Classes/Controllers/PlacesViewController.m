//
//  PlacesViewController.m
//  Keepy
//
//  Created by Troy Payne on 4/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import "PlacesViewController.h"
#import "KPLocalPlace.h"

@interface PlacesViewController () <UITableViewDataSource, UITableViewDelegate, AddPlaceViewCellDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation PlacesViewController

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.places.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    if (indexPath.row == 0) {
        static NSString *AddPlaceViewCellIdentifier = @"AddPlaceViewCell";
        AddPlaceViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:AddPlaceViewCellIdentifier];
        cell.delegate = self;
        return cell;
    }

    if (indexPath.row <= (self.places.count) && indexPath.row != 0) {
        static NSString *PlaceViewCellIdentifier = @"PlaceViewCell";
        PlaceViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:PlaceViewCellIdentifier];
        cell.place = [self.places objectAtIndex:indexPath.row - 1];
        return cell;
    }

    else {

        static NSString *AddPlaceViewCellIdentifier = @"AddPlaceViewCell";
        AddPlaceViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:AddPlaceViewCellIdentifier];
        cell.delegate = self;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 0) {
        return [PlaceViewCell height];
    } else {
        return [AddPlaceViewCell height];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


    // DDLogInfo(@"AMount Of places = %d\nClick on row %d",[self.places count],indexPath.row-1);

    // DDLogInfo(@"%@",[self.places objectAtIndex:indexPath.row-1]);
    
    if(indexPath.row == 0){
        [self addPlace];
    }
    else{
        self.selectedPlace = [self.places objectAtIndex:indexPath.row - 1];
        [self performSegueWithIdentifier:@"exitPlaces" sender:self];
    }
}

- (void)addPlace {
    self.selectedPlace = nil;
    [self performSegueWithIdentifier:@"exitPlacesThenAdd" sender:self];
}

@end


@interface PlaceViewCell ()

@property(nonatomic, weak) IBOutlet UILabel *placeTitle;

@end

//The places list cell
@implementation PlaceViewCell

- (void)setPlace:(KPLocalPlace *)place {
    if (_place != place) {
        _place = place;
        [self setNeedsLayout];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.placeTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f];
    self.placeTitle.text = self.place.title;
}

+ (CGFloat)height {
    return 43.0f;
}

@end

@interface AddPlaceViewCell ()

@property(nonatomic, weak) IBOutlet UILabel *addPlaceTitle;

@end

//The title "add place +
@implementation AddPlaceViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.addPlaceTitle.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f];
}

+ (CGFloat)height {
    return 43.0f;
}

- (IBAction)addPlace:(id)sender {
    [self.delegate addPlace];
}

@end