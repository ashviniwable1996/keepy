//
//  ItemEditViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"
#import "KPPopViewController.h"

@protocol ItemEditDelegate

- (void)itemEditFinished;

@optional
- (void)itemEditCancelled;

@end

@interface ItemEditViewController : KPPopViewController

@property(nonatomic, assign) id <ItemEditDelegate> delegate;

@property (nonatomic) BOOL isNewKid;

- (instancetype)initWithItem:(Item *)item andMode:(NSInteger)mode;

- (instancetype)initWithImageInfo:(NSDictionary *)imageInfo;

- (instancetype)initWithImagesInfos:(NSArray *)imagesInfos;

- (void)refreshImageInfo:(NSDictionary *)imageInfo;

- (void)refreshVideoInfo:(NSDictionary *)videoInfo;

- (instancetype)initWithVideoInfo:(NSDictionary *)videoInfo;

@end
