//
//  FanCell.h
//  Keepy
//
//  Created by Troy Payne on 2/20/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

typedef enum {
    FanCellTypeMom,
    FanCellTypeDad,
    FanCellTypeGrandma,
    FanCellTypeGrandpa,
    FanCellTypeFamily,
    FanCellTypeFriend,
    FanCellTypeAunt,
    FanCellTypeUncle
} FanCellType;

@interface FanCell : UITableViewCell

@property(nonatomic, assign) FanCellType fanCellType;
@property(nonatomic, strong) NSDictionary *fan;

+ (CGFloat)height;

- (void)layout;

@end
