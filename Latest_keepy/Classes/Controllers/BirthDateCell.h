//
//  BirthDateCell.h
//  Keepy
//
//  Created by Troy Payne on 2/14/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol BirthDateCellDelegate;

@interface BirthDateCell : UITableViewCell

@property(nonatomic, weak) id <BirthDateCellDelegate> delegate;

+ (CGFloat)height;

@end

@protocol BirthDateCellDelegate <NSObject>

- (void)birthDate;

@end