//
//  DKPlaybackPosition.h

//
//  Created by Daniel Karsh on 4/16/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DKPlaybackPosition : UIView

@property(nonatomic, strong) IBOutlet UISlider *slider;
@property(nonatomic, strong) IBOutlet UIProgressView *loading;
@property(nonatomic, strong) IBOutlet UILabel *currentTime;
@property(nonatomic, strong) IBOutlet UILabel *remainingTime;
@end
