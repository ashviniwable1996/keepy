//
//  FamilyFailViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/18/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@protocol FamilyFailViewControllerDelegate;

@interface FamilyFailViewController : UIViewController

@property(nonatomic, weak) id <FamilyFailViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *kids;
@property(nonatomic, strong) NSArray *family;

//- (CGFloat)height;
//
@end

@protocol FamilyFailViewControllerDelegate <NSObject>

- (void)doneAddingFamily;

@end
