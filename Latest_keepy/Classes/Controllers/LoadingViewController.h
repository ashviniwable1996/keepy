//
//  LoadingViewController.h
//  Keepy
//
//  Created by Troy Payne on 2/11/14.
//  Copyright (c) 2014 keepy Inc. All rights reserved.
//

@interface LoadingViewController : UIViewController

@end

@interface KPLoadingNavigationController : UINavigationController

@end

@interface SegueLoading : UIStoryboardSegue

@end