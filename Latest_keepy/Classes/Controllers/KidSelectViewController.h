//
//  KidSelectViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/19/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KidSelectDelegate

- (void)kidSelected:(Kid *)kid;

@end

@interface KidSelectViewController : UIViewController

@property(nonatomic, assign) id <KidSelectDelegate> delegate;

@end
