//
//  DevConfViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/14/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class DevConfViewController: UITableViewController {
    struct Item {
        var title: String
        var detail: String
        var name: String
    }
    
    var items = [[Item]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadItems()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        loadItems()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "API Endpoint"
        default:
            return nil
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RightDetail", for: indexPath)
        
        let item = items[indexPath.section][indexPath.row]

        cell.textLabel!.text = item.title
        cell.detailTextLabel!.text = item.detail

        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DevConfTextFieldViewController {
            
            if let indexPath = tableView.indexPathForSelectedRow {
                let item = items[indexPath.section][indexPath.row]
                vc.title = self.tableView(tableView, titleForHeaderInSection: indexPath.section)
                vc.headerText = item.title
                vc.defaultName = item.name
                vc.defaultText = item.detail
            }
        }
    }
    @IBAction func onDonePressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadItems() {
        items = [[Item]]()
        
        let item0 = Item(title: "Keepy", detail: Defaults.apiEndpoint, name: Defaults.APIEndpointKey)
        let item1 = Item(title: "Amazon", detail: Defaults.s3URLString, name: Defaults.S3URLStringKey)
        let item2 = Item(title: "CloudFront", detail: Defaults.assetsURLString, name: Defaults.AssetsURLStringKey)
        items.append([item0, item1, item2])
        
        tableView.reloadData()
    }
}
