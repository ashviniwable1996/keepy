//
//  DevConfTextFieldViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/14/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class DevConfTextFieldViewController: UITableViewController {
    @IBOutlet weak var textField: UITextField!

    var defaultName: String!
    var defaultText = ""
    var headerText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.text = defaultText
        textField.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let updatedText = textField.text ?? ""
        if defaultText != updatedText {
            NSLog("changed -> \(updatedText)")
            if updatedText.lengthOfBytes(using: String.Encoding.utf8) == 0 {
                Defaults.singleton.removeObject(forKey: defaultName)
            } else {
                Defaults.singleton.set(updatedText, forKey: defaultName)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerText
    }
}
