//
// Created by Antonio Bello on 3/30/16.
// Copyright (c) 2016 Keepy. All rights reserved.
//

import SDWebImage

private enum ImageFormat: String {
	case Jpeg = "jpg"
}

enum ImageSize : String {
	case Small = "small"
	case Medium = "medium"
	case Original = "original"

	func compose(name: String, suffix: String) -> String {
		switch self {
		case .Original:
			return "\(name).\(suffix)"
		case .Small: fallthrough
		case .Medium:
			return "\(name)_\(self.rawValue).\(suffix)"
		}
	}
}

final class ImageProvider {
	static let instance = ImageProvider(imageCache: SDImageCache.shared(), webImageManager: SDWebImageManager.shared())

	fileprivate let imageCache: SDImageCache
	fileprivate let webImageManager: SDWebImageManager

	fileprivate init(imageCache: SDImageCache, webImageManager: SDWebImageManager) {
		self.imageCache = imageCache
		self.webImageManager = webImageManager
	}
}

extension ImageProvider {
	func loadImage(localItem: KPLocalItem, imageSize: ImageSize, completion: @escaping (UIImage?) -> Void) -> SDWebImageOperation? {
		let originalItemImageKey: String?
		let itemImageKey : String?

		if let originalItemImage = KPLocalAsset.fetch(byLocalID: UInt(localItem.originalImageAssetID)) {
			originalItemImageKey = Defaults.assetsURLString + imageSize.compose(name: originalItemImage.urlHash(), suffix: ImageFormat.Jpeg.rawValue)
		} else {
			originalItemImageKey = .none
		}

		if let itemImage = KPLocalAsset.fetch(byLocalID: UInt(localItem.imageAssetID)) {
			itemImageKey = Defaults.assetsURLString + imageSize.compose(name: itemImage.urlHash(), suffix: ImageFormat.Jpeg.rawValue)
		} else {
			itemImageKey = .none
		}

		let key: String?
		if let itemImageKey = itemImageKey, self.imageCache.diskImageExists(withKey: itemImageKey) {
			key = itemImageKey;
		} else if let originalItemImageKey = originalItemImageKey, self.imageCache.diskImageExists(withKey: originalItemImageKey) {
			key = originalItemImageKey;
		} else {
			key = itemImageKey;
		}

		if let key = key {
			let url = URL(string:key)!
            print(url)
			return self.webImageManager.downloadImage(with: url, options: [.retryFailed], progress: nil) { (image, error, cacheType, finished, imageURL) in
				DispatchQueue.main.async {
					completion(image)
				}
			}
		} else {
			completion(.none)
			return .none
		}
	}
    
    func getOriginalImageURL(localItem: KPLocalItem, imageSize: ImageSize) -> String? {
        let originalItemImageKey: String?
        let itemImageKey : String?
        
        if let originalItemImage = KPLocalAsset.fetch(byLocalID: UInt(localItem.originalImageAssetID)) {
            originalItemImageKey = Defaults.assetsURLString + imageSize.compose(name: originalItemImage.urlHash(), suffix: ImageFormat.Jpeg.rawValue)
        } else {
            originalItemImageKey = .none
        }
        
        if let itemImage = KPLocalAsset.fetch(byLocalID: UInt(localItem.imageAssetID)) {
            itemImageKey = Defaults.assetsURLString + imageSize.compose(name: itemImage.urlHash(), suffix: ImageFormat.Jpeg.rawValue)
        } else {
            itemImageKey = .none
        }
        
        let key: String?
        if let itemImageKey = itemImageKey, self.imageCache.diskImageExists(withKey: itemImageKey) {
            key = itemImageKey;
        } else if let originalItemImageKey = originalItemImageKey, self.imageCache.diskImageExists(withKey: originalItemImageKey) {
            key = originalItemImageKey;
        } else {
            key = itemImageKey;
        }
        
        if let key = key {
            let url = URL(string:key)!
            return key
            print(url)
        } else {
            
            return ""
        }
    }
    
}
