//
// Created by Antonio Bello on 3/30/16.
// Copyright (c) 2016 Keepy. All rights reserved.
//

import Foundation
import SDWebImage

@objc class KPImageProviderWrapper: NSObject {
	static let ImageSizeSmall: NSString = "small"
	static let ImageSizeMedium: NSString = "medium"
	static let ImageSizeOriginal: NSString = "original"

	static let instance = KPImageProviderWrapper(imageProvider: ImageProvider.instance)

	fileprivate let imageProvider: ImageProvider
	fileprivate init(imageProvider: ImageProvider) {
		self.imageProvider = imageProvider
	}
}

extension KPImageProviderWrapper {
	func loadImage(localItem: KPLocalItem, imageSize: String, completion: @escaping (UIImage?) -> Void) -> SDWebImageOperation? {
		let imageSize = ImageSize(rawValue: imageSize)!
		return imageProvider.loadImage(localItem: localItem, imageSize: imageSize, completion: completion)
	}
    func getOriginalImageURL(localItem: KPLocalItem, imageSize: String) -> String? {
        let imageSize = ImageSize(rawValue: imageSize)!
        return imageProvider.getOriginalImageURL(localItem: localItem, imageSize: imageSize)
    }
}
