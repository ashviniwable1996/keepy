//
//  CachingAssistant.h
//  Keepy
//
//  Created by Arik Sosman on 6/9/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CachingAssistant : NSObject

+ (NSString *)cacheLocationForExternalURL:(NSURL *)url;

@end
