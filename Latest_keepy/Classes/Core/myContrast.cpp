#include "QuadLib.h"

cv::Mat myDDS(cv::Mat inp, double k) {

    //cv::Mat out(inp.rows,  inp.cols, CV_8UC3, cv::Scalar(0, 0, 0, 0));
    cv::Mat out = inp.clone();
    int x, y;
    for (y = 0; y < inp.rows; y++) {
        byte *iptr = inp.ptr<uchar>(y);
        byte *optr = out.ptr<uchar>(y);

        for (x = 0; x < inp.cols; x++, iptr += 3, optr += 3) {
            int b = iptr[0];
            int g = iptr[1];
            int r = iptr[2];
            // a is min of 3
            int a = (b < g ? b : g);
            if (r < a) {
                a = r;
            }
            a = int(k * a + 0.5);
            optr[0] = b - a;
            optr[1] = g - a;
            optr[2] = r - a;
        }
    }
    return (out);
}

#define fix(x, n)      (int)((x)*(1 << (n)) + 0.5)
#define descale       CV_DESCALE

#define cscGr_32f  0.299f
#define cscGg_32f  0.587f
#define cscGb_32f  0.114f

/* BGR/RGB -> Gray */
#define csc_shift  14
#define cscGr  fix(cscGr_32f,csc_shift)
#define cscGg  fix(cscGg_32f,csc_shift)
#define cscGb  fix(cscGb_32f,csc_shift)

static int gLow, gHgh;

void myContrastInit(cv::Mat clr, double *gmax) {
    double pLow = 1.0;
    double pHgh = 1.0;

#define ISTEP 4

    int j, H[256];
    for (j = 0; j < 256; j++) {
        H[j] = 0;
    }
    int x, y;
    for (y = 0; y < clr.rows; y += ISTEP) {
        byte *iptr = clr.ptr<uchar>(y);
        for (x = 0; x < clr.cols; x += ISTEP, iptr += 3 * ISTEP) {
            int b = iptr[0];
            int g = iptr[1];
            int r = iptr[2];
            int v = cscGb * b + cscGg * g + cscGr * r;
            ++H[v >> csc_shift];
        }
    }
    // Total number of pixels
    int num = 0;
    for (j = 0; j < 256; j++) {
        num += H[j];
    }

    int nLow, nHgh;

    // Convert low and high percentiles into pixel counts
    nLow = cvRound(pLow * num / 100.0);
    nHgh = cvRound(pHgh * num / 100.0);

    // Find the low and high cutoff levels
    for (j = 0, num = 0; j < 256; j++) {
        num += H[j];
        if (num > nLow) {
            break;
        }
    }
    gLow = j;
    for (j = 255, num = 0; j >= 0; j--) {
        num += H[j];
        if (num > nHgh) {
            break;
        }
    }
    gHgh = j;

    *gmax = 255.0 / (float) (gHgh - gLow);
}

cv::Mat myContrastStretch(cv::Mat clr, double gain) {
    cv::Mat out(clr.rows, clr.cols, CV_8UC3, cv::Scalar(0));
    byte iLUT[256];

    int FromZero = 0;
    int j;

    if (FromZero) {
        for (j = 0; j <= gLow; j++) {
            iLUT[j] = 0;
        }
        for (j = gLow + 1; j < gHgh; j++) {
            iLUT[j] = cvRound((j - gLow) * gain);
        }
        for (j = gHgh; j < 256; j++) {
            iLUT[j] = iLUT[gHgh - 1];
        }
    }
    else {
        for (j = 255; j >= gHgh; j--) {
            iLUT[j] = 255;
        }
        for (j = gHgh - 1; j >= gLow; j--) {
            iLUT[j] = cvRound(255.0 + (j - gHgh) * gain);
        }
        for (j = gLow - 1; j >= 0; j--) {
            iLUT[j] = iLUT[gLow];
        }
    }

    //for(j = 0; j < 256; j++) {
    //	printf("iLUT[%3d] = %3d\n", j, iLUT[j]);
    //}

    for (int y = 0; y < clr.rows; y++) {
        byte *iptr = clr.ptr<uchar>(y);
        byte *optr = out.ptr<uchar>(y);
        for (int x = 0; x < clr.cols; x++, iptr += 3, optr += 3) {
            optr[0] = iLUT[iptr[0]];
            optr[1] = iLUT[iptr[1]];
            optr[2] = iLUT[iptr[2]];
        }
    }
    return (out);
}
