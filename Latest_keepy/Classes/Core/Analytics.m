//
//  Analytics.m
//  Keepy
//
//  Created by Yaniv Solnik on 8/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

@implementation Analytics

+ (instancetype)sharedAnalytics {
    /*
    static dispatch_once_t pred;
    static Analytics *_sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject startMixpanel];
    });
    return _sharedObject;*/

    return nil;
}

- (void)startMixpanel {
    [Mixpanel sharedInstanceWithToken:@"f22ff22eee1d8bcd75c9d7a664bb1c95"];
}

- (void)track:(NSString *)eventId {
    [self track:eventId properties:nil];
}

- (void)track:(NSString *)eventId properties:(NSDictionary *)properties {
    NSMutableDictionary *p = [[NSMutableDictionary alloc] init];
    if (properties != nil) {
        [p addEntriesFromDictionary:properties];
    }

    if (self.defaultProperties != nil) {
        [p addEntriesFromDictionary:self.defaultProperties];
    }

    [[Mixpanel sharedInstance] track:eventId properties:p];
}

@end
