//
//  AgeCalculator.m
//  Keepy
//
//  Created by Arik Sosman on 4/21/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "AgeCalculator.h"

@implementation AgeCalculator

+ (NSInteger)calculateAgeForBirthday:(NSDate *)birthday {

    NSDate *today = [NSDate date];

    return [self calculateAgeForBirthday:birthday atDate:today];

}

+ (NSInteger)calculateAgeForBirthday:(NSDate *)birthday atDate:(NSDate *)date {

    NSDate *newBirthdayDate = [self removeHourAndMinutes:birthday];
    NSDate *newPresentDate =  [self removeHourAndMinutes:date];
    
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
            components:NSCalendarUnitYear
              fromDate:newBirthdayDate
                toDate:newPresentDate
               options:0];

    return ageComponents.year;

}

+ (NSDate *)removeHourAndMinutes:(NSDate *)date {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone defaultTimeZone]];
    
    NSDateComponents* newDateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    newDateComponents.hour = 0;
    newDateComponents.minute = 0;
    newDateComponents.second = 0;
    
    NSDate *newDate = [calendar dateFromComponents:newDateComponents];
    
    return newDate;
}

@end
