//
//  ImageActivityProvider.swift
//  Keepy
//
//  Created by Ruslan Kolchakov on 6/7/17.
//  Copyright © 2017 Keepy. All rights reserved.
//

import Foundation

class ImageActivityProvider: NSObject, UIActivityItemSource {
    var image: UIImage
    
    init(image: UIImage) {
        self.image = image
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return image
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
        return image
    }
}
