        //
//  ServerComm.m
//
//
//  Created by Yaniv Solnik on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <objc/runtime.h>
#import <CommonCrypto/CommonHMAC.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

// #import <MagicalRecord/MagicalRecord.h>
// #import <MagicalRecord/CoreData+MagicalRecord.h>
#import <AdSupport/ASIdentifierManager.h> 
#import "NSData+MPBase64.h"
#import "Fan.h"
#import "AFURLSessionManager.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "KPLocalKidItem.h"
#import "KPLocalItem.h"
#import "KPLocalKid.h"
#import "KPLocalPlace.h"
#import "KPLocalAsset.h"

#import "Keepy-Swift.h"
#import "NSURLRequest+cURL.h"

@interface NSString (fixPlus)

- (NSString *)fixPlusSign;

@end

@implementation ServerComm

+ (instancetype)instance {
    return [[ServerComm alloc] init];
}

- (NSString *)GetUUID {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *) string;
}

- (NSString *)signRequest:(NSURLRequest *)req {
    NSString *key = [[UserManager sharedInstance] getMe].authKey;

    if (key == nil) {
        return @"123";
    }

    NSMutableString *input = [[NSMutableString alloc] init];


    [input appendFormat:@"%@", [req.URL.path lowercaseString]];
    [input appendFormat:@"%@", [req.HTTPMethod lowercaseString]];

    NSDictionary *headers = [req allHTTPHeaderFields];
    /*
     NSArray * sortedKeys = [[headers allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
     for (int i = 0; i < [sortedKeys count]; i++)
     {
     [input appendFormat:@"%@%@", [[sortedKeys objectAtIndex:i] lowercaseString],
     [[headers valueForKey:[sortedKeys objectAtIndex:i]] lowercaseString]];
     }*/
    [input appendFormat:@"content-type%@",
                        [[headers valueForKey:@"content-type"] lowercaseString]];
    [input appendFormat:@"reqtime%@",
                        [[headers valueForKey:@"reqtime"] lowercaseString]];
    [input appendFormat:@"uid%@",
                        [[headers valueForKey:@"uid"] lowercaseString]];

    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [input cStringUsingEncoding:NSASCIIStringEncoding];

    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];

    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);

    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];

    NSString *hash = [HMAC mp_base64EncodedString];

    //DDLogInfo(@"input:%@", input);
    return hash;
}

- (void)sendRequest:(NSString *)apiMethod withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock {
    [self sendRequest:apiMethod withParams:params openMethod:NO onSuccess:aSuccessBlock onFail:aFailBlock];
}

- (void)sendRequest:(NSString *)apiMethod withParams:(NSDictionary *)params openMethod:(BOOL)isOpenMethod onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock {
    [self sendRequest:apiMethod withParams:params openMethod:isOpenMethod onSuccess:aSuccessBlock onFail:aFailBlock withHttpMethod:@"POST" onHandler:nil];
}


- (void)SendReqPriorityVeryHigh:(NSString *)apiMethod
                     withParams:(NSDictionary *)params
                     openMethod:(BOOL)isOpenMethod
                      onSuccess:(RequestSuccessBlock)aSuccessBlock
                         onFail:(RequestFailBlock)aFailBlock
                 withHttpMethod:(NSString *)httpMethod
                      onHandler:(void (^)(void))handler {
    requestSuccessBlock = aSuccessBlock;
    requestFailBlock = aFailBlock;

    NSMutableString *paramsStr = [[NSMutableString alloc] init];
    NSString *astr = @"";
    for (NSString *key in params.allKeys) {
        id v = [params valueForKey:key];
        if ([v isKindOfClass:[NSString class]]) {
            v = [v fixPlusSign];
        }

        [paramsStr appendFormat:@"%@%@=%@", astr, key, v];
        astr = @"&";
    }

    NSString *getParamsStr = @"";
    if ([httpMethod isEqualToString:@"GET"]) {
        getParamsStr = [NSString stringWithFormat:@"?%@", paramsStr];
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", Defaults.apiEndpoint, apiMethod, getParamsStr]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url];
    [req setHTTPMethod:httpMethod];
    [req addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    [req addValue:[NSString stringWithFormat:@"%d", [[UserManager sharedInstance] getMe].userId.intValue] forHTTPHeaderField:@"uid"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [req addValue:[formatter stringFromDate:[NSDate date]] forHTTPHeaderField:@"reqtime"];

    NSString *hash = [self signRequest:req];
    [req addValue:[NSString stringWithFormat:@"Basic %@", hash] forHTTPHeaderField:@"Authorization"];
    [req addValue:[NSString stringOfUserAgent] forHTTPHeaderField:@"User-Agent"];
    
    NSLog(@"zvika526 ServerComm Call %@ %@ ...", httpMethod, url);
//    DDLogInfo(@"Params:%@", paramsStr);

    if ([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"]) {
        NSData *bodyData = [paramsStr dataUsingEncoding:NSUTF8StringEncoding];
        [req setHTTPBody:bodyData];
    }

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:req];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setShouldExecuteAsBackgroundTaskWithExpirationHandler:handler];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"zvika526 ServerResponse %@ %@\n%@", httpMethod, url, operation.responseString);
        
        if ([responseObject valueForKey:@"message"]) {
            [GlobalUtils showKeepyPopup:[responseObject valueForKey:@"message"]];
        }

        if (aSuccessBlock) {
            aSuccessBlock(responseObject);
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error: %@", error);

        if (aFailBlock) {
            aFailBlock(error);
        }

    }];

    [op start];


    self.activeOperation = op;


    if ([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"]) {
        NSData *bodyData = [paramsStr dataUsingEncoding:NSUTF8StringEncoding];
        [req setHTTPBody:bodyData];
    }
}


- (void)sendRequest:(NSString *)apiMethod
         withParams:(NSDictionary *)params
         openMethod:(BOOL)isOpenMethod
          onSuccess:(RequestSuccessBlock)aSuccessBlock
             onFail:(RequestFailBlock)aFailBlock
     withHttpMethod:(NSString *)httpMethod
          onHandler:(void (^)(void))handler {
    requestSuccessBlock = aSuccessBlock;
    requestFailBlock = aFailBlock;

    NSMutableArray *paramsArr = [NSMutableArray array];
    for (NSString *key in params.allKeys) {
        id v = [params valueForKey:key];
        if ([v isKindOfClass:[NSArray class]]) {
            for (id i in v) {
                [paramsArr addObject:[NSString stringWithFormat:@"%@[]=%@", key, i]];
            }
        } else {
            if ([v isKindOfClass:[NSString class]]) {
                v = [v fixPlusSign];
            }
            [paramsArr addObject:[NSString stringWithFormat:@"%@=%@", key, v]];
        }
    }
    NSString *paramsStr = [paramsArr componentsJoinedByString:@"&"];
    
    NSString *getParamsStr = @"";
    if ([httpMethod isEqualToString:@"GET"]) {
        getParamsStr = [NSString stringWithFormat:@"?%@", paramsStr];
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", Defaults.apiEndpoint, apiMethod, getParamsStr]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url];
    [req setHTTPMethod:httpMethod];
    [req addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    [req addValue:[NSString stringWithFormat:@"%d", [[UserManager sharedInstance] getMe].userId.intValue] forHTTPHeaderField:@"uid"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *requestTime = [formatter stringFromDate:[NSDate date]];
    [req addValue:requestTime forHTTPHeaderField:@"reqtime"];

    NSString *hash = [self signRequest:req];
    [req addValue:[NSString stringWithFormat:@"Basic %@", hash] forHTTPHeaderField:@"Authorization"];
    [req addValue:[NSString stringOfUserAgent] forHTTPHeaderField:@"User-Agent"];
    
    NSLog(@"%@ %@ ...", httpMethod, url);
    // DDLogInfo(@"Params:%@", paramsStr);

    if ([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"]) {
        NSData *bodyData = [paramsStr dataUsingEncoding:NSUTF8StringEncoding];
        [req setHTTPBody:bodyData];
    }
    
    //NSLog(@"%@", [req cURLCommandString]);  //commented out by amitg
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:req];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setShouldExecuteAsBackgroundTaskWithExpirationHandler:handler];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@ %@\n%@", httpMethod, url, operation.responseString);
        
        if ([responseObject valueForKey:@"message"]) {
            // DDLogInfo(@"%@",[responseObject valueForKey:@"message"]);

            [GlobalUtils showKeepyPopup:[responseObject valueForKey:@"message"]];
        }

        if (aSuccessBlock) {
            aSuccessBlock(responseObject);
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error: %@", error);

        if (aFailBlock) {
            aFailBlock(error);
        }

    }];

    [[NSOperationQueue mainQueue] addOperation:op];


    self.activeOperation = op;

    if ([httpMethod isEqualToString:@"POST"] || [httpMethod isEqualToString:@"PUT"]) {
        NSData *bodyData = [paramsStr dataUsingEncoding:NSUTF8StringEncoding];
        [req setHTTPBody:bodyData];
    }
}
- (void)sendRequest:(NSString *)apiMethod WithParameters:(NSDictionary *)params withRequestType:(NSString *)httpType onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock{
    requestSuccessBlock = aSuccessBlock;
    requestFailBlock = aFailBlock;
    

    NSDictionary *headers = @{ @"content-type": @"application/json" };

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", apiMethod]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url];
    [req setHTTPMethod:httpType];
    [req setAllHTTPHeaderFields:headers];
    if ([httpType isEqualToString:@"POST"]) {
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    [req setHTTPBody:postData];
    }
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:req
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        requestFailBlock(error);

                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSError *myError = nil;
                                                        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&myError];
                                                        
                                                        if (responseDict){
                                                            NSError *error;
                                                            
                                                            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseDict
                                                                                                               options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                                                                 error:&error];
                                                            
                                                            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                                            
                                                            NSLog(@"Response Dict : %@",jsonString);
                                                            requestSuccessBlock(responseDict);

                                                        }
                                                    }
                                                }];
    [dataTask resume];
}
- (void)uploadFiles:(NSString *)apiMethod withFilesData:(NSArray *)filesData onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock {
    [self uploadFiles:apiMethod withFilesData:filesData withParams:nil onSuccess:aSuccessBlock onFail:aFailBlock];
}

- (void)uploadFiles:(NSString *)apiMethod withFilesData:(NSArray *)filesData withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock {
    [self uploadFiles:apiMethod withFilesData:filesData withParams:params onSuccess:aSuccessBlock onFail:aFailBlock onProgress:nil onHandler:nil];
}

- (void)uploadFiles:(NSString *)apiMethod withFilesData:(NSArray *)filesData withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock onHandler:(void (^)(void))handler {
    requestSuccessBlock = aSuccessBlock;
    requestFailBlock = aFailBlock;

    NSURL *url = [NSURL URLWithString:Defaults.s3URLString];
    NSLog(@"POST %@ ...", url);

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];


    NSError *err;

    AFHTTPRequestSerializer *serializer = (AFHTTPRequestSerializer *) manager.requestSerializer;


    NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:[url absoluteString] parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {


        if (params != nil) {
            for (NSString *key in params.allKeys) {
                [formData appendPartWithFormData:[[params valueForKey:key] dataUsingEncoding:NSUTF8StringEncoding] name:key];
            }
        }

        [formData appendPartWithFormData:[[NSString stringWithFormat:@"assets/%@", [filesData[0] valueForKey:@"fileName"]] dataUsingEncoding:NSUTF8StringEncoding] name:@"key"];
        [formData appendPartWithFormData:[@"AKIAIMK5EH2KQAJL4DXA" dataUsingEncoding:NSUTF8StringEncoding] name:@"AWSAccessKeyId"];
        [formData appendPartWithFormData:[@"public-read" dataUsingEncoding:NSUTF8StringEncoding] name:@"acl"];
        [formData appendPartWithFormData:[@"200" dataUsingEncoding:NSUTF8StringEncoding] name:@"success_action_status"];

        for (NSDictionary *fileData in filesData) {
            if ([fileData valueForKey:@"data"] != nil) {
                [formData appendPartWithFileData:(NSData *) [fileData valueForKey:@"data"] name:[fileData valueForKey:@"fieldName"] fileName:[fileData valueForKey:@"fileName"] mimeType:[fileData valueForKey:@"mimeType"]];
            }
        }

    }                                                                   error:&err];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [op setShouldExecuteAsBackgroundTaskWithExpirationHandler:handler];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@ %@\n%@", @"POST", url, operation.responseString);
        
        if (aSuccessBlock) {
            aSuccessBlock(responseObject);
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error: %@", error);

        [[Analytics sharedAnalytics] track:@"server-request-error" properties:@{@"url" : apiMethod, @"error" : [error localizedDescription]}];

        if (aFailBlock) {
            aFailBlock(error);
        }

    }];

    [[NSOperationQueue mainQueue] addOperation:op];

    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        //DDLogInfo(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
        if (aProgressBlock) {
            aProgressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        }
    }];

    self.activeOperation = op;
}

- (void)uploadFile:(NSString *)apiMethod withFileData:(NSDictionary *)fileData withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock onHandler:(void (^)(void))handler {
    requestSuccessBlock = aSuccessBlock;
    requestFailBlock = aFailBlock;
    
    NSURL *url = [NSURL URLWithString:Defaults.s3URLString];
    NSLog(@"POST %@ ...", url);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    
    
    NSError *err;
    
    AFHTTPRequestSerializer *serializer = (AFHTTPRequestSerializer *) manager.requestSerializer;
    
    
    NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:[url absoluteString] parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
        
        
        if (params != nil) {
            for (NSString *key in params.allKeys) {
                [formData appendPartWithFormData:[[params valueForKey:key] dataUsingEncoding:NSUTF8StringEncoding] name:key];
            }
        }
        
        [formData appendPartWithFormData:[[NSString stringWithFormat:@"assets/%@", [fileData valueForKey:@"fileName"]] dataUsingEncoding:NSUTF8StringEncoding] name:@"key"];
        [formData appendPartWithFormData:[@"AKIAIMK5EH2KQAJL4DXA" dataUsingEncoding:NSUTF8StringEncoding] name:@"AWSAccessKeyId"];
        [formData appendPartWithFormData:[@"public-read" dataUsingEncoding:NSUTF8StringEncoding] name:@"acl"];
        [formData appendPartWithFormData:[@"200" dataUsingEncoding:NSUTF8StringEncoding] name:@"success_action_status"];
        
        if ([fileData valueForKey:@"data"] != nil) {
            [formData appendPartWithFileData:(NSData *) [fileData valueForKey:@"data"] name:[fileData valueForKey:@"fieldName"] fileName:[fileData valueForKey:@"fileName"] mimeType:[fileData valueForKey:@"mimeType"]];
        }
        
    }                                                                   error:&err];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [op setShouldExecuteAsBackgroundTaskWithExpirationHandler:handler];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@ %@\n%@", @"POST", url, operation.responseString);
        
        if (aSuccessBlock) {
            aSuccessBlock(responseObject);
        }
        
    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogInfo(@"Error: %@", error);
        
        [[Analytics sharedAnalytics] track:@"server-request-error" properties:@{@"url" : apiMethod, @"error" : [error localizedDescription]}];
        
        if (aFailBlock) {
            aFailBlock(error);
        }
        
    }];
    
    [[NSOperationQueue mainQueue] addOperation:op];
    
    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        //DDLogInfo(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
        if (aProgressBlock) {
            aProgressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        }
    }];
    
    self.activeOperation = op;
}

#pragma mark - User Methods

- (void)registerUser:(NSString *)email withName:(NSString *)name andPassword:(NSString *)password andParentType:(NSInteger)parentType andOtherTxt:(NSString *)otherTxt andFBId:(NSString *)fbId andFBAccessToken:(NSString *)fbAccessToken withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {

    NSMutableString *mStr = [email mutableCopy];
    //CFStringTrimWhitespace((CFMutableStringRef) mStr);
    
   // email = [mStr copy];
    
    if(mStr != nil) {
        CFStringTrimWhitespace((CFMutableStringRef) mStr);
        email = [mStr copy];
    }
    

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //zvika601 adding branchIO
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"BranchIOParamsKey"]){
        params = [NSMutableDictionary dictionaryWithDictionary: [[NSUserDefaults standardUserDefaults] objectForKey:@"BranchIOParamsKey"]];
    }
    
    params[@"email"] = email;
    params[@"name"] = name;

    if (password != nil) {
        params[@"password"] = password;
    }

    params[@"parentType"] = @(parentType);

    if (otherTxt != nil) {
        params[@"otherTxt"] = otherTxt;
    }

    if (fbId != nil) {
        params[@"facebookId"] = fbId;
    }

    if (fbAccessToken != nil) {
        params[@"facebookAccessToken"] = fbAccessToken;
    }


    NSString *deviceToken = Defaults.deviceToken;
    if (deviceToken != nil) {
        params[@"deviceToken"] = deviceToken;
    }

    NSString *facebookAdsId = [[UserManager sharedInstance] facebookAdsId];
    if (facebookAdsId != nil) {
        params[@"facebookAdsId"] = facebookAdsId;
    }

    NSString *registrationSignature = Defaults.registrationSignature;
    if (registrationSignature != nil) {
        params[@"registrationTrackingSignature"] = registrationSignature;
    }

    params[@"clientVersion"] = [NSString stringOfClientVersion];

    //zvika601 only for debugging NSLog(@"zvika601 registerUser param=%@",[params description] );
    
    [self        sendRequest:@"user" withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)updateUser:(NSString *)email withName:(NSString *)name andPassword:(NSString *)password andParentType:(NSInteger)parentType andOtherTxt:(NSString *)otherTxt andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"email"] = email;
    params[@"name"] = name;
    if (![password isEqualToString:@""]) {
        params[@"password"] = password;
    }
    params[@"parentType"] = @(parentType);

    params[@"otherTxt"] = otherTxt;

    NSString *deviceToken = Defaults.deviceToken;
    if (deviceToken != nil) {
        params[@"deviceToken"] = deviceToken;
    }

    NSString *facebookAdsId = [[UserManager sharedInstance] facebookAdsId];
    if (facebookAdsId != nil) {
        params[@"facebookAdsId"] = facebookAdsId;
    }

    NSString *registrationSignature = Defaults.registrationSignature;
    if (registrationSignature != nil) {
        params[@"registrationTrackingSignature"] = registrationSignature;
    }

    params[@"clientVersion"] = [NSString stringOfClientVersion];

    if (birthdate != nil) {
        params[@"birthdate"] = birthdate;
    }

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;

    [self        sendRequest:[NSString stringWithFormat:@"user/%d", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)login:(NSString *)email withPassword:(NSString *)password withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {

    NSMutableString *mStr = [email mutableCopy];
    CFStringTrimWhitespace((CFMutableStringRef) mStr);
    email = [mStr copy];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"email"] = email;
    params[@"password"] = password;


    NSString *deviceToken = Defaults.deviceToken;
    if (deviceToken != nil) {
        params[@"deviceToken"] = deviceToken;
    }

    NSString *facebookAdsId = [[UserManager sharedInstance] facebookAdsId];
    if (facebookAdsId != nil) {
        params[@"facebookAdsId"] = facebookAdsId;
    }

    NSString *registrationSignature = Defaults.registrationSignature;
    if (registrationSignature != nil) {
        params[@"registrationTrackingSignature"] = registrationSignature;
    }

    params[@"clientVersion"] = [NSString stringOfClientVersion];

    [self        sendRequest:@"user/login" withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)fblogin:(NSString *)facebookAccessToken withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"facebookAccessToken"] = facebookAccessToken;

    NSString *deviceToken = Defaults.deviceToken;
    if (deviceToken != nil) {
        params[@"deviceToken"] = deviceToken;
    }

    NSString *facebookAdsId = [[UserManager sharedInstance] facebookAdsId];
    if (facebookAdsId != nil) {
        params[@"facebookAdsId"] = facebookAdsId;
    }

    NSString *registrationSignature = Defaults.registrationSignature;
    if (registrationSignature != nil) {
        params[@"registrationTrackingSignature"] = registrationSignature;
    }

    params[@"clientVersion"] = [NSString stringOfClientVersion];

    [self        sendRequest:@"user/fblogin" withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)forgotPassword:(NSString *)email withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"email"] = email;

    [self        sendRequest:@"user/forgotPassword" withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)updateDeviceToken:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSString *deviceToken = Defaults.deviceToken;
    if (deviceToken == nil) {
        return;
    }

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"deviceToken"] = deviceToken;

    params[@"clientVersion"] = [NSString stringOfClientVersion];

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d", myId] withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)updateFacebookAdsId:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSString *facebookAdsId = [[UserManager sharedInstance] facebookAdsId];
    if (facebookAdsId == nil) {
        return;
    }

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"facebookAdsId"] = facebookAdsId;

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d", myId] withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)updateRegistrationSignature:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSString *registrationSignature = Defaults.registrationSignature;
    if (registrationSignature == nil) {
        return;
    }

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"registrationTrackingSignature"] = registrationSignature;

    params[@"clientVersion"] = [NSString stringOfClientVersion];

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d", myId] withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];

}

- (void)updateDropboxCredentials:(NSString *)dbStr withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSArray *arr = [dbStr componentsSeparatedByString:@"&"];
    for (NSString *str in arr) {
        NSArray *barr = [str componentsSeparatedByString:@"="];
        if ([barr[0] isEqualToString:@"oauth_token"]) {
            params[@"dropboxAccessToken"] = barr[1];
        }
        else if ([barr[0] isEqualToString:@"oauth_token_secret"]) {
            params[@"dropboxSecret"] = barr[1];
        }
        else if ([barr[0] isEqualToString:@"uid"]) {
            params[@"dropboxUserId"] = barr[1];
        }
    }


    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d", myId] withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)updateFacebookTokens:(NSString *)facebookId andFacebookAccessToken:(NSString *)facebookAccessToken andHasPublishPermissions:(BOOL)hasPublishPermissions andSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"facebookId"] = facebookId;
    params[@"facebookAccessToken"] = facebookAccessToken;
    params[@"facebookHasPublishPermissions"] = @(hasPublishPermissions);

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d", myId] withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];

}

- (void)updateTwitterTokens:(NSString*)twitterId token:(NSString *)token secret:(NSString *)secret handle:(NSString *)handle andSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"twitterId"] = twitterId;
    params[@"twitterAccessToken"] = token;
    params[@"twitterTokenSecret"] = secret;
    params[@"twitterHandle"] = handle;
    
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d", myId] withParams:params openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)syncDropbox:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/syncDropbox", myId] withParams:nil openMethod:YES onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)verifyAppStoreReceipt:(NSString*)receipt
                    productId:(NSString*)productId
                     planType:(NSString*)planType
                   couponCode:(NSString*)couponCode
                   completion:(void(^)(NSDictionary* response, NSError* error))completion {

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"receipt"] = receipt;
    params[@"productId"] = productId;
#if DEBUG
    params[@"sandbox"] = @(1);
#else
    params[@"sandbox"] = @(0);
#endif
    [params setValue:planType forKey:@"planType"];
    [params setValue:couponCode forKey:@"coupon"];

    [self sendRequest:[NSString stringWithFormat:@"user/%d/verifyAppStoreReceipt", myId]
           withParams:params
           openMethod:YES
            onSuccess:^(NSDictionary* data) { if (completion) { completion(data, nil); } }
               onFail:^(NSError* error) { if (completion) { completion(nil, error); } }
       withHttpMethod:@"POST"
            onHandler:nil];
}

- (void)addKid:(NSString *)name withGender:(NSInteger)gender andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self addKid:name withGender:gender andBirthdate:birthdate withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock allowDummy:YES];
}

- (void)addKid:(NSString *)name withGender:(NSInteger)gender andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock allowDummy:(BOOL)allowDummy {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"name"] = name;
    params[@"gender"] = @(gender);
    if (birthdate != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        params[@"birthdate"] = [formatter stringFromDate:birthdate];
    }

    // DDLogInfo(@"%@",params);

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/kid", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {

                if (allowDummy) {
                    NSDictionary *dummyResult = @{@"status" : @(0), @"result" : @{@"id" : @([self getDummyId])}};

                    if (aSuccessBlock) {
                        aSuccessBlock(dummyResult);
                    }
                }
                else {
                    if (aFailBlock) {
                        aFailBlock(error);
                    }
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)updateKid:(NSInteger)kidId withName:(NSString *)name andGender:(NSInteger)gender andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self updateKid:kidId withName:name andGender:gender andBirthdate:birthdate withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock allowDummy:YES];
}

- (void)updateKid:(NSInteger)kidId withName:(NSString *)name andGender:(NSInteger)gender andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock allowDummy:(BOOL)allowDummy {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"name"] = name;
    params[@"gender"] = @(gender);
    if (birthdate != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        params[@"birthdate"] = [formatter stringFromDate:birthdate];
    }

    [self        sendRequest:[NSString stringWithFormat:@"kid/%ld", (long)kidId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {

                if (allowDummy) {
                    NSDictionary *dummyResult = @{@"status" : @(0), @"result" : @{@"id" : @(kidId)}};

                    if (aSuccessBlock) {
                        aSuccessBlock(dummyResult);
                    }
                }
                else {
                    if (aFailBlock) {
                        aFailBlock(error);
                    }
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];

}

- (void)uploadKidPhoto:(NSInteger)kidId withImage:(UIImage *)image withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self uploadKidPhoto:kidId withImage:image withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock allowDummy:YES];
}

- (void)uploadKidPhoto:(NSInteger)kidId withImage:(UIImage *)image withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock allowDummy:(BOOL)allowDummy {
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
    NSLog(@"FNTRACE===: %@", @"uploadKidPhoto");

    [self prepareAsset:0 andMediaType:0 withSuccessBlock:^(id result) {
        long assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] longValue];
        [self uploadAsset:[[result valueForKey:@"result"] valueForKey:@"hash"] andMediaType:0 withData:imageData andPolicy:[[result valueForKey:@"result"] valueForKey:@"policy"] andSignature:[[result valueForKey:@"result"] valueForKey:@"policyHash"] withSuccessBlock:^(id r2) {

            [self commitAsset:[[[result valueForKey:@"result"] valueForKey:@"id"] longValue] andFileSize:imageData.length withSuccessBlock:^(id result) {


                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                params[@"assetId"] = @(assetId);
                [self        sendRequest:[NSString stringWithFormat:@"kid/%ld/photo", (long)kidId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                            if (aSuccessBlock) {
                                aSuccessBlock(data);
                            }

                        }         onFail:^(NSError *error) {
                            if (aFailBlock) {
                                aFailBlock(error);
                            }
                        } withHttpMethod:@"POST"
                               onHandler:nil];


            }    andFailBlock:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            }       onHandler:nil];

        }    andFailBlock:^(NSError *error) {
            if (aFailBlock) {
                aFailBlock(error);
            }

        }      onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            //if (progressBlock)
            //progressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        }       onHandler:nil];
    }     andFailBlock:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    }];

}

- (void)deleteKid:(NSInteger)kidId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self        sendRequest:[NSString stringWithFormat:@"kid/%ld", (long)kidId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"DELETE"
                   onHandler:nil];
}

- (void)stopFollowKid:(NSInteger)kidId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self        sendRequest:[NSString stringWithFormat:@"kid/%ld/stopFollow", (long)kidId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)addContactList:(NSArray *)contacts withCampaignId:(NSString *)campaignId withSuccessBlock:(APISuccessBlock)aSuccessBlock
          andFailBlock:(APIFailBlock)aFailBlock {


    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:contacts
                                                       options:0
                                                         error:&error];

    if (!jsonData) {

    } else {

        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        params[@"recipients"] = JSONString;

    }


    [self        sendRequest:[NSString stringWithFormat:@"campaign/%@/email", campaignId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];

}
- (void)getTheProductImageWithParameters:(NSDictionary *)params withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self sendRequest:[NSString stringWithFormat:@"https://api.print.io/api/v/5/source/api/productpreview/?recipeid=f255af6f-9614-4fe2-aa8b-1b77b936d9d6"] WithParameters:params withRequestType:@"POST" onSuccess:^(NSDictionary *data) {
        if (aSuccessBlock) {
            aSuccessBlock(data);
        }
    } onFail:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    }];
}
- (void)getThePriceOfTheProducts:(NSString *)productID withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self sendRequest:[NSString stringWithFormat:@"https://api.print.io/api/v/5/source/api/productvariants/?recipeid=f255af6f-9614-4fe2-aa8b-1b77b936d9d6&countryCode=US&productId=%@&version=5&source=api&all=false&languageCode=en&currencyCode=USD",productID] WithParameters:nil withRequestType:@"GET" onSuccess:^(NSDictionary *data) {
        if (aSuccessBlock) {
//            NSLog(@"Data From Price API:%@",[self getJSON:data]);
            aSuccessBlock(data);
        }
    } onFail:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    }];
}
- (NSString*)getJSON:(NSDictionary*)dict{
    
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

- (void)getTheProductTempleate:(NSString *)productSKU withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {

    [self sendRequest:[NSString stringWithFormat:@"https://api.print.io/api/v/5/source/api/producttemplates/?recipeid=f255af6f-9614-4fe2-aa8b-1b77b936d9d6&languageCode=en&sku=%@",productSKU] WithParameters:nil withRequestType:@"GET" onSuccess:^(NSDictionary *data) {
        if (aSuccessBlock) {
            aSuccessBlock(data);
        }
    } onFail:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    }];
}
- (void)activator:(NSString *)activatorId withSuccessBlock:(APISuccessBlock)aSuccessBlock
     andFailBlock:(APIFailBlock)aFailBlock {


    [self        sendRequest:[NSString stringWithFormat:@"activator/%@", activatorId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];

}

- (void)addItem:(NSString *)title
   selectedKids:(NSSet <NSNumber*>*)selectedKids
        andDate:(NSDate *)date
       andPlace:(NSUInteger)placeId
    andCropRect:(NSString *)cropRect
andOriginalAssetId:(NSUInteger)assetId
    andRotation:(float)rotation
         andSat:(float)sat andCon:(float)con
         andBri:(float)bri
andAutoEnhanceOn:(BOOL)autoEnhanceOn
    andItemType:(NSUInteger)itemType
   andExtraData:(NSString *)extraData
andShareWithFans:(BOOL)shareWithFans
        andTags:(NSArray *)tags
 andExtraParams:(NSMutableDictionary *)extraParams
withSuccessBlock:(APISuccessBlock)aSuccessBlock
   andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (title.length) {
        params[@"title"] = title;
    }
    
    NSMutableArray* kidIds = [NSMutableArray array];
    for (NSNumber* i in selectedKids.allObjects) {
        [kidIds addObject:[NSString stringWithFormat:@"%@", i]];
    }
    params[@"kidsIds"] = [kidIds componentsJoinedByString:@"|"];
    if (tags.count > 0) {
        NSMutableArray* tagIds = [NSMutableArray array];
        for (NSNumber* i in tags) {
            [tagIds addObject:[NSString stringWithFormat:@"%@", i]];
        }
        params[@"tags"] = [tagIds componentsJoinedByString:@","];
    }
    
    params[@"placeId"] = [NSString stringWithFormat:@"%ld", (long)placeId];
    if (date != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        params[@"itemDate"] = [formatter stringFromDate:date];
    }
    params[@"cropRect"] = cropRect;
    params[@"originalAssetId"] = [NSString stringWithFormat:@"%ld", (long)assetId];
    params[@"rotation"] = [NSString stringWithFormat:@"%2.2f", rotation];
    params[@"sat"] = [NSString stringWithFormat:@"%2.2f", sat];
    params[@"con"] = [NSString stringWithFormat:@"%2.2f", con];
    params[@"bri"] = [NSString stringWithFormat:@"%2.2f", bri];
    params[@"itemType"] = [NSString stringWithFormat:@"%ld", (long)itemType];
    if (extraData.length > 0) {
        params[@"extraData"] = [NSString stringWithFormat:@"%@", extraData];
    }
    params[@"shareStatus"] = [NSString stringWithFormat:@"%d", shareWithFans];
    
    params[@"filterId"] = @"0";  // Leave this not to surprise the server.
    params[@"autoEnhanceOn"] = [NSString stringWithFormat:@"%d", autoEnhanceOn];
    
    [params addEntriesFromDictionary:extraParams];
    
    //DDLogInfo(@"%@",params);
    NSLog(@"Parameters Sent:%@",params);

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/item", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {
        
        if (aSuccessBlock) {
            aSuccessBlock(data);
        }
        
    }         onFail:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    } withHttpMethod:@"POST"
                   onHandler:nil];
    
}


- (void)addItem:(NSString *)title
      selectedKids:(NSSet <NSNumber*>*)selectedKids
           andDate:(NSDate *)date
          andPlace:(NSUInteger)placeId
       andCropRect:(NSString *)cropRect
andOriginalImageHash:(NSString *)originalImageHash
andOriginalAssetId:(NSUInteger)assetId
       andRotation:(float)rotation
            andSat:(float)sat andCon:(float)con
            andBri:(float)bri
  andAutoEnhanceOn:(BOOL)autoEnhanceOn
       andItemType:(NSUInteger)itemType
      andExtraData:(NSString *)extraData
  andShareWithFans:(BOOL)shareWithFans
           andTags:(NSArray *)tags
    andExtraParams:(NSMutableDictionary *)extraParams
  withSuccessBlock:(APISuccessBlock)aSuccessBlock
      andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (title.length) {
        params[@"title"] = title;
    }
    
    NSMutableArray* kidIds = [NSMutableArray array];
    for (NSNumber* i in selectedKids.allObjects) {
        [kidIds addObject:[NSString stringWithFormat:@"%@", i]];
    }
    params[@"kidsIds"] = [kidIds componentsJoinedByString:@"|"];
    if (tags.count > 0) {
        NSMutableArray* tagIds = [NSMutableArray array];
        for (NSNumber* i in tags) {
            [tagIds addObject:[NSString stringWithFormat:@"%@", i]];
        }
        params[@"tags"] = [tagIds componentsJoinedByString:@","];
    }

    params[@"placeId"] = [NSString stringWithFormat:@"%ld", (long)placeId];
    if (date != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        params[@"itemDate"] = [formatter stringFromDate:date];
    }
    params[@"cropRect"] = cropRect;
    params[@"originalAssetId"] = [NSString stringWithFormat:@"%ld", (long)assetId];
    params[@"rotation"] = [NSString stringWithFormat:@"%2.2f", rotation];
    params[@"sat"] = [NSString stringWithFormat:@"%2.2f", sat];
    params[@"con"] = [NSString stringWithFormat:@"%2.2f", con];
    params[@"bri"] = [NSString stringWithFormat:@"%2.2f", bri];
    params[@"itemType"] = [NSString stringWithFormat:@"%ld", (long)itemType];
    if (extraData.length > 0) {
        params[@"extraData"] = [NSString stringWithFormat:@"%@", extraData];
    }
    params[@"shareStatus"] = [NSString stringWithFormat:@"%d", shareWithFans];

    params[@"filterId"] = @"0";  // Leave this not to surprise the server.
    params[@"autoEnhanceOn"] = [NSString stringWithFormat:@"%d", autoEnhanceOn];

    [params addEntriesFromDictionary:extraParams];

    //DDLogInfo(@"%@",params);
    NSLog(@"Parameters Sent:%@",params);
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/item", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];

}


- (void)addVideo:(NSUInteger)kidId_wtf
       withTitle:(NSString *)title
    selectedKids:(NSSet <NSNumber*>*)selectedKids
         andDate:(NSDate *)date
        andPlace:(NSUInteger)placeId
     andCropRect:(NSString *)cropRect
 andVideoAssetId:(NSInteger)videoId
 andThumpAssetId:(NSInteger)thumbId
andAutoEnhanceOn:(BOOL)autoEnhanceOn
     andItemType:(NSInteger)itemType
    andExtraData:(NSString *)extraData
andShareWithFans:(BOOL)shareWithFans
         andTags:(NSArray *)tags
     andDuration:(NSString *)duration
withSuccessBlock:(APISuccessBlock)aSuccessBlock
    andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (title.length) {
        params[@"title"] = title;
    }

    NSMutableArray* kidIds = [NSMutableArray array];
    for (NSNumber* i in selectedKids.allObjects) {
        [kidIds addObject:[NSString stringWithFormat:@"%@", i]];
    }
    params[@"kidsIds"] = [kidIds componentsJoinedByString:@"|"];
    
    params[@"placeId"] = [NSString stringWithFormat:@"%lu", (unsigned long)placeId];
    if (date != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        params[@"itemDate"] = [formatter stringFromDate:date];
    }
    params[@"cropRect"] = cropRect;
    params[@"hdVideoAssetId"] = [NSString stringWithFormat:@"%ld", (long)videoId];
    params[@"originalAssetId"] = [NSString stringWithFormat:@"%ld", (long)thumbId];
    params[@"rotation"] = [NSString stringWithFormat:@"%2.2f", 0.f];
    params[@"sat"] = [NSString stringWithFormat:@"%2.2f", 1.f];
    params[@"con"] = [NSString stringWithFormat:@"%2.2f", 1.f];
    params[@"bri"] = [NSString stringWithFormat:@"%2.2f", 0.f];
    params[@"itemType"] = [NSString stringWithFormat:@"%ld", (long)itemType];
    params[@"extraData"] = [NSString stringWithFormat:@"%@", extraData ? extraData : @""];
    params[@"shareStatus"] = [NSString stringWithFormat:@"%i", shareWithFans];

    params[@"filterId"] = [NSString stringWithFormat:@"%d", 0];
    params[@"autoEnhanceOn"] = [NSString stringWithFormat:@"%d", autoEnhanceOn];
    params[@"duration"] = duration == nil ? @"" : duration;

    //DDLogInfo(@"%@",params);

    if (tags != nil && [tags count] > 0) {
        params[@"tags"] = [tags componentsJoinedByString:@","];
    }

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;

    [self SendReqPriorityVeryHigh:[NSString stringWithFormat:@"user/%d/item", myId]
                       withParams:params
                       openMethod:NO
                        onSuccess:^(NSDictionary *data) {

                            if (aSuccessBlock) {
                                aSuccessBlock(data);
                            }

                        } onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            }      withHttpMethod:@"POST"
                        onHandler:nil];

}


- (void)updateItem:(KPLocalItem *)item imageChanged:(BOOL)imageChanged withTags:(NSArray *)tags andExtraParams:(NSDictionary *)extraParams withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (item.title != nil) {
        params[@"title"] = item.title;
    }

    NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:item];
    NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

    NSMutableSet *kidServerIDs = [NSMutableSet set];
    for (KPLocalKid *kid in itemKids) {
        [kidServerIDs addObject:@(kid.serverID)];
    }

    NSString *kidsIds = [kidServerIDs.allObjects componentsJoinedByString:@"|"];


    params[@"kidsIds"] = kidsIds;


    KPLocalPlace *itemPlace = [KPLocalPlace fetchByLocalID:item.placeID];

    if (itemPlace) {

        params[@"placeId"] = @(itemPlace.serverID).stringValue;
    }

    if (item.itemDate != 0) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        params[@"itemDate"] = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:item.itemDate]];
    }

    if (item.cropRect != nil) {
        //DLog(@"************** sending item.cropRect to server '%@'", item.cropRect);
        params[@"cropRect"] = item.cropRect;
    }

    // DDLogInfo(@"%@",item.originalImage);


    KPLocalAsset *itemOriginalImage = [KPLocalAsset fetchByLocalID:item.originalImageAssetID];

    params[@"imageChanged"] = @(imageChanged);
    params[@"originalAssetId"] = @(itemOriginalImage.serverID);
    params[@"rotation"] = [NSString stringWithFormat:@"%2.2f", item.rotation];
    params[@"sat"] = [NSString stringWithFormat:@"%2.2f", item.saturation];
    params[@"con"] = [NSString stringWithFormat:@"%2.2f", item.contrast];
    params[@"bri"] = [NSString stringWithFormat:@"%2.2f", item.brightness];
    params[@"itemType"] = [NSString stringWithFormat:@"%ld", (long)item.itemType];
    params[@"extraData"] = [NSString stringWithFormat:@"%@", item.extraData];
    params[@"shareStatus"] = [NSString stringWithFormat:@"%ld", (long)item.shareStatus];
    params[@"filterId"] = [NSString stringWithFormat:@"%ld", (long)item.filterID];
    params[@"autoEnhanceOn"] = [NSString stringWithFormat:@"%d", item.autoEnhanceOn];


    if (tags != nil && [tags count] > 0) {
        params[@"tags"] = [tags componentsJoinedByString:@","];
    }

    [params addEntriesFromDictionary:extraParams];

    // DDLogInfo(@"%@",params);

    //int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"item/%lu", (unsigned long)item.serverID] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {
                /*
                NSData* jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:nil];
                NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
                DDLogInfo(@"%@", jsonString);
                */

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];

}

- (void)deleteItem:(NSInteger)itemId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self        sendRequest:[NSString stringWithFormat:@"item/%ld", (long)itemId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"DELETE"
                   onHandler:nil];
}

- (void)shareItem:(NSInteger)itemId andMessage:(NSString *)message andFacebook:(NSInteger)facebook andTwitter:(NSInteger)twitter andKeepyFBShare:(BOOL)keepyFBShare withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"fb"] = @(facebook);
    params[@"twitter"] = @(twitter);
    params[@"keepyFBShare"] = @(keepyFBShare);
    params[@"message"] = message;

    [self        sendRequest:[NSString stringWithFormat:@"item/%ld/share", (long)itemId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)uploadItemFiles:(NSInteger)itemId withImage:(UIImage *)image andOriginalImage:(UIImage *)originalImage withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
    NSLog(@"FNTRACE===: %@", @"uploadItemFiles1");
    
    NSMutableDictionary *fileData = [[NSMutableDictionary alloc] init];
    [fileData setValue:imageData forKey:@"data"];
    [fileData setValue:@"image" forKey:@"fieldName"];
    [fileData setValue:[NSString stringWithFormat:@"item-%ld.jpg", (long)itemId] forKey:@"fileName"];
    [fileData setValue:@"image/jpg" forKey:@"mimeType"];

    NSData *originalImageData = UIImageJPEGRepresentation(originalImage, 0.7);
    NSLog(@"FNTRACE===: %@", @"uploadItemFiles2");
    NSMutableDictionary *originalFileData = [[NSMutableDictionary alloc] init];
    [originalFileData setValue:originalImageData forKey:@"data"];
    [originalFileData setValue:@"originalImage" forKey:@"fieldName"];
    [originalFileData setValue:[NSString stringWithFormat:@"original-item-%ld.jpg", (long)itemId] forKey:@"fileName"];
    [originalFileData setValue:@"image/jpg" forKey:@"mimeType"];


    [self uploadFiles:[NSString stringWithFormat:@"item/%ld/assets", (long)itemId] withFilesData:@[fileData, originalFileData] onSuccess:^(NSDictionary *data) {
        if (aSuccessBlock) {
            aSuccessBlock(data);
        }

    }          onFail:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    }];
}

- (void)uploadItemComment:(NSUInteger)itemId withVideo:(NSData *)videoData andPreviewImage:(UIImage *)previewImage andCommentLength:(NSInteger)commentLength withType:(NSInteger)commentType isStory:(BOOL)isStory withExtraData:(NSString *)extraData withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock andProgressBlock:(UploadProgressBlock)progressBlock {
    NSData *previewImageData = UIImageJPEGRepresentation(previewImage, 0.8); //changed 1.0 to 0.8 by amitg
    NSLog(@"FNTRACE===: %@", @"uploadItemComment");

    __block long videoAssetId = -1;
    __block long previewImageAssetId = -1;

    RequestSuccessBlock successBlock = ^(id result) {
        if (previewImageAssetId > -1 && (videoAssetId > -1 || commentType == 2)) {
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            params[@"commentLength"] = @(commentLength);
            params[@"commentAssetId"] = @(videoAssetId);
            params[@"previewAssetId"] = @(previewImageAssetId);
            params[@"commentType"] = @(commentType);
            if (isStory) {
                params[@"isStory"] = @(1);
            }

            if (extraData.length > 0) {
                params[@"extraData"] = extraData;
            }

            [self        sendRequest:[NSString stringWithFormat:@"item/%ld/comment", (long)itemId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                        if (aSuccessBlock) {
                            aSuccessBlock(data);
                        }

                    }         onFail:^(NSError *error) {
                        if (aFailBlock) {
                            aFailBlock(error);
                        }
                    } withHttpMethod:@"POST"
                           onHandler:nil];


        }
    };

    //upload video or audio file
    NSLog(@"FNTRACE upload Itemcomment: %ld", commentType);
    if (commentType != 2) {
        [self prepareAsset:0 andMediaType:(commentType == 0) ? 10 : 2 withSuccessBlock:^(id result) {
            long assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] longValue];
            [self uploadAsset:[[result valueForKey:@"result"] valueForKey:@"hash"] andMediaType:(commentType == 0) ? 1 : 2 withData:videoData andPolicy:[[result valueForKey:@"result"] valueForKey:@"policy"] andSignature:[[result valueForKey:@"result"] valueForKey:@"policyHash"] withSuccessBlock:^(id r2) {

                [self commitAsset:[[[result valueForKey:@"result"] valueForKey:@"id"] longValue] andFileSize:videoData.length withSuccessBlock:^(id result) {

                    videoAssetId = assetId;
                    successBlock(nil);

                }    andFailBlock:^(NSError *error) {
                    if (aFailBlock) {
                        aFailBlock(error);
                    }
                }       onHandler:nil];

            }    andFailBlock:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }

            }      onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                if (progressBlock) {
                    progressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
                }
            }       onHandler:nil];
        }     andFailBlock:^(NSError *error) {
            if (aFailBlock) {
                aFailBlock(error);
            }
        }];
    }

    NSLog(@"FNTRACE preview image");
    //upload previewImage
    [self prepareAsset:1 andMediaType:0 withSuccessBlock:^(id result) {
        long assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] longValue];
        [self uploadAsset:[[result valueForKey:@"result"] valueForKey:@"hash"] andMediaType:0 withData:previewImageData andPolicy:[[result valueForKey:@"result"] valueForKey:@"policy"] andSignature:[[result valueForKey:@"result"] valueForKey:@"policyHash"] withSuccessBlock:^(id r2) {

            [self commitAsset:[[[result valueForKey:@"result"] valueForKey:@"id"] longValue] andFileSize:previewImageData.length withSuccessBlock:^(id result) {

                previewImageAssetId = assetId;
                successBlock(nil);


            }    andFailBlock:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            }       onHandler:nil];

        }    andFailBlock:^(NSError *error) {
            if (aFailBlock) {
                aFailBlock(error);
            }

        }      onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            //if (progressBlock)
            //progressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        }       onHandler:nil];
    }     andFailBlock:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    }];
}

- (void)deleteComment:(NSInteger)commentId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self        sendRequest:[NSString stringWithFormat:@"comment/%ld", (long)commentId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"DELETE"
                   onHandler:nil];
}

- (void)getFanByEmail:(NSString *)email successBlock:(APISuccessBlock)aSuccessBlock failBlock:(APIFailBlock)aFailBlock {
    CFStringRef safeString = CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef) email, NULL, CFSTR("/%&=?$#+-~@<>|\\*,.()[]{}^!"), kCFStringEncodingUTF8);
    email = [NSString stringWithFormat:@"%@", safeString];
    [self        sendRequest:@"getFanByEmail" withParams:@{@"fanEmail" : email} openMethod:NO onSuccess:^(NSDictionary *data) {
                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }
            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)getVideoAssetStatus:(NSString *)videoAssetID successBlock:(APISuccessBlock)aSuccessBlock failBlock:(APIFailBlock)aFailBlock {

    [self        sendRequest:[NSString stringWithFormat:@"asset/%@/status", videoAssetID] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {
                aSuccessBlock ? aSuccessBlock(data) : (nil);

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];

}


- (void)getItems:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self getItems:aSuccessBlock andFailBlock:aFailBlock andForceItemsIds:nil];
}

- (void)getItems:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock andForceItemsIds:(NSString *)forceItemsIds {

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    if (forceItemsIds) {
        params[@"forceItemsIds"] = forceItemsIds;
    }

    NSDate* fromDate = Defaults.itemsLastUpdatedAt;
    if (fromDate) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        params[@"fromDate"] = [formatter stringFromDate:fromDate];
        NSLog(@"fromDate: %@", fromDate);
    }
    
    [self sendRequest:[NSString stringWithFormat:@"user/%d/items", myId]
           withParams:params
           openMethod:NO
            onSuccess:^(NSDictionary *data) {
                Defaults.itemsLastUpdatedAt = [NSDate date];
                if (aSuccessBlock) { aSuccessBlock(data); }
            }
               onFail:^(NSError *error) {
                if (aFailBlock) { aFailBlock(error); }
            }
       withHttpMethod:@"GET"
            onHandler:nil];
}

#pragma mark - Collection


- (void)addCollection:(NSString*)title
                 kids:(NSArray<NSNumber*>*)kids
       collectionDate:(NSDate*)date
                 tags:(NSArray<NSNumber*>*)tags
                items:(NSArray<NSNumber*>*)items
                place:(NSUInteger)placeId
          shareStatus:(BOOL)shareStatus
           completion:(void(^)(NSDictionary* response, NSError* error))completion {
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    if (title.length) { params[@"title"] = title; }
    if (tags.count > 0) { params[@"tags"] = tags; }
    
    if (date != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        params[@"collectionDate"] = [formatter stringFromDate:date];
    }
    
    params[@"kids"] = kids;
    params[@"items"] = items;
    params[@"placeId"] = [NSString stringWithFormat:@"%ld", (long)placeId];
    params[@"shareStatus"] = shareStatus ? @"1" : @"0";
    
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self sendRequest:[NSString stringWithFormat:@"user/%d/collection", myId]
           withParams:params
           openMethod:NO
            onSuccess:^(NSDictionary *data) { if (completion) { completion(data, nil); } }
               onFail:^(NSError *error) { if (completion) { completion(nil, error); } }
       withHttpMethod:@"POST"
            onHandler:nil];
}

- (void)shareCollection:(NSInteger)collectionId message:(NSString*)message facebook:(BOOL)facebook twitter:(BOOL)twitter keepy:(BOOL)keepy
             completion:(void(^)(NSDictionary* response, NSError* error))completion {
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    if (message.length) { params[@"message"] = message; }
    params[@"facebook"] = facebook ? @"1" : @"0";
    params[@"twitter"] = twitter ? @"1" : @"0";
    params[@"keepy"] = keepy ? @"1" : @"0";
    
    [self sendRequest:[NSString stringWithFormat:@"collection/%ld/share", (long)collectionId]
           withParams:params
           openMethod:NO
            onSuccess:^(NSDictionary *data) { if (completion) { completion(data, nil); } }
               onFail:^(NSError *error) { if (completion) { completion(nil, error); } }
       withHttpMethod:@"POST"
            onHandler:nil];
}

- (void)getCollection:(NSInteger)collectionId
           completion:(void(^)(NSDictionary* response, NSError* error))completion {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [self sendRequest:[NSString stringWithFormat:@"collection/%ld", (long)collectionId]
           withParams:params
           openMethod:NO
            onSuccess:^(NSDictionary *data) { if (completion) { completion(data, nil); } }
               onFail:^(NSError *error) { if (completion) { completion(nil, error); } }
       withHttpMethod:@"GET"
            onHandler:nil];
}

#pragma mark -

- (void)markItemRead:(NSInteger)itemId andReadAction:(NSInteger)readAction withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"userId"] = @(myId);
    params[@"readAction"] = @(readAction);

    [self        sendRequest:[NSString stringWithFormat:@"item/%ld/markRead", (long)itemId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)markCommentRead:(NSInteger)itemId andCommentId:(NSInteger)commentId andReadAction:(NSInteger)readAction withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"userId"] = @(myId);
    params[@"readAction"] = @(readAction);

    [self        sendRequest:[NSString stringWithFormat:@"item/%ld/comment/%ld/markRead", (long)itemId, (long)commentId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

#pragma mark - Story

- (void)uploadStory:(NSUInteger)itemId withStoryLength:(NSInteger)storyLength withData:(NSData *)data withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock andProgressBlock:(UploadProgressBlock)aProgressBlock {

    NSLog(@"FNTRACE uploadSTORY");
    
    [self prepareAsset:0 andMediaType:2 withSuccessBlock:^(id result) {
        long assetId = [[[result valueForKey:@"result"] valueForKey:@"id"] longValue];
        [self uploadAsset:[[result valueForKey:@"result"] valueForKey:@"hash"] andMediaType:2 withData:data andPolicy:[[result valueForKey:@"result"] valueForKey:@"policy"] andSignature:[[result valueForKey:@"result"] valueForKey:@"policyHash"] withSuccessBlock:^(id r2) {

            [self commitAsset:[[[result valueForKey:@"result"] valueForKey:@"id"] longValue] andFileSize:data.length withSuccessBlock:^(id result) {

                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                params[@"storyLength"] = @(storyLength);
                params[@"assetId"] = @(assetId);
                [self        sendRequest:[NSString stringWithFormat:@"item/%ld/story", (long)itemId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                            if (aSuccessBlock) {
                                aSuccessBlock(data);
                            }

                        }         onFail:^(NSError *error) {
                            if (aFailBlock) {
                                aFailBlock(error);
                            }
                        } withHttpMethod:@"POST"
                               onHandler:nil];


            }    andFailBlock:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            }       onHandler:nil];

        }    andFailBlock:^(NSError *error) {
            if (aFailBlock) {
                aFailBlock(error);
            }

        }      onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            if (aProgressBlock) {
                aProgressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
            }
        }       onHandler:nil];
    }     andFailBlock:^(NSError *error) {
        if (aFailBlock) {
            aFailBlock(error);
        }
    }];
}

- (void)deleteStory:(NSInteger)storyId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock; {
    [self        sendRequest:[NSString stringWithFormat:@"story/%ld", (long)storyId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"DELETE"
                   onHandler:nil];
}

#pragma mark - Kids

- (void)getKids:(BOOL)myOnly withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"myOnly"] = @(myOnly);

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/kids", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];
}

#pragma mark - Fans

- (void)addFan:(NSString *)email withName:(NSString *)name andRelationType:(NSInteger)relationType andFacebookId:(NSString *)facebookId andFacebookRequestId:(NSString *)facebookRequestId andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock; {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (facebookId != nil) {
        params[@"facebookId"] = facebookId;
        params[@"facebookRequestId"] = facebookRequestId;
    }
    else {
        params[@"email"] = email;
    }
    params[@"name"] = name;
    params[@"relationType"] = @(relationType);

    if (birthdate != nil) {
        params[@"birthdate"] = birthdate;
    }

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/fan", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];

}

- (void)updateFan:(Fan *)fan andFacebookRequestId:(NSString *)facebookRequestId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock; {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (fan.facebookId != nil) {
        params[@"facebookId"] = fan.facebookId;
        if (facebookRequestId != nil) {
            params[@"facebookRequestId"] = facebookRequestId;
        }
    }
    else {
        params[@"email"] = fan.email;
    }
    params[@"name"] = fan.nickname;
    params[@"relationType"] = @([fan.relationType intValue]);
    if (fan.birthdate != nil) {
        params[@"birthdate"] = fan.birthdate;
    }

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/fan/%d", myId, fan.fanId.intValue] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];

}

- (void)deleteFan:(NSInteger)fanId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/fan/%ld", myId, (long)fanId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"DELETE"
                   onHandler:nil];

}

- (void)reinviteFan:(NSInteger)fanId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/fan/%ld", myId, (long)fanId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)getFans:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/fans", myId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];

}

- (void)respondToFanRequest:(NSInteger)fanRequestId andAccept:(BOOL)doAccept withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"accept"] = @(doAccept);

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/fanrequest/%ld", myId, (long)fanRequestId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

#pragma mark - Notifications

- (void)getNotifications:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/notifications", myId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];

}

- (void)markNotificationRead:(NSInteger)notificationId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/notification/%ld", myId, (long)notificationId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)deleteNotification:(NSInteger)notificationId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/notification/%ld", myId, (long)notificationId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"DELETE"
                   onHandler:nil];
}

#pragma mark - UserInfo

- (void)getUserInfo:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    NSString *idfaString = @"";
    if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled])
    {
        NSString * idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        idfaString = [NSString stringWithFormat:@"?IDFA=%@",idfa];

    }
    
    
    [self sendRequest:[NSString stringWithFormat:@"user/%d/info%@", myId, idfaString] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil
    ];
}

- (void)getPrivateLink:(KPLocalItem *)item aSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;

    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:item.imageAssetID];

    [self        sendRequest:[NSString stringWithFormat:@"user/%d/%lu/%@/getPrivateLinkData", myId, (unsigned long)item.serverID, itemImage.urlHash] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];
}

#pragma mark - Facebook Invite Request

- (void)addFacebookInviteRequest:(NSString *)requestId withFBIds:(NSString *)fbIds andSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"fbRequestId"] = requestId;
    params[@"fbIds"] = fbIds;

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/fbrequestinvite", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

#pragma mark - Asset

- (BOOL)isJPEGValid:(NSData *)jpeg {
    if ([jpeg length] < 4) {return NO;}

    const unsigned char *bytes = (const unsigned char *) [jpeg bytes];
    //const char *bytes = (const char *)[jpeg bytes];
    if (bytes[0] != 0xFF || bytes[1] != 0xD8) {return NO;}
    if (bytes[[jpeg length] - 2] != 0xFF ||
            bytes[[jpeg length] - 1] != 0xD9) {
        return NO;
    }
    return YES;
}

- (void)downloadAssetFile:(KPLocalAsset *)asset withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *diskCachePath = [paths[0] stringByAppendingPathComponent:@"Assets"];

    if (![[NSFileManager defaultManager] fileExistsAtPath:diskCachePath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:diskCachePath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
    }


    NSString *fileExt = @"tmp";
    if (asset.assetType == 0) {
        fileExt = @"jpg";
    } else if (asset.assetType == 1) {
        fileExt = @"mov";
    } else if (asset.assetType == 2) {
        fileExt = @"mp4";
    } else if (asset.assetType == 4) {
        fileExt = @"mp4";
    }


    NSString *filePath = [diskCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", asset.urlHash, fileExt]];

    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        if (asset.assetType != 0 || [self isJPEGValid:[NSData dataWithContentsOfFile:filePath]]) {
            if (aSuccessBlock) {
                aSuccessBlock(filePath);
            }

            return;
        }
        else {
            //DDLogInfo(@"JPEG NOT VALID");
        }
    }


    //AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@", SERVER_URL]]];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.%@", Defaults.assetsURLString, asset.urlHash, fileExt]];

    DDLogInfo(@"Download:%@", [url absoluteString]);
    NSMutableURLRequest *rq = [[NSMutableURLRequest alloc] initWithURL:url];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:rq];

    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    __weak AFHTTPRequestOperation *op = operation;

    [operation setCompletionBlock:^{
        if (op.isCancelled) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                NSError *err;
                [[NSFileManager defaultManager] removeItemAtPath:fileExt error:&err];
            }

            return;
        }

        if (asset.assetType == -1) {
            NSString *contentType = [op.response.allHeaderFields valueForKey:@"Content-Type"];
            NSString *newFileExt = @"xxx";
            if ([contentType rangeOfString:@"image"].location != NSNotFound) {
                asset.assetType = 0;
                newFileExt = @"jpg";
            }
            else if ([contentType rangeOfString:@"video"].location != NSNotFound) {
                asset.assetType = 1;
                newFileExt = @"mov";
            }
            else if ([contentType rangeOfString:@"audio"].location != NSNotFound) {
                asset.assetType = 2;
                newFileExt = @"mp4";
            }

            NSString *newFilePath = [filePath stringByReplacingOccurrencesOfString:@"tmp" withString:newFileExt];
            NSError *err;
            [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:newFilePath error:&err];

            [asset saveChanges];
            // [NSManagedObjectContext MR]
            
            
            // MagicalRecord *record = [[MagicalRecord alloc] init];
            
            // [NSManagedObjectContext MR_contextForCurrentThread];
            // [NSManagedObjectContext MR_]
            
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

            if (aSuccessBlock) {
                aSuccessBlock(newFilePath);
            }
        }

        if (aSuccessBlock) {
            aSuccessBlock(filePath);
        }

    }];

    self.activeOperation = operation;

    [operation start];
}

- (void)saveAssetData:(NSData *)data withFileExt:(NSString *)fileExt andAssetHash:(NSString *)assetHash {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *diskCachePath = [paths[0] stringByAppendingPathComponent:@"Assets"];
    NSString *filePath = [diskCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", assetHash, fileExt]];
    [data writeToFile:filePath atomically:YES];
}

- (void)prepareAsset:(NSInteger)assetType andMediaType:(NSInteger)mediaType
    withSuccessBlock:(APISuccessBlock)aSuccessBlock
        andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    NSDictionary *para = @{@"ownerId" : @(myId),
            @"assetType" : @(assetType),
            @"mediaType" : @(mediaType),
            @"directS3Upload" : @(1)};

    if (mediaType == 1) {
        para = @{@"ownerId" : @(myId),
                @"assetType" : @(assetType),
                @"mediaType" : @(mediaType),
                @"format" : @(1),
                @"directS3Upload" : @(1)};

    }
    if (mediaType == 10) {
        para = @{@"ownerId" : @(myId),
                @"assetType" : @(assetType),
                @"mediaType" : @(1),
                @"format" : @(0),
                @"directS3Upload" : @(1)};

    }


    [self SendReqPriorityVeryHigh:[NSString stringWithFormat:@"asset/prepare"]
                       withParams:para

                       openMethod:NO
                        onSuccess:^(NSDictionary *data) {

                            if (aSuccessBlock) {
                                aSuccessBlock(data);
                            }

                        } onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            }      withHttpMethod:@"GET"
                        onHandler:nil];
}

- (void)prepareAsset:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self prepareAsset:1 andMediaType:0 withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock];
}

- (void)prepareVideoAsset:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self prepareAsset:1 andMediaType:1 withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock];
}

- (void)uploadAsset:(NSString *)assetHash withData:(NSData *)data andPolicy:(NSString *)policy andSignature:(NSString *)signature withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock {
    [self uploadAsset:assetHash andMediaType:0 withData:data andPolicy:policy andSignature:signature withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock onProgress:aProgressBlock onHandler:nil];
}


- (void)uploadVideoAsset:(NSString *)assetHash withData:(NSData *)data andPolicy:(NSString *)policy andSignature:(NSString *)signature withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock onHandler:(void (^)(void))handler {
    [self uploadAsset:assetHash andMediaType:4 withData:data andPolicy:policy andSignature:signature withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock onProgress:aProgressBlock onHandler:handler];
}


- (void)uploadAsset:(NSString *)assetHash
       andMediaType:(NSInteger)mediaType
           withData:(NSData *)data
          andPolicy:(NSString *)policy
       andSignature:(NSString *)signature
   withSuccessBlock:(APISuccessBlock)aSuccessBlock
       andFailBlock:(APIFailBlock)aFailBlock
         onProgress:(UploadProgressBlock)aProgressBlock
          onHandler:(void (^)(void))handler {
    NSString *fileExt = @".jpg";
    NSString *mimeType = @"image/jpg";
    if (mediaType == 1) {
        fileExt = @".mov";
        mimeType = @"video/quicktime";
    }
    else if (mediaType == 2) {
        fileExt = @".mp4";
        mimeType = @"audio/mp4";
    }
    else if (mediaType == 4) {
        fileExt = @".mp4";
        mimeType = @"video/mp4";
    }
NSLog(@"FNTRACE uploadASSET: %ld", mediaType);
    
    NSMutableDictionary *fileData = [[NSMutableDictionary alloc] init];
    [fileData setValue:data forKey:@"data"];
    [fileData setValue:@"file" forKey:@"fieldName"];
    [fileData setValue:[NSString stringWithFormat:@"%@%@", assetHash, fileExt] forKey:@"fileName"];
    [fileData setValue:mimeType forKey:@"mimeType"];
    //[fileData setValue:@"image/jpg" forKey:@"mimeType"];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"policy"] = (policy) ? policy : @"";
    params[@"signature"] = (signature) ? signature : @"";
    params[@"Content-Type"] = mimeType;


    [self uploadFiles:@"" withFilesData:@[fileData] withParams:params

               onSuccess:^(NSDictionary *data) {
                   if (aSuccessBlock) {
                       aSuccessBlock(data);
                   }

               } onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }

            } onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                if (aProgressBlock) {
                    aProgressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
                }

            }  onHandler:handler
    ];
}

- (void)getAssetPolicy:(long)assetId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    [self        sendRequest:[NSString stringWithFormat:@"asset/%ld/policy", assetId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];
}

- (void)commitAsset:(NSUInteger)assetId andFileSize:(long)fileSize withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onHandler:(void (^)(void))handler
{
    [self commitAsset:assetId attemptNumber:0 andFileSize:fileSize withSuccessBlock:aSuccessBlock andFailBlock:aFailBlock onHandler:handler];
}


- (void)commitAsset:(NSUInteger)assetId attemptNumber:(NSUInteger)attemptNumber andFileSize:(long)fileSize withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onHandler:(void (^)(void))handler
{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"fileSize"] = @(fileSize);

    [self sendRequest:[NSString stringWithFormat:@"asset/%ld/commit", (unsigned long)assetId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {
        
        [UploadTracker sharedInstance].isRetrying = false;
        [[UploadTracker sharedInstance] removeItemToWatchList:[NSString stringWithFormat:@"%lu", (unsigned long)assetId]];
        if (aSuccessBlock) {
            aSuccessBlock(data);
            
        }
        
    }         onFail:^(NSError *error) {
        if (aFailBlock) {
            if (attemptNumber < 60)
            {
                [UploadTracker sharedInstance].isRetrying = true;
                NSLog(@"failed asset upload retrying. Attempt: %lu", (unsigned long)attemptNumber);
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self commitAsset:assetId
                        attemptNumber:attemptNumber+1
                          andFileSize:fileSize
                     withSuccessBlock:aSuccessBlock
                         andFailBlock:aFailBlock
                            onHandler:handler];
                });
                
                
            }
            else
            {
                [[UploadTracker sharedInstance] setItemFailed: [NSString stringWithFormat:@"%lu", (unsigned long)assetId]];
                NSLog(@"failed asset upload retrying. No more retrys");
                aFailBlock(error);
            }
        }
    } withHttpMethod:@"POST"
            onHandler:handler];
    
}

#pragma mark - Places

- (void)addPlace:(NSString *)title withLon:(double)lon andLat:(double)lat withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"title"] = title;
    params[@"lon"] = @(lon);
    params[@"lat"] = @(lat);

    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/place", myId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"POST"
                   onHandler:nil];
}

- (void)editPlace:(NSUInteger)placeId withTitle:(NSString *)title andLon:(double)lon andLat:(double)lat withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"title"] = title;
    params[@"lon"] = @(lon);
    params[@"lat"] = @(lat);

    [self        sendRequest:[NSString stringWithFormat:@"place/%lu", (unsigned long)placeId] withParams:params openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"PUT"
                   onHandler:nil];
}

- (void)deletePlace:(NSUInteger)placeId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    
    [self sendRequest:[NSString stringWithFormat:@"place/%lu", (unsigned long)placeId]
           withParams:nil
           openMethod:NO
            onSuccess:^(NSDictionary *data) {
                if (aSuccessBlock) { aSuccessBlock(data); }
            }
               onFail:^(NSError *error) {
                if (aFailBlock) { aFailBlock(error); }
            }
       withHttpMethod:@"DELETE"
            onHandler:nil];
}

- (void)getPlaces:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock {
    int myId = [[UserManager sharedInstance] getMe].userId.intValue;
    [self        sendRequest:[NSString stringWithFormat:@"user/%d/places", myId] withParams:nil openMethod:NO onSuccess:^(NSDictionary *data) {

                if (aSuccessBlock) {
                    aSuccessBlock(data);
                }

            }         onFail:^(NSError *error) {
                if (aFailBlock) {
                    aFailBlock(error);
                }
            } withHttpMethod:@"GET"
                   onHandler:nil];
}

- (void)cancelOperation {

    [self.activeOperation cancel];


    if (requestFailBlock) {
        requestFailBlock(nil);
    }

}

- (NSInteger)getDummyId {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        int i = [[userDefs objectForKey:@"nextDummyId"] intValue];
        if (i == 0) {
            i = -100;
        }

        i--;
        [userDefs setObject:@(i) forKey:@"nextDummyId"];
        [userDefs synchronize];

        return i;
    }

    return -1;
}

@end

#define ACTIVITY_VIEW_TAG 5

@implementation UIImageView (Keepy)

static char kAssetObjectKey;

- (void)setImageWithAsset:(Asset *)asset {
    self.image = nil;
    UIActivityIndicatorView *vActivity = (UIActivityIndicatorView *) [self viewWithTag:ACTIVITY_VIEW_TAG];
    if (!vActivity) {
        vActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        vActivity.tag = ACTIVITY_VIEW_TAG;
        vActivity.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        vActivity.hidesWhenStopped = YES;
        [self addSubview:vActivity];
    }
    [vActivity startAnimating];

    self.asset = asset;

    [[ServerComm instance] downloadAssetFile:asset withSuccessBlock:^(id result) {

        Asset *currentAsset = (Asset *) objc_getAssociatedObject(self, &kAssetObjectKey);
        if (currentAsset != asset) { //old call
            return;
        }
        [self cleanActivityIndicator];
        self.image = [UIImage imageWithContentsOfFile:result];
    }                           andFailBlock:^(NSError *error) {
        [self cleanActivityIndicator];
    }];
}

- (void)cleanActivityIndicator {
    UIView *vActivity = [self viewWithTag:ACTIVITY_VIEW_TAG];
    [vActivity removeFromSuperview];
}

- (void)setAsset:(Asset *)asset {
    objc_setAssociatedObject(self, &kAssetObjectKey, asset, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@implementation NSString (fixPlus)

- (NSString *)fixPlusSign {
    
    NSCharacterSet *impermissibleCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"!*'();:@&=+$,/?#[]"];
    NSCharacterSet *permissibleCharacterSet = [impermissibleCharacterSet invertedSet];
    // NSMutableCharacterSet *permissibleCharacterSet = [NSCharacterSet alphanumericCharacterSet].mutableCopy;
    // [permissibleCharacterSet addCharactersInString:@"@,-._"];
    
    NSString *strictFix = [self stringByAddingPercentEncodingWithAllowedCharacters:permissibleCharacterSet];
    
    /*
    
    NSString *oldFix = [self stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    NSString *properFix = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *postFix = [properFix stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    */
     
    return strictFix;
    // return 
}

@end



