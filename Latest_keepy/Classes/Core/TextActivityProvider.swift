//
//  TextActivityProvider.swift
//  Keepy
//
//  Created by Ruslan Kolchakov on 6/7/17.
//  Copyright © 2017 Keepy. All rights reserved.
//

import Foundation

class TextActivityProvider: NSObject, UIActivityItemSource {
    var text: String = ""
    
    init(text: String) {
        self.text = text
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return NSObject()
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
        if activityType == .postToFacebook {
            return nil
        }
        return text
    }
}
