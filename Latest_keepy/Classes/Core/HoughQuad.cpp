//#include "stdafx.h"
//#include "cv.h"
//#include "highgui.h"
#include "QuadLib.h"


//extern int gDebug;
//extern int pDebug;
#define gDebug 0
#define pDebug 1

CvLinePolar Hough2Line(HLINE l, float rho, float theta, int numrho);

void DrawPolarLine(CvLinePolar l);

extern IplImage *clrQuad;

CvPoint2D32f iSectLine(CvLinePolar p1, CvLinePolar p2);

int pcomp(const void *a, const void *b) {
    return (*(int *) a > *(int *) b ? -1 : 1);
}

int hcomp(const void *a, const void *b) {
    HLINE *la = (HLINE *) a;
    HLINE *lb = (HLINE *) b;
    return (la->inum > lb->inum ? -1 : 1);
}

#define MAXLINES 64
static HLINE hLines[MAXLINES];
static CvLinePolar pLines[MAXLINES];
static int lActive[MAXLINES];
CvScalar Colors[4] = {{0, 0, 255}, {0, 255, 0}, {255, 0, 0}, {0, 255, 255}};


QuadErrorCode HoughQuad(cv::Mat edg, float rho, float theta, int threshold, CvPoint2D32f Q[4]) {
    byte *image;
    int step, width, height;
    int numangle, numrho;
//    int total = 0;
    float ang;
    int r, n;
    int i, j;
    float irho = 1 / rho;

    int linesMax = 8;

    image = edg.ptr<uchar>(0);
    step = edg.cols;
    width = edg.cols;
    height = edg.rows;

    numangle = cvRound(CV_PI / theta);
    numrho = cvRound(((width + height) * 2 + 1) / rho);

    int *accum = (int *) malloc((numangle + 2) * (numrho + 2) * sizeof(int));
    float *tabSin = (float *) malloc(numangle * sizeof(float));
    float *tabCos = (float *) malloc(numangle * sizeof(float));

    memset(accum, 0, sizeof(accum[0]) * (numangle + 2) * (numrho + 2));

    for (ang = 0, n = 0; n < numangle; ang += theta, n++) {
        tabSin[n] = (float) (sin(ang) * irho);
        tabCos[n] = (float) (cos(ang) * irho);
    }

    // stage 1. fill accumulator
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (image[i * step + j] != 0) {
                for (n = 0; n < numangle; n++) {
                    int weight = image[i * step + j] / EDGEDEC;
                    r = cvRound(j * tabCos[n] + i * tabSin[n]);
                    r += (numrho - 1) / 2;
                    accum[(n + 1) * (numrho + 2) + r + 1] += weight;
                }
            }
        }
    }
#ifdef SPECIAL
	int rcode = QuadSpecial(accum, numangle, numrho, edg, rho, theta);
#endif

    int nLines = 0;

    // stage 2. find local maximums
    for (r = 0; r < numrho; r++) {
        for (n = 0; n < numangle; n++) {
            int base = (n + 1) * (numrho + 2) + r + 1;
            int inum = accum[base];
            if (inum > threshold &&
                    inum > accum[base - 1] && inum >= accum[base + 1] &&
                    inum > accum[base - numrho - 2] && inum >= accum[base + numrho + 2]) {
                hLines[nLines].base = base;
                hLines[nLines].iangle = n;
                hLines[nLines].irho = r;
                hLines[nLines].inum = accum[base];
                nLines++;
            }
        }
    }

    // Sort the local maxima by magnitude
    qsort(hLines, nLines, sizeof(HLINE), hcomp);

    // stage 4. store the first min(total,linesMax) lines to the output buffer
    linesMax = MIN(linesMax, nLines);


    for (i = 0; i < linesMax; i++) {
        pLines[i] = Hough2Line(hLines[i], rho, theta, numrho);
        if (pDebug) {
            printf("Regular peak %5d (%5d)  -> %.3f %.3f\n",
                    hLines[i].base, hLines[i].inum, pLines[i].angle, pLines[i].rho);
        }
    }

    cv::Mat cdg;

    if (gDebug > 0) {
        cvtColor(edg, cdg, CV_GRAY2RGB);
        for (i = 0; i < linesMax; i++) {
            float rho = pLines[i].rho;
            float theta = pLines[i].angle;
            CvPoint pt1, pt2;
            double a = cos(theta), b = sin(theta);
            double x0 = a * rho, y0 = b * rho;
            pt1.x = cvRound(x0 + 1000 * (-b));
            pt1.y = cvRound(y0 + 1000 * (a));
            pt2.x = cvRound(x0 - 1000 * (-b));
            pt2.y = cvRound(y0 - 1000 * (a));
            line(cdg, pt1, pt2, CV_RGB(255, 0, 0));
        }
        imshow("INTERNAL", cdg);
        cvWaitKey(1);
    }


    for (j = 0; j < linesMax; j++) {
        lActive[j] = 1;
    }

    for (i = 0; i < linesMax; i++) {
        if (lActive[i] == 0) {
            continue;
        }
        CvLinePolar a = pLines[i];
        for (j = i + 1; j < linesMax; j++) {
            CvLinePolar b = pLines[j];

            if (pDebug) {
                printf("i = %2d j = %2d Comparing (%.2f %.2f) -> (%.2f %.2f)\n", i, j, a.angle, a.rho, b.angle, b.rho);
            }

            float drho1 = fabs(a.rho - b.rho);
            float dtheta1 = fabs(a.angle - b.angle);

            // Flip over Pi
            float drho2 = fabs(a.rho + b.rho);
            float dtheta2;
            if (a.angle > b.angle) {
                dtheta2 = fabs(b.angle + CV_PI - a.angle);
            } else {
                dtheta2 = fabs(a.angle + CV_PI - b.angle);
            }

            if (pDebug > 0) {
                printf("drho1 = %.1f dtheta1 = %.2f\n", drho1, dtheta1);
                printf("drho2 = %.1f dtheta2 = %.2f\n", drho2, dtheta2);
            }

            // rho threshold should depend on image dims
#define DRHO    30.0
#define DTHETA    0.4
            if (drho1 < DRHO && dtheta1 < DTHETA) {
                // Disable the shortest line
                lActive[j] = 0;
            }
            if (drho2 < DRHO && dtheta2 < DTHETA) {
                // Disable the shortest line
                lActive[j] = 0;
            }
        }
    }
    // Skip the disabled lines
    for (i = 0, j = 0; i < linesMax; i++) {
        if (lActive[i]) {
            pLines[j++] = pLines[i];
        }
    }
    linesMax = j;

    if (pDebug) {
        printf("Final %d lines\n", linesMax);
        for (i = 0, j = 0; i < linesMax; i++) {
            CvLinePolar a = pLines[i];
            printf("Final %1d (%.2f %.2f)\n", i, a.angle, a.rho);
        }
    }

    if (linesMax != 4) {
        free(accum);
        free(tabSin);
        free(tabCos);
        return (QUAD_NoQuad);
    }

    // Find pairs of almost parallel lines
    CvLinePolar a = pLines[0];
    // Line iortho is orthogonal to 0
    float dtmax = 0.0;
    int iortho = -1;
    for (i = 1; i < 4; i++) {
        CvLinePolar b = pLines[i];

#ifdef PAIRING_BUG
        //float dtheta1 = fabs(a.angle - b.angle);
        //float dtheta2 = fabs(a.angle + CV_PI - b.angle);
#else
        float dtheta1;
        float dtheta2;

        if (a.angle >= b.angle) {
            dtheta1 = a.angle - b.angle;
            dtheta2 = b.angle + CV_PI - a.angle;
        }
        else {
            dtheta1 = b.angle - a.angle;
            dtheta2 = a.angle + CV_PI - b.angle;
        }
#endif
        float dtheta = MIN(dtheta1, dtheta2);

        if (pDebug > 0) {
            printf("i = %d, dtheta1 = %f dtheta2 = %f dtheta = %f\n", i, dtheta1, dtheta2, dtheta);
        }

        if (dtheta > dtmax) {
            dtmax = dtheta;
            iortho = i;
        }
    }

    if (pDebug > 0) {
        printf("iortho = %d\n", iortho);
    }


    // Line iparal is parallel to 0
    float dtmin = 9.9;
    int iparal = -1;
    for (i = 1; i < 4; i++) {
        if (i == iortho) {
            continue;
        }
        CvLinePolar b = pLines[i];
#ifdef PAIRING_BUG
        //float dtheta1 = fabs(a.angle - b.angle);
        //float dtheta2 = fabs(a.angle + CV_PI - b.angle);
#else
        float dtheta1;
        float dtheta2;

        if (a.angle >= b.angle) {
            dtheta1 = a.angle - b.angle;
            dtheta2 = b.angle + CV_PI - a.angle;
        }
        else {
            dtheta1 = b.angle - a.angle;
            dtheta2 = a.angle + CV_PI - b.angle;
        }
#endif
        float dtheta = MIN(dtheta1, dtheta2);
        if (dtheta < dtmin) {
            dtmin = dtheta;
            iparal = i;
        }
    }
    if (pDebug > 0) {
        printf("iparal = %d\n", iparal);
    }

    for (i = 1; i < 4; i++) {
        if (i != iortho && i != iparal) {
            break;
        }
    }
    int iother = i;

    if (pDebug > 0) {
        printf("iother = %d\n", iother);
    }


    Q[0] = iSectLine(pLines[0], pLines[iortho]);
    Q[1] = iSectLine(pLines[iortho], pLines[iparal]);
    Q[2] = iSectLine(pLines[iparal], pLines[iother]);
    Q[3] = iSectLine(pLines[iother], pLines[0]);

    if (pDebug > 0) {
        for (i = 0; i < 4; i++) {
            printf("Q[%d] = %.1f %.1f\n", i, Q[i].x, Q[i].y);
        }
    }

    if (gDebug) {
        for (i = 0; i < 4; i++) {
            CvPoint p = cvPoint(cvRound(Q[i].x), cvRound(Q[i].y));
            circle(cdg, p, 5, Colors[i], -1);
        }
        imshow("INTERNAL", cdg);
        cvWaitKey(1);
    }

    free(accum);
    free(tabSin);
    free(tabCos);

    return (QUAD_OK);
}
