//
//  CachingAssistant.m
//  Keepy
//
//  Created by Arik Sosman on 6/9/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "CachingAssistant.h"

#import "PseudoRandomnessAssistant.h"

@implementation CachingAssistant

+ (NSString *)cacheLocationForExternalURL:(NSURL *)url{
    
    NSString *path = url.path;
    NSString *pathHash = [PseudoRandomnessAssistant sha256:path];
    NSString *fileName = [NSString stringWithFormat:@"cache-%@.txt", pathHash];
    
    NSArray *documentDirectoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = documentDirectoryPaths[0];
    NSString *dataModelDirectory = [documentDirectory stringByAppendingPathComponent:@"cache"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:dataModelDirectory]) {
        [fileManager createDirectoryAtPath:dataModelDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *dataModelSQLitePath = [dataModelDirectory stringByAppendingPathComponent:fileName];
    
    return dataModelSQLitePath;
    
}





@end
