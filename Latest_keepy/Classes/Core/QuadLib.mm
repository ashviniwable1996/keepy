// MKA-Edges.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
//#include "cv.h"
//#include "highgui.h"
#import "QuadLib.h"
//#import "MAOpenCV.h"

//NO_IPHONE extern int pDebug;
//NO_IPHONE extern int gDebug;
#define gDebug 0
#define pDebug 0

CvPoint2D32f iSectLine(CvLinePolar p1, CvLinePolar p2) {
    double a1 = cos(p1.angle);
    double b1 = sin(p1.angle);
    double c1 = -p1.rho;
    double a2 = cos(p2.angle);
    double b2 = sin(p2.angle);
    double c2 = -p2.rho;

    double u = b1 * c2 - b2 * c1;
    double v = a2 * c1 - a1 * c2;
    double w = a1 * b2 - a2 * b1;
    u /= w;
    v /= w;

    if (0) {
        printf("X %.1f %.1f\n", u, v);
        printf("X in Line 1: %.1f\n", a1 * u + b1 * v + c1);
        printf("X in Line 2: %.1f\n", a2 * u + b2 * v + c2);
    }


    CvPoint2D32f q;
    q.x = u;
    q.y = v;
    return (q);
}


cv::Mat clrQuad;


CvLinePolar Hough2Line(HLINE l, float rho, float theta, int numrho) {
    CvLinePolar line;
    int idx = l.base; // Can send idx only
    float scale = 1. / (numrho + 2);
    int n = cvFloor(idx * scale) - 1;
    int r = idx - (n + 1) * (numrho + 2) - 1;
    line.rho = (r - (numrho - 1) * 0.5f) * rho;
    line.angle = n * theta;

    return (line);
}


cv::Mat kpResize(cv::Mat inp, int wmax, int hmax, double *ratio) {
    // Determine decicv::Mation factor (to preserve aspect ratio)
    double wratio = (double) (inp.cols) / (double) wmax;
    double hratio = (double) (inp.rows) / (double) hmax;
    *ratio = (wratio > hratio ? wratio : hratio);
    if (*ratio <= 1.0) {
        *ratio = 1.0;
    }
    // Compute image dims
    int iWidth = cvRound(inp.cols / *ratio);
    int iHeight = cvRound(inp.rows / *ratio);

    cv::Size s = cv::Size(iWidth, iHeight);

    cv::Mat Out(iWidth, iHeight, CV_8UC3);
    resize(inp, Out, s, 0.0, 0.0, CV_INTER_AREA);

    return (Out);
}

// TODO: need to discard surface edges by classifying the surface color

cv::Mat WeightedEdges(cv::Mat iedge) {
    // Do horizontal then 45 deg scan and take minimum distance
    int x, y;
    // LR Scan<
    int cols = iedge.cols;
    int rows = iedge.rows;

    cv::Mat wedge(rows, cols, CV_8U, cv::Scalar(0));

    int edgeval;
    byte *eptr;
    byte *wptr;

    for (y = 0; y < rows; y++) {
        eptr = iedge.ptr<uchar>(y);
        wptr = wedge.ptr<uchar>(y);
        edgeval = EDGEVAL;
        // LR Scan
        for (x = 0; x < cols; x++) {
            if (edgeval <= 0) {
                break;
            }
            if (eptr[x] > 0) {
                if (edgeval > wptr[x]) {
                    wptr[x] = edgeval;
                }
                edgeval -= EDGEDEC;
            }
        }
        // RL Scan
        edgeval = EDGEVAL;
        for (x = cols - 1; x >= 0; x--) {
            if (edgeval <= 0) {
                break;
            }
            if (eptr[x] > 0) {
                if (edgeval > wptr[x]) {
                    wptr[x] = edgeval;
                }
                edgeval -= EDGEDEC;
            }
        }
    }

    int estep = cols;
    int wstep = cols;

    for (x = 0; x < cols; x++) {
        eptr = iedge.ptr<uchar>(0) + x;
        wptr = wedge.ptr<uchar>(0) + x;
        edgeval = EDGEVAL;
        // Top-Bottom Scan
        for (y = 0; y < rows; y++) {
            if (edgeval <= 0) {
                break;
            }
            if (eptr[0] > 0) {
                if (edgeval > wptr[0]) {
                    wptr[0] = edgeval;
                }
                edgeval -= EDGEDEC;
            }
            eptr += estep;
            wptr += wstep;
        }
        // Bottom-Top Scan
        eptr = iedge.ptr<uchar>(rows - 1) + x;
        wptr = wedge.ptr<uchar>(rows - 1) + x;

        edgeval = EDGEVAL;
        // Bottom-Top Scan
        for (y = rows - 1; y >= 0; y--) {
            if (edgeval <= 0) {
                break;
            }
            if (eptr[0] > 0) {
                if (edgeval > wptr[0]) {
                    wptr[0] = edgeval;
                }
                edgeval -= EDGEDEC;
            }
            eptr -= estep;
            wptr -= wstep;
        }
    }

    if (gDebug) {
        imshow("WEDGE", wedge);
        cvWaitKey(0);
    }
    return (wedge);
}

#ifdef CONTOUR2POLY

int cv::MatchPoints(CvPoint p, CvPoint q) {
	int dx = p.x - q.x;
	int dy = p.y - q.y;
	if(dx * dx + dy * dy < 16)
		return(1);
	return(0);
}

#define MINLEN

typedef enum {
	vUnknown, vRedundant, vCorner, vEndpoint
}	VERTYP; // Vertex type

typedef struct {
	CvPoint p;
	VERTYP typ;
}	VERTEX;

#define VMAX 8

typedef struct {
	double dLen; // digital length
	double wLen; // weighted length
	int vNum; // number of vertices
	int cNum; // Number of corners
	VERTEX vPoints[VMAX];
}	PLINE; // PolyLine

#define PMAX 32

PLINE pLines[PMAX];
#define TMAX 16
VERTEX tPoints[TMAX];

int Quad3Verts(PLINE *pl, CvPoint2D32f Quad[4]) {
	printf("Quad3Verts\n");
	int i, qnum;
    
	for(i = 0, qnum = 0; i < pl->vNum; i++) {
		VERTEX *v = pl->vPoints + i;
		printf("V%01d (%01d): %4d %4d\n", i, v->typ, v->p.x, v->p.y);
		if(v->typ == vCorner) {
			Quad[qnum].x = v->p.x;
			Quad[qnum].y = v->p.y;
			++qnum;
		}
	}
	if(qnum != 3)
		return(-1);
	// Compute the line from 1->0 with the line from 3->4
	CvPoint p0 = pl->vPoints[0].p;
	CvPoint p1 = pl->vPoints[1].p;
	// Compute the line from 1->0 with the line from 3->4
	double a1 = p0.y - p1.y;
	double b1 = p1.x - p0.x;
	double c1 = -a1 * p0.x - b1 * p0.y;
	printf("Check 0: %.f\n", a1 * p0.x + b1 * p0.y + c1);
	printf("Check 1: %.f\n", a1 * p1.x + b1 * p1.y + c1);
    
	// Compute the line from 4->3 with the line from 3->4
	CvPoint p3 = pl->vPoints[3].p;
	CvPoint p4 = pl->vPoints[4].p;
	// Intersect the line from 1->0 with the line from 3->4
	double a2 = p3.y - p4.y;
	double b2 = p4.x - p3.x;
	double c2 = -a2 * p3.x - b2 * p3.y;
    
	if(0) {
		printf("Check 0: %.f\n", a2 * p3.x + b2 * p3.y + c2);
		printf("Check 1: %.f\n", a2 * p4.x + b2 * p4.y + c2);
	}
    
    
    // Intersection Point in Homogeneous coordinates
	double u = b1 * c2 - b2 * c1;
	double v = a2 * c1 - a1 * c2;
	double w = a1 * b2 - a2 * b1;
	u /= w;
	v /= w;
    
	if(0) {
		printf("X %.1f %.1f\n", u, v);
		printf("X in Line 1: %.1f\n", a1 * u + b1 * v + c1);
		printf("X in Line 2: %.1f\n", a2 * u + b2 * v + c2);
	}
    
	Quad[3].x = u;
	Quad[3].y = v;
	return (0);
}


QuadErrorCode Edge2Quad(IplImage *edge, CvPoint2D32f Q[4]) {
    
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* contour = 0;
	// Traverse external contours only - to avoid double counting of a closed contour
	cvFindContours( edge, storage, &contour, sizeof(CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	CvSeq *head = contour;
    
	int i, j, pNum = 0;
	IplImage *cedge;
    
	if(gDebug) {
		cedge = cvCreateImage(cvGetSize(edge), 8, 3);
		cvZero(cedge);
	}
    
    
	// Minimum contour length
	int minlen = edge->width < edge->height ? edge->width : edge->height;
    
	for(contour = head; contour != 0; contour = contour->h_next )
	{
		double len = cvContourPerimeter(contour);
		if(len <= minlen)
			continue;
        
		if(pNum == PMAX) {
			printf("Reached %3d = max number of polylines\n", pNum);
			break;
		}
		PLINE *pl = pLines + pNum;
		++pNum;
        
		pl->dLen = len;
        
		int lCount = contour->total; // low count: original number of points on arc
        
		// Compute the contour weight
		CvPoint *pArray = (CvPoint*) malloc(lCount * sizeof(CvPoint));
		cvCvtSeqToArray(contour, pArray, CV_WHOLE_SEQ);
		for(i=0; i < lCount; i++) {
			int x = pArray[i].x;
			int y = pArray[i].y;
			byte *wptr = (byte*)(edge->imageData + y * edge->widthStep) + x;
			pl->wLen += (double)(wptr[0]) / (double)EDGEDEC;
		}
		printf("dLen, wLen = %.1f %.1f\n", pl->dLen, pl->wLen);
        
		if(pl->wLen < (double)minlen)
			continue;
        
		free(pArray);
        
		CvScalar color;
        
		// approxicv::Mate contour with accuracy proportional
		// to the contour perimeter
		CvSeq* result = cvApproxPoly( contour, sizeof(CvContour), storage,
                                     CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.02, 0 );
		
		printf("%2d vertices\n", result->total);
        
		// Copy initial vertices into tPoints
		int vNum = result->total;
		for(i = 0; i < result->total; i++) {
			CvPoint *p = (CvPoint*)cvGetSeqElem( result, i );
			tPoints[i].typ = vUnknown;
			tPoints[i].p = *p;
			printf("tPoint %2d: %3d %3d\n", i, p->x, p->y);
		}
		// Test for closed contour - no cv::MatchPoints
		int ClosedContour = 1;
		for(i = 0; i < vNum; i++) {
			for(j = i + 1; j < vNum; j++) {
				if(cv::MatchPoints(tPoints[i].p, tPoints[j].p)) {
					ClosedContour = 0;
				}
			}
		}
		printf("ClosedContour = %d\n", ClosedContour);
        
		if(ClosedContour) {
			for(i = 0; i < vNum; i++) {
				tPoints[i].typ = vCorner;
			}
		}
		else {
			// Look for the 1st endpoint in order to traverse the polygon from endpoint to endpoint
			for(i = 0; i < vNum; i++) {
				for(j = i + 1; j < vNum; j++) {
					if(cv::MatchPoints(tPoints[i].p, tPoints[j].p))
						break;
					printf("Endpoint search i = %d j = %d\n", i, j);
				}
				if(j == vNum)
					break;
			}
			if(i == vNum) {
				// No endpoint
				printf("No endpoint\n");
			}
			int epsta = i;
			tPoints[i].typ = vEndpoint;
			if(vNum == 2) {
				// No corners, just endpoints
				tPoints[0].typ = vEndpoint;
				tPoints[1].typ = vEndpoint;
			}
            
			printf("(%3d,%3d): %1d\n", tPoints[i].p.x, tPoints[i].p.y, tPoints[i].typ);
			// Classify tPoints
			for(i = epsta + 1; i != epsta; i = (i + 1)%vNum) {
				if(tPoints[i].typ != Unknown) // Already classified
					continue;
				for(j = i + 1; j != epsta; j = (j + 1)%vNum) {
					if(tPoints[j].typ != Unknown)
						continue;
					if(cv::MatchPoints(tPoints[i].p, tPoints[j].p)) {
						tPoints[i].typ = vCorner;
						tPoints[j].typ = vRedundant;
						break;
					}
				}
				if(j == epsta) {
					tPoints[i].typ = vEndpoint;
				}
				printf("(%3d,%3d): %1d\n", tPoints[i].p.x, tPoints[i].p.y, tPoints[i].typ);
			}
		}
        
        
        
		// Copy tPoints -> pl->vPoints skipping redundant points
		pl->cNum = 0;
		for(i = 0, j = 0; i < vNum; i++) {
			if(tPoints[i].typ == vRedundant)
				continue;
			if(tPoints[i].typ == vCorner)
				++(pl->cNum);
			pl->vPoints[j++] = tPoints[i];
		}
		pl->vNum = j;
        
        
		// Identify corners (double instance) + endpoints
		CvScalar red = cvScalar(0, 0, 255);
		CvScalar grn = cvScalar(0, 255, 0);
		CvScalar blu = cvScalar(255, 0, 0);
        
		if(gDebug) {
			cvDrawContours( cedge, contour, red, red, -1, 1, 8 );
			for(i = 0; i < pl->vNum; i++) {
				color = (pl->vPoints[i].typ == vCorner ? grn : blu);
				cvCircle(cedge, pl->vPoints[i].p, 3, color, -1);
			}
			cvShowImage("EDGES", cedge);
			cvWaitKey(0);
		}
	}
	printf("pNum = %3d\n", pNum);
	
    
	// Sort by number of corners, if equal - by length
	for(i = 0; i < pNum; i++) {
		PLINE *pl = pLines + i;
		if(pl->cNum == 4) { // Perfect
			printf("PERFECT\n");
			for(j = 0; j < 4; j++) {
				Q[j].x = pl->vPoints[j].p.x;
				Q[j].y = pl->vPoints[j].p.y;
			}
			return(QUAD_OK);
		}
		else if(pl->cNum == 3) {
			// Try to extend edges
			int rcode = Quad3Verts(pl, Q);
			if(gDebug) {
				CvPoint n = cvPoint(cvRound(Q[3].x), cvRound(Q[3].y));
				cvCircle(cedge, n, 2, cvScalar(255));
				cvShowImage("EDGES", cedge);
				cvWaitKey(0);
			}
		}
	}
    
	if(gDebug) {
		cvReleaseImage(&cedge);
	}
	
	return (QUAD_OK);
}
#endif


//
//
//IplImage *Luminate(IplImage *clr) {
//	CvSize iSize	= cvGetSize(clr);
//	IplImage *lum	= cvCreateImage(iSize, 8, 1);
//	cvCvtColor(clr, lum, CV_BGR2GRAY);
//	return(lum);
//}
//
//// Detect the edges of the color image clr
//
//IplImage *edgemap(IplImage *clr) {
//	CvSize iSize	= cvGetSize(clr);
//	IplImage *lum	= Luminate(clr);
//	IplImage *edg	= cvCreateImage(iSize, 8, 1);
//	cvCanny(lum, edg, 50, 100);
//	cvReleaseImage(&lum);
//	return(edg);
//}

CvFont fText;

void MyInitFont() {
    double hScale = 0.75;
    double vScale = 0.75;
    int lineWidth = 2;
    cvInitFont(&fText, CV_FONT_HERSHEY_SIMPLEX | CV_FONT_ITALIC, hScale, vScale, 0, lineWidth);
}


void ArrangeQuad(CvPoint2D32f Q[4]) {
    // Q is assumed start upper left and move CCW
    CvPoint2D32f P[4];
    // Copy Q->P
    int i;
    for (int i = 0; i < 4; i++) {
        P[i] = Q[i];
    }

    // Find the Diagonal Intersection Point
    float xmid = 0.0;
    float ymid = 0.0;
    for (int i = 0; i < 4; i++) {
        xmid += P[i].x;
        ymid += P[i].y;
    }
    xmid /= 4.0;
    ymid /= 4.0;

    int vFound[4];
    for (int i = 0; i < 4; i++) {
        vFound[i] = 0;
    }

    if (pDebug) {
        printf("xmid = %.2f ymid = %.2f\n", xmid, ymid);
    }

    float x, y;

    // Rearrange the vertices
    for (int i = 0; i < 4; i++) {
        x = P[i].x;
        y = P[i].y;

        if (x < xmid && y < ymid) {
            vFound[0] = 1;
            Q[0] = P[i];
        }
        if (x < xmid && y > ymid) {
            vFound[1] = 1;
            Q[1] = P[i];
        }
        if (x > xmid && y > ymid) {
            vFound[2] = 1;
            Q[2] = P[i];
        }
        if (x > xmid && y < ymid) {
            vFound[3] = 1;
            Q[3] = P[i];
        }
    }

    if (gDebug) {
        char sVertex[100];

        MyInitFont();

        for (i = 0; i < 4; i++) {
            if (!vFound[i]) {
                continue;
            }
            printf("srcQuad[%d] = %.1f %.1f\n", i, Q[i].x, Q[i].y);
            // Show vertex index
            CvPoint pText = cvPoint(cvRound(Q[i].x) + 20, cvRound(Q[i].y) + 20);
            sprintf(sVertex, "%1d", i);

            putText(clrQuad, sVertex, pText, CV_FONT_HERSHEY_SIMPLEX, 0.75, cvScalar(255, 0, 255), 2);
        }
        if (gDebug) {
            imshow("WEDGE + LINES", clrQuad);
            cvWaitKey(0);
        }
    }
}

QuadErrorCode HoughQuad(cv::Mat edg, float rho, float theta, int threshold, CvPoint2D32f Q[4]);

// Can be replaced by another utility for image saving

void mySaveImage(char *fName, IplImage *img) {
    cvSaveImage(fName, img);
}


cv::Mat edgemap(cv::Mat clr) {
    cv::Mat lum, edg;
    cvtColor(clr, lum, CV_BGR2GRAY);
    Canny(lum, edg, 100, 200);
    return (edg);
}


QuadErrorCode BestQuad(cv::Mat big, CvPoint2D32f Q[4], int hue_edges) {
    double ratio;
    QuadErrorCode errcode;

    cv::Mat inp = kpResize(big, 480, 480, &ratio);

    cv::Mat edg = edgemap(inp);

    if (gDebug) {
        imshow("EDG", edg);
        //waitKey(0);
    }

    cv::Mat wdg = WeightedEdges(edg);

//    int nAngle = 128;
//    int nRho = 256;
//    float rho = (float) ((wdg.cols + wdg.rows) * 2 + 1) / (float) nRho;
//    float theta = Pi / (float) nAngle;
    CvPoint2D32f Qh[4];

    errcode = HoughQuad(wdg, 4.0, CV_PI / 90, 128, Qh);

    if (errcode != QUAD_OK) {
        return (errcode);
    }

    // Verify the the vertices are not far away
    for (int j = 0; j < 4; j++) {
        Q[j] = Qh[j];
    }

    for (int j = 0; j < 4; j++) {
        float x = Q[j].x;
        float y = Q[j].y;

        if (x < -100.0 || x > 600) {
            return (QUAD_FarCorners);
        }
        if (y < -100.0 || y > 600) {
            return (QUAD_FarCorners);
        }
    }

    if (gDebug) {
        cvtColor(edg, clrQuad, CV_GRAY2BGR);
    }

    // Arrange Vertices
    ArrangeQuad(Q);

    // Restore original dimensions
    for (int j = 0; j < 4; j++) {
        Q[j].x *= ratio;
        Q[j].y *= ratio;
    }
    return (QUAD_OK);
}


cv::Mat RectQuad(cv::Mat inp, CvPoint2D32f srcQuad[4], int decFactor) {
    CvPoint2D32f dstQuad[4];
    float W, H;

    // Find the longest edge
    double dx, dy;
    dx = srcQuad[0].x - srcQuad[1].x;
    dy = srcQuad[0].y - srcQuad[1].y;
    float r01 = sqrt(dx * dx + dy * dy);

    dx = srcQuad[1].x - srcQuad[2].x;
    dy = srcQuad[1].y - srcQuad[2].y;
    float r12 = sqrt(dx * dx + dy * dy);

    dx = srcQuad[2].x - srcQuad[3].x;
    dy = srcQuad[2].y - srcQuad[3].y;
    float r23 = sqrt(dx * dx + dy * dy);

    dx = srcQuad[3].x - srcQuad[0].x;
    dy = srcQuad[3].y - srcQuad[0].y;
    float r30 = sqrt(dx * dx + dy * dy);

    H = (r01 + r23) / 1.0;
    W = (r12 + r30) / 1.0;

    H /= decFactor;
    W /= decFactor;

    // fix result size to be not larger then source
    cv::Size origSize = inp.size();
    float aspect = W / H;

    W = MIN(W, origSize.width);
    H = W / aspect;
    H = MIN(H, origSize.height);
    W = H * aspect;

    dstQuad[0].x = 0.0;
    dstQuad[0].y = 0.0;

    dstQuad[1].x = 0.0;
    dstQuad[1].y = H;

    dstQuad[2].x = W;
    dstQuad[2].y = H;

    dstQuad[3].x = W;
    dstQuad[3].y = 0.0;

    int i;

    if (gDebug) {
        for (i = 0; i < 4; i++) {
            CvPoint c = cvPoint(cvRound(srcQuad[i].x), cvRound(srcQuad[i].y));
            circle(inp, c, 5, cvScalar(0, 255, 0), -1);
        }
        imshow("SRC", inp);
        //cvSaveImage("C:\\_MKA\\SRC.png", inp);
        //waitKey(0);
    }

    cv::Point2f sQuad[4];
    cv::Point2f dQuad[4];

    for (i = 0; i < 4; i++) {
        sQuad[i] = srcQuad[i];
        dQuad[i] = dstQuad[i];
    }

    cv::Mat Twarp = getPerspectiveTransform(sQuad, dQuad);

    int flags;

    flags = CV_INTER_NN;
    flags = CV_INTER_LINEAR;

    cv::Size dsize;
    dsize.width = cvRound(W);
    dsize.height = cvRound(H);

    cv::Mat out;

    warpPerspective(inp, out, Twarp, dsize, flags);

    return (out);
}
