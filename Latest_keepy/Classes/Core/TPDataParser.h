//
//  TPDataParser.h
//  Tipengo
//
//  Created by Yaniv Solnik on 1/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface TPDataParser : NSObject

+ (instancetype)instance;
/*
-(TPEvent*)createEvent:(NSDictionary*)data withMatchLevel:(int)matchLevel;
-(TPEvent*)updateEvent:(NSDictionary*)data;
-(User*)createUser:(NSDictionary*)data;
-(Place*)createPlace:(NSDictionary*)data;
-(TPStat*)createStat:(int)statType withGender:(int)gender andName:(NSString*)statName andValue:(int)value forEvent:(TPEvent*)tpevent;
-(void)addMatchesToEvent:(NSDictionary*)data withEvent:(TPEvent*)tpevent;
*/
@end
