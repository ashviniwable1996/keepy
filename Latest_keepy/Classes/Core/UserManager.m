//
//  UserManager.m
//  solnik
//
//  Created by Yaniv Solnik on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>





#import "AppDelegate.h"
#import "KPDataParser.h"
#import <Crashlytics/Crashlytics.h>

#import "HomeViewController.h"
#import "KPLocalKid.h"
#import "KPLocalTag.h"
#import "Keepy-Swift.h"

@interface UserManager () <FBSDKSharingDelegate, FBSDKAppInviteDialogDelegate> {
    BOOL _isAfterLogin;
    BOOL _isAfterNewKid;
    BOOL _isAfterRegistartion;
    NSMutableArray * tags1;
}

@end

@implementation UserManager

@dynamic triggers;
@dynamic isAfterLogin;
@dynamic isAfterRegistration;

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self != nil) {
    }
    return self;
}

- (UIView *)globalView {
    return [[UIApplication sharedApplication].delegate window];
}

- (void)logout {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    [userDefs setObject:nil forKey:@"feedMode"];
    [userDefs synchronize];
}

#pragma mark user update

- (User *)getMe:(NSManagedObjectContext *)moc {
    return [User MR_findFirstByAttribute:@"isMe" withValue:@YES inContext:moc];
}

- (User *)getMe {
    return [self getMe:[NSManagedObjectContext MR_contextForCurrentThread]];
}

- (BOOL)isLoggedIn {
    User *me = [self getMe];
    return (me != nil);
}

- (void)refreshMyData {
    DDLogInfo(@"Refresh My Data");
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

    DDLogInfo(@"Refresh My Data Thread");

    @try {
        [[Crashlytics sharedInstance] setUserEmail:[self getMe].email];
        [[Crashlytics sharedInstance] setUserName:[self getMe].name];
        [[Crashlytics sharedInstance] setUserIdentifier:[self getMe].email];
    } @catch (NSException *e) {}

    [GlobalUtils updateAnalyticsUser];

    self.triggers = nil;

    __block BOOL kidsLoaded = NO;
    __block BOOL fansLoaded = NO;

    void (^createHomeBlock)() = ^void() {
        if (fansLoaded && kidsLoaded && [[UserManager sharedInstance] isAfterRegistration]) {
//                [[UserManager sharedInstance] setIsAfterRegistartion:NO];

            AppDelegate *d = (AppDelegate *) [[UIApplication sharedApplication] delegate];
            [d createHome];
            [d.navController setViewControllers:@[d.viewController] animated:NO];

            if ([[Kid MR_findAll] count] < 2) {
                EditKidViewController *editKidVC = ((HomeViewController *) d.viewController.frontViewController).editKidVC;
                if ([d.navController.viewControllers count] == 1) {
                    [d.navController pushViewController:editKidVC animated:NO];
                    [d.navController.navigationItem setHidesBackButton:YES];
                }
            }
            else if ([[[UserManager sharedInstance] myKids] count] > 0) {
                if (![[UserManager sharedInstance] isAfterRegistration]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_NEXTSTEP_NOTIFICATION object:nil];
                    });
                }
            }
        }
    };

    if ([[UserManager sharedInstance] isAfterRegistration]) {
        kidsLoaded = YES;
        createHomeBlock();
    } else {
        [[ServerComm instance] getItems:^(id result) {
            NSLog(@"Get Items: %@",[self getJSON:result]);

            // DDLogInfo(@"%@",result);

           //  NSLog(@"zvika526 getItems result=%@",result);
            int status = [[result valueForKey:@"status"] intValue];
            if (status == 0) {
                [[KPDataParser instance] parseKids:(NSArray *) [result valueForKey:@"result"] complete:^{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{

                        NSString *userType = @"Creator";
                        // NSArray *allKids = [Kid MR_findAll];
                        // NSArray *allKids = [KPLocalKid fetchAll];
                        NSArray *myKids = [KPLocalKid fetchMine];


                        BOOL hasKids = [KPLocalKid hasKids];
                        BOOL hasOwnKids = [KPLocalKid hasOwnKids];

                        BOOL isCreator = hasOwnKids;
                        BOOL isFan = hasKids && !hasOwnKids;


                        if (isFan) {
                            userType = @"Fan";
                            //[GlobalUtils showTip:3 withCompleteBlock:^{
                            //}];
                        }

                        [[Mixpanel sharedInstance] registerSuperProperties:@{@"UserType" : userType}];
                        [[Mixpanel sharedInstance] registerSuperProperties:@{@"isCreator" : @(isCreator) }];
                        [[Mixpanel sharedInstance] registerSuperProperties:@{@"isFan" : @(isFan)}];
                        [[[Mixpanel sharedInstance] people] set:@"isFan" to:@(isFan)];
                        [[[Mixpanel sharedInstance] people] set:@"isCreator" to:@(isCreator)];
                        [[[Mixpanel sharedInstance] people] set:@"Number Of Kids" to:@(myKids.count)];

                        [SVProgressHUD dismiss];

                        kidsLoaded = YES;
                        
                        
//
//                        NSDictionary * dict1 = @{@"itemID":[NSNumber numberWithUnsignedInteger:27],
//                                                @"tagID":[NSNumber numberWithUnsignedInteger:42]
//                                                };
//                        NSDictionary * dict2 = @{@"itemID":[NSNumber numberWithUnsignedInteger:28],
//                                                @"tagID":[NSNumber numberWithUnsignedInteger:40]
//                                                };
//                        NSDictionary * dict3 = @{@"itemID":[NSNumber numberWithUnsignedInteger:30],
//                                                @"tagID":[NSNumber numberWithUnsignedInteger:16]
//                                                };
//                        NSDictionary * dict4 = @{@"itemID":[NSNumber numberWithUnsignedInteger:35],
//                                                @"tagID":[NSNumber numberWithUnsignedInteger:42]
//                                                };
//
//                        NSArray * dictArray = @[dict1,dict2,dict3,dict4];
//
//                        for (NSDictionary * dictAry in dictArray) {
//                            KPLocalItemTag *itemTagRelation = [KPLocalItemTag create];
//                            itemTagRelation.tagID =  [dictAry[@"tagID"] integerValue];//(NSUInteger)dictAry[@"tagID"] ;
//                            itemTagRelation.itemID = [dictAry[@"itemID"] integerValue];//(NSUInteger)dictAry[@"itemID"];
//                            [itemTagRelation saveChanges];
//                        }
                        
                        //Added by Aakash -
                        
                        createHomeBlock();

                        //zvika526
                        
//                        NSLog(@"zvika526 222result=%@",result);
//                        //zvika526 fix
//    
//                        NSArray *itemsArray = [[result valueForKey:@"result"][0] valueForKey:@"items"];
//                        NSLog(@"%@",itemsArray);
                       // tags1 = [NSMutableArray alloc] initWithObjects:kpl count:<#(NSUInteger)#>
//                        NSMutableSet  * tagsId = [NSMutableSet new];
//                        for (KPLocalTag *tag  in tags1 ){
//                            [tagsId addObject:tag.serverID];
//                        }
//
//                        [KPLocalItemTag setLinkedTags:itemsArray forItem:localItem];
//                        var tagIds = Set<UInt>()
//                        for tags1 * i in tags1 { tagIds.insert(i.serverID) }
                        
//                        KPLocalItemTag.setLinkedTags(tags, for:localItem)
//                         NSLog(@"zvika526 222result=%@",itemsArray);
//                        
//                        for (id item in itemsArray) {
//                            
//                            if([item valueForKey:@"tags"])
//                            {
//                                NSArray *tagArray = [item valueForKey:@"tags"];
//                                
//                                
//                                NSArray *relatedTagIDs = [KPLocalItemTag fetchTagIDsForItem:item];
//                                if(![relatedTagIDs containsObject:@(localTag.identifier)]){ // this one has not been added yet (before zvika)
//                                    NSArray *newRelatedTagIDs = [relatedTagIDs arrayByAddingObject:@(localTag.identifier)];
//                                    NSArray *newLinkedTags = [KPLocalTag idsToObjects:newRelatedTagIDs];
//                                    
//                                    [KPLocalItemTag setLinkedTags:newLinkedTags forItem:item];
//                                }
//
//                                
//                                
//                            }
//                            
//                        }
//                        [[KPDataParser instance] parseTags:itemsArray];
                        
                        
                        
                    });
                    //[[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION object:nil];
                }];
            }
        }                  andFailBlock:^(NSError *error) {

            kidsLoaded = YES;
            createHomeBlock();

            DDLogInfo(@"error refreshing data");
        }];
    }


    [[ServerComm instance] getNotifications:^(id result) {
        [[KPDataParser instance] parseNotifications:[result valueForKey:@"result"]];
    }                          andFailBlock:^(NSError *error) {

    }];

    [[ServerComm instance] getPlaces:^(id result) {
        [[KPDataParser instance] parsePlaces:[result valueForKey:@"result"]];
    }                   andFailBlock:^(NSError *error) {

    }];

    [[ServerComm instance] getFans:^(id result) {
        NSLog(@"Fans Data: %@",[self getJSON:result]);

        [[KPDataParser instance] parseFans:[result valueForKey:@"result"]];

        int n = 0;
        for (Fan *fan in [[UserManager sharedInstance] myFans]) {
            if ([fan.didApprove boolValue]) {
                n++;
            }
        }
        [[[Mixpanel sharedInstance] people] set:@"Number Of Fans" to:@(n)];


        fansLoaded = YES;
        createHomeBlock();


    }                 andFailBlock:^(NSError *error) {
        fansLoaded = YES;
        createHomeBlock();

    }];
        
    [[ServerComm instance] getUserInfo:^(id result) {
        
        NSLog(@"User Data: %@",[self getJSON:result]);
        
        result = [result valueForKey:@"result"];

        //DDLogInfo(@"%@",result);
        
        if(!result){
            // this is probably due to connecting to a different server than the one you logged in to
            [SVProgressHUD showErrorWithStatus:@"You appear to be connecting to a different server than the one you are logged in with."];
            return;
        }

        //set dynamicMessages for tell a friend action
        if ([result valueForKey:@"activators"]) {

            NSArray *dArray = (NSArray *) [result valueForKey:@"activators"];

            // DDLogInfo(@"%@",dArray);

            if ([dArray count] > 0) {
                self.dynamicMessages = (NSArray *) dArray;
            }
            /*
            NSArray *filteredarray = [dArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(id == %@)", @"tell-a-friend"]];

            if ([filteredarray count] > 0) {
                self.dynamicMessages = [filteredarray firstObject];
            }*/

        }


        if ([result valueForKey:@"triggers"] != nil) {
            self.triggers = [result valueForKey:@"triggers"];
        }

        if ([result valueForKey:@"tags"] != nil) {
            [[KPDataParser instance] parseTags:[result valueForKey:@"tags"]];
        }

        User *me = [self getMe];
        
        NSDictionary* planInfo = [result valueForKey:@"planInfo"];
        Defaults.planType = [[planInfo valueForKey:@"planType"] integerValue];
        Defaults.keepyMonthlySpace = [[planInfo valueForKey:@"keepyMonthlySpace"] integerValue];
        Defaults.keepiesUsedThisMonth = [[planInfo valueForKey:@"keepiesUsedThisMonth"] integerValue];
        if ([planInfo valueForKey:@"expirationDate"] == [NSString class]) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            Defaults.planExpirationDate = [formatter dateFromString:[planInfo valueForKey:@"expirationDate"]];
        } else {
            Defaults.planExpirationDate = nil;
        }
        
        me.parentType = @([[[result valueForKey:@"user"] valueForKey:@"parentType"] intValue]);

        if ([[result valueForKey:@"user"] valueForKey:@"country"] != [NSNull null] && [[result valueForKey:@"user"] valueForKey:@"country"] != nil) {
            me.country = [[result valueForKey:@"user"] valueForKey:@"country"];
        }

        if ([[result valueForKey:@"user"] valueForKey:@"zazzleUrl"] != [NSNull null] && [[result valueForKey:@"user"] valueForKey:@"zazzleUrl"] != nil) {
            me.zazzleUrl = [[result valueForKey:@"user"] valueForKey:@"zazzleUrl"];
        }


        me.twitterHandle = [[result valueForKey:@"user"] valueForKey:@"twitterHandle"];
        me.facebookId = [[result valueForKey:@"user"] valueForKey:@"facebookId"];

        if ([[result valueForKey:@"user"] valueForKey:@"createDate"] != [NSNull null] && [[result valueForKey:@"user"] valueForKey:@"createDate"] != nil) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            me.createDate = [formatter dateFromString:[[result valueForKey:@"user"] valueForKey:@"createDate"]];
        }
        
        if ([planInfo valueForKey:@"registrationReferralId"] != [NSNull null]) {
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"registrationReferralId" : [planInfo valueForKey:@"registrationReferralId"]}];
        }

        //DDLogInfo(@"MyPlan:%d totalMB:%2f", me.planType.intValue, (float)me.totalUsedBytesValue/1000000.00);
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FANS_REFRESHED_NOTIFICATION object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:PLAN_REFRESHED_NOTIFICATION object:nil];
        }];



        ///xxx[GlobalUtils fireTrigger:TRIGGER_TYPE_ADDPHOTO];

        //try to upload missing assets

        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *diskCachePath = [paths[0] stringByAppendingPathComponent:@"Assets"];

        for (NSDictionary *massetDict in [result valueForKey:@"missingAssets"]) {
            //int atype = [[massetDict valueForKey:@"assetType"] intValue];
            int mtype = [[massetDict valueForKey:@"mediaType"] intValue];
            int aid = [[massetDict valueForKey:@"id"] intValue];
            NSString *assetHash = [massetDict valueForKey:@"assetHash"];
            NSString *assetURL = assetHash;
            if (assetHash == nil || [assetHash isEqual:[NSNull null]]) {
                assetURL = [NSString stringWithFormat:@"%d", aid];
            }

            NSString *fileExt = @"tmp";
            if (mtype == 0) {
                fileExt = @"jpg";
            } else if (mtype == 1) {
                fileExt = @"mov";
            } else if (mtype == 2) {
                fileExt = @"mp4";
            }

            NSData *data = nil;

            if (mtype == 0) {
                UIImage *aimage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, assetURL]];

                data = UIImageJPEGRepresentation(aimage, 0.8);
            }
            else {
                NSString *filePath = [diskCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", assetURL, fileExt]];

                if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                    data = [NSData dataWithContentsOfFile:filePath];
                }
            }

            if (data != nil) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
NSLog(@"FNTRACE userManager");
                    
                    [[ServerComm instance] getAssetPolicy:aid withSuccessBlock:^(id r2) {
                        [[ServerComm instance] uploadAsset:assetHash withData:data andPolicy:[[r2 valueForKey:@"result"] valueForKey:@"policy"] andSignature:[[r2 valueForKey:@"result"] valueForKey:@"policyHash"] withSuccessBlock:^(id result) {
                            [[ServerComm instance] commitAsset:aid andFileSize:data.length withSuccessBlock:^(id result) {
                                DDLogInfo(@"Uploaded...");
                            }                     andFailBlock:^(NSError *error) {

                            }                        onHandler:nil];

                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                        }                     andFailBlock:^(NSError *error) {
                            [GlobalUtils updateAssetUpload:aid totalBytesWritten:0 totalBytesExpectedToWrite:0];

                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                        }                       onProgress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                            DDLogInfo(@"uploading:%lld of %lld --- %d", totalBytesWritten, totalBytesExpectedToWrite, aid);
                            [GlobalUtils updateAssetUpload:aid totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];

                        }];

                    }                        andFailBlock:^(NSError *error) {

                    }];
                });

            }

        }

    }                     andFailBlock:^(NSError *error) {

    }];
    //});

    if ([[UserManager sharedInstance] facebookAdsId].length == 0) {
        FBSDKGraphRequest* request = [FBSDKAppEvents requestForCustomAudienceThirdPartyIDWithAccessToken:nil];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (error) {
                NSLog(@"FB Error: %@", error);
            } else {
                NSLog(@"FB result: %@", result);
                self.facebookAdsId = [result valueForKey:@"custom_audience_third_party_id"];
            }
        }];
    }
}

- (NSArray *)myKids {
    NSArray *result = [Kid MR_findByAttribute:@"isMine" withValue:@1 andOrderBy:@"name" ascending:YES];
    if (result == nil) {
        return [[NSArray alloc] init];
    } else {
        return result;
    }
}

- (NSArray *)myPlaces {
    
    NSArray *result = [Place MR_findAllSortedBy:@"title,placeId" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"title.length>0"] inContext:[NSManagedObjectContext MR_contextForCurrentThread]];
    //NSArray *result = [Place MR_findAllSortedBy:@"title,placeId" ascending:YES];
    if (result == nil) {
        return [[NSArray alloc] init];
    } else {
        return result;
    }

}

- (NSArray *)myFans {
    return [Fan MR_findAllSortedBy:@"nickname" ascending:YES withPredicate:nil];
    /*
    if ([[self myKids] count] == 0)
        return [[NSArray alloc] init];
    
    Kid *kid = [[[UserManager sharedInstance] myKids] objectAtIndex:0];
    
    NSArray *result = [Fan MR_findAllSortedBy:@"nickname" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"kids contains %@", kid]];
    if (result == nil)
        return [[NSArray alloc] init];
    else
        return result;
     */
}

- (BOOL)isMyKid:(KPLocalKid *)kid {
    return kid.isMine;
    // NSArray *mykids = [self myKids];
    // return [mykids containsObject:kid];
}

- (Kid *)keepyKid {
    Kid *kid = [Kid MR_findFirstByAttribute:@"kidId" withValue:@(88)];
    if (kid == nil) {
        kid = [Kid MR_createEntity];
        kid.kidId = @(88);
        kid.name = @"keepy";
    }
    return kid;
}

- (void)setSelectedKid:(KPLocalKid *)selectedKid {
    _selectedKid = selectedKid;
    if (selectedKid.isMine) {
        [GlobalUtils setCurrentSelectedKid:selectedKid];
    }
}

- (void)setFeedMode:(int)feedMode {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(feedMode) forKey:@"feedMode"];
        [userDefs synchronize];
    }
}

- (int)feedMode {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        id feedMode = [userDefs objectForKey:@"feedMode"];
        if(!feedMode || feedMode == [NSNull null]){
            return 1;
        }
        return [feedMode intValue];
    }
    return 1;
}

- (void)setCameraMode:(int)cameraMode {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(cameraMode) forKey:@"cameraMode"];
        [userDefs synchronize];
    }
}

- (int)cameraMode {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [[userDefs objectForKey:@"cameraMode"] intValue];
    }
    return 0;
}

#pragma mark - triggers

- (void)setTriggers:(NSArray *)triggers {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:triggers forKey:@"triggers"];
        [userDefs synchronize];
    }
}

- (NSArray *)triggers {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [userDefs objectForKey:@"triggers"];
    }

    return nil;
}

#pragma mark - social networks sharing

- (void)setFacebookShare:(BOOL)facebookShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(facebookShare) forKey:@"facebookShare"];
        [userDefs synchronize];
    }
}

- (BOOL)facebookShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [[userDefs objectForKey:@"facebookShare"] boolValue];
    }

    return NO;
}

- (void)setTwitterShare:(BOOL)twitterShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(twitterShare) forKey:@"twitterShare"];
        [userDefs synchronize];
    }
}

- (BOOL)twitterShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [[userDefs objectForKey:@"twitterShare"] boolValue];
    }

    return NO;
}

- (void)setPinterestShare:(BOOL)pinterestShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(pinterestShare) forKey:@"pinterestShare"];
        [userDefs synchronize];
    }
}

- (BOOL)pinterestShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [[userDefs objectForKey:@"pinterestShare"] boolValue];
    }

    return NO;
}

- (void)setKeepyFBShare:(BOOL)keepyFBShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(keepyFBShare) forKey:@"keepyFBShare"];
        [userDefs synchronize];
    }
}

- (BOOL)keepyFBShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [[userDefs objectForKey:@"keepyFBShare"] boolValue];
    }

    return NO;
}

- (void)setDidSpreadLove:(BOOL)didSpreadLove {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(didSpreadLove) forKey:@"didSpreadLove"];
        [userDefs synchronize];
    }
}

- (BOOL)didSpreadLove {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [[userDefs objectForKey:@"didSpreadLove"] boolValue];
    }

    return NO;
}

- (void)setApprovedKeepyGalleryShare:(BOOL)approvedKeepyGalleryShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(approvedKeepyGalleryShare) forKey:@"approvedKeepyGalleryShare"];
        [userDefs synchronize];
    }
}

- (BOOL)approvedKeepyGalleryShare {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [[userDefs objectForKey:@"approvedKeepyGalleryShare"] boolValue];
    }

    return NO;
}

- (void)setFacebookAdsId:(NSString *)facebookAdsId {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:facebookAdsId forKey:@"facebookAdsId"];
        [userDefs synchronize];
    }

    if ([self isLoggedIn]) {
        [[ServerComm instance] updateFacebookAdsId:^(id result) {

        }                             andFailBlock:^(NSError *error) {

        }];
    }

}

- (NSString *)facebookAdsId {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        return [userDefs objectForKey:@"facebookAdsId"];
    }

    return nil;
}


- (void)setShareWithFans:(BOOL)shareWithFans {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(shareWithFans) forKey:@"shareWithFans"];
        [userDefs synchronize];
    }
}

- (BOOL)shareWithFans {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        if ([userDefs objectForKey:@"shareWithFans"] == nil) {
            return ([[self myFans] count] > 0);
        }

        return [[userDefs objectForKey:@"shareWithFans"] boolValue];
    }


    return NO;
}

- (void)setIsAfterLogin:(BOOL)isAfterLogin {
    _isAfterLogin = isAfterLogin;
}

- (BOOL)isAfterLogin {
    return _isAfterLogin;
}

- (void)setIsAfterRegistration:(BOOL)isAfterRegistration {
    _isAfterRegistartion = isAfterRegistration;
}

- (void)setIsAfterOnBoardProccess:(BOOL)isAfterOnBoardProccess {
    _isAfterOnBoardProccess = isAfterOnBoardProccess;
}


- (BOOL)isAfterRegistration {
    return _isAfterRegistartion;
}

- (void)setIsAfterNewKid:(BOOL)isAfterNewKid {
    _isAfterNewKid = isAfterNewKid;
}

- (BOOL)isAfterNewKid {
    return _isAfterNewKid;
}

#pragma mark - Facebook

- (void)requireFacebookFromViewController:(UIViewController*)viewController success:(CompleteWithResultBlock)success failure:(CompleteWithResultBlock)failure {

    fbLoginBlock = success;
    fbLoginFailedBlock = failure;
    
    FBSDKAccessToken* currentAccessToken = [FBSDKAccessToken currentAccessToken];
    if (currentAccessToken == nil) {
        NSArray *permissions = @[@"email"];
        
        FBSDKLoginManager* fb = [FBSDKLoginManager new];
        [fb logInWithReadPermissions:permissions
#if FBSDK_46
                  fromViewController:viewController
#endif
                             handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (result.isCancelled) {
                if (fbLoginFailedBlock != nil) {
                    fbLoginFailedBlock(nil);
                    fbLoginFailedBlock = nil;
                }
            } else {
                if (fbLoginBlock != nil) {
                    fbLoginBlock(nil);
                    fbLoginBlock = nil;
                }
            }
        }];
    }
    else {
        //ablock();
        //[self fbDidLogin];
        if (fbLoginBlock != nil) {
            fbLoginBlock(nil);
            fbLoginBlock = nil;
        }

    }
}

- (void)fbDidLogin {
    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    
    if ([self getMe] == nil) //fbLogin to app
    {
        if (fbLoginBlock != nil) {
            fbLoginBlock(accessToken);
            fbLoginBlock = nil;
        }
        return;
    }

    //Get User's Facebook Profile
    [self graphPath:@"me" completion:^(id fbresult) {
        NSSet* permissions = accessToken.permissions;
        BOOL hasPublishPermission = [permissions containsObject:@"publish_actions"];
        [[ServerComm instance] updateFacebookTokens:[fbresult valueForKey:@"id"]
                             andFacebookAccessToken:accessToken.tokenString
                           andHasPublishPermissions:hasPublishPermission
                                    andSuccessBlock:^(id result) {
                                        
                                        DDLogInfo(@"%@", fbresult);
                                        User *me = [self getMe];
                                        me.facebookId = [fbresult valueForKey:@"id"];
                                        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                                            [self updateServerWithFBTokens];
                                        }];
                                        
                                        if (fbLoginBlock != nil) {
                                            fbLoginBlock(fbresult);
                                            fbLoginBlock = nil;
                                        }
                                    }
                                       andFailBlock:^(NSError *error) {
                                           if (fbLoginBlock != nil) {
                                               fbLoginBlock(nil);
                                               fbLoginBlock = nil;
                                           }
                                       }
         ];
    }];
}

- (void)getFBFriends:(CompleteWithResultBlock)completeBlock {
    [self graphPath:@"me/friends" completion:completeBlock];
}

- (void)getFBFamily:(CompleteWithResultBlock)completeBlock {
    [self graphPath:@"me/family" completion:completeBlock];
}

- (void)graphPath:(NSString*)path completion:(CompleteWithResultBlock)completion {
    FBSDKGraphRequest* graph = [[FBSDKGraphRequest alloc] initWithGraphPath:path parameters:nil];
    [graph startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id fbresult, NSError* error) {
        if (error) {
            NSLog(@"FB error: %@", error);
            return;
        }
        NSLog(@"FB graph(%@) %@", path, fbresult);
        
        if (completion) {
            completion(fbresult);
        }
    }];
}

/**
* Called when the user canceled the authorization dialog.
*/
- (void)fbDidNotLogin:(BOOL)cancelled {
    //DDLogInfo(@"did not login");
}

/**
* Called when the request logout has succeeded.
*/
- (void)fbDidLogout {
    //DDLogInfo(@"FBLogout");
}

- (void)updateServerWithFBTokens {
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        if ([[self getMe] facebookId] != nil) {
            FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
            NSSet* permissions = accessToken.permissions;
            BOOL hasPublishPermission = [permissions containsObject:@"publish_actions"];
            [[ServerComm instance] updateFacebookTokens:[[self getMe] facebookId]
                                 andFacebookAccessToken:accessToken.tokenString
                               andHasPublishPermissions:hasPublishPermission
                                        andSuccessBlock:^(id result) {}
                                           andFailBlock:^(NSError *error) {}
             ];
        }
    });
}

- (void)appInviteFromViewController:(UIViewController*)viewController {
    
    FBSDKAppInviteContent* content = [FBSDKAppInviteContent new];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/849053001816400"];
#if FBSDK_46
    content.appInvitePreviewImageURL = [NSURL URLWithString:@"https://scontent.fsnc1-1.fna.fbcdn.net/hphotos-xap1/t31.0-8/1039952_651548501550419_1077827303_o.jpg"];
    
    [FBSDKAppInviteDialog showFromViewController:viewController withContent:content delegate:self];
#else
    content.previewImageURL = [NSURL URLWithString:@"https://scontent.fsnc1-1.fna.fbcdn.net/hphotos-xap1/t31.0-8/1039952_651548501550419_1077827303_o.jpg"];
    [FBSDKAppInviteDialog showWithContent:content delegate:self];
#endif
    [[[Mixpanel sharedInstance] people] increment:@"activator-count-of-facebook" by:@(1)];
}

#pragma mark - FBSDKSharingDelegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    DDLogInfo(@"Story published.");
    [[Mixpanel sharedInstance] track:@"activator-completed" properties:@{@"success" : @"t"}];
    
    [self setDidSpreadLove:YES];
    
    int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
    });
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    DDLogInfo(@"Error publishing story: %@", error);
    [[Mixpanel sharedInstance] track:@"activator-completed" properties:@{@"success" : @"f"}];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    DDLogInfo(@"User canceled story publishing.");
}

#pragma mark - FBSDKAppInviteDialogDelegate

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
    DDLogInfo(@"invite success: %@", results);
    [[Mixpanel sharedInstance] track:@"activator-completed" properties:@{@"success" : @"t"}];
    
    int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
    });
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
    DDLogInfo(@"failed to invite: %@", error);
    [[Mixpanel sharedInstance] track:@"activator-completed" properties:@{@"success" : @"f"}];
}
- (NSString*)getJSON:(NSDictionary*)dict{
    
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}
@end
