//
//  GlobalUtils.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/22/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@class KPLocalKid;
@class KPLocalItem;
@class ItemsViewController;
@class Kid;
@class Place;
@class Item;
@class User;

typedef NS_ENUM(NSInteger, NotificationType) {
    NotificationTypeFanApproved = 0,
    NotificationTypeItems = 1,
    NotificationTypeCommentItem = 2,
    NotificationTypeNewFan = 3,
    NotificationTypeInappFan1 = 4,
    NotificationTypeInappKids = 8,
    NotificationTypeInappFan2 = 9,
    NotificationTypeCommentCollection = 13,
    NotificationTypeBookIsReady = 15
};

typedef NS_ENUM(NSInteger, NotificationStatus) {
    NotificationStatusUnread = 0,
    NotificationStatusRead = 1,
};

@interface UINavigationController (autoRotate)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;

- (BOOL)shouldAutorotate;

- (UIInterfaceOrientationMask)supportedInterfaceOrientations;

@end

@interface GlobalUtils : NSObject

@property (strong, nonatomic) ItemsViewController *itemsVC;

+ (instancetype)sharedInstance;

+ (BOOL)isEditMode;

+ (void)setIsEditMode:(BOOL)isEditMode;

+ (NSInteger)daysBetweenDate:(NSDate *)fromDateTime andDate:(NSDate *)toDateTime;

+ (NSString *)createCropRectStr:(NSArray *)points;

+ (NSArray *)getPointsFromCropRectStr:(NSString *)str;

+ (Kid *)getLastSelectedKid;

+ (void)setCurrentSelectedKid:(KPLocalKid *)kid;

+ (Place *)getLastSelectedPlace;

+ (void)setCurrentSelectedPlace:(Place *)kid;

+ (NSDate *)getLastSelectedDate;

+ (void)setCurrentSelectedDate:(NSDate *)date;

+ (void)outputToSpeaker;

+ (void)shareItem:(KPLocalItem *)item withSource:(NSString *)sourceStr sender:(UIButton *)sender;

+ (void)buyItem:(Item *)item withSource:(NSString *)sourceStr;

+ (NSString *)getParentTypeStr:(NSInteger)parentType;

+ (NSString *)getUserParentTypeStr:(User *)user;

+ (UIImage *)getKidDefaultImage:(NSInteger)gender;

+ (NSString *)getGenderStr:(NSInteger)gender;

+ (NSString *)getKidNames:(NSArray *)kids;

+ (NSString *)getKidNamesv2:(NSArray *)kids;

+ (NSString *)getKidNamesToShare:(NSArray *)kids;

+ (void)clearStoriesPlayed;

+ (BOOL)wasStoryPlayed:(NSInteger)storyId;

+ (void)storyPlayed:(NSInteger)storyId;

+ (void)execNotification:(NSInteger)notificationId withType:(NSInteger)notificationType andItems:(NSString *)items;

+ (void)saveUser:(NSDictionary *)data;

+ (UIImage *)imageFromColor:(UIColor *)color;

+ (void)addFanPop:(NSDictionary *)userDict;

+ (void)showKeepyPopup:(NSDictionary *)data;

+ (void)showNoSpace:(UIViewController *)viewController;

+ (void)showUpgrade;

+ (void)showUpgradeScreenWithParameters:(NSDictionary *)params fromViewController:(NSString *)fromVC;

+ (void)showTip:(NSInteger)tipIndex withView:(UIView *)parentView withCompleteBlock:(void (^)())completeBlock;

+ (void)hideAllTips;

+ (BOOL)checkCanInviteFans;

+ (void)updateAssetUpload:(NSInteger)assetId totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite;

+ (void)clearAssetsUpload;

+ (void)spreadTheLove;

+ (NSDictionary *)parseURLParams:(NSString *)query;

+ (void)showRateApp;

+ (void)showLoveApp;

+ (void)showHateApp;

+ (void)sendFeedback;

+ (void)rateAppOnStore;

+ (void)openSpreadLoveEmail:(NSArray *)recipients;

+ (BOOL)fireTrigger:(NSInteger)triggerType;

+ (void)killTriggerPopCounter:(NSString *)triggerId;

- (void)showItemsInFullScreen:(NSArray *)items withIndex:(NSInteger)index andCommentId:(NSInteger)commentId;

+ (void)storeCurrentItemData:(NSDictionary *)itemDict;

+ (NSDictionary *)getCurrentItemData;

+ (void)upladCurrentItem:(void (^)(id result))completeBlock;

+ (void)storeCurrentCommentData:(NSDictionary *)commentData;

+ (void)postNotificationForNewItem:(KPLocalItem *)item;

+ (ALAssetsLibrary *)defaultAssetsLibrary;

+ (BOOL)checkHasSpace;

+ (void)updateAnalyticsUser;


+ (UIViewController *)topMostController;

+ (BOOL)isConnected;

+ (void)facebookLogin:(NSArray<NSString*>*)permissions viewController:(UIViewController*)viewController completion:(void(^)(id result, NSError* error))completion;

+ (void)twitterLoginWithCompletion:(void(^)(id result, NSError* error))completion;

//+ (void)showRate

@end
