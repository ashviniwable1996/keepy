//
//  PseudoRandomnessAssistant.h
//  Keepy
//
//  Created by Arik Sosman on 6/10/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PseudoRandomnessAssistant : NSObject

+ (NSInteger)obtainPseudoRandomValueForDictionary:(NSDictionary *)dictionary withinMinimum:(NSInteger)min andMaximum:(NSInteger)max;
+ (NSString *)sha256:(NSString *)input;

@end
