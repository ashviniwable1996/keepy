//
//  AgeCalculator.h
//  Keepy
//
//  Created by Arik Sosman on 4/21/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgeCalculator : NSObject

+ (NSInteger)calculateAgeForBirthday:(NSDate *)birthday;

+ (NSInteger)calculateAgeForBirthday:(NSDate *)birthday atDate:(NSDate *)date;

+ (NSDate *)removeHourAndMinutes:(NSDate *)date;

@end
