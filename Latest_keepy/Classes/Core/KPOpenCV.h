//
//  KPOpenCV.h
//  Keepy
//
//  Created by Yaniv Solnik on 3/13/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPOpenCV : NSObject

+ (NSDictionary *)kpCropImage:(UIImage *)image withPoints:(NSArray *)points;

//+ (UIImage *)UIImageFromIplImage:(IplImage *)image;
//+ (IplImage *)CreateIplImageFromUIImage:(UIImage *)image;
+ (void)saveImage:(UIImage *)image toFileName:(NSString *)fileName;

//+(CGRect)openCvFaceDetect:(UIImage*)targetImage;
+ (UIImage *)rotateImage:(UIImage *)img byOrientationFlag:(UIImageOrientation)orient;

//+(UIImage*)kpEnhanceImage:(UIImage*)image withSat:(float)sat andCon:(float)con;
+ (NSDictionary *)kpGetImageBestRect:(UIImage *)image;

+ (CGPoint)findCorner:(UIImage *)image atPoint:(CGPoint)point;
//+(double)getMaxContrastGain:(UIImage*)image;

@end
