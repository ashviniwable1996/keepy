//
//  TPDataParser.m
//  Tipengo
//
//  Created by Yaniv Solnik on 1/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "TPDataParser.h"

@implementation TPDataParser

+ (instancetype)instance {
    return [[TPDataParser alloc] init];
}

/*
-(TPEvent*)createEvent:(NSDictionary*)data withMatchLevel:(int)matchLevel
{
    NSString *eventId = [data valueForKey:@"Id"];
    TPEvent *tpevent = [TPEvent MR_findFirstByAttribute:@"fbId" withValue:eventId];
    if (tpevent == nil)
        tpevent = [TPEvent MR_createEntity];
    
    tpevent.fbId = eventId;
    tpevent.name = [data valueForKey:@"Name"];
    tpevent.eventTime = [self getDateDotNet:[data valueForKey:@"StartTime"]];
    tpevent.matchLevelValue = matchLevel;

    //add attending friends
    [self addAttendingFriends:(NSArray*)[data valueForKey:@"RepliedFriends"] withEvent:tpevent];
    //add stats
    
    //attending count
    [self createGendersStats:(NSArray*)[data valueForKey:@"Genders"] withEvent:tpevent];
    
    //Ages
    [self createAgesStats:(NSArray*)[data valueForKey:@"Ages"] withEvent:tpevent];
    
    //MaleRelationships
    [self createRelationshipsStats:(NSArray*)[data valueForKey:@"MaleRelationships"] withEvent:tpevent andGender:TPGenderMale];
    
    //FemaleRelationships
    [self createRelationshipsStats:(NSArray*)[data valueForKey:@"FemaleRelationships"] withEvent:tpevent andGender:TPGenderFemale];
        
    
    return tpevent;
}

-(TPEvent*)updateEvent:(NSDictionary*)data
{
    NSString *eventId = [[data valueForKey:@"Details"] valueForKey:@"Id"];
    TPEvent *tpevent = [TPEvent MR_findFirstByAttribute:@"fbId" withValue:eventId];
    if (tpevent == nil)
        tpevent = [TPEvent MR_createEntity];
    
    tpevent.fbId = eventId;
    tpevent.name = [[data valueForKey:@"Details"] valueForKey:@"Name"];
    tpevent.locationName = [[data valueForKey:@"Details"] valueForKey:@"Location"];
    tpevent.eventTime = [self getDateDotNet:[[data valueForKey:@"Details"] valueForKey:@"StartTime"]];
    tpevent.attendingCountValue = [[[data valueForKey:@"Details"] valueForKey:@"AttendingCount"] intValue];
    
    
    //add attending friends
    [self addAttendingFriends:(NSArray*)[data valueForKey:@"AttendingFriends"] withEvent:tpevent];

    //add stats
    
    //attending count
    [self createGendersStats:(NSArray*)[[data valueForKey:@"Statistics"] valueForKey:@"Genders"] withEvent:tpevent];
    
    //Ages
    [self createAgesStats:(NSArray*)[[data valueForKey:@"Statistics"] valueForKey:@"Ages"] withEvent:tpevent];
    
    //MaleRelationships
    [self createRelationshipsStats:(NSArray*)[[data valueForKey:@"Statistics"] valueForKey:@"MaleRelationships"] withEvent:tpevent andGender:TPGenderMale];
    
    //FemaleRelationships
    [self createRelationshipsStats:(NSArray*)[[data valueForKey:@"Statistics"] valueForKey:@"FemaleRelationships"] withEvent:tpevent andGender:TPGenderFemale];
    
    //movies
    [self createMoviesStats:(NSArray*)[[data valueForKey:@"Statistics"] valueForKey:@"Movies"] withEvent:tpevent];
    
    //music
    [self createMusicStats:(NSArray*)[[data valueForKey:@"Statistics"] valueForKey:@"Music"] withEvent:tpevent];
    
    //places
    [self createPlacesStats:(NSArray*)[[data valueForKey:@"Statistics"] valueForKey:@"Places"] withEvent:tpevent];
    
    
    return tpevent;
}

-(User*)createUser:(NSDictionary*)data
{
    User *user = [User MR_findFirstByAttribute:@"fbId" withValue:[data valueForKey:@"Id"]];
    
    if (user == nil)
        user = [User MR_createEntity];
    
    user.name = [data valueForKey:@"Name"];
    user.fbId = [data valueForKey:@"Id"];
    return user;
}

-(Place*)createPlace:(NSDictionary*)data
{
    return nil;
}

-(TPStat*)createStat:(int)statType withGender:(int)gender andName:(NSString*)statName andValue:(int)value forEvent:(TPEvent*)tpevent
{
    TPStat *stat = [TPStat MR_createEntity];
    stat.statTypeValue = statType;
    stat.gender.intValue = gender;
    stat.statValueValue = value;
    stat.statName = statName;
    if (tpevent != nil)
        stat.tpevent = tpevent;
    
    return stat;
}

-(void)addMatchesToEvent:(NSDictionary*)data withEvent:(TPEvent*)tpevent
{
    NSArray *matches = (NSArray*)[data valueForKey:@"Matches"];
    for (NSDictionary *matchDict in matches)
    {
        User *auser = [self createUser:[matchDict valueForKey:@"Value"]];
        BOOL userExistsInEvent = NO;
        for (Match *match in tpevent.matches)
        {
            if ([match.user.fbId isEqualToString:auser.fbId])
            {
                userExistsInEvent = YES;
                break;
            }
        }
        
        if (!userExistsInEvent)
        {
            Match *match = [Match MR_createEntity];
            match.user = auser;
            [tpevent addMatchesObject:match];
        }
    }
}

#pragma mark - Utils

-(void) createGendersStats:(NSArray*)genders withEvent:(TPEvent*)tpevent
{
    for (NSDictionary *gdict in genders)
    {
        int gender = 0;
        if ([[gdict valueForKey:@"Key"] isEqualToString:@"Female"])
            gender = 1;
        
        TPStat *stat = [tpevent findStatByType:TPStatTypeAttendingCount withName:[gdict valueForKey:@"Key"] andGender:gender];
        if (stat == nil)
            [self createStat:TPStatTypeAttendingCount withGender:gender andName:[gdict valueForKey:@"Key"] andValue:[[gdict valueForKey:@"Value"] intValue] forEvent:tpevent];
        else
            stat.statValueValue = [[gdict valueForKey:@"Value"] intValue];
    }    
}

-(void) createAgesStats:(NSArray*)ages withEvent:(TPEvent*)tpevent
{
    for (NSDictionary *adict in ages)
    {
        int age = [[adict valueForKey:@"Key"] intValue];
        NSString *ageStr = [NSString stringWithFormat:@"%d", age];
        
        TPStat *stat = [tpevent findStatByType:TPStatTypeAges withName:ageStr andGender:TPGenderBoth];
        if (stat == nil)
            [self createStat:TPStatTypeAges withGender:TPGenderBoth andName:ageStr andValue:[[adict valueForKey:@"Value"] intValue] forEvent:tpevent];
        else
            stat.statValueValue = [[adict valueForKey:@"Value"] intValue];
    }
}

-(void) createRelationshipsStats:(NSArray*)arr withEvent:(TPEvent*)tpevent andGender:(int)gender
{
    for (NSDictionary *adict in arr)
    {
        NSString *akey = [adict valueForKey:@"Key"];
        
        TPStat *stat = [tpevent findStatByType:TPStatTypeRelationships withName:akey andGender:gender];
        if (stat == nil)
            [self createStat:TPStatTypeRelationships withGender:gender andName:akey andValue:[[adict valueForKey:@"Value"] intValue] forEvent:tpevent];
        else
            stat.statValueValue = [[adict valueForKey:@"Value"] intValue];
    }
    
}

-(void) createMoviesStats:(NSArray*)arr withEvent:(TPEvent*)tpevent
{
    for (NSDictionary *adict in arr)
    {
        NSString *akey = [adict valueForKey:@"Key"];
        
        TPStat *stat = [tpevent findStatByType:TPStatTypeMovies withName:akey andGender:TPGenderBoth];
        if (stat == nil)
            [self createStat:TPStatTypeMovies withGender:TPGenderBoth andName:akey andValue:[[adict valueForKey:@"Value"] intValue] forEvent:tpevent];
        else
            stat.statValueValue = [[adict valueForKey:@"Value"] intValue];
        
    }
}

-(void) createMusicStats:(NSArray*)arr withEvent:(TPEvent*)tpevent
{
    for (NSDictionary *adict in arr)
    {
        NSString *akey = [adict valueForKey:@"Key"];
        
        TPStat *stat = [tpevent findStatByType:TPStatTypeMusic withName:akey andGender:TPGenderBoth];
        if (stat == nil)
            [self createStat:TPStatTypeMusic withGender:TPGenderBoth andName:akey andValue:[[adict valueForKey:@"Value"] intValue] forEvent:tpevent];
        else
            stat.statValueValue = [[adict valueForKey:@"Value"] intValue];
    }
    
}

-(void) createPlacesStats:(NSArray*)arr withEvent:(TPEvent*)tpevent
{
    for (NSDictionary *adict in arr)
    {
        NSString *akey = [adict valueForKey:@"Key"];
        
        TPStat *stat = [tpevent findStatByType:TPStatTypePlaces withName:akey andGender:TPGenderBoth];
        if (stat == nil)
            [self createStat:TPStatTypePlaces withGender:TPGenderBoth andName:akey andValue:[[adict valueForKey:@"Value"] intValue] forEvent:tpevent];
        else
            stat.statValueValue = [[adict valueForKey:@"Value"] intValue];
        
    }
    
}

-(void) addAttendingFriends:(NSArray*)friends withEvent:(TPEvent*)tpevent
{
    for (NSDictionary *friendData in friends)
    {
        User *friend = [self createUser:friendData];
        if (![tpevent.attending containsObject:friend])
            [tpevent addAttendingObject:friend];
    }
}

- (NSDate*) getDateDotNet:(NSString *)dateString
{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = [dateString rangeOfString:@"("].location+1;
    int endPos = [dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}
*/

@end
