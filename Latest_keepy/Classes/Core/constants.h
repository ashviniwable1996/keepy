//
//  constants.h
//  solnik
//
//  Created by Yaniv Solnik on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef solnik_constants_h
#define solnik_constants_h

#pragma mark - notifications

#define USER_LOGIN_NOTIFICATION @"UserLoginNotification"
#define USER_LOGOUT_NOTIFICATION @"UserLogoutNotification"
#define ALBUM_SELECT_NOTIFICATION @"AlbumSelectNotification"
#define KID_SELECT_NOTIFICATION @"KidSelectNotification"
#define KID_ADDED_NOTIFICATION @"KidAddedNotification"
#define DELETE_KID_NOTIFICATION @"KidDeleteNotification"
#define ITEM_ADDED_NOTIFICATION @"ItemAddedNotification"
#define COMMENT_ADDED_NOTIFICATION @"CommentAddedNotification"
#define NEW_ALBUM_ITEM_NOTIFICATION @"NewAlbumItemNotification"
#define APP_MODE_CHANGED_NOTIFICATION @"AppModeChangedNotification"
#define FANS_REFRESHED_NOTIFICATION @"FansRefreshedNotification"
#define KIDS_REFRESHED_NOTIFICATION @"KidsRefreshedNotification"
#define LOVE_CHANGED_NOTIFICATION @"LoveChangedNotification"
#define FILTER_ON_NOTIFICATION @"FilterOnNotification"
#define FILTER_OFF_NOTIFICATION @"FilterOffNotification"
#define ITEM_DELETED_NOTIFICATION @"ItemDeletedNotification"
#define OPEN_CAMERA_NOTIFICATION @"OpenCameraNotification"
#define CHANGE_KID_NOTIFICATION @"ChangeKidNotification"
#define SHOW_KIDS_MENU_NOTIFICATION @"ShowKidsMenuNotification"
#define NOTIFICATIONS_REFRESHED_NOTIFICATION @"NotificationsRefreshedNotification"
#define ITEM_UPDATED_NOTIFICATION @"ItemUpdatedNotification"
#define CLOSE_FLOATING_VIEW @"CloseFloatingView"
#define SHOW_UNLIMITED_AFTER_ONBOARD @"showUnlimitedAfterOnBoarding"
#define DROPBOX_REFRESHED_NOTIFICATION @"DropBoxRefreshedNotification"
#define PLAN_REFRESHED_NOTIFICATION @"PlanRefreshedNotification"
#define UPLOAD_ASSETS_PROGRESS_NOTIFICATION @"UploadAssetsProgressNotification"
#define SHOW_NEXTSTEP_NOTIFICATION @"showNextStep"
#define ADD_PHOTO_END_NOTIFICATION @"addPhotoEndNotification"
#define CLOSE_NEXT_STEP_NOTIFICATION @"closeNextStepNotification"
#define CANCEL_CURRENT_UPLOAD_NOTIFICATION @"cancelCurrentUploadNotification"
#define INTERNET_CONNECTION_REVIVED_NOTIFICATION @"InternetConnectionRevivedNotification"
#define ITEM_ADDED_FROM_SERVER_NOTIFICATION @"ItemAddedFromServerNotification"
#define TELL_A_FRIEND_CONTACT_SHARING_DONE @"TLAMassage"
#define TELL_A_FRIEND_TRIGGERD @"TLATriggedMassage"
#define NOTIFICATIONS_NEED_TO_BE_RESET @"resetNotification"
#define PHOTO_ITEM_REFRESH @"photoItemRefresh"
#define CLOSE_ON_SHARE_ALSO @"CloseClickedOnShare"

#pragma mark - filter types

#define FILTER_TYPE_ALL 0
#define FILTER_TYPE_COMMENTS 1
#define FILTER_TYPE_LIKES 2
#define FILTER_TYPE_NEW 3
#define FILTER_TYPE_YEAR 4
#define FILTER_TYPE_CONTACTS 5

#pragma mark - buttons types

#define BUTTON_TYPE_NORMAL 0
#define BUTTON_TYPE_BACK 1
#define BUTTON_TYPE_PRIME 2

#pragma mark - Relation Type

#define RELATION_MOM 0
#define RELATION_DAD 1
#define RELATION_GRANDMA 2
#define RELATION_GRANPA 3
#define RELATION_FAMILY 4
#define RELATION_FRIEND 5
#define RELATION_AUNT 6
#define RELATION_UNCLE 7

#pragma mark - Triggers

#define TRIGGER_TYPE_OPENAPP 0
#define TRIGGER_TYPE_ADDPHOTO 1
#define TRIGGER_TYPE_ADDSTORY 2
#define TRIGGER_TYPE_ADDCOMMENT 3
#define TRIGGER_TYPE_ADDKID 4
#define TRIGGER_TYPE_ADDFAN 5
#define TRIGGER_TYPE_SPREADTHELOVE 6

#define TRIGGER_PARAM_PHOTOSCOUNT 0
#define TRIGGER_PARAM_STORIESCOUNT 1
#define TRIGGER_PARAM_COMMENTSCOUNT 2
#define TRIGGER_PARAM_KIDSCOUNT 3
#define TRIGGER_PARAM_FANSCOUNT 4

#define TRIGGER_COMPARER_EQUAL 0
#define TRIGGER_COMPARER_SMALLERTHAN 1
#define TRIGGER_COMPARER_GREATERTHAN 2

#define versionToPopUp @"2.0"

#endif
