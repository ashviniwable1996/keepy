//
//  KPOpenCV.m
//  Keepy
//
//  Created by Yaniv Solnik on 3/13/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KPOpenCV.h"
#import "QuadLib.h"

@implementation KPOpenCV

UIImage *scaleAndRotateImage(UIImage *image) {
    int kMaxResolution = 2500; // Or whatever

    CGImageRef imgRef = image.CGImage;

    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);

    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width / height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }

    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch (orient) {

        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;

        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;

        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;

        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;

        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;

        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;

        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;

        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;

        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];

    }

    UIGraphicsBeginImageContext(bounds.size);

    CGContextRef context = UIGraphicsGetCurrentContext();

    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }

    CGContextConcatCTM(context, transform);

    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return imageCopy;
}

/*
+(IplImage *)CreateIplImageFromUIImage:(UIImage *)image {
    
    //image = [UIImage imageWithCGImage:[image CGImage] scale:1.0 orientation:UIImageOrientationRight];
    //image = [self rotateImage:image byOrientationFlag:image.imageOrientation];
    //image = scaleAndRotateImage(image);
        CGImageRef imageRef = image.CGImage;
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        IplImage *iplimage = cvCreateImage(cvSize(image.size.width, image.size.height), IPL_DEPTH_8U, 4);
        CGContextRef contextRef = CGBitmapContextCreate(iplimage->imageData, iplimage->width, iplimage->height,
                                                        iplimage->depth, iplimage->widthStep,
                                                        colorSpace, kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault);
        CGContextDrawImage(contextRef, CGRectMake(0, 0, image.size.width, image.size.height), imageRef);
        CGContextRelease(contextRef);
        CGColorSpaceRelease(colorSpace);
        
        IplImage *ret = cvCreateImage(cvGetSize(iplimage), IPL_DEPTH_8U, 3);
        cvCvtColor(iplimage, ret, CV_BGRA2BGR);
        cvReleaseImage(&iplimage);
        
        return ret;
    
}

+(UIImage *)UIImageFromIplImage:(IplImage *)image {
    DDLogInfo(@"IplImage (%d, %d) %d bits by %d channels, %d bytes/row %s", image->width, image->height, image->depth, image->nChannels, image->widthStep, image->channelSeq);
    
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	NSData *data = [NSData dataWithBytes:image->imageData length:image->imageSize];
	CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
	CGImageRef imageRef = CGImageCreate(image->width, image->height,
										image->depth, image->depth * image->nChannels, image->widthStep,
										colorSpace, kCGImageAlphaNone|kCGBitmapByteOrderDefault,
										provider, NULL, false, kCGRenderingIntentDefault);
	UIImage *ret = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(colorSpace);
	return ret;
}
 */

+ (UIImage *)rotateImage:(UIImage *)img byOrientationFlag:(UIImageOrientation)orient {
    CGImageRef imgRef = img.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    CGSize imageSize = bounds.size;
    CGFloat boundHeight;

    switch (orient) {

        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;

        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;

        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;

        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;

        default:
            // image is not auto-rotated by the photo picker, so whatever the user
            // sees is what they expect to get. No modification necessary
            transform = CGAffineTransformIdentity;
            break;

    }

    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    if ((orient == UIImageOrientationDown) || (orient == UIImageOrientationRight) || (orient == UIImageOrientationUp)) {
        // flip the coordinate space upside down
        CGContextScaleCTM(context, 1, -1);
        CGContextTranslateCTM(context, 0, -height);
    }

    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return imageCopy;
}

+ (NSDictionary *)kpCropImage:(UIImage *)image withPoints:(NSArray *)points {
    image = [[UIImage alloc] initWithCGImage:image.CGImage scale:1 orientation:image.imageOrientation];
    //[self saveImage:image toFileName:@"test1"];
    //DDLogInfo(@"***algo started:%d", image.imageOrientation);
    //IplImage *inp = [self CreateIplImageFromUIImage:image];
    cv::Mat inp = [self cvMatWithImage:image];

    CvPoint2D32f Q[4];
    //CvSize mySize = cvSize(640, 480);

    if (points == nil || [points count] < 4) {
        DDLogInfo(@"***Best Quad Started");
        int errCode = BestQuad(inp, Q, 1);
        DDLogInfo(@"***Best Quad Finished(Error Code:%d)", errCode);
        if (errCode != 0) {
            return nil;
        }

    }
    else {
        for (int i = 0; i < 4; i++) {
            CGPoint p = [[points objectAtIndex:i] CGPointValue];
            Q[i].x = p.x;
            Q[i].y = p.y;
        }
    }

    int decFactor = 1;
    DDLogInfo(@"***Rect Quad Started");
    cv::Mat result = RectQuad(inp, Q, decFactor);
    DDLogInfo(@"***Rect Quad Finished");


    NSMutableArray *newPoints = [[NSMutableArray alloc] init];
    for (int i = 0; i < 4; i++) {
        [newPoints addObject:[NSValue valueWithCGPoint:CGPointMake(Q[i].x, Q[i].y)]];
    }

    UIImage *resultImage = [self imageWithCVMat:result];

    /*
    NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], [NSString stringWithFormat:@"test2.jpg"]];
    NSData* imageData = UIImageJPEGRepresentation(resultImage, 0.8);
    [imageData writeToFile:tmpPath atomically:NO];
      */
    return [NSDictionary dictionaryWithObjectsAndKeys:resultImage, @"image",
                                                      [self kpFixPoints:newPoints], @"points",
                    nil];

}

+ (NSDictionary *)kpGetImageBestRect:(UIImage *)image {
    image = [[UIImage alloc] initWithCGImage:image.CGImage scale:1 orientation:UIImageOrientationUp];
    cv::Mat inp = [self cvMatWithImage:image];

    CvPoint2D32f Q[4];
    int errCode = BestQuad(inp, Q, 0);
    DDLogInfo(@"***Best Quad Finished(Error Code:%d)", errCode);
    if (errCode != 0) {
        return nil;
    }

    NSMutableArray *newPoints = [[NSMutableArray alloc] init];
    for (int i = 0; i < 4; i++) {
        [newPoints addObject:[NSValue valueWithCGPoint:CGPointMake(Q[i].x, Q[i].y)]];
    }

    //cvReleaseImage(&inp);

    return [NSDictionary dictionaryWithObjectsAndKeys:
            [self kpFixPoints:newPoints], @"points",
                    nil];

}

+ (NSArray *)kpFixPoints:(NSArray *)points {

    CGPoint tl = [[points objectAtIndex:0] CGPointValue];
    CGPoint bl = [[points objectAtIndex:1] CGPointValue];
    CGPoint br = [[points objectAtIndex:2] CGPointValue];
    CGPoint tr = [[points objectAtIndex:3] CGPointValue];

    CGPoint p;

    if (tl.x > tr.x) {
        p = tl;
        tl = tr;
        tr = p;
    }

    if (bl.x > br.x) {
        p = bl;
        bl = br;
        br = p;
    }

    if (tl.y > bl.y) {
        p = tl;
        tl = bl;
        bl = p;
    }

    if (tr.y > br.y) {
        p = tr;
        tr = br;
        br = p;
    }


    return [NSArray arrayWithObjects:[NSValue valueWithCGPoint:tl],
                                     [NSValue valueWithCGPoint:bl],
                                     [NSValue valueWithCGPoint:br],
                                     [NSValue valueWithCGPoint:tr],
                    nil];
}


+ (UIImage *)kpEnhanceImage:(UIImage *)image withSat:(float)sat andCon:(float)con {
    //[self saveImage:image toFileName:@"1"];
    cv::Mat inp = [self cvMatWithImage:image];
    cv::Mat result;

    if (sat > 0) {
        result = myDDS(inp, sat);
        DDLogInfo(@"***AutoColor1 Finished:%2f", sat);

        if (con > 0) {
            double gmax;
            myContrastInit(result, &gmax);
            result = myContrastStretch(result, con);
            DDLogInfo(@"***AutoColor2 Finished:%2f", con);
        }
        //cvReleaseImage(&inp);
    }
    else if (con > 0) {
        double gmax;
        myContrastInit(inp, &gmax);
        result = myContrastStretch(inp, con);
        DDLogInfo(@"***AutoColor2 Finished:%2f", con);
        //cvReleaseImage(&inp);
    }
    else {
        result = inp;
    }


    UIImage *resultImage = [self imageWithCVMat:result];

    //[self saveImage:resultImage toFileName:@"2"];


    return resultImage;
}


+ (void)saveImage:(UIImage *)image toFileName:(NSString *)fileName {
    NSString *tmpPath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], [NSString stringWithFormat:@"%@.png", fileName]];
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:tmpPath atomically:NO];
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    CGImageRef imageRef;
    CGFloat width;
    CGFloat height;
    CGAffineTransform transform;
    CGRect bounds;
    CGFloat ratio;
    CGFloat scaleRatio;
    CGSize imageSize;
    CGFloat boundHeight;
    UIImageOrientation orientation;
    CGContextRef context;
    UIImage *newImage;

    float DETECT_IMAGE_MAX_SIZE = 5000;

    imageRef = image.CGImage;
    width = CGImageGetWidth(imageRef);
    height = CGImageGetHeight(imageRef);
    bounds = CGRectMake(0, 0, width, height);

    if (width > DETECT_IMAGE_MAX_SIZE || height > DETECT_IMAGE_MAX_SIZE) {
        ratio = width / height;

        if (ratio > 1) {
            bounds.size.width = DETECT_IMAGE_MAX_SIZE;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.width = bounds.size.height * ratio;
            bounds.size.height = DETECT_IMAGE_MAX_SIZE;
        }
    }

    scaleRatio = bounds.size.width / width;
    imageSize = CGSizeMake(width, height);
    orientation = image.imageOrientation;

    switch (orientation) {
        case UIImageOrientationUp:

            transform = CGAffineTransformIdentity;

            break;

        case UIImageOrientationDown:

            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, (CGFloat) M_PI);

            break;

        case UIImageOrientationLeft:

            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation((CGFloat) 0, imageSize.width);
            transform = CGAffineTransformRotate(transform, (CGFloat) (3 * M_PI / 2));

            break;

        case UIImageOrientationRight:

            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, (CGFloat) 0);
            transform = CGAffineTransformRotate(transform, (CGFloat) (M_PI / 2));

            break;

        case UIImageOrientationUpMirrored:

            transform = CGAffineTransformMakeTranslation(imageSize.width, (CGFloat) 0);
            transform = CGAffineTransformScale(transform, (CGFloat) -1, (CGFloat) 1);

            break;

        case UIImageOrientationDownMirrored:

            transform = CGAffineTransformMakeTranslation((CGFloat) 0, imageSize.height);
            transform = CGAffineTransformScale(transform, (CGFloat) 1, (CGFloat) -1);

            break;

        case UIImageOrientationLeftMirrored:

            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, (CGFloat) -1, (CGFloat) 1);
            transform = CGAffineTransformRotate(transform, (CGFloat) (3 * M_PI / 2));

            break;

        case UIImageOrientationRightMirrored:

            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale((CGFloat) -1, (CGFloat) 1);
            transform = CGAffineTransformRotate(transform, (CGFloat) (M_PI / 2));

            break;

        default:

            transform = CGAffineTransformIdentity;

            break;
    }

    UIGraphicsBeginImageContext(bounds.size);

    context = UIGraphicsGetCurrentContext();

    if (orientation == UIImageOrientationRight || orientation == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }

    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imageRef);

    newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return newImage;
}

/*
+(CGRect)openCvFaceDetect:(UIImage*)targetImage
{
    cvSetErrMode(CV_ErrModeParent);
    
    IplImage *image = [self CreateIplImageFromUIImage:targetImage];
    
    // Scaling down
    IplImage *small_image = cvCreateImage(cvSize(image->width/2,image->height/2), IPL_DEPTH_8U, 3);
    cvPyrDown(image, small_image, CV_GAUSSIAN_5x5);
    int scale = 2;
    
    // Load XML
    NSString *path = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_default" ofType:@"xml"];
    CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad([path cStringUsingEncoding:NSASCIIStringEncoding], NULL, NULL, NULL);
    CvMemStorage* storage = cvCreateMemStorage(0);
    
    // Detect faces and draw rectangle on them
    CvSeq* faces = cvHaarDetectObjects(small_image, cascade, storage, 1.2f, 2, CV_HAAR_DO_CANNY_PRUNING, cvSize(0,0), cvSize(20, 20));
    cvReleaseImage(&small_image);
    
    for(int i = 0; i < faces->total; i++) {
        DDLogInfo(@"HERE");
        /
        // Calc the rect of faces
        CvRect cvrect = *(CvRect*)cvGetSeqElem(faces, i);
        CGRect face_rect = CGContextConvertRectToDeviceSpace(contextRef, CGRectMake(cvrect.x * scale, cvrect.y * scale, cvrect.width * scale, cvrect.height * scale));
        
        if(overlayImage) {
            CGContextDrawImage(contextRef, face_rect, overlayImage.CGImage);
        } else {
            CGContextStrokeRect(contextRef, face_rect);
        }/
        
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef contextRef = CGBitmapContextCreate(NULL, targetImage.size.width, targetImage.size.height,
                                                    8, targetImage.size.width * 4,
                                                    colorSpace, kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault);
    CGContextDrawImage(contextRef, CGRectMake(0, 0, targetImage.size.width, targetImage.size.height), targetImage.CGImage);
    CGRect face_rect = CGRectMake(0, 0, 0, 0);
    if (faces->total > 0)
    {
        CvRect cvrect = *(CvRect*)cvGetSeqElem(faces, 0);
        face_rect = CGContextConvertRectToDeviceSpace(contextRef, CGRectMake(cvrect.x * scale, cvrect.y * scale, cvrect.width * scale, cvrect.height * scale));
    }
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(contextRef);
    cvReleaseMemStorage(&storage);
    cvReleaseHaarClassifierCascade(&cascade);    
    
    return face_rect;
    
    

}
 */

+ (CGPoint)findCorner:(UIImage *)image atPoint:(CGPoint)point {
    /*
    IplImage *inp = [self CreateIplImageFromUIImage:image];
    int x = cvRound(point.x);
    int y = cvRound(point.y);

    int result = FixCorner(inp, &x, &y, 2);
    if (result == QUAD_FixFailed)
    {
        x = -1;
        y = -1;
    }
    
    cvReleaseImage(&inp);
    
    return CGPointMake(x, y);*/

    return CGPointMake(0, 0);
}

+ (double)getMaxContrastGain:(UIImage *)image {
    cv::Mat inp = [self cvMatWithImage:image];

    double gmax;
    //int gLow, gHgh;
    myContrastInit(inp, &gmax);//, &gLow, &gHgh);
    //cvReleaseImage(&inp);

    return gmax;
}

+ (UIImage *)imageWithCVMat:(const cv::Mat &)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize() * cvMat.total()];

    CGColorSpaceRef colorSpace;

    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }

    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef) data);

    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                     // Width
            cvMat.rows,                                     // Height
            8,                                              // Bits per component
            8 * cvMat.elemSize(),                           // Bits per pixel
            cvMat.step[0],                                  // Bytes per row
            colorSpace,                                     // Colorspace
            kCGImageAlphaNone | kCGBitmapByteOrderDefault,  // Bitmap info flags
            provider,                                       // CGDataProviderRef
            NULL,                                           // Decode
            false,                                          // Should interpolate
            kCGRenderingIntentDefault);                     // Intent

    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);

    return image;
}

+ (cv::Mat)cvMatWithImage:(UIImage *)image {
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;

    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels

    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to backing data
            cols,                       // Width of bitmap
            rows,                       // Height of bitmap
            8,                          // Bits per component
            cvMat.step[0],              // Bytes per row
            colorSpace,                 // Colorspace
            kCGImageAlphaNoneSkipLast |
                    kCGBitmapByteOrderDefault); // Bitmap info flags

    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);

    return cvMat;
}

@end
