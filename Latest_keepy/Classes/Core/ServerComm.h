//
//  ServerComm.h
//  
//
//  Created by Yaniv Solnik on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Asset+KP.h"
#import "Item+KP.h"
#import "Kid.h"
#import "Place.h"

@class KPLocalItem;

typedef void (^RequestSuccessBlock)(NSDictionary *data);

typedef void (^RequestFailBlock)(NSError *error);

typedef void (^APISuccessBlock)(id result);

typedef void (^APIFailBlock)(NSError *error);

typedef void (^RequestProgressBlock)(float progress);

typedef void (^UploadProgressBlock)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite);

@interface ServerComm : NSObject {
    RequestSuccessBlock requestSuccessBlock;
    RequestFailBlock requestFailBlock;
    RequestProgressBlock requestProgressBlock;
    //UserUpdateBlock userUpdateBlock;
    //UserUpdateFailBlock userUpdateFailBlock;
}

@property(nonatomic, strong) NSOperation *activeOperation;

+ (instancetype)instance;

- (void)sendRequest:(NSString *)apiMethod withParams:(NSDictionary *)params openMethod:(BOOL)isOpenMethod onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock;

- (void)sendRequest:(NSString *)apiMethod withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock;

- (void)sendRequest:(NSString *)apiMethod WithParameters:(NSDictionary *)params withRequestType:(NSString *)httpType onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock;

- (void)uploadFiles:(NSString *)apiMethod withFilesData:(NSArray *)filesData onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock;

- (void)uploadFiles:(NSString *)apiMethod withFilesData:(NSArray *)filesData withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock;


- (void)uploadFiles:(NSString *)apiMethod withFilesData:(NSArray *)filesData withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock
         onProgress:(UploadProgressBlock)aProgressBlock onHandler:(void (^)(void))handler;

- (void)uploadFile:(NSString *)apiMethod withFileData:(NSDictionary *)fileData withParams:(NSDictionary *)params onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock onHandler:(void (^)(void))handler;

- (void)sendRequest:(NSString *)apiMethod withParams:(NSDictionary *)params openMethod:(BOOL)isOpenMethod onSuccess:(RequestSuccessBlock)aSuccessBlock onFail:(RequestFailBlock)aFailBlock withHttpMethod:(NSString *)httpMethod onHandler:(void (^)(void))handler;

- (void)cancelOperation;

- (void)registerUser:(NSString *)email withName:(NSString *)name andPassword:(NSString *)password andParentType:(NSInteger)parentType andOtherTxt:(NSString *)otherTxt andFBId:(NSString *)fbId andFBAccessToken:(NSString *)fbAccessToken withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)login:(NSString *)email withPassword:(NSString *)password withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)fblogin:(NSString *)facebookAccessToken withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateDeviceToken:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateFacebookAdsId:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateRegistrationSignature:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateFacebookTokens:(NSString *)facebookId andFacebookAccessToken:(NSString *)facebookAccessToken andHasPublishPermissions:(BOOL)hasPublishPermissions andSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateTwitterTokens:(NSString*)twitterId token:(NSString *)token secret:(NSString *)secret handle:(NSString *)handle andSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateDropboxCredentials:(NSString *)dbStr withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)forgotPassword:(NSString *)email withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateUser:(NSString *)email withName:(NSString *)name andPassword:(NSString *)password andParentType:(NSInteger)parentType andOtherTxt:(NSString *)otherTxt andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)syncDropbox:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)verifyAppStoreReceipt:(NSString *)receipt
                    productId:(NSString *)productId
                     planType:(NSString *)planType
                   couponCode:(NSString *)couponCode
                   completion:(void(^)(NSDictionary* response, NSError* error))completion;


- (void)addKid:(NSString *)name withGender:(NSInteger)gender andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateKid:(NSInteger)kidId withName:(NSString *)name andGender:(NSInteger)gender andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)uploadKidPhoto:(NSInteger)kidId withImage:(UIImage *)image withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)deleteKid:(NSInteger)kidId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)stopFollowKid:(NSInteger)kidId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)addContactList:(NSArray *)contacts withCampaignId:(NSString *)campaignId withSuccessBlock:(APISuccessBlock)aSuccessBlock
          andFailBlock:(APIFailBlock)aFailBlock;
- (void)getTheProductImageWithParameters:(NSDictionary *)params withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;
- (void)getThePriceOfTheProducts:(NSString *)productID withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;
- (void)getTheProductTempleate:(NSString *)productSKU withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

//Items
- (void)addItem:(NSString *)title
      selectedKids:(NSSet *)selectedKids
           andDate:(NSDate *)date
          andPlace:(NSUInteger)placeId
       andCropRect:(NSString *)cropRect
andOriginalAssetId:(NSUInteger)assetId
       andRotation:(float)rotation
            andSat:(float)sat andCon:(float)con
            andBri:(float)bri
  andAutoEnhanceOn:(BOOL)autoEnhanceOn
       andItemType:(NSUInteger)itemType
      andExtraData:(NSString *)extraData
  andShareWithFans:(BOOL)shareWithFans
           andTags:(NSArray *)tags
    andExtraParams:(NSMutableDictionary *)extraParams
  withSuccessBlock:(APISuccessBlock)aSuccessBlock
      andFailBlock:(APIFailBlock)aFailBlock;

- (void)addItem:(NSString *)title
   selectedKids:(NSSet *)selectedKids
        andDate:(NSDate *)date
       andPlace:(NSUInteger)placeId
    andCropRect:(NSString *)cropRect
andOriginalImageHash:(NSString *)originalImageHash
andOriginalAssetId:(NSUInteger)assetId
    andRotation:(float)rotation
         andSat:(float)sat andCon:(float)con
         andBri:(float)bri
andAutoEnhanceOn:(BOOL)autoEnhanceOn
    andItemType:(NSUInteger)itemType
   andExtraData:(NSString *)extraData
andShareWithFans:(BOOL)shareWithFans
        andTags:(NSArray *)tags
 andExtraParams:(NSMutableDictionary *)extraParams
withSuccessBlock:(APISuccessBlock)aSuccessBlock
   andFailBlock:(APIFailBlock)aFailBlock;

- (void)addVideo:(NSUInteger)kidId
       withTitle:(NSString *)title
    selectedKids:(NSSet *)selectedKids
         andDate:(NSDate *)date
        andPlace:(NSUInteger)placeId
     andCropRect:(NSString *)cropRect
 andVideoAssetId:(NSInteger)videoId
 andThumpAssetId:(NSInteger)thumbId
andAutoEnhanceOn:(BOOL)autoEnhanceOn
     andItemType:(NSInteger)itemType
    andExtraData:(NSString *)extraData
andShareWithFans:(BOOL)shareWithFans
         andTags:(NSArray *)tags
     andDuration:(NSString *)duration
withSuccessBlock:(APISuccessBlock)aSuccessBlock
    andFailBlock:(APIFailBlock)aFailBlock;

- (void)uploadAsset:(NSString *)assetHash
       andMediaType:(NSInteger)mediaType
           withData:(NSData *)data
          andPolicy:(NSString *)policy
       andSignature:(NSString *)signature
   withSuccessBlock:(APISuccessBlock)aSuccessBlock
       andFailBlock:(APIFailBlock)aFailBlock
         onProgress:(UploadProgressBlock)aProgressBlock
          onHandler:(void (^)(void))handler;


- (void)uploadVideoAsset:(NSString *)assetHash withData:(NSData *)data andPolicy:(NSString *)policy andSignature:(NSString *)signature withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock onHandler:(void (^)(void))handler;

- (void)updateItem:(KPLocalItem *)item imageChanged:(BOOL)imageChanged withTags:(NSArray *)tags andExtraParams:(NSMutableDictionary *)extraParams withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)uploadItemFiles:(NSInteger)itemId withImage:(UIImage *)image andOriginalImage:(UIImage *)originalImage withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)uploadStory:(NSUInteger)itemId withStoryLength:(NSInteger)storyLength withData:(NSData *)data withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock andProgressBlock:(UploadProgressBlock)aProgressBlock;

- (void)uploadItemComment:(NSUInteger)itemId withVideo:(NSData *)videoData andPreviewImage:(UIImage *)previewImage andCommentLength:(NSInteger)commentLength withType:(NSInteger)commentType isStory:(BOOL)isStory withExtraData:(NSString *)extraData withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock andProgressBlock:(UploadProgressBlock)progressBlock;

- (void)deleteComment:(NSInteger)commentId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)markItemRead:(NSInteger)itemId andReadAction:(NSInteger)readAction withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)markCommentRead:(NSInteger)itemId andCommentId:(NSInteger)commentId andReadAction:(NSInteger)readAction withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)deleteItem:(NSInteger)itemId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)shareItem:(NSInteger)itemId andMessage:(NSString *)message andFacebook:(NSInteger)facebook andTwitter:(NSInteger)twitter andKeepyFBShare:(BOOL)keepyFBShare withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)getFanByEmail:(NSString *)email successBlock:(APISuccessBlock)aSuccessBlock failBlock:(APIFailBlock)aFailBlock;

- (void)getVideoAssetStatus:(NSString *)videoAssetID successBlock:(APISuccessBlock)aSuccessBlock failBlock:(APIFailBlock)aFailBlock;


- (void)getItems:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)getItems:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock andForceItemsIds:(NSString *)forceItemsIds;

- (void)getKids:(BOOL)myOnly withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)deleteStory:(NSInteger)storyId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)prepareAsset:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)prepareVideoAsset:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)prepareAsset:(NSInteger)assetType andMediaType:(NSInteger)mediaType withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)uploadAsset:(NSString *)assetHash withData:(NSData *)data andPolicy:(NSString *)policy andSignature:(NSString *)signature withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock;

//-(void) uploadAsset:(NSString*)assetHash andMediaType:(int)mediaType withData:(NSData*)data andPolicy:(NSString*)policy andSignature:(NSString*)signature withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onProgress:(UploadProgressBlock)aProgressBlock;
- (void)commitAsset:(NSUInteger)assetId andFileSize:(long)fileSize withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock onHandler:(void (^)(void))handler;

- (void)getAssetPolicy:(long)assetId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;


//-(NSURL*) assetURL:(Asset*)asset;
- (void)downloadAssetFile:(Asset *)asset withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)saveAssetData:(NSData *)data withFileExt:(NSString *)fileExt andAssetHash:(NSString *)assetHash;

- (void)addPlace:(NSString *)title withLon:(double)lon andLat:(double)lat withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)editPlace:(NSUInteger)placeId withTitle:(NSString *)title andLon:(double)lon andLat:(double)lat withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)deletePlace:(NSUInteger)placeId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)getPlaces:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)addFan:(NSString *)email withName:(NSString *)name andRelationType:(NSInteger)relationType andFacebookId:(NSString *)facebookId andFacebookRequestId:(NSString *)facebookRequestId andBirthdate:(NSDate *)birthdate withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)updateFan:(Fan *)fan andFacebookRequestId:(NSString *)facebookRequestId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)deleteFan:(NSInteger)fanId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)reinviteFan:(NSInteger)fanId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)getFans:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)respondToFanRequest:(NSInteger)fanRequestId andAccept:(BOOL)doAccept withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;


- (void)getNotifications:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)markNotificationRead:(NSInteger)notificationId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)deleteNotification:(NSInteger)notificationId withSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)getUserInfo:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)getPrivateLink:(KPLocalItem *)item aSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)addFacebookInviteRequest:(NSString *)requestId withFBIds:(NSString *)fbIds andSuccessBlock:(APISuccessBlock)aSuccessBlock andFailBlock:(APIFailBlock)aFailBlock;

- (void)activator:(NSString *)activatorId withSuccessBlock:(APISuccessBlock)aSuccessBlock
     andFailBlock:(APIFailBlock)aFailBlock;

- (void)addCollection:(NSString*)title
                 kids:(NSArray<NSNumber*>*)kids
       collectionDate:(NSDate*)date
                 tags:(NSArray<NSNumber*>*)tags
                items:(NSArray<NSNumber*>*)items
                place:(NSUInteger)placeId
          shareStatus:(BOOL)shareStatus
           completion:(void(^)(NSDictionary* response, NSError* error))completion;

- (void)shareCollection:(NSInteger)collectionId
                message:(NSString*)message
               facebook:(BOOL)facebook
                twitter:(BOOL)twitter
                  keepy:(BOOL)keepy
             completion:(void(^)(NSDictionary* response, NSError* error))completion;

- (void)getCollection:(NSInteger)collectionId
           completion:(void(^)(NSDictionary* response, NSError* error))completion;

@end

@interface UIImageView (Keepy)

- (void)setImageWithAsset:(Asset *)asset;

@end
