//
//  KPDataParser.m
//  Keepy
//
//  Created by Yaniv Solnik on 1/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>


#import "KPDataParser.h"
#import "KPLocalKid.h"
#import "KPLocalKidItem.h"
#import "KPLocalItem.h"
#import "KPLocalPlace.h"
#import "KPLocalAsset.h"
#import "KPLocalStory.h"
#import "KPLocalComment.h"
#import "KPSQLiteManager.h"
#import "KPLocalTag.h"
#import "KPLocalItemTag.h"

@interface KPDataParser()

@property (strong, nonatomic) NSMutableDictionary *cachedKids;
@property (strong, nonatomic) NSMutableDictionary *cachedItems;
@property (strong, nonatomic) NSMutableDictionary *cachedAssets;

@property int modifiedKidCount;

@property BOOL isCurrentlyParsingKids;

@end

@implementation KPDataParser

static NSMutableDictionary *multitaggedPhotos;

+ (instancetype)instance {
    
    // return [self new];
    
    static KPDataParser *sharedParser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedParser = [[self alloc] init];
    });
    return sharedParser;
    
}

- (void)parseKids:(NSArray *)kids complete:(KPCompleteBlock)complete {

    // NSData *jsonData = [NSJSONSerialization dataWithJSONObject:kids options:NSJSONWritingPrettyPrinted error:nil];
    // NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    
    
    // return;
    
    if(self.isCurrentlyParsingKids){
        return;
    }
    
    
    if (kids == nil) {
        return;
    }


    /*
    if (![[UserManager sharedInstance] isAfterLogin]){
        
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];
            complete();
        }];
        
        return;
    }*/

    multitaggedPhotos = @{}.mutableCopy;





    // User *me = [[UserManager sharedInstance] getMe];



    @synchronized (self) {
        
        self.isCurrentlyParsingKids = YES;
            
        dispatch_async(dispatch_queue_create(nil, nil), ^{
            

            self.modifiedKidCount = 0;
            
            
            [[KPSQLiteManager getManager].dbQueue inDatabase:^(FMDatabase *db) {
                [db beginTransaction];
            }];
             

            // prepare the cache!
            // may the cache be ever in your favor!
            // [self cacheObjects];
            
            CGFloat finishedKids = 0;

            BOOL isAfterLogin = [[UserManager sharedInstance] isAfterLogin];
            
            for (NSDictionary *kidData in kids) {

                CFAbsoluteTime beforeFMDB = CFAbsoluteTimeGetCurrent();

                // FMDB
                KPLocalKid *localKid = [self initializeKid:kidData];

                CFAbsoluteTime afterFMDB = CFAbsoluteTimeGetCurrent();
                CGFloat fmdbDuration = afterFMDB - beforeFMDB;

                // FMDB
                NSLog(@"Initialized kid with ID %lu and local mapping %lu (time: %f)", (unsigned long)localKid.serverID, (unsigned long)localKid.identifier, fmdbDuration);
                
                ++finishedKids;

                if(isAfterLogin){
                    dispatch_async(dispatch_get_main_queue(), ^{
                       [SVProgressHUD showProgress:(finishedKids/kids.count) status:@"loading kids' data"];
                    });
                }

                // testing whether this annoying popup appears after the first kid is parsed
                // return;

                /*
                CFAbsoluteTime beforeCoreData = CFAbsoluteTimeGetCurrent();
                Kid *kid = [self createKid:kidData];
                
                
                if (![me.kids containsObject:kid]){
                    [me addKidsObject:kid];
                }
                
                
                CFAbsoluteTime afterCoreData = CFAbsoluteTimeGetCurrent();
                CGFloat coreDataDuration = afterCoreData - beforeCoreData;

                NSLog(@"Initialized CoreData kid with ID %i (time: %f)", kid.kidId.intValue, coreDataDuration);
                */



                /* [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                 // we need this before we proceed with the next kid in order to save stuff before we do multitagging
                 }]; */

            }

            
            [[KPSQLiteManager getManager].dbQueue inDatabase:^(FMDatabase *db) {
                [db commit];
            }];
             
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(self.modifiedKidCount > 0){
                    [SVProgressHUD showWithStatus:@"processing updates"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];
                }
                complete();
                
            });
            
            self.isCurrentlyParsingKids = NO;
                    
        });

    }

        

        
        
        












    /*
    
    ///Add the kids to items
    NSArray *myKids = [[UserManager sharedInstance] myKids];
    NSArray *items = [Item MR_findAll];
    NSMutableArray *kidList = [NSMutableArray new];
    NSMutableArray *objectstoremove = [NSMutableArray new];

    
    ///IF THIS GET ALL DATA FROM SERVER
    for (Item *item in items)
    {
        
        [kidList removeAllObjects];
        [objectstoremove removeAllObjects];
        
        NSArray *kidsIds = [item.kidsIds componentsSeparatedByString:@"|"];
        
        if ([kidsIds count] > 1 && item.kids.count != kidsIds.count)
        {
            
            for (NSString *kidId in kidsIds)
            {
                
                
                for (Kid *kid in myKids)
                {
                    if (kid.kidId.intValue == kidId.intValue)
                    {
                        [kidList addObject:kid];
                    }
                }
                
            }
        
            
            NSSet *selectedKidsSet = [NSSet setWithArray:kidList];
            
            item.kids = selectedKidsSet;
            
            
        }
        
        
    }*/


    /*
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_REFRESHED_NOTIFICATION object:nil];
        complete();
    }];
    */

}



- (void)cacheObjects{
    
    NSArray *kids = [KPLocalKid fetchAll];
    NSArray *items = [KPLocalItem fetchAll];
    NSArray *assets = [KPLocalAsset fetchAll];
    
    self.cachedKids = @{}.mutableCopy;
    for(KPLocalKid *currentKid in kids){
        self.cachedKids[@(currentKid.serverID)] = currentKid;
    }
    
    self.cachedItems = @{}.mutableCopy;
    for(KPLocalItem *currentItem in items){
        self.cachedItems[@(currentItem.serverID)] = currentItem;
    }
    
    self.cachedAssets = @{}.mutableCopy;
    for(KPLocalAsset *currentAsset in assets){
        self.cachedAssets[@(currentAsset.serverID)] = currentAsset;
    }
    
}



- (KPLocalKid *)initializeKid:(NSDictionary *)kidData {

    NSUInteger serverKidID = [kidData[@"id"] intValue];
    KPLocalKid *localKid = [KPLocalKid fetchByServerID:serverKidID];
    // KPLocalKid *localKid = self.cachedKids[@(serverKidID)];

    BOOL modifiedThisKid = NO;
    
    if(serverKidID == 93){ // the kid whose link with item 377279 seems not to be established
        NSLog(@"here");
    }
    
    NSArray *relatedItemIDs = @[];

    if (!localKid) {
        localKid = [KPLocalKid create];
        localKid.serverID = serverKidID;
        modifiedThisKid = YES;
    } else {
        // this kid had already existed before we fetched it from the server
        // so let's see how many items are currently associated with it
        relatedItemIDs = [KPLocalKidItem fetchItemIDsForKid:localKid];
    }

    // the number of items that have been before
//    NSUInteger previousItemCount = relatedItemIDs.count;


    localKid.name = kidData[@"name"];
    localKid.gender = [kidData[@"gender"] intValue];
    localKid.isMine = [kidData[@"isParent"] boolValue];

    id birthdate = kidData[@"birthdate"];
    if (birthdate && birthdate != [NSNull null]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z";
        NSDate *birthdateObject = [formatter dateFromString:birthdate];
        localKid.birthdate = birthdateObject.timeIntervalSince1970;
    }

    id userRelationType = kidData[@"userRelationType"];
    if (userRelationType && userRelationType != [NSNull null]) {
        localKid.userRelationType = [userRelationType intValue];
    }

    id userNickname = kidData[@"userNickname"];
    if (userNickname && userNickname != [NSNull null]) {
        localKid.userNickname = userNickname;
    }

    id assetID = kidData[@"assetId"];
    id assetHash = kidData[@"hash"];
    if (assetID && assetID != [NSNull null]) {
        KPLocalAsset *localAsset = [self initializeAssetByID:[assetID intValue] withType:0 withHash:assetHash];
        localKid.imageAssetID = localAsset.identifier;
    }


    BOOL addedNewItem = NO;

    id items = kidData[@"items"];
    if (items && items != [NSNull null]) {

        NSArray *itemDictionaries = items;
        for (NSDictionary *currentItemData in itemDictionaries) {
            KPLocalItem *currentItem = [self initializeItem:currentItemData];

            // this item is not yet associated
            if (![relatedItemIDs containsObject:@(currentItem.identifier)]) {
                KPLocalKidItem *connection = [KPLocalKidItem create];
                connection.itemID = currentItem.identifier;
                connection.kidID = localKid.identifier;
                [connection saveChanges];

                addedNewItem = YES;
            }

            
            NSArray *itemTagIDs = [KPLocalItemTag fetchTagIDsForItem:currentItem];
            NSLog(@"ItemTagsID = %@",itemTagIDs);
            if ([currentItemData valueForKey:@"tags"] != nil) {
                NSArray * tagsArray = [currentItemData valueForKey:@"tags"];
                for (NSDictionary * dict in tagsArray) {
                    KPLocalTag *tag = [KPLocalTag fetchByServerID:[dict[@"id"] integerValue]];
                    
                    if (tag != nil) {
                        
                        if (![itemTagIDs containsObject:@(tag.identifier)]) {
                            
                            KPLocalItemTag *itemTagRelation = [KPLocalItemTag create];
                            itemTagRelation.itemID = currentItem.identifier;
                            itemTagRelation.tagID = tag.identifier;
                            [itemTagRelation saveChanges];
                        }
                    }
                }
            }
        }
    }

    [localKid saveChanges];

    if (addedNewItem) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_FROM_SERVER_NOTIFICATION object:localKid];
        modifiedThisKid = YES;
    }
    
    if(modifiedThisKid){
        self.modifiedKidCount++;
    }

    return localKid;
}

- (Kid *)createKid:(NSDictionary *)kidData {

    // DLog(@"********************** createItem %@", kidData);



    NSUInteger oldNumberOfItems = 0;

    Kid *kid = [Kid MR_findFirstByAttribute:@"kidId" withValue:[kidData valueForKey:@"id"]];


    if (kid == nil) {
        kid = [Kid MR_createEntity];
    }
    else {
        oldNumberOfItems = [kid.items.allObjects count];
    }


    kid.kidId = @([[kidData valueForKey:@"id"] intValue]);
    kid.name = [kidData valueForKey:@"name"];

    if ([kidData valueForKey:@"birthdate"] != [NSNull null]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z"];
        kid.birthdate = [formatter dateFromString:[kidData valueForKey:@"birthdate"]];
    }

    kid.gender = @([[kidData valueForKey:@"gender"] intValue]);
    kid.isMine = @([[kidData valueForKey:@"isParent"] boolValue]);
    if ([kidData valueForKey:@"assetId"] != [NSNull null]) {
        
        // BROKEN: createAsset is now FMDB-only
        // kid.image = [self createAssetById:[[kidData valueForKey:@"assetId"] intValue] withType:0 withHash:[kidData valueForKey:@"hash"]];
    }

    if ([kidData valueForKey:@"userRelationType"] != [NSNull null]) {
        kid.userRelationType = @([[kidData valueForKey:@"userRelationType"] intValue]);
    }

    if ([kidData valueForKey:@"userNickname"] != [NSNull null]) {
        kid.userNickname = [kidData valueForKey:@"userNickname"];
    }


    if ([kidData valueForKey:@"items"]) {
        for (NSDictionary *itemData in (NSArray *) [kidData valueForKey:@"items"]) {
            Item *item = [self createItem:itemData];
            if (![kid.items containsObject:item]) {
                [kid addItemsObject:item];
            }
            // [item addKidsObject:kid];

            // DDLogInfo(@"%@",item.kids);
        }
    }


    if (oldNumberOfItems != [kid.items.allObjects count]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_FROM_SERVER_NOTIFICATION object:kid];
    }


    return kid;
}

- (KPLocalItem *)initializeItem:(NSDictionary *)itemData {

    NSUInteger serverItemID = [itemData[@"id"] intValue];
    
    if(serverItemID == 946139){ // this is the infamous quote
        NSLog(@"here");
    }
    if(serverItemID == 942991){ // causes reload in notification center
        NSLog(@"here");
    }
    if(serverItemID == 377279){ // appears to have some linking issues, in that it is relinked and triggers an item added notification
        NSLog(@"here");
    }
    
    KPLocalItem *localItem = [KPLocalItem fetchByServerID:serverItemID];
    // KPLocalItem *localItem = self.cachedItems[@(serverItemID)];

    if (!localItem) {
        localItem = [KPLocalItem create];
        localItem.serverID = serverItemID;
    }

//    NSString *kidsIDs = itemData[@"kidsIds"];
//    BOOL isAfterLogin = [[UserManager sharedInstance] isAfterLogin];
//    BOOL isMultitagged = ([kidsIDs rangeOfString:@"|"].location != NSNotFound);

    id title = itemData[@"title"];
    if (title && title != [NSNull null]) {
        localItem.title = title;
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    id itemDate = itemData[@"itemDate"];
    if (itemDate && itemDate != [NSNull null]) {
        localItem.itemDate = [formatter dateFromString:itemDate].timeIntervalSince1970;
    }

    id createDate = itemData[@"createDate"];
    if (createDate && createDate != [NSNull null]) {
        localItem.createDate = [formatter dateFromString:createDate].timeIntervalSince1970;
    }

    id updateDate = itemData[@"updateDate"];
    if (updateDate && updateDate != [NSNull null]) {
        localItem.updateDate = [formatter dateFromString:updateDate].timeIntervalSince1970;
    }

    id cropRect = itemData[@"cropRect"];
    if (cropRect && cropRect != [NSNull null]) {
        localItem.cropRect = cropRect;
    }

    id shareStatus = itemData[@"shareStatus"];
    if (shareStatus && shareStatus != [NSNull null]) {
        localItem.shareStatus = [shareStatus intValue];
    }

    // image properties
    localItem.rotation = [itemData[@"rotation"] doubleValue];
    localItem.saturation = [itemData[@"sat"] doubleValue];
    localItem.contrast = [itemData[@"con"] doubleValue];
    localItem.itemType = [itemData[@"itemType"] intValue];

    id brightness = itemData[@"bri"];
    if (brightness && brightness != [NSNull null]) {
        localItem.brightness = [itemData[@"brightness"] doubleValue];
    }

    id readAction = itemData[@"rid"];
    if (readAction && readAction != [NSNull null]) {
        localItem.readAction = [readAction intValue];
    }

    id imageID = itemData[@"imageId"];
    id imageHash = itemData[@"imageHash"];
    id itemType = itemData[@"itemType"];
    id videoAssetID = itemData[@"sdVideoAssetId"];
    id originalImageID = itemData[@"originalImageId"];

    KPLocalAsset *localImageAsset = [self initializeAssetByID:[imageID intValue] withType:0 withHash:imageHash];
    localItem.imageAssetID = localImageAsset.identifier;

    KPLocalAsset *originalImageAsset;
    if ([itemType intValue] == 4) {
        id videoAssetHash = itemData[@"sdVideoAssetHash"];
        originalImageAsset = [self initializeAssetByID:[videoAssetID intValue] withType:0 withHash:videoAssetHash];
    } else {
        id originalImageHash = itemData[@"originalImageHash"];
        originalImageAsset = [self initializeAssetByID:[originalImageID intValue] withType:0 withHash:originalImageHash];
    }
    localItem.originalImageAssetID = originalImageAsset.identifier;

    id storyID = itemData[@"storyId"];
    id storyLength = itemData[@"storyLength"];
    id assetID = itemData[@"storyAssetId"];
    id assetHash = itemData[@"storyAssetHash"];

    if (storyID && storyID != [NSNull null]) {
        KPLocalStory *localStory = [self initializeStory:[storyID intValue] withAssetID:[assetID intValue] andAssetType:2 andStoryLength:[storyLength intValue] andAssetHash:assetHash];
        localItem.storyID = localStory.identifier;
    }

    BOOL createdComment = NO;

    id comments = itemData[@"comments"];
    if (comments && comments != [NSNull null]) {

        NSArray *commentDictionaries = comments;

        for (NSDictionary *currentCommentData in commentDictionaries) {

            NSUInteger serverCommentID = [currentCommentData[@"id"] intValue];
            KPLocalComment *localComment = [KPLocalComment fetchByServerID:serverCommentID];

            if (!localComment) {
                createdComment = YES;
            }

            localComment = [self initializeComment:currentCommentData];
            localComment.itemID = localItem.identifier;
            [localComment saveChanges];
        }

    }

    id placeID = itemData[@"placeId"];
    if (placeID && placeID != [NSNull null]) {

        NSUInteger serverPlaceID = [placeID intValue];
        KPLocalPlace *localPlace = [KPLocalPlace fetchByServerID:serverPlaceID];

        if (!localPlace) {
            localPlace = [KPLocalPlace create];
            localPlace.serverID = serverPlaceID;
            [localPlace saveChanges];
        }

        localItem.placeID = localPlace.identifier;

    }

    id extraData = itemData[@"extraData"];
    if (extraData && extraData != [NSNull null]) {
        localItem.extraData = extraData;
    }

    [localItem saveChanges];
    
    
    if ([itemData valueForKey:@"tags"] != nil) {
        [self parseTags:[itemData valueForKey:@"tags"]];
    }
    
    
    if (createdComment) { // notify everybody that this item has gotten a new comment
        [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:localItem];
    }

    return localItem;

}

- (Item *)createItem:(NSDictionary *)itemData {
    // DDLogInfo(@"%@",[itemData objectForKey:@"kidsIds"]);
    //DLog(@"********************** createItem %@", itemData);



    int itemID = [itemData[@"id"] intValue];

    if (itemID == 856398) {
        // multitagged Tesla
    }
    if(itemID == 942991){ // item from notification center causing reload
        NSLog(@"loading that item");
    }

    BOOL isNew = NO;

    Item *item = nil;

    NSString *kidsIDs = itemData[@"kidsIds"];
    BOOL isAfterLogin = [[UserManager sharedInstance] isAfterLogin];
    BOOL isMultitagged = ([kidsIDs rangeOfString:@"|"].location != NSNotFound); // this is used as a separator

    if (!isAfterLogin) {
        item = [Item MR_findFirstByAttribute:@"itemId" withValue:[itemData valueForKey:@"id"]];
    } else if (isMultitagged) {

        item = multitaggedPhotos[@(itemID)];
        if (item) { // if it's after the log in, and there's a multitag, we do not want to re-edit all the properties again
            return item;
        }

    }


    // item = [Item MR_findFirstByAttribute:@"itemId" withValue:@(itemID)];

    if (item == nil) {

        item = [Item MR_createEntity];
        isNew = YES;

        if (isMultitagged) {
            multitaggedPhotos[@(itemID)] = item;
        }

    }



    //DDLogInfo(@"Create Item After CreateEntity");

    //DDLogInfo(@"Create Item Started:%d", [[itemData valueForKey:@"id"] intValue]);

    if ([[itemData valueForKey:@"id"] intValue] == 323) {
        DDLogInfo(@"bo");
    }

    item.itemId = @(itemID);

    if ([itemData valueForKey:@"title"] != [NSNull null]) {
        item.title = [itemData valueForKey:@"title"];
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];

    if ([itemData valueForKey:@"itemDate"] != [NSNull null]) {
        item.itemDate = [formatter dateFromString:[itemData valueForKey:@"itemDate"]];
    }

    if ([itemData valueForKey:@"createDate"] != [NSNull null]) {
        item.createDate = [formatter dateFromString:[itemData valueForKey:@"createDate"]];
    }

    if ([itemData valueForKey:@"updateDate"] != [NSNull null]) {
        item.updateDate = [formatter dateFromString:[itemData valueForKey:@"updateDate"]];
    }

    if ([itemData valueForKey:@"cropRect"] != [NSNull null]) {
        item.cropRect = [itemData valueForKey:@"cropRect"];
    }

    if ([itemData valueForKey:@"shareStatus"] != [NSNull null]) {
        item.shareStatus = @([[itemData valueForKey:@"shareStatus"] intValue]);
    }

    // DDLogInfo(@"%d",item.sharestatus.intValue);

    item.rotation = @([[itemData valueForKey:@"rotation"] floatValue]);
    item.sat = @([[itemData valueForKey:@"sat"] floatValue]);
    item.con = @([[itemData valueForKey:@"con"] floatValue]);
    if ([itemData valueForKey:@"bri"] != [NSNull null]) {
        item.bri = @([[itemData valueForKey:@"bri"] floatValue]);
    }

    // BROKEN
    // item.image = [self createAssetById:[[itemData valueForKey:@"imageId"] intValue] withType:0 withHash:[itemData valueForKey:@"imageHash"]];
    
    if ([[itemData valueForKey:@"itemType"] intValue] == 4) {
        
        // BROKEN
        // item.originalImage = [self createAssetById:[[itemData valueForKey:@"sdVideoAssetId"] intValue] withType:0 withHash:[itemData valueForKey:@"sdVideoAssetHash"]];

    } else {
        
        // BROKEN
        // item.originalImage = [self createAssetById:[[itemData valueForKey:@"originalImageId"] intValue] withType:0 withHash:[itemData valueForKey:@"originalImageHash"]];
    }
    if ([itemData valueForKey:@"rid"] == [NSNull null]) {
        item.readAction = @(0);
    } else {
        item.readAction = @([[itemData valueForKey:@"rid"] intValue]);
    }

    //DDLogInfo(@"Create Item BeforeStory");

    //story    
    if ([itemData valueForKey:@"storyId"] != [NSNull null]) {
        item.story = [self createStory:[[itemData valueForKey:@"storyId"] intValue] withAssetId:[[itemData valueForKey:@"storyAssetId"] intValue] andAssetType:2 andStoryLength:[[itemData valueForKey:@"storyLength"] intValue] andAssetHash:[itemData valueForKey:@"storyAssetHash"]];
    }
    else {
        item.story = nil;
    }

    //DDLogInfo(@"Create Item Before Comments");

    if ([itemData valueForKey:@"comments"] != nil) {

        for (NSDictionary *commentData in (NSArray *) [itemData valueForKey:@"comments"]) {
            [self createComment:commentData item:item];
        }

    }

    //DDLogInfo(@"Create Item Before Place");

    //place
    if ([itemData valueForKey:@"placeId"] != [NSNull null] && [itemData valueForKey:@"placeId"] != nil) {
        Place *place = [Place MR_findFirstByAttribute:@"placeId" withValue:@([[itemData valueForKey:@"placeId"] intValue])];
        if (place == nil) {
            place = [Place MR_createEntity];
            place.placeId = @([[itemData valueForKey:@"placeId"] intValue]);
        }
        item.place = place;
    }

    item.itemType = @([[itemData valueForKey:@"itemType"] intValue]);
    if ([itemData valueForKey:@"extraData"] != [NSNull null] && [itemData valueForKey:@"extraData"] != nil) {
        item.extraData = [itemData valueForKey:@"extraData"];
    }

    /*
    if ([itemData valueForKey:@"kidsIds"]){
        item.kidsIds = [itemData valueForKey:@"kidsIds"];
    }
    */



    if ([item.kids.allObjects count] > 0) {
        if ([[(Kid *) item.kids.allObjects[0] kidId] intValue] != [[itemData valueForKey:@"kidId"] intValue]) {
            Kid *kid = [Kid MR_findFirstByAttribute:@"kidId" withValue:[itemData valueForKey:@"kidId"]];
            for (NSInteger i = [item.kids.allObjects count] - 1; i > -1; i--) {
                [item removeKidsObject:item.kids.allObjects[i]];
            }

            [item addKidsObject:kid];
        }
    }

    if ([itemData valueForKey:@"tags"] != nil) {
        [self parseTags:[itemData valueForKey:@"tags"]];
    }

    // DDLogInfo(@"%@",item);

    return item;
}

- (KPLocalItem *)addNewItem:(NSDictionary *)result andData:(NSDictionary *)itemData {

    // DDLogInfo(@"%@",result);

    KPLocalItem *item = [KPLocalItem create];
    item.serverID = [[result valueForKey:@"id"] intValue];
    
    if(item.serverID == 942991){ // item causing reload in notification center
        NSLog(@"here");
    }
    
    if ([itemData valueForKey:@"title"] != nil) {
        item.title = [itemData valueForKey:@"title"];
    }
    item.itemDate = ((NSDate *) [itemData valueForKey:@"itemDate"]).timeIntervalSince1970;

    KPLocalAsset *itemImage = [[KPDataParser instance] createAssetById:[[result valueForKey:@"assetId"] intValue] withType:0 withHash:[result valueForKey:@"assetHash"]];
    item.imageAssetID = itemImage.identifier;

    item.itemType = [[itemData valueForKey:@"itemType"] intValue];
    
    KPLocalAsset *itemOriginalImage;
    if ([[itemData valueForKey:@"itemType"] intValue] == 4) {

        itemOriginalImage = [[KPDataParser instance] createAssetById:[[result valueForKey:@"sdVideoAssetId"] intValue] withType:4 withHash:[result valueForKey:@"sdVideoAssetHash"]];

    } else {

        itemOriginalImage = [[KPDataParser instance] createAssetById:[[result valueForKey:@"originalAssetId"] intValue] withType:0 withHash:[result valueForKey:@"originalAssetHash"]];

    }

    item.originalImageAssetID = itemOriginalImage.identifier;

    item.createDate = [NSDate date].timeIntervalSince1970;
    item.updateDate = [NSDate date].timeIntervalSince1970;
    item.cropRect = [itemData valueForKey:@"cropRect"];
    item.shareStatus = [[itemData valueForKey:@"shareStatus"] integerValue];

    item.rotation = [[itemData valueForKey:@"rotation"] floatValue];
    item.saturation = [[itemData valueForKey:@"sat"] floatValue];
    item.contrast = [[itemData valueForKey:@"con"] floatValue];
    if ([itemData valueForKey:@"bri"] != [NSNull null]) {
        item.brightness = [[itemData valueForKey:@"bri"] floatValue];
    }

    if ([itemData valueForKey:@"autoEnhanceOn"] != [NSNull null]) {
        item.autoEnhanceOn = [[itemData valueForKey:@"autoEnhanceOn"] boolValue];
    }

    if ([itemData valueForKey:@"filterId"] != [NSNull null]) {
        item.filterID = [[itemData valueForKey:@"filterId"] boolValue];
    }



    //filterId


    if (![[itemData valueForKey:@"extraData"] isEqual:[NSNull null]]) {
        item.extraData = [itemData valueForKey:@"extraData"];
    }

    NSArray *kidsIds = [[itemData valueForKey:@"kidsIds"] componentsSeparatedByString:@"|"];
    NSMutableSet *linkedKids = [NSMutableSet set];

    // NSArray *kids = [Kid MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"SELF.kidId IN %@", kidsIds]];

    for(NSNumber *currentKidServerID in kidsIds){

        KPLocalKid *currentKid = [KPLocalKid fetchByServerID:currentKidServerID.intValue];
        // Handling to avoid crash if currentKid is nil.
        if (currentKid != nil) {
        [linkedKids addObject:currentKid];
        }
    }

    [KPLocalKidItem setLinkedKids:linkedKids.allObjects forItem:item];

    // [item addKids:[NSSet setWithArray:kids]];
    /*for (NSString *kidId in kidsIds) {
        Kid *kid = [Kid MR_findFirstByAttribute:@"kidId" withValue:kidId];
        [item addKidsObject:kid];
    }*/

    return item;
}

- (Asset *)createAsset:(NSDictionary *)assetData {
    Asset *asset = nil;

    if (![[UserManager sharedInstance] isAfterLogin]) {
        asset = [Asset MR_findFirstByAttribute:@"assetId" withValue:[assetData valueForKey:@"id"]];
    }

    if (asset == nil) {
        asset = [Asset MR_createEntity];
        asset.assetType = @(-1);
    }

    asset.assetId = @([[assetData valueForKey:@"id"] intValue]);
    asset.assetType = @([[assetData valueForKey:@"assetType"] intValue]);
    if ([assetData valueForKey:@"hash"] != nil && ![[assetData valueForKey:@"hash"] isEqual:[NSNull null]]) {
        asset.assetHash = [assetData valueForKey:@"hash"];
    }

    return asset;
}

- (KPLocalAsset *)initializeAssetByID:(NSUInteger)assetID withType:(NSInteger)assetType withHash:(id)assetHash {

    KPLocalAsset *localAsset = [KPLocalAsset fetchByServerID:assetID];
    // KPLocalAsset *localAsset = self.cachedAssets[@(assetID)];

    if (!localAsset) { // doesn't exist yet

        localAsset = [KPLocalAsset create];
        localAsset.serverID = assetID;
        localAsset.assetType = assetType;
        localAsset.assetStatus = 0;

    }

    if (assetHash && assetHash != [NSNull null]) { // it's a legit, non-null value
        localAsset.assetHash = assetHash;
    }

    [localAsset saveChanges];

    return localAsset;

}

- (KPLocalAsset *)createAssetById:(NSInteger)assetId withType:(NSInteger)assetType withHash:(NSString *)assetHash {
    KPLocalAsset *asset = nil;

    if (![[UserManager sharedInstance] isAfterLogin]) {
        asset = [KPLocalAsset fetchByServerID:assetId];
    }

    if (asset == nil) {
        asset = [KPLocalAsset create];
        asset.assetType = assetType;
        asset.assetStatus = 0;
    }

    asset.serverID = assetId;
    if (assetHash != nil && ![assetHash isEqual:[NSNull null]]) {
        asset.assetHash = assetHash;
    }

    [asset saveChanges];

    return asset;
}

- (KPLocalStory *)initializeStory:(NSUInteger)storyID withAssetID:(NSUInteger)assetID andAssetType:(NSInteger)assetType andStoryLength:(NSInteger)storyLength andAssetHash:(id)assetHash {

    KPLocalStory *localStory = [KPLocalStory fetchByServerID:storyID];

    if (!localStory) {

        localStory = [KPLocalStory create];
        localStory.serverID = storyID;
        localStory.storyLength = storyLength;

        KPLocalAsset *localAsset = [self initializeAssetByID:assetID withType:assetType withHash:assetHash];
        localStory.assetID = localAsset.identifier;

    }

    [localStory saveChanges];

    return localStory;

}

- (Story *)createStory:(NSInteger)storyId withAssetId:(NSInteger)assetId andAssetType:(NSInteger)assetType andStoryLength:(NSInteger)storyLength andAssetHash:(NSString *)assetHash {
    Story *story = nil;

    if (![[UserManager sharedInstance] isAfterLogin]) {
        story = [Story MR_findFirstByAttribute:@"storyId" withValue:@(storyId)];
    }

    if (story == nil) {
        story = [Story MR_createEntity];
    }

    story.storyId = @(storyId);
    story.storyLength = @(storyLength);
    
    // BROKEN
    // story.asset = [self createAssetById:assetId withType:assetType withHash:assetHash];

    return story;
}

- (KPLocalComment *)initializeComment:(NSDictionary *)commentData {

    NSUInteger serverCommentID = [commentData[@"id"] intValue];
    
    if(serverCommentID == 133207){
        NSLog(@"a comment made by a fan");
    }
    
    KPLocalComment *localComment = [KPLocalComment fetchByServerID:serverCommentID];

    BOOL isNew = NO;

    if (!localComment) {

        localComment = [KPLocalComment create];
        localComment.serverID = serverCommentID;

        isNew = YES;
    }

    id creationDate = commentData[@"createDate"];
    if (creationDate && creationDate != [NSNull null]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        NSDate *commentDate = [formatter dateFromString:creationDate];
        localComment.commentDate = commentDate.timeIntervalSince1970;
    }

    id wasRead = commentData[@"rid"];
    if (wasRead && wasRead != [NSNull null]) {
        localComment.wasRead = YES; // [wasRead boolValue];
    }

    id commentLength = commentData[@"commentLength"];
    if (commentLength && commentLength != [NSNull null]) {
        localComment.commentLength = [commentLength intValue];
    }

    id commentType = commentData[@"commentType"];
    if (commentType && commentType != [NSNull null]) {
        localComment.commentType = [commentType intValue];
    }

    id previewImageID = commentData[@"previewImageId"];
    if (previewImageID && previewImageID != [NSNull null]) {

        id previewImageHash = commentData[@"previewImageHash"];
        KPLocalAsset *localAsset = [self initializeAssetByID:[previewImageID intValue] withType:0 withHash:previewImageHash];
        localComment.previewImageAssetID = localAsset.identifier;

    }

    if (localComment.commentType == 0 || localComment.commentType == 1) { // whatever that means

        NSInteger assetType = localComment.commentType + 1;
        id videoAssetID = commentData[@"videoAssetId"];

        if (videoAssetID && videoAssetID != [NSNull null]) {
            id videoAssetHash = commentData[@"videoAssetHash"];
            KPLocalAsset *localAsset = [self initializeAssetByID:[videoAssetID intValue] withType:assetType withHash:videoAssetHash];
            localComment.videoAssetID = localAsset.identifier;
        }

    }

    id extraData = commentData[@"extraData"];
    if (extraData && extraData != [NSNull null]) {
        localComment.extraData = extraData;
    }

    id isStory = commentData[@"isStory"];
    if (isStory && isStory != [NSNull null]) {
        localComment.isStory = [isStory boolValue];
    }

    id userNickname = commentData[@"userNickname"];
    if (userNickname && userNickname != [NSNull null]) {
        localComment.creatorName = userNickname;
    }
    
    id fanName = commentData[@"fanName"];
    if (fanName && fanName != [NSNull null]) {
        localComment.creatorName = fanName;
    }

    id userRelationType = commentData[@"userRelationType"];
    if (userRelationType && userRelationType != [NSNull null]) {
        localComment.creatorRelationType = [userRelationType intValue];
    }

    id fanRelationType = commentData[@"fanRelationType"];
    if (fanRelationType && fanRelationType != [NSNull null]) {
        localComment.creatorRelationType = [fanRelationType intValue];
    }

    id isParent = commentData[@"isParent"];
    if (isParent && isParent != [NSNull null]) {
        id parentType = commentData[@"parentType"];
        id name = commentData[@"name"];

        if (parentType && parentType != [NSNull null]) {
            localComment.creatorRelationType = [parentType intValue];
        }

        if (name && name != [NSNull null]) {
            localComment.creatorName = name;
        }

    }

    id userID = commentData[@"userId"];
    if (userID && userID != [NSNull null]) {
        localComment.creatorUserID = [userID intValue];
    }

    id fanID = commentData[@"fanId"];
    if (fanID && fanID != [NSNull null]) {
        localComment.creatorFanID = [fanID intValue];
    }

    // TODO: choose item/comment association method
    // id itemID = commentData[@"itemId"];

    [localComment saveChanges];

    return localComment;

}

- (Comment *)createComment:(NSDictionary *)commentData item:(Item *)item {
    Comment *comment = nil;

    if (![[UserManager sharedInstance] isAfterLogin]) {
        comment = [Comment MR_findFirstByAttribute:@"commentId" withValue:[commentData valueForKey:@"id"]];
    }

    BOOL isNew = NO;
    if (comment == nil) {
        comment = [Comment MR_createEntity];
        isNew = YES;
    }

    if ([commentData valueForKey:@"id"] == [NSNull null]) {
        return nil;
    }


    comment.commentId = @([[commentData valueForKey:@"id"] intValue]);

    if ([commentData valueForKey:@"createDate"] != [NSNull null]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        comment.commentDate = [formatter dateFromString:[commentData valueForKey:@"createDate"]];
    }



    comment.wasRead = @([commentData valueForKey:@"rid"] != [NSNull null]);

    if ([commentData valueForKey:@"commentLength"] != [NSNull null]) {
        comment.commentLength = @([[commentData valueForKey:@"commentLength"] intValue]);
    }

    if ([commentData valueForKey:@"commentType"] != [NSNull null]) {
        comment.commentType = @([[commentData valueForKey:@"commentType"] intValue]);
    }

    if ([commentData valueForKey:@"previewImageId"] != [NSNull null]) {
        // BROKEN
        // comment.previewImage = [self createAssetById:[[commentData valueForKey:@"previewImageId"] intValue] withType:0 withHash:[commentData valueForKey:@"previewImageHash"]];

    }

    if (comment.commentType.intValue == 0) {
        if ([commentData valueForKey:@"videoAssetId"] == [NSNull null]) {
            return nil;
        }

        // BROKEN
        // comment.video = [self createAssetById:[[commentData valueForKey:@"videoAssetId"] intValue] withType:1 withHash:[commentData valueForKey:@"videoAssetHash"]];
    }
    else if (comment.commentType.intValue == 1) {
        if ([commentData valueForKey:@"videoAssetId"] == [NSNull null]) {
            return nil;
        }

        // BROKEN
        // comment.video = [self createAssetById:[[commentData valueForKey:@"videoAssetId"] intValue] withType:2 withHash:[commentData valueForKey:@"videoAssetHash"]];
    }

    if ([commentData valueForKey:@"extraData"] != [NSNull null]) {
        comment.extraData = [commentData valueForKey:@"extraData"];
    }

    if ([commentData valueForKey:@"isStory"] != [NSNull null]) {
        comment.isStory = @([[commentData valueForKey:@"isStory"] boolValue]);
    }

    if ([commentData valueForKey:@"userNickname"] != [NSNull null]) {
        comment.creatorName = [commentData valueForKey:@"userNickname"];
    }

    if ([commentData valueForKey:@"fanName"] != [NSNull null]) {
        comment.creatorName = [commentData valueForKey:@"fanName"];
    }

    if ([commentData valueForKey:@"userRelationType"] != [NSNull null]) {
        comment.creatorRelationType = @([[commentData valueForKey:@"userRelationType"] intValue]);
    }

    if ([commentData valueForKey:@"fanRelationType"] != [NSNull null]) {
        comment.creatorRelationType = @([[commentData valueForKey:@"fanRelationType"] intValue]);
    }

    if ([commentData valueForKey:@"isParent"] != [NSNull null] && [[commentData valueForKey:@"isParent"] intValue] == 1) {
        if ([commentData valueForKey:@"parentType"] != [NSNull null]) {
            comment.creatorRelationType = @([[commentData valueForKey:@"parentType"] intValue]);
        }

        if ([commentData valueForKey:@"name"] != [NSNull null]) {
            comment.creatorName = [commentData valueForKey:@"name"];
        }

    }


    if ([commentData valueForKey:@"userId"] != [NSNull null]) {
        comment.creatorUserId = @([[commentData valueForKey:@"userId"] intValue]);
    }

    if ([commentData valueForKey:@"fanId"] != [NSNull null]) {
        comment.creatorUserId = @([[commentData valueForKey:@"fanId"] intValue]);
    }

    if ([commentData valueForKey:@"itemId"]) {
        //Item *item = [Item MR_findFirstByAttribute:@"itemId" withValue:[commentData valueForKey:@"itemId"]];
        if (![item.comments containsObject:comment]) {
            [item addCommentsObject:comment];

            if (isNew) {
                [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_UPDATED_NOTIFICATION object:item];
            }

        }
    }


    return comment;
}

- (Album *)createAlbumFromItem:(Item *)item withKid:(Kid *)kid {
    NSInteger year = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:item.itemDate].year;
    Album *album = [Album MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"kid=%@ AND year=%d", kid, year]];
    if (album == nil) {
        album = [Album MR_createEntity];
        album.year = @(year);
        album.kid = kid;
    }

    if (![album.items containsObject:item]) {
        [album addItemsObject:item];
    }

    return album;
}

- (void)parsePlaces:(NSArray *)places {
    if (places == nil) {
        return;
    }

    for (NSDictionary *placeDict in places) {
        
        KPLocalPlace *place = [KPLocalPlace fetchByServerID:[[placeDict valueForKey:@"id"] intValue]];
        
        // Place *place = [Place MR_findFirstByAttribute:@"placeId" withValue:[NSNumber numberWithInt:[[placeDict valueForKey:@"id"] intValue]]];
        if (place == nil) {
            // place = [Place MR_createEntity];
            place = [KPLocalPlace create];
        }

        place.serverID = [[placeDict valueForKey:@"id"] intValue];
        place.title = [placeDict valueForKey:@"title"];
        place.longitude = [[placeDict valueForKey:@"lon"] doubleValue];
        place.latitude = [[placeDict valueForKey:@"lat"] doubleValue];
        
        [place saveChanges];
        
    }
    
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {

    }];
}

- (void)parseFans:(NSArray *)fans {
    //if ([[[UserManager sharedInstance] myKids] count] == 0)
    //    return;

    if (fans == nil) {
        return;
    }

    for (NSDictionary *fanDict in fans) {
        int fanId = [[fanDict valueForKey:@"id"] intValue];
        Fan *fan = [Fan MR_findFirstByAttribute:@"fanId" withValue:@(fanId)];
        if (fan == nil) {
            fan = [Fan MR_createEntity];
        }

        fan.fanId = @(fanId);
        fan.userId = @([[fanDict valueForKey:@"userId"] intValue]);
        fan.nickname = [fanDict valueForKey:@"fanName"];
        if ([fanDict valueForKey:@"email"] != nil && ![[fanDict valueForKey:@"email"] isEqual:[NSNull null]]) {
            fan.email = [fanDict valueForKey:@"email"];
        } else if ([fanDict valueForKey:@"fanEmail"] != nil && ![[fanDict valueForKey:@"fanEmail"] isEqual:[NSNull null]]) {
            fan.email = [fanDict valueForKey:@"fanEmail"];
        }

        if ([fanDict valueForKey:@"fanFacebookId"] != nil && ![[fanDict valueForKey:@"fanFacebookId"] isEqual:[NSNull null]]) {
            fan.facebookId = [fanDict valueForKey:@"fanFacebookId"];
        }

        fan.relationType = @([[fanDict valueForKey:@"fanRelationType"] intValue]);
        fan.didApprove = @([[fanDict valueForKey:@"status"] intValue] == 1);

        if ([fanDict valueForKey:@"birthdate"] != [NSNull null]) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z"];
            fan.birthdate = [formatter dateFromString:[fanDict valueForKey:@"birthdate"]];
        }
    }

    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:FANS_REFRESHED_NOTIFICATION object:nil];
    }];

}

- (void)parseNotifications:(NSArray *)notifications {
    if (notifications == nil) {
        NSLog(@"empty notifications");
        return;
    }
    
    int added = 0;
    
    for (NSDictionary* d in notifications) {
        NSInteger nid = [d[@"id"] integerValue];
        KPNotification * noti = [KPNotification MR_findFirstByAttribute:@"notificationId" withValue:@(nid)];
        
        if (!noti) {
            noti = [KPNotification MR_createEntity];
            
            noti.notificationId = @(nid);
            noti.notificationType = @([d[@"notificationType"] intValue]);
            noti.subject = d[@"subject"];
            
            if ([d[@"senderId"] isKindOfClass:[NSNumber class]]) {
                noti.senderId = d[@"senderId"];
            }
            
            if ([d[@"items"] isKindOfClass:[NSString class]]) {
                noti.items = d[@"items"];
            }
            
            if ([d[@"notificationDate"] isKindOfClass:[NSString class]]) {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                noti.notificationDate = [formatter dateFromString:d[@"notificationDate"]];
            }
            noti.status = @([d[@"status"] intValue]);  // Only mutable field.
            added++;
        }
    }
    
    NSLog(@"Notifications: %lu received / %d added", (unsigned long)notifications.count, added);
    
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATIONS_REFRESHED_NOTIFICATION object:nil];
    }];
}

- (void)parseTags:(NSArray *)tags {
    //zvik526
    if (tags == nil) {
        return;
    }
    
    if(tags.count < 1){
        return;
    }

    for (NSDictionary *tagDict in tags) {
        if (tagDict[@"id"] == NULL || tagDict[@"id"] == nil){} else {
        if ( ([tagDict[@"id"] intValue] != 0)  ||  (tagDict[@"id"]  != nil )){
        int serverID = [tagDict[@"id"] intValue];

        Tag *tag = [Tag MR_findFirstByAttribute:@"tagId" withValue:@([[tagDict valueForKey:@"id"] intValue])];
        KPLocalTag *localTag = [KPLocalTag fetchByServerID:serverID];
        
        if (tag == nil) {
            tag = [Tag MR_createEntity];
        }

        if(!localTag){
            localTag = [KPLocalTag create];
            localTag.serverID = serverID;
        }

        tag.tagId = @([[tagDict valueForKey:@"id"] intValue]);
        tag.name = [tagDict valueForKey:@"name"];

        localTag.name = tagDict[@"name"];

        [localTag saveChanges];



        id itemID = tagDict[@"itemId"];
        
        if(itemID && itemID != [NSNull null]){

            int itemServerID = [itemID intValue];
            KPLocalItem *item = [KPLocalItem fetchByServerID:itemServerID];

            if(item){
            //zvika524
                // check tag relations (before zvika)
                NSArray *relatedTagIDs = [KPLocalItemTag fetchTagIDsForItem:item];
                if(![relatedTagIDs containsObject:@(localTag.identifier)]){ // this one has not been added yet (before zvika)
                    NSArray *newRelatedTagIDs = [relatedTagIDs arrayByAddingObject:@(localTag.identifier)];
                    NSArray *newLinkedTags = [KPLocalTag idsToObjects:newRelatedTagIDs];

                    [KPLocalItemTag setLinkedTags:newLinkedTags forItem:item];
                }
                
            }

        }

        
        // basically the same as above, just for Core Data
        if ([tagDict valueForKey:@"itemId"] != nil) {
            Item *item = [Item MR_findFirstByAttribute:@"itemId" withValue:[tagDict valueForKey:@"itemId"]];
            if (![item.tags containsObject:tag]) {
                [item addTagsObject:tag];
            }
        }
    }
        }
    }

    if (![[UserManager sharedInstance] isAfterLogin]) {
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {

        }];
    }
}


@end
