//
//  GlobalUtils.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/22/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <TwitterKit/TwitterKit.h>

#import <MagicalRecord/CoreData+MagicalRecord.h>

#import <LaunchKit/LaunchKit.h>


#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "FullScreenContainerViewController.h"
#import "KPDataParser.h"

#import "EditFanViewController.h"
#import "AccountViewController.h"
#import <MessageUI/MessageUI.h>
#import "SelectFanSourceViewController.h"

#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>
#import "KPLocalKid.h"
#import "KPWebViewController.h"
#import <uservoice_iphone_sdk/UserVoice.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <Mixpanel/Mixpanel.h>
#import "KPLocalItem.h"
#import "KPLocalAsset.h"
#import "KPLocalKidItem.h"
#import "KPLocalItemTag.h"
#import "KPLocalTag.h"
#import "KPPlan.h"
#import "KPPlan+API.h"

#import "PrintIOManager.h"

#import "Keepy-Swift.h"
#import <StoreKit/StoreKit.h>

typedef void (^CompleteBlock)();

static NSArray<NSString*>* GetItemIdsFromResponse(NSDictionary* response);
static void ShowItemsInFullScreen(NSArray<NSString*>* itemIds, NSInteger commentId);

@interface GlobalUtils () <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property(nonatomic) BOOL isEditMode;
@property(nonatomic, strong) KPLocalKid *kid;
@property(nonatomic, strong) Place *place;
@property(nonatomic, strong) NSDate *date;
@property(nonatomic, strong) NSMutableDictionary *storiesPlayed;
@property(nonatomic, strong) CompleteBlock tipCompleteBlock;
@property(nonatomic, strong) UIView *currentTip;
@property(nonatomic, strong) NSMutableDictionary *uploadingAssets;
@property(nonatomic, strong) NSDictionary *currentItemData;
@property(nonatomic, strong) NSDictionary *currentCommentData;
@property(nonatomic, strong) UINavigationController *navController;

@property(nonatomic) BOOL isOnFeedbackFlow;

@end


@implementation GlobalUtils

+ (instancetype)sharedInstance {
    static dispatch_once_t pred;
    static id sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

+ (BOOL)isEditMode {
    return [[GlobalUtils sharedInstance] isEditMode];
}

+ (void)setIsEditMode:(BOOL)isEditMode {
    [[GlobalUtils sharedInstance] setIsEditMode:isEditMode];
}

+ (NSInteger)daysBetweenDate:(NSDate *)fromDateTime andDate:(NSDate *)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;

    if (fromDateTime == nil || toDateTime == nil) {
        return 0;
    }

    NSCalendar *calendar = [NSCalendar currentCalendar];

    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];

    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];

    return [difference day];
}

+ (NSString *)createCropRectStr:(NSArray *)points {
    NSMutableString *result = [[NSMutableString alloc] init];
    NSString *sep = @"";
    for (id pi in points) {
        CGPoint p = [pi CGPointValue];
        [result appendFormat:@"%@%2.2f,%2.2f", sep, p.x, p.y];
        sep = @"|";
    }

    return result;
}

+ (NSArray *)getPointsFromCropRectStr:(NSString *)str {
    NSMutableArray *result = [[NSMutableArray alloc] init];

    if (str == nil || str.length == 0) {
        return result;
    }

    NSArray *arr = [str componentsSeparatedByString:@"|"];
    for (NSString *ps in arr) {
        NSArray *parr = [ps componentsSeparatedByString:@","];
        if ([parr count] > 0) {
            CGPoint p = CGPointMake([parr[0] floatValue], [parr[1] floatValue]);
            [result addObject:[NSValue valueWithCGPoint:p]];
        }
    }

    return result;
}

+ (Kid *)getLastSelectedKid {
    return [[self sharedInstance] kid];
}

+ (void)setCurrentSelectedKid:(KPLocalKid *)kid {
    [[self sharedInstance] setKid:kid];
}

- (void)setKid:(KPLocalKid *)kid {
    _kid = kid;
}

+ (Place *)getLastSelectedPlace {
    return [[self sharedInstance] place];
}

+ (void)setCurrentSelectedPlace:(Place *)place {
    [[self sharedInstance] setPlace:place];
}

+ (NSString *)getKidNames:(NSArray *)kids {
    if (kids.count == 0) {
        return @"";
    }

    if (kids.count == 1) {
        Kid *kid = kids[0];
        return kid.name;
    }

    if (kids.count == 2) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        return [NSString stringWithFormat:@"%@ and %@", kid1.name, kid2.name];
    }

    if (kids.count == 3) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        Kid *kid3 = kids[2];
        return [NSString stringWithFormat:@"%@, %@ and %@", kid1.name, kid2.name, kid3.name];
    }

    if (kids.count == 4) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        Kid *kid3 = kids[2];
        Kid *kid4 = kids[3];
        return [NSString stringWithFormat:@"%@, %@, %@ and %@", kid1.name, kid2.name, kid3.name, kid4.name];
    }

    if (kids.count > 4) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        Kid *kid3 = kids[2];
        Kid *kid4 = kids[3];
        return [NSString stringWithFormat:@"%@, %@, %@, %@ and %lu %@", kid1.name, kid2.name, kid3.name, kid4.name, (unsigned long)(kids.count - 4), ((kids.count - 4) == 1 ? @"other" : @"others")];
    }

    return @"";
}

+ (NSString *)getKidNamesv2:(NSArray *)kids {
    if (kids.count == 0) {
        return @"";
    }

    if (kids.count == 1) {
        KPLocalKid *kid = kids[0];
        return kid.name;
    }

    if (kids.count == 2) {
        KPLocalKid *kid1 = kids[0];
        KPLocalKid *kid2 = kids[1];
        return [NSString stringWithFormat:@"%@ and %@", kid1.name, kid2.name];
    }

    if (kids.count == 3) {
        KPLocalKid *kid1 = kids[0];
        KPLocalKid *kid2 = kids[1];
        KPLocalKid *kid3 = kids[2];
        return [NSString stringWithFormat:@"%@, %@ and %@", kid1.name, kid2.name, kid3.name];
    }

    if (kids.count > 3) {
        KPLocalKid *kid1 = kids[0];
        KPLocalKid *kid2 = kids[1];
        KPLocalKid *kid3 = kids[2];

        return [NSString stringWithFormat:@"%@, %@, %@ and %lu %@", kid1.name, kid2.name, kid3.name, (unsigned long)(kids.count - 3), ((kids.count - 3) == 1 ? @"other" : @"others")];
    }

    return @"";
}

+ (NSString *)getKidNamesToShare:(NSArray *)kids {
    if (kids.count == 0) {
        return @"";
    }
    
    if (kids.count == 1) {
        Kid *kid = kids[0];
        return [NSString stringWithFormat:@"%@'s", kid.name];
    }
    
    if (kids.count == 2) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        return [NSString stringWithFormat:@"%@'s and %@'s", kid1.name, kid2.name];
    }
    
    if (kids.count == 3) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        Kid *kid3 = kids[2];
        return [NSString stringWithFormat:@"%@'s, %@'s and %@'s", kid1.name, kid2.name, kid3.name];
    }
    
    if (kids.count == 4) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        Kid *kid3 = kids[2];
        Kid *kid4 = kids[3];
        return [NSString stringWithFormat:@"%@'s, %@'s, %@'s and %@'s", kid1.name, kid2.name, kid3.name, kid4.name];
    }
    
    if (kids.count > 4) {
        Kid *kid1 = kids[0];
        Kid *kid2 = kids[1];
        Kid *kid3 = kids[2];
        Kid *kid4 = kids[3];
        return [NSString stringWithFormat:@"%@'s, %@'s, %@'s, %@'s and %lu %@'s", kid1.name, kid2.name, kid3.name, kid4.name, (unsigned long)(kids.count - 4), ((kids.count - 4) == 1 ? @"other" : @"others")];
    }
    
    return @"";
}

+ (NSDate *)getLastSelectedDate {
    return [[self sharedInstance] date];
}

+ (void)setCurrentSelectedDate:(NSDate *)date {
    [[self sharedInstance] setDate:date];
}

+ (void)outputToSpeaker {
    AVAudioSession* session = [AVAudioSession sharedInstance];
    NSError* error = nil;
    
    // Use AVAudioSessionCategoryPlayAndRecord to use AVAudioSessionPortOverrideSpeaker
    if (![session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error]) {
        NSLog(@"failed to set category: %@", error);
    } else if (![session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error]) {
        NSLog(@"failed to override output: %@", error);
    }
}

+ (void)shareItem:(KPLocalItem *)item withSource:(NSString *)sourceStr sender:(UIButton *)sender {

    if (![[UIActivityViewController class] respondsToSelector:@selector(init)]) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"this feature is currently available only on iOS6")];
        return;
    }
    
    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:item.imageAssetID];
    NSLog(@"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ assetHash: %@, with assetId: %lu", itemImage.urlHash, (unsigned long)item.imageAssetID);

    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@_medium.jpg", Defaults.assetsURLString, itemImage.urlHash]];
    NSLog(@"loading: %@", url);
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        //NSLog(@"progress: %ld / %d", (long)receivedSize, expectedSize);
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"preparing")];
        });

    }                                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {

        if(!image){
            [SVProgressHUD showErrorWithStatus:@"This Keepy is not currently available."];
            return;
        }
        
        NSArray *itemKidIDs = [KPLocalKidItem fetchKidIDsForItem:item];
        NSArray *itemKids = [KPLocalKid idsToObjects:itemKidIDs];

        NSString *kidName = nil;
        if (itemKidIDs.count > 1) {
            kidName = [GlobalUtils getKidNames:itemKids];
        } else {
            kidName = ((KPLocalKid *) itemKids[0]).name;
        }
        
        NSMutableString *tagsNames = [NSMutableString string];

        NSArray *itemTagIDs = [KPLocalItemTag fetchTagIDsForItem:item];
        NSArray *itemTags = [KPLocalTag idsToObjects:itemTagIDs];

        NSString *tagname = nil;
        if (itemTagIDs.count > 0) {
            for (KPLocalTag *tag in itemTags) {
                tagname = [NSString stringWithFormat:@"#%@ ", [tag.name stringByReplacingOccurrencesOfString:@" " withString:@""]];
                [tagsNames appendString:tagname];
            }
        }
        else {
            tagsNames = [NSMutableString stringWithFormat:@"#parenting #keepythat #memories", nil];
        }

        // DDLogInfo(@"%@",[NSString stringWithString:tagsNames]);
        
        NSString *linkToApplication = nil;

        //Fix in 1.95.3

        /*
        if (item.itemType.intValue == 4){
            imageURL = [NSString stringWithFormat:@"\n%@",[resultDictionary objectForKey:@"short_url"]];
        }
        else imageURL = @"";
        */
        //imageURL = [NSString stringWithFormat:@"\n%@", resultDictionary[@"short_url"]];
        
        linkToApplication = @"getapp.keepy.me/iloveit";
        
        NSString *messageStr = nil;
        NSString *trimmedTitle = [item.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (trimmedTitle != nil && ![trimmedTitle isEqualToString:@""]) {
            messageStr = [NSString stringWithFormat:@"\"%@\"\n%@'s latest memory via @keepyme\nCheck it out: %@\n%@", trimmedTitle, kidName, linkToApplication, [NSString stringWithString:tagsNames]];
        } else {
            messageStr = [NSString stringWithFormat:@"%@'s latest memory via @keepyme\nCheck it out: %@\n%@", kidName, linkToApplication, [NSString stringWithString:tagsNames]];
        }
        
      //  UIImage* imageToShare = [image addFrame];
        UIImage* imageToShare = [image addFrameNew];
        
        TextActivityProvider *textProviter = [[TextActivityProvider alloc] initWithText: messageStr];
        ImageActivityProvider *imageProviter = [[ImageActivityProvider alloc] initWithImage: imageToShare];
        
        NSArray *itemsToShare = @[imageProviter, textProviter];

        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypeCopyToPasteboard, UIActivityTypePrint, UIActivityTypeAssignToContact];

        activityVC.completionWithItemsHandler = ^(NSString * activityType, BOOL completed, NSArray * returnedItems, NSError * activityError) {
            if(!activityType){
                activityType = @"Cancelled";
            }
            DDLogInfo(@"completed dialog - activity: %@ - finished flag: %d", activityType, completed);
            [[Mixpanel sharedInstance] track:@"Share Photo" properties:@{@"Medium" : activityType, @"Source" : sourceStr, @"Completion" : @(completed)}];
        };

        [SVProgressHUD dismiss];

        activityVC.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
        presentationController.sourceView = sender;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[GlobalUtils topMostController] presentViewController:activityVC animated:YES completion:nil];
        });

//        [[GlobalUtils topMostController] presentViewController:activityVC animated:YES completion:nil];
    }];
}

+ (void)buyItem:(KPLocalItem *)item withSource:(NSString *)sourceStr {
    
    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:item.imageAssetID];
    
    UIViewController *topViewController = [GlobalUtils topMostController];
    [PrintIOManager showPrintIODialogInViewController:topViewController withImage:(id)itemImage];
    
    // we don't wanna load the full-size image
    
    return;
    
    // this is for Print.IO
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, itemImage.urlHash]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showProgress:(float) receivedSize / (float) expectedSize status:KPLocalizedString(@"preparing")];
        });
        
    }                                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        
        if(!image){
            [SVProgressHUD showErrorWithStatus:@"This Keepy is not currently available."];
            return;
        }
        
        [SVProgressHUD dismiss];
        
        UIViewController *topViewController = [GlobalUtils topMostController];
        [PrintIOManager showPrintIODialogInViewController:topViewController withImage:image];
        
    }];    
     
     return;
    
    // this is for Zazzle
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, (itemImage.assetHash == nil) ? [NSString stringWithFormat:@"%lu", (unsigned long)itemImage.serverID] : itemImage.assetHash];
    imageUrl = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef) imageUrl, NULL, CFSTR("!*'();:@&=+$,/?%#[]\" "), kCFStringEncodingUTF8));

    NSString *zazzleBaseUrl = @"www.zazzle.com";
    if ([[[UserManager sharedInstance] getMe] zazzleUrl] != nil) {
        zazzleBaseUrl = [[[UserManager sharedInstance] getMe] zazzleUrl];
    }

    NSString *s = [NSString stringWithFormat:@"http://%@/keepyme&rut=Go back to keepyme\'s store", zazzleBaseUrl];

    s = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef) s, NULL, CFSTR("!*'();:@&=+$,/?%#[]\" "), kCFStringEncodingUTF8));

    NSString *zazzleUrl = [NSString stringWithFormat:@"http://%@/api/create/at-238615977946474380?rf=238615977946474380&ax=DesignBlast&sr=250239466529695921&cg=0&ed=true&continueUrl=%@&fwd=ProductPage&tc=KEEPY_BUFFET&ic=&t_image0_iid=%@&t_keepyimage_iid=%@&tc=%d&ic=%@", zazzleBaseUrl, s, imageUrl, imageUrl, [[UserManager sharedInstance] getMe].userId.intValue, (itemImage.assetHash == nil) ? [NSString stringWithFormat:@"%lu", (unsigned long)itemImage.serverID] : itemImage.assetHash];


    DDLogInfo(@"%@", zazzleBaseUrl);

    KPWebViewController *avc = [[KPWebViewController alloc] initWithUrl:zazzleUrl andTitle:KPLocalizedString(@"print & give")];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    navController.view.tag = 1;
    [[GlobalUtils topMostController] presentViewController:navController animated:YES completion:nil];

}

+ (NSString *)getParentTypeStr:(NSInteger)parentType {
    if (parentType == 1) {
        return @"Dad";
    } else if (parentType == 0) {
        return @"Mom";
    } else {
        return @"x";
    }
}

+ (NSString *)getUserParentTypeStr:(User *)user {
    if (user.parentType.intValue == 1) {
        return @"Dad";
    } else if (user.parentType.intValue == 0) {
        return @"Mom";
    } else if (user.parentType.intValue == 2) {
        if (user.otherTxt.length > 0) {
            return user.otherTxt;
        } else {
            return @"other";
        }
    } else {
        return @"x";
    }
}

+ (NSString *)getGenderStr:(NSInteger)gender {
    if (gender == 1) {
        return @"Boy";
    } else {
        return @"Girl";
    }
}


+ (UIImage *)getKidDefaultImage:(NSInteger)gender {
    if (gender == 2) {
        return [UIImage imageNamed:@"boy-avatar"];
    } else {
        return [UIImage imageNamed:@"girl-avatar"];
    }
}

#pragma mark - Story Played

+ (void)clearStoriesPlayed {
    [[self sharedInstance] setStoriesPlayed:[[NSMutableDictionary alloc] init]];
}

+ (BOOL)wasStoryPlayed:(NSInteger)storyId {
    if ([[self sharedInstance] storiesPlayed] == nil) {
        return NO;
    }

    return ([[[self sharedInstance] storiesPlayed] valueForKey:[NSString stringWithFormat:@"%ld", (long)storyId]] != nil);
}

+ (void)storyPlayed:(NSInteger)storyId {
    if ([[self sharedInstance] storiesPlayed] == nil) {
        [[self sharedInstance] setStoriesPlayed:[[NSMutableDictionary alloc] init]];
    }

    [[[self sharedInstance] storiesPlayed] setValue:@(1) forKey:[NSString stringWithFormat:@"%ld", (long)storyId]];
}

#pragma mark - Full Screen

- (void)showItemsInFullScreen:(NSArray *)items withIndex:(NSInteger)index andCommentId:(NSInteger)commentId {
    NSInteger commentIndex = -1;
    if (commentId != -1) {
        
        // Item *item = items[index];
        KPLocalItem *item = items[index];
        
        // commentIndex = [item getCommentIndexById:commentId] + 1;
        commentIndex = [item fetchCommentIndexByID:commentId];
        
    }
    
    [self showItemsInFullScreen:items withIndex:index andCommentIndex:commentIndex];
    
}

- (void)showItemsInFullScreen:(NSArray *)items withIndex:(NSInteger)index andCommentIndex:(NSInteger)commentIndex {
    FullScreenContainerViewController *avc = [[FullScreenContainerViewController alloc] initWithItems:items andIndex:index withCommentIndex:commentIndex];
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    if (d.viewController.presentedViewController) {
        [d.viewController.presentedViewController presentViewController:avc animated:YES completion:nil];
    } else {
        [d.viewController presentViewController:avc animated:YES completion:nil];
    }

    //[d.window setRootViewController:avc];
    //[d.window addSubview:avc.view];
}

#pragma mark - Notifications

+ (void)execNotification:(NSInteger)notificationId withType:(NSInteger)notificationType andItems:(NSString *)items {
    __block KPNotification *notification = [KPNotification MR_findFirstByAttribute:@"notificationId" withValue:@(notificationId)];

    //DDLogInfo(@"%@",items);

    //mark notification as read
    if (notification == nil || notification.status.intValue == NotificationStatusUnread) {
        [[ServerComm instance] markNotificationRead:notificationId withSuccessBlock:^(id result) {

            if (notification == nil) {
                notification = [KPNotification MR_createEntity];
                notification.notificationId = @(notificationId);
                notification.notificationType = @(notificationType);
                notification.items = items;
            }
            notification.status = @(NotificationStatusRead);
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATIONS_REFRESHED_NOTIFICATION object:nil];

        }                              andFailBlock:^(NSError *error) {

        }];
    }

    if (notificationType == NotificationTypeNewFan) {
        int fanId = [items intValue];
        Fan *fan = [Fan MR_findFirstByAttribute:@"fanId" withValue:@(fanId)];
        if (fan != nil) {
            fan.didApprove = @YES;
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [[NSNotificationCenter defaultCenter] postNotificationName:FANS_REFRESHED_NOTIFICATION object:nil];
        }
        return;
    }

    int commentId = -1;

    //check if notification items exists, if not bring them...
    NSMutableString *itemsToFetch = [[NSMutableString alloc] init];
    NSString *commaStr = @"";
    NSMutableArray *itemIds = [[NSMutableArray alloc] initWithArray:[items componentsSeparatedByString:@","]];
    if (notificationType == NotificationTypeItems) //new photos notification, remove first element
    {
        [itemIds removeObjectAtIndex:0];
    }
    else if (notificationType == NotificationTypeCommentItem) //new comment, only last element is item
    {
        commentId = [itemIds[1] intValue];
        [itemIds removeObjectsInRange:NSMakeRange(0, 2)];
    }

    for (NSString *itemId in itemIds) {
        
        // TODO: re-enable notification reload in the case of comments
        if (notificationType == NotificationTypeCommentItem && FALSE) //when new comment always fetch item
        {
            [itemsToFetch appendFormat:@"%@%@", commaStr, itemId];
            commaStr = @",";
        }
        else {
            int itemIDPrimitive = itemId.intValue;
            KPLocalItem *item = [KPLocalItem fetchByServerID:itemIDPrimitive];
            // Item *item = [Item MR_findFirstByAttribute:@"itemId" withValue:@([itemId intValue])];
            if (item == nil) {
                [itemsToFetch appendFormat:@"%@%@", commaStr, itemId];
                commaStr = @",";
            }
        }
    }

    //Haggai add ! (not) to if (![itemsToFetch isEqualToString:@""])

    if ([itemsToFetch isEqualToString:@""]) {
        
        // let's describe the item IDs
        NSMutableArray *localItems = @[].mutableCopy;
        for(NSString *currentItemID in itemIds){
            KPLocalItem *currentItem = [KPLocalItem fetchByServerID:currentItemID.intValue];
            if(currentItem){
                [localItems addObject:currentItem];
            }
        }
        
        //show...
        // NSArray *arr = [Item MR_findAllSortedBy:@"createDate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"itemId in %@", itemIds]];

        [[self sharedInstance] showItemsInFullScreen:localItems withIndex:0 andCommentId:commentId];
        // [[self sharedInstance] showItemsInFullScreen:arr withIndex:0 andCommentId:commentId];
    }
    else {
        [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];
        //get missing items from server...

        [[ServerComm instance] getItems:^(id result) {

                    //DDLogInfo(@"%@",result);

                    [[KPDataParser instance] parseKids:(NSArray *) [result valueForKey:@"result"] complete:^{

                        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                        
                        if (notificationType == NotificationTypeCommentCollection) {
                            NSString* collectionId = [itemIds lastObject];
                            if ([collectionId isKindOfClass:[NSString class]]) {
                                [[ServerComm instance] getCollection:collectionId.integerValue completion:^(NSDictionary* response, NSError* error) {
                                    if (error) {
                                        NSLog(@"fail to get collection: %@", error);
                                    } else {
                                        NSLog(@"collection! %@", response);
                                        
                                        NSArray* arr = GetItemIdsFromResponse(response);
                                        ShowItemsInFullScreen(arr, 0);
                                    }
                                    [SVProgressHUD dismiss];
                                }];
                            } else {
                                NSLog(@"unexpected items: %@", itemIds);
                                [SVProgressHUD dismiss];
                            }
                            
                        } else {
                            ShowItemsInFullScreen(itemIds, commentId);
                            [SVProgressHUD dismiss];
                        }
                    }];
                }
                           andFailBlock:^(NSError *error) {
                               if (error) {
                                   DDLogInfo(@"%@", error);
                                   [SVProgressHUD dismiss];
                               }
                           } andForceItemsIds:itemsToFetch];
    }
}

#pragma mark - store user data from registration/login

+ (void)saveUser:(NSDictionary *)data {
    User *me = [User MR_createEntity];
    me.userId = @([[data valueForKey:@"id"] intValue]);
    me.name = [data valueForKey:@"name"];
    me.email = [data valueForKey:@"email"];
    me.authKey = [data valueForKey:@"authKey"];

    if ([data valueForKey:@"createDate"] != [NSNull null] && [data valueForKey:@"createDate"] != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        me.createDate = [formatter dateFromString:[data valueForKey:@"createDate"]];
    }

    if ([data valueForKey:@"parentType"] == nil || [[data valueForKey:@"parentType"] isEqual:[NSNull null]]) {
        me.parentType = @(-1);
    } else {
        me.parentType = @([[data valueForKey:@"parentType"] intValue]);
    }

    if ([data valueForKey:@"otherTxt"] != nil && ![[data valueForKey:@"otherTxt"] isEqual:[NSNull null]]) {
        me.otherTxt = [data valueForKey:@"otherTxt"];
    }

    if ([data valueForKey:@"registrationReferralId"] != nil && ![[data valueForKey:@"registrationReferralId"] isEqual:[NSNull null]]) {

        [[Mixpanel sharedInstance] registerSuperProperties:@{@"registrationReferralId" : [data valueForKey:@"registrationReferralId"]}];


    }
    /*
    if ([data valueForKey:@"registrationTrackingSignature"] != nil && ![[data valueForKey:@"registrationTrackingSignature"] isEqual:[NSNull null]])
        me.registrationSignature = [data valueForKey:@"registrationTrackingSignature"];
    */
    me.isMe = @YES;

    if ([data valueForKey:@"birthdate"] != nil && ![[data valueForKey:@"birthdate"] isEqual:[NSNull null]]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z"];
        me.birthdate = [formatter dateFromString:[data valueForKey:@"birthdate"]];
    }

    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];

    [[UserManager sharedInstance] refreshMyData]; // this is a time hog!

    [[ServerComm instance] updateDeviceToken:^(id result) {

    }                           andFailBlock:^(NSError *error) {

    }];

    [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOGIN_NOTIFICATION object:nil];
}

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    //  [[UIColor colorWithRed:222./255 green:227./255 blue: 229./255 alpha:1] CGColor]) ;
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

+ (void)addFanPop:(NSDictionary *)userDict {
    if (userDict == nil || ![userDict isEqual:[NSDictionary class]]) {
        return;
    }
    
    NSString* email = userDict[@"email"];
    if (!email) { return; }
    
    NSArray *myKids = [[UserManager sharedInstance] myKids];
    if ([myKids count] == 0) {
        return;
    }

    NSArray *items = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids IN %@", myKids]];
    if ([items count] == 0) {
        return;
    }

    NSArray *fans = [Fan MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"email == %@", email]];
    if ([fans count] != 0) {
        return;
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction* yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
            Fan *afan = [Fan MR_createEntity];
            afan.nickname = [userDict valueForKey:@"name"];
            afan.email = email;
            EditFanViewController *avc = [[EditFanViewController alloc] initWithFan:afan];
            
            GlobalUtils *glUtils = [self sharedInstance];
            glUtils.navController = [[UINavigationController alloc] initWithRootViewController:avc];
            glUtils.navController.view.tag = 1;
            
            AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
            UIViewController* vc = d.viewController;
            if (vc.presentedViewController) {
                [vc.presentedViewController dismissViewControllerAnimated:YES completion:^{
                    [vc presentViewController:glUtils.navController animated:YES completion:nil];
                }];
            }
            else {
                [vc presentViewController:glUtils.navController animated:YES completion:nil];
            }
        } ];
        
        UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"add as fan?"
                                                                    message:[NSString stringWithFormat:@"would you like to invite %@ to be a fan of your kids?", [userDict valueForKey:@"name"]]
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:noAction];
        [ac addAction:yesAction];
        [ac presentWithAnimated:YES completion:nil];
    });
}

+ (void)requestPaymentForPlan:(KPPlan *)plan {
    
    if (plan.product == nil) {
        return;
    }
    
    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    [properties setValue:[self simplifiedSKUStringForPlan:plan] forKey:@"plan-type"];
    [properties setValue:plan.title forKey:@"plan-period"];
    [properties setValue:@"" forKey:@"coupon-code"];
    [[Mixpanel sharedInstance] track:@"my-keepies-choose-plan" properties:properties];
    
    SKPayment *payment = [SKPayment paymentWithProduct:plan.product];
    
    if ([SKPaymentQueue canMakePayments]) {
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"please make sure in-app purchases are allowed. go to Settings>General>Restrictions", @"")];
    }
}

+ (NSString *)simplifiedSKUStringForPlan:(KPPlan *)plan {
    NSString *planType = @"orange";
    NSArray *components = [plan.sku componentsSeparatedByString:@"."];
    if (components.count > 2) {
        planType = [components objectAtIndex:2];
    }
    return planType;
}


+ (void)showKeepyPopup:(NSDictionary *)data {
    
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"" message:data[@"body"] preferredStyle:UIAlertControllerStyleAlert];
    
    NSArray *buttons = data[@"buttons"];
    
    for (int i=0; i<buttons.count; i++) {
        NSDictionary *currentButtonDetails = buttons[i];
        UIAlertAction* action = [UIAlertAction actionWithTitle:currentButtonDetails[@"title"]
                                                         style:(i == buttons.count - 1) ? UIAlertActionStyleCancel : UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction* action) {
                                                           
                                                           if ([data valueForKey:@"name"] != nil) {
                                                               [[Mixpanel sharedInstance] track:[NSString stringWithFormat:@"%@ - %@ tap", [data valueForKey:@"name"], currentButtonDetails[@"title"]]];
                                                               
                                                           }
                                                           
                                                           //[[Analytics sharedAnalytics] track:@"popup-tap" properties:[NSDictionary dictionaryWithObjectsAndKeys:[sender titleForState:UIControlStateNormal], @"pop-btn", nil]];
                                                           
                                                           if ([data valueForKey:@"triggerId"] != nil) {
                                                               [GlobalUtils killTriggerPopCounter:[data valueForKey:@"triggerId"]];
                                                           }
                                                           
                                                           KPPlan *plan = currentButtonDetails[@"plan"];
                                                           if(plan && [plan isKindOfClass:[KPPlan class]]){
                                                               [self requestPaymentForPlan:plan];
                                                               return;
                                                           }
                                                           
                                                           NSString *aURL = currentButtonDetails[@"link"];
                                                           if (![aURL isEqualToString:@""]) {
                                                               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:aURL]];
                                                               //return;
                                                           }else{
                                                               
                                                               UIViewController *controller = currentButtonDetails[@"viewController"];
                                                               if(controller && [controller isKindOfClass:[UIViewController class]]){
                                                                   // let's pop it
                                                                   [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
                                                               }
                                                           }
                                                       }];
        [ac addAction:action];
    }
    
    [ac presentWithAnimated:YES completion:nil];
}

+ (void)showNoSpace:(UIViewController *)viewController {

    [[Mixpanel sharedInstance] track:@"you are out of space"];

    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    [[Mixpanel sharedInstance] track:@"max-keepies-reached" properties:properties];
    
    NSString *outOfSpaceMessage = @"You have already saved 15 awesome Keepies this month. \n\nYou can save more next month or subscribe now to save unlimited memories.";

    [KPPlan getPlansForCouponCode:nil resultBlock:^(NSArray *plans, NSError *error) {
    
        NSArray *skus = [plans valueForKey:@"sku"];
        
        [[StoreKitManager sharedManager] productsWithIdentifiers:[NSSet setWithArray:skus]
                                                      completion:^(SKProductsResponse * response, NSError * error)
         {
             if (error) {
                 NSLog(@"error: %@", error);
                 return;
             }
             
             for (KPPlan *plan in plans) {
                 plan.product = [response productWithIdentifier:plan.sku];
                 DDLogInfo(@"%@", plan.product.localizedPrice);
             }
            
            
            NSMutableArray *buttons = @[].mutableCopy;
            
            [buttons addObject:@{
                                 @"title": @"Next Month",
                                 @"link": @"",
                                 @"viewController": viewController
                                 }];
            
            
            for(KPPlan *currentPlan in plans){
                
                NSString *localizedPrice = currentPlan.product.localizedPrice;
                
                NSString *localizedPriceDescription;
                if([currentPlan.title isEqualToString:@"monthly"]){
                    localizedPriceDescription = [NSString stringWithFormat:@"%@/mo", localizedPrice];
                }else if([currentPlan.title isEqualToString:@"yearly"]){
                    localizedPriceDescription = [NSString stringWithFormat:@"%@/year", localizedPrice];
                }else{
                    localizedPriceDescription = [NSString stringWithFormat:@"%@", localizedPrice];
                }
                
                NSMutableDictionary *currentButton = @{}.mutableCopy;
                currentButton[@"plan"] = currentPlan;
                currentButton[@"title"] = localizedPriceDescription;
                currentButton[@"link"] = @"kpyme://upgrade";
                
                [buttons addObject:currentButton];
            }
            
            NSDictionary *d = @{@"id" : @(-1), @"body" : outOfSpaceMessage, @"buttons" : buttons, @"name" : @"no-space"};
            
            [self showKeepyPopup:d];
        }
         ];
    }];
}

+ (void)showUpgrade {

//    AccountViewController *avc = [[AccountViewController alloc] initWithNibName:@"AccountViewController" bundle:nil];

    //added by amitg, the above block commented out by amitg
    UnlimitedViewController *avc = [[UIStoryboard storyboardWithName:@"Unlimited" bundle:nil] instantiateViewControllerWithIdentifier:@"UnlimitedTable"];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    navController.view.tag = 1;
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;

    if (d.navController.presentedViewController != nil) {
        [d.navController.presentedViewController presentViewController:navController animated:YES completion:nil];
    } else {
        [d.window.rootViewController presentViewController:navController animated:YES completion:nil];
    }

}
+ (void)showUpgradeScreenWithParameters:(NSDictionary *)params fromViewController:(NSString *)fromVC {
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
//    [[Mixpanel sharedInstance] track: fromVC properties: params];

    UnlimitedViewController *avc = [[UIStoryboard storyboardWithName:@"Unlimited" bundle:nil] instantiateViewControllerWithIdentifier:@"UnlimitedTable"];
    avc.isFromAnotherVC = YES;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    if (d.navController.presentedViewController != nil) {
        [d.navController.presentedViewController presentViewController:navController animated:YES completion:nil];
    } else {
        [d.window.rootViewController presentViewController:navController animated:YES completion:nil];
    }
         });
}
+ (void)showTip:(NSInteger)tipIndex withView:(UIView *)parentView withCompleteBlock:(void (^)())completeBlock {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        if ([[userDefs objectForKey:[NSString stringWithFormat:@"tip%ld", (long)tipIndex]] intValue] != 0) {
            if (completeBlock) {
                completeBlock();
            }

            return;
        }

        [userDefs setObject:@(1) forKey:[NSString stringWithFormat:@"tip%ld", (long)tipIndex]];
        [userDefs synchronize];
    }

    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;

    if (parentView == nil) {
        parentView = [GlobalUtils topMostController].view;
    }

    float dy = 0;
    if (parentView == nil) {
        dy = 0;
        if (d.navController.presentedViewController != nil) {
            parentView = d.navController.presentedViewController.view;
        } else if (d.window.rootViewController.presentedViewController != nil) {
            parentView = d.window.rootViewController.presentedViewController.view;
            dy = 0;
        }
        else {
            parentView = d.window.rootViewController.view;
        }
    }

    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, dy, parentView.frame.size.width, parentView.frame.size.height)];
    aview.alpha = 0;

    //DDLogInfo(@"height:%2f ay:%2f", parentView.frame.size.height, parentView.frame.origin.y);
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0 - parentView.frame.origin.y, parentView.frame.size.width, parentView.frame.size.height + parentView.frame.origin.y - dy)];
    img.image = [UIImage imageNamed:[NSString stringWithFormat:@"tip%ld", (long)tipIndex]];
    [aview addSubview:img];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:[self sharedInstance] action:@selector(tipTap:)];
    [img addGestureRecognizer:gesture];
    img.userInteractionEnabled = YES;

    [[self sharedInstance] setTipCompleteBlock:completeBlock];

    [parentView addSubview:aview];

    [[self sharedInstance] setCurrentTip:aview];

    [UIView animateWithDuration:0.5 animations:^{
        aview.alpha = 1;
    }                completion:^(BOOL finished) {

    }];
}

- (void)tipTap:(UITapGestureRecognizer *)gesture {
    [UIView animateWithDuration:0.5 animations:^{
        self.currentTip.alpha = 0;
    }                completion:^(BOOL finished) {
        self.currentTip = nil;

        if (self.tipCompleteBlock) {
            self.tipCompleteBlock();
            self.tipCompleteBlock = nil;
        }

    }];
}

+ (void)hideAllTips {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        for (int i = 1; i < 11; i++) {
            [userDefs setObject:@(1) forKey:[NSString stringWithFormat:@"tip%d", i]];
        }

        [userDefs synchronize];
    }

}

+ (BOOL)checkCanInviteFans {
    BOOL needsPhoto = NO;

    // NSArray *myKids = [[UserManager sharedInstance] myKids];
    NSArray *myKids = [KPLocalKid fetchMine];
    if ([myKids count] == 0) {

        [[Mixpanel sharedInstance] track:@"fans tap: no kids"];


        AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
        [d.viewController showViewController:d.viewController.frontViewController];

        NSDictionary *dict = @{@"id" : @(-1), @"body" : KPLocalizedString(@"please add at least one kid before adding fans"), @"buttons" : @[@{@"title" : KPLocalizedString(@"cancel"), @"link" : @""}, @{@"title" : KPLocalizedString(@"add kid"), @"link" : @"kpyme://addkid?source=add-fan-popup"}], @"name" : @"fans-addkid"};
        [self showKeepyPopup:dict];

        return NO;
    }

    // NSArray *items = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids IN %@", myKids]];
    NSArray *myItemIDs = [KPLocalKidItem fetchItemIDsForKids:myKids];
    NSArray *items = [KPLocalItem idsToObjects:myItemIDs];
    
    if ([items count] == 0) {

        [[Mixpanel sharedInstance] track:@"fans tap: no photos"];


        needsPhoto = YES;

        AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
        [d.viewController showViewController:d.viewController.frontViewController];

        NSDictionary *dict = @{@"id" : @(-1), @"body" : KPLocalizedString(@"please upload at least one photo before adding fans"), @"buttons" : @[@{@"title" : KPLocalizedString(@"cancel"), @"link" : @""}, @{@"title" : KPLocalizedString(@"ok"), @"link" : @"kpyme://addphoto"}], @"name" : @"fans-addphoto"};
        [self showKeepyPopup:dict];

        return NO;
    }

    return YES;
}

+ (void)clearAssetsUpload {
    [[self sharedInstance] clearAssetsUpload];
}

- (void)clearAssetsUpload {
    self.uploadingAssets = nil;
    NSDictionary *r = @{@"count" : @(0), @"totalBytesWritten" : @(0), @"totalBytesExpectedToWrite" : @(0)};
    [[NSNotificationCenter defaultCenter] postNotificationName:UPLOAD_ASSETS_PROGRESS_NOTIFICATION object:r];
}

+ (void)updateAssetUpload:(NSInteger)assetId totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite {
    [[self sharedInstance] updateAssetUpload:assetId totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
}

- (void)updateAssetUpload:(NSInteger)assetId totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite {
    if (self.uploadingAssets == nil) {
        self.uploadingAssets = [[NSMutableDictionary alloc] init];
    }

    if (totalBytesExpectedToWrite == totalBytesWritten) {
        [self.uploadingAssets removeObjectForKey:[NSString stringWithFormat:@"%ld", (long)assetId]];
    }
    else {
        NSDictionary *d = @{@"totalBytesWritten" : @(totalBytesWritten), @"totalBytesExpectedToWrite" : @(totalBytesExpectedToWrite)};
        [self.uploadingAssets setValue:d forKey:[NSString stringWithFormat:@"%ld", (long)assetId]];
    }

    int c = 0;
    long tbytes = 0;
    long ebytes = 0;
    for (NSString *key in self.uploadingAssets) {
        NSDictionary *d = self.uploadingAssets[key];

        c++;
        tbytes += [[d valueForKey:@"totalBytesWritten"] longValue];
        ebytes += [[d valueForKey:@"totalBytesExpectedToWrite"] longValue];
    }

    NSDictionary *r = @{@"count" : @(c), @"totalBytesWritten" : @(tbytes), @"totalBytesExpectedToWrite" : @(ebytes)};
    [[NSNotificationCenter defaultCenter] postNotificationName:UPLOAD_ASSETS_PROGRESS_NOTIFICATION object:r];
}

+ (NSDictionary *)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
                [kv[1]
                        stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        params[kv[0]] = val;
    }
    return params;
}

#pragma mark - spread the love

+ (void)spreadTheLove {
    [[self sharedInstance] spreadTheLove];
}

- (void)spreadTheLove {
    SelectFanSourceViewController *avc = [[SelectFanSourceViewController alloc] initWithMode:1];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
    //[navController setNavigationBarHidden:YES];
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    if (d.viewController.presentedViewController) {
        [d.viewController.presentedViewController presentViewController:navController animated:YES completion:nil];
    } else {
        [d.viewController presentViewController:navController animated:YES completion:nil];
    }

}

+ (void)openSpreadLoveEmail:(NSArray *)recipients {
    [[self sharedInstance] openSpreadLoveEmail:recipients];
}

- (void)openSpreadLoveEmail:(NSArray *)recipients {


    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
        [mailComposeViewController setSubject:KPLocalizedString(@"You\'ve Got To Try Keepy!")];
        if (recipients != nil) {
            [mailComposeViewController setToRecipients:recipients];
        }

        [mailComposeViewController setBccRecipients:@[@"free-keepies@mg.keepy.me"]];

        [mailComposeViewController setMessageBody:[NSString stringWithFormat:KPLocalizedString(@"Hey!<br/><br/>I've been using <b>Keepy</b> to snap, share & save my kids\' art, schoolwork, mementos and more and it\'s great!  <br/><br/><a href=\"http://keepy.me/dl?r=u%d|SPREADTHELOVE\">CLICK HERE</a> <b>to download the FREE Keepy app!</b><br/><br/>It\'s private, but you can invite grandparents and loved ones to see everything you upload. Then, they can leave video comments on your kids\' stuff which is really sweet.<br/><br/>You\'ve got to check it out!<br/><br/>-%@<br/><br/>ps... If you use the link above and add just one photo (aka keepy), both you and I will get one month of unlimited keepies!"), [[UserManager sharedInstance] getMe].userId.intValue, [[UserManager sharedInstance] getMe].name] isHTML:YES];
        mailComposeViewController.mailComposeDelegate = self;
        mailComposeViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [[GlobalUtils topMostController] presentViewController:mailComposeViewController animated:YES completion:nil];
    }
    else if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.body = [NSString stringWithFormat:@"check out this awesome free app to save & share your kids\' art - http://keepy.me/dl?r=u%d|SPREADTHELOVE", [[UserManager sharedInstance] getMe].userId.intValue];
        controller.messageComposeDelegate = self;
        controller.modalPresentationStyle = UIModalPresentationCurrentContext;
        [[GlobalUtils topMostController] presentViewController:controller animated:YES completion:nil];
    } else {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"Can\'t send sms or email\nplease configure an account via Settings.app")];
    }
}

+ (UIViewController *)topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;

    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }

    return topController;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {

    switch (result) {
        case MFMailComposeResultCancelled:
            /*
 [[Mixpanel sharedInstance] track:@"spread tap - cancelled"];
*/
            break;

        case MFMailComposeResultSent:
            /*
 [[Mixpanel sharedInstance] track:@"spread tap - sent"];

             */

            if (!self.isOnFeedbackFlow) {
                [[[Mixpanel sharedInstance] people] increment:@"spread-the-love-invite-counter" by:@(1)];
                [[UserManager sharedInstance] setDidSpreadLove:YES];

                int64_t delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                    [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
                });
            }

            break;

        case MFMailComposeResultFailed:
            /*
 [[Mixpanel sharedInstance] track:@"spread tap - failed"];

             */
            break;

        case MFMailComposeResultSaved:
            /*
 [[Mixpanel sharedInstance] track:@"spread tap - saved"];
*/
            break;


    }

    [controller dismissViewControllerAnimated:YES completion:^{
        if (result == MFMailComposeResultSent) {
            AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
            [d.viewController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }];

    [[GlobalUtils sharedInstance] setIsOnFeedbackFlow:NO];

}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result; {
    [controller dismissViewControllerAnimated:YES completion:nil];

    switch (result) {
        case MessageComposeResultCancelled:
            [[Mixpanel sharedInstance] track:@"spread tap - cancelled"];

            break;

        case MessageComposeResultSent:
            [[Mixpanel sharedInstance] track:@"spread tap - sent"];


            [[UserManager sharedInstance] setDidSpreadLove:YES];

            int64_t delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
            });

            break;

        case MessageComposeResultFailed:
            [[Mixpanel sharedInstance] track:@"spread tap - failed"];

            break;

    }
}


#pragma mark - rate app

+ (void)rateAppOnStore {
    [[[Mixpanel sharedInstance] people] set:@"KeepyRateUs" to:@"love-app-appstore"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id647088205"]];
}

+ (void)showRateApp {
    [[[Mixpanel sharedInstance] people] set:@"KeepyRateUs" to:@"love-app"];
    
    if ([SKStoreReviewController class]) {
        [SKStoreReviewController requestReview];
    } else {
        NSDictionary *d = @{@"id" : @"rateapp", @"body" : KPLocalizedString(@"Aw, thanks! :-) Will you take a sec to rate Keepy in the app store?  Please!"), @"buttons" : @[@{@"title" : KPLocalizedString(@"ok"), @"link" : @"kpyme://rateapponstore"}, @{@"title" : KPLocalizedString(@"later"), @"link" : @""}], @"name" : @"rate-app"};
        [self showKeepyPopup:d];
    }
}

+ (void)showLoveApp {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"LaunchDates"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"LaunchDatesAfterPurchase"];
    
    NSDictionary *d = @{@"id" : @"loveapp", @"body" : KPLocalizedString(@"Hi! Do you like Keepy?"), @"buttons" : @[@{@"title" : KPLocalizedString(@"yes"), @"link" : @"kpyme://rateapp"}, @{@"title" : KPLocalizedString(@"no"), @"link" : @"kpyme://hateapp"}], @"name" : @"rate-app"};
    [self showKeepyPopup:d];
}

+ (void)showHateApp {
    [[[Mixpanel sharedInstance] people] set:@"KeepyRateUs" to:@"hate-app"];

    NSDictionary *d = @{@"id" : @"hateapp", @"body" : KPLocalizedString(@":-( Please let us know how we can improve Keepy to make it a better experience for you."), @"buttons" : @[@{@"title" : KPLocalizedString(@"ok"), @"link" : @"kpyme://sendfeedback"}, @{@"title" : KPLocalizedString(@"No, thanks"), @"link" : @""}], @"name" : @"rate-app"};
    [self showKeepyPopup:d];
}

+ (void)sendFeedback {
    [[[Mixpanel sharedInstance] people] set:@"KeepyRateUs" to:@"hate-app-feedback"];

    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;

    if ([MFMailComposeViewController canSendMail]) {

        [[self sharedInstance] setIsOnFeedbackFlow:YES];
        UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];

        MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
        [mailComposeViewController setSubject:KPLocalizedString(@"Keeper Feedback")];
        [mailComposeViewController setToRecipients:@[@"tickets@keepy.uservoice.com"]];
        mailComposeViewController.mailComposeDelegate = [self sharedInstance];
        mailComposeViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [vc presentViewController:mailComposeViewController animated:YES completion:nil];
//        [d.navController presentViewController:mailComposeViewController animated:YES completion:nil];
    }
    else {
        UVConfig *config = [UVConfig configWithSite:@"keepy.uservoice.com"
                                             andKey:@"xlgORbPAktc4kuuIKvaIA"
                                          andSecret:@"dOMs8y7cMcntxnPyxiJILmGhYZqrJbY7mv5rcfM0" andEmail:[[UserManager sharedInstance] getMe].email andDisplayName:[[UserManager sharedInstance] getMe].name andGUID:[NSString stringWithFormat:@"%d", [[UserManager sharedInstance] getMe].userId.intValue]];

        [UserVoice presentUserVoiceInterfaceForParentViewController:d.navController andConfig:config];
    }
}
+ (UIViewController *)visibleViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
    {
        return rootViewController;
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        return [self visibleViewController:lastViewController];
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController.presentedViewController;
        UIViewController *selectedViewController = tabBarController.selectedViewController;
        
        return [self visibleViewController:selectedViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    
    return [self visibleViewController:presentedViewController];
}

#pragma mark - triggers

+ (BOOL)fireTrigger:(NSInteger)triggerType {
    // Let's not show any notifications at all, until we're done with the onboarding.
    if ([[UserManager sharedInstance] isAfterRegistration]) {
        return NO;
    }

    NSArray *triggers = [[UserManager sharedInstance] triggers];
    if (triggers == nil) {
        return NO;
    }

    NSArray *paramsValues = [self getParamsValuesForTriggers];

    // DDLogInfo(@"%@",paramsValues);

    /*
    if (triggerType == TRIGGER_TYPE_ADDPHOTO && [[paramsValues objectAtIndex:TRIGGER_PARAM_PHOTOSCOUNT]intValue]==1)
    {
        [TapjoyConnect actionComplete:@"1fa5ec16-3fd5-4009-94a4-f68faf47cf34"];
    }
    else if (triggerType == TRIGGER_TYPE_ADDSTORY && [[paramsValues objectAtIndex:TRIGGER_PARAM_STORIESCOUNT]intValue]==1)
    {
        [TapjoyConnect actionComplete:@"1d969637-5854-4758-b5ab-91973b4bcedd"];
    }
    else if (triggerType == TRIGGER_TYPE_ADDCOMMENT && [[paramsValues objectAtIndex:TRIGGER_PARAM_COMMENTSCOUNT]intValue]==1)
    {
        [TapjoyConnect actionComplete:@"18b2613b-3431-43e4-97fa-98e83ef3ed7f"];
    }
    */

    for (NSDictionary *trigger in triggers) {

        // DDLogInfo(@"%@",trigger);

        if ([[trigger valueForKey:@"type"] intValue] == triggerType) {
            NSArray *conditions = [trigger valueForKey:@"conditions"];
            BOOL isConditionMet = NO;
            for (NSArray *subConditions in conditions) {
                BOOL isSubConditionMet = YES;
                for (NSDictionary *subCondition in subConditions) {
                    int pid = [[subCondition valueForKey:@"pid"] intValue];
                    int c = [[subCondition valueForKey:@"c"] intValue];
                    int v = [[subCondition valueForKey:@"v"] intValue];

                    BOOL b = ((c == TRIGGER_COMPARER_EQUAL && [paramsValues[pid] intValue] == v) ||
                            (c == TRIGGER_COMPARER_SMALLERTHAN && [paramsValues[pid] intValue] < v) ||
                            (c == TRIGGER_COMPARER_GREATERTHAN && [paramsValues[pid] intValue] > v));

                    isSubConditionMet = isSubConditionMet && b;
                }

                isConditionMet = isConditionMet || isSubConditionMet;
            }

            if (isConditionMet) {
                NSInteger daysPassed = [self getTriggerDaysPassed:[trigger valueForKey:@"id"]];
                if (daysPassed == -1 || daysPassed >= [[trigger valueForKey:@"sleepDays"] intValue]) {
                    int tc = [self increaseTriggerPopCounter:[trigger valueForKey:@"id"] withFrequency:[[trigger valueForKey:@"frequency"] intValue]];
                    if (tc == 1) {
                        NSMutableDictionary *popData = [[NSMutableDictionary alloc] initWithDictionary:[trigger valueForKey:@"popData"]];
                        if ([[trigger valueForKey:@"frequency"] intValue] > 0) {
                            [popData setValue:[trigger valueForKey:@"id"] forKey:@"triggerId"];
                        }

                        [self setTriggerLastPopDate:[trigger valueForKey:@"id"]];
                        if ([popData valueForKey:@"fansPopup"] != nil) {
                        }
                        else {
                            if ([[trigger valueForKey:@"id"] isEqualToString:@"rateapp"]) {
                                [GlobalUtils showLoveApp];
                            }
                            else {
                                [self showKeepyPopup:popData];
                            }
                        }
                        return YES;
                    }
                }
            }
        }
    }

    return NO;
}

+ (NSArray *)getParamsValuesForTriggers {
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:5];
    for (int i = 0; i < 5; i++) {
        [result addObject:@(0)];
    }

    NSArray *myKids = [[UserManager sharedInstance] myKids];
    NSArray *myFans = [[UserManager sharedInstance] myFans];

    NSArray *items = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids IN %@", myKids]];
    NSArray *stories = [Story MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"item IN %@", items]];
    NSArray *comments = [Comment MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"user == %@", [[UserManager sharedInstance] getMe]]];

    result[TRIGGER_PARAM_PHOTOSCOUNT] = @([items count]);
    result[TRIGGER_PARAM_KIDSCOUNT] = @([myKids count]);
    result[TRIGGER_PARAM_FANSCOUNT] = @([myFans count]);
    result[TRIGGER_PARAM_STORIESCOUNT] = @([stories count]);
    result[TRIGGER_PARAM_COMMENTSCOUNT] = @([comments count]);

    return result;
}

+ (int)increaseTriggerPopCounter:(NSString *)triggerId withFrequency:(int)frequency {
    int result = 0;

    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        result = [[userDefs objectForKey:[NSString stringWithFormat:@"triggerPopCounter-%@", triggerId]] intValue];
        if (result > -1) {
            if (result == abs(frequency)) {
                result = 1;
            } else {
                result++;
            }


            [userDefs setObject:@(result) forKey:[NSString stringWithFormat:@"triggerPopCounter-%@", triggerId]];
            [userDefs synchronize];
        }
    }

    return result;
}

+ (NSInteger)getTriggerDaysPassed:(NSString *)triggerId {
    NSInteger result = 0;

    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        if ([userDefs objectForKey:[NSString stringWithFormat:@"triggerLastPop-%@", triggerId]] == nil) {
            return -1;
        }

        NSDate *lastPopDate = (NSDate *) [userDefs objectForKey:[NSString stringWithFormat:@"triggerLastPop-%@", triggerId]];
        NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                components:NSCalendarUnitDay
                  fromDate:lastPopDate
                    toDate:[NSDate date]
                   options:0];
        result = [ageComponents day];
    }

    return result;
}

+ (void)setTriggerLastPopDate:(NSString *)triggerId {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:[NSDate date] forKey:[NSString stringWithFormat:@"triggerLastPop-%@", triggerId]];
        [userDefs synchronize];
    }
}

+ (void)killTriggerPopCounter:(NSString *)triggerId {
    NSUserDefaults *userDefs = [NSUserDefaults standardUserDefaults];
    if (userDefs) {
        [userDefs setObject:@(-1) forKey:[NSString stringWithFormat:@"triggerPopCounter-%@", triggerId]];
        [userDefs synchronize];
    }
}

#pragma mark - current item

+ (void)storeCurrentItemData:(NSDictionary *)itemDict {
    [[self sharedInstance] setCurrentItemData:itemDict];
}


+ (NSDictionary *)getCurrentItemData {
    return [[self sharedInstance] currentItemData];
}

+ (void)upladCurrentItem:(void (^)(id result))completeBlock {
    /*
    [SVProgressHUD showWithStatus:KPLocalizedString(@"adding item")];
    

    [[Mixpanel sharedInstance] track:@"add item request"];

    
    int placeId = [[[[self sharedInstance] currentItemData] valueForKey:@"placeId"] intValue];
    
    NSDictionary *imageInfo = [[[self sharedInstance] currentItemData] valueForKey:@"imageInfo"];
    int kidId = [[[[self sharedInstance] currentItemData] valueForKey:@"kidId"] intValue];
    NSString *itemTitle = [[[self sharedInstance] currentItemData] valueForKey:@"itemTitle"];
    NSDate *itemDate = [[[self sharedInstance] currentItemData] valueForKey:@"itemDate"];
    NSData *storyRecordingData = [[[self sharedInstance] currentItemData] valueForKey:@"storyRecordingData"];
    int recordingLength = [[[[self sharedInstance] currentItemData] valueForKey:@"recordingLength"] intValue];
    
    [[ServerComm instance] addItem:kidId withTitle:itemTitle andDate:itemDate andPlace:placeId andCropRect:[GlobalUtils createCropRectStr:[imageInfo valueForKey:@"points"]] andOriginalAssetId:[[imageInfo valueForKey:@"assetId"] intValue] andRotation:[[imageInfo valueForKey:@"rotation"] floatValue] andSat:[[imageInfo valueForKey:@"saturation"] floatValue] andCon:[[imageInfo valueForKey:@"contrast"] floatValue] andItemType:0 andExtraData:@"" withSuccessBlock:^(id result) {
        
        if ([[result valueForKey:@"status"] intValue] == 0)
        {
            //Save local files with assets ids
            
            [[SDImageCache sharedImageCache] storeImage:(UIImage*)[imageInfo valueForKey:@"originalImage"] forKey:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"originalAssetHash"] ]];
            
            [[SDImageCache sharedImageCache] storeImage:(UIImage*)[imageInfo valueForKey:@"resultImage"] forKey:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
            
            [[SDImageCache sharedImageCache] storeImage:(UIImage*)[imageInfo valueForKey:@"resultImage"] forKey:[NSString stringWithFormat:@"%@%@_iphone.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
            
            
            //save thumbnails
            UIImage *smallImage = [(UIImage*)[imageInfo valueForKey:@"resultImage"] imageScaledToFitSize:CGSizeMake(192, 192)];
            
            [[SDImageCache sharedImageCache] storeImage:smallImage forKey:[NSString stringWithFormat:@"%@%@_small.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
            
            UIImage *mediumImage = [(UIImage*)[imageInfo valueForKey:@"resultImage"] imageScaledToFitSize:CGSizeMake(600, 600)];
            [[SDImageCache sharedImageCache] storeImage:mediumImage forKey:[NSString stringWithFormat:@"%@%@_medium.jpg", [[KPDefaults sharedDefaults] assetsURLString], [[result valueForKey:@"result"] valueForKey:@"assetHash"]]];
            
            //Add new Item to local database
            NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
            [itemData setValue:itemDate forKey:@"itemDate"];
            [itemData setValue:[GlobalUtils createCropRectStr:[imageInfo valueForKey:@"points"]] forKey:@"cropRect"];
            [itemData setValue:@(kidId) forKey:@"kidId"];
            if (itemTitle.length)
                [itemData setValue:itemTitle forKey:@"title"];
            
            [itemData setValue:[imageInfo valueForKey:@"saturation"] forKey:@"sat"];
            [itemData setValue:[imageInfo valueForKey:@"contrast"] forKey:@"con"];
            [itemData setValue:[imageInfo valueForKey:@"rotation"] forKey:@"rotation"];
            
            
            Item *item = [[KPDataParser instance] addNewItem:[result valueForKey:@"result"] andData:itemData];
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            
 [[Mixpanel sharedInstance] track:@"add photo"];

            
            if (storyRecordingData != nil)
            {
                [[ServerComm instance] uploadStory:item.itemId.intValue withStoryLength:recordingLength withData:storyRecordingData withSuccessBlock:^(id result) {
                    int status = [[result valueForKey:@"status"] intValue];
                    if (status == 0)
                    {
                        Asset *asset = [Asset MR_createEntity];
                        asset.assetId.intValue = [[result valueForKey:@"assetId"] intValue];
                        asset.assetHash = [result valueForKey:@"hash"];
                        asset.assetType.intValue = 2;
                        
                        Story *story = [Story MR_createEntity];
                        story.storyId.intValue = [[result valueForKey:@"storyId"] intValue];
                        story.storyLength.floatValue = recordingLength;
                        story.asset = asset;
                        
                        item.story = story;
                        
                        [[ServerComm instance] saveAssetData:storyRecordingData withFileExt:@"mp4" andAssetHash:asset.urlHash];
                        
                        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                        
             [[[Mixpanel sharedInstance] people] increment:@"Number of stories" by:@(1)];
                        [[Mixpanel sharedInstance] track:@"story added"];

                        [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION  object:item];
                        
                        [SVProgressHUD dismiss];
                        
                        [[self sharedInstance] setCurrentItemData:nil];
                        if ([[self sharedInstance] currentCommentData] != nil)
                        {
                            [[self sharedInstance] uploadCurrentComment:item andCompleteBlock:completeBlock];
                        }
                        else
                        {
                            if (completeBlock)
                                completeBlock(item);
                        }
                    }
                    else
                    {
                        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error uploading story")];
                    }
                } andFailBlock:^(NSError *error) {
                    [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
                } andProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                    //DDLogInfo(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
                    [SVProgressHUD showProgress:(float)totalBytesWritten/(float)totalBytesExpectedToWrite status:KPLocalizedString(@"uploading story")];
                }];
            }
            else
            {
     [[[Mixpanel sharedInstance] people] increment:@"Number of photos" by:@(1)];

                [[NSNotificationCenter defaultCenter] postNotificationName:ITEM_ADDED_NOTIFICATION  object:item];
                
                [SVProgressHUD dismiss];
                [[self sharedInstance] setCurrentItemData:nil];
                if ([[self sharedInstance] currentCommentData] != nil)
                {
                    [[self sharedInstance] uploadCurrentComment:item andCompleteBlock:completeBlock];
                }
                else
                {
                    if (completeBlock)
                        completeBlock(item);
                }
                
            }
        }
        else if ([[result valueForKey:@"status"] intValue] == 2) //no more space...
        {
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [GlobalUtils showNoSpace];
            });
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"error adding item")];
        }
        
    } andFailBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
    }];    
     */
}

+ (void)storeCurrentCommentData:(NSDictionary *)commentData {
    [[self sharedInstance] setCurrentCommentData:commentData];
}

- (void)uploadCurrentComment:(Item *)item andCompleteBlock:(void (^)(id result))completeBlock {
}

+ (void)postNotificationForNewItem:(KPLocalItem *)item {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MAIPCSuccess" object:nil];

    int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        if (![self fireTrigger:TRIGGER_TYPE_ADDPHOTO] && item.storyID > 0) {
            [self fireTrigger:TRIGGER_TYPE_ADDSTORY];
        }
    });
}

+ (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    [ALAssetsLibrary disableSharedPhotoStreamsSupport];
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    
    return library;
}

#if TO_BE_DELETED
+ (void)shareLoveWithFacebook {
    [[UserManager sharedInstance] requireFacebook:^(id result) {
        NSString *linkURL = [NSString stringWithFormat:@"http://keepy.me/dl?r=u%d|SPREADTHELOVE", [[UserManager sharedInstance] getMe].userId.intValue];
        NSString *pictureURL = @"http://keepy.me/images/fb-share-app.png";

        // Prepare the native share dialog parameters
        FBLinkShareParams *shareParams = [[FBLinkShareParams alloc] init];
        shareParams.link = [NSURL URLWithString:@"https://developers.facebook.com/docs/ios/share/"];
        shareParams.link = [NSURL URLWithString:linkURL];
        shareParams.name = KPLocalizedString(@"Checkout keepy!");
        shareParams.caption = KPLocalizedString(@"if you register to keepy we both get one month of unlimited keepies!");
        shareParams.picture = [NSURL URLWithString:pictureURL];
        shareParams.linkDescription = KPLocalizedString(@"Hey! I'm using the Keepy app to save my kids art, schoolwork, and mementos in an awesome, interactive and private timeline.<br/><br/>You can invite grandparents and loved ones to see everything you upload and they can leave video comments on it.<br/><br/>It's really cool.<br/><br/>  Download Keepy for FREE!<br/><br/>ps...if you do, and add just one photo (aka keepy), both you and I will get one month of unlimited keepies!");


        if (NO) {//[FBDialogs canPresentShareDialogWithParams:shareParams]){

            [FBDialogs presentShareDialogWithParams:shareParams
                                        clientState:nil
                                            handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                                if (error) {
                                                    DDLogInfo(@"Error publishing story.");
                                                } else if (results[@"completionGesture"] && [results[@"completionGesture"] isEqualToString:@"cancel"]) {
                                                    DDLogInfo(@"User canceled story publishing.");
                                                } else {
                                                    DDLogInfo(@"Story published.");

                                                    [[UserManager sharedInstance] setDidSpreadLove:YES];

                                                    int64_t delayInSeconds = 1.0;
                                                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                                                        [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
                                                    });

                                                }
                                            }];

        } else {

            FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
            params.link = [NSURL URLWithString:linkURL];
            //if ([FBDialogs canPresentShareDialogWithParams:params])
            if (NO) {
                [FBDialogs presentShareDialogWithLink:[NSURL URLWithString:linkURL]
                                              handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                                  if (error) {
                                                      DDLogInfo(@"Error: %@", error.description);
                                                  } else {
                                                      DDLogInfo(@"Success!");

                                                      [[UserManager sharedInstance] setDidSpreadLove:YES];

                                                      int64_t delayInSeconds = 1.0;
                                                      dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                                      dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                                                          [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
                                                      });

                                                  }
                                              }];
            }
            else {
                // Prepare the web dialog parameters
                NSDictionary *params = @{
                        @"name" : shareParams.name,
                        @"caption" : shareParams.caption,
                        @"description" : shareParams.description,
                        @"picture" : pictureURL,
                        @"link" : linkURL
                };

                // Invoke the dialog
                [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                                       parameters:params
                                                          handler:
                                                                  ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                                      if (error) {
                                                                          DDLogInfo(@"Error publishing story.");
                                                                      } else {
                                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                                              DDLogInfo(@"User canceled story publishing.");
                                                                          } else {
                                                                              DDLogInfo(@"Story published.");

                                                                              [[UserManager sharedInstance] setDidSpreadLove:YES];

                                                                              int64_t delayInSeconds = 1.0;
                                                                              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                                                              dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

                                                                                  [GlobalUtils fireTrigger:TRIGGER_TYPE_SPREADTHELOVE];
                                                                              });

                                                                          }
                                                                      }
                                                                  }];
            }
        }
    }];
}

#endif

+ (BOOL)checkHasSpace {
    /*
    if ([[[UserManager sharedInstance] getMe] planType.intValue] != 0)
        return YES;
    
    if ([[UserManager sharedInstance] getMe].keepyMonthlySpace.intValue <= [[Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]] count])
    {
        [GlobalUtils showNoSpace];
        return NO;
    }*/

    return YES;
}

+ (void)updateAnalyticsUser {

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSUInteger myKidsCount = [[[UserManager sharedInstance] myKids] count];
        
        NSArray *myKids = [KPLocalKid fetchMine];
        myKidsCount = myKids.count;
        
        NSArray *allKids = [Kid MR_findAll];
        allKids = [KPLocalKid fetchAll];

        NSArray *allKeepies = [Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]];
        NSArray *allKeepyIDs = [KPLocalKidItem fetchItemIDsForKids:myKids];
        
        NSArray *allStories = [Story MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"item in %@", allKeepies]];
        NSArray *allFans = [Fan MR_findByAttribute:@"didApprove" withValue:@(YES)];
        
        User* me = [[UserManager sharedInstance] getMe];
        
        NSMutableDictionary *p = [NSMutableDictionary new];
        p[@"$email"] = me.email;
        p[@"$name"] = me.name;
        p[@"parentType"] = [GlobalUtils getUserParentTypeStr:me];
        p[@"isCreator"] = @(myKidsCount > 0);
        p[@"isFan"] = @([allKids count] > myKidsCount + 1);
        
        p[@"keepies"] = @([allKeepies count]);
        p[@"keepies"] = @(allKeepyIDs.count);
        
        p[@"kids"] = @(myKidsCount);
        
        p[@"voice-overs"] = @([allStories count]);
        p[@"fans-invites"] = @([[[UserManager sharedInstance] myFans] count]);
        p[@"fans"] = @([allFans count]);
        p[@"dropbox-linked"] = @(DBClientsManager.authorizedClient || DBClientsManager.authorizedTeamClient);
        p[@"available-keepies"] = @([[UserManager sharedInstance] getMe].keepySpace.intValue - [allKeepies count]);
        p[@"keepies-space"] = @([[UserManager sharedInstance] getMe].keepySpace.intValue);
        p[@"facebookId"] = me.facebookId ? me.facebookId : @"";
        p[@"twitterHandle"] = me.twitterHandle ? me.twitterHandle : @"";
        p[@"feed-mode"] = ([[UserManager sharedInstance] feedMode] == 0) ? @"feed" : @"gallery";
        p[@"tfurl"] = [NSString stringWithFormat:@"http://keepy.me/xdl/tf.html?uid=%d", me.userId.intValue];
        
        p[@"printio-version"] = [PrintIOManager getPrintIOVersion] ?: @"N/A";
        p[@"$created"] = me.createDate ?: @"N/A";
        
        [[Mixpanel sharedInstance] createAlias:me.email forDistinctID:[Mixpanel sharedInstance].distinctId];
        [[Mixpanel sharedInstance] identify:me.email];
        
        for (NSString *key in p.allKeys) {
            [[[Mixpanel sharedInstance] people] set:key to:[p valueForKey:key]];
        }
        [[Mixpanel sharedInstance] registerSuperProperties:p];
        
        [[LaunchKit sharedInstance] setUserIdentifier:me.userId.description email:me.email name:me.name];
    });
}

+ (BOOL)isConnected {
    AppDelegate *d = (AppDelegate *) [UIApplication sharedApplication].delegate;
    return d.isConnected;
}

#pragma mark - Share

+ (void)facebookLogin:(NSArray<NSString*>*)permissions viewController:(UIViewController*)viewController completion:(void(^)(id result, NSError* error))completion {
    FBSDKLoginManager* fb = [FBSDKLoginManager new];
    [fb logInWithPublishPermissions:permissions
#if FBSDK_46
                 fromViewController:viewController
#endif
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    completion(nil, error);
                                } else {
                                    if (result.isCancelled) {
                                        completion(nil, nil);
                                    } else {
                                        NSString* tokenString = result.token.tokenString;
                                        NSString* userID = result.token.userID;
                                        [[ServerComm new] updateFacebookTokens:userID
                                                        andFacebookAccessToken:tokenString
                                                      andHasPublishPermissions:YES
                                                               andSuccessBlock:^(id result) {
                                                                   completion(result, nil);
    
                                                               } andFailBlock:^(NSError *error) {
                                                                   completion(nil, error);
                                                               }
                                         ];
                                    }
                                }
                            }];
}

+ (void)twitterLoginWithCompletion:(void(^)(id result, NSError* error))completion {
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            [[ServerComm new] updateTwitterTokens:session.userID
                                            token:session.authToken
                                           secret:session.authTokenSecret
                                           handle:session.userName
                                   andSuccessBlock:^(id result) {
                                       completion(result, nil);
                                   } andFailBlock:^(NSError *error) {
                                       completion(nil, error);
                                   }
             ];
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
            completion(nil, error);
        }
    }];
}
@end


#pragma mark - UINavigationController (autoRotate)

@implementation UINavigationController (autoRotate)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return [self.topViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

- (BOOL)shouldAutorotate {
    return [self.visibleViewController shouldAutorotate];
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (![[self.viewControllers lastObject] isKindOfClass:NSClassFromString(@"ViewController")]) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else {
        return [self.topViewController supportedInterfaceOrientations];
    }
}

@end

static NSArray<NSString*>* GetItemIdsFromResponse(NSDictionary* response) {
    NSMutableArray* arr = [NSMutableArray array];
    
    NSDictionary* result = response[@"result"];
    if ([result isKindOfClass:[NSDictionary class]]) {
        NSArray* items = result[@"items"];
        if ([items isKindOfClass:[NSArray class]]) {
            for (NSDictionary* i in items) {
                if (![i isKindOfClass:[NSDictionary class]]) { continue; }
                
                NSNumber* itemId = i[@"id"];
                if ([itemId isKindOfClass:[NSNumber class]]) {
                    [arr addObject:itemId.stringValue];
                }
            }
        }
    }

    return arr;
}

static void ShowItemsInFullScreen(NSArray<NSString*>* itemIds, NSInteger commentId) {
    NSMutableArray* arr = [NSMutableArray new];
    for (NSString* i in itemIds) {
        KPLocalItem* item = [KPLocalItem fetchByServerID:i.intValue];
        if (item) {
            [arr addObject:item];
        }
    }
    
    if (arr.count > 0) {
        [[GlobalUtils sharedInstance] showItemsInFullScreen:arr withIndex:0 andCommentId:commentId];
    }
}
