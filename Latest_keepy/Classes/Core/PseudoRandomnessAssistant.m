//
//  PseudoRandomnessAssistant.m
//  Keepy
//
//  Created by Arik Sosman on 6/10/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "PseudoRandomnessAssistant.h"

#import <CommonCrypto/CommonDigest.h>

@implementation PseudoRandomnessAssistant

+ (NSInteger)obtainPseudoRandomValueForDictionary:(NSDictionary *)dictionary withinMinimum:(NSInteger)min andMaximum:(NSInteger)max{
    
    // first of all, let's obtain the pseudo random input
    NSString *hash = [self randomizeInputDictionary:dictionary];
    
    NSInteger actualMinimum = MIN(min, max);
    NSInteger actualMaximum = MAX(min, max);
    
    NSInteger delta = actualMaximum-actualMinimum + 1; // we add the plus one because the modulus would not allow the last value
    
    
    NSString *substring = [hash substringToIndex:16];
    NSScanner *scanner = [NSScanner scannerWithString:substring];
    
    unsigned long long numericValue;
    [scanner scanHexLongLong:&numericValue];
    // let's take a 16-
 
    NSInteger remainder = numericValue % delta;
    NSInteger finalOutput = actualMinimum + remainder;
    
    return finalOutput;
    
}

+ (NSString *)randomizeInputDictionary:(NSDictionary *)dictionary{ // this dictionary should consist of strings
    
    // let's convert the dictionary to a string
    
    NSArray *unsortedKeys = dictionary.allKeys;
    NSArray *sortedKeys = [unsortedKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    // now we have the case insensitive comparison
    NSString *inputString = @"";
    
    for(NSString *key in sortedKeys){
        
        NSString *value = dictionary[key];
        if(![value isKindOfClass:[NSString class]]){
            @throw [[NSException alloc] initWithName:@"UnsupportedArgumentException" reason:@"All dictionary components passed to the pseudo-randomizer must be strings." userInfo:nil];
        }
        
        
        NSDictionary *currentDictionary = @{key: value};
        NSData *currentJSON = [NSJSONSerialization dataWithJSONObject:currentDictionary options:0 error:nil];
        NSString *currentJSONString = [currentJSON base64EncodedStringWithOptions:0];
        
        inputString = [NSString stringWithFormat:@"%@|%@", inputString, currentJSONString];
        
        
    }
    
    return [self sha256:inputString];
    
}

+ (NSString *)sha256:(NSString *)input{
    
    if(!input){
        return nil;
    }
    
    const char *s=[input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(keyData.bytes, (CC_LONG)keyData.length, digest);
    NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash=[out description];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    return hash;
}

@end
