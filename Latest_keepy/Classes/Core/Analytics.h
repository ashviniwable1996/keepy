//
//  Analytics.h
//  Keepy
//
//  Created by Yaniv Solnik on 8/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Analytics : NSObject

+ (instancetype)sharedAnalytics;

- (void)track:(NSString *)eventId;

- (void)track:(NSString *)eventId properties:(NSDictionary *)properties;

@property(nonatomic, strong) NSDictionary *defaultProperties;

@end
