//
//  UserManager.h
//  solnik
//
//  Created by Yaniv Solnik on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerComm.h"
#import "User.h"
#import "Kid.h"

@class KPLocalKid;

//typedef void (^FBLoginBlock)();
typedef void (^CompleteWithResultBlock)(id result);

typedef void (^KPFacebookAccessTokenBlock)(NSString *accessToken, NSError *error);

@interface UserManager : NSObject {
    CompleteWithResultBlock fbLoginBlock;
    CompleteWithResultBlock fbLoginFailedBlock;
}

@property(nonatomic, strong) KPLocalKid *selectedKid;
@property(nonatomic, strong) Kid *keepyKid;
@property(nonatomic) int feedMode;
@property(nonatomic) int cameraMode;
@property(nonatomic, strong) NSArray *triggers;
@property(nonatomic) BOOL facebookShare;
@property(nonatomic) BOOL twitterShare;
@property(nonatomic) BOOL pinterestShare;
@property(nonatomic) BOOL keepyFBShare;
@property(nonatomic) BOOL didSpreadLove;
@property(nonatomic) BOOL approvedKeepyGalleryShare;
@property(nonatomic, strong) NSString *facebookAdsId;
@property(nonatomic) BOOL shareWithFans;
@property(nonatomic) BOOL isAfterLogin;
@property(nonatomic) BOOL isAfterOnBoardProccess;
@property(nonatomic) BOOL isAfterNewKid;
@property(nonatomic) BOOL isAfterRegistration;
@property(nonatomic, strong) NSArray *dynamicMessages;


+ (instancetype)sharedInstance;

- (void)logout;

- (User *)getMe;

- (User *)getMe:(NSManagedObjectContext *)moc;

- (BOOL)isLoggedIn;

- (void)refreshMyData;

- (NSArray *)myKids;

- (NSArray *)myPlaces;

- (NSArray *)myFans;

- (BOOL)isMyKid:(Kid *)kid;
//global utils
//+ (BOOL)validateEmail:(NSString *)email;

- (void)requireFacebookFromViewController:(UIViewController*)viewController success:(CompleteWithResultBlock)success failure:(CompleteWithResultBlock)failure;

- (void)getFBFriends:(CompleteWithResultBlock)completeBlock;

- (void)getFBFamily:(CompleteWithResultBlock)completeBlock;

- (void)updateServerWithFBTokens;

- (void)fbDidLogin;

- (void)appInviteFromViewController:(UIViewController*)viewController;

@end
