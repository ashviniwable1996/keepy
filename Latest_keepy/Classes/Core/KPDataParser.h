//
//  KPDataParser.h
//  Keepy
//
//  Created by Yaniv Solnik on 1/15/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "User.h"
#import "Kid.h"
#import "Item+KP.h"
#import "Asset+KP.h"
#import "Story.h"
#import "Comment.h"
#import "Album.h"
#import "Fan.h"
#import "KPNotification.h"
#import "Tag.h"

@class KPLocalAsset;
@class KPLocalItem;

typedef void (^KPCompleteBlock)();

@interface KPDataParser : NSObject

+ (instancetype)instance;

- (void)parseKids:(NSArray *)kids complete:(KPCompleteBlock)complete;

//- (void)parseItems:(NSArray *)items;

- (void)parsePlaces:(NSArray *)places;

- (void)parseFans:(NSArray *)fans;

- (void)parseNotifications:(NSArray *)notifications;

- (void)parseTags:(NSArray *)tags;

- (KPLocalAsset *)createAssetById:(NSInteger)assetId withType:(NSInteger)assetType withHash:(NSString *)assetHash;

- (KPLocalItem *)addNewItem:(NSDictionary *)result andData:(NSDictionary *)itemData;

@end
