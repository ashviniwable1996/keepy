typedef struct CvLinePolar {
    float rho;
    float angle;
} CvLinePolar;

typedef struct {
    int irho;
    int iangle;
    int inum;
    int base;
} HLINE;

#define Pi     ((float)CV_PI)

#define EDGEVAL 240
#define EDGEDEC 120

enum QuadErrorCode {
    QUAD_OK, QUAD_NoQuad, QUAD_FarCorners, QUAD_ThinShape, QUAD_FixFailed, QUAD_UnknownError
};

typedef struct {
    float tBest;
    float tRect;
    float tFltr;
    float tEdge;
} TIMEVEC;

//EXTERN TIMEVEC tVec;

typedef unsigned char byte;

// Interface

QuadErrorCode BestQuad(cv::Mat big, CvPoint2D32f Q[4], int hue_edges);

cv::Mat RectQuad(cv::Mat inp, CvPoint2D32f srcQuad[4], int decFactor);

cv::Mat myDDS(cv::Mat inp, double k);

void myContrastInit(cv::Mat clr, double *gmax);

cv::Mat myContrastStretch(cv::Mat clr, double gain);

//cv::Mat myContrast(cv::Mat inp, double k);
cv::Mat kpResize(cv::Mat inp, int wmax, int hmax, double *ratio);

void myShowImage(char *caption, cv::Mat img);

/*
QuadErrorCode BestQuad(IplImage *img, CvPoint2D32f Quad[4], int hue_edges);
IplImage *RectQuad(IplImage *inp, CvPoint2D32f srcQuad[4], int decFactor);
QuadErrorCode PointQuad(IplImage *img, CvPoint2D32f Quad[4]);

IplImage *Luminate(IplImage *clr);
IplImage *imgResize(IplImage *inp, int wmax, int hmax, double *ratio);
void myShowImage(char *caption, IplImage *img);

//IplImage *AutoColor(IplImage *clr, int LumOnly, float pLow, float pHgh, float gain);
IplImage *myImageContrastFilter(IplImage *clr, float gain, int force_gain);
IplImage *myImageSaturationFilter(IplImage *clr, float gain, int force_gain);
IplImage *myDDS(IplImage *inp, double k);
IplImage *myContrast(IplImage *clr, double _gain);
void myContrastInit(IplImage *clr, double *gmax, int *gLow, int *gHgh);

int FixCorner(IplImage *clr, int *x, int *y, float pSearch);
*/
