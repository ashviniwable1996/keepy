//
//  UIViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 11/10/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Mixpanel

extension UIViewController {
    // MARK: - Mixpanel
    
     override open class func initialize() {
        if self !== UIViewController.self { return }  // make sure this isn't a subclass
        
//        struct Static {
//            static var token: Int = 0
//        }
        
        func swizzle(_ original: Selector, swizzled: Selector) {
            let originalMethod = class_getInstanceMethod(self, original)
            let swizzledMethod = class_getInstanceMethod(self, swizzled)
            
            let didAddMethod = class_addMethod(self, original, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
            
            if didAddMethod {
                class_replaceMethod(self, swizzled, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
            } else {
                method_exchangeImplementations(originalMethod, swizzledMethod);
            }
        }
        
        //dispatch_once(&Static.token) {
            swizzle(Selector("viewWillAppear:"), swizzled: Selector("keepy_viewWillAppear:"))
            swizzle(Selector("viewWillDisappear:"), swizzled: Selector("keepy_viewWillDisappear:"))
        //}
    }
    
    // MARK: - Method Swizzling
    
    func keepy_viewWillAppear(_ animated: Bool) {
        // This has been disabled because generating too many unneeded events
        // in mixpanel
        //track("appear")
        self.keepy_viewWillAppear(animated)
    }
    
    func keepy_viewWillDisappear(_ animated: Bool) {
        // This has been disabled because generating too many unneeded events
        // in mixpanel
        //track("disappear")
        self.keepy_viewWillDisappear(animated)
    }
    
    // MARK: -
    
    func track(_ action: String, properties: [String : AnyObject] = [:]) {
        if let trackingName = trackingName {
            let event = "\(trackingName)-\(action)"
            Mixpanel.sharedInstance()?.track(event, properties: properties)
        }
    }
    
    fileprivate struct AssociatedKeys {
        static var DescriptiveName = "keepy_TrackingName"
    }
    
    var trackingName: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.DescriptiveName) as? String
        }
        
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.DescriptiveName,
                    newValue as NSString?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
}
