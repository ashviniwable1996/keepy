//
//  ProductCollectionView.swift
//  Keepy
//
//  Created by Aakash Wadhwa on 02/05/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

import UIKit
import SDWebImage
import Mixpanel

@objc protocol ProductCollectionViewProtocol {
    func  closeTheView(sender: ProductCollectionView)
    func openShowAllView(sender: ProductCollectionView)
}

class ProductCollectionView: UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var skus = [NSMutableDictionary]()
    var closeButton = UIButton()
    var showAllButton = UIButton()
    var inputArray = [[String: Any]]()
    var inputProductIds = [String]()
    var productPriceArray = [String]()
    var prdouctSKUs = [String]()
    var printMessageLabel = UILabel()

    var delegate : ProductCollectionViewProtocol! = nil
    var currentVC = UIViewController ()
    var imageURL = ""
    var productCollectionView : UICollectionView?
    var finalProductDetails = [[String: Any]]()
    private let kCellfooterReuse : String = "CustomClassForProductFooterCollection"
    private let kCellReuse : String = "ProductCollectionViewCell"
    
    
    
    override func draw(_ rect: CGRect) {
       
    }
    override init(frame:CGRect) {
        super.init(frame:frame)
    }
    
    init ( imageURL: String , currentViewController: UIViewController, frameOfView: CGRect) {
        super.init(frame: frameOfView)
            
        currentVC = currentViewController
        self.imageURL = imageURL
        
        self.backgroundColor =  UIColor.white
        
        //Close Button
        self.closeButton = UIButton.init(frame: CGRect(x: self.frame.size.width - 25, y: 5 , width: 20, height: 20))
        self.closeButton.setTitle("X", for: .normal)
        self.closeButton.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        self.closeButton.setTitleColor(UIColor.init(red: 102.0/255.0, green: 87.0/255.0, blue: 83.0/255.0, alpha: 1.0), for: .normal)

        self.closeButton.addTarget(self, action: #selector(closeTheCustomCollectionView), for: .touchUpInside)
        
        var kidsName = ""
        if let kids = UserDefaults.standard.value(forKey: "kidzIDs") as? [Int] {
            
            var kid1 = [String]()
            for tagId in kids {
                let localKid = KPLocalKid.fetch(byServerID: UInt(tagId))
                kid1.append((localKid?.name)!)
            }
            
            if kid1.count ==  1{
                kidsName = "Print " + "\(kid1[0])'s" + " Memories."
            } else if kid1.count > 1 && kid1.count < 3{
                kidsName = "Print " + "\(kid1[0])'s" + " and " + "\(kid1[1])'s" + " Memories."
            } else {
                kidsName = "Print " + "\(kid1[0])'s" + "," + " \(kid1[1])'s" + " and" + " others" + " Memories."
            }
        }
        
         //Print message label
        self.printMessageLabel = UILabel.init(frame: CGRect(x: 10, y: 0 , width: self.frame.size.width - 30, height: 30))
        self.printMessageLabel.textAlignment = .left
        self.printMessageLabel.font = UIFont.systemFont(ofSize: 12.0)
        self.printMessageLabel.textColor = UIColor.keepyPinkColor()
        self.printMessageLabel.lineBreakMode = .byWordWrapping
        self.printMessageLabel.text = kidsName
        
        //Collection View
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.productCollectionView = UICollectionView(frame: CGRect(x: 5, y:  30, width: self.frame.width - 10 , height: self.frame.height - 27),collectionViewLayout:layout)

        self.productCollectionView?.delegate = self
        self.productCollectionView?.dataSource = self
        self.productCollectionView?.backgroundColor = UIColor.white
        self.bringSubview(toFront: self.productCollectionView!)
        
        
        let myNib = UINib(nibName: "CustomClassForProductFooterCollection",bundle: nil)
        self.productCollectionView?.register(myNib, forSupplementaryViewOfKind:UICollectionElementKindSectionFooter, withReuseIdentifier: kCellfooterReuse)
        self.addSubview(self.productCollectionView!)
        self.addSubview(self.closeButton)
        self.addSubview(self.printMessageLabel)
        return
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented"); }
    

    func getTheProductTemplatesForSKU(_ parameters : String, index: Int)  {
        
        ServerComm.instance().getTheProductTempleate(parameters, withSuccessBlock: { response in
            
            guard let result = response as? [String: Any]
                else {
                    return
            }
            guard let templates = result["Options"] as? [AnyObject] else {
                return
            }
            var templatesArr: Array<KpyImageProductTemplate> = [];
            let product = KpyImageProductTemplate()
            for templateJson in templates{
                let template = KpyImageProductTemplate(templateJson);
                templatesArr.append(template);
                product.templatesArray.append(template)
            }
            if index < 3 {
                self.callAPIForProductPrice(self.inputProductIds[index + 1], index: index + 1)
            }
        }) { (e: Error!) in
            print("Error is: \(e.localizedDescription)")
        }
    }
    func getTheImagesArrayForAllTheProducsts(imagesURL: String)  {

        let parameterPhoneCase = [
            "SKU": "PhoneCase-iphone6Plus-Gloss",
            "Template": "Single",
            "Images": [
                [
                    "LayerId": "26CB7",
                    "SpaceId": "02136",
                    "Image": [
                        "Url": imagesURL,
                        "MaxFit": "false",
                        "X1": "0", "X2": "1254", "Y1": "0", "Y2": "2083"
                    ]
                ]
            ]
            ] as? [String: Any]
        
        let parameterAdjustableStrobe = [
            "SKU": "AdjustableTote-16x16",
            "Template": "Single",
            "Images": [
                [
                    "LayerId": "B71F9",
                    "SpaceId": "6547B",
                    "Image": [
                        "Url": imagesURL,
                        "MaxFit": "true"
                    ]
                ]
            ]
            ] as? [String: Any]
        
        let parameterPillows = [
            "SKU": "ThrowPillow_Sewn_14x14",
            "Template": "Single",
            "Images": [
                [
                    "LayerId": "D92B2",
                    "SpaceId": "49E68",
                    "Image": [
                        "Url": imagesURL,
                        "MaxFit": "false",
                        "X1": "0", "X2": "3100", "Y1": "0", "Y2": "3100"
                    ]
                ]
            ]
            ] as? [String: Any]
        
        let parameterTshirt = [
            "SKU": "Apparel-DTG-TShirt-Bella-3001-2XL-AshGrey-Mens-CF",
            "Template": "Single",
            "Images": [
                [
                    "LayerId": "4B7FA",
                    "SpaceId": "8DAB4",
                    "Image": [
                        "Url": imagesURL,
                        "MaxFit": "true"
                    ]
                ]
            ]
        ] as? [String: Any]
        
        self.inputArray = [ parameterPhoneCase!,parameterAdjustableStrobe!,parameterPillows!,parameterTshirt!]
        self.callAPI(inputArray[0], index: 0)
    }
    func callAPI(_ parameters : [String: Any], index: Int) {
        ServerComm.instance().getTheProductImage(withParameters: parameters, withSuccessBlock: { response in
//            print("Response OF API:\(response ?? "" )")
            if let result  = response as? [String: Any] {
                if result["HadError"] as? Bool == false {
                    let productNames = ["PhoneCase-iphone6Plus","Adjustable Strap Tote","Throw Pillow","T-Shirt"]
                    if let imageURL = result["Images"] as? [[String: AnyObject?]] {
                        
                        if let imagesURL = imageURL[0]["Url"] as? String {
                            
                            var pro = Product()
                            pro.imageURL = imagesURL
                            if let sku = parameters["SKU"] as? String {
                                pro.productSKU = sku
                            }
                            pro.productHighEndMultiplicationFactor = "\(index + 25)"
                            pro.productName = productNames[index]
                            FinalProduct.shared.finalProductArray.append(pro)
                        }
                    }
                    if index < 3 {
                        self.callAPI(self.inputArray[index + 1], index: index + 1)
                    }
                }
            }
            
        }) { (e: Error!) in
            print("Error is: \(e.localizedDescription)")
            
        }
    }
    func getThePriceForTheProducts(){
        self.inputProductIds = ["57","159","93","40"]
        self.callAPIForProductPrice(self.inputProductIds[0], index: 0)
    }
    func callAPIForProductPrice(_ parameters : String, index: Int) {
        ServerComm.instance().getThePrice(ofTheProducts: parameters, withSuccessBlock: { response in
//            print("Response OF API:\(response ?? "" )")
            if let result  = response as? [String: Any] {
                if let productVariant = result["ProductVariants"] as? [[String: AnyObject?]] {
                    print("Array Count:\(self.productPriceArray.count)")
                    for (id,temp) in FinalProduct.shared.finalProductArray.enumerated() {
                        var filteredArray =  productVariant.filter({$0["Sku"] as? String == temp.productSKU })
                        if filteredArray.count > 0 {
                            if let formatedPrice = filteredArray[0]["PriceInfo"]!!["Price"] as? Double {
                                
//                                if temp.productSKU == productVariant[indexNo]["Sku"]! as? String {
                                    print("Item Match")
                                    print("\(formatedPrice)")
                                    var newItem = temp
                                    newItem.productPrice = "\(formatedPrice)"
                                    self.productPriceArray.append("\(formatedPrice)")
                                    FinalProduct.shared.finalProductArray.remove(at: id)
                                    FinalProduct.shared.finalProductArray.insert(newItem, at: id)
                                    break
//                                }
                            }
                        }
                    }
                    for temp in self.productPriceArray {
                        print("Prices :\(temp)")
                    }
                }
                if index < 3 {
                    self.callAPIForProductPrice(self.inputProductIds[index + 1], index: index + 1)
                }
                self.finalProductDetails.removeAll()
                if self.productPriceArray.count == 4 {
                    for temp in FinalProduct.shared.finalProductArray {
                        var dictOfProducts = [String: Any]()
                        dictOfProducts["productName"] = temp.productName
                        dictOfProducts["productPrice"] = temp.productPrice
                        dictOfProducts["productMulFactor"] = temp.productHighEndMultiplicationFactor
                        dictOfProducts["productSku"] = temp.productSKU
                        dictOfProducts["productImageUrl"] = temp.imageURL
                        
                        self.finalProductDetails.append(dictOfProducts)

                        print("Product Name:\(temp.productName)")
                        print("Product Price:\(temp.productPrice)")
                        print("Product MultiplicationFactor:\(temp.productHighEndMultiplicationFactor)")
                        print("Product SKU:\(temp.productSKU)")
                        print("Product imageURL:\(temp.imageURL)")
                    }
                    UserDefaults.standard.removeObject(forKey: "FinalProducts")
                    UserDefaults.standard.set(self.finalProductDetails, forKey: "FinalProducts")
                    UserDefaults.standard.synchronize()
                    NotificationCenter.default.post(name: Notification.Name("CloseClickedOnShare"), object: nil)

                }
                
            }
        }) { (e: Error!) in
            print("Error is: \(e.localizedDescription)")
            
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if let productList = UserDefaults.standard.value(forKey: "FinalProducts") as? [[String: Any]] {
            return productList.count
        }
//        return FinalProduct.shared.finalProductArray.count
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let myNib = UINib(nibName: "CustomClassForProductFooterCollection",bundle: nil)
        self.productCollectionView?.register(myNib, forSupplementaryViewOfKind:UICollectionElementKindSectionFooter, withReuseIdentifier: kCellfooterReuse)
        
        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter,
                                                                         withReuseIdentifier:kCellfooterReuse, for: indexPath) as! CustomClassForProductFooterCollection
        footerView.showAllButton.setTitle("more", for: .normal)
        footerView.showAllButton.setTitleColor(UIColor.keepyBlueColor(), for: .normal)
        footerView.showAllButton.addTarget(self, action: #selector(openTheShowAllScreen), for: .touchUpInside)
        
        footerView.backgroundColor = UIColor.white
        return footerView
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 27.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:
        IndexPath) -> UICollectionViewCell {
        
        self.productCollectionView?.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: kCellReuse)
        
        let cell = self.productCollectionView?.dequeueReusableCell(withReuseIdentifier:
            "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        
        cell.innerViewOfProduct.frame = CGRect(x: 5, y: 5, width: cell.frame.size.width - 10, height: cell.frame.size.height - 5)
        let shadowSize : CGFloat = 1.5
        let shadowPath = UIBezierPath(rect: CGRect(x: -1 / 2,
                                                   y: -1 / 2,
                                                   width: cell.innerViewOfProduct.frame.size.width + shadowSize ,
                                                   height: cell.innerViewOfProduct.frame.size.height - shadowSize ))

        cell.innerViewOfProduct.layer.masksToBounds = false
        cell.innerViewOfProduct.layer.shadowColor = UIColor.black.cgColor
        cell.innerViewOfProduct.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.innerViewOfProduct.layer.shadowOpacity = 0.3
        cell.innerViewOfProduct.layer.cornerRadius = 5.0
        cell.innerViewOfProduct.layer.shadowPath = shadowPath.cgPath
        
        if let productList = UserDefaults.standard.value(forKey: "FinalProducts") as? [[String: Any]] {
            let productDict = productList[indexPath.row]
            
            var imageString = ""
            if let imageStringURL = productDict["productImageUrl"] as? String {
                imageString = imageStringURL
            }
            if let endcoded1 = imageString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: endcoded1)        {
                cell.productImageView.image = nil
                SDWebImageManager.shared().downloadImage(with: url, options: SDWebImageOptions.highPriority, progress: nil, completed: { (image: UIImage!, error: Error!, type: SDImageCacheType, finished: Bool, url: URL!) in
                    
                    if error == nil
                    {
                        print("Image UrL:\(url)")
                        cell.productImageView.sd_setImage(with: url)
                    } else {
                        cell.productImageView.image = UIImage.init(named: "AppIcon")
                        print("Error for showing Image:\(error.localizedDescription)")
                    }
                })
            } else{
                print("Spmething is wrong")
            }
           
            if let prodName = productDict["productName"] as? String {
                cell.productName.text = prodName
            }
            
            cell.productName.textAlignment = .center
            var standardPrice  = ""
            
            if let  standardPriceLocal = productDict["productPrice"] as? String {
                standardPrice = standardPriceLocal
            }
            if standardPrice == "" {
                standardPrice = "0"
            }
           
            let multiplicationFactor =  productDict["productMulFactor"] as! String
            let intStandardPrize : Int = NSString(string: standardPrice).integerValue
            let intMultiplePrize : Int = NSString(string: multiplicationFactor).integerValue
            let highestPrice = (intStandardPrize) + (((intStandardPrize * intMultiplePrize) / 100))
            
            
            let attributes = [
                NSFontAttributeName: UIFont.systemFont(ofSize: 10.0),
                NSForegroundColorAttributeName: UIColor.keepyBlueColor(),
                NSStrikethroughStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue)
            ]
            let myTitle = NSMutableAttributedString(string:"$\(highestPrice).99", attributes: attributes)
            
            let myNormalAttributedTitle1 = NSAttributedString(string: " $\(standardPrice)",
                attributes: [NSForegroundColorAttributeName : UIColor.keepyPinkColor(),NSFontAttributeName:UIFont.systemFont(ofSize:10.0 )])
            myTitle.append(myNormalAttributedTitle1)
            
            cell.productPricing.attributedText = myTitle
            cell.productPricing.textAlignment = .center
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(CGSize(width: ((UIScreen.main.bounds.size.width ) / 2) - 10 , height:
            ((productCollectionView?.frame.size.height)!-30)/2))
        return CGSize(width: ((UIScreen.main.bounds.size.width - 10 ) / 2) , height:
            (((productCollectionView?.frame.size.height)!-30 )/2)  )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath:
        IndexPath) {
        
        if let productList = UserDefaults.standard.value(forKey: "FinalProducts") as? [[String: Any]] {
            let productDict = productList[indexPath.row]
            if let  skuName = productDict["productSku"] as? String {
                let properties = ["productSKU":skuName]
                Mixpanel.sharedInstance()?.track("PopUpScreen_SKU", properties: properties)
            }
        }
        PrintIOManager.showPreselectedPrintIODialog(in: currentVC)

    }
    func openTheShowAllScreen()  {
        delegate.openShowAllView(sender: self)
    }
    func closeTheCustomCollectionView()  {
        delegate.closeTheView(sender: self)
    }
    func reloadCollectionViewData()  {
        self.productCollectionView?.reloadData()
    }
}
