//
//  ProductCollectionViewCell.swift
//  Keepy
//
//  Created by Aakash Wadhwa on 02/05/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPricing: UILabel!
   
    @IBOutlet weak var innerViewOfProduct: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

}
