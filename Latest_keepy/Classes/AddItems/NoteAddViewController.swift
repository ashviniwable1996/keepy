//
//  NoteAddViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/22/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class NoteAddViewController: UIViewController {
    @IBOutlet weak var textView: KPTextView!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.font = UIFont(name: "ARSMaquettePro-Light", size:15)
        textView.placeholder = NSLocalizedString("Jot down a quote, a love note, a memory, a moment or anything else you want to keep", comment: "edit note")
        textView.text = ""
        textView.becomeFirstResponder()
        
        trackingName = "addnote"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: "keyboardDidShowNotification:", name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: "keyboardWillHideNotification:", name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        textView.setNeedsLayout()
        textView.setNeedsDisplay()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? AddStoryViewController2 {
            let image = CreateNoteImage(textView)
            let item = StoryItem(type: .quote)
            item.editInfo.originalImage = image
            item.editInfo.thumbnail = image.scaledToFillSize(CGSize(width: 100, height: 100), center: false)
            item.extraData = textView!.text
            vc.storyItems = [item]
        }
    }
    
    // MARK: - UIKeyboardDidShowNotification
    
    func keyboardDidShowNotification(_ notification: Notification) {
        let info  = notification.userInfo!
        let rawFrame = info[UIKeyboardFrameEndUserInfoKey] as! CGRect
        
        bottomLayoutConstraint.constant = 8 + rawFrame.height
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {
        bottomLayoutConstraint.constant = 8
    }
    
    // MARK: - UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        nextButton.isEnabled = textView.text.length > 0
    }
}
