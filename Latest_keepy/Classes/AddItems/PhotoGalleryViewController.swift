//
//  PhotoGalleryViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/18/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Photos
import SVProgressHUD

private let kCellGap: CGFloat = 3
private let kNumColumns: CGFloat = 3

class PhotoGalleryViewController: UICollectionViewController {
    struct CellItem {
        enum `Type` {
        case camera
        case note
        case photo
        case video
        }
        var type: Type
        var title: String!
        var asset: PHAsset!
        var image: UIImage!
    }
    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    var cellItems: [CellItem] = []
    var selectedIndexPaths: [IndexPath] = []

    var tableView: UITableView?
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    var collections: [PHAssetCollection] = []
    var currentCollection: PHAssetCollection?
    var thumbnailSize: CGSize!
    
    var addCamera = false
    var addNote = false
    
    lazy var imageManager: PHImageManager = {
        return PHImageManager.default()
    }()
    
    lazy var imageRequestOption: PHImageRequestOptions = {
        let o = PHImageRequestOptions()
        o.isSynchronous = false
        o.isNetworkAccessAllowed = true
        o.version = .current
        o.resizeMode = .exact
        return o
    }()
    
    // MARK: - View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Layout
        let cols = kNumColumns
        let size = floor((self.view.bounds.width - (kCellGap * 2) + kCellGap) / cols - kCellGap)
        flowLayout.minimumLineSpacing = kCellGap
        flowLayout.minimumInteritemSpacing = kCellGap
        flowLayout.itemSize = CGSize(width: size, height: size)
        flowLayout.sectionInset = UIEdgeInsets(top: kCellGap, left: kCellGap, bottom: kCellGap, right: kCellGap)
        
        let scale = UIScreen.main.scale
        thumbnailSize = CGSize(width: size * scale, height: size * scale)
        
        // Title View
        let v = TitleButtonView.loadFromNib()
        v.titleLabel.text = "All Photos"
        v.sizeToFit()
        v.onTouchDown = {
            self.toggleAlbums()
        }
        self.navigationItem.titleView = v
        
        trackingName = "photogallery"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        NotificationCenter.default.addObserver(self, selector: #selector(refreshPhotos), name: Notification.Name("photoItemRefresh"), object: nil)
        PHPhotoLibrary.requestAuthorization { [weak self] status in
            switch status {
            case .authorized:
                self?.reloadData()
            case .restricted: fallthrough
            case .denied: fallthrough
            case .notDetermined:
                print("Access to camera roll not granted")
                self?.navigationController?.dismiss(animated: true, completion: nil)
            }
        }

        updatePlanInfo()
    }
    func refreshPhotos()  {
        PHPhotoLibrary.requestAuthorization { [weak self] status in
            switch status {
            case .authorized:
                self?.reloadData()
            case .restricted: fallthrough
            case .denied: fallthrough
            case .notDetermined:
                print("Access to camera roll not granted")
                self?.navigationController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? AddStoryViewController2 {
            track("next", properties: ["items": NSNumber(value: selectedIndexPaths.count)])
            vc.storyItems = []
            for i in selectedIndexPaths {
               // let item = cellItems[i.row]
                if(cellItems.count > i.row){
                    let item = cellItems[i.row]
                    let storyItem = StoryItem(asset: item.asset!)
                    vc.storyItems.append(storyItem)
                    print("FNTRACE PGVC1")
                    print(item)
                    print(item.asset!)
                    print(storyItem)
                }
            }
            vc.completion = { [weak self] in
                if let myself = self {
                    myself.selectedIndexPaths = []
                    for (idx, item) in myself.cellItems.enumerated() {
                        for j in vc.storyItems {
                            if item.asset == j.asset {
                                print(IndexPath(row: idx, section: 0))
                                myself.selectedIndexPaths.append(IndexPath(row: idx, section: 0))
                                break
                            }
                        }
                        if myself.selectedIndexPaths.count == vc.storyItems.count { break }
                    }
                }
            }
        }
        if let nc = segue.destination as? UINavigationController {
            if let vc = nc.viewControllers.first as? TakePhotoViewController {
                track("camera")
                vc.completion = {
                    var row = 0
                    if self.addCamera { row += 1 }
					if self.addNote { row += 1 }
                    self.selectedIndexPaths = [IndexPath(row: row, section: 0)]
                    for cell in self.collectionView!.visibleCells {
                        if let a = cell as? AssetCell {
                            self.updateSelectedNumber(a)
                        }
                    }
                    self.updateNextButton()
                }
            }
        }
        if let _ = segue.destination as? NoteAddViewController {
            track("note")
        }
    }
    
    // MARK: - UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellItems.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = cellItems[indexPath.row]
        
        switch item.type {
        case .camera, .note:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommandCell", for: indexPath) as! CommandCell
            cell.imageView.image = item.image
            cell.label.text = item.title
            return cell
        default: break
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetCell", for: indexPath) as! AssetCell
        cell.asset = item.asset
        if let n = selectedIndexPaths.index(of: indexPath) {
            cell.number = n + 1
        } else {
            cell.number = 0
        }
        
        imageManager.requestImage(for: item.asset!, targetSize: thumbnailSize, contentMode: .aspectFill, options: imageRequestOption) { (image: UIImage?, info: [AnyHashable: Any]?) in
            if cell.asset == item.asset && image != nil {
                cell.imageView.image = image
                cell.imageView.contentMode = .scaleAspectFill
            }
        }
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = cellItems[indexPath.row]
        
        switch item.type {
        case .camera:
            performSegue(withIdentifier: "TakePhoto", sender: nil)
            
        case .note:
            if getQuota() > 0 {
                
                let avc = NoteEditorViewController(mode: 1)!
                self.navigationController?.pushViewController(avc, animated: true)
                
//                let addPhotoNavController = UINavigationController(rootViewController: avc)
//                self.navigationController?.presentViewController(addPhotoNavController, animated: true, completion: nil)
//                performSegueWithIdentifier("AddNote", sender: nil)
            } else {
                suggestUpgrade()
                collectionView.deselectItem(at: indexPath, animated: true)
            }
            
        default:
            collectionView.deselectItem(at: indexPath, animated: true)
            if let index = selectedIndexPaths.index(of: indexPath) {
                selectedIndexPaths.remove(at: index)
            } else {
                let numberLeft : Int = Defaults.keepyMonthlySpace - Defaults.keepiesUsedThisMonth
                
                if getQuota() > selectedIndexPaths.count {
                    selectedIndexPaths.append(indexPath)
                } else if (Defaults.planType == .free && numberLeft <= Defaults.maxItemsPerCollection )
                {
                    suggestUpgrade()
                }
            }
            
            for cell in collectionView.visibleCells {
                if let a = cell as? AssetCell {
                    updateSelectedNumber(a)
                }
            }
            updateNextButton()
        }
    }
    
    fileprivate func getQuota() -> Int {
        
        switch Defaults.planType {
        case .free:
            let numberLeft : Int = Defaults.keepyMonthlySpace - Defaults.keepiesUsedThisMonth
            return numberLeft > Defaults.maxItemsPerCollection ? Defaults.maxItemsPerCollection : numberLeft
            //return Defaults.maxItemsPerCollection
        case .unlimited, .premium:
            return Defaults.maxItemsPerCollection
            
        default:
            return Int.min
        }
    }
    
    fileprivate func suggestUpgrade() {
        let s: String
        
        let left = Defaults.keepyMonthlySpace - Defaults.keepiesUsedThisMonth
        if left > 0 {
            s = NSLocalizedString("Only \(left) free keepies left!\n\nYou can save more next month or subscribe now to save unlimited memories.", comment: "")
        } else {
            s = NSLocalizedString("No free keepies left!\n\nYou can save more next month or subscribe now to save unlimited memories.", comment: "")
        }
        
        let ac = UIAlertController(title: nil, message: s, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: NSLocalizedString("Learn More", comment: ""), style: .default, handler: { (ac: UIAlertAction) in
            self.track("upgrade", properties: ["message":s as AnyObject])
            
        //    let vc = AccountViewController(nibName: "AccountViewController", bundle: nil)
            
            let vc  = UIStoryboard(name: "Unlimited", bundle: nil).instantiateViewController(withIdentifier: "UnlimitedTable")
            
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        self.present(ac, animated: true, completion: nil)
    }
    
    // MARK: - IBAction
    
    @IBAction func onCancelPressed(_ sender: AnyObject) {
        track("cancel")
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private
    
    fileprivate func updateNextButton() {
        if selectedIndexPaths.count == 0 {
            nextButton.isEnabled = false
        } else {
            nextButton.isEnabled = getQuota() >= selectedIndexPaths.count
        }
    }
    
    fileprivate func updateSelectedNumber(_ cell: AssetCell) {
        guard let indexPath = self.collectionView?.indexPath(for: cell) else {
            return
        }
        
        if let index = selectedIndexPaths.index(of: indexPath) {
            cell.number = index + 1
            
        } else {
            cell.number = 0
        }
    }

    fileprivate func reloadData() {
        let opts = PHFetchOptions()
        opts.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        //        if #available(iOS 9, *, *) {
        //            opts.fetchLimit = 200
        //        }
        
        var items: [CellItem] = []
        var title = "All Photos"
        
        let result: PHFetchResult<PHAsset>
        if let c = self.currentCollection {
            result = PHAsset.fetchAssets(in: c, options: opts)
            title = c.localizedTitle!
        } else {
            result = PHAsset.fetchAssets(with: opts)
            if self.addCamera {
                items.insert(CellItem(type: .camera, title: NSLocalizedString("take a photo", comment: ""), asset: nil, image: UIImage(named: "camera_icon")), at: 0)
            }
            if self.addNote {
                let idx = (items.count > 0 && items[0].type == .camera) ? 1 : 0
                items.insert(CellItem(type: .note, title: NSLocalizedString("add a note", comment: ""), asset: nil, image: UIImage(named: "note_icon")), at: idx)
            }
        }
        print("# of assets: \(result.count)")
        
        result.enumerateObjects({ (asset: AnyObject, idx: Int, stop: UnsafeMutablePointer) in
            if let a = asset as? PHAsset {
                items.append(CellItem(type: a.mediaType == .video ? .video : .photo, title: nil, asset: a, image: nil))
            }
        })
        
        // Maybe assets are removed in "add story" and came back.
        updateNextButton()
        
        async {
            let v = self.navigationItem.titleView as! TitleButtonView
            v.titleLabel.text = title
            v.sizeToFit()
            self.cellItems = items
            self.collectionView!.reloadData()
        }
    }
    
    fileprivate func fetchAlbums() {
        let options = PHFetchOptions()
        options.predicate = NSPredicate(format: "estimatedAssetCount >= 0")
        
        let userAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: options)
        
        userAlbums.enumerateObjects({ (object: AnyObject!, count: Int, stop: UnsafeMutablePointer) in
            let collection = object as! PHAssetCollection
            
            let result = PHAsset.fetchAssets(in: collection, options:nil)
            print("album title: \(String(describing: collection.localizedTitle)) (\(result.count))")
            
            if result.count > 0 {
                self.collections.append(collection)
                self.tableView?.reloadData()
            }
        })
        
        
        let customAlbums = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: options)
        
        customAlbums.enumerateObjects({ (object: AnyObject!, count: Int, stop: UnsafeMutablePointer) in
            let collection = object as! PHAssetCollection
            
            let result = PHAsset.fetchAssets(in: collection, options:nil)
            print("album title: \(collection.localizedTitle) (\(result.count))")
            
            if result.count > 0 {
                self.collections.append(collection)
                self.tableView?.reloadData()
            }
        })
        
    }
    
    fileprivate func toggleAlbums() {
        let target = self.view.bounds
        let origin = CGRect(x: target.minX, y: target.minY - target.height, width: target.width, height: target.height)
        if let t = tableView {
            track("album-close")
            cancelButton.isEnabled = true
            updateNextButton()
            
            UIView.animate(withDuration: 0.2,
                animations: {
                    t.frame = origin
                },
                completion: { (finished: Bool) -> Void in
                    t.removeFromSuperview()
                    t.delegate = nil
                    t.dataSource = nil
                    self.tableView = nil
                }
            )
        } else {
            track("album-open")
            // Disable buttons
            cancelButton.isEnabled = false
            nextButton.isEnabled = false
            
            let t = UITableView(frame: origin, style: .plain)
            view.addSubview(t)
            t.delegate = self
            t.dataSource = self
            t.separatorInset = UIEdgeInsets.zero
            t.layoutMargins = UIEdgeInsets.zero
            t.contentInset = collectionView!.contentInset
            
            t.register(AlbumNameCell.self, forCellReuseIdentifier: "AlbumNameCell")
            tableView = t
            
            if collections.count == 0 {
                async {
                    self.fetchAlbums()
                }
            }
            
            UIView.animate(withDuration: 0.2, animations: {
                t.frame = target
            }) 
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension PhotoGalleryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumNameCell", for: indexPath) as! AlbumNameCell
        
        let collection = collections[indexPath.row]
        
        let opts = PHFetchOptions()
        opts.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let result = PHAsset.fetchAssets(in: collection, options: opts)
        
        let currentTag = cell.tag + 1
        cell.tag = currentTag
        
        let im = PHImageManager.default()
        let o = PHImageRequestOptions()
        o.resizeMode = PHImageRequestOptionsResizeMode.exact
        
//        im.requestImage(for: result.firstObject as! PHAsset, targetSize: thumbnailSize, contentMode: .aspectFill, options: o) { (image: UIImage?, info: [AnyHashable: Any]?) in
//            if cell.tag == currentTag {
//                cell.imageView!.image = image
//                cell.setNeedsLayout()
//            }
//        }
        
        if result.count > 0 {
            let firstObj:PHAsset = result.firstObject as! PHAsset
            im.requestImage(for: firstObj, targetSize: thumbnailSize, contentMode: .aspectFill, options: o) { (image: UIImage?, info: [AnyHashable: Any]?) in
                if cell.tag == currentTag {
                    cell.imageView!.image = image
                    cell.setNeedsLayout()
                }
            }
        }
        
        
        cell.textLabel!.text = "\(collection.localizedTitle!)"
        cell.detailTextLabel!.text = "\(result.count)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentCollection != collections[indexPath.row] {
            currentCollection = collections[indexPath.row]
            selectedIndexPaths = []
            reloadData()
        }
        
        let v = self.navigationItem.titleView as! TitleButtonView
        track("album-\(v.titleLabel.text!)")
        v.toggle()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.layoutMargins = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.separatorInset = UIEdgeInsets.zero
    }
}

// MARK: - Private

private enum PlanType : Int {
    case unknown = -1
    case free = 0
    case premium = 1
    case unlimited = 2
}

extension PhotoGalleryViewController {
    fileprivate func updatePlanInfo() {
        ServerComm.instance().getUserInfo({ response in
            if let dict = response as? [String:AnyObject], let result = dict["result"] as? [String:AnyObject], let planInfo = result["planInfo"] as? [String:AnyObject] {
                print("userInfo/planInfo = \(planInfo)")
                if let planType = planInfo["planType"] as? Int, let keepiesUsedThisMonth = planInfo["keepiesUsedThisMonth"] as? Int, let keepyMonthlySpace = planInfo["keepyMonthlySpace"] as? Int, let maxItemsPerCollection = planInfo["maxItemsPerCollection"] as? Int {
                    Defaults.planType = Defaults.PlanType(rawValue: planType) ?? .unlimited
                    Defaults.keepiesUsedThisMonth = keepiesUsedThisMonth
                    Defaults.keepyMonthlySpace = keepyMonthlySpace
                    Defaults.maxItemsPerCollection = maxItemsPerCollection
                    
                    let left = keepyMonthlySpace - keepiesUsedThisMonth
                    print("userInfo/updated: left(\(left))")
                }
                
            } else {
                print("userInfo/unexpected_response: \(response)")
            }
            
        }) { (error: Error!) in
            print("userInfo/failure: \(error)");
            Defaults.planType = .unknown
            SVProgressHUD.showError(withStatus: "Server is busy. Please try again later.")
        }
    }
}

// MARK: - TitleButtonView

class TitleButtonView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var onTouchDown: ((Void) -> Void)?
    
    // MARK: -
    
    class func loadFromNib() -> TitleButtonView {
        let v = Bundle.main.loadNibNamed("TitleButtonView", owner: self, options: nil)!.last as! TitleButtonView
        if let attr = UINavigationBar.appearance().titleTextAttributes {
            v.titleLabel.textColor = attr[NSForegroundColorAttributeName] as? UIColor
            v.titleLabel.font = attr[NSFontAttributeName] as? UIFont
        }
        return v
    }
    
    func toggle() {
        rotate(0.2)
        onTouchDown?()
    }
    
    // MARK: - View
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        toggle()
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        bounds.size = intrinsicContentSize
    }
    
    override var intrinsicContentSize : CGSize {
        var size = CGSize.zero
        if titleLabel != nil {
            titleLabel.sizeToFit()
            size = titleLabel.bounds.size
            size.width += imageView.bounds.width
        }
        return size
    }
    
    // MARK: - Private
    
    fileprivate var angle = 0.0
    
    fileprivate func rotate(_ duration: TimeInterval) {
        angle = angle == 0 ? 179.9 : 0
        UIView.animate(withDuration: duration, animations: {
            self.imageView.transform = CGAffineTransform(rotationAngle: CGFloat(self.angle / 180.0 * M_PI))
        }) 
    }
}
