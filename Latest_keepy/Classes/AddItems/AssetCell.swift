//
//  AssetCell.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/23/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Photos

class AssetCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var orderLabel: AssetOrderLabel!
    @IBOutlet weak var durationView: UIView!
    @IBOutlet weak var durationLabel: UILabel!
    
    var asset: PHAsset! {
        didSet {
            if asset.mediaType == .video {
                let duration = UInt32(floor(asset.duration))
                durationLabel.text = String(format:"%01lu:%02lu", duration / 60, duration % 60)
                durationView.isHidden = false
            } else {
                durationView.isHidden = true
            }
        }
    }
    
    var number: Int = 0 {
        didSet {
            if number > 0 {
                orderLabel?.text = "\(number)"
                orderLabel?.isHidden = false
                contentView.layer.borderWidth = 5
            } else {
                orderLabel?.isHidden = true
                contentView.layer.borderWidth = 0
            }
            setNeedsDisplay()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let c = UIColor(red: 0.192, green: 0.702, blue: 0.749, alpha: 0.9)
        contentView.layer.borderColor = c.cgColor
        contentView.layer.borderWidth = 0
        orderLabel.backgroundColor = c
        orderLabel.isHidden = true
        orderLabel.layer.masksToBounds = true
        durationView.isHidden = true
    }
    
    override func prepareForReuse() {
        number = 0
        imageView.image = UIImage(named: "calendar-image-placeholder")
        imageView.contentMode = .center
        durationView.isHidden = true
        orderLabel.isHidden = true
    }
}

class CommandCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        let selectionView = UIView()
        selectionView.backgroundColor = UIColor.lightGray
        selectedBackgroundView = selectionView
        backgroundColor = UIColor(rgb: 0x6c5e5a)
    }
}

class AlbumNameCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView!.frame.origin.x = 0
        imageView!.frame.origin.y = 0
        textLabel!.frame.origin.x = imageView!.frame.width + 8
    }
}

// MARK: - AssetOrderLabel

class AssetOrderLabel: UILabel {
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        size.height += 5
        size.width += 10
        if size.width < size.height {
            size.width = size.height
        }
        layer.cornerRadius = size.height / 2
        return size
    }
}
