//
//  StoryDateTableViewCell.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class StoryDateTableViewCell: SelectCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        textLabel!.text = NSLocalizedString("when", comment: "")
        date = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        textLabel!.text = NSLocalizedString("when", comment: "")
        date = nil
    }

    var date: Date? {
        didSet {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            detailText = formatter.string(from: date ?? Date())
        }
    }
    
    override class func dequeue(tableView tv: UITableView, forIndexPath indexPath: IndexPath) -> StoryDateTableViewCell {
        return super.dequeue(tableView: tv, forIndexPath: indexPath) as! StoryDateTableViewCell
    }
}

class SelectCell: UITableViewCell {
    var placeHolder = "select"
    var detailText: String? {
        didSet {
            let desc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
            if let s = detailText {
                detailTextLabel?.text = s
                detailTextLabel?.textColor = UIColor(rgb: 0x57423f)
                detailTextLabel?.font = UIFont(name:"ARSMaquettePro-Medium", size:desc.pointSize)
                detailTextLabel?.alpha = 1.0
            } else {
                detailTextLabel?.text = placeHolder
                detailTextLabel?.textColor = UIColor(rgb: 0x57423f)
                detailTextLabel?.font = UIFont(name:"ARSMaquettePro-Light", size:desc.pointSize)
                detailTextLabel?.alpha = 0.4
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        selectionStyle = .default
        accessoryType = .disclosureIndicator
        textLabel?.text = "select"
        detailTextLabel?.text = placeHolder
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
        
        if let l = textLabel {
            let desc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
            l.textColor = UIColor(rgb: 0x57423f)
            l.font = UIFont(name:"ARSMaquettePro-Light", size:desc.pointSize)
        }
    }
    
    override class func dequeue(tableView tv: UITableView, forIndexPath indexPath: IndexPath) -> SelectCell {
        return super.dequeue(tableView: tv, forIndexPath: indexPath) as! SelectCell
    }
}

class ToggleCell: UITableViewCell {
    let toggle = UISwitch()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        selectionStyle = .none
        textLabel?.text = "toggle"
        accessoryView = toggle
        
        if let l = textLabel {
            l.textColor = UIColor(rgb: 0x57423f)
            l.font = UIFont(name:"ARSMaquettePro-Light", size:17)
        }
        
        toggle.addTarget(self, action: "_valueChanged:", for: .valueChanged)
    }
    
    override class func dequeue(tableView tv: UITableView, forIndexPath indexPath: IndexPath) -> ToggleCell {
        return super.dequeue(tableView: tv, forIndexPath: indexPath) as! ToggleCell
    }
    
    func _valueChanged(_ sender: UISwitch) {
        valueChanged?()
    }
    
    var valueChanged: ((Void)->Void)?
}
