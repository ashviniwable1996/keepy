//
//  VoiceVideoTableViewCell.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class VoiceVideoTableViewCell: UITableViewCell {

    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var voiceButton: UIButton!
    
    @IBAction func onVoiceButtonPressed(_ sender: AnyObject) {
        onVoicePressed?()
    }
    
    @IBAction func onVideoButtonPressed(_ sender: AnyObject) {
        onVideoPressed?()
    }
    
    var onVoicePressed: ((Void)->Void)?
    var onVideoPressed: ((Void)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override class func register(tableView tv: UITableView) {
        registerNib(tableView: tv)
    }
}
