//
//  StoryItem.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/28/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Photos

class StoryItem {
    var localItem: KPLocalItem?
    
    let type: StoryItemType
    var asset: PHAsset?
    
    // PrepareAssetOperation
    var assetId: UInt = 0
    var assetHash: String?
    var policy: String?
    var policyHash: String?
    
    var thumbnailAssetId: UInt = 0
    var thumbnailAssetHash: String?
    
    func requestImageData(_ completion: @escaping ((_ data: Data?)->Void)) {
        switch type {
        case .photo:
            let opts = PHImageRequestOptions()
            opts.isSynchronous = false

            //commented out by amit, replaced by a new block below:
//            PHImageManager.default().requestImageData(for: asset!, options: opts) { (data: Data?, uti: String?, orientation: UIImageOrientation, info: [AnyHashable: Any]?) in
//                if let isDegraded = info?[PHImageResultIsDegradedKey] as? NSNumber {
//                    if isDegraded.intValue == 1 { return }
//                }
//                completion(data)
            
            PHImageManager.default().requestImage(for: asset!,targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: opts) {(image: UIImage?, info: [AnyHashable: Any]?) in
                if let isDegraded = info?[PHImageResultIsDegradedKey] as? NSNumber {
                    if isDegraded.intValue == 1 { return }
                }
                if let image = image {
                    let oriented = image.normalizedImage
                    let result = UIImageJPEGRepresentation(oriented, 0.8)
                    completion(result)
                }
            }
        case .video:
            let opts = PHImageRequestOptions()
            opts.isSynchronous = false
            PHImageManager.default().requestImage(for: asset!, targetSize: CGSize(width: 600, height: 600), contentMode: PHImageContentMode.default, options: opts) { (image: UIImage?, info: [AnyHashable: Any]?) in
                if let isDegraded = info?[PHImageResultIsDegradedKey] as? NSNumber {
                    if isDegraded.intValue == 1 { return }
                }
                if let image = image {
                    let oriented = image.normalizedImage
                    let result = UIImageJPEGRepresentation(oriented, 0.8)  // Best quality. changed from 1 to 0.8 by amitg
                    completion(result)
                }
            }
            
        case .quote:
            if let image = editInfo.originalImage {
                let result = UIImageJPEGRepresentation(image, 0)  // Low quarity
                completion(result)
            }
            break
        }
    }

    func requestUrl(_ completion: @escaping ((_ url: URL?)->Void)) {
        print("type: \(type)")
        switch type {
//zvika524 fix Removed this code and added new case .Photo
//        case .Photo:
//            asset!.requestContentEditingInputWithOptions(PHContentEditingInputRequestOptions()) { (input, _) in
//                if let input = input {
//                    // Copy the file to Document folder so that the background process can access it later.
//                    let fm = NSFileManager.defaultManager()
//                    let doc = try! fm.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false)
//                    let url = doc.URLByAppendingPathComponent(input.fullSizeImageURL!.pathComponents!.last!)
//                    do { try fm.copyItemAtURL(input.fullSizeImageURL!, toURL: url) } catch {}
//                    completion(url: url)
//                } else {
//                    completion(url: nil)
//                }
//            }
//
        case .photo:
            
            let editingOptions = PHContentEditingInputRequestOptions()
            editingOptions.isNetworkAccessAllowed = true
            asset!.requestContentEditingInput(with: editingOptions) { (editingInput, _) in
                if  let input = editingInput, let imgURL = input.fullSizeImageURL {
//                if let input = input {
                    // Copy the file to Document folder so that the background process can access it later.
                    let fm = FileManager.default
                    let doc = try! fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let lastCompoent = input.fullSizeImageURL!.pathComponents.last!
                    var url: URL
                    if(lastCompoent  == "FullSizeRender.jpg"){
                        var str = "\(input.fullSizeImageURL!)"
                        
                        // commentd by Ashvini
                        //  let rangOfIMG_XXXX = str.range(of: "IMG_")!.lowerBound..<str.index(str.range(of: "IMG_")!.lowerBound, offsetBy: 8)
                        //  str = str.substring(with: rangOfIMG_XXXX)
                        
                        // added by Ashvini
                        if (!str.isEmpty || str != "") {
                            let firstindex = str.range(of: "IMG_")!.lowerBound
                            let index = str.range(of: "IMG_")!.lowerBound
                            let secondindex = str.index(index, offsetBy: 8)
                            let rangeofimg_xxxx = firstindex..<secondindex
                            str = str.substring(with: rangeofimg_xxxx)
                        }
                        
                        
                        // added by Ashvini
//                        if((str != "") || (str != nil)) {
//                            let firstindex = str.range(of: "IMG_")!.lowerBound
//                            let index = str.range(of: "IMG_")!.lowerBound
//                            let secondindex = str.index(index, offsetBy: 8)
//                           // if((firstindex != nil) && (secondindex != nil)) {
//                                let rangeofimg_xxxx = firstindex..<secondindex
//                                str = str.substring(with: rangeofimg_xxxx)
//                           // }
//                        }
                        
                        let urlString = "\(doc)" + str + ".jpg"
                        url = URL(string: urlString)!
                    } else
                    {
                        // original code
                        url = doc.appendingPathComponent(input.fullSizeImageURL!.pathComponents.last!)
                    }
                    do {
                     //  added by amitg
                        print("FNTRACE filecopy")
                        print(input.fullSizeImageURL!)
                        let imageData = try Data(contentsOf: input.fullSizeImageURL!)
                        if let image = UIImage(data: imageData) {
                            if let imagedata = UIImageJPEGRepresentation(image, 0.8) {
                                    try? imagedata.write(to:url, options: .atomic)
                                }
                        }

                       // try fm.copyItem(at: input.fullSizeImageURL!, to: url)
                    }
                    catch {
                        print("zvika524 FFFFFFFFFFFFFFFFFFFFFFFFFfail to copy")
                    }
                    print("zvika524 !!!!!!! url=\(url)\n\n")
                    completion(url)
                }
                else
                {
                    completion(nil)
                }
            }

        case .video:
            let opts = PHVideoRequestOptions()
            opts.isNetworkAccessAllowed = true
            PHImageManager.default().requestAVAsset(forVideo: asset!, options: opts) { (avasset: AVAsset?, mix: AVAudioMix?, info: [AnyHashable: Any]?) in
                if let urlasset = avasset as? AVURLAsset {
                    // Copy the file to Document folder so that the background process can access it later.
                    let fm = FileManager.default
                    let doc = try! fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let url = doc.appendingPathComponent(urlasset.url.pathComponents.last!)
                    do { try fm.copyItem(at: urlasset.url, to: url) } catch {}
                    completion(url)
                } else {
                    completion(nil)
                }
            }
        default:
            NSLog("BUG: not a video")
            completion(nil)
        }
    }
    
    func requestImage(_ completion: @escaping ((_ image: UIImage?)->Void)) {
        switch type {
        case .photo, .video:
            let opts = PHImageRequestOptions()
            opts.isSynchronous = true
            print("FNTRACE sStoryItem.swift")
//            opts.resizeMode = PHImageRequestOptionsResizeMode.Exact //causes cropping issues
            PHImageManager.default().requestImage(for: asset!, targetSize: CGSize(width: 600, height: 600), contentMode: PHImageContentMode.default, options: opts) { (image: UIImage?, info: [AnyHashable: Any]?) in
                if let isDegraded = info?[PHImageResultIsDegradedKey] as? NSNumber {
                    if isDegraded.intValue == 1 { return }
                }
                
                //added by amitg
                if let image = image {
                    let oriented = image.normalizedImage
                    let result = UIImageJPEGRepresentation(oriented, 0.8)  as NSData?
                    let imagenew = UIImage(data:(result as! NSData) as Data)
                    completion(imagenew)
                }
                
//                if let image = image {
//                    completion(image.normalizedImage)
//                }
            }
            
        case .quote:
            completion(editInfo.originalImage)
            break
        }
    }
    
    var videoUrl: URL? {
        var url: URL?
        
        let semaphore = DispatchSemaphore(value: 0)
        let opts = PHVideoRequestOptions()
        opts.isNetworkAccessAllowed = true
        PHImageManager.default().requestAVAsset(forVideo: asset!, options: opts) { (avasset: AVAsset?, mix: AVAudioMix?, info: [AnyHashable: Any]?) in
            if let urlasset = avasset as? AVURLAsset {
                url = urlasset.url
            }
            semaphore.signal()
        }
        while (semaphore.wait(timeout: DispatchTime.now()) != .success) {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.1))
        }
        return url
    }
    
    var editInfo: ImageEditInfo
        
    var title: String?
    var kids: Set<UInt> = []
    var date: Date?
    var placeId: UInt = 0
    var tags: Set<UInt> = []
    var share: Bool = true
    
    var extraData: String? = nil
    var extraParams: [String:AnyObject]?

    // AddItemOperation
    var itemId: UInt = 0
    var originalAssetHash: String?
    var originalAssetId: UInt = 0
    
    // AddVideoItemOperation
    var sdVideoAssetId: Int = 0
    var hdVideoAssetId: String?
    var sdVideoAssetHash: String?
    var hdVideoAssetHash: String?
    var short_url: String?
    
    // MARK: -
    
    init(type aType: StoryItemType, image: UIImage?=nil) {
        type = aType
        editInfo = ImageEditInfo()
        print("FNTRACE Storyitem.swift2")
        //editInfo.originalImage = image
        //^ commentedut by amitg and the following block added
        if let image = image {
            let result = UIImageJPEGRepresentation(image, 0.8)  as NSData?
            let imagenew = UIImage(data:(result as! NSData) as Data)
            editInfo.originalImage = imagenew
       }
    }
    
    init(asset anAsset: PHAsset) {
        asset = anAsset
        if anAsset.mediaType == .video {
            type = .video
        } else {
            type = .photo
        }
        editInfo = ImageEditInfo()
    }
    
    var prepareOperation: PrepareOperation?
    var addOperation: AddItemOperation?
    
    func pointsAsString() -> String {
        var arr = [String]()
        for i in editInfo.points {
            arr.append(String(format: "%2.2f,%2.2f", i.x, i.y))
        }
        return arr.joined(separator: "|")
    }
}

enum StoryItemType : Int {
    case photo = 0
    case quote = 2
    case video = 4
}
