//
//  CategoriesPickerViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/24/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class CategoriesPickerViewController: UITableViewController {
    var tags: [KPLocalTag] = []
    var completion: ((Void) -> Void)?
    
    fileprivate var items: [KPLocalTag] = []
    fileprivate var selectedIndexPaths: [IndexPath:Bool] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tags: NSArray = KPLocalTag.fetchAll()! as NSArray
        let sort = NSSortDescriptor(key: "name", ascending: true)
        items = tags.sortedArray(using: [sort]) as! [KPLocalTag]
        
        UITableViewCell.register(tableView: tableView)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for (i, e) in items.enumerated() {
            for t in tags {
                if t.serverID == e.serverID {
                    let indexPath = IndexPath(row: i, section: 0)
                    selectedIndexPaths[indexPath] = true
                    break
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let completion = completion {
            var arr = [KPLocalTag]()
            for (i,_) in selectedIndexPaths {
                arr.append(items[i.row])
            }
            tags = arr.sorted { $0.name.compare($1.name) == .orderedAscending }
            completion()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return NSLocalizedString("tag your keepies now, so later you can filter them", comment: "")
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.dequeue(tableView: tableView, forIndexPath: indexPath)
        configureCell(cell, indexPath: indexPath)
        return cell
    }

    func configureCell(_ cell: UITableViewCell, indexPath: IndexPath) {
        cell.textLabel!.text = items[indexPath.row].name
        cell.accessoryType = selectedIndexPaths[indexPath] != nil ? .checkmark : .none
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndexPaths[indexPath] != nil {
            selectedIndexPaths.removeValue(forKey: indexPath)
        } else {
            selectedIndexPaths[indexPath] = true
        }
        
        let cell = tableView.cellForRow(at: indexPath)!
        configureCell(cell, indexPath: indexPath)
    }
}
