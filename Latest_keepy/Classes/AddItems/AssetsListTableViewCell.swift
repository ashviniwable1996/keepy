//
//  AssetsListTableViewCell.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Photos

private let kPadding: CGFloat = 8.0
private let kSpacing: CGFloat = 5.0

class AssetsListTableViewCell: UITableViewCell, UITextViewDelegate {
    var itemImage: UIImage? {
        didSet {
            let varHeight = (UIScreen.main.bounds.size.width * 100 ) / 320
            let size = CGSize(width: varHeight, height: varHeight)//CGSize(width: scrollView.bounds.height, height: scrollView.bounds.height)
            let rect = CGRect(x: kPadding, y: 0, width: size.width, height: size.height - 2)
            let v = self.imageView(itemImage!, frame: rect, index: 0, type: .photo)
            scrollView.addSubview(v)
        }
    }
    
    fileprivate func imageView(_ image: UIImage, frame: CGRect, index: Int, type: StoryItemType) -> UIImageView {
        let v = UIImageView(image: image)
        v.frame = frame
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = false
        v.isUserInteractionEnabled = true
        v.layer.shadowColor = UIColor.black.cgColor
        v.layer.shadowOffset = CGSize.zero
        v.layer.shadowOpacity = 1
        v.layer.shadowRadius = 2.0
        
        // edit button (invisible)
        let b = UIButton(type: .custom)
        b.frame = v.bounds
        b.addTarget(self, action: #selector(AssetsListTableViewCell.onEditPressed(_:)), for: .touchUpInside)
        b.tag = index  // asset index
        v.addSubview(b)
        
        // delete button
        let d = UIButton(type: .custom)
        d.setImage(UIImage(named: "add-story-delete-photo-icon"), for: UIControlState())
        d.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 0, 0)
        d.frame = CGRect(x: frame.width - 44, y: frame.height - 44, width: 40, height: 40)
        d.addTarget(self, action: #selector(AssetsListTableViewCell.onDeletePressed(_:)), for: .touchUpInside)
        d.tag = index  // asset index
        v.addSubview(d)
        
        if type == .video {
            let iv = UIImageView(image: UIImage(named: "play-video-small-gallery"))
            v.addSubviewCenter(iv)
        }
        return v
    }
    
    var items: [StoryItem] = [] {
        didSet {
            let im = PHImageManager.default()
            let o = PHImageRequestOptions()
            o.isSynchronous = false
            o.isNetworkAccessAllowed = true
            o.version = .current
            
            for i in scrollView.subviews { i.removeFromSuperview() }
            scrollView.clipsToBounds = false
            
            let varHeight = (UIScreen.main.bounds.size.width * 100 ) / 320
            let size = CGSize(width: varHeight, height: varHeight)
            //let size = CGSize(width: scrollView.bounds.height, height: scrollView.bounds.height)
            var rect = CGRect(x: kPadding, y: 0, width: size.width, height: size.height - 2)
            for (i, item) in items.enumerated() {
                
                if let asset = item.asset {
                    im.requestImage(for: asset, targetSize: rect.size, contentMode: .aspectFill, options: o) { (image: UIImage?, info: [AnyHashable: Any]?) in
						if let image = item.editInfo.editedImage ?? image {
							if let v = self.scrollView.viewWithTag(i + 100) {
								v.removeFromSuperview()
							}
							
							let actualImage = image.scaledToFillSize(CGSize(width: rect.size.width * 3, height: rect.size.height * 3))
							
							rect.origin.x = kSpacing + (rect.width + kSpacing) * CGFloat(i)
							let v = self.imageView(actualImage, frame: rect, index: i, type: item.type)
							v.tag = i + 100
							self.scrollView.addSubview(v)
							self.scrollView.contentSize = CGSize(width: rect.maxX + kPadding, height: rect.maxY)
						}
                    }
                } else if let t = item.editInfo.thumbnail {
                    let actualImage = t.enhance(item.editInfo.autoEnhance, rotation: Int(item.editInfo.rotation)).scaledToFillSize(CGSize(width: rect.size.width * 3, height: rect.size.height * 3))
                    
                    let v = self.imageView(actualImage, frame: rect, index: i, type: item.type)
                    
                    scrollView.addSubview(v)
                    scrollView.contentSize = CGSize(width: rect.maxX + kPadding, height: rect.maxY)
                    rect.origin.x += rect.width + kSpacing
                }
            }
        }
    }
    
    @IBAction func onEditPressed(_ sender: UIButton) {
        NSLog("edit - \(sender.tag)")
        onAssetEditPressed?(sender.tag)
    }
    
    @IBAction func onDeletePressed(_ sender: UIButton) {
        NSLog("delete - \(sender.tag)")
        onAssetDeletePressed?(sender.tag)
    }
    
    @IBOutlet weak var textView: KPTextView!
    @IBOutlet weak var scrollView: UIScrollView!

    var onTextDidChange: ((Void)->Void)?
    var onAssetEditPressed: ((Int)->Void)?
    var onAssetDeletePressed: ((Int)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero        
        textView.isScrollEnabled = false
        textView.delegate = self
    }
    
    override class func register(tableView tv: UITableView) {
        registerNib(tableView: tv)
    }
    
    // MARK: - UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        
        // Resize the cell only when cell's size is changed
        if size.height != newSize.height {
            UIView.setAnimationsEnabled(false)
            tableView?.beginUpdates()
            tableView?.endUpdates()
            UIView.setAnimationsEnabled(true)
            
            if let thisIndexPath = tableView?.indexPath(for: self) {
                tableView?.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
        }
        onTextDidChange?()
    }
}

extension UIImage {
    func scaledToFillSize(_ size: CGSize, center: Bool = true) -> UIImage {
        let scale = max(size.width / self.size.width, size.height / self.size.height)
        let width = self.size.width * scale
        let height = self.size.height * scale
        var imageRect = CGRect(x: 0, y: 0, width: width, height: height)
        if center {
            imageRect.origin.x = (size.width - width) / 2.0
            imageRect.origin.y = (size.height - height) / 2.0
        }
        
        UIGraphicsBeginImageContextWithOptions(size, true, self.scale)
        self.draw(in: imageRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
