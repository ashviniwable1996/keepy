//
//  CropViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/5/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import SVProgressHUD

class CropViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var originalButton: UIButton!
    @IBOutlet weak var customButton: UIButton!
    @IBOutlet weak var squareButton: UIButton!
    @IBOutlet weak var skewButton: UIButton!
    @IBOutlet weak var applyButton: UIBarButtonItem!
    
    // MARK: -
    
    var originalImage: UIImage!
    var croppedImage: UIImage?
    var points: [CGPoint] = []
    
    var completion: ((Void) -> Void)?
    
    fileprivate var adjustView: MADrawRect!
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateImage()
        
        adjustView = MADrawRect(frame: CGRect.zero)
        adjustView.delegate = self
        view.addSubview(adjustView)
        
        onCustomPressed(customButton)
        
        if points.count == 4 {
            // TODO: This points need to be adjusted to take into account "rotation" parameter.
            
            let topLeft = points[0]
            let bottomLeft = points[1]
            let bottomRight = points[2]
            let topRight = points[3]
            
            if topLeft.x != bottomLeft.x || topLeft.y != topRight.y || bottomRight.x != topRight.x || bottomRight.y != bottomLeft.y {
                onSkewPressed(skewButton)
            } else {
                //let topLeft0 = originalPoints[0]
                //let bottomLeft0 = originalPoints[1]
                let bottomRight0 = originalPoints[2]
                let topRight0 = originalPoints[3]
                
                let w = Int(topRight.x - topLeft.x)
                let h = Int(bottomLeft.y - topLeft.y)
                
                if topLeft.x == 0 && bottomLeft.x == 0 && topRight.x == topRight0.x && bottomRight.x == bottomRight0.x && w == h {
                    onSquarePressed(squareButton)
                } else {
                    onCustomPressed(customButton)
                }
            }
        }
        
        // Disable the button at last because onPressed() enables the button.
        applyButton.isEnabled = false
        
        trackingName = "cropphoto"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        completion?()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        adjustView.frame = imageView.frame
        showPoints()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func onOriginalPressed(_ sender: UIButton) {
        updateButtonSelection(sender)
        adjustView.isHidden = true
        adjustView.changeCropMode(kOriginalCropMode)
    }
    
    @IBAction func onCustomPressed(_ sender: UIButton) {
        updateButtonSelection(sender)
        adjustView.isHidden = false
        adjustView.changeCropMode(kCustomCropMode)
    }
    
    @IBAction func onSquarePressed(_ sender: UIButton) {
        updateButtonSelection(sender)
        adjustView.isHidden = false
        adjustView.changeCropMode(kSquareCropMode)
    }
    
    @IBAction func onSkewPressed(_ sender: UIButton) {
        updateButtonSelection(sender)
        adjustView.isHidden = false
        adjustView.changeCropMode(kSkewCropMode)
    }
    
    @IBAction func onApplyPressed(_ sender: UIBarButtonItem) {
        track("apply")
        sender.isEnabled = false
        
        if originalButton.isSelected {
            croppedImage = nil
            points = []
            navigationController?.popViewController(animated: true)
            return
        }
        
        points = getPoints()
        
        var arr: [ NSValue ] = []
        for p in points {
            arr.append(NSValue(cgPoint: p))
        }
        
        SVProgressHUD.show()
        
        async(.background) {
            let result = KPOpenCV.kpCropImage(self.originalImage, withPoints: arr)!
            self.croppedImage = result["image"] as? UIImage
            async {
                SVProgressHUD.dismiss()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    // MARK: - Private
    
    fileprivate func updateImage() {
        if imageView == nil { return }
        imageView.image = originalImage
        imageView.contentMode  = .scaleAspectFit
    }
    
    fileprivate func updateButtonSelection(_ sender: UIButton) {
        customButton.isSelected = false
        originalButton.isSelected = false
        squareButton.isSelected = false
        skewButton.isSelected = false
        sender.isSelected = true
        applyButton.isEnabled = true
    }
    
    fileprivate lazy var originalPoints: [CGPoint] = {
        let points = [ CGPoint(x: 0, y: 0),
                       CGPoint(x: 0, y: self.imageView.image!.size.height),
                       CGPoint(x: self.imageView.image!.size.width, y: self.imageView.image!.size.height),
                       CGPoint(x: self.imageView.image!.size.width, y: 0) ]
        return points
    }()
    
    fileprivate func showPoints() {
        let imageSize = imageView.image!.size
        let ratio = fmin(imageView.bounds.width / imageSize.width, imageView.bounds.height / imageSize.height)
        
        var dx = (imageView.bounds.size.width - imageView.image!.size.width * ratio) / 2
        var dy = (imageView.bounds.size.height - imageView.image!.size.height * ratio) / 2
        dx += (imageView.frame.origin.x - adjustView.frame.origin.x)
        dy += (imageView.frame.origin.y - adjustView.frame.origin.y)
        
        if adjustView.originalRect.isEmpty {
            let ptTopLeft = originalPoints[0]
            let ptBottomLeft = originalPoints[1]
            let ptBottomRight = originalPoints[2]
            let ptTopRight = originalPoints[3]
            
            adjustView.topLeftCorner(to: CGPoint(x: ptTopLeft.x * ratio + dx, y: ptTopLeft.y * ratio + dy))
            adjustView.topRightCorner(to: CGPoint(x: ptTopRight.x * ratio + dx, y: ptTopRight.y * ratio + dy))
            adjustView.bottomRightCorner(to: CGPoint(x: ptBottomRight.x * ratio + dx, y: ptBottomRight.y * ratio + dy))
            adjustView.bottomLeftCorner(to: CGPoint(x: ptBottomLeft.x * ratio + dx, y: ptBottomLeft.y * ratio + dy))
            adjustView.caculateSquareSide()
            
            let width = (ptTopRight.x * ratio + dx) - (ptTopLeft.x * ratio + dx)
            let height = (ptBottomLeft.y * ratio + dy) - (ptTopRight.y * ratio + dy)
            let x = ptTopLeft.x * ratio + dx
            let y = ptTopLeft.y * ratio + dy
            
            adjustView.originalRect = CGRect(x: x, y: y, width: width, height: height)
            adjustView.updateOriginalRect()
        }
        
        let ptTopLeft, ptBottomLeft, ptBottomRight, ptTopRight: CGPoint
        
        if points.count == 4 {
            ptTopLeft = points[0]
            ptBottomLeft = points[1]
            ptBottomRight = points[2]
            ptTopRight = points[3]
        } else {
            ptTopLeft = originalPoints[0]
            ptBottomLeft = originalPoints[1]
            ptBottomRight = originalPoints[2]
            ptTopRight = originalPoints[3]
        }
        adjustView.topLeftCorner(to: CGPoint(x: ptTopLeft.x * ratio + dx, y: ptTopLeft.y * ratio + dy))
        adjustView.topRightCorner(to: CGPoint(x: ptTopRight.x * ratio + dx, y: ptTopRight.y * ratio + dy))
        adjustView.bottomRightCorner(to: CGPoint(x: ptBottomRight.x * ratio + dx, y: ptBottomRight.y * ratio + dy))
        adjustView.bottomLeftCorner(to: CGPoint(x: ptBottomLeft.x * ratio + dx, y: ptBottomLeft.y * ratio + dy))
    }
    
    fileprivate func getPoints() -> [CGPoint] {
        
        // zvika
        
        //  int screenWidth = [UIScreen mainScreen].currentMode.size.width;
        
        var scaleFactor = CGFloat(1)
        
        
        let tx = (imageView.frame.origin.x - adjustView.frame.origin.x)
        let ty = (imageView.frame.origin.y - adjustView.frame.origin.y)
        
        // print("This is the origin offset tx =" +  "\(tx)" + "  ty=" + "\(ty) \n" );
        
        
        var ptBottomLeft = adjustView.coordinates(forPoint: 1, withScaleFactor:scaleFactor)
        var ptBottomRight = adjustView.coordinates(forPoint: 2, withScaleFactor:scaleFactor)
        var ptTopRight = adjustView.coordinates(forPoint: 3, withScaleFactor:scaleFactor)
        var ptTopLeft = adjustView.coordinates(forPoint: 4, withScaleFactor: scaleFactor)
        
        // print("adjustView.coordinatesForPoint(X, withScaleFactor:scaleFactor)")
        // print("0ptBottomLeft = \(ptBottomLeft) ptBottomRight= \(ptBottomRight) ptTopRight = \(ptBottomLeft)  ptTopLeft= \(ptBottomRight) \n");
        
        scaleFactor = imageView.contentScale
        // print("scaleFactor =\(scaleFactor)")
        
        // print("adjustView.frame \(adjustView.frame) imageView \(imageView.frame) ")
        // print("imageView \(imageView?.image?.size) ")
        
        // print("++ adjustView W = \(adjustView.frame.width)")
        // print("++ adjustView  H = \(adjustView.frame.height)")
        // print("++ Ratio adjustView W/H = \(adjustView.frame.width / adjustView.frame.height)")
        // print("++ Ratio adjustView W/H = \(adjustView.frame.height / adjustView.frame.width)")

        
        // print(">> imageView W = \(imageView?.image?.size.width)!")
        // print(">> imageView  H = \(imageView?.image?.size.height)!")
        // print(">> Ratio imageView = \((imageView?.image?.size.width)! / (imageView?.image?.size.height)!)")

        // print("MyScaleFacor height \(adjustView.frame.height / (imageView?.image?.size.height)!)")
        // print("MyScaleFacor width \(adjustView.frame.width / (imageView?.image?.size.width)!)")
        
        ptBottomLeft = CGPoint(x: (ptBottomLeft.x - tx) / scaleFactor, y: (ptBottomLeft.y - ty) / scaleFactor);
        ptBottomRight = CGPoint(x: (ptBottomRight.x - tx) / scaleFactor, y: (ptBottomRight.y - ty) / scaleFactor);
        ptTopRight = CGPoint(x: (ptTopRight.x - tx) / scaleFactor, y: (ptTopRight.y - ty) / scaleFactor);
        ptTopLeft = CGPoint(x: (ptTopLeft.x - tx) / scaleFactor, y: (ptTopLeft.y - ty) / scaleFactor);
        
        // print("1ptBottomLeft = \(ptBottomLeft) ptBottomRight= \(ptBottomRight) ptTopRight = \(ptBottomLeft)  ptTopLeft= \(ptBottomRight)");
        
        
        
        
        let dx = (imageView.bounds.size.width - imageView.image!.size.width * scaleFactor) / 2 / scaleFactor;
        let dy = (imageView.bounds.size.height - imageView.image!.size.height * scaleFactor) / 2 / scaleFactor;
        
        // print("dx = \(dx) dy= \(dy)");
        
        ptBottomLeft = CGPoint(x: ptBottomLeft.x - dx, y: ptBottomLeft.y - dy);
        ptBottomRight = CGPoint(x: ptBottomRight.x - dx, y: ptBottomRight.y - dy);
        ptTopRight = CGPoint(x: ptTopRight.x - dx, y: ptTopRight.y - dy);
        ptTopLeft = CGPoint(x: ptTopLeft.x - dx, y: ptTopLeft.y - dy);
        
        // print("2ptBottomLeft = \(ptBottomLeft) ptBottomRight= \(ptBottomRight) ptTopRight = \(ptBottomLeft)  ptTopLeft= \(ptBottomRight)");
        
        
        
        return [ ptTopLeft, ptBottomLeft, ptBottomRight, ptTopRight]
    }
}

// MARK: - MADrawRectDelegate

extension CropViewController : MADrawRectDelegate {
    func rectDidChange(_ rect: MADrawRect) {
        applyButton.isEnabled = true
    }
}

extension UIImageView {
    var contentScale: CGFloat {
        let imageSize = image!.size
        let imageScale = fmin(bounds.width / imageSize.width, bounds.height / imageSize.height)
        return imageScale
    }
}
