//
//  CarouselViewClass.swift
//  Keepy
//
//  Created by Aakash Wadhwa on 23/04/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

import UIKit
import SDWebImage

@objc protocol CarouselViewProtocol {
    func  closeTheView(sender: CarouselViewClass)
}

class CarouselViewClass: UIView , iCarouselDataSource, iCarouselDelegate
{
    var skus = [NSMutableDictionary]()
    var inputArray = [[String: Any]]()
    var inputProductIds = [String]()
    var productPriceArray = [String]()
    var carouserlView = iCarousel()
    var currentVC = UIViewController ()
    
    var leftButton = UIButton()
    var rightButton = UIButton()
    var closeButton = UIButton()
    var imageURL = ""
    var delegate : CarouselViewProtocol! = nil
    
    override func draw(_ rect: CGRect) {
//        self.addSubview(rightButton)
//        self.addSubview(leftButton)
        self.addSubview(self.closeButton)
        self.addSubview(carouserlView)
    
//        self.bringSubview(toFront:  self.rightButton)
//        self.bringSubview(toFront:  self.leftButton)
//        
//        self.leftButton.layer.zPosition = 1
//        self.rightButton.layer.zPosition = 1
    }
    override init(frame:CGRect) {
        super.init(frame:frame)
    }

    init( imageURL: String , currentViewController: UIViewController, frameOfView: CGRect) {
        super.init(frame: frameOfView);
        
        let sku1 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/fafe13431ddedbb48d68068fb7503b18.png","sku":"StandardMug-FullImage-11oz", "price":"--", "productId":"61"]
        let sku2 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/380a221ba0727d429749d91151c77f16.png","sku":"Framed_10x10_Black_Gloss", "price":"--", "productId":"41"]
        let sku3 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/05286fa3ae070b9cffb0b3916ad3f658.png","sku":"MetalMagnet_2x2", "price":"--", "productId":"116"]
        self.skus = [sku2, sku1, sku3]
        
        currentVC = currentViewController
        self.imageURL = imageURL
      
        self.backgroundColor = UIColor.init(red: 44.0/255.0, green: 142.0/255.0, blue: 161.0/255.0, alpha: 1)

        // Adding Buttons and Carousel View
        self.rightButton = UIButton.init(frame: CGRect(x: self.frame.size.width - 60, y: self.frame.size.height / 2 - 25 , width: 50, height: 50))
        self.rightButton.setImage(UIImage.init(named: "rightIcon"), for: .normal)
        self.leftButton = UIButton.init(frame: CGRect(x: self.frame.origin.x + 10, y: self.frame.size.height / 2 - 25, width: 50, height: 50))
        self.leftButton.setImage(UIImage.init(named: "leftIcon"), for: .normal)
        
        //Close Button
        self.closeButton = UIButton.init(frame: CGRect(x: self.frame.size.width - 55, y: 0 , width: 50, height: 30))
        self.closeButton.setTitle("Close", for: .normal)
        self.closeButton.setTitleColor(UIColor.yellow, for: .normal)
        self.closeButton.addTarget(self, action: #selector(closeTheCustomCarouselView), for: .touchUpInside)
       
        //Carousel View
        self.carouserlView.frame = CGRect(x: 0, y: 30, width: self.frame.width , height: 160)
        
        
        self.carouserlView.type = .coverFlow
        self.carouserlView.delegate = self
        self.carouserlView.dataSource = self
        self.carouserlView.backgroundColor = UIColor.white
        return;
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented"); }
    
    func getTheImagesArrayForAllTheProducsts(imageURL: String)  {
        let parameterMug =       ["SKU" : "StandardMug-FullImage-11oz",
                                "Template": "Single", "Images": [ [ "LayerId": "06B1D", "Image": ["Url": imageURL, "MaxFit": "true", "X1": "640", "X2": "3040", "Y1": "177", "Y2": "1299" ] ] ] ] as? [String: Any]
        
        let parameterFramedPrints = [
            "SKU": "Framed_10x10_Black_Gloss",
            "Template": "Single",
            "Images": [
                [
                    "LayerId": "22DDE",
                    "SpaceId": "32618",
                    "Image": [
                        "Url": imageURL,
                        "MaxFit": "true",
                        "X1": "1050", "X2": "4050", "Y1": "1050", "Y2": "4050"
                    ]
                ]
            ]
            ] as? [String: Any]
        
        
        let parameterMetalMagnet = [
            "SKU": "MetalMagnet_2x2",
            "Template": "Single",
            "Images": [
                [
                    "LayerId": "F93DF",
                    "SpaceId": "F45D9",
                    "Image": [
                        "Url": imageURL,
                        "MaxFit": "true",
                         "X1": "0", "X2": "657", "Y1": "0", "Y2": "657"
                    ]
                ]
            ]
            ] as? [String: Any]
        
        let parameterPhoneCase = [
            "SKU": "PhoneCase-iphone6Plus-Gloss",
            "Template": "Single",
            "Images": [
            [
            "LayerId": "26CB7",
            "SpaceId": "02136",
            "Image": [
            "Url": imageURL,
            "MaxFit": "false",
            "X1": "0", "X2": "1254", "Y1": "0", "Y2": "2083"
            ]
            ]
            ]
        ] as? [String: Any]
        
        let parametersGreetFold = [
            "SKU": "Greet-Fold-3.5x5-Dull-Single-1Pack",
            "Template": "Single",
            "Images": [
            [
            "LayerId": "88E6E",
            "SpaceId": "D739D",
            "Image": [
            "Url": imageURL,
            "MaxFit": "false"
            ]
            ]
            ]
        ] as? [String: Any]
        
        let parameterCanvasMini = [
            "SKU": "CanvasMini-4x6",
            "Template": "Single",
            "Images": [
            [
            "LayerId": "D579E",
            "SpaceId": "83B4A",
            "Image": [
            "Url": imageURL,
            "MaxFit": "true",
            "X1": "0", "X2": "1237", "Y1": "0", "Y2": "1837"
            
            ]
            ]
            ]
        ] as? [String: Any]
        
        let parameterAcyrlicPrints = [
            "SKU": "AcrylicPrint_1/4in_8x10",
            "Template": "Single",
            "Images": [
            [
            "LayerId": "D2570",
            "SpaceId": "0A7ED",
            "Image": [
            "Url": imageURL,
            "MaxFit": "false",
            "X1": "0", "X2": "3038", "Y1": "0", "Y2": "2438"
            
            ]
            ]
            ]
        ] as? [String: Any]
        
        let parameterAdjustableStrobe = [
            "SKU": "AdjustableTote-16x16",
            "Template": "Single",
            "Images": [
            [
            "LayerId": "B71F9",
            "SpaceId": "6547B",
            "Image": [
            "Url": imageURL,
            "MaxFit": "true"
            ]
            ]
            ]
                ] as? [String: Any]
        
        let parameterArtposters = [
            "SKU": "ArtPoster-85x11",
            "Template": "Single",
            "Images": [
            [
            "LayerId": "FC598",
            "SpaceId": "A9C96",
            "Image": [
            "Url": imageURL,
            "MaxFit": "true"
            ]
            ]
            ]
            ] as? [String: Any]
        
        let parameterHoodies = [
            "SKU": "Apparel-DTG-Hoodie-Gildan-18500-L-SportGrey-Mens-CB",
            "Template": "Single",
            "Images": [
            [
            "LayerId": "3F71A",
            "SpaceId": "0D624",
            "Image": [
            "Url": imageURL,
            "MaxFit": "true"
            ]
            ]
            ]
            ] as? [String: Any]
        
        self.inputArray = [parameterMug!, parameterFramedPrints!, parameterMetalMagnet!,parameterPhoneCase!]
        
        self.callAPI(inputArray[0], index: 0)
        
//        let parameterFramedPrints = [
//            "SKU": "Framed_10x10_Black_Gloss",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "22DDE",
//                    "SpaceId": "32618",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "true",
//                        "X1": "1050", "X2": "4050", "Y1": "1050", "Y2": "4050"
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parameterMug =  ["SKU" : "StandardMug-FullImage-11oz",
//                             "Template": "Single",
//                             "Images": [ [ "LayerId": "06B1D",
//                                           "Image": ["Url": imageURL,
//                                                     "MaxFit": "true",
//                                                     "X1": "640", "X2": "3040", "Y1": "177", "Y2": "1299" ] ] ] ] as? [String: Any]
//        
//        let parameterMetalMagnet = [
//            "SKU": "MetalMagnet_2x2",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "F93DF",
//                    "SpaceId": "F45D9",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "true",
//                        "X1": "0", "X2": "657", "Y1": "0", "Y2": "657"
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parameterPhoneCase = [
//            "SKU": "PhoneCase-iphone6Plus-Gloss",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "26CB7",
//                    "SpaceId": "02136",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "false",
//                        "X1": "0", "X2": "1254", "Y1": "0", "Y2": "2083"
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parametersGreetFold = [
//            "SKU": "Greet-Fold-3.5x5-Dull-Single-1Pack",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "88E6E",
//                    "SpaceId": "D739D",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "false"
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parameterCanvasMini = [
//            "SKU": "CanvasMini-4x6",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "D579E",
//                    "SpaceId": "83B4A",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "true",
//                        "X1": "0", "X2": "1237", "Y1": "0", "Y2": "1837"
//                        
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parameterAcyrlicPrints = [
//            "SKU": "AcrylicPrint_1/4in_8x10",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "D2570",
//                    "SpaceId": "0A7ED",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "false",
//                        "X1": "0", "X2": "3038", "Y1": "0", "Y2": "2438"
//                        
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parameterAdjustableStrobe = [
//            "SKU": "AdjustableTote-16x16",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "B71F9",
//                    "SpaceId": "6547B",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "true"
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parameterArtposters = [
//            "SKU": "ArtPoster-85x11",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "FC598",
//                    "SpaceId": "A9C96",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "true"
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        let parameterHoodies = [
//            "SKU": "Apparel-DTG-Hoodie-Gildan-18500-L-SportGrey-Mens-CB",
//            "Template": "Single",
//            "Images": [
//                [
//                    "LayerId": "3F71A",
//                    "SpaceId": "0D624",
//                    "Image": [
//                        "Url": imageURL,
//                        "MaxFit": "true"
//                    ]
//                ]
//            ]
//            ] as? [String: Any]
//        
//        self.inputArray = [ parameterFramedPrints!,parameterMug!, parameterMetalMagnet!,parameterPhoneCase!,parametersGreetFold!,parameterCanvasMini!,parameterAcyrlicPrints!,parameterAdjustableStrobe!,parameterArtposters!,parameterHoodies!]
    }
    func getThePriceForTheProducts(){
        //self.inputProductIds = ["61","41","116","57"]
        let sku = self.skus[0];
        self.callAPIForProductPrice(sku.value(forKey: "productId") as! String, index: 0)
    }
    func callAPIForProductPrice(_ parameters : String, index: Int) {
        ServerComm.instance().getThePrice(ofTheProducts: parameters, withSuccessBlock: { response in
            print("Response OF API:\(response ?? "" )")
            if let result  = response as? [String: Any] {
                    if let productVariant = result["ProductVariants"] as? [[String: AnyObject?]] {
                        if let formatedPrice = productVariant[0]["PriceInfo"]!!["FormattedPrice"] as? String {
                            self.productPriceArray.append(formatedPrice)
                            print("Array Count:\(self.productPriceArray.count)")
                            
                            for temp in self.productPriceArray {
                                print("Prices :\(temp)")
                            }
                        }
                    }
                    if index < 2 {
                        self.callAPIForProductPrice(self.inputProductIds[index + 1], index: index + 1)
                    }
                    if self.productPriceArray.count == 3 {
                        UserDefaults.standard.set(self.productPriceArray, forKey: "ProductPriceArray")
                        UserDefaults.standard.synchronize()
                    }
                
            }
        }) { (e: Error!) in
            print("Error is: \(e.localizedDescription)")

        }
    }
    func callAPI(_ parameters : [String: Any], index: Int) {
        ServerComm.instance().getTheProductImage(withParameters: parameters, withSuccessBlock: { response in
            print("Response OF API:\(response ?? "" )")
            if let result  = response as? [String: Any] {
                if result["HadError"] as? Bool == false {
                    if let imageURL = result["Images"] as? [[String: AnyObject?]] {
                        if let imageURL = imageURL[0]["Url"] as? String {
                            let skuObj = self.skus[index]
                            skuObj["imageURL"] = imageURL
                            print("Array Count:\(self.skus.count)")
                            
                            for temp in self.skus {
                                print("SKU Obj:\(temp)")
                            }
                        }
                    }
                    if index < 2 {
                        self.callAPI(self.inputArray[index + 1], index: index + 1)
                    }
                    if self.skus.count == 3 {
                        UserDefaults.standard.set(self.skus, forKey: "ProductImageArray")
                        UserDefaults.standard.synchronize()
//                        self.carouserlView.reloadData()
                    }
                }
            }
            
        }) { (e: Error!) in
            print("Error is: \(e.localizedDescription)")
            
        }
    }
 
    // MARK:- iCarousel Delegates
    func numberOfItems(in carousel: iCarousel) -> Int {
//        return self.imageArray.count
        return self.skus.count

    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var itemView: UIImageView
        
        //reuse view if available, otherwise create a new view
        if let view = view as? UIImageView {
            itemView = view
            //get a reference to the label in the recycled view
        } else {
            //don't do anything specific to the index within
            //this `if ... else` statement because the view will be
            //recycled and used with other index values later
            
            itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 140 , height: 140))
            itemView.layer.borderWidth = 1;
            itemView.layer.borderColor = UIColor.gray.cgColor;
            itemView.backgroundColor = UIColor.clear
            let skuObj : NSMutableDictionary = self.skus[index]
//            let image = self.imageArray[index]
//            itemView.image = UIImage.init(named: image)
//            itemView.contentMode = .scaleAspectFit

            if let encoded = (skuObj["imageURL"] as? String)?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encoded)
            {
                print(url)
                SDWebImageManager.shared().downloadImage(with: url, options: SDWebImageOptions.highPriority, progress: nil, completed: { (image: UIImage!, error: Error!, type: SDImageCacheType, finished: Bool, url: URL!) in

                    if error == nil
                    {
                        itemView.image = image
                    } else {
                        itemView.image = UIImage.init(named: "AppIcon")
                        print("Error for showing Image:\(error.localizedDescription)")
                    }
                })

                itemView.contentMode = .scaleAspectFit
            }else{
                print("Spmething is wrong")
            }
            //let url = URL(string: self.imageArray[index])
            
//            itemView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "AppIcon"))
            
            
        }
        return itemView
    }
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.8
        }else if(option == .tilt){
            return value * 0.8
        }
        print(option)
        return value
    }
    
    func reloadCarouselData()  {
        carouserlView.reloadData()
        carouserlView.currentItemIndex = Int(ceil(Double(self.skus.count/2)));
    }
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        PrintIOManager.showPreselectedPrintIODialog(in: currentVC)
    }
    func carouselDidScroll(_ carousel: iCarousel) {
        
    }
    func closeTheCustomCarouselView()  {
        delegate.closeTheView(sender: self)
    }
}
