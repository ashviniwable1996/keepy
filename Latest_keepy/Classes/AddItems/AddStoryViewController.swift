//
//  AddStoryViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/23/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import AVKit
import Photos
import SVProgressHUD
import SDWebImage
import Mixpanel

class AddStoryViewController2: UITableViewController {
    var storyItems: [StoryItem] = []
    var localItem: KPLocalItem?
    var share = true
    var storyTitle: String?
    var tags: [KPLocalTag] = []
    var place: KPLocalPlace?
    var when: Date?
    // var shouldPushFeedsVC:Bool = false
    var selectedKidIds: Set<UInt> = []
    
    var completion: ((Void)->Void)?
    
    var editInfo = ImageEditInfo()
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    fileprivate var collectionId: Int?
    var prepareOperationQueue: OperationQueue?
    var prepareOperations: [PrepareOperation] = []
    
    fileprivate var lastDate: Date {
        var date = Date.distantPast
        if let localItem = localItem {
            return Date(timeIntervalSince1970: localItem.createDate)
        }
        for i in storyItems {
            if let d = i.asset?.creationDate {
                if d.timeIntervalSince(date) > 0 {
                    date = d
                }
            }
        }
        return date == Date.distantPast ? Date() : date
    }
    
    // MARK: - Voice/Video comment
    
    var voiceRecordingData: Data?
    var voiceRecordingLength: Int = 0
    var videoRecordingData: Data?
    var videoRecordingLength: Int = 0
    var videoRecordingImage: UIImage?
    
    var voiceStoryToDelete: KPLocalStory?
    var videoStoryToDelete: KPLocalComment?
    
    // MARK: - View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        
        StoryDateTableViewCell.register(tableView: tableView)
        SelectCell.register(tableView: tableView)
        ToggleCell.register(tableView: tableView)
        KidsPickerTableViewCell.register(tableView: tableView)
        AssetsListTableViewCell.register(tableView: tableView)
        VoiceVideoTableViewCell.register(tableView: tableView)
        
        // If this is the root view controller, this is for editting an existing item.
        if navigationController?.viewControllers.count == 1 {
            navigationItem.title = NSLocalizedString("edit story", comment: "")
        } else {
            // This view should not go back. Why? Absurd.
            //navigationItem.leftBarButtonItem = nil  // Use the default Back button instead.
        }
        
        if let localItem = localItem {
            storyTitle = localItem.title
            
            if let ids = KPLocalKidItem.fetchKidIDs(for: localItem) {
                for i in KPLocalKid.ids(to: ids) {
                    if let kid = i as? KPLocalKid {
                        selectedKidIds.insert(kid.serverID)
                    }
                }
            }
            if let tagIds = KPLocalItemTag.fetchIDs(for: localItem) as? [NSNumber] {
                for i in tagIds {
                    // zvika526 if let fixes the crash bug
                    if let tag = KPLocalTag.fetch(byLocalID: UInt(i.intValue)) {
                        tags.append(tag)
                    }
                }
            }
            place = KPLocalPlace.fetch(byLocalID: UInt(localItem.placeID))
            when = Date(timeIntervalSince1970: localItem.itemDate)
            share = localItem.shareStatus == 1
            
            if let story = KPLocalStory.fetch(byLocalID: UInt(localItem.storyID)) {  // audio story
                voiceRecordingLength = story.storyLength
            }
            
            if StoryItemType(rawValue: localItem.itemType) == .quote {
                let item = StoryItem(type: .quote)
                item.extraData = localItem.extraData
                item.localItem = localItem
                self.localItem = nil
                storyItems = [item]
                print("FNTRACE loadLoclItemImage1");
                loadLocalItemImage(localItem) { [weak self] in
                    let originalImage = $0.square()
                    let thumbnail = originalImage.resize(size: CGSize(width: 128, height: 128))
                    print("FNTRACE EOI7")
                    self?.storyItems[0].editInfo.originalImage = originalImage
                    self?.storyItems[0].editInfo.thumbnail = thumbnail
                }
            }
            
        } else if let kids = KPLocalKid.kidsOrderByBirthday() {
            if kids.count == 1 {
                selectedKidIds = [kids.first!.serverID]
            }
        }
        
        trackingName = "addstory"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        shouldShowProgress = false
        updateSaveButton()
        NotificationCenter.default.addObserver(self, selector: #selector(AddStoryViewController2.uploadOperationNotification(_:)), name: NSNotification.Name(rawValue: "UploadOperation"), object: nil)
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        prepareAssets()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        UIApplication.shared.isIdleTimerDisabled = false
        
        if isMovingFromParentViewController {
            prepareOperationQueue?.cancelAllOperations()
            completion?()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0: return 3  // kids, photos, comments
        case 1: return 1  // categories
        case 2: return 2  // when, where
        case 3: return 1  // share
        default: break
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { return CGFloat.leastNormalMagnitude }
        return super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    func configureCell(_ cell: UITableViewCell, indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let c = cell as! KidsPickerTableViewCell
                c.label.text = NSLocalizedString("who is in the picture?", comment: "")
                c.selectedKidIds = selectedKidIds
                c.kids = KPLocalKid.kidsOrderByBirthday()
                c.onKidPressed = { [weak self] in
                    self?.selectedKidIds = c.selectedKidIds
                    self?.updateSaveButton()
                }
            case 1:
                let c = cell as! AssetsListTableViewCell
                c.items = storyItems
                if let localItem = localItem {
                    if let image = self.editInfo.editedImage {
                        var thumbnail = image.square().resize(size: CGSize(width: 500, height: 500))
                        if localItem.itemType == 4 {  // Video
                            let image = UIImage(named: "play-video-small-gallery")!
                            thumbnail = thumbnail.append(image)
                        }
                        
                        c.itemImage = thumbnail
                        
                    } else {
                        print("FNTRACE loadLoclItemImage main2");
                        loadLocalItemImage(localItem) {
                            var thumbnail = $0.square().resize(size: CGSize(width: 500, height: 500))
                            if localItem.itemType == 4 {  // Video
                                let image = UIImage(named: "play-video-small-gallery")!
                                thumbnail = thumbnail.append(image)
                            }

                            c.itemImage = thumbnail
                        }
                    }
                }
                c.textView.text = storyTitle
                c.textView.placeholder = NSLocalizedString("add title or description...", comment: "")
                c.onTextDidChange = { [weak self] in
                    self?.storyTitle = c.textView.text
                }
                c.onAssetDeletePressed = { [weak self] (idx: Int) in
                    self?.onAssetDeletePressed(idx)
                }
                c.onAssetEditPressed = { [weak self] (idx: Int) in
                    self?.onAssetEditPressed(idx)
                }
            default:
                let voiceButton: String
                if voiceRecordingLength > 0 || (voiceStoryToDelete == nil && findVoiceStory() != nil) {
                    voiceButton = "edit"
                } else {
                    voiceButton = "add voice"
                }
                let videoButton: String
                if videoRecordingLength > 0 || (videoStoryToDelete == nil && findVideoStory() != nil) {
                    videoButton = "edit"
                } else {
                    videoButton = "add video"
                }
                
                let c = cell as! VoiceVideoTableViewCell
                c.voiceButton.setTitle(voiceButton, for: UIControlState())
                c.videoButton.setTitle(videoButton, for: UIControlState())
                c.onVoicePressed = { [weak self] in self?.openVoiceRecordViewController() }
                c.onVideoPressed = { [weak self] in self?.openVideoRecordViewController() }
            }
            break
            
        case 1:  // categories
            let c = cell as! SelectCell
            c.textLabel!.text = NSLocalizedString("categories", comment: "")
            c.placeHolder = "select"
            
            var arr = [String]()
            for i in tags {
                arr.append(i.name)
            }
            c.detailText = arr.joined(separator: ",")
            
        case 2:
            let c = cell as! SelectCell
            switch indexPath.row {
            case 0:  // when
                let c = cell as! StoryDateTableViewCell
                c.date = when ?? lastDate
                
            default:  // where
                c.textLabel!.text = NSLocalizedString("where", comment: "")
                c.placeHolder = "add"
                c.detailText = place?.title
            }
            
        default:
            let c = cell as! ToggleCell
            c.textLabel!.text = NSLocalizedString("share with fans", comment: "")
            c.toggle.isOn = share
            c.valueChanged = { [weak self] in
                self?.share = c.toggle.isOn
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type: UITableViewCell.Type
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0: type = KidsPickerTableViewCell.self
            case 1: type = AssetsListTableViewCell.self
            default: type = VoiceVideoTableViewCell.self
            }
        case 1:
            type = SelectCell.self  // categories
        case 2:
            switch indexPath.row {
            case 0: type = StoryDateTableViewCell.self  // when
            default: type = SelectCell.self  // where
            }
        default: type = ToggleCell.self
        }
        
        let cell = type.dequeue(tableView: tableView, forIndexPath: indexPath)
        configureCell(cell, indexPath: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            performSegue(withIdentifier: "categories", sender: nil)
        case 2:
            switch indexPath.row {
            case 0: performSegue(withIdentifier: "when", sender: nil)
            default: performSegue(withIdentifier: "where", sender: nil)
            }
        default: break
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CategoriesPickerViewController {
            track("category")
            vc.tags = tags
            vc.completion = { [weak self] in
                let path = IndexPath(row: 0, section: 1)
                if let c = self?.tableView.cellForRow(at: path) {
                    self?.tags = vc.tags
                    self?.configureCell(c, indexPath: path)
                }
            }

        } else if let vc = segue.destination as? WherePickerViewController {
            track("place")
            vc.place = place
            vc.completion = { [weak self] in
                let path = IndexPath(row: 1, section: 2)
                if let c = self?.tableView.cellForRow(at: path) {
                    self?.place = vc.place
                    self?.configureCell(c, indexPath: path)
                }
            }
        } else if let vc = segue.destination as? WhenPickerViewController {
            track("date")
            vc.when = when ?? lastDate
            vc.completion = { [weak self] in
                let path = IndexPath(row: 0, section: 2)
                if let c = self?.tableView.cellForRow(at: path) {
                    self?.when = vc.when
                    self?.configureCell(c, indexPath: path)
                }
            }
        } else if let nc = segue.destination as? UINavigationController {
            if let vc = nc.viewControllers.first as? EditAssetViewController {
                track("photo")
                if let item = sender as? StoryItem {
                    vc.info = item.editInfo
                } else if let _ = sender as? KPLocalItem {
                    vc.info = editInfo
                }
                vc.completion = { [weak self] in
                    vc.dismiss(animated: true, completion: nil)
                    let path = IndexPath(row: 1, section: 0)
                    if let c = self?.tableView.cellForRow(at: path) {
                        if let item = sender as? StoryItem {
                            item.editInfo = vc.info
                        } else {
                            self?.editInfo = vc.info
                        }
                        self?.configureCell(c, indexPath: path)
                    }
                }
            } else if let vc = nc.viewControllers.first as? NoteEditViewController {
                track("note")
                let _ = vc.view  // ViewDidLoad()
                vc.textView.text = storyItems[0].extraData
                vc.completion = { [weak self] in
                    vc.dismiss(animated: true, completion: nil)
                    let path = IndexPath(row: 1, section: 0)
                    if let c = self?.tableView.cellForRow(at: path) {
                        let image = CreateNoteImage(vc.textView)
                        if let localItem = self?.localItem {
                            localItem.extraData = vc.textView.text
                        } else {
                            let item = self?.storyItems[0]
                            item?.extraData = vc.textView.text
                            print("FNTRACE EOI1")
                            item?.editInfo.originalImage = image
                            item?.editInfo.thumbnail = image.scaledToFillSize(CGSize(width: 100, height: 100), center: false)
                        }
                        self?.configureCell(c, indexPath: path)
                    }
                }
            }
        } else if let vc = segue.destination as? AVPlayerViewController {
            track("video")
            if let item = sender as? StoryItem {
                vc.player = AVPlayer(url: item.videoUrl!)
            } else if let _ = sender as? KPLocalItem {
            }
        } else if let vc = segue.destination as? ShareCollectionViewController {
            track("saved", properties: ["collectionId" : NSNumber(value: self.collectionId ?? 0), "items" : NSNumber(value: storyItems.count)])
            vc.collectionId = self.collectionId ?? 0
            vc.items = storyItems.map { $0.localItem! }
            vc.caption = storyTitle ?? ""
        }
    }
    
    // MARK: - Notification
    
    var shouldShowProgress = false
    var itemsToUpload = 0
    
    func uploadOperationNotification(_ note: Notification) {
        if !shouldShowProgress { return }
        
        let title: String
        if let index = note.userInfo?["index"] as? Int {
            title = itemsToUpload > 1 ? ": \(index+1)/\(itemsToUpload)" : ""
        } else if let s = note.userInfo?["title"] as? String {
            title = ": \(s)"
        } else {
            return
        }
        
        async {
            if let written = (note.userInfo?["totalBytesWritten"] as AnyObject).int64Value,
                let total = (note.userInfo?["totalBytesExpectedToWrite"] as AnyObject).int64Value {
                let progress = Double(written) / Double(total)
                if progress < 1 {
                    SVProgressHUD.showProgress(Float(progress), status: "Uploading\(title)", maskType: .black)
                }
            } else {
                SVProgressHUD.show(withStatus: "Preparing\(title)", maskType: .black)
            }
        }
    }
    
    // MARK: - Private
    
    fileprivate func prepareAssets() {
        prepareOperationQueue = OperationQueue()
        prepareOperationQueue!.maxConcurrentOperationCount = 1  // To save memory
        
        itemsToUpload = storyItems.count
        
        for (idx, i) in storyItems.reversed().enumerated() {
            if i.prepareOperation != nil { return }
            if i.localItem != nil && i.localItem!.serverID != 0 { return }
            
            if i.type == .video {
                let op = VideoPrepareOperation(item: i)
                op.index = idx
                i.prepareOperation = op
                prepareOperationQueue!.addOperation(op)
                prepareOperations.append(op)
            } else {
                let op = ImagePrepareOperation(item: i)
                op.index = idx
                i.prepareOperation = op
                prepareOperationQueue!.addOperation(op)
            }
        }
    }
}

extension AddStoryViewController2 {
    func updateSaveButton() {
        saveButton.isEnabled = selectedKidIds.count > 0
    }
    
    func showError(_ error: Error?) {
        if let e = error {
            track("error", properties: ["message":e.localizedDescription as AnyObject])
            async { [weak self] in
                SVProgressHUD.showError(withStatus: e.localizedDescription)
                self?.updateSaveButton()
                self?.shouldShowProgress = false
            }
        }
    }
    
    // MARK: - IBAction
    
    func onAssetEditPressed(_ index: Int) {
        track("edit")
        let sender: AnyObject
        let type: StoryItemType
        if let localItem = self.localItem {
            sender = localItem
            type = StoryItemType(rawValue: localItem.itemType)!
            
            SVProgressHUD.show(withStatus: "one sec...")
            
            print("FNTRACE onAssetEditPressed-1")
            
            // Here should be original image
            let la = KPLocalAsset.fetch(byLocalID: UInt(localItem.originalImageAssetID))!
            let la2 = KPLocalAsset.fetch(byLocalID: UInt(localItem.imageAssetID))!
            let manager = SDWebImageManager.shared()!
            let url = "\(Defaults.assetsURLString)\(la.urlHash()).jpg"
            let url2 = "\(Defaults.assetsURLString)\(la2.urlHash()).jpg"
            print("FNTRACE onAssetEditPressed-2 \(url)")
            print("FNTRACE onAssetEditPressed-3 \(url2)")
            manager.downloadImage(with: URL(string: url), options: SDWebImageOptions(rawValue: 0),
                progress: { (receivedSize: Int, expectedSize: Int) in
                    
                },
                completed: { [weak self] (image: UIImage?, error: Error?, cacheType: SDImageCacheType, finished: Bool, url: URL?) in
                    if image != nil {
                        
                        let rotatedImage = image!.fixOrientation()
                        
                        print("FNTRACE EOI2")
                        self?.editInfo.originalImage = rotatedImage
                        
                        SVProgressHUD.dismiss()
                        switch type {
                        case .photo: self?.performSegue(withIdentifier: "editPhoto", sender: sender)
                        case .video: self?.performSegue(withIdentifier: "play", sender: sender)
                        case .quote: self?.performSegue(withIdentifier: "editNote", sender: sender)
                        }
                    } else {
                        manager.downloadImage(with: URL(string: url2), options: SDWebImageOptions(rawValue: 0),
                                              progress: { (receivedSize: Int, expectedSize: Int) in
                                                
                        },
                          completed: { [weak self] (image: UIImage?, error: Error?, cacheType: SDImageCacheType, finished: Bool, url: URL?) in
                            self?.editInfo.originalImage = image ?? UIImage(named: "feed-image-frame")  // Kludge. So stupid.
                            print("FNTRACE EOI3")
                            SVProgressHUD.dismiss()
                            switch type {
                            case .photo: self?.performSegue(withIdentifier: "editPhoto", sender: sender)
                            case .video: self?.performSegue(withIdentifier: "play", sender: sender)
                            case .quote: self?.performSegue(withIdentifier: "editNote", sender: sender)
                            }
                        })
                    }
                }
            )
            return
        } else {
            let item = self.storyItems[index]
            type = item.type
            sender = item
            print("FNTRACE onAssetEditPressed2")

            item.requestImage {
                item.editInfo.originalImage = $0?.normalizedImage
                print("FNTRACE EOI4")
                switch type {
                case .photo: self.performSegue(withIdentifier: "editPhoto", sender: sender)
                case .video: self.performSegue(withIdentifier: "play", sender: sender)
                case .quote: self.performSegue(withIdentifier: "editNote", sender: sender)
                }
            }
        }
    }
    
    func onAssetDeletePressed(_ index: Int) {
        track("delete")
        let type: StoryItemType
        if let localItem = self.localItem {
            type = StoryItemType(rawValue: localItem.itemType)!
        } else {
            let item = self.storyItems[index]
            type = item.type
        }
        let title: String
        switch type {
        case .video: title = "delete video"
        case .photo: title = "delete photo"
        case .quote: title = "delete quote"
        }
        let ac = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString("are you sure?", comment: ""), preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "delete", style: .destructive, handler: { (n: UIAlertAction) in
            if let localItem = self.localItem ?? self.storyItems[0].localItem {
                let itemId = localItem.serverID
                
                SVProgressHUD.show(withStatus: "deleting...")
                ServerComm.instance().deleteItem(Int(itemId), withSuccessBlock: { _ in
                    localItem.remove()
                    NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ItemDeletedNotification"), object:localItem.serverID)
                    SVProgressHUD.dismiss()
                    
                    }, andFail: { (e: Error!) in
                        SVProgressHUD.showError(withStatus: e.localizedDescription)
                })
                return
            }
            self.storyItems.remove(at: index)
            if self.storyItems.count > 0 {
                self.updateSaveButton()  // Maybe disabled by onSavePressed
                self.tableView.reloadData()  // Reload "when"
            } else {
                self.navigationController?.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(ac, animated: true, completion: nil)
    }
    
    @IBAction func onCancelPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSavePressed(_ sender: UIBarButtonItem) {
        saveButton.isEnabled = false
        
        print("FNTRACE SAVE PRESSED");
        
        SVProgressHUD.show(withStatus: NSLocalizedString("adding item", comment: ""), maskType: .black)
        shouldShowProgress = true
        
        print("FNTRACE localitemSave");
        print(editInfo);
        
        // If it's for an update, update existing data.
        if storyItems.count == 1 {
            if storyItems[0].type == .quote {
                localItem = storyItems[0].localItem
                localItem?.extraData = storyItems[0].extraData
            }
        }
        if let localItem = localItem {
            if editInfo.points.count > 0 {
                var arr = [String]()
                for i in editInfo.points {
                    arr.append(String(format: "%2.2f,%2.2f", i.x, i.y))
                }
                localItem.cropRect = arr.joined(separator: "|")
            }
            localItem.autoEnhanceOn = editInfo.autoEnhance
            localItem.brightness = Double(editInfo.brightness)
            localItem.saturation = Double(editInfo.saturation)
            localItem.contrast = Double(editInfo.contrast)
            localItem.rotation = Double(editInfo.rotation)
            localItem.title = storyTitle ?? ""
            localItem.shareStatus = share ? 1 : 0
            localItem.placeID = Int(place?.identifier ?? 0)
            if let when = when { localItem.itemDate = when.timeIntervalSince1970 }
            
            print("FNTRACE localitem");
            print("FNTRACE localitem2 \(editInfo)");
            print(editInfo);
            
            if selectedKidIds.count > 0 {
                var localKids = [KPLocalKid]()
                var kids = [String]()
                for i in selectedKidIds {
                    kids.append("\(i)")
                    localKids.append(KPLocalKid.fetch(byServerID: i))
                }
                localItem.kidsIDs = kids.joined(separator: "|")
                KPLocalKidItem.setLinkedKids(localKids, for: localItem)
            }
            
            var tagIds = Set<UInt>()
            for i in tags { tagIds.insert(i.serverID) }
            KPLocalItemTag.setLinkedTags(tags, for:localItem)
            
            let op = UpdateItemOperation(item: localItem, tags: Array(tagIds), info: editInfo)
            async(.background) { [weak self] in
                let q = OperationQueue()
                if let d = self?.voiceRecordingData, let l = self?.voiceRecordingLength {
                    NSLog("updating: voice story")
                    let op = UploadVoiceOperation(item: localItem, length: l, data: d)
                    q.addOperation(op)
                }
                if let d = self?.videoRecordingData, let p = self?.videoRecordingImage, let l = self?.videoRecordingLength {
                    NSLog("updating: video story")
                    let op = UploadVideoCommentOperation(item: localItem, length: l, data: d, preview: p)
                    q.addOperation(op)
                }
                q.waitUntilAllOperationsAreFinished()
                
                self?.prepareOperationQueue?.addOperation(op)
                self?.prepareOperationQueue?.waitUntilAllOperationsAreFinished()
                localItem.saveChanges()
                
                func deleteVoiceStory(_ completion: @escaping (()->())) {
                    if let voiceStoryToDelete = self?.voiceStoryToDelete {
                        ServerComm.instance().deleteStory(Int(voiceStoryToDelete.serverID), withSuccessBlock: { _ in
                            voiceStoryToDelete.remove()
                            NSLog("voice story: deleted")
                            completion()
                        }, andFail: { (error: Error!) in
                            completion()
                        })
                    } else {
                        completion()
                    }
                }
                func deleteVideoStory(_ completion: @escaping (()->())) {
                    if let videoStoryToDelete = self?.videoStoryToDelete {
                        ServerComm.instance().deleteComment(Int(videoStoryToDelete.serverID), withSuccessBlock: { _ in
                            videoStoryToDelete.remove()
                            NSLog("video story: deleted")
                            completion()
                        }, andFail: { _ in
                            completion()
                        })
                    } else {
                        completion()
                    }
                }
                
                deleteVoiceStory() {
                    deleteVideoStory() {
                        async {
                            NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "ItemUpdatedNotification"), object:localItem)
                            SVProgressHUD.dismiss()
                            self?.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
            return
        }
        
        // If it's for adding new items, do it asynchronously.
        async(.background) { [weak self] in self?.save
            { (error: Error?) in
                if let e = error
                {
                    NSLog("!!!!!!!!!!!!!!! %@", e.localizedDescription)
                    self?.showError(e)
                    return
                }
                NSLog("************* save: item is good")
                async(.background) { [weak self] in self?.saveLocalItem()
                    { (error: Error?) in
                        if let e = error
                        {
                            self?.showError(e)
                            return
                        }
//                        UploadTracker.sharedInstance.removeItemToWatchList(localItem.itemId)
                        
                        NSLog("************* save: local item is good")
                        async // Update in the main thread.
                        {
                            SVProgressHUD.dismiss()
                          //  self?.performSegue(withIdentifier: "ShareCollection", sender: nil)
                            
                            // Ashvini made this 2 lines change
                            //  self?.performSegue(withIdentifier: "ShareCollection", sender: nil)
                            self?.navigationController?.dismiss(animated: true, completion: nil)
                            
                            let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
                            let buildNumber = Bundle.main.object(forInfoDictionaryKey:"CFBundleVersion") as? String
                            
                            let appVersion = UserDefaults.standard.value(forKey: "AppVersion") as? String
                            let appBuildNo = UserDefaults.standard.value(forKey: "AppBuildNo") as? String
                            
                            if ((appVersion == nil && appBuildNo == nil) || ((appVersion != versionNumber) || (appBuildNo != buildNumber))){
                                // Aakash
                                // Show Love App only once made change on 13 Apr 2018
                                // Update the AppVersion & Build No in UserDefaults
                                if let appVer = versionNumber {
                                    UserDefaults.standard.setValue(appVer, forKey: "AppVersion")

                                }
                                if let appBuild = buildNumber {
                                    UserDefaults.standard.setValue(appBuild, forKey: "AppBuildNo")
                                }
                                UserDefaults.standard.synchronize()
                                print("Success for App Review")
                                GlobalUtils.showLoveApp()

                            } else {
                                print("Same build no")

                            }
//                            let launchDates = UserDefaults.standard.object(forKey: "LaunchDates") as? Array ?? []
//                            let launchDatesAfterPurchase = UserDefaults.standard.object(forKey: "LaunchDatesAfterPurchase") as? Array ?? []
//
//                            // 1.
//                            if (launchDatesAfterPurchase.count == 1) {
//                                GlobalUtils.showLoveApp()
//                            }
//
//                            // 2.
//                            else {
//                                if (launchDatesAfterPurchase.count >= 2) {
//                                    let firstDate = launchDatesAfterPurchase[launchDatesAfterPurchase.count - 2] as! NSDate
//                                    if (firstDate.daysBetweenDates(startDate: firstDate, endDate: NSDate()) <= 2) {
//                                        GlobalUtils.showLoveApp()
//                                    }
//
//                                    // 3.
//                                    else if (launchDates.count >= 5) {
//                                        let firstDate = launchDates[launchDates.count - 5] as! NSDate
//                                        if (firstDate.daysBetweenDates(startDate: firstDate, endDate: NSDate()) <= 30) {
//                                            GlobalUtils.showLoveApp()
//                                        }
//                                    }
//                                }
//
//                                // 3.
//                                else if (launchDates.count >= 5) {
//                                    let firstDate = launchDates[launchDates.count - 5] as! NSDate
//                                    if (firstDate.daysBetweenDates(startDate: firstDate, endDate: NSDate()) <= 30) {
//                                        GlobalUtils.showLoveApp()
//                                    }
//                                }
//                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Private
    
    fileprivate func save(_ completion: @escaping (_ error: Error?) -> Void) {
        let placeId = place?.serverID ?? 0
        let date = when
        let shareWithFun = share
        let kids = selectedKidIds
        var tagIds = Set<UInt>()
        for i in tags { tagIds.insert(i.serverID) }
        
        var kidsUniqueID = [Int]()
        for i in kids {kidsUniqueID.append(Int(i))}
        UserDefaults.standard.set(kidsUniqueID, forKey: "kidzIDs")
        UserDefaults.standard.synchronize()
        
        NSLog("Waiting until all upload operations are finished...")
        prepareOperationQueue?.waitUntilAllOperationsAreFinished()
        for i in storyItems {
            if let e = i.prepareOperation?.error {
                completion(e)
                return
            }
        }
//^^^^^^
//        async {
//            SVProgressHUD.showWithStatus("Saving", maskType: .Black)
//        }
        print("FNTRACE save1");
        for element in storyItems {
            print(element)
        }
        for i in storyItems {
            i.title = storyTitle ?? ""
            i.kids = kids
            i.placeId = placeId
            i.date = date ?? i.asset?.creationDate ?? Date()
            i.tags = tagIds
            i.share = shareWithFun
            let op = AddItemOperation(item: i)
            i.addOperation = op
            prepareOperationQueue?.addOperation(op)
        }
        
        NSLog("Waiting until all add-item operations are finished...")
        prepareOperationQueue?.waitUntilAllOperationsAreFinished()
        print("FNTRACE ADDCollection1")
        for i in storyItems {
            if let e = i.addOperation!.error {
                completion(e)
                return
            }
        }
        print("FNTRACE ADDCollection2")
        if storyItems.count < 2 {
            return completion(nil)
        }
        print("FNTRACE ADDCollection3")
        // Only when it has two or more items, save them as a Collection.
        let items = storyItems.map { $0.itemId }
        ServerComm.instance().addCollection(storyTitle ?? "",
                                            kids: Array(kids).map{NSNumber(value: $0)},
                                            collectionDate: date ?? lastDate,
                                            tags: Array(tagIds).map{NSNumber(value: $0)},
                                            items: items.map{NSNumber(value: $0)},
                                            place: placeId,
                                            shareStatus: shareWithFun) { (response: [AnyHashable: Any]!, error: Error!) in
                //{"status":0,"result":{"id":427},"error":null}
            if let e = error {
                completion(e)
                return
            }
            
            if let response = response as? [String:AnyObject], let status = response["status"] as? Int, let result = response["result"] as? [String:AnyObject] {
                if status == 0 {
                    self.collectionId = result["id"] as? Int
                    completion(nil)
                    return
                }
            }
            completion(NSError(string: "Something went wrong") as Error)
        }
    }

    fileprivate func saveAsset(_ item: StoryItem, assetHash: String) {
        print("FNTRACE saveASSET");
        if let image = item.editInfo.editedImage ?? item.editInfo.originalImage {
            print("FNTRACE EOI5")
            SaveImageCache(image, assetHash: assetHash)
        } else {
             print("FNTRACE EOI52") //possible soluton here!!
            item.requestImage { SaveImageCache($0, assetHash: assetHash) }  // Load if not existed.
        }
    }

    fileprivate func saveLocalItem(_ completion: (_ error: Error?) -> Void) {
        var operations: [AsyncOperation] = []
        var q = OperationQueue()
        q.maxConcurrentOperationCount = 1
        print("FNTRACE saveLocalItem");
        for item in storyItems {
            saveAsset(item, assetHash: item.assetHash!)
            saveAsset(item, assetHash: item.originalAssetHash!)

            var kids = [KPLocalKid]()
            for i in item.kids {
                if let data = KPLocalKid.fetch(byServerID: i){
                    kids.append(data)
                }
                
            }
            
            print("creating new item: \(item.itemId)")
            print("originalAssetHash: \(item.originalAssetHash)")
            print("assetHash: \(item.assetHash)")
            print("sdVideoAssetHash: \(item.sdVideoAssetHash)")
            let localItem = KPLocalItem.createItem(item.itemId, title: item.title, itemDate: item.date, assetId: item.assetId, assetHash: item.assetHash, itemType: UInt(item.type.rawValue),
                originalAssetId: item.type == .video ? UInt(item.sdVideoAssetId) : item.originalAssetId,
                originalAssetHash: item.type == .video ? item.sdVideoAssetHash : item.originalAssetHash,
                cropRect: item.pointsAsString(), shareStatus: share, rotation: item.editInfo.rotation, saturation: item.editInfo.saturation, contrast: item.editInfo.contrast, brightness: item.editInfo.brightness, autoEnhanceOn: item.editInfo.autoEnhance, extraData: item.extraData, kids: kids)!
            localItem.placeID = Int(place?.identifier ?? 0)
            
            item.localItem = localItem
            
            var tagIds = Set<UInt>()
            for i in KPLocalItemTag.fetchIDs(for: item.localItem) {
                tagIds.insert(UInt((i as! NSString).intValue))
            }
            
            for i in tags {
                if tagIds.contains(i.serverID) { continue }
                
                let itemTagRelation = KPLocalItemTag.create()!
                itemTagRelation.tagID = i.identifier
                itemTagRelation.itemID = localItem.identifier
                itemTagRelation.saveChanges()
            }
            
            print("saving new item: \(item.itemId)")

            //this is tracking the persitance of the item to match its image
            //^^^^^^^^^^^^^^^
            UploadTracker.sharedInstance.addItemToWatchList("\(localItem.serverID),\(item.assetId),\(localItem.imageAssetID)")
            
            
            localItem.saveChanges()
            
            if let data = voiceRecordingData {
                let op = UploadVoiceOperation(item: localItem, length: voiceRecordingLength, data: data)
                q.addOperation(op)
                operations.append(op)
            }
            if let data = videoRecordingData, let preview = videoRecordingImage {
                let op = UploadVideoCommentOperation(item: localItem, length: videoRecordingLength, data: data, preview: preview)
                q.addOperation(op)
                operations.append(op)
            }
            
            
            for i in item.prepareOperation?.uploadOperations ?? [] {
                if i is ImageUploadOperation {
                    
                    q.addOperation(i)
                
                }
            }
            
//            if operations.count > 0 {
//                 Process queue asap and free just to save some memory.
                NSLog("waiting until voice/video operations are finished")
                q.waitUntilAllOperationsAreFinished()
//            }
        }
        
        for op in operations {
            if let e = op.error {
                completion(e)
                return
            }
        }
        
        // Finally, upload video files asynchronoulsy.
        for item in storyItems {
            for i in item.prepareOperation?.uploadOperations ?? [] {
                if i is VideoUploadOperation {
                    q.addOperation(i)
                }
            }
        }
        
        print("all items are saved")
        
        NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()
        print("all items are saved to persisitent store")
        
        // Notifiy "feed" to update.
        for item in storyItems {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ItemAddedNotification"), object:item.localItem)
            GlobalUtils.postNotification(forNewItem: item.localItem)
        }
        
        let m = Mixpanel.sharedInstance()
        m?.people.increment("Number of photos", by: NSNumber(value: storyItems.count))
        GlobalUtils.updateAnalyticsUser()
        
        Defaults.keepiesUsedThisMonth += storyItems.count
        completion(nil)
    }
}

// MARK: - audio / video story

extension AddStoryViewController2 {
    
    fileprivate func openVoiceRecordViewController() {
        let vc: RecordViewController!
        
        if voiceRecordingLength > 0 {
            if let data = voiceRecordingData {
                vc = RecordViewController(data: data, andLength: voiceRecordingLength, andMode: 0)
            } else {
                var data: Data? = nil
                if let story = findVoiceStory() {
                    let asset = KPLocalAsset.fetch(byLocalID: UInt(story.assetID))
                    let url = URL(string: "\(Defaults.assetsURLString)\(asset!.urlHash).mp4")!
                    data = try? Data(contentsOf: url)  // Blocking. Very bad implementation. Fix this later.
                }
                vc = RecordViewController(data: data, andLength: voiceRecordingLength, andMode: 0)
            }
            
        } else {
            let voiceItem: KPLocalItem! = findVoiceStory() == nil ? nil : localItem
            vc = RecordViewController(item: voiceItem, withMode: 0)
        }
        
        vc.completion = { (data: Data?, length: Int, mode: Int, image: UIImage?) in
            self.voiceStoryToDelete = self.findVoiceStory()
            self.voiceRecordingData = data
            self.voiceRecordingLength = length
            self.tableView.reloadData()  // Reload button title.
        }
        
        let _ = vc.view  // run viewDidLoad()
        vc.instructionsLabel.text = makeInstruction()
        
        if let item = localItem {
            print("FNTRACE loadLoclItemImage3");
            loadLocalItemImage(item) {
                vc.itemImage = $0
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            loadStoryItemImage(storyItems[0]) {
                vc.itemImage = $0
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    fileprivate func findVoiceStory() -> KPLocalStory? {
        if let localItem = localItem {
            return KPLocalStory.fetch(byLocalID: UInt(localItem.storyID))
        }
        return nil
    }
    
    fileprivate func findVideoStory() -> KPLocalComment? {
        if let localItem = localItem {
            for i in KPLocalComment.fetchComments(for: localItem) as! [KPLocalComment] {
                if i.isStory {  // video story
                    return i
                }
            }
        }
        return nil
    }
    
    fileprivate func openVideoRecordViewController() {
        let vc: RecordViewController!
        
        if videoRecordingLength > 0 {
            if let data = videoRecordingData {
                vc = RecordViewController(data: data, andLength: videoRecordingLength, andMode: 1)
            } else {
                var data: Data? = nil
                for i in KPLocalComment.fetchComments(for: localItem) as! [KPLocalComment] {
                    if i.isStory {  // video story
                        let asset = KPLocalAsset.fetch(byLocalID: UInt(i.videoAssetID))
                        let url = URL(string: "\(Defaults.assetsURLString)\(asset!.urlHash).mov")!
                        data = try? Data(contentsOf: url)  // Blocking. Very bad implementation. Fix this later.
                        break
                    }
                }
                vc = RecordViewController(data: data, andLength: videoRecordingLength, andMode: 1)
            }
        } else {
            let videoItem: KPLocalItem! = findVideoStory() == nil ? nil : localItem
            vc = RecordViewController(item: videoItem, withMode: 1)
        }
        
        vc.completion = { (data: Data?, length: Int, mode: Int, image: UIImage?) in
            self.videoStoryToDelete = self.findVideoStory()
            self.videoRecordingData = data
            self.videoRecordingLength = length
            self.videoRecordingImage = image
            self.tableView.reloadData()  // Reload button title.
        }
        
        let _ = vc.view  // run viewDidLoad()
        vc.instructionsLabel.text = makeInstruction()
        
        if let item = localItem {
            print("FNTRACE loadLoclItemImage4");
            loadLocalItemImage(item) {
                vc.itemImage = $0
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            loadStoryItemImage(storyItems[0]) {
                vc.itemImage = $0
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    fileprivate func makeInstruction() -> String {
        if let item = localItem {
            if let itemType = StoryItemType(rawValue: item.itemType) {
                switch  itemType {
                case .photo: return "gush about the photo"
                case .video: return "gush about the video"
                case .quote: return "gush about the note"
                }
            }
        } else {
            if storyItems.count == 1 {
                switch storyItems[0].type {
                case .photo: return "gush about the photo"
                case .video: return "gush about the video"
                case .quote: return "gush about the note"
                }
            } else {
                return "gush aobut the collection"
            }
        }
        return "gush aobut this"
    }
    
    func loadLocalItemImage(_ item: KPLocalItem, completion: @escaping ((_ image: UIImage) -> Void)) {
        let la = KPLocalAsset.fetch(byLocalID: UInt(item.imageAssetID))
        
        let manager = SDWebImageManager.shared()!
        let url = "\(Defaults.assetsURLString)\(la!.urlHash())_medium.jpg"
        print("FNTRACE loadLoclItemImage main");
        print(url);
        manager.downloadImage(with: URL(string: url), options: SDWebImageOptions(rawValue: 0),
            progress: { (receivedSize: Int, expectedSize: Int) in },
            completed: { (image: UIImage!, error: Error!, cacheType: SDImageCacheType, finished: Bool, url: URL!) in
                if let image = image {
                    completion(image)
                } else {
                    completion(UIImage(named: "feed-image-frame")!)  // Stupid, but better than crash.
                }
            }
        )
    }
    
    func loadStoryItemImage(_ item: StoryItem, completion: @escaping ((_ image: UIImage) -> Void)) {
        switch item.type {
        case .quote:
            print("FNTRACE EOI6")
            completion(item.editInfo.originalImage!)
            
        case .photo, .video:
            let im = PHImageManager.default()
            let o = PHImageRequestOptions()
            o.isSynchronous = false
            o.isNetworkAccessAllowed = true
            o.version = .current
            
            print("FNTRACE loadstoryitemimage")
            let size = CGSize(width: 300, height: 300)
            im.requestImage(for: item.asset!, targetSize: size, contentMode: .aspectFit, options: o) { (image: UIImage?, info: [AnyHashable: Any]?) in
                if let isDegraded = info?[PHImageResultIsDegradedKey] as? NSNumber {
                    if isDegraded.intValue == 1 { return }
                }
                if let oriented = image?.normalizedImage {
                    completion(oriented)
                } else {
                    completion(UIImage(named: "feed-image-frame")!)  // Stupid, but better than crash.
                }
            }
        }
    }
}

private func SaveImageCache(_ image: UIImage?, assetHash: String) {
    guard let image = image else { return }
    print("FNTRACE saveImageCache");
    
    let key = "\(Defaults.assetsURLString)\(assetHash)"
    let cache = SDImageCache.shared()!
    
    cache.store(image, forKey:"\(key).jpg")
    print("caching: \(image) as \(key).jpg")
    
    let s = image.imageScaled(toFit: CGSize(width: 192, height: 192))
    cache.store(s, forKey:"\(key)_small.jpg")
    print("caching: \(s) as \(key)_small.jpg")
    
    let m = image.imageScaled(toFit: CGSize(width: 600, height: 600))
    cache.store(m, forKey:"\(key)_medium.jpg")
    print("caching: \(m) as \(key)_medium.jpg")
    
    // Do we really need this?
    cache.store(image, forKey:"\(key)_iphone.jpg")
}
