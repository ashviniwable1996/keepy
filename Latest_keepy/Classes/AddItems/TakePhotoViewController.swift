//
//  TakePhotoViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/19/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import AVFoundation
import SVProgressHUD

class TakePhotoViewController: UIViewController {
    var previewLayer: AVCaptureVideoPreviewLayer!
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCaptureStillImageOutput!
    
    var image: UIImage?
    
    var completion: ((Void) -> Void)?
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            let captureInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession.addInput(captureInput)
            captureSession.beginConfiguration()
            captureSession.sessionPreset = AVCaptureSessionPresetPhoto
            captureSession.commitConfiguration()
        } catch let e as NSError {
            NSLog("Camera error: \(e)")
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(previewLayer, at: 0)
        
        stillImageOutput = AVCaptureStillImageOutput()
        captureSession.addOutput(stillImageOutput)
        
        updateOrientation(UIDevice.current)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: "deviceOrientationDidChange:", name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        trackingName = "camera"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        captureSession?.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        captureSession?.stopRunning()
        UIDevice.current.endGeneratingDeviceOrientationNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func onCancelPressed(_ sender: UIBarButtonItem) {
        track("cancel")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onShutterPressed(_ sender: AnyObject) {
        track("shutter")
        if let session = captureSession {
            SVProgressHUD.show()
            
            let connection = stillImageOutput.connections.last as! AVCaptureConnection
            stillImageOutput.captureStillImageAsynchronously(from: connection) { (buffer: CMSampleBuffer?, error: Error?) -> Void in
                session.stopRunning()
                
                if buffer != nil {
                    let data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)!
                    self.image = UIImage(data: data)
                    
                    self.performSegue(withIdentifier: "edit", sender: nil)
                }
                
                SVProgressHUD.dismiss()
            }
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? EditAssetViewController {
            var info = ImageEditInfo()
            info.originalImage = image?.normalizedImage
            vc.info = info
            vc.completion = {
                SVProgressHUD.show()
                let image = vc.info.editedImage ?? vc.info.croppedImage ?? self.image!
                UIImageWriteToSavedPhotosAlbum(image, self, "image:didFinishSavingWithError:contextInfo:", nil)
            }
        }
    }

    // MARK: - UIImageWriteToSavedPhotosAlbum
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutableRawPointer) {
        if let e = error {
            SVProgressHUD.showError(withStatus: e.localizedDescription)
            NSLog("failed to save: \(error)")
        } else {
            SVProgressHUD.dismiss()
            completion?()
            dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - UIDeviceOrientationDidChangeNotification
    
    func deviceOrientationDidChange(_ note: Notification) {
        guard let device = note.object as? UIDevice else { return }
        updateOrientation(device)
    }
    
    // MARK: - Private
    
    fileprivate func updateOrientation(_ device: UIDevice) {
        guard let conn = stillImageOutput.connections.last as? AVCaptureConnection, conn.isVideoOrientationSupported else { return }
        
        let o: AVCaptureVideoOrientation
        switch device.orientation {
        case .portraitUpsideDown: o = .portraitUpsideDown
        case .landscapeLeft: o = .landscapeRight
        case .landscapeRight: o = .landscapeLeft
        default: o = .portrait
        }
        
        conn.videoOrientation = o
    }
}
