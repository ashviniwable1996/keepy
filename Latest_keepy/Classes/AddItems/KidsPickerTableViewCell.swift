//
//  KidsPickerTableViewCell.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import SDWebImage

private let kPadding: CGFloat = 8.0
private let kSpacing: CGFloat = 2.0

class KidsPickerTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var selectedKidIds: Set<UInt> = []
    var kids: [KPLocalKid] = [] {
        didSet {
            for i in scrollView.subviews { i.removeFromSuperview() }
            DispatchQueue.main.async {
                
            
                let imageSize = CGSize(width: self.scrollView.bounds.height - 10, height: self.scrollView.bounds.height - 10)
            
            var rect = CGRect(x: kPadding, y: 0, width: imageSize.width, height: imageSize.height + 4)
                for (i, k) in self.kids.enumerated() {
                
                let defaultImage = GlobalUtils.getKidDefaultImage(k.gender)!
                
                let v = KidViewStory.instance()
                v.imageView.image = defaultImage
                v.label.text = k.name
                v.label.sizeToFit()
                v.setNeedsDisplay()
                v.frame = rect
                v.tag = i
                    v.selected = self.selectedKidIds.contains(k.serverID)
                v.onPressed = {
                    if v.selected {
                        v.selected = false
                        var arr = Set<UInt>()
                        for i in self.selectedKidIds {
                            if i != k.serverID { arr.insert(i) }
                        }
                        self.selectedKidIds = arr
                    } else {
                        v.selected = true
                        self.selectedKidIds.insert(k.serverID)
                    }
                    
                    self.onKidPressed?()
                }
                
                if let a = KPLocalAsset.fetch(byLocalID: UInt(k.imageAssetID)) {
                    let url = URL(string: "\(Defaults.assetsURLString)\(a.urlHash()).jpg")
                    
                    SDWebImageManager.shared().downloadImage(with: url, options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (image: UIImage!, error: Error!, type: SDImageCacheType, finished: Bool, url: URL!) in
                        
                        if error == nil
                        {
                            v.imageView.image = image.imageMasked(withElipse: defaultImage.size)
                        }
                    })
                }
                
                self.scrollView.addSubview(v)
                self.scrollView.contentSize = CGSize(width: rect.maxX + kPadding, height: rect.maxY)
                rect.origin.x += rect.width + kSpacing
            }
            }
        }
    }
    
    var onKidPressed: ((Void)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override class func register(tableView tv: UITableView) {
        registerNib(tableView: tv)
    }
}
