//
//  KidViewStory.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/28/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class KidViewStory: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var selected = false {
        didSet {
            label.textColor = selected ? UIColor.black : UIColor.lightGray
            label.setNeedsDisplay()
            imageView.alpha = selected ? 1.0 : 0.5
        }
    }

    class func instance() -> KidViewStory {
        return Bundle.main.loadNibNamed("KidViewStory", owner:self, options:nil)!.first as! KidViewStory
    }
    
    var onPressed: ((Void)->Void)?

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        onPressed?()
    }
}
