//
//  ShareCollectionViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/20/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import Mixpanel

class ShareCollectionViewController: UIViewController
{
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var keepyButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    @IBOutlet weak var shareAlsoLabel: UILabel!
    @IBOutlet weak var fbLabel: UILabel!
    @IBOutlet weak var twitterLabel: UILabel!
    @IBOutlet weak var keepyLabel: UILabel!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var printProductsButton: UIButton!
    @IBOutlet weak var carouselBackView: UIView!
    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var collectionId: Int = 0
    var items: [KPLocalItem] = []
    var caption: String = ""
    var imagesArray = [String] ()
    fileprivate func canvas(_ numberOfItems: Int = 1) -> UIImage {
        let white0 = UIImage(color: UIColor.white, size: CGSize(width: 150, height: 150), shadow: 5)
        var image: UIImage = white0
        for i in 0..<numberOfItems-1 {
            let degree = 3.0/Double(i+1) + Double(arc4random_uniform(5))
            let sign = arc4random_uniform(1) == 1 ? 1.0 : -1.0
            image = image.insert(white0.rotate(CGFloat(degree * sign)))
        }
        return image
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        twitterButton.isSelected = Defaults.shareWithTwitter
//        fbButton.isSelected = Defaults.shareWithFacebook
        keepyButton.isSelected = Defaults.shareWithKeepy
        updateShareButton()
        
        var kidIds = Array<Int>()
        var tagIds = Array<Int>()
        
        for i in items {
            if let arr = KPLocalKidItem.fetchKidIDs(for: i) as? Array<Int> {
                kidIds += arr
            }
            if let arr = KPLocalItemTag.fetchIDs(for: i) as? Array<Int> {
                tagIds += arr
            }

        }
        
        let kids = KPLocalKid.ids(to: kidIds)
        let kidsName = GlobalUtils.getKidNames(toShare: kids)!
        
        let tags = KPLocalTag.ids(to: tagIds)!
        
        var tagArray = Array<String>()
        for i in tags {
            let s = (i as? KPLocalTag)?.name.replacingOccurrences(of: " ", with: "") ?? ""
            tagArray.append("#\(s)")
        }
        
        let title = caption.trim().isEmpty ? "" : "\"\(caption.trim())\""
        let hashtags = tagArray.joined(separator: " ")
        let s = "\(title) \(kidsName) latest memory cherished using https://keepy.me #childhoodmemories #Ilovemykids \(hashtags)".trim()
        textView.text = s
        
        self.imageView.image = self.canvas(self.items.count)  // Draw canvas
        loadImage()
        
        trackingName = "sharestory"
        
//        DispatchQueue.main.async {
//        self.printProductsButton?.tag = 0
//        self.printProductsButton?.layer.cornerRadius = 9
////        printProductsButton?.layer.borderWidth = 1.5
////        printProductsButton?.layer.borderColor = UIColor.white.cgColor
//        self.printProductsButton?.backgroundColor = UIColor.keepyPinkColor()
//        self.printProductsButton?.titleLabel?.textAlignment = .center
//        self.printProductsButton?.titleLabel?.numberOfLines = 0
//
//        let myNormalAttributedTitle = NSMutableAttributedString(string: "PRINT WITH KEEPY STORE",
//                                                         attributes: [NSForegroundColorAttributeName : UIColor.white,NSFontAttributeName:UIFont.systemFont(ofSize:15.0 )])
//        let myNormalAttributedTitle1 = NSAttributedString(string: "\n 20% off with code MDAY18",
//                                                         attributes: [NSForegroundColorAttributeName : UIColor.white,NSFontAttributeName:UIFont.systemFont(ofSize:10.0 )])
//         myNormalAttributedTitle.append(myNormalAttributedTitle1)
//
//        self.printProductsButton.setAttributedTitle(myNormalAttributedTitle, for: .normal)
//        self.view.bringSubview(toFront: self.printProductsButton)
//        }
    }
    
    func loadImage() {
        let item = items[0]
        let itemID = item.imageAssetID
        let la = KPLocalAsset.fetch(byLocalID: UInt(item.imageAssetID))!
        
        let key = "\(Defaults.assetsURLString)\(la.urlHash())_medium.jpg"
        let keyForRendering = "\(Defaults.assetsURLString)\(la.urlHash())_small.jpg"
        UserDefaults.standard.set(keyForRendering, forKey: "RenderingImage")
        UserDefaults.standard.set(itemID, forKey: "ImageItemID")
        UserDefaults.standard.synchronize()

        let cache = SDImageCache.shared()!
        cache.diskImageExists(withKey: key) { (exists: Bool) in
            if exists {
                print("cache exists: \(key)")
                if let image = cache.imageFromMemoryCache(forKey: key) ?? cache.imageFromDiskCache(forKey: key) {
                    let square = image.square().resize(size: CGSize(width: 140, height: 140))
                    self.imageView.image = self.canvas(self.items.count).append(square)
                    self.spinner.stopAnimating()
                    print("image loaded: \(key)")
                    return
                }
            }
            self.download(key) {
                let square = $0.square().resize(size: CGSize(width: 140, height: 140))
                self.imageView.image = self.canvas(self.items.count).append(square)
                self.spinner.stopAnimating()
                print("image downloaded: \(key)")
            }
        }
    }
    
    fileprivate func download(_ key: String, completion: @escaping (_ image: UIImage)->()) {
        let manager = SDWebImageManager.shared()!
        manager.downloadImage(with: URL(string: key),
            options: SDWebImageOptions(rawValue: 0),
            progress: { (receivedSize: Int, expectedSize: Int) in
                print("progress: \(receivedSize) / \(expectedSize)")
            },
            completed: { (image, error, cacheType, finished, url) in
                let medium: UIImage
                if let image = image {
                    medium = image
                } else {
                    print("BUG: empty image: \(url)")
                    medium = UIImage(named: "Default")!  // Kludge. Just to avoid crash, but should never happen.
                }
                
                completion(medium)
            }
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: "onKeyboardWillShow:", name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: "onKeyboardWillHide:", name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if UIScreen.main.bounds.width == 320 {
            self.scrollView.isScrollEnabled = true
            self.viewsHeightConstraint.constant = 80
            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.viewsHeightConstraint.constant)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
   
    // MARK: - Notification
    
    func onKeyboardWillShow(_ notification: Notification) {
        let info = notification.userInfo!
        
        let rect = info[UIKeyboardFrameEndUserInfoKey] as! CGRect
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let opts = UIViewAnimationOptions(rawValue: curve << 16)
        
        UIView.animate(withDuration: duration, delay: 0, options: opts, animations: {
//            self.bottomConstraint.constant = rect.height
            self.view.frame.origin.y = -rect.height 

            self.view.layoutIfNeeded()
        }) { (finish: Bool) in
            self.scrollView.isScrollEnabled = true
        }
    }
    
    func onKeyboardWillHide(_ notification: Notification) {
        let info = notification.userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let opts = UIViewAnimationOptions(rawValue: curve << 16)
        
        UIView.animate(withDuration: duration, delay: 0, options: opts, animations: {
//            self.bottomConstraint.constant = 0
            self.view.frame.origin.y = 0

            self.view.layoutIfNeeded()
        }) { (finish: Bool) in
            if UIScreen.main.bounds.width > 320 {
                self.scrollView.isScrollEnabled = false
            }
        }
    }

    
    // MARK: - IBAction
    
    @IBAction func onSharePressed(_ sender: UIBarButtonItem) {
//        track("share", properties: ["action":"share" as AnyObject, "fb": NSNumber(value: fbButton.isSelected), "twitter":NSNumber(value: twitterButton.isSelected), "keepy":NSNumber(value: keepyButton.isSelected)])
        track("share", properties: ["action":"share" as AnyObject, "twitter":NSNumber(value: twitterButton.isSelected), "keepy":NSNumber(value: keepyButton.isSelected)])
        if collectionId > 0 {
            ServerComm.instance().shareCollection(collectionId, message: textView.text, facebook: false, twitter: twitterButton.isSelected, keepy: keepyButton.isSelected) {
                 (response: [AnyHashable: Any]!, error: Error!) in
                if let e = error {
                    print("error: \(e)")
                } else {
                    print("shared: \(response)")
                }
            }
        } else {
            let item = items[0]
            
            ServerComm.instance().shareItem(Int(item.serverID), andMessage: textView.text, andFacebook:  0, andTwitter: twitterButton.isSelected ? 1 : 0, andKeepyFBShare: keepyButton.isSelected, withSuccessBlock: { response in
                print("shared: \(response)")
            }, andFail: { (e: Error!) in
                print("error: \(e)")
            })
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func onCancelPressed(_ sender: UIBarButtonItem) {
        track("cancel")
        self.navigationController?.dismiss(animated: true, completion: nil)
//        self.navigationController?.dismiss(animated: true, completion: {
//            NotificationCenter.default.post(name: Notification.Name("CloseClickedOnShare"), object: nil)
//        })
        
    }
    
    @IBAction func onFbButtonPressed(_ sender: UIButton) {
        if sender.isSelected == false {
            track("fb-on")
            sender.isEnabled = false
            
            GlobalUtils.facebookLogin(["publish_actions"], viewController: self) { [weak self] (response, error) in
                if let e = error {
                    print("error: \(e)")
                } else if let r = response as? [String:AnyObject] {
                    print("ok: \(r)")
                    self?.updateFbButton(true)
                } else {
                    print("canceled")
                    self?.track("fb-cancel")
                }
                sender.isEnabled = true
            }
        } else {
            track("fb-off")
            updateFbButton(false)
        }
    }
    fileprivate func updateFbButton(_ selected: Bool) {
//        fbButton.isSelected = selected
        updateShareButton()
        Defaults.shareWithFacebook = selected
        fbLabel.textColor = selected ? UIColor(rgb: 0x3D5C95) : UIColor.lightGray
    }
    
    @IBAction func onTwitterButtonPressed(_ sender: UIButton) {
        if sender.isSelected == false {
            track("twitter-on")
            sender.isEnabled = false
            SVProgressHUD.show(withStatus: "one sec..")
            
            GlobalUtils.twitterLogin() { [weak self] (response, error) in
                if let e = error {
                    print("error: \(e)")
                    let ac = UIAlertController(title: nil, message: "Your Twitter account is not available. You can fix your account in settings.", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    ac.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (_: UIAlertAction) in
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }))
                    self?.present(ac, animated: true, completion: nil)
                } else if let r = response as? [String:AnyObject] {
                    print("ok: \(r)")
                    self?.updateTwitterButton(true)
                } else {
                    self?.track("twitter-cancel")
                    print("canceled")
                }
                sender.isEnabled = true
                SVProgressHUD.dismiss()
            }
        } else {
            track("twitter-off")
            updateTwitterButton(false)
        }
    }
    fileprivate func updateTwitterButton(_ selected: Bool) {
        twitterButton.isSelected = selected
        updateShareButton()
        Defaults.shareWithTwitter = selected
        twitterLabel.textColor = selected ? UIColor(rgb: 0x1EADEA) : UIColor.lightGray
    }
    
    @IBAction func onKeepyButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        updateShareButton()
        Defaults.shareWithKeepy = sender.isSelected
        keepyLabel.textColor = sender.isSelected ? UIColor(rgb: 0x55413e) : UIColor.lightGray
        track(sender.isSelected ? "keepy-on" : "keepy-off")
    }
    
    // MARK: - Private
    
    fileprivate func updateShareButton() {
//        shareButton.isEnabled = fbButton.isSelected || twitterButton.isSelected || keepyButton.isSelected
        shareButton.isEnabled = twitterButton.isSelected || keepyButton.isSelected

    }
    
    @IBAction func printProductButtonClicked(_ sender: UIButton) {
        PrintIOManager.showPreselectedPrintIODialog(in: self)
        let properties = ["":""]
        Mixpanel.sharedInstance()?.track("print.io-from-sharescreen", properties: properties)
    }
    
}
