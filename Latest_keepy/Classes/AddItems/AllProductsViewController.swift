//
//  AllProductsViewController.swift
//  Keepy
//
//  Created by Aakash Wadhwa on 02/05/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

import UIKit
import SDWebImage
import Mixpanel

class AllProductsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var skus = [NSMutableDictionary]()
    var product = [FinalProduct]()
    @IBOutlet weak var allProductsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sku1 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/1930939be47b58bfa6913defebb3b91b.png","sku":"PhoneCase-iphone6Plus-Gloss", "price":"11", "productId":"61", "highPrice":"25","name":"PhoneCase-iphone6Plus"]
        
        let sku2 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/68d5ef45a837a1cb3b1cbc160575f33c.png","sku":"StandardMug-FullImage-11oz", "price":"21", "productId":"41","highPrice":"28","name":"StandardMug"]
        
        let sku3 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/30fe5486d6aaddd60667aba04b39de0a.png","sku":"MetalMagnet_2x2", "price":"31", "productId":"116","highPrice":"30","name":"MetalMagnet"]
        
        let sku4 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/6c117304032cc549338a8ee324f32c6e.png","sku":"Framed_10x10_Black_Gloss", "price":"41", "productId":"116","highPrice":"35","name":"Framed Prints"]
        
        let sku5 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/1930939be47b58bfa6913defebb3b91b.png","sku":"PhoneCase-iphone6Plus-Gloss", "price":"11", "productId":"61", "highPrice":"25","name":"PhoneCase-iphone6Plus"]
        
        let sku6 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/68d5ef45a837a1cb3b1cbc160575f33c.png","sku":"StandardMug-FullImage-11oz", "price":"21", "productId":"41","highPrice":"28","name":"StandardMug"]
        
        let sku7 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/30fe5486d6aaddd60667aba04b39de0a.png","sku":"MetalMagnet_2x2", "price":"31", "productId":"116","highPrice":"30","name":"MetalMagnet"]
        
        let sku8 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/6c117304032cc549338a8ee324f32c6e.png","sku":"Framed_10x10_Black_Gloss", "price":"41", "productId":"116","highPrice":"35","name":"Framed Prints"]
        
        let sku9 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/1930939be47b58bfa6913defebb3b91b.png","sku":"PhoneCase-iphone6Plus-Gloss", "price":"11", "productId":"61", "highPrice":"25","name":"PhoneCase-iphone6Plus"]
        
        let sku10 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/68d5ef45a837a1cb3b1cbc160575f33c.png","sku":"StandardMug-FullImage-11oz", "price":"21", "productId":"41","highPrice":"28","name":"StandardMug"]
        
        let sku11 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/30fe5486d6aaddd60667aba04b39de0a.png","sku":"MetalMagnet_2x2", "price":"31", "productId":"116","highPrice":"30","name":"MetalMagnet"]
        
        let sku12 : NSMutableDictionary = ["imageURL":"https://gtnimgmanipcdn.azureedge.net/img-manip/6c117304032cc549338a8ee324f32c6e.png","sku":"Framed_10x10_Black_Gloss", "price":"41", "productId":"116","highPrice":"35","name":"Framed Prints"]
        
        self.skus = [sku2, sku1, sku3,sku4,sku5,sku6,sku7,sku8,sku9,sku10,sku11,sku12]
        
        
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.allProductsCollectionView.reloadData()
        }
    }
    @IBAction func doneButtonClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
            print("Dismissed")
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
//       return self.skus.count
        return FinalProduct.shared.finalProductArray.count
    }

   
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:
        IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:
            "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        let product = FinalProduct.shared.finalProductArray[indexPath.row]
        if let endcoded1 = product.imageURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: endcoded1)        {
            print(url)
            SDWebImageManager.shared().downloadImage(with: url, options: SDWebImageOptions.highPriority, progress: nil, completed: { (image: UIImage!, error: Error!, type: SDImageCacheType, finished: Bool, url: URL!) in
                
                if error == nil
                {
                    cell.productImageView.image = image
                } else {
                    cell.productImageView.image = UIImage.init(named: "AppIcon")
                    print("Error for showing Image:\(error.localizedDescription)")
                }
            })
            
            //            cell.productImageView.contentMode = .scaleAspectFit
        }else{
            print("Spmething is wrong")
        }
        cell.productName.text = product.productName
        cell.productName.textAlignment = .center
        cell.productName.sizeToFit()
        var standardPrice  = ""
        if let  standardPriceLocal = product.productPrice as? String {
            standardPrice = standardPriceLocal
        }
       
        if standardPrice == "" {
            standardPrice = "0"
        }
        let multiplicationFactor = product.productHighEndMultiplicationFactor
        let intStandardPrize : Int = NSString(string: standardPrice).integerValue
        let intMultiplePrize : Int = NSString(string: multiplicationFactor).integerValue
        let highestPrice = (intStandardPrize) + (((intStandardPrize * intMultiplePrize) / 100))
        
        
        let attributes = [
            NSFontAttributeName: UIFont.systemFont(ofSize: 10.0),
            NSForegroundColorAttributeName: UIColor.keepyBlueColor(),
            NSStrikethroughStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue)
        ]
        let myTitle = NSMutableAttributedString(string:"$\(highestPrice).99", attributes: attributes)
        
        let myNormalAttributedTitle1 = NSAttributedString(string: " $\(standardPrice)",
            attributes: [NSForegroundColorAttributeName : UIColor.keepyPinkColor(),NSFontAttributeName:UIFont.systemFont(ofSize:10.0 )])
        myTitle.append(myNormalAttributedTitle1)
        
        cell.productPricing.attributedText = myTitle //skuObj["price"] as? String//"$100"
        cell.productPricing.textAlignment = .center
//        let skuObj : NSMutableDictionary = self.skus[indexPath.row]
//
//        if let encoded = (skuObj["imageURL"] as? String)?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
//            let url = URL(string: encoded)
//        {
//            print(url)
//            SDWebImageManager.shared().downloadImage(with: url, options: SDWebImageOptions.highPriority, progress: nil, completed: { (image: UIImage!, error: Error!, type: SDImageCacheType, finished: Bool, url: URL!) in
//
//                if error == nil
//                {
//                    cell.productImageView.image = image
//                } else {
//                    cell.productImageView.image = UIImage.init(named: "AppIcon")
//                    print("Error for showing Image:\(error.localizedDescription)")
//                }
//            })
//
//            //            cell.productImageView.contentMode = .scaleAspectFit
//        }else{
//            print("Spmething is wrong")
//        }
//        cell.productName.text = skuObj["name"] as? String//"Mug"
//        cell.productName.textAlignment = .center
//        cell.productName.sizeToFit()
//        var standardPrice  = ""
//        if let  standardPriceLocal = skuObj["price"] as? String {
//            standardPrice = standardPriceLocal
//        }
//        let multiplicationFactor = skuObj["highPrice"] as? String
//        let intStandardPrize = Int(standardPrice)!
//        let intMultiplePrize = Int(multiplicationFactor!)!
//        let highestPrice = (intStandardPrize) + (((intStandardPrize * intMultiplePrize) / 100))
//
//
//        let attributes = [
//            NSFontAttributeName: UIFont.systemFont(ofSize: 10.0),
//            NSForegroundColorAttributeName: UIColor.black,
//            NSStrikethroughStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue)
//        ]
//        let myTitle = NSMutableAttributedString(string:"$\(highestPrice).99", attributes: attributes)
//
//        let myNormalAttributedTitle1 = NSAttributedString(string: " $\(standardPrice)",
//            attributes: [NSForegroundColorAttributeName : UIColor.red,NSFontAttributeName:UIFont.systemFont(ofSize:10.0 )])
//        myTitle.append(myNormalAttributedTitle1)
//
//        cell.productPricing.attributedText = myTitle //skuObj["price"] as? String//"$100"
//        cell.productPricing.textAlignment = .center
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(CGSize(width: (collectionView.frame.size.width / 2) - 1, height:
            (collectionView.frame.size.width / 2) - 1))
        return CGSize(width: (collectionView.frame.size.width / 2) - 1, height:
            (collectionView.frame.size.width / 2) - 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath:
        IndexPath) {
        PrintIOManager.showPreselectedPrintIODialog(in: self)
    }
}
   


