//
//  NoteEditViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 10/26/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

class NoteEditViewController: UIViewController {
    var completion: ((Void) -> Void)?
    
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: KPTextView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.becomeFirstResponder()
        textView.font = UIFont(name: "ARSMaquettePro-Light", size:15)
        textView.placeholder = NSLocalizedString("Jot down a quote, a love note, a memory, a moment or anything else you want to keep", comment: "edit note")
        
        trackingName = "editnote"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: "keyboardDidShowNotification:", name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: "keyboardWillHideNotification:", name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        textView.setNeedsLayout()
        textView.setNeedsDisplay()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIKeyboardDidShowNotification
    
    func keyboardDidShowNotification(_ notification: Notification) {
        let info  = notification.userInfo!
        let rawFrame = info[UIKeyboardFrameEndUserInfoKey] as! CGRect
        
        bottomLayoutConstraint.constant = 8 + rawFrame.height
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {
        bottomLayoutConstraint.constant = 8
    }
    
    // MARK: - UITextViewDelegate

    func textViewDidChange(_ textView: UITextView) {
        saveButton.isEnabled = textView.text.length > 0
    }
    
    // MARK: - IBAction
    
    @IBAction func onSavePressed(_ sender: UIBarButtonItem) {
        completion?()
    }
    
    @IBAction func onCancelPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}

private let kPadding: CGFloat = 20
private let kSpacing: CGFloat = 10

func CreateNoteImage(_ textView: UITextView) -> UIImage {
    let image = UIImage(named: "quote-icon")!
    
    var size = textView.contentSize
    size.width += (kPadding * 2)
    size.height += (kPadding * 2)
    size.height += image.size.height + kSpacing
    if size.height < size.width { size.height = size.width }
    
    UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
    
    let p = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: size.height), cornerRadius: 5.0)
    p.addClip()
    UIColor.white.setFill()
    p.fill()
    
    let iconRect = CGRect(x: (size.width - image.size.width) / 2, y: kPadding, width: image.size.width, height: image.size.height)
    image.draw(in: iconRect)
    
    UIColor(rgb: 0x523c37).set()
    
    let s = textView.text as NSString
    
    let y = kPadding + image.size.height + kSpacing
    let textRect = CGRect(x: kPadding, y: y, width: textView.contentSize.width, height: textView.contentSize.height)
    let attrs: [String:AnyObject] = [NSFontAttributeName : textView.font!]
    s.draw(in: textRect, withAttributes: attrs)
    
    let img = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    return img
}
