//
//  EditAssetViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/28/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Mixpanel

class EditAssetViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var enhanceButton: UIButton!
    @IBOutlet weak var cropButton: UIButton!
    @IBOutlet weak var rotateButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    // MARK: -
    
    var originalInfo: ImageEditInfo!
    var info: ImageEditInfo!
    var completion: ((Void) -> Void)?
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = NSLocalizedString("edit photo", comment: "")
        
        originalInfo = info
        updateImage()
        
        trackingName = "editphoto"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CropViewController {
            Mixpanel.sharedInstance()?.track("edit-photo", properties: ["action":"open-crop"])
            if info.rotation > 0 {
                print("Rotation: \(info.rotation)")
                print("Rotation Points: \(info.points)")

                vc.originalImage = info.editedImage
            } else {
                vc.originalImage = info.originalImage
            }
            vc.points = info.points
            vc.croppedImage = info.croppedImage
            vc.completion = {
                self.info.croppedImage = vc.croppedImage
                print("Cropped Points: \(vc.points)")

               //  self.info.points = vc.points
                self.updateImage()
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func onCancelPressed(_ sender: UIBarButtonItem) {
        track("cancel")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSavePressed(_ sender: UIBarButtonItem) {
        track("save")
        sender.isEnabled = false
        completion?()
    }
    
    @IBAction func onEnhancePressed(_ sender: UIButton) {
        info.autoEnhance = !info.autoEnhance
        if info.autoEnhance {
            info.saturation = 1.1
            info.contrast = 1.1
            info.brightness = 0.1
            enhanceButton.isSelected = true
        } else {
            info.saturation = 1.0
            info.contrast = 1.0
            info.brightness = 0
            enhanceButton.isSelected = false
        }
        updateImage()
    }
    
    @IBAction func onCropPressed(_ sender: UIButton) {
    }
    
    @IBAction func onRotatePressed(_ sender: UIButton) {
        if info.rotation > 0 { info.rotation -= 90 } else { info.rotation = 270 }
        updateImage()
    }
    
    @IBAction func onResetPressed(_ sender: UIButton) {
        info.croppedImage = nil
        info.points = []
        info.rotation = 0
        info.autoEnhance = false
        info.saturation = 1.0
        info.contrast = 1.0
        info.brightness = 0
        updateImage()
    }
    
    // MARK: - Private
    
    fileprivate func updateImage() {
        if imageView == nil { return }
        
        // Free memory first
        imageView.image = nil
        
        if var image: UIImage = info.croppedImage ?? info.originalImage {
            
            if info.rotation != 0 || info.autoEnhance {
                image = image.enhance(info.autoEnhance, rotation: Int(info.rotation))
                info.editedImage = image
            } else {
                info.editedImage = info.croppedImage
            }
            imageView.image = image
            imageView.contentMode  = .scaleAspectFit
            imageView.setNeedsLayout()
            updateResetButton()
        } else {
            
        }
    }
    
    fileprivate func updateResetButton() {
        resetButton.isEnabled = info.rotation != 0 || info.points.count > 0 || info.autoEnhance || info.croppedImage != nil
        
        if navigationController?.viewControllers.count == 1 {
            saveButton.isEnabled = originalInfo != info
        } else {
            saveButton.isEnabled = true
        }
        
        enhanceButton.isSelected = info.autoEnhance
    }
}

struct ImageEditInfo : Equatable {
    var originalImage: UIImage?
    var croppedImage: UIImage?
    var editedImage: UIImage?
    var thumbnail: UIImage?
    
    var points: [CGPoint] = []
    var rotation: Float = 0
    var autoEnhance = false
    var brightness: Float = 0.0
    var saturation: Float = 1.0
    var contrast: Float = 1.0
}

func ==(lhs: ImageEditInfo, rhs: ImageEditInfo) -> Bool {
    if lhs.rotation != rhs.rotation { return false }
    if lhs.autoEnhance != rhs.autoEnhance { return false }
    if lhs.brightness != rhs.brightness { return false }
    if lhs.saturation != rhs.saturation { return false }
    if lhs.contrast != rhs.contrast { return false }
    if lhs.points.count != rhs.points.count { return false }
    for i in 0..<lhs.points.count {
        if lhs.points[i] != rhs.points[i] { return false }
    }
    return true
}
