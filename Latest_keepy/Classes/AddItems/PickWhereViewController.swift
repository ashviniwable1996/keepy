//
//  PickWhereViewController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 9/25/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit
import Mixpanel
import SVProgressHUD
import MagicalRecord

class WherePickerViewController: UITableViewController {

    var items: [KPLocalPlace] = []
    var place: KPLocalPlace?
    
    var completion: ((Void) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITableViewCell.register(tableView: tableView)
        
        for i in KPLocalPlace.fetchAll() as! [KPLocalPlace] {
            if i.title == nil {
                NSLog("WTF: how come this cannot be removed? - \(i.serverID)")
                continue
            }
            items.append(i)
        }
        
        if let place = place {
            for (i, e) in items.enumerated() {
                if e.serverID == place.serverID {
                    selectedIndexPath = IndexPath(row: i, section: 0)
                    break
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let path = selectedIndexPath {
            place = items[path.row]
        } else {
            place = nil
        }
        completion?()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.dequeue(tableView: tableView, forIndexPath: indexPath)
        
        let item = items[indexPath.row]
        cell.textLabel!.text = item.title
        cell.accessoryType = (indexPath == selectedIndexPath) ? .checkmark : .none
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let path = selectedIndexPath {
                if indexPath.row < path.row {
                    selectedIndexPath = IndexPath(row: path.row-1, section: 0)
                } else if indexPath.row == path.row {
                    selectedIndexPath = nil
                }
            }
            let place = items[indexPath.row]
            ServerComm.instance().deletePlace(place.serverID, withSuccessBlock: nil, andFail: nil)
            
            items.remove(at: indexPath.row)
            place.remove()
            NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    var selectedIndexPath: IndexPath?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == selectedIndexPath {
            selectedIndexPath = nil
            tableView.deselectRow(at: indexPath, animated: true)
            if let cell = tableView.cellForRow(at: indexPath) {
                cell.accessoryType = .none
            }
        } else {
            selectedIndexPath = indexPath
        }
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - IBAction
    
    @IBAction func onAddPressed(_ sender: AnyObject) {
        Mixpanel.sharedInstance()?.track("add place tap", properties:["source" : "add story"])

        let ac = UIAlertController(title: nil, message: "place name", preferredStyle: .alert)
        ac.addTextField { (tf: UITextField) -> Void in
            tf.enablesReturnKeyAutomatically = true
        }
        ac.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "add", style: .default, handler: { (a: UIAlertAction) -> Void in
            if let tf = ac.textFields?.first {
                if let s = tf.text {
                    self.addPlace(s.trim())
                }
            }
        }))
        
        present(ac, animated: true, completion: nil)
    }
    
    func addPlace(_ name: String) {
        if name.length == 0 { return }
        for (idx, item) in self.items.enumerated() {
            if item.title.trim() == name {
                let path = IndexPath(row: idx, section: 0)
                self.tableView.selectRow(at: path, animated: true, scrollPosition: .top)
                self.selectedIndexPath = path
                after(0.4) {
                    self.navigationController?.popViewController(animated: true)
                }
                return
            }
        }
        
        SVProgressHUD.show()
        
        ServerComm.instance().addPlace(name, withLon: 0, andLat: 0, withSuccessBlock: { result in
            var serverId: UInt?
            
            if let dict = result as? NSDictionary {
                if let status = dict["status"] as? Int {
                    if status == 0 {
                        if let r = dict["result"] as? NSDictionary {
                            if let id = r["id"] as? UInt {
                                serverId = id
                            }
                        }
                    }
                }
            }
            if let serverId = serverId {
                let place = KPLocalPlace.create()!
                place.serverID = serverId
                place.title = name
                place.saveChanges()
                
                NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStoreAndWait()
                self.items.append(place)
                
                let path = IndexPath(row: self.items.count-1, section: 0)
                self.tableView.insertRows(at: [path], with: .right)
                self.selectedIndexPath = path
                    
                after(0.4) {
                    self.navigationController?.popViewController(animated: true)
                }
                
            } else {
                "error updating place"
            }
            SVProgressHUD.dismiss()

            }) { (error: Error!) -> Void in
                NSLog("server error: \(error)")
                SVProgressHUD.showError(withStatus: NSLocalizedString("there was a problem connecting to keepy\'s servers, please try again soon", comment: ""))
        }
    }
}

class WhenPickerViewController: UIViewController {
    var when: Date?
    
    @IBOutlet weak var datePicker: UIDatePicker!

    var completion: ((Void) -> Void)?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        datePicker.date = when ?? Date()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        when = datePicker.date
        completion?()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

enum DispatchPriority {
    case background, `default`, high, low, main
    var queue: DispatchQueue {
        let p: DispatchQueue.GlobalQueuePriority
        switch self {
        case .background: p = DispatchQueue.GlobalQueuePriority.background
        case .default: p = DispatchQueue.GlobalQueuePriority.default
        case .high: p = DispatchQueue.GlobalQueuePriority.high
        case .low: p = DispatchQueue.GlobalQueuePriority.low
        default: return DispatchQueue.main
        }
        return DispatchQueue.global(priority: p)
    }
}

func async(_ priority: DispatchPriority = .main, block: @escaping ()->()) {
    priority.queue.async(execute: block)
}

func after(_ after: Double, priority: DispatchPriority = .main, block: @escaping ()->()) {
    let t = DispatchTime.now() + Double(Int64(after * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    priority.queue.asyncAfter(deadline: t, execute: block)
}
