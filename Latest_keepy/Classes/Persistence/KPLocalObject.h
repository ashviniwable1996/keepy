//
// Created by Arik Sosman on 4/9/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KPLocalObject : NSObject

@property(readonly) NSUInteger identifier;

+ (NSString *)getDatabaseTable;

+ (instancetype)create;

- (void)reload;

- (void)remove;


+ (instancetype)fetchByLocalID:(NSUInteger)localID;

+ (NSArray *)initializeArrayFromFetchResponses:(NSArray *)resultDictionaries;

+ (instancetype)initializeFromFetchResponse:(NSDictionary *)resultDictionary;

+ (NSArray *)fetchAll;

+ (NSArray *)objectsToIDs:(NSArray *)objects;

+ (NSArray *)idsToObjects:(NSArray *)ids;

// returns NO on failure to save, YES otherwise
- (BOOL)saveChanges;

- (BOOL)saveAttribute:(NSString *)attribute;

- (BOOL)saveAttributes:(NSArray *)attributes;

@end