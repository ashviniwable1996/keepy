//
// Created by Arik Sosman on 4/9/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <objc/runtime.h>
#import "KPLocalObject.h"
#import "KPSQLiteManager.h"




@interface ArrayQuerySelector : NSObject

@property (strong, nonatomic) NSString *parenthesesSelectorString;
@property (strong, nonatomic) NSDictionary *parameterDictionary;

@end

@implementation ArrayQuerySelector

+ (NSString *)generateUniqueReplacementName {
    
    NSString *uniqueString = [[NSProcessInfo processInfo] globallyUniqueString];
    // we need to remove unsupported characters
    
    NSArray *supportedComponents = [uniqueString componentsSeparatedByCharactersInSet:[NSCharacterSet alphanumericCharacterSet].invertedSet];
    NSString *supportedString = [supportedComponents componentsJoinedByString:@""];
    
    return supportedString;
    
}

@end






@interface KPLocalObject ()

@property(readwrite) NSUInteger identifier;

/*
This is for columns whose values need to be stored in external files. That way, if the value of the column is changed
temporarily while reading or writing, it will not trigger the automatic export of the new value to a file.
 */
@property BOOL doingIntervalIO;

@property(strong, nonatomic) NSMutableSet *modifiedAttributes;

@end

@implementation KPLocalObject

+ (NSString *)getDatabaseTable {
    NSAssert(NO, @"getDatabaseTable is not implemented");
    return nil;
}

+ (void)runAbstractionCheck { // make sure the method is not called from the LocalObject class

    if (self.class == [KPLocalObject class]) {

        [NSException raise:@"Abstract Class Method" format:@"This method should not be called from the LocalObject class. It needs to be called from one of its implementations."];

    }

}

+ (instancetype)create {

    [self runAbstractionCheck];

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self getDatabaseTable];

    // let's insert something into it
    NSString *queryString = [NSString stringWithFormat:@"INSERT INTO %@ DEFAULT VALUES", dbTable];
    NSString *fetchString = [NSString stringWithFormat:@"SELECT LAST_INSERT_ROWID() FROM %@", dbTable];

    __block NSNumber *insertionID; // we use __block in order to be able to assign it inside the block

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        [db executeUpdate:queryString];

        FMResultSet *result = [db executeQuery:fetchString];
        if (result.next) {
            insertionID = result.resultDictionary[@"LAST_INSERT_ROWID()"];
        }
        [result close];

    }];

    KPLocalObject *instance = [self new];
    instance.identifier = insertionID.unsignedIntegerValue;

    [instance initializeObservation];

    return instance;

}

- (void)remove {

    [self.class runAbstractionCheck];

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE id = :identifier", dbTable];

    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:query withParameterDictionary:@{@"identifier" : @(self.identifier)}];
    }];

}

- (void)reload {

    [self.class runAbstractionCheck];

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    __block NSDictionary *resultDictionary;

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id = :identifier", dbTable];

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"identifier" : @(self.identifier)}];
        if (result.next) {
            resultDictionary = result.resultDictionary;
        }
        [result close];

    }];

    [self loadResultDictionary:resultDictionary];

}

+ (instancetype)fetchByLocalID:(NSUInteger)localID {

    [self runAbstractionCheck];

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    __block NSDictionary *resultDictionary;

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id = :identifier", dbTable];

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"identifier" : @(localID)}];
        if (result.next) {
            resultDictionary = result.resultDictionary;
        }
        [result close];

    }];

    return [self initializeFromFetchResponse:resultDictionary];

}

+ (NSArray *)fetchAll {

    [self runAbstractionCheck];

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@", dbTable];

    NSMutableArray *results = @[].mutableCopy;

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query];

        while (result.next) {

            [results addObject:result.resultDictionary];

        }

    }];

    return [self initializeArrayFromFetchResponses:results];

}

+ (NSArray *)initializeArrayFromFetchResponses:(NSArray *)resultDictionaries {

    NSMutableArray *instances = @[].mutableCopy;

    for (NSDictionary *resultDictionary in resultDictionaries) {

        KPLocalObject *instance = [self initializeFromFetchResponse:resultDictionary];
        if (instance) {
            [instances addObject:instance];
        }

    }

    return instances.copy;

}

+ (instancetype)initializeFromFetchResponse:(NSDictionary *)resultDictionary {

    if (!resultDictionary) {
        return nil;
    }

    KPLocalObject *instance = [self new];
    [instance loadResultDictionary:resultDictionary];
    [instance initializeObservation];
    return instance;

};

- (void)loadResultDictionary:(NSDictionary *)resultDictionary {

    [self.class runAbstractionCheck];

    self.doingIntervalIO = YES;

    for (NSString *key in resultDictionary) {

        id value = resultDictionary[key];

        if ([value isKindOfClass:[NSNull class]]) {continue;} // we don't want/need nulls, correct?

        if ([key isEqualToString:@"id"]) {

            [self setValue:value forKey:@"identifier"];

            continue; // we need the identifier to save stuff properly :D

        }

        SEL getter = NSSelectorFromString(key);

        if ([self respondsToSelector:getter]) {

            @try {

                if ([value isKindOfClass:[NSNull class]]) {

                    [self setNilValueForKey:key];

                    continue;

                }

                [self setValue:value forKey:key];

            } @catch (NSException *e) {

                // NSLog(@"Getter, but no setter for %@", key);

            }

        } else {

            // NSLog(@"No getter (and subsequently no setter) for %@", key);

        }

    }

    self.doingIntervalIO = NO;

}

- (void)initializeObservation {

    if (self.modifiedAttributes) {return;} // the observation has already started
    self.modifiedAttributes = [NSMutableSet set];

    NSArray *allProperties = [self.class getAllProperties];

    for (NSString *currentProperty in allProperties) {

        // let's create an observer
        [self addObserver:self forKeyPath:currentProperty options:NSKeyValueObservingOptionNew context:nil];

    }

}

- (void)stopObservation {

    self.modifiedAttributes = nil;
    NSArray *allProperties = [self.class getAllProperties];

    for (NSString *currentProperty in allProperties) {

        // let's create an observer
        @try {
            [self removeObserver:self forKeyPath:currentProperty];
        } @catch(id anException){
            //do nothing, obviously it wasn't attached because an exception was thrown
        }
    }

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {

    if (self.doingIntervalIO) {return;} // we don't care about changes while complicated stuff happens

    [self.modifiedAttributes addObject:keyPath];

}


+ (NSArray *)getAllProperties {

    NSMutableSet *allProperties = [NSMutableSet set];

    Class currentClass = [self class];
    Class ownClass = [KPLocalObject class];

    while ([currentClass isSubclassOfClass:ownClass] && currentClass != ownClass) {

        // get all the parent properties
        NSArray *currentProperties = [self getPropertiesOfClass:currentClass];
        currentClass = [currentClass superclass];

        [allProperties addObjectsFromArray:currentProperties];

    }

    return allProperties.allObjects;

}

+ (NSArray *)getPropertiesOfClass:(Class)class {

    NSMutableArray *allProperties = @[].mutableCopy;

    unsigned int count;
    objc_property_t *properties = class_copyPropertyList(class, &count);
    for (size_t i = 0; i < count; ++i) {
        NSString *key = [NSString stringWithCString:property_getName(properties[i]) encoding:NSUTF8StringEncoding];
        [allProperties addObject:key];
    }
    free(properties);

    return allProperties.copy;

};

- (BOOL)saveAttribute:(NSString *)attribute {

    return [self saveAttributes:@[attribute]];

}

- (BOOL)saveChanges {

    return [self saveAttributes:self.modifiedAttributes.allObjects];

}

- (BOOL)saveAttributes:(NSArray *)attributes {

    [self.class runAbstractionCheck];

    if (!attributes) {return YES;}

    if (attributes.count < 1) {return YES;} // there is nothing to save

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSMutableDictionary *values = @{}.mutableCopy;

    NSString *updateString = @"";

    self.doingIntervalIO = YES;

    NSArray *originalModifiedAttributes = self.modifiedAttributes.allObjects;

    for (NSString *currentAttribute in attributes) {

        [self.modifiedAttributes removeObject:currentAttribute];

        SEL getter = NSSelectorFromString(currentAttribute);

        if ([self respondsToSelector:getter]) {

            id value = [self valueForKey:currentAttribute];

            if (!value) { // this value is nil

                value = [NSNull null];

            }

            values[currentAttribute] = value;
            updateString = [NSString stringWithFormat:@"%@, %@ = :%@", updateString, currentAttribute, currentAttribute];

        } else {

            NSLog(@"No getter for %@", currentAttribute);

        }

    }

    self.doingIntervalIO = NO;

    if (values.count < 1) {return YES;} // there is nothing to save

    values[@"identifier"] = @(self.identifier);

    updateString = [updateString substringFromIndex:2]; // we don't need the ", " at the beginning

    // FMDatabase *db = [SCSQLiteManager getActiveManager].dbObject;

    NSString *queryString = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE id = :identifier", dbTable, updateString];

    __block BOOL couldUpdate;

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        couldUpdate = [db executeUpdate:queryString withParameterDictionary:values];

        if (!couldUpdate) { // the update failed

            // the modified attributes need to be restored
            [self.modifiedAttributes addObjectsFromArray:originalModifiedAttributes];

            NSString *error = [db lastErrorMessage];
            [NSException raise:@"Object Saving Error" format:@"%@", error];

        }

    }];

    return couldUpdate;

}

+ (NSArray *)objectsToIDs:(NSArray *)objects {

    [self runAbstractionCheck];

    NSMutableArray *ids = @[].mutableCopy;

    for (KPLocalObject *object in objects) {

        [ids addObject:@(object.identifier)];

    }

    return ids.copy;

}

+ (NSArray *)idsToObjects:(NSArray *)ids {

    [self runAbstractionCheck];

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];
    
    ArrayQuerySelector *querySelector = [self createSelectorForArray:ids];

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id IN (%@)", dbTable, querySelector.parenthesesSelectorString];
    
    
    NSMutableArray *results = @[].mutableCopy;

    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        
        
        FMResultSet *result = [db executeQuery:query withParameterDictionary:querySelector.parameterDictionary];
        while (result.next) {
            [results addObject:result.resultDictionary];
        }
        [result close];
        
         
    }];
    
    // return @[];
    
    return [self initializeArrayFromFetchResponses:results];
    
    
    
    NSMutableArray *objects = @[].mutableCopy;
    
    for (NSNumber *currentID in ids) {

        KPLocalObject *currentObject = [self fetchByLocalID:currentID.intValue];
        if (currentObject) {
            [objects addObject:currentObject]; // integers, to be safe
        }

    }

    return objects.copy;

}

+ (ArrayQuerySelector *)createSelectorForArray:(NSArray *)elements{
    
    ArrayQuerySelector *querySelector = [ArrayQuerySelector new];

    NSMutableDictionary *parameterDictionary = @{}.mutableCopy;
    NSString *selectorString = @"";
    
    if(elements.count > 0){
        
        for (id currentElement in elements) {
            
            NSString *currentName = [ArrayQuerySelector generateUniqueReplacementName];
            NSNumber *currentID;
            
            if([currentElement isKindOfClass:[NSNumber class]]){
                currentID = currentElement;
                
            }else if([currentElement isKindOfClass:[NSString class]]){
            
                NSString *currentIDString = currentElement;
                currentID = @(currentIDString.intValue);
                
            }else if([currentElement isKindOfClass:[KPLocalObject class]]){
                KPLocalObject *currentObject = currentElement;
                currentID = @(currentObject.identifier);
            }
            
            parameterDictionary[currentName] = currentID;
            selectorString = [NSString stringWithFormat:@"%@, :%@", selectorString, currentName];
            
        }
        
        selectorString = [selectorString substringFromIndex:2];
        
    }
    
    querySelector.parenthesesSelectorString = selectorString;
    querySelector.parameterDictionary = parameterDictionary.copy;
    
    return querySelector;
    
}

- (void)dealloc {

    [self stopObservation];

}

@end





