//
// Created by Arik Sosman on 4/14/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"


@interface KPLocalServerObject : KPLocalObject

@property NSUInteger serverID;

+ (instancetype)fetchByServerID:(NSUInteger)serverID;

@end