//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"


@interface KPLocalRelationship : KPLocalObject


+ (NSArray *)fetchRelationIDsForObjects:(NSArray *)objects withReferenceColumn:(NSString *)referenceColumnName targetColumn:(NSString *)targetColumnName;

+ (NSArray *)fetchRelationIDsForObject:(KPLocalObject *)object withReferenceColumn:(NSString *)referenceColumnName targetColumn:(NSString *)targetColumnName;

+ (void)setLinkedEntities:(NSArray *)linkedEntities forObject:(KPLocalObject *)object withReferenceColumn:(NSString *)referenceColumnName targetColumn:(NSString *)targetColumnName;

+ (NSArray *)fetchRelationshipsForObject:(KPLocalObject *)object withReferenceColumn:(NSString *)columnName;

+ (NSString *)generateUniqueReplacementName ;

@end