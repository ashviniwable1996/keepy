//
// Created by Arik Sosman on 4/7/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPSQLiteManager.h"

@interface KPSQLiteManager ()

@property(readwrite, nonatomic) FMDatabaseQueue *dbQueue;
@property(readwrite, nonatomic) NSString *dbPath;

@end

@implementation KPSQLiteManager

// this is the shared static active manager
// it is initialized in the init method, and used henceforth
static KPSQLiteManager *manager;

+ (KPSQLiteManager *)initManagerWithPath:(NSString *)dbPath {

    if (![NSThread isMainThread]) {
        [NSException raise:@"Not Main Thread" format:@"The SQLite Manager needs to be initialized from the main thread."];
    }

    // let's instantiate ourselves
    KPSQLiteManager *sharedManager = [self new];
    sharedManager.dbPath = dbPath;

    // let's make sure this path exists
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dbFolder = [dbPath stringByDeletingLastPathComponent];

    if (![fileManager fileExistsAtPath:dbFolder]) {
        [fileManager createDirectoryAtPath:dbFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }

    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    sharedManager.dbQueue = queue;

    NSLog(@"initialized data model at path: %@", dbPath);

    [sharedManager prepareSchema];

    // we want the active static instance of the manager to become the one we just initialized
    manager = sharedManager;

    // and then we return it, simply for convenience
    return manager;

}

+ (KPSQLiteManager *)getManager {

    if (!manager) {
        [NSException raise:@"SQLite Manager Not Initialized" format:@"You need to initialize the SQLiteManager with initManagerWithPath: before calling this method"];
    }

    return manager;
}

#pragma mark PREPARE SCHEMA

- (void)prepareSchema {

    [self.dbQueue inDatabase:^(FMDatabase *db) {

#pragma mark SQLite all tables

        // let's first create the actual tables with just the auto-incrementing ID column in each
        [self createTables:db];

        // adding the data columns
        [self createDataColumns:db];

        // let's prepare the relationships
        [self createRelationships:db];

    }];

}

- (void)createTables:(FMDatabase *)db {
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS albums (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS appdel (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS comments (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS fans (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS kids (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS notifications (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS places (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS stories (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS tags (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS assets (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
}

- (void)createDataColumns:(FMDatabase *)db {
// albums table
    [db executeUpdate:@"ALTER TABLE albums ADD COLUMN year INTEGER "];

    // assets table
    [db executeUpdate:@"ALTER TABLE assets ADD COLUMN assetHash TEXT "];
    [db executeUpdate:@"ALTER TABLE assets ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE assets ADD COLUMN assetStatus INTEGER "];
    [db executeUpdate:@"ALTER TABLE assets ADD COLUMN assetType INTEGER "];

    // comments table
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN commentDate INTEGER "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN commentLength INTEGER "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN commentType INTEGER "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN creatorFanID INTEGER "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN creatorName TEXT "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN creatorRelationType INTEGER "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN creatorUserID INTEGER "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN extraData TEXT "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN isStory INTEGER (1) "];
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN wasRead INTEGER (1) "];

    // fans table
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN birthdate INTEGER "];
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN didApprove INTEGER (1) "];
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN email TEXT "];
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN facebookID TEXT "];
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN nickname TEXT "];
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN relationType INTEGER "];
    [db executeUpdate:@"ALTER TABLE fans ADD COLUMN userID INTEGER "];

    // items table
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN autoEnhanceOn INTEGER (1) "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN brightness REAL "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN contrast REAL "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN createDate INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN cropRect TEXT "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN extraData TEXT "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN filter INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN filterID INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN itemDate INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN itemType INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN kidsIDs TEXT "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN readAction INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN rotation REAL "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN saturation REAL "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN shareStatus INTEGER "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN title TEXT "];
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN updateDate INTEGER "];

    // kids table
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN birthdate INTEGER "];
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN gender INTEGER "];
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN isMine INTEGER (1) "];
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN name TEXT "];
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN userNickname TEXT "];
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN userRelationType INTEGER "];

    // notifications table
    [db executeUpdate:@"ALTER TABLE notifications ADD COLUMN items TEXT "];
    [db executeUpdate:@"ALTER TABLE notifications ADD COLUMN notificationDate INTEGER "];
    [db executeUpdate:@"ALTER TABLE notifications ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE notifications ADD COLUMN notificationType INTEGER "];
    [db executeUpdate:@"ALTER TABLE notifications ADD COLUMN senderID INTEGER "];
    [db executeUpdate:@"ALTER TABLE notifications ADD COLUMN status INTEGER "];
    [db executeUpdate:@"ALTER TABLE notifications ADD COLUMN subject TEXT "];

    // places table
    [db executeUpdate:@"ALTER TABLE places ADD COLUMN latitude REAL "];
    [db executeUpdate:@"ALTER TABLE places ADD COLUMN longitude REAL "];
    [db executeUpdate:@"ALTER TABLE places ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE places ADD COLUMN title TEXT "];

    // stories table
    [db executeUpdate:@"ALTER TABLE stories ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE stories ADD COLUMN storyLength INTEGER "];

    // tags table
    [db executeUpdate:@"ALTER TABLE tags ADD COLUMN name TEXT "];
    [db executeUpdate:@"ALTER TABLE tags ADD COLUMN serverID INTEGER "];

    // users table
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN authKey TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN birthdate INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN country TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN createDate INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN email TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN expirationDate INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN facebookID TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN isMe INTEGER (1) "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN keepiesUsedThisMonth INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN keepyMonthlySpace INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN keepySpace INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN name TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN otherTxt TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN parentType INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN planType INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN registrationSignature TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN storageLimit INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN totalUsedBytes INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN twitterHandle TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN twitterID TEXT "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN serverID INTEGER "];
    [db executeUpdate:@"ALTER TABLE users ADD COLUMN zazzleURL TEXT "];
}

- (void)createRelationships:(FMDatabase *)db {

    // Album
    // many albums <-> many items (album_items)
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS album_items (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"ALTER TABLE album_items ADD COLUMN albumID INTEGER   REFERENCES albums(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];
    [db executeUpdate:@"ALTER TABLE album_items ADD COLUMN itemID INTEGER   REFERENCES items(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // many albums -> one kid (albums: kidID)
    [db executeUpdate:@"ALTER TABLE albums ADD COLUMN kidID INTEGER   REFERENCES kids(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // Comment
    // many comments -> one item (comments: itemID)
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN itemID INTEGER   REFERENCES items(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // one comment -> one asset (preview image)
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN previewImageAssetID INTEGER   REFERENCES assets(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // many comments -> one user (comments: userID)
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN userID INTEGER   REFERENCES users(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // one comment -> one asset (video)
    [db executeUpdate:@"ALTER TABLE comments ADD COLUMN videoAssetID INTEGER   REFERENCES assets(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // Fan
    // many fans <-> many kids (kid_fans)
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS kid_fans (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"ALTER TABLE kid_fans ADD COLUMN kidID INTEGER   REFERENCES kids(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];
    [db executeUpdate:@"ALTER TABLE kid_fans ADD COLUMN fanID INTEGER   REFERENCES fans(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // Item
    // one item -> one asset (image)
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN imageAssetID INTEGER   REFERENCES assets(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // many items <-> many kids (kid_items)
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS kid_items (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"ALTER TABLE kid_items ADD COLUMN kidID INTEGER   REFERENCES kids(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];
    [db executeUpdate:@"ALTER TABLE kid_items ADD COLUMN itemID INTEGER   REFERENCES items(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // one item -> one asset (original image)
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN originalImageAssetID INTEGER   REFERENCES assets(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // many items -> one place (items: placeID)
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN placeID INTEGER   REFERENCES places(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // one item -> one story
    [db executeUpdate:@"ALTER TABLE items ADD COLUMN storyID INTEGER   REFERENCES stories(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // many items <-> many tags (item_tags)
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS item_tags (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"ALTER TABLE item_tags ADD COLUMN itemID INTEGER   REFERENCES items(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];
    [db executeUpdate:@"ALTER TABLE item_tags ADD COLUMN tagID INTEGER   REFERENCES tags(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    // Kid
    // one kid -> one asset (image)
    [db executeUpdate:@"ALTER TABLE kids ADD COLUMN imageAssetID INTEGER   REFERENCES assets(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];

    /* we do not need to store the user kids relations because there is only one user and all kids are either mine or not
    // many kids <-> many users (user_kids)
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS user_kids (id INTEGER PRIMARY KEY AUTOINCREMENT)"];
    [db executeUpdate:@"ALTER TABLE user_kids ADD COLUMN userID INTEGER "];
    [db executeUpdate:@"ALTER TABLE user_kids ADD COLUMN kidID INTEGER "];
    */

    // Story
    // one story -> one asset
    [db executeUpdate:@"ALTER TABLE stories ADD COLUMN assetID INTEGER   REFERENCES assets(id) ON UPDATE CASCADE ON DELETE SET DEFAULT "];
}


@end
