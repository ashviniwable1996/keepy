//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"


@interface KPLocalUser : KPLocalServerObject

@property(strong, nonatomic) NSString *authKey;
@property NSTimeInterval birthdate;
@property(strong, nonatomic) NSString *country;
@property NSTimeInterval createDate;
@property(strong, nonatomic) NSString *email;
@property NSTimeInterval expirationDate;
@property(strong, nonatomic) NSString *facebookID;
@property BOOL isMe;
@property NSInteger keepiesUsedThisMonth;
@property NSInteger keepyMonthlySpace;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *otherTxt;
@property NSInteger parentType;
@property NSInteger planType;
@property(strong, nonatomic) NSString *registrationSignature;
@property NSInteger storageLimit;
@property NSInteger totalUsedBytes;
@property(strong, nonatomic) NSString *twitterHandle;
@property(strong, nonatomic) NSString *twitterID;
@property(strong, nonatomic) NSString *zazzleURL;

@end