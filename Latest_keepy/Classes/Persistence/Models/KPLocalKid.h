//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"


@class KPLocalServerObject;

@interface KPLocalKid : KPLocalServerObject

@property NSTimeInterval birthdate;
@property NSInteger gender;
@property BOOL isMine;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *userNickname;
@property NSInteger userRelationType;
@property NSInteger imageAssetID;

+ (NSUInteger)getKeepyKidID;

+ (BOOL)hasKids;

+ (BOOL)hasOwnKids;

+ (NSArray *)fetchMine;
+ (NSArray <KPLocalKid*>*)KidsOrderByBirthday;

@end