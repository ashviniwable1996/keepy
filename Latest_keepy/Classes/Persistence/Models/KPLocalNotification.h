//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"


@interface KPLocalNotification : KPLocalServerObject

@property(strong, nonatomic) NSString *items;
@property NSInteger notificationDate;
@property NSInteger notificationType;
@property NSInteger senderID;
@property NSInteger status;
@property(strong, nonatomic) NSString *subject;

@end