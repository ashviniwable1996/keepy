//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalAsset.h"


@implementation KPLocalAsset {

}
+ (NSString *)getDatabaseTable {
    return @"assets";
}

- (NSString *)urlHash {
    NSString *result = self.assetHash;
    if (result == nil || result.length == 0) {
        result = @(self.serverID).stringValue;
    }
    return result;
}

@end