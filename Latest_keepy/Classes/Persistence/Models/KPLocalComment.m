//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalComment.h"
#import "KPLocalItem.h"
#import "KPSQLiteManager.h"


@implementation KPLocalComment {

}
+ (NSString *)getDatabaseTable {
    return @"comments";
}

+ (NSArray *)fetchCommentsForItem:(KPLocalItem *)item {

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE itemID = :itemID ORDER BY commentDate ASC", dbTable];

    NSMutableArray *results = @[].mutableCopy;

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"itemID" : @(item.identifier)}];

        while (result.next) {

            [results addObject:result.resultDictionary];

        }

    }];

    return [self initializeArrayFromFetchResponses:results];

}

@end