//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//


#import "KPLocalKid.h"
#import "KPSQLiteManager.h"
//#import "KidsMenuViewController.h"
#import "KPLocalObject.h"


@implementation KPLocalKid {

}

+ (NSUInteger)getKeepyKidID {
    return 88;
}

+ (BOOL)hasKids {

    NSUInteger overallKidCount = [self fetchAll].count;

    NSUInteger minimumCount = 1;
//    if ([KidsMenuViewController isKeepyFeedActive]) { //was hard coded to no anyways
 //       minimumCount = 2;
//    }

    return overallKidCount >= minimumCount;

}

+ (BOOL)hasOwnKids {

    NSUInteger overallKidCount = [self fetchMine].count;
    NSUInteger minimumCount = 1;
    return overallKidCount >= minimumCount;

}


+ (NSString *)getDatabaseTable {
    return @"kids";
}

+ (NSArray *)fetchMine {

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE isMine = :isMine ORDER BY name ASC", dbTable];

    NSMutableArray *results = @[].mutableCopy;

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{
                @"isMine" : @YES,

        }];

        while (result.next) {

            [results addObject:result.resultDictionary];

        }

    }];

    return [self initializeArrayFromFetchResponses:results];

}

+ (NSArray <KPLocalKid*>*)KidsOrderByBirthday {
    
    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE isMine = :isMine ORDER BY birthdate DESC", dbTable];
    
    NSMutableArray *results = @[].mutableCopy;
    
    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{ @"isMine" : @YES }];
        while (result.next) {
            [results addObject:result.resultDictionary];
        }
    }];
    
    return [self initializeArrayFromFetchResponses:results];
}

@end