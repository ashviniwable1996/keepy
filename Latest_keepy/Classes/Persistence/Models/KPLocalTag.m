//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalTag.h"
#import "KPSQLiteManager.h"
#import "KPLocalItem.h"

@implementation KPLocalTag {

}
+ (NSString *)getDatabaseTable {
    return @"tags";
}

+ (NSArray *)fetchUsed{
    
    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id IN (SELECT tagID FROM item_tags)", dbTable];
    
    NSMutableArray *results = @[].mutableCopy;
    
    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *result = [db executeQuery:query];
        
        while (result.next) {
            
            [results addObject:result.resultDictionary];
            
        }
        
    }];
    
    return [self initializeArrayFromFetchResponses:results];
    
}

+ (NSArray *)fetchForKid:(KPLocalKid *)kid{
    
    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [KPLocalItem getDatabaseTable];
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ INNER JOIN item_tags ON items.id = item_tags.itemID WHERE items.id IN (SELECT itemID FROM kid_items WHERE kidID = :kidID)", dbTable];
    
    NSMutableArray *resultIDs = @[].mutableCopy;
    
    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"kidID": @(kid.identifier)}];
        
        while (result.next) {
            
            [resultIDs addObject:result.resultDictionary[@"tagID"]];
            
        }
        
    }];
    
    return [self idsToObjects:resultIDs];
    
    // return [self initializeArrayFromFetchResponses:results];
    
    
    
}

@end