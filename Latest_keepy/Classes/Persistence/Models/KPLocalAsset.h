//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"


NS_ASSUME_NONNULL_BEGIN

@interface KPLocalAsset : KPLocalServerObject

@property(strong, nonatomic) NSString *assetHash;
@property NSInteger assetStatus;
@property NSInteger assetType;

- (NSString *)urlHash;

@end

NS_ASSUME_NONNULL_END
