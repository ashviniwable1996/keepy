//
// Created by Arik Sosman on 4/9/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"


@interface KPLocalAlbum : KPLocalObject

@property int year;

@end