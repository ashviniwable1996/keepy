//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"


@interface KPLocalFan : KPLocalServerObject

@property NSTimeInterval birthdate;
@property BOOL didApprove;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSString *facebookID;
@property(strong, nonatomic) NSString *nickname;
@property NSInteger relationType;
@property NSInteger userID;

@end