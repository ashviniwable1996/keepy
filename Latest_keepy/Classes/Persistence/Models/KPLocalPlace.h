//
// Created by Arik Sosman on 4/9/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"


@interface KPLocalPlace : KPLocalServerObject

@property double latitude;
@property double longitude;
@property(strong, nonatomic) NSString *title;

@end