//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"

@class KPLocalItem;


@interface KPLocalComment : KPLocalServerObject

@property (assign, nonatomic) NSTimeInterval commentDate;
@property (assign, nonatomic) NSInteger commentLength;
@property (assign, nonatomic) NSInteger commentType;
@property (assign, nonatomic) NSInteger creatorFanID;
@property (strong, nonatomic) NSString *creatorName;
@property (assign, nonatomic) NSInteger creatorRelationType;
@property (assign, nonatomic) NSInteger creatorUserID;
@property (strong, nonatomic) NSString *extraData;
@property (assign, nonatomic) BOOL isStory;
@property (assign, nonatomic) BOOL wasRead;
@property (assign, nonatomic) NSInteger itemID;
@property (assign, nonatomic) NSInteger previewImageAssetID;
@property (assign, nonatomic) NSInteger userID;
@property (assign, nonatomic) NSInteger videoAssetID;

+ (NSArray *)fetchCommentsForItem:(KPLocalItem *)item;
@end