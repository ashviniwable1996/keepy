//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"

@class KPLocalComment;


@interface KPLocalItem : KPLocalServerObject

@property (assign, nonatomic) BOOL autoEnhanceOn;
@property (assign, nonatomic) double brightness;
@property (assign, nonatomic) double contrast;
@property (assign, nonatomic) NSTimeInterval createDate;
@property (strong, nonatomic) NSString *cropRect;
@property (strong, nonatomic) NSString *extraData;
@property (assign, nonatomic) NSInteger filter;
@property (assign, nonatomic) NSInteger filterID;
@property (assign, nonatomic) NSTimeInterval itemDate;
@property (assign, nonatomic) NSInteger itemType;
@property (strong, nonatomic) NSString *kidsIDs;
@property (assign, nonatomic) NSInteger readAction;
@property (assign, nonatomic) double rotation;
@property (assign, nonatomic) double saturation;
@property (assign, nonatomic) NSInteger shareStatus;
@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) NSTimeInterval updateDate;

// relationships
@property (assign, nonatomic) NSInteger imageAssetID;
@property (assign, nonatomic) NSInteger originalImageAssetID;
@property (assign, nonatomic) NSInteger placeID;
@property (assign, nonatomic) NSInteger storyID;

- (KPLocalComment *)fetchVideoStory;
- (NSInteger)fetchCommentIndexByID:(NSInteger)commentId;

+ (instancetype)createItem:(NSUInteger)itemId title:(NSString*)title itemDate:(NSDate*)itemDate assetId:(NSUInteger)assetId assetHash:(NSString*)assetHash itemType:(NSUInteger)itemType originalAssetId:(NSUInteger)originalAssetId originalAssetHash:(NSString*)originalAssetHash cropRect:(NSString*)cropRect shareStatus:(BOOL)shareStatus rotation:(float)rotation saturation:(float)saturation contrast:(float)contrast brightness:(float)brightness autoEnhanceOn:(BOOL)autoEnhanceOn extraData:(NSString*)extraData kids:(NSArray<KPLocalKid*>*)kids;

@end
