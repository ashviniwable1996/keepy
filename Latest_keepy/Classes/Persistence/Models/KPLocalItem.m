//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalItem.h"
#import "KPLocalComment.h"
#import "KPDataParser.h"
#import "KPLocalAsset.h"
#import "KPLocalKidItem.h"

@implementation KPLocalItem {

}
+ (NSString *)getDatabaseTable {
    return @"items";
}

- (NSInteger)fetchCommentIndexByID:(NSInteger)commentId {
    
    NSArray *arr = [KPLocalComment fetchCommentsForItem:self];
    // NSArray *arr = [self getCommentsOrderedByDate];
    
    for (int i = 0; i < [arr count]; i++) {
        KPLocalComment *comment = arr[i];
        if (comment.serverID == commentId) {
            return i;
        }
    }
    
    return -1;
}

- (KPLocalComment *)fetchVideoStory {

    NSArray *relatedComments = [KPLocalComment fetchCommentsForItem:self];

    for (KPLocalComment *comment in relatedComments) {
        if (comment.isStory) {
            return comment;
        }
    }

    return nil;

}

+ (instancetype)createItem:(NSUInteger)itemId
                     title:(NSString*)title
                  itemDate:(NSDate*)itemDate
                   assetId:(NSUInteger)assetId
                 assetHash:(NSString*)assetHash
                  itemType:(NSUInteger)itemType
           originalAssetId:(NSUInteger)originalAssetId
         originalAssetHash:(NSString*)originalAssetHash
                  cropRect:(NSString*)cropRect
               shareStatus:(BOOL)shareStatus
                  rotation:(float)rotation
                saturation:(float)saturation
                  contrast:(float)contrast
                brightness:(float)brightness
             autoEnhanceOn:(BOOL)autoEnhanceOn
                 extraData:(NSString*)extraData
                      kids:(NSArray<KPLocalKid*>*)kids {
    
    KPLocalItem *item = [KPLocalItem create];
    item.serverID = itemId;
    
    if(item.serverID == 942991){ // item causing reload in notification center
        NSLog(@"here");
    }
    
    item.title = title;

    item.itemDate = itemDate.timeIntervalSince1970;
    
    KPLocalAsset *itemImage = [[KPDataParser instance] createAssetById:assetId withType:0 withHash:assetHash];
    NSLog(@"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ assetHash: %@, with assetId: %lu", assetHash, (unsigned long)itemImage.identifier);
    item.imageAssetID = itemImage.identifier;
    
    item.itemType = itemType;
    
    KPLocalAsset *itemOriginalImage;
    if (itemType == 4) {
        itemOriginalImage = [[KPDataParser instance] createAssetById:originalAssetId withType:4 withHash:originalAssetHash];
    } else {
        itemOriginalImage = [[KPDataParser instance] createAssetById:originalAssetId withType:0 withHash:originalAssetHash];
    }
    
    item.originalImageAssetID = itemOriginalImage.identifier;
    
    item.createDate = [NSDate date].timeIntervalSince1970;
    item.updateDate = [NSDate date].timeIntervalSince1970;
    item.cropRect = cropRect;
    item.shareStatus = shareStatus ? 1 : 0;
    
    item.rotation = rotation;
    item.saturation = saturation;
    item.contrast = contrast;
    item.contrast = brightness;
    
    item.autoEnhanceOn = autoEnhanceOn;
    
    item.extraData = extraData;
    
    [KPLocalKidItem setLinkedKids:kids forItem:item];
    
    return item;
}

@end