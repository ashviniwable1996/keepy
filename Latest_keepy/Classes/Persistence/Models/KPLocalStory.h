//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"


@interface KPLocalStory : KPLocalServerObject

@property NSInteger storyLength;
@property NSInteger assetID;

@end