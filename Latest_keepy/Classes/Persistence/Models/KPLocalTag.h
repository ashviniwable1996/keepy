//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalObject.h"
#import "KPLocalServerObject.h"
#import "KPLocalKid.h"


@interface KPLocalTag : KPLocalServerObject

@property(strong, nonatomic) NSString *name;

+ (NSArray *)fetchUsed;
+ (NSArray *)fetchForKid:(KPLocalKid *)kid;

@end