//
// Created by Arik Sosman on 4/14/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalServerObject.h"
#import "KPSQLiteManager.h"


@implementation KPLocalServerObject {

}

+ (instancetype)fetchByServerID:(NSUInteger)serverID {

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    __block NSDictionary *resultDictionary;

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE serverID = :identifier", dbTable];
    NSLog(@"Query:%@",query);
    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"identifier" : @(serverID)}];
        if (result.next) {
            resultDictionary = result.resultDictionary;
        }
        [result close];

    }];

    return [self initializeFromFetchResponse:resultDictionary];

}

@end
