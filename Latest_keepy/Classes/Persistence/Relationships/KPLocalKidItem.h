//
// Created by Arik Sosman on 4/14/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalRelationship.h"



@class KPLocalKid;
@class KPLocalItem;
@class KPLocalTag;


@interface KPLocalKidItem : KPLocalRelationship

@property NSUInteger kidID;
@property NSUInteger itemID;

+ (NSArray *)fetchItemIDsForKid:(KPLocalKid *)kid withTag:(KPLocalTag *)tag;

+ (NSArray *)fetchItemIDsForKid:(KPLocalKid *)kid;

+ (NSArray *)fetchItemIDsForKids:(NSArray *)kids;
+ (NSArray *)fetchItemIDsForKids:(NSArray *)kids withTag:(KPLocalTag *)tag;

+ (NSArray *)fetchKidIDsForItem:(KPLocalItem *)item;

+ (void)setLinkedKids:(NSArray *)kids forItem:(KPLocalItem *)item;
@end