//
// Created by Arik Sosman on 4/17/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KPLocalRelationship.h"

@class KPLocalTag;
@class KPLocalItem;

@interface KPLocalItemTag : KPLocalRelationship

@property NSUInteger itemID;
@property NSUInteger tagID;

+ (NSArray *)fetchItemIDsForTag:(KPLocalTag *)tag;

+ (NSArray *)fetchTagIDsForItem:(KPLocalItem *)item;

+ (void)setLinkedTags:(NSArray *)tags forItem:(KPLocalItem *)item;
@end