//
// Created by Arik Sosman on 4/14/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalKidItem.h"
#import "KPLocalKid.h"
#import "KPLocalItem.h"
#import "KPLocalTag.h"

#import "KPSQLiteManager.h"

@implementation KPLocalKidItem {

}

+ (NSString *)getDatabaseTable {
    return @"kid_items";
}

+ (NSArray *)fetchItemIDsForKid:(KPLocalKid *)kid withTag:(KPLocalTag *)tag{
    
    NSString *referenceColumnName = @"kidID";
    NSString *targetColumnName = @"itemID";
    
    if(!tag){
        return [self fetchRelationIDsForObject:kid withReferenceColumn:referenceColumnName targetColumn:targetColumnName];
    }
    
    
    
    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];
    
    NSMutableArray *resultIDs = @[].mutableCopy;
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = :identifier AND itemID IN (SELECT itemID FROM item_tags WHERE tagID = :tagID)", dbTable, referenceColumnName];
    
    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"identifier" : @(kid.identifier), @"tagID": @(tag.identifier)}];
        while (result.next) {
            [resultIDs addObject:result.resultDictionary[targetColumnName]];
        }
        [result close];
        
    }];
    
    return resultIDs;
    
    return nil;
    
}

+ (NSArray *)fetchItemIDsForKid:(KPLocalKid *)kid {

    return [self fetchRelationIDsForObject:kid withReferenceColumn:@"kidID" targetColumn:@"itemID"];

}

+ (NSArray *)fetchItemIDsForKids:(NSArray *)kids withTag:(KPLocalTag *)tag{
    
    NSString *referenceColumnName = @"kidID";
    NSString *targetColumnName = @"itemID";
    
    if(!tag){
        return [self fetchRelationIDsForObjects:kids withReferenceColumn:@"kidID" targetColumn:@"itemID"];
    }
    
    
    
    
    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];
    
    NSMutableArray *resultIDs = @[].mutableCopy;
    
    if (kids.count == 0) {
        return resultIDs; // these cannot possibly be associated with anything
    }
    
    
    NSString *identifierVariableNames = @"";
    NSMutableDictionary *identifierDictionary = @{}.mutableCopy;
    
    for (KPLocalObject *currentObject in kids) {
        
        NSString *currentName = [KPLocalRelationship generateUniqueReplacementName];
        identifierVariableNames = [NSString stringWithFormat:@"%@, :%@", identifierVariableNames, currentName];
        identifierDictionary[currentName] = @(currentObject.identifier);
        
    }
    
    identifierDictionary[@"tagID"] = @(tag.identifier);
    
    identifierVariableNames = [identifierVariableNames substringFromIndex:2];
    
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ IN (%@) AND itemID IN (SELECT itemID FROM item_tags WHERE tagID = :tagID)", dbTable, referenceColumnName, identifierVariableNames];
    
    
    [manager.dbQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *result = [db executeQuery:query withParameterDictionary:identifierDictionary];
        while (result.next) {
            [resultIDs addObject:result.resultDictionary[targetColumnName]];
        }
        [result close];
        
    }];
    
    return resultIDs;
    
}

+ (NSArray *)fetchItemIDsForKids:(NSArray *)kids {

    return [self fetchRelationIDsForObjects:kids withReferenceColumn:@"kidID" targetColumn:@"itemID"];

}

+ (NSArray *)fetchKidIDsForItem:(KPLocalItem *)item {

    return [self fetchRelationIDsForObject:item withReferenceColumn:@"itemID" targetColumn:@"kidID"];

}

+ (void)setLinkedKids:(NSArray *)kids forItem:(KPLocalItem *)item {

    [self setLinkedEntities:kids forObject:item withReferenceColumn:@"itemID" targetColumn:@"kidID"];

}

@end