//
// Created by Arik Sosman on 4/17/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalItemTag.h"
#import "KPLocalItem.h"
#import "KPLocalTag.h"


@implementation KPLocalItemTag {

}

+ (NSString *)getDatabaseTable {
    return @"item_tags";
}

+ (NSArray *)fetchItemIDsForTag:(KPLocalTag *)tag {

    return [self fetchRelationIDsForObject:tag withReferenceColumn:@"tagID" targetColumn:@"itemID"];

}

+ (NSArray *)fetchTagIDsForItem:(KPLocalItem *)item {

    return [self fetchRelationIDsForObject:item withReferenceColumn:@"itemID" targetColumn:@"tagID"];

}

+ (void)setLinkedTags:(NSArray *)tags forItem:(KPLocalItem *)item {
//zvika526
    [self setLinkedEntities:tags forObject:item withReferenceColumn:@"itemID" targetColumn:@"tagID"];

}

@end