//
// Created by Arik Sosman on 4/10/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import "KPLocalRelationship.h"
#import "KPSQLiteManager.h"


@implementation KPLocalRelationship {

}

+ (NSArray *)fetchRelationIDsForObjects:(NSArray *)objects withReferenceColumn:(NSString *)referenceColumnName targetColumn:(NSString *)targetColumnName {

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSMutableArray *resultIDs = @[].mutableCopy;

    if (objects.count == 0) {
        return resultIDs; // these cannot possibly be associated with anything
    }


    NSString *identifierVariableNames = @"";
    NSMutableDictionary *identifierDictionary = @{}.mutableCopy;

    for (KPLocalObject *currentObject in objects) {

        NSString *currentName = [self generateUniqueReplacementName];
        identifierVariableNames = [NSString stringWithFormat:@"%@, :%@", identifierVariableNames, currentName];
        identifierDictionary[currentName] = @(currentObject.identifier);

    }

    identifierVariableNames = [identifierVariableNames substringFromIndex:2];


    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ IN (%@)", dbTable, referenceColumnName, identifierVariableNames];


    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:identifierDictionary];
        while (result.next) {
            [resultIDs addObject:result.resultDictionary[targetColumnName]];
        }
        [result close];

    }];

    return resultIDs;

}

+ (NSArray *)fetchRelationIDsForObject:(KPLocalObject *)object withReferenceColumn:(NSString *)referenceColumnName targetColumn:(NSString *)targetColumnName {

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSMutableArray *resultIDs = @[].mutableCopy;

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = :identifier ", dbTable, referenceColumnName];
    NSLog(@"Query:%@",query);
    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"identifier" : @(object.identifier)}];
        while (result.next) {
            [resultIDs addObject:result.resultDictionary[targetColumnName]];
        }
        [result close];

    }];

    return resultIDs;

}

+ (void)setLinkedEntities:(NSArray *)linkedEntities forObject:(KPLocalObject *)object withReferenceColumn:(NSString *)referenceColumnName targetColumn:(NSString *)targetColumnName {

    NSArray *currentlyLinkedEntityIDs = [self fetchRelationIDsForObject:object withReferenceColumn:referenceColumnName targetColumn:targetColumnName];
    NSSet *currentlyLinkedEntityIDSet = [NSSet setWithArray:currentlyLinkedEntityIDs];

    NSMutableSet *newLinkedEntityIDSet = [NSMutableSet set];
    for (KPLocalObject *currentLinkedEntity in linkedEntities) {
        [newLinkedEntityIDSet addObject:@(currentLinkedEntity.identifier)];
    }


    // these things below are the changes

    NSMutableSet *addedEntityIDSet = newLinkedEntityIDSet.mutableCopy;
    [addedEntityIDSet minusSet:currentlyLinkedEntityIDSet];

    NSMutableSet *removedEntityIDSet = currentlyLinkedEntityIDSet.mutableCopy;
    [removedEntityIDSet minusSet:newLinkedEntityIDSet];


    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    if (removedEntityIDSet.count > 0) {

        NSString *removalVariableNames = @"";
        NSMutableDictionary *removalDictionary = @{}.mutableCopy;

        for (NSNumber *currentRemovedID in removedEntityIDSet) {

            NSString *currentName = [self generateUniqueReplacementName];
            removalVariableNames = [NSString stringWithFormat:@"%@, :%@", removalVariableNames, currentName];
            removalDictionary[currentName] = currentRemovedID;

        }

        // do substrings
        removalVariableNames = [removalVariableNames substringFromIndex:2];

        // remove the entities that are to be removed
        NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ IN (%@) AND %@ = :anchorID", dbTable, targetColumnName, removalVariableNames, referenceColumnName];

        [manager.dbQueue inDatabase:^(FMDatabase *db) {

            removalDictionary[@"anchorID"] = @(object.identifier);

            BOOL result = [db executeUpdate:query withParameterDictionary:removalDictionary];
            NSLog(@"result = %d", result);

        }];

    }


    if (addedEntityIDSet.count > 0) {

        NSString *additionQuery = [NSString stringWithFormat:@"UPDATE %@ SET %@ = :targetID, %@ = :anchorID WHERE id = :linkerID", dbTable, targetColumnName, referenceColumnName];
        
        for (NSNumber *currentAdditionID in addedEntityIDSet) {
            
            KPLocalObject *linker = [[self class] create];

            [manager.dbQueue inDatabase:^(FMDatabase *db) {

                BOOL someResult = [db executeUpdate:additionQuery withParameterDictionary:@{@"targetID" : currentAdditionID, @"anchorID" : @(object.identifier), @"linkerID": @(linker.identifier)}];
                NSLog(@"did it work? %d", someResult);

            }];

        }

    }

}

+ (NSString *)generateUniqueReplacementName {

    NSString *uniqueString = [[NSProcessInfo processInfo] globallyUniqueString];
    // we need to remove unsupported characters

    NSArray *supportedComponents = [uniqueString componentsSeparatedByCharactersInSet:[NSCharacterSet alphanumericCharacterSet].invertedSet];
    NSString *supportedString = [supportedComponents componentsJoinedByString:@""];

    return supportedString;

}

+ (NSArray *)fetchRelationshipsForObject:(KPLocalObject *)object withReferenceColumn:(NSString *)columnName {

    KPSQLiteManager *manager = [KPSQLiteManager getManager];
    NSString *dbTable = [self.class getDatabaseTable];

    NSMutableArray *resultDictionaries = @[].mutableCopy;

    NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = :identifier", dbTable, columnName];

    [manager.dbQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *result = [db executeQuery:query withParameterDictionary:@{@"identifier" : @(object.identifier)}];
        while (result.next) {
            [resultDictionaries addObject:result.resultDictionary];
        }
        [result close];

    }];

    return [self initializeArrayFromFetchResponses:resultDictionaries];

}

@end
