//
// Created by Arik Sosman on 4/7/15.
// Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"


@interface KPSQLiteManager : NSObject

@property(strong, nonatomic, readonly) FMDatabaseQueue *dbQueue;
@property(strong, nonatomic, readonly) NSString *dbPath;

+ (KPSQLiteManager *)initManagerWithPath:(NSString *)dbPath;

+ (KPSQLiteManager *)getManager;

@end