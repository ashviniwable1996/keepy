//
//  PrintIOPhotoSourceKeepy.m
//  Keepy
//
//  Created by Arik Sosman on 6/22/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "PrintIOPhotoSourceKeepy.h"
#import "PrintIOSourceItemKid.h"
#import "PrintIOSourceItemMoveUp.h"
#import "PrintIOSourceItemKeepy.h"

#import "KPLocalKid.h"
#import "KPLocalItem.h"
#import "KPLocalKidItem.h"

@implementation PrintIOPhotoSourceKeepy

- (NSString *)title{
    // return @"";
    return @"Keepy";
}


- (NSString *)buttonIconPath {
    // return [[NSBundle mainBundle] pathForResource:@"printio-keepy-logo" ofType:@"png"];
    return [[NSBundle mainBundle] pathForResource:@"app-icon57x57" ofType:@"png"];
    
}


- (BOOL)isAuthed{
    return YES;
}



- (void)requestConcreteItems:(NSMutableArray *)items forItem:(id<PIOPhotoSourceItem>)item inScope:(id<PIOPhotoSourceItem>)currentScope page:(NSUInteger)page count:(NSUInteger)count withCompletionHandler:(PIOItemsCompletionHandler)completionHandler errorHandler:(PIOPhotoSourceErrorHandler)errorHandler{
    
    /*
    if([item isKindOfClass:[PrintIOSourceItemMoveUp class]]){
        
        PrintIOSourceItemMoveUp *currentItem = item;
        
        if(currentItem.parentItem){
            
            completionHandler(@[currentItem.parentItem]);
            return;
            
        }
        
        completionHandler(nil);
        return;
        
    }
    
    
    
    PrintIOSourceItemMoveUp *moveUp = [PrintIOSourceItemMoveUp new];
    moveUp.parentItem = item;
    
    [items addObject:moveUp];
    
    */
    
    
    
    if(item){ // if an item is selected, we don't wanna return anything
        
    
        
        // let's check whether it's a kid item
        if([item isKindOfClass:[PrintIOSourceItemKid class]]){
            
            PrintIOSourceItemKid *currentItem = item;
            KPLocalKid *currentKid = currentItem.sourceKid;
            
            // let's get all the current kid's pictures
            
            NSArray *itemIDs = [KPLocalKidItem fetchItemIDsForKid:currentKid];
            NSArray *unsortedKeepies = [KPLocalItem idsToObjects:itemIDs];
            
            
            NSSortDescriptor *dateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"itemDate" ascending:NO];
            NSArray *allKeepies = [unsortedKeepies sortedArrayUsingDescriptors:@[dateSortDescriptor]];
            
            
            
            for(KPLocalItem *currentKeepy in allKeepies){
                
                if(currentKeepy.itemType != 4 ) {
                    
                    PrintIOSourceItemKeepy *currentKeepyItem = [PrintIOSourceItemKeepy new];
                    currentKeepyItem.sourceKeepy = currentKeepy;
                    
                    [items addObject:currentKeepyItem];
                    
                }
                
            }
            
            
            completionHandler(items);
            return;
            
            
            
            
        }
        
        // we are trying to go up, I guess, right?
        // errorHandler(nil);
        // return;
        
    }
    
    
    
    
    
    NSLog(@"test");
    
    NSArray *allChildren = [KPLocalKid fetchAll];
//    NSMutableArray *itemSources = @[].mutableCopy;
    
    
    NSSortDescriptor *ageSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"birthdate" ascending:NO];
    NSArray *sortedKids = [allChildren sortedArrayUsingDescriptors:@[ageSortDescriptor]];
    
    
    
    for(KPLocalKid *currentChild in sortedKids){
        
        PrintIOSourceItemKid *currentKidItem = [PrintIOSourceItemKid new];
        currentKidItem.sourceKid = currentChild;
        
        [items addObject:currentKidItem];
        
    }
    
    
    
    completionHandler(items);
    
    
}


@end
