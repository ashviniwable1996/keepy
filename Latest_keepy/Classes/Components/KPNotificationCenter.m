//
//  KPNotificationCenter.m
//  Keepy
//
//  Created by Troy Payne on 2/3/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import "KPNotificationCenter.h"
#import "KPNotificationPresenter.h"

@interface KPNotificationCenter ()

@property(nonatomic, strong) KPNotificationPresenter *presenter;
@property(nonatomic, assign) BOOL isRevealed;

@end

@implementation KPNotificationCenter

- (KPNotificationCenter *)init {
    if ((self = [super init])) {
        _isRevealed = NO;
    }
    return self;
}

+ (KPNotificationCenter *)sharedCenter {
    static KPNotificationCenter *sharedCenter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCenter = [[self class] new];
    });
    return sharedCenter;
}

- (void)presentNotification:(KPNotificationMessage *)notification {
    if (self.isRevealed == NO) {
        self.presenter = [[KPNotificationPresenter alloc] init];
        __weak KPNotificationCenter *weakSelf = self;
        [self.presenter revealNotification:notification finished:^{
            weakSelf.isRevealed = YES;
            [weakSelf performSelector:@selector(presentNotification:) withObject:nil afterDelay:notification.timeout];
        }];
    } else {
        __weak KPNotificationCenter *weakSelf = self;
        [self.presenter dismiss:^{
            weakSelf.isRevealed = NO;
            if (notification) {
                [weakSelf presentNotification:notification];
            }
        }];
    }
}

- (void)revealNotification:(KPNotificationMessage *)notification {
    if (self.isRevealed == NO) {
        self.presenter = [[KPNotificationPresenter alloc] init];
        __weak KPNotificationCenter *weakSelf = self;
        [self.presenter revealNotification:notification finished:^{
            weakSelf.isRevealed = YES;
        }];
    } else {
        __weak KPNotificationCenter *weakSelf = self;
        [self.presenter dismiss:^{
            weakSelf.isRevealed = NO;
            [weakSelf revealNotification:notification];
        }];
    }
}

@end
