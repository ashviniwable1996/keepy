//
//  KPNotificationPresenter.h
//  Keepy
//
//  Created by Troy Payne on 2/4/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import "KPNotificationMessage.h"

typedef void (^KPNotificationPresenterFinishedBlock)();

@interface KPNotificationPresenter : NSObject

- (void)revealNotification:(KPNotificationMessage *)notification finished:(KPNotificationPresenterFinishedBlock)finished;

- (void)dismiss:(KPNotificationPresenterFinishedBlock)finished;

@end
