//
//  KPSelectionPopViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 4/3/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KPSelectionPopViewController.h"

#import "Keepy-Swift.h"

@interface KPSelectionPopViewController ()

@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) NSArray *items;
@property(nonatomic) int selectedIndex;
@property(nonatomic, strong) UIImageView *arrowImage;

@end

@implementation KPSelectionPopViewController

- (instancetype)initWithItems:(NSArray *)items withSelectedIndex:(int)selectedIndex andPoint:(CGPoint)p {
    self = [super initWithNibName:@"KPSelectionPopViewController" bundle:nil];
    if (self) {
        self.items = items;
        self.selectedIndex = selectedIndex;
        self.arrowPoint = p;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    CGRect r = [[UIScreen mainScreen] bounds];
    self.view.frame = r;
    self.view.bounds = r;

    UIView *bkgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bkgView.backgroundColor = [UIColor blackColor];
    bkgView.alpha = 0.5;
    [self.view addSubview:bkgView];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    gesture.cancelsTouchesInView = YES;
    [self.view addGestureRecognizer:gesture];

    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width - 20, self.view.bounds.size.height)];
    container.center = self.view.center;
    [self.view addSubview:container];

    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doNothingTap:)];
    gesture.cancelsTouchesInView = NO;
    [container addGestureRecognizer:gesture];


    UIImageView *bkgImage = [[UIImageView alloc] initWithFrame:container.bounds];
    bkgImage.image = [[UIImage imageNamed:@"popover-BG"] resizableImageWithCapInsets:UIEdgeInsetsMake(44, 17.5, 44, 17.5)];
    [container addSubview:bkgImage];

    self.arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width - 20 - 21) / 2, 0, 21, 11)];
    self.arrowImage.image = [UIImage imageNamed:@"popover-arrow"];

    [container addSubview:self.arrowImage];

    self.scrollView = [[UIScrollView alloc] initWithFrame:container.bounds];
    self.scrollView.showsVerticalScrollIndicator = NO;

    [container addSubview:self.scrollView];

    if (self.items == nil || [[self.items objectAtIndex:0] isKindOfClass:[NSDate class]]) {
        float ah = 200;
        float aw = 300;
        float ay = self.arrowPoint.y;// - ah - 11;
        float ax = self.arrowPoint.x;
        float arrowDX = 0;
        if (ax + aw > self.view.bounds.size.width - 20) {
            ax = self.view.bounds.size.width - 10 - aw;
            arrowDX = (self.arrowPoint.x - ax);
        }

        bkgImage.frame = CGRectMake(0, 11, aw, ah);
        self.scrollView.frame = bkgImage.frame;
        container.frame = CGRectMake(10, ay, aw, ah + 11);
        container.center = self.view.center;
        self.arrowImage.frame = CGRectMake(20 + arrowDX, 0, 21, 11);

        UIDatePicker *pickerView = [[UIDatePicker alloc] init];
        [pickerView setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        [pickerView addTarget:self action:@selector(datePickerValueDidChange:) forControlEvents:UIControlEventValueChanged];
        pickerView.datePickerMode = UIDatePickerModeDate;
        if (self.items != nil) {
            pickerView.date = (NSDate *) [self.items objectAtIndex:0];
        }

        //[container addSubview:pickerView];
        CGSize pickerSize = [pickerView sizeThatFits:CGSizeZero];

        UIView *pickerTransformView = [[UIView alloc] initWithFrame:CGRectMake(-10.0f, 0.0f, pickerSize.width, pickerSize.height)];
        pickerTransformView.transform = CGAffineTransformMakeScale(0.85f, 0.85f);
        [self.view layoutIfNeeded];
        
        [pickerTransformView addSubview:pickerView];
        [container addSubview:pickerTransformView];
       
    }
    else {
        __block float ax = 20;
        __block float ay = 15;

        void(^createButtonBlock)(id item, int aindex, BOOL isPlaces) = ^(id item, int aindex, BOOL isPlaces) {


            NSString *atitle = @"";
            Asset *aImage = nil;

            if ([item isKindOfClass:[Kid class]]) {
                atitle = [(Kid *) item name];
                aImage = ((Kid *) item).image;
            }
            else if ([item isKindOfClass:[Place class]]) {
                atitle = [(Place *) item title];
            }

            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn addTarget:self action:@selector(itemTap:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = aindex;
            [btn setTitleColor:[UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:14];
            CGRect aframe = btn.titleLabel.frame;
            aframe.size.width = 70;
            btn.titleLabel.frame = aframe;
            btn.titleLabel.numberOfLines = 1;
            btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            if (item != nil) {
                [btn setTitle:[NSString stringWithFormat:@"%@", atitle] forState:UIControlStateNormal];
            }
            else {
                if (isPlaces) {
                    [btn setTitle:KPLocalizedString(@"add place") forState:UIControlStateNormal];
                } else {
                    [btn setTitle:KPLocalizedString(@"add") forState:UIControlStateNormal];
                }
            }


            if (isPlaces) {
                btn.frame = CGRectMake(ax, ay, 260, 44);
                btn.titleLabel.textAlignment = NSTextAlignmentLeft;
                //btn.backgroundColor = UIColorFromRGB(0xffffff);

                CGRect aframe = btn.titleLabel.frame;
                aframe.size.width = 260;
                btn.titleLabel.frame = aframe;

                [btn setBackgroundImage:[[UIImage imageNamed:@"edit-photo-button"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 23.5, 0, 23.5)] forState:UIControlStateNormal];

            }
            else {
                btn.frame = CGRectMake(ax, ay, 53, 53);
                if (item != nil) {
                    //[btn setBackgroundImage:[UIImage imageNamed:@"kidPicBg"] forState:UIControlStateNormal];
                }
                else {
                    [btn setBackgroundImage:[UIImage imageNamed:@"settings-add-kid"] forState:UIControlStateNormal];
                }
                btn.titleEdgeInsets = UIEdgeInsetsMake(70, 0, 0, 0);
            }

            [self.scrollView addSubview:btn];


            if ([item isKindOfClass:[Kid class]]) {
                UIImageView *kidImg = [[UIImageView alloc] initWithFrame:CGRectMake(ax, ay, 53, 53)];
                [self.scrollView addSubview:kidImg];

                __weak UIImageView *wkidImg = kidImg;

                if (aImage == nil) {
                    kidImg.image = [GlobalUtils getKidDefaultImage:((Kid *) item).gender.intValue];
                } else {
                    [kidImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, aImage.urlHash]] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        wkidImg.image = [wkidImg.image imageMaskedWithElipse:CGSizeMake(290, 290)];
                    }];
                }
            }
            else if (isPlaces) {
                /*
                UIImageView *placeImg = [[UIImageView alloc] initWithFrame:CGRectMake(ax+3, ay+3, 47, 47)];
                placeImg.image = [UIImage imageNamed:@"place"];
                [self.scrollView addSubview:placeImg];
                 */
                ay += 50;
                return;
            }

            ax += 73;
            if (ax > self.view.frame.size.width - 73) {
                ax = 15;
                ay += 73;
            }

        };

        BOOL isPlaces = NO;
        if ([self.items count] > 0 && [[self.items objectAtIndex:0] isKindOfClass:[Place class]]) {
            isPlaces = YES;
        }
        for (int i = 0; i < [self.items count]; i++) {
            createButtonBlock([self.items objectAtIndex:i], i, isPlaces);
        }
        createButtonBlock(nil, -1, isPlaces);

        float ah = ay + 73 + 10;

        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width - 20, ah);

        if (ah > 166) {
            ah = 166;
        }

        float aw = self.view.bounds.size.width - 20;
        if (isPlaces) {
            aw = 300;
        } else if ([self.items count] < 3) {
            aw = 20 + 73 * ([self.items count] + 1);
        }

        ay = self.arrowPoint.y;// - ah - 11;
        ax = self.arrowPoint.x;
        float arrowDX = 0;
        if (ax + aw > self.view.bounds.size.width - 20) {
            ax = self.view.bounds.size.width - 10 - aw;
            arrowDX = (self.arrowPoint.x - ax);
        }

        bkgImage.frame = CGRectMake(0, 11, aw, ah);
        self.scrollView.frame = bkgImage.frame;
        container.frame = CGRectMake(ax, ay, aw, ah + 11);
        self.arrowImage.frame = CGRectMake(20 + arrowDX, 0, 21, 11);
    }

    self.arrowImage.hidden = self.hideArrow;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)itemTap:(UIButton *)sender {
    [self.delegate itemSelected:sender.tag];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.alpha = 0;
    }                completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

- (void)viewTap:(UITapGestureRecognizer *)gesture {
    [self.delegate itemSelectionCancelled];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.alpha = 0;
    }                completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];

}

- (void)doNothingTap:(UITapGestureRecognizer *)gesture {

}

- (void)datePickerValueDidChange:(UIDatePicker *)datePicker {
    //self.date = datePicker.date;
    [self.delegate datePickerValueDidChange:datePicker.date];
}

- (void)setHideArrow:(BOOL)hideArrow {
    _hideArrow = hideArrow;
    self.arrowImage.hidden = hideArrow;
}

@end
