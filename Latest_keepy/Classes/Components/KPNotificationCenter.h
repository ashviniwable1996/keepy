//
//  KPNotificationCenter.h
//  Keepy
//
//  Created by Troy Payne on 2/3/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import "KPNotificationMessage.h"

@interface KPNotificationCenter : NSObject

+ (KPNotificationCenter *)sharedCenter;

- (void)presentNotification:(KPNotificationMessage *)notification;

- (void)revealNotification:(KPNotificationMessage *)notification;

@end



