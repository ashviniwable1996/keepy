//
//  PrintIOManager.m
//  Keepy
//
//  Created by Arik Sosman on 6/25/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//


#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

#import <PrintIO/PrintIO.h>
#import <PrintIO/PIOSideMenuButton.h>

#import "PrintIOManager.h"
#import "PrintIOPhotoSourceKeepy.h"

#import "KPNotificationCenter.h"
#import "KPNotification.h"
#import "ItemsViewController.h"

#import "Keepy-Swift.h"


static NSArray *_items = nil;
static NSDictionary *_denormalizedItems = nil;

@interface PrintIOManager()<PrintIODelegate> {
    
}

@end


@implementation PrintIOManager

+ (instancetype)sharedInstance
{
    static PrintIOManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [PrintIOManager new];
    });
    return sharedInstance;
}

+ (NSArray *)items {
    return _items;
}

+ (void)setItems:(NSArray *)items {
    _items = items;
}

+ (NSDictionary *)denormalizedItems {
    return _denormalizedItems;
}

+ (void)setDenormalizedItems:(NSDictionary *)denormalizedItems {
    _denormalizedItems = denormalizedItems;
}

+ (void)showPrintIODialogInViewController:(UIViewController *)viewController withImage:(UIImage *)image{
    
    // [[NSNotificationCenter defaultCenter] postNotificationName:@"AlbumCreationButtonTappedNotification" object:nil];
    
    
    
    
    
    NSString *productionRecipeID = @"f920cf62-d501-4db3-a101-60d62050a457";
    NSString *stagingRecipeID = @"5775ba71-ac25-4c49-b73f-77043c6da9ca";
    
    
    NSMutableDictionary *printIODetails = @{}.mutableCopy;
    NSString *printIOVersion = [self getPrintIOVersion];
    if(printIOVersion){
        printIODetails[@"printio-version"] = printIOVersion;
    }
    
    [[Mixpanel sharedInstance] track:@"Opening Print.IO" properties:printIODetails];
    
    PrintIO *printIO = [[PrintIO alloc] initWithViewController:viewController environment:PRINTIO_PRODUCTION productionRecipeId:productionRecipeID stagingRecipeId:stagingRecipeID];
    
    
    
    // initialize US defaults
    // [printIO setCountryCode:@"US"];
    // [printIO setCurrencyCode:@"usd"];
    
    [printIO setAvailablePhotoSources:@[]];
    PrintIOPhotoSourceKeepy *keepySource = [PrintIOPhotoSourceKeepy new];
    [printIO setCustomPhotoSources:@[keepySource]];
    
    
    
    if(!image){ // there was no specific image passed, so it was not the gift button, so we allow only albums
        
//        NSArray *availableProducts = @[@(PRODUCT_DELUXE_MINIBOOK()), @(PRODUCT_MINIBOOKS()), @(PRODUCT_PHOTO_BOOKS()), @(PRODUCT_PHOTOGRAPHIC_BOOKS()), @(PRODUCT_SOFTCOVER_PHOTOBOOKS()), @(PRODUCT_TINYBOOKS()), @(PRODUCT_LAYFLAT_PHOTOBOOKS())];
        // [printIO setAvailableProducts:availableProducts];
        // [printIO hideCategoriesInFeaturedProducts:YES];
        
    }else{
        
        [printIO setImages:@[image]];
        // [printIO disablePhotoSourcesWhenImagesArePassedIn:YES];
        
    }
    
    
    [printIO hideIconForUploadInstructions:YES];
    [printIO setDefaultPhotoSource:(PIOPhotoSources)@[keepySource] albumId:@""];
    
    
    
    UIColor *keepyBlue = [UIColor colorWithRed:12/255.0 green:197/255.0 blue:219/255.0 alpha:1];
    UIColor *keepyGray = [UIColor colorWithRed:231/255.0 green:230/255.0 blue:230/255.0 alpha:1];
    UIColor *keepyDarkGray = [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1];
    
    // 
    [printIO setSelectionColor:keepyBlue];
    [printIO setPositiveButtonsBackgroundColor:keepyBlue titleColor:[UIColor whiteColor]];
    [printIO setNegativeButtonsBackgroundColor:keepyGray titleColor:[UIColor blackColor]];
    
    // UIImage *image = [UIImage imageNamed:@"back_icon_printio"];
    
    NSString *backIconPath = [[NSBundle mainBundle] pathForResource:@"back_icon_printio" ofType:@"png"];
    NSString *navbarBGPath = [[NSBundle mainBundle] pathForResource:@"printio-navbar-bg" ofType:@"png"];
    
    [printIO setIconForBackButton:backIconPath];
    [printIO setNavigationBarBackground:navbarBGPath];
    // [printIO setIconForSaveButtonInCustomizeProduct:backIconPath];
    // printIO 
    
    
    // [printIO setStatusBarDark:YES hidden:YES];
    
    [printIO showCountrySelectionOnScreen:PIO_SCREEN_FEATURED_PRODUCTS backgroundColor:keepyDarkGray];
    
    [printIO open];
    
    
    // PIODefaultPhotoSource *dps = [[PIODefaultPhotoSource alloc] init];
    
    
    
    
    
    
    
    
    // let's customize this thing, shall we?
    // UIImage *firstImage = [UIImage imageNamed:@"calendar-add-age-4"];
    // [printIO setImages:@[firstImage, @"https://www.google.com/images/srpr/logo11w.png"]];
    
    // [printIO disablePhotoSourcesWhenImagesArePassedIn:YES];
    
    
    
    // let's create a custom photo source
    
    
    
    
    // printIO.delegate = self;
    [printIO open];
    
}

+ (NSString *)getPrintIOVersion{
    
    NSURL * bundleURL = [[NSBundle mainBundle] URLForResource:@"PrintIOBundle" withExtension:@"bundle"];
    
    if (!bundleURL)
    {
        return @"Error Getting Bundle URL";
    }
    
    NSBundle * bundle = [NSBundle bundleWithURL:bundleURL];
    
    if (!bundle)
    {
        return [@"Error Loading Bundle" stringByAppendingString:[bundleURL absoluteString]];
    }
    
    NSDictionary *infoDictionary = [bundle infoDictionary];
    NSString *version = [NSString stringWithFormat:@"%@ (%i)", [infoDictionary valueForKey:@"CFBundleShortVersionString"], [[infoDictionary valueForKey:@"CFBundleVersion"]intValue]];
    
    return version;
    
}

+ (void)showPreselectedPrintIODialogInViewController:(UIViewController *)viewController {
//    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@""
//                                                                message:NSLocalizedString(@"PrintIOWarnAboutTime", comment: @"")
//                                                         preferredStyle:UIAlertControllerStyleAlert];
//    [ac addAction:[UIAlertAction actionWithTitle: @"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[PrintIOManager sharedInstance] showPreselectedPrintIODialogInViewController: viewController];
//    }]];
//    [ac presentWithAnimated:YES completion:nil];
}

+ (void)showBookReadyDialogInViewController:(UIViewController *)viewController {
    // Check last showing popup
    NSDate *lastShowingPopup = [[NSUserDefaults standardUserDefaults] valueForKey:ShowedPopupDate];
    if (lastShowingPopup &&
        [lastShowingPopup daysBetweenDatesWithStartDate:lastShowingPopup endDate:[NSDate date]] < 7) {
        return;
    }
    
    // Show popup
    NSArray *unsortedKids = [KPLocalKid fetchAll];
    NSString *names = @"";
    
    for (int i = 0; i < [unsortedKids count] - 1; i++) {
        names = [names stringByAppendingString:[unsortedKids[i] name]];
        names = [names stringByAppendingString:@", "];
    }
    names = [names stringByAppendingString:[[unsortedKids lastObject] name]];
    
    BookReadyDialogView *dialog = [BookReadyDialogView create:@"BookReadyDialogView"];
    dialog.bookImage = [UIImage imageNamed:@"photobook"];
    NSString *messageTemplate = NSLocalizedString(@"PrintIOBookIsReady", @"");
    //dialog.message = [NSString stringWithFormat:messageTemplate, names];
    dialog.message = messageTemplate;
    
    __block Boolean isFirstTime = YES;
    dialog.actionConfirm = ^{
//        if (isFirstTime) {
//            isFirstTime = NO;
//            return;
//        }
        [PrintIOManager showPreselectedPrintIODialogInViewController: viewController];
    };
    dialog.actionCancel = ^{
        [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:ShowedPopupDate];
    };
    [dialog show];
}

+ (void)showBookReadyNotification {
    // Check last showing notification
    NSDate *lastShowingNotification = [[NSUserDefaults standardUserDefaults] valueForKey:ShowedNotificationDate];
    if (lastShowingNotification &&
        [lastShowingNotification daysBetweenDatesWithStartDate:lastShowingNotification endDate:[NSDate date]] < 1) {
        return;
    }
    
    // Check that notification does not exist
    NSArray *arr = [KPNotification MR_findAllSortedBy:@"notificationDate" ascending:NO];
    for (KPNotification *notification in arr) {
        if (notification.notificationType.intValue == NotificationTypeBookIsReady) {
            return;
        }
    }
    
    // Show notification
    KPNotification *noti = [KPNotification MR_createEntity];
    noti.notificationId = @(0);
    noti.notificationType = @(NotificationTypeBookIsReady);
    noti.subject = NSLocalizedString(@"PrintIOBookIsReadyNotification", @"");
    noti.notificationDate = [NSDate date];
    noti.status = @(NotificationStatusUnread);
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:ShowedNotificationDate];
}

- (void)showPreselectedPrintIODialogInViewController:(UIViewController *)viewController {
    // Disable preselected book functionality
    [PrintIOManager showPrintIODialogInViewController:viewController withImage: nil];
    return;
    
    
    
    NSString *productionRecipeID = @"f920cf62-d501-4db3-a101-60d62050a457";
    NSString *stagingRecipeID = @"5775ba71-ac25-4c49-b73f-77043c6da9ca";
    
    
    NSMutableDictionary *printIODetails = @{}.mutableCopy;
    NSString *printIOVersion = [PrintIOManager getPrintIOVersion];
    if(printIOVersion){
        printIODetails[@"printio-version"] = printIOVersion;
    }
    
    [[Mixpanel sharedInstance] track:@"Opening Print.IO" properties:printIODetails];
    
    NSArray *items = [PrintIOManager items];
    NSDictionary *denormalizedItems = [PrintIOManager denormalizedItems];
    
    NSInteger numberOfPager = 40;
    if (items && items.count >= numberOfPager) {
        NSMutableArray *imageURLs = [NSMutableArray new];

        NSMutableSet *_preselectedImages = [NSMutableSet new];
        for (int i = 0; i < numberOfPager; i++) {
            KPLocalItem *item = (KPLocalItem *)items[numberOfPager - i];
            KPLocalAsset *originalImage = denormalizedItems[@(item.identifier)][@"originalImage"];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, originalImage.urlHash]];
            NSLog(@"%@", url);
            [imageURLs addObject:url.absoluteString];
            
            [_preselectedImages addObject:url.absoluteString];
        }
        self.preselectedImages = _preselectedImages;
        
        
        PrintIO *printIO = [[PrintIO alloc] initWithViewController:viewController environment:PRINTIO_PRODUCTION productionRecipeId:productionRecipeID stagingRecipeId:stagingRecipeID];
        printIO.delegate = self;
        
        [printIO setImages: imageURLs];
        [printIO setPreselectPassedImages: YES];
        [printIO disablePhotoSourcesWhenImagesArePassedIn: YES];
        [printIO goToProductId: PRODUCT_HARDCOVER_PHOTOBOOKS() withSKU: @"HardcoverBook-ImgWrpCover-115x85-100#GlossPaper-Matte-40"];
        
        [printIO open];
    }
}

// MARK: - PrintIODelegate

- (void)PrintIOWidgetOnOpen {
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)PrintIOWidgetOnCloseWithData:(NSDictionary *)data {
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)printIO:(PrintIO *)printIO didCompleteOrderWithData:(NSDictionary *)data {
    NSSet *printedImagesSet = [[NSUserDefaults standardUserDefaults] valueForKey:PrintedImagesDefaulsKey];
    
    if (printedImagesSet == nil) {
        printedImagesSet = [NSSet new];
    }
    
    printedImagesSet = [printedImagesSet setByAddingObjectsFromSet:self.preselectedImages];
    [[NSUserDefaults standardUserDefaults] setObject:printedImagesSet forKey:PrintedImagesDefaulsKey];
}

@end
