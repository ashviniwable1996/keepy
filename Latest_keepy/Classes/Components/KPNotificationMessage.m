//
//  KPNotificationMessage.m
//  Keepy
//
//  Created by Troy Payne on 2/4/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import "KPNotificationMessage.h"

@implementation KPNotificationMessage

- (instancetype)init {
    if ((self = [super init])) {
        _bgColor = [UIColor colorWithRed:(238.0f / 255.0f) green:(22.0f / 255.0f) blue:(119.0f / 255.0f) alpha:1.0f];
        _textColor = [UIColor colorWithRed:(255.0f / 255.0f) green:(255.0f / 255.0f) blue:(255.0f / 255.0f) alpha:1.0f];
        _dismissHandler = nil;
        _timeout = 5.0;
    }
    return self;
}

@end