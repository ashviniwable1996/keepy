//
//  PrintIOManager.h
//  Keepy
//
//  Created by Arik Sosman on 6/25/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const PrintedImagesDefaulsKey = @"PrintedImagesDefaulsKey";
static NSString *const ShowedNotificationDate = @"ShowedNotificationDate";
static NSString *const ShowedPopupDate = @"ShowedPopupDate";


@interface PrintIOManager : NSObject

@property(nonatomic, strong) NSSet *preselectedImages;

+ (NSArray *)items;
+ (void)setItems:(NSArray *)items;
+ (NSDictionary *)denormalizedItems;
+ (void)setDenormalizedItems:(NSDictionary *)denormalizedItems;


+ (void)showPrintIODialogInViewController:(UIViewController *)viewController withImage:(UIImage *)image;
+ (NSString *)getPrintIOVersion;
+ (void)showPreselectedPrintIODialogInViewController:(UIViewController *)viewController;
+ (void)showBookReadyDialogInViewController:(UIViewController *)viewController;
+ (void)showBookReadyNotification;

- (void)showPreselectedPrintIODialogInViewController:(UIViewController *)viewController;

@end
