//
//  PKPopViewController.m
//  Keepy
//
//  Created by Yaniv Solnik on 3/14/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KPPopViewController.h"

#import "Keepy-Swift.h"

@interface KPPopViewController ()

@property(nonatomic, strong) UIButton *leftButton;
@property(nonatomic, strong) UIButton *rightButton;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) NSDictionary *leftButtonInfo;

@end

@implementation KPPopViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = UIColorFromRGB(0xe6e4e3);//[UIColor colorWithPatternImage:[UIImage imageNamed:@"lightBgTile128x128"]];
    /*
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        UIView *topBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        topBar.backgroundColor = [UIColor colorWithRed:0.337 green:0.255 blue:0.235 alpha:1];
        topBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:topBar];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        self.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:18];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.text = self.title;
        self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:self.titleLabel];
    }
    else
    {
        [self.navigationController setNavigationBarHidden:NO];
    }*/

    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1], NSForegroundColorAttributeName, [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17], NSFontAttributeName, nil];

    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.view.tintColor = UIColorFromRGB(0x2CA0C4);
        //self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0xEFEEED);
        //self.navigationController.navigationBar.translucent = NO;
    }

    [self.navigationController setNavigationBarHidden:NO];

}


- (void)setButtons:(NSDictionary *)leftButtonInfo andRightButtonInfo:(NSDictionary *)rightButtonInfo {
    self.leftButtonInfo = leftButtonInfo;

    if (NO)//(floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        if (leftButtonInfo != nil) {
            self.leftButton = [self createButton:leftButtonInfo isLeftButton:YES];
        }

        if (rightButtonInfo != nil) {
            self.rightButton = [self createButton:rightButtonInfo isLeftButton:NO];
        }
    }
    else {

        if (leftButtonInfo != nil) {
            UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithTitle:[leftButtonInfo valueForKey:@"title"] style:UIBarButtonItemStylePlain target:self action:@selector(topBtnTap:)];
            [leftBtn setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Light" size:17]} forState:UIControlStateNormal];

            if ([[leftButtonInfo valueForKey:@"buttonType"] intValue] == BUTTON_TYPE_BACK)
                DDLogInfo(@"test");
                //self.navigationItem.backBarButtonItem = leftBtn;
            else {
                self.navigationItem.leftBarButtonItem = leftBtn;
            }

        }
        else {
            self.navigationItem.leftBarButtonItem = nil;
        }

        if (rightButtonInfo != nil) {
            UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithTitle:[rightButtonInfo valueForKey:@"title"] style:UIBarButtonItemStylePlain target:self action:@selector(topBtnTap:)];
            rightBtn.tag = 1;
            rightBtn.enabled = ([[rightButtonInfo valueForKey:@"disabled"] intValue] != 1);
            [rightBtn setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17]} forState:UIControlStateNormal];
            self.navigationItem.rightBarButtonItem = rightBtn;
        }
        else {
            self.navigationItem.rightBarButtonItem = nil;
        }
    }
}

- (UIButton *)createButton:(NSDictionary *)btnInfo isLeftButton:(BOOL)isLeftButton {
    UIButton *result = [UIButton buttonWithType:UIButtonTypeCustom];
    if (!isLeftButton) {
        result.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    }


    if ([[btnInfo valueForKey:@"buttonType"] intValue] == BUTTON_TYPE_NORMAL) {
        [result setBackgroundImage:[[UIImage imageNamed:@"button-defaultBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 15.5, 0, 15.5)] forState:UIControlStateNormal];
        [result setBackgroundImage:[[UIImage imageNamed:@"button-defaultBG-disabled"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 15.5, 0, 15.5)] forState:UIControlStateDisabled];

    }
    else if ([[btnInfo valueForKey:@"buttonType"] intValue] == BUTTON_TYPE_BACK) {
        [result setBackgroundImage:[[UIImage imageNamed:@"button-backBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 14, 0, 14)] forState:UIControlStateNormal];
    }
    else if ([[btnInfo valueForKey:@"buttonType"] intValue] == BUTTON_TYPE_PRIME) {
        [result setBackgroundImage:[[UIImage imageNamed:@"button-prime-actionBG"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 15.5, 1, 15.5)] forState:UIControlStateNormal];
        [result setBackgroundImage:[[UIImage imageNamed:@"button-prime-actionBG-disabled"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 15.5, 1, 15.5)] forState:UIControlStateDisabled];
    }

    result.enabled = ([[btnInfo valueForKey:@"disabled"] intValue] != 1);

    [result setTitleColor:[UIColor colorWithRed:0.314 green:0.235 blue:0.216 alpha:1] forState:UIControlStateNormal];
    [result setTitleColor:[UIColor colorWithRed:0.529 green:0.486 blue:0.478 alpha:1] forState:UIControlStateDisabled];
    result.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:12];
    [result setTitle:[btnInfo valueForKey:@"title"] forState:UIControlStateNormal];

    CGSize textSize = [[btnInfo valueForKey:@"title"] sizeWithFont:result.titleLabel.font thatFitsToSize:CGSizeMake(200, 30)];

    float ax = 9;
    float w = textSize.width;
    float minW = 35;
    if ([[btnInfo valueForKey:@"buttonType"] intValue] == BUTTON_TYPE_BACK) {
        minW = 41;
    }

    if (w < minW) {
        w = minW;
    }

    if (!isLeftButton) {
        ax = self.view.frame.size.width - 30 - w;
    }

    result.frame = CGRectMake(ax, 9, 20 + w, 30);
    result.tag = (isLeftButton) ? 0 : 1;
    [result addTarget:self action:@selector(topBtnTap:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:result];

    return result;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setTitle:(NSString *)title {
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.titleLabel.text = title;
    } else {
        self.navigationItem.title = title;
    }
}


- (void)setRightButtonEnabled:(BOOL)rightButtonEnabled {

    _rightButtonEnabled = rightButtonEnabled;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.rightButton.enabled = rightButtonEnabled;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = rightButtonEnabled;
    }
}

#pragma mark - Actions

- (void)topBtnTap:(UIButton *)sender {

}

@end
