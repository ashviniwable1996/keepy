//
//  KPPopView.m
//  Keepy
//
//  Created by Yaniv Solnik on 3/14/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import "KPPopView.h"

@implementation KPPopView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
