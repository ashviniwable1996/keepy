//
//  PrintIOSourceItemKidAge.h
//  Keepy
//
//  Created by Arik Sosman on 6/25/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <PrintIO/PrintIO.h>
#import <PrintIO/PIOPhotoSourceItem.h>
#import <PrintIO/PIODefaultPhotoSourceItem.h>

@interface PrintIOSourceItemKidAge : PIODefaultPhotoSourceItem <PIOPhotoSourceItem> 

// @property (strong, nonatomic)

@end
