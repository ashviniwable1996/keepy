//
//  KPNotificationPresenter.m
//  Keepy
//
//  Created by Troy Payne on 2/4/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import "KPNotificationPresenter.h"
#import "KPNotificationView.h"

@interface KPNotificationPresenter ()

@property(nonatomic, strong) KPNotificationWindow *notificationWindow;

@end

@implementation KPNotificationPresenter

- (instancetype)init {
    if ((self = [super init])) {
        _notificationWindow = [[KPNotificationWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _notificationWindow.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _notificationWindow.userInteractionEnabled = YES;
        _notificationWindow.autoresizesSubviews = YES;
        _notificationWindow.opaque = NO;
        _notificationWindow.hidden = NO;
        _notificationWindow.windowLevel = UIWindowLevelStatusBar;
    }
    return self;
}

- (void)dealloc {
    self.notificationWindow.hidden = YES;
    [self.notificationWindow removeFromSuperview];
    self.notificationWindow.rootViewController = nil;
    self.notificationWindow = nil;
}

- (void)revealNotification:(KPNotificationMessage *)notification finished:(KPNotificationPresenterFinishedBlock)finished {
    if (notification.dismissHandler) {
        KPNotificationActionView *notificationView = (KPNotificationActionView *) [[[NSBundle mainBundle] loadNibNamed:@"KPNotificationView" owner:self options:nil] lastObject];
        notificationView.notificationMessage = notification;
        [notificationView layoutSubviews];

        CGRect visibleFrame = notificationView.frame;
        CGRect hiddenFrame = CGRectMake(notificationView.frame.origin.x, -notificationView.frame.size.height, notificationView.frame.size.width, notificationView.frame.size.height);

        KPNotificationPresenterFinishedBlock originalTapHandler = notificationView.notificationMessage.tapHandler;
        KPNotificationPresenterFinishedBlock wrappingTapHandler = ^{
            [notificationView removeFromSuperview];
            originalTapHandler();
        };
        notificationView.notificationMessage.tapHandler = wrappingTapHandler;

        KPNotificationPresenterFinishedBlock originalDismissHandler = notificationView.notificationMessage.dismissHandler;
        KPNotificationPresenterFinishedBlock wrappingDismissHandler = ^{
            [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
                notificationView.frame = hiddenFrame;
                notificationView.alpha = 0.0f;
            }                completion:^(BOOL fin) {
                [notificationView removeFromSuperview];
                originalDismissHandler();
            }];
        };
        notificationView.notificationMessage.dismissHandler = wrappingDismissHandler;

        self.notificationWindow.notificationView = notificationView;
        self.notificationWindow.rootViewController = [[KPNotificationRootViewController alloc] init];
        [self.notificationWindow.rootViewController.view addSubview:notificationView];

        notificationView.frame = hiddenFrame;
        notificationView.alpha = 0.0f;
        [UIView animateWithDuration:0.5f delay:0.0f options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut) animations:^{
            notificationView.frame = visibleFrame;
            notificationView.alpha = 0.9f;
        }                completion:^(BOOL fin) {
            if (fin) {
                finished();
            }
        }];
    } else {
        KPNotificationView *notificationView = (KPNotificationView *) [[[NSBundle mainBundle] loadNibNamed:@"KPNotificationView" owner:self options:nil] firstObject];
        notificationView.notificationMessage = notification;
        [notificationView layoutSubviews];

        CGRect visibleFrame = notificationView.frame;
        CGRect hiddenFrame = CGRectMake(notificationView.frame.origin.x, -notificationView.frame.size.height, notificationView.frame.size.width, notificationView.frame.size.height);

        KPNotificationPresenterFinishedBlock originalTapHandler = notificationView.notificationMessage.tapHandler;
        KPNotificationPresenterFinishedBlock wrappingTapHandler = ^{
            [notificationView removeFromSuperview];
            originalTapHandler();
        };
        notificationView.notificationMessage.tapHandler = wrappingTapHandler;

        self.notificationWindow.notificationView = notificationView;
        self.notificationWindow.rootViewController = [[KPNotificationRootViewController alloc] init];
        [self.notificationWindow.rootViewController.view addSubview:notificationView];

        notificationView.frame = hiddenFrame;
        notificationView.alpha = 0.0f;
        [UIView animateWithDuration:0.5f delay:0.0f options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut) animations:^{
            notificationView.frame = visibleFrame;
            notificationView.alpha = 0.9f;
        }                completion:^(BOOL fin) {
            if (fin) {
                finished();
            }
        }];
    }
}

- (void)dismiss:(KPNotificationPresenterFinishedBlock)finished {
    UIView *notificationView = self.notificationWindow.notificationView;
    CGRect hiddenFrame = CGRectMake(notificationView.frame.origin.x, -notificationView.frame.size.height, notificationView.frame.size.width, notificationView.frame.size.height);
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
        notificationView.frame = hiddenFrame;
        notificationView.alpha = 0.0f;
    }                completion:^(BOOL fin) {
        if (fin) {
            [notificationView removeFromSuperview];
            finished();
        }
    }];
}

@end
