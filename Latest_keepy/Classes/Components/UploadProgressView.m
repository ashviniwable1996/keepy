//
//  UploadProgressView.m
//  Keepy
//
//  Created by Ryan Lons on 2/2/16.
//  Copyright © 2016 Keepy. All rights reserved.
//

#import "UploadProgressView.h"

@interface UploadProgressView()
{
    UIActivityIndicatorView * indicator;
}

@end

@implementation UploadProgressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.frame.size.width - 25, 10, 10, 10)];
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [indicator startAnimating];
        [self addSubview:indicator];
        
        
        self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.9];
        
        
        
        //-------------- Cancel button
//        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//        btn.tag = 1215;
//        btn.frame = CGRectMake(265, 3, 28, 28);
//        
//        [btn setImage:[UIImage imageNamed:@"cancel-upload.png"] forState:UIControlStateNormal];
//        
//        [btn addTarget:self action:@selector(cancelVideoUpload:) forControlEvents:UIControlEventTouchUpInside];
//        [self.uploadProgressView addSubview:btn];
        
        
        /*
         //-------------- Retry button
         UIButton *retry = [UIButton buttonWithType:UIButtonTypeSystem];
         [retry setTitle:@"RETRY" forState:UIControlStateNormal];
         retry.titleLabel.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
         retry.tag = 1315;
         retry.frame = CGRectMake(170,2,50, 28);
         [retry addTarget:self action:@selector(retryAddingItem) forControlEvents:UIControlEventTouchUpInside];
         retry.hidden = YES;
         [self.uploadProgressView addSubview:retry];*/
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.frame.size.width - 30, 20)];
        _label.font = [UIFont fontWithName:@"ARSMaquettePro-Medium" size:11];
        _label.text = @"processing...";
        _label.textColor = [UIColor blackColor];
        _label.tag = 2222;
        [self addSubview:_label];
        
        
//        UIImageView *thumb = [[UIImageView alloc] initWithFrame:CGRectMake(5, 3, 28, 28)];
//        
//        //    thumb.backgroundColor = [UIColor grayColor];
//        if (self.thumbnail) {
//            thumb.image = self.thumbnail;
//        } else {
//            if (self.item) {
//                KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.item.imageAssetID];
//                [thumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, itemImage.urlHash]] completed:nil];
//            }
//        }
//        
//        thumb.tag = 2221;
//        [self.uploadProgressView addSubview:thumb];
        
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(15, 20, self.frame.size.width - 50, 5)];
        _progressView.tag = 111;
        _progressView.progress = 0.5;
        _progressView.progressTintColor = [UIColor colorWithRed:238 / 256.f green:22 / 256.f blue:119 / 256.f alpha:1];
        [self addSubview:_progressView];
        [self setNeedsDisplay];
        
    }
    return self;
}



@end

