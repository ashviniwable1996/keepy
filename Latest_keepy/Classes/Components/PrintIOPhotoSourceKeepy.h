//
//  PrintIOPhotoSourceKeepy.h
//  Keepy
//
//  Created by Arik Sosman on 6/22/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PrintIO/PrintIO.h>
#import <PrintIO/PIOPhotoSource.h>
#import <PrintIO/PIODefaultPhotoSource.h>


@interface PrintIOPhotoSourceKeepy : PIODefaultPhotoSource <PIOPhotoSource>

@end
