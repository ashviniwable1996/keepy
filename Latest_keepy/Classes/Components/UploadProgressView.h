//
//  UploadProgressView.h
//  Keepy
//
//  Created by Ryan Lons on 2/2/16.
//  Copyright © 2016 Keepy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadProgressView : UIView

@property (nonatomic, strong) UILabel * label;
@property (nonatomic, strong) UIProgressView * progressView;

@end
