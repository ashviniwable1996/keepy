//
//  KPSelectionPopViewController.h
//  Keepy
//
//  Created by Yaniv Solnik on 4/3/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KPSelectionDelegate

@required
- (void)itemSelected:(NSUInteger)aindex;

- (void)itemSelectionCancelled;

@optional
- (void)datePickerValueDidChange:(NSDate *)date;

@end

@interface KPSelectionPopViewController : UIViewController

@property(nonatomic, assign) id <KPSelectionDelegate> delegate;
@property(nonatomic) CGPoint arrowPoint;
@property(nonatomic) BOOL hideArrow;

- (instancetype)initWithItems:(NSArray *)items withSelectedIndex:(int)selectedIndex andPoint:(CGPoint)p;

@end
