//
//  PrintIOSourceItemKeepy.m
//  Keepy
//
//  Created by Arik Sosman on 6/24/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "PrintIOSourceItemKeepy.h"

#import "KPLocalItem.h"
#import "KPLocalAsset.h"

#import "Keepy-Swift.h"

@implementation PrintIOSourceItemKeepy

- (BOOL)isImageItem{
    return YES;
}

/*
- (NSString *)publicURLPath{
    
    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.sourceKeepy.imageAssetID];
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", [[KPDefaults sharedDefaults] assetsURLString], itemImage.urlHash]];
    
    return imageURL.path;
    
}
*/

- (void)fetchImageInPhotoSource:(id<PIOPhotoSource>)photoSource isThumbnail:(BOOL)thumbnail withCompletionHandler:(void (^)(UIImage *))imageFetchCompletionHandler{
    
    KPLocalAsset *itemImage = [KPLocalAsset fetchByLocalID:self.sourceKeepy.imageAssetID];
    NSURL *imageURL;
    
    
    if(thumbnail){
        imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@_medium.jpg", Defaults.assetsURLString, itemImage.urlHash]];
    }else{
        imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, itemImage.urlHash]];
    }
    
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
    
    if(image){
        
        imageFetchCompletionHandler(image);
        
    }
}

- (NSString *)uniqueIdentifier{
    
    return [NSString stringWithFormat:@"keepy|%lu|%lu", (unsigned long)self.sourceKeepy.identifier, (unsigned long)self.sourceKeepy.serverID];
    
}

@end
