//
//  PrintIOSourceItemKeepy.h
//  Keepy
//
//  Created by Arik Sosman on 6/24/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <PrintIO/PrintIO.h>
#import <PrintIO/PIOPhotoSourceItem.h>
#import <PrintIO/PIODefaultPhotoSourceItem.h>

@interface PrintIOSourceItemKeepy : PIODefaultPhotoSourceItem

@property (strong, nonatomic) KPLocalItem *sourceKeepy;

@end
