//
//  KPNotificationView.h
//  Keepy
//
//  Created by Troy Payne on 2/4/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import "KPNotificationMessage.h"

@interface KPNotificationView : UIView

@property(nonatomic, strong) KPNotificationMessage *notificationMessage;

@end


@interface KPNotificationActionView : UIView

@property(nonatomic, strong) KPNotificationMessage *notificationMessage;

@end


@interface KPNotificationWindow : UIWindow

@property(nonatomic, strong) UIView *notificationView;

@end


@interface KPNotificationRootViewController : UIViewController

@end
