//
//  KPNotificationView.m
//  Keepy
//
//  Created by Troy Payne on 2/4/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

#import <CoreText/CoreText.h>
#import "KPNotificationView.h"

#define NOTIFICATION_VIEW_CLASS 50783

@interface KPNotificationView ()

@property(nonatomic, weak) IBOutlet UIButton *button;
@property(nonatomic, weak) IBOutlet UIImageView *image;
@property(nonatomic, weak) IBOutlet UILabel *message;

@end


@implementation KPNotificationView

- (void)layoutSubviews {
    [super layoutSubviews];
    self.image.image = self.notificationMessage.image;
    self.backgroundColor = self.notificationMessage.bgColor;
    self.message.textColor = self.notificationMessage.textColor;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@""];
    
    if(self.notificationMessage.message){ // occasionally, this may be nil, in which case the string allocation fails
    
        attributedString = [[NSMutableAttributedString alloc] initWithString:self.notificationMessage.message];
        [attributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Medium" size:14.0f], NSForegroundColorAttributeName : [UIColor whiteColor]} range:NSMakeRange(0, self.notificationMessage.message.length)];
        self.message.attributedText = attributedString;
        
    }
        
    CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(248.0f, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
    CGFloat height = ceilf(rect.size.height);
    self.message.frame = CGRectMake(self.message.frame.origin.x, self.message.frame.origin.y, self.message.frame.size.width, fmaxf(height + 5.0f, 44.0f));
   // self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fmaxf((10.0f + self.message.frame.size.height + 10.0f), 64.0f));
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, UIScreen.mainScreen.bounds.size.width, fmaxf((10.0f + self.message.frame.size.height + 10.0f), 64.0f));
    self.button.frame = self.frame;
    
    // Written by Ashvini
    NSLog(@"This is notification popup frame");
    NSLog(@"%@",self.frame);
}

- (IBAction)tap:(id)sender {
    self.notificationMessage.tapHandler();
}

@end


@interface KPNotificationActionView ()

@property(nonatomic, weak) IBOutlet UIButton *button;
@property(nonatomic, weak) IBOutlet UIImageView *image;
@property(nonatomic, weak) IBOutlet UILabel *message;

@end


@implementation KPNotificationActionView

- (void)layoutSubviews {
    [super layoutSubviews];
    self.image.image = self.notificationMessage.image;
    self.backgroundColor = self.notificationMessage.bgColor;
    self.message.textColor = self.notificationMessage.textColor;

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.notificationMessage.message];
    [attributedString setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Medium" size:14.0f], NSForegroundColorAttributeName : [UIColor whiteColor]} range:NSMakeRange(0, self.notificationMessage.message.length)];
    self.message.attributedText = attributedString;
    CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(212.0f, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
    CGFloat height = ceilf(rect.size.height);
    self.message.frame = CGRectMake(self.message.frame.origin.x, self.message.frame.origin.y, self.message.frame.size.width, fmaxf(height + 5.0f, 37.0f));
  //  self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fmaxf((10.0f + self.message.frame.size.height + 10.0f), 62.0f));
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, UIScreen.mainScreen.bounds.size.width, fmaxf((10.0f + self.message.frame.size.height + 10.0f), 62.0f));
    
    self.button.frame = self.frame;
    
    // Written by Ashvini
    NSLog(@"This is notification popup frame");
    NSLog(@"%@",self.frame);
       
}

- (IBAction)tap:(id)sender {
    self.notificationMessage.tapHandler();
}

- (IBAction)close:(id)sender {
    self.notificationMessage.dismissHandler();
}

@end


@implementation KPNotificationWindow

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *superHitView = [super hitTest:point withEvent:event];
    if (superHitView.tag == NOTIFICATION_VIEW_CLASS) {
        return superHitView;
    } else {
        UIWindow *nextWindow;
        BOOL encounteredBannerWindow = NO;
        for (UIWindow *window in [[UIApplication sharedApplication].windows reverseObjectEnumerator]) {
            if (encounteredBannerWindow && ![window isKindOfClass:[KPNotificationWindow class]]) {
                nextWindow = window;
                break;
            }

            if (!encounteredBannerWindow && [window isKindOfClass:[KPNotificationWindow class]]) {
                encounteredBannerWindow = YES;
            }
        }

        if (nextWindow) {
            return [nextWindow hitTest:point withEvent:event];
        } else {
            return superHitView;
        }
    }
}

@end

@implementation KPNotificationRootViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // TODO: return result of main window's controller
    return YES;
}

@end
