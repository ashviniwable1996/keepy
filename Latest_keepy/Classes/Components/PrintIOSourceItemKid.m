//
//  PrintIOSourceItemKid.m
//  Keepy
//
//  Created by Arik Sosman on 6/24/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import "PrintIOSourceItemKid.h"

#import "KPLocalKid.h"
#import "KPLocalAsset.h"
#import "Keepy-Swift.h"

@implementation PrintIOSourceItemKid

- (BOOL)isImageItem{
    return NO;
}

- (NSString *)uniqueIdentifier {
    
    return [NSString stringWithFormat:@"kid|%lu|%lu", (unsigned long)self.sourceKid.identifier, (unsigned long)self.sourceKid.serverID];
    
}

- (void)fetchImageInPhotoSource:(id<PIOPhotoSource>)photoSource isThumbnail:(BOOL)thumbnail withCompletionHandler:(void (^)(UIImage *))imageFetchCompletionHandler {
    
    KPLocalAsset *imageAsset = [KPLocalAsset fetchByLocalID:self.sourceKid.imageAssetID];
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, imageAsset.urlHash]];
    
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
    
    if(!image){
        image = [GlobalUtils getKidDefaultImage:self.sourceKid.gender];
    }
    
    imageFetchCompletionHandler(image);
    
    // if(image){
    
    // UIImage *maskedImage = [image imageMaskedWithElipse:CGSizeMake(290, 290)];
    // imageCompletionHandler(maskedImage);

}
 

- (NSString *)displayName {
    
    return self.sourceKid.name;
    
}

@end
