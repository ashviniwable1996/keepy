//
//  PrintIOSourceItemMoveUp.h
//  Keepy
//
//  Created by Arik Sosman on 6/24/15.
//  Copyright (c) 2015 Keepy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <PrintIO/PrintIO.h>
#import <PrintIO/PIOPhotoSourceItem.h>
#import <PrintIO/PIODefaultPhotoSourceItem.h>

@interface PrintIOSourceItemMoveUp : PIODefaultPhotoSourceItem <PIOPhotoSourceItem>

@property (strong, nonatomic) NSObject<PIOPhotoSourceItem> *parentItem;

@end
