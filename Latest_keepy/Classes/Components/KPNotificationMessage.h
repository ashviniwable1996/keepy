//
//  KPNotificationMessage.h
//  Keepy
//
//  Created by Troy Payne on 2/4/14.
//  Copyright (c) 2014 Jhaniv LTD. All rights reserved.
//

typedef void (^KPNotificationMessageTapHandlingBlock)();

typedef void (^KPNotificationMessageDismissHandlingBlock)();

@interface KPNotificationMessage : NSObject

@property(nonatomic, copy) NSString *message;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) UIColor *bgColor;
@property(nonatomic, strong) UIColor *textColor;
@property(nonatomic, copy) KPNotificationMessageTapHandlingBlock tapHandler;
@property(nonatomic, copy) KPNotificationMessageDismissHandlingBlock dismissHandler;
@property(nonatomic, assign) NSTimeInterval timeout;

@end
