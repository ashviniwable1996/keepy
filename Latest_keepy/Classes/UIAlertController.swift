//
//  UIAlertController.swift
//  Keepy
//
//  Created by Naoki Hiroshima on 11/2/15.
//  Copyright © 2015 Keepy. All rights reserved.
//

import UIKit

extension UIAlertController {

    class func alert(message: String) -> UIAlertController {
        let ac = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        return ac
    }
    
    class func alert(error: NSError) -> UIAlertController {
        let ac = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        return ac
    }
    
    func present(animated: Bool, completion: (() -> ())?) {
        guard let window = UIApplication.shared.delegate?.window else { return }
        var vc = window?.rootViewController
        while let next = vc?.presentedViewController {
            vc = next
        }
        vc?.present(self, animated: animated, completion: completion)
    }
}
