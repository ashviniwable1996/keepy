//
//  KpyImageProductTemplate.swift
//  Keepy
//
//  Created by Aakash Wadhwa on 07/05/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

import UIKit

class KpyImageProductTemplate: NSObject {
    
     var name: String = "";
     var imageUrl: String = "";
     var isDefault: Bool = false;
     var spaces: Array<KpyImageSpaces> = [];
     var templatesArray = [KpyImageProductTemplate]()
    public override init() {
        super.init();
    }
    
    init(_ jsonObj: AnyObject) {
        super.init();
        self.parseJson(jsonObj);
    }
    
    func parseJson(_ jsonObj: AnyObject){
        if let nameS = jsonObj["Name"] as? String { self.name = nameS; }
        if let imageUrlS = jsonObj["ImageUrl"] as? String { self.imageUrl = imageUrlS; }
        if let isDefaultS = jsonObj["IsDefault"] as? Bool { self.isDefault = isDefaultS; }
        if let spacesObj = jsonObj["Spaces"] as? Array<AnyObject> {
            for spaceJson in spacesObj {
                self.spaces.append(KpyImageSpaces(spaceJson));
            }
        }
    }
}
