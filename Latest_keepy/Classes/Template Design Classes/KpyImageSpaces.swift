//
//  KpyImageSpaces.swift
//  Keepy
//
//  Created by Aakash Wadhwa on 07/05/18.
//  Copyright © 2018 Keepy. All rights reserved.
//

import UIKit

class KpyImageSpaces: NSObject {
    
     var id: String = "";
     var index: Int = -1;
     var finalX1: Int = 0;
     var finalX2: Int = 0;
     var finalY1: Int = 0;
     var finalY2: Int = 0;
     var layers: Array<KpyImageLayer> = [];
    
    init(_ jsonObj: AnyObject) {
        super.init();
        self.parseJson(jsonObj);
    }
    
    func parseJson(_ jsonObj: AnyObject){
        if let idS = jsonObj["Id"] as? String { self.id = idS; }
        if let indexS = jsonObj["Index"] as? Int { self.index = indexS; }
        if let finalX1S = jsonObj["FinalX1"] as? Int { self.finalX1 = finalX1S; }
        if let finalX2S = jsonObj["FinalX2"] as? Int { self.finalX2 = finalX2S; }
        if let finalY1S = jsonObj["FinalY1"] as? Int { self.finalY1 = finalY1S; }
        if let finalY2S = jsonObj["FinalY2"] as? Int { self.finalY2 = finalY2S; }
        if let layersObj = jsonObj["Layers"] as? Array<AnyObject> {
            for layerJson in layersObj {
                self.layers.append(KpyImageLayer(layerJson));
            }
        }
    }
}
