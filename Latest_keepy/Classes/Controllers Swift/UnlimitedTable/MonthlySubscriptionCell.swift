//
//  MonthlySubscriptionCell.swift
//  Keepy
//
//  Created by Mindbowser on 09/01/19.
//  Copyright © 2019 Keepy. All rights reserved.
//

import UIKit

class MonthlySubscriptionCell: UITableViewCell {
    @IBOutlet weak var selectionBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
