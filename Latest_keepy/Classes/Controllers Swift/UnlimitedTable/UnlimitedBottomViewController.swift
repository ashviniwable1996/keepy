//
//  UnlimitedBottomViewController.swift
//  Keepy
//
//  Created by Mindbowser on 09/01/19.
//  Copyright © 2019 Keepy. All rights reserved.
//

import UIKit

class UnlimitedBottomViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var bottomTableview: UITableView!
    var unlimitedVC:UnlimitedViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(loadTableData), name: NSNotification.Name(rawValue: "load"), object: nil)
        self.bottomTableview.dataSource = self
        self.bottomTableview.delegate = self
        self.bottomTableview.reloadData()
    }

    func loadTableData(){
       self.bottomTableview.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            return 45
        case 3:
            return 225
        default:
           return 25
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 1:
            let cell = self.bottomTableview.dequeueReusableCell(withIdentifier: "AnnualSubscriptionCell", for: indexPath) as! AnnualSubscriptionCell
            //cell.backgroundColor = UIColor.green
            return cell
        case 3:
            let cell = self.bottomTableview.dequeueReusableCell(withIdentifier: "SubscriptionInfoCell", for: indexPath) as! SubscriptionInfoCell
           // cell.backgroundColor = UIColor.yellow
            return cell
        default:
            let cell = self.bottomTableview.dequeueReusableCell(withIdentifier: "MonthlySubscriptionCell", for: indexPath) as! MonthlySubscriptionCell
           // cell.backgroundColor = UIColor.purple
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
