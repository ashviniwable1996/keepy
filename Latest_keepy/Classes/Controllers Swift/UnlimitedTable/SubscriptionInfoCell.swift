//
//  SubscriptionInfoCell.swift
//  Keepy
//
//  Created by Mindbowser on 09/01/19.
//  Copyright © 2019 Keepy. All rights reserved.
//

import UIKit

class SubscriptionInfoCell: UITableViewCell {
    @IBOutlet weak var continueSubscrptnBtn: UIButton!
    @IBOutlet weak var subscrptnInfoLbl: UILabel!
    @IBOutlet weak var subscrptionLblHeight: NSLayoutConstraint!
    @IBOutlet weak var bottom3BtnsView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        continueSubscrptnBtn.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
