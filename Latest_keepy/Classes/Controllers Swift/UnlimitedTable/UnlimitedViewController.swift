//
//  UnlimitedViewController.swift
//  Keepy
//
//  Created by Kolchakov Ruslan on 5/29/17.
//  Copyright © 2017 Keepy. All rights reserved.
//

import UIKit
import Mixpanel
import SVProgressHUD
import StoreKit
import AppsFlyerLib
import Crashlytics
import FBSDKCoreKit
import SafariServices
import ActiveLabel
//added by amitg


@objcMembers class UnlimitedViewController: UIViewController {
    enum ActionButtonsPresentation: Int {
        case hide = 0, montlyAndAnnaully1, montlyAndAnnaully2, onlyMonthly, onlyAnnually
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var billedAnually: UILabel!
    @IBOutlet weak var billedPerMonth: UILabel!
    @IBOutlet weak var tryforMonthLbl: UILabel!
    @IBOutlet weak var titleNavigationImageWidth: NSLayoutConstraint!
    @IBOutlet weak var titleNavigationImage: UIImageView!
    @IBOutlet weak var titleNavigationTop: NSLayoutConstraint!
   @IBOutlet weak var navigationCloseBtnTop: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var firstPlanButton: UIButton?
    @IBOutlet weak var secondPlanButton: UIButton?
    @IBOutlet weak var secondButtonTopConstaint: NSLayoutConstraint?
    @IBOutlet weak var subscriptionLabel: ActiveLabel? //UILabel?
    @IBOutlet weak var privacyLabel: UILabel?
    @IBOutlet weak var bottomView: UIView?
    @IBOutlet weak var bottomTblView: UITableView!
    @IBOutlet weak var bottomViewBottomConstaing: NSLayoutConstraint?
    @IBOutlet weak var bottomviewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomviewHeightConstraint: NSLayoutConstraint!
    
    fileprivate var numberOfRows = 5
    
    //Zvika vars for the plans and subscriptions
    fileprivate var plans = [KPPlan]()
    fileprivate var plansInAnyOrder = [KPPlan]()
    fileprivate var purchasedPlan: KPPlan?
    //private var  selectedPlanIndex = 0 zvika implement it as botton tag
    fileprivate var couponApplied = false
    fileprivate var couponError = ""
    fileprivate var couponCode = "no"
    fileprivate var couponSource = ""
    fileprivate var strKidsNames = ""
    fileprivate var billingTermsMessage = ""
    var finalDiscount = ""
    @objc var isFromAnotherVC = false
    
    var isBottomTblView = false
    var sbscrptnLblHeight:CGFloat = 0
    var sbscrptnCellHeight:CGFloat = 0
    var selectedPlanNo:Int = 0
    var allPlansHeight:CGFloat = 0
    var isBottomPopupHide = false
    
//    fileprivate var actionButtonsPresentation: ActionButtonsPresentation = .hide {
//        didSet {
//            firstPlanButton?.tag = 0
//            secondPlanButton?.tag = 1
////            firstPlanButton?.backgroundColor = UIColor.clear
//            firstPlanButton?.setTitleColor(UIColor.white, for: .normal)
//            firstPlanButton?.titleLabel?.textAlignment = .center
//            firstPlanButton?.titleLabel?.numberOfLines = 0
//            secondPlanButton?.backgroundColor = UIColor.white
//            secondPlanButton?.setTitleColor(UIColor.keepyPaywallTopColor(), for: .normal)
//            secondPlanButton?.isHidden = false
//            secondPlanButton?.titleLabel?.textAlignment = .center
//            secondPlanButton?.titleLabel?.numberOfLines = 0
//
//            if(!isFromAnotherVC){
//                tryforMonthLbl.isHidden = true
//                billedPerMonth.isHidden = true
//                billedAnually.isHidden = true
////               let currency = priceStringForProduct(item: plans[0].product)
//                let monthyPlanPrice = (plans[0].product.localizedPrice ?? "") + NSLocalizedString("/month", comment: "")
//                let annuallyPlanPrice =  (plans[1].product.localizedPrice ?? "") + NSLocalizedString("/year", comment: "")
//                switch actionButtonsPresentation {
//                case .hide:
//                    hidePlans()
//                    return
//                case .montlyAndAnnaully1:
//                    secondButtonTopConstaint?.constant = 83
//
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsTwoOptionsTemplate", comment: ""),
//                                                 arguments: [monthyPlanPrice, annuallyPlanPrice])
//                case .montlyAndAnnaully2:
//                    secondButtonTopConstaint?.constant = 83
//
//                    secondPlanButton?.backgroundColor = UIColor.clear
//                    secondPlanButton?.setTitleColor(UIColor.white, for: .normal)
//                    //                firstPlanButton?.backgroundColor = UIColor.white
//                    //                firstPlanButton?.setTitleColor(UIColor.keepyPaywallTopColor(), for: .normal)
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsTwoOptionsTemplate", comment: ""),
//                                                 arguments: [monthyPlanPrice, annuallyPlanPrice])
//                case .onlyMonthly:
//                    secondButtonTopConstaint?.constant = 22
//                    secondPlanButton?.isHidden = true
//
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsTwoOptionsTemplate", comment: ""),
//                                                 arguments: [monthyPlanPrice])
//                case .onlyAnnually:
//                    firstPlanButton?.tag = 1
//                    secondButtonTopConstaint?.constant = 22
//                    secondPlanButton?.isHidden = true
//
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsOneOptionTemplate", comment: ""),
//                                                 arguments: [annuallyPlanPrice])
//                }
//
//                //            let label = ActiveLabel()
//                //            label.numberOfLines = 0
//                //            label.enabledTypes = [.url]
//                //            label.textColor = .white
//
//                //let tap = UITapGestureRecognizer(target: self, action: #selector(UnlimitedViewController.tapFunction))
//                subscriptionLabel?.isUserInteractionEnabled = true
//                //subscriptionLabel?.addGestureRecognizer(tap)
//
//                subscriptionLabel?.textColor = .white
//                subscriptionLabel?.text = billingTermsMessage
//
//                let text = billingTermsMessage
//                let underlineAttriString = NSMutableAttributedString(string: text)
//                //            let range1 = (text as NSString).range(of:"Terms of Use")
//                //            underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
//                //            let range2 = (text as NSString).range(of:"Privacy Policy")
//                //            underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
//                //
//
//                //subscriptionLabel? = ActiveLabel()
//                let customType = ActiveType.custom(pattern: "\\bTerms of Use\\b")
//                let customType2 = ActiveType.custom(pattern: "\\bPrivacy Policy\\b")
//                //subscriptionLabel?.customColor[customType] = UIColor.white
//                //subscriptionLabel?.customColor[customType2] = UIColor.white
//
//                subscriptionLabel?.enabledTypes.append(customType)
//                subscriptionLabel?.enabledTypes.append(customType2)
//
//                subscriptionLabel?.configureLinkAttribute = { (type, attributes, isSelected) in
//                    var atts = attributes
//                    switch type {
//                    case customType:
//                        //                    atts[NSFontAttributeName] = isSelected ? UIFont.boldSystemFont(ofSize: 16) : UIFont.boldSystemFont(ofSize: 14)
//                        atts[NSForegroundColorAttributeName] = UIColor.white
//                        atts[NSUnderlineStyleAttributeName] = 1
//                        break;
//                    case customType2:
//                        //                    atts[NSFontAttributeName] = isSelected ? UIFont.boldSystemFont(ofSize: 16) : UIFont.boldSystemFont(ofSize: 14)
//                        atts[NSForegroundColorAttributeName] = UIColor.white
//                        atts[NSUnderlineStyleAttributeName] = 1
//                        break;
//                    default: ()
//                    }
//
//                    return atts
//                }
//
//
//                subscriptionLabel?.handleCustomTap(for: customType) { element in
//                    print("Custom type tapped: \(element)")
//                    self.tapTerms()
//                }
//                subscriptionLabel?.handleCustomTap(for: customType2) { element in
//                    print("Custom type tapped: \(element)")
//                    self.tapPrivacy()
//                }
//                subscriptionLabel?.attributedText = underlineAttriString
//                if UIScreen.main.bounds.size.width == 320 {
//                    subscriptionLabel?.textAlignment = .center
//
//                } else {
//                    subscriptionLabel?.textAlignment = .justified
//                }
//                //            firstPlanButton?.setTitle(String(format: "TRY FREE FOR 7 DAYS\n%@", monthyPlanPrice), for: .normal);
//                //            secondPlanButton?.setTitle(String(format: "Subscribe annually and save 60%%\n%@", annuallyPlanPrice), for: .normal);
//
////                tryforMonthLbl.text = "TRY FREE FOR 7 DAYS"
//                            let str = NSMutableAttributedString(string: String(format: "TRY FREE FOR 7 DAYS\n%@", monthyPlanPrice))
//                            str.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:15, weight: UIFontWeightRegular), range: NSMakeRange(0, 19))
//                            str.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:10, weight: UIFontWeightRegular), range: NSMakeRange(20, str.length - 20))
//
//                            firstPlanButton?.setAttributedTitle(str, for: .normal)
//
//                let str2 = NSMutableAttributedString(string: String(format: "Subscribe annually and save %@%%\n%@",finalDiscount, annuallyPlanPrice))
//                str2.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:15, weight: UIFontWeightRegular), range: NSMakeRange(0, 31))
//                str2.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:10, weight: UIFontWeightRegular), range: NSMakeRange(32, 5))
//                secondPlanButton?.setAttributedTitle(str2, for: .normal)
//
//            }
//            else{//When user first time signed in
//                let monthlyPriceString = plans[0].product.localizedPrice
////                let stringArray = monthlyPriceString?.components(separatedBy:CharacterSet.whitespaces)
//
//                let monthlyCharges = Double(plans[1].product.price)/12
////                plans[1].product.price = monthlyCharges
//
//                let monthPrice = NSString(format: "%.2f", monthlyCharges)
//
//                let monthyPlanPrice =  (plans[1].product.localizedPrice ?? "") + NSLocalizedString(" billed annually", comment: "")
//                billedPerMonth.text = monthyPlanPrice
//                billedAnually.text = "\(monthlyPriceString!)" + "/m"
//                let annuallyPlanPrice =  (plans[1].product.localizedPrice ?? "") + NSLocalizedString("/year", comment: "")
//                switch actionButtonsPresentation {
//                case .hide:
//                    hidePlans()
//                    return
//                case .montlyAndAnnaully1:
//                    secondButtonTopConstaint?.constant = 83
//
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsTwoOptionsTemplate", comment: ""),
//                                                 arguments: [monthyPlanPrice, annuallyPlanPrice])
//                case .montlyAndAnnaully2:
//                    secondButtonTopConstaint?.constant = 83
//
//                    secondPlanButton?.backgroundColor = UIColor.clear
//                    secondPlanButton?.setTitleColor(UIColor.white, for: .normal)
//                    //                firstPlanButton?.backgroundColor = UIColor.white
//                    //                firstPlanButton?.setTitleColor(UIColor.keepyPaywallTopColor(), for: .normal)
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsTwoOptionsTemplate", comment: ""),
//                                                 arguments: [monthyPlanPrice, annuallyPlanPrice])
//                case .onlyMonthly:
//                    secondButtonTopConstaint?.constant = 22
//                    secondPlanButton?.isHidden = true
//
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsTwoOptionsTemplate", comment: ""),
//                                                 arguments: [monthyPlanPrice])
//                case .onlyAnnually:
//                    firstPlanButton?.tag = 1
//                    secondButtonTopConstaint?.constant = 22
//                    secondPlanButton?.isHidden = true
//
//                    billingTermsMessage = String(format: NSLocalizedString("BillingTermsOneOptionTemplate", comment: ""),
//                                                 arguments: [annuallyPlanPrice])
//                }
//
//                //            let label = ActiveLabel()
//                //            label.numberOfLines = 0
//                //            label.enabledTypes = [.url]
//                //            label.textColor = .white
//
//                //let tap = UITapGestureRecognizer(target: self, action: #selector(UnlimitedViewController.tapFunction))
//                subscriptionLabel?.isUserInteractionEnabled = true
//                //subscriptionLabel?.addGestureRecognizer(tap)
//
//                subscriptionLabel?.textColor = .white
//                subscriptionLabel?.text = billingTermsMessage
//
//                let text = billingTermsMessage
//                let underlineAttriString = NSMutableAttributedString(string: text)
//                //            let range1 = (text as NSString).range(of:"Terms of Use")
//                //            underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
//                //            let range2 = (text as NSString).range(of:"Privacy Policy")
//                //            underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
//                //
//
//                //subscriptionLabel? = ActiveLabel()
//                let customType = ActiveType.custom(pattern: "\\bTerms of Use\\b")
//                let customType2 = ActiveType.custom(pattern: "\\bPrivacy Policy\\b")
//                //subscriptionLabel?.customColor[customType] = UIColor.white
//                //subscriptionLabel?.customColor[customType2] = UIColor.white
//
//                subscriptionLabel?.enabledTypes.append(customType)
//                subscriptionLabel?.enabledTypes.append(customType2)
//
//                subscriptionLabel?.configureLinkAttribute = { (type, attributes, isSelected) in
//                    var atts = attributes
//                    switch type {
//                    case customType:
//                        //                    atts[NSFontAttributeName] = isSelected ? UIFont.boldSystemFont(ofSize: 16) : UIFont.boldSystemFont(ofSize: 14)
//                        atts[NSForegroundColorAttributeName] = UIColor.white
//                        atts[NSUnderlineStyleAttributeName] = 1
//                        break;
//                    case customType2:
//                        //                    atts[NSFontAttributeName] = isSelected ? UIFont.boldSystemFont(ofSize: 16) : UIFont.boldSystemFont(ofSize: 14)
//                        atts[NSForegroundColorAttributeName] = UIColor.white
//                        atts[NSUnderlineStyleAttributeName] = 1
//                        break;
//                    default: ()
//                    }
//
//                    return atts
//                }
//
//
//                subscriptionLabel?.handleCustomTap(for: customType) { element in
//                    print("Custom type tapped: \(element)")
//                    self.tapTerms()
//                }
//                subscriptionLabel?.handleCustomTap(for: customType2) { element in
//                    print("Custom type tapped: \(element)")
//                    self.tapPrivacy()
//                }
//                subscriptionLabel?.attributedText = underlineAttriString
//                if UIScreen.main.bounds.size.width == 320 {
//                    subscriptionLabel?.textAlignment = .center
//
//                } else {
//                    subscriptionLabel?.textAlignment = .justified
//                }
//                //            firstPlanButton?.setTitle(String(format: "TRY FREE FOR 7 DAYS\n%@", monthyPlanPrice), for: .normal);
//                //            secondPlanButton?.setTitle(String(format: "Subscribe annually and save 60%%\n%@", annuallyPlanPrice), for: .normal);
//
//                tryforMonthLbl.text = "TRY FREE FOR 7 DAYS"
//                //            let str = NSMutableAttributedString(string: String(format: "TRY FREE FOR 7 DAYS\n%@", monthyPlanPrice))
//                //            str.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:15, weight: UIFontWeightRegular), range: NSMakeRange(0, 19))
//                //            str.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:10, weight: UIFontWeightRegular), range: NSMakeRange(20, 5))
//                //            firstPlanButton?.setAttributedTitle(str, for: .normal)
//
//                let str2 = NSMutableAttributedString(string: String(format: "Subscribe annually and save %@%%\n%@",finalDiscount, annuallyPlanPrice))
//                str2.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:15, weight: UIFontWeightRegular), range: NSMakeRange(0, 31))
//                str2.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize:10, weight: UIFontWeightRegular), range: NSMakeRange(32, 5))
//                secondPlanButton?.setAttributedTitle(str2, for: .normal)
//
//            }
//
//
//
//
//
//
//           // Ashvini commented this
//          //  showPlans()
//        }
//    }
    
    //MARK: - current currency
    func priceStringForProduct(item: SKProduct) -> String? {
        let price = item.price
        if price == NSDecimalNumber(decimal: 0.00) {
            return "GET" //or whatever you like really... maybe 'Free'
        } else {
            let numberFormatter = NumberFormatter()
            let locale = item.priceLocale
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = locale
            return numberFormatter.string(from: price)
        }
    }

    // StoreKitMaanager - free trial period
    func unitName(unitRawValue:UInt) -> String {
        switch unitRawValue {
        case 0: return "days"
        case 1: return "weeks"
        case 2: return "months"
        case 3: return "years"
        default: return ""
        }
    }
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getKidsnames()
        getPlans()
        
        self.isBottomTblView = false
        self.selectedPlanNo = 0
        
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.estimatedRowHeight = 100
        tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 35, right: 0)
        
        secondPlanButton?.layer.cornerRadius = 9
        secondPlanButton?.layer.borderWidth = 1.5
        secondPlanButton?.layer.borderColor = UIColor.white.cgColor
        
        firstPlanButton?.layer.cornerRadius = 9
        firstPlanButton?.layer.borderWidth = 1.5
        firstPlanButton?.layer.borderColor = UIColor.white.cgColor
        
        navigationController?.isNavigationBarHidden = true
        
        AppsFlyerTracker.shared().trackEvent("my-keepies-started-checkout", withValues: [:])
        Answers.logCustomEvent(withName: "my-keepies-started-checkout", customAttributes: [:])
        FBSDKAppEvents.logEvent("my-keepies-started-checkout")
        
        if self.view.frame.size.height >= 812  {
           self.titleNavigationTop.constant = 17
           self.navigationCloseBtnTop.constant = 17
        }
        
        if isFromAnotherVC{
            titleNavigationImageWidth.constant = 180
           // self.titleNavigationImage.image = UIImage.init(named: "try keepy unlimited")
            self.titleNavigationImage.image = UIImage.init(named: "keepy unlimited")
        } else {
            titleNavigationImageWidth.constant = 150
            self.titleNavigationImage.image = UIImage.init(named: "keepy unlimited")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.isBottomTblView = false
        NotificationCenter.default.addObserver(self, selector: #selector(UnlimitedViewController.transactionUpdatedNotification(_:)), name: NSNotification.Name(rawValue: "TransactionUpdated"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
       super.viewDidAppear(animated)
    }
    
    func showBottomPopup() {
        self.bottomTblView.dataSource = self
        self.bottomTblView.delegate = self
        self.isBottomTblView = true
        let sbscrptnLbl = UILabel(frame: CGRect(x: 0, y: 0, width: (self.view.frame.width - 12), height: 0))
        sbscrptnLbl.textAlignment = .center
        sbscrptnLbl.lineBreakMode = .byWordWrapping
        sbscrptnLbl.numberOfLines = 0
        sbscrptnLbl.font = UIFont.systemFont(ofSize: 10.0)
        let text = "Payment will be charged to your iTunes account at confirmation of purchase. Subscriptions will automatically renew unless auto-renew is turned off at least 24 hours before the end of current period. Your account will be charged according to your plan for renewal within 24 hours prior to the end of the current period. You can manage or turn off auto-renew in your Apple ID account settings at any time after purchase."
        sbscrptnLbl.text = text
        sbscrptnLbl.sizeToFit()
        self.sbscrptnLblHeight = sbscrptnLbl.frame.size.height
        if self.view.frame.size.height >= 812  {
            self.sbscrptnLblHeight += 15
            self.sbscrptnCellHeight = 100 + self.sbscrptnLblHeight
        } else {
            self.sbscrptnLblHeight += 10
            self.sbscrptnCellHeight = 83 + self.sbscrptnLblHeight
        }
        
        // calculating required height to show all plans
        let yearlyplan = self.plans.filter { $0.sku == "me.keepy.orange.yearly1" }
        let yearlyPromoplan = self.plans.filter { $0.sku == "me.keepy.orange.yearly1.promo" }
        if (yearlyplan.count > 0) || (yearlyPromoplan.count > 0) {
            self.allPlansHeight = CGFloat(((self.plans.count - 1) * 60) + 70)
        } else {
            self.allPlansHeight = CGFloat(self.plans.count * 60)
        }
        
        UIView.animate(withDuration: 0.5, animations: {
//            self.bottomviewTopConstraint.constant = -(CGFloat((self.plans.count * 60)) + self.sbscrptnCellHeight)
//            self.bottomviewHeightConstraint.constant = CGFloat(self.plans.count * 60) + self.sbscrptnCellHeight
            self.bottomviewTopConstraint.constant = -(self.allPlansHeight + self.sbscrptnCellHeight)
            self.bottomviewHeightConstraint.constant = self.allPlansHeight + self.sbscrptnCellHeight
            self.bottomTblView.reloadData()
            self.view.layoutIfNeeded()
        })
    }
    
    func hideBottomPopup() {
        UIView.animate(withDuration: 0.5, animations: {
//            self.bottomviewTopConstraint.constant = -(CGFloat((self.plans.count * 60)) + 63)
//            self.bottomviewHeightConstraint.constant = CGFloat(self.plans.count * 60) + 63
            if self.view.frame.size.height >= 812  {
                self.bottomviewTopConstraint.constant = -(self.allPlansHeight + 71)
                self.bottomviewHeightConstraint.constant = self.allPlansHeight + 71
               
                if self.isBottomPopupHide == true {
                    let subscrptnCell = self.bottomTblView.cellForRow(at: IndexPath(row: self.plans.count, section: 0)) as! SubscriptionInfoCell
                    subscrptnCell.subscrptnInfoLbl.isHidden = true
                }
                
            } else {
                self.bottomviewTopConstraint.constant = -(self.allPlansHeight + 63)
                self.bottomviewHeightConstraint.constant = self.allPlansHeight + 63
            }
            self.view.layoutIfNeeded()
        })
    }
    
    
    @IBAction func privacyBtnTap(_ sender: Any) {
        print("tap working1")
        let url = URL(string: "http://keepy.me/privacy.html")
        let vc = SFSafariViewController(url: url!)
        present(vc, animated: true, completion: nil)
        //http://keepy.me/terms.html
    }
    
    
    @IBAction func termsOfUseBtnTap(_ sender: Any) {
        print("tap working3")
        let url = URL(string: "http://keepy.me/terms.html")
        let vc = SFSafariViewController(url: url!)
        present(vc, animated: true, completion: nil)
        //http://keepy.me/terms.html
    }
    
    @IBAction func restorePurchaseBtnTap(_ sender: Any) {
        Mixpanel.sharedInstance()!.track("receipt-debugging-pressed-restore-button", properties: [:])
        SVProgressHUD.show(withStatus: "verifying")
        StoreKitManager.sharedManager.restorePurchases { (response, error) in
            if error != nil {
                SVProgressHUD.showError(withStatus: error?.localizedDescription)
            } else {
                SVProgressHUD.dismiss()
                let ac = UIAlertController.alert(message: "your previous purchases have been successfully restored")
                self.present(ac, animated: true)
                DispatchQueue.main.async(execute: {
                    NSManagedObjectContext.mr_contextForCurrentThread().mr_saveOnlySelfAndWait()
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func tapPrivacy() {
        print("tap working1")
        let url = URL(string: "http://keepy.me/privacy.html")
        let vc = SFSafariViewController(url: url!)
        present(vc, animated: true, completion: nil)
        
        //http://keepy.me/terms.html
    }
    
    @objc func tapTerms() {
        print("tap working3")
        let url = URL(string: "http://keepy.me/terms.html")
        let vc = SFSafariViewController(url: url!)
        present(vc, animated: true, completion: nil)
        
        //http://keepy.me/terms.html
        
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
      
        print("tap working")
        let url = URL(string: "http://keepy.me/privacy.html")
        let vc = SFSafariViewController(url: url!)
        present(vc, animated: true, completion: nil)
        
        //http://keepy.me/terms.html

    }
    
    
    fileprivate var isOnTheTop: Bool = true {
        didSet {
            if oldValue != isOnTheTop {
                 // Ashvini commented this
              //  showPlans()
            }
        }
    }
    
    fileprivate var isOnTheBottom: Bool = false {
        didSet {
            if oldValue != isOnTheBottom, isOnTheBottom {
                 // Ashvini commented this
               // showPlans()
            }
        }
    }
    
    fileprivate var isAutomaticScroll: Bool = false
    
    
  
    
    
    
    // MARK: - Actions
    
    @IBAction func dissmiss(_ sender: UIButton) {
        print("dissmissUnlimitedTableView")
        dismiss(animated: true) {}
    }
    
    @IBAction func requestPayment(_ sender: UIButton) {
        var plan : KPPlan
        
        // // Commented by Ashvini
//        if(!isFromAnotherVC){
//         plan = self.plans[sender.tag]
//        }
//        else{
//         plan = self.plans[1]
//        }
        
        // // Added by Ashvini
        plan = self.plans[self.selectedPlanNo]
        
        self.purchasedPlan = plan
        
        if (plan.product == nil) {
            return
        }
        
        let type = self.simplifiedSKUStringForPlan(plan)
        let period = plan.title ?? ""
        let coupon = self.couponCode ?? ""
        let props = ["plan-type" : type,
                     "plan-period" : period,
                     "coupon-code" : coupon]
        AppsFlyerTracker.shared().trackEvent("my-keepies-choosed-plan-\(plan.sku ?? "")", withValues: [:])
        Answers.logCustomEvent(withName: "my-keepies-choosed-plan-\(plan.sku ?? "")", customAttributes: [:])
        Mixpanel.sharedInstance()?.track("my-keepies-choose-plan", properties: props)
        FBSDKAppEvents.logEvent("my-keepies-choosed-plan-\(plan.sku ?? "")")
        
        SVProgressHUD.show(withStatus: NSLocalizedString("loading", comment: ""))
        
        StoreKitManager.sharedManager.buy(plan.product, planType: type, couponCode: coupon) { (response, error) in
            if (error != nil)
            {
                if ((error as? NSError)?.code == SKError.Code.paymentCancelled.rawValue){
                    SVProgressHUD.dismiss()             
                    AppsFlyerTracker.shared().trackEvent("my-keepies-canceled", withValues: [:])
                    Answers.logCustomEvent(withName: "my-keepies-canceled", customAttributes: [:])
                    FBSDKAppEvents.logEvent("my-keepies-canceled")
                } else {
                    SVProgressHUD.showError(withStatus: error!.localizedDescription)
                }
            }
            else
            {
                SVProgressHUD.dismiss()
                let alertController = UIAlertController(title:nil,
                                                        message: NSLocalizedString("thank you!", comment: "")
                    , preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .cancel) { [weak self] _ in
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
                
                Mixpanel.sharedInstance()?.track("my-keepies-purchase", properties: nil)
                
                DispatchQueue.main.async(execute: {
                    NSManagedObjectContext.mr_contextForCurrentThread().mr_saveOnlySelfAndWait()
                })
                
            }
        }
    }
    
    @IBAction func showSubscriptionDetails(_ sender: UIButton) {
        let title = NSLocalizedString("BillingTermsTitle", comment: "")
        let alertController = UIAlertController(title: title, message: billingTermsMessage, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Close", comment: ""), style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    // MARK: - Notification
  
    func transactionUpdatedNotification(_ notification: Notification) {
        guard let paymentTransaction = notification.userInfo?["transaction"] as? SKPaymentTransaction else {
            return
        }
        let transactionState = paymentTransaction.transactionState
        print("transactionStateValue OnUnlimitedVC: \(transactionState.rawValue)")
        
        if paymentTransaction.transactionState == .purchased {
            print("transactionStateValue OnUnlimitedVC: PURCHASED")

            if let purchasedPlan = self.purchasedPlan {
                UserDefaults.standard.set(true, forKey: "PurchaseHasBeenMade")
                
                let values: [String: Any] = [AFEventParamContentId: purchasedPlan.sku ?? "",
                                             AFEventParamContentType: purchasedPlan.product.localizedTitle,
                                             AFEventParamRevenue: purchasedPlan.product.price.intValue,
                                             AFEventParamCurrency: purchasedPlan.product.priceLocale.currencyCode ?? ""]
                //AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: values)
            }
            Answers.logCustomEvent(withName: "my-keepies-purchase-made-\(paymentTransaction.payment.productIdentifier)", customAttributes: [:])
            FBSDKAppEvents.logEvent("my-keepies-purchase-made-\(paymentTransaction.payment.productIdentifier)")
            dismiss(animated: true, completion: nil)
        } else if paymentTransaction.transactionState == .restored {
            dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Private functions
    
//    fileprivate func showPlans() {
//        guard let subscriptionLabel = subscriptionLabel, actionButtonsPresentation != .hide else {
//            return
//        }
//
//        subscriptionLabel.sizeToFit()
//
//        var newConstaint = -(subscriptionLabel.frame.height - 9)
//        if isFromAnotherVC {
//            newConstaint = -(subscriptionLabel.frame.height - 20)
//        }
//        if isOnTheTop || isOnTheBottom {
//            newConstaint = 0
//        }
//
//        if bottomViewBottomConstaing?.constant == newConstaint {
//            return
//        }
//
//        bottomViewBottomConstaing?.constant = newConstaint
//
//        tableView!.delegate = nil
//
//        UIView.animate(withDuration: 0.5, animations: {
//            self.view.layoutIfNeeded()
//        }) { _ in
//            self.tableView!.delegate = self
//        }
  //  }

    fileprivate func hidePlans() {
        self.view.layoutIfNeeded()
        guard let bottomView = bottomView else {
            return
        }
        
        if bottomViewBottomConstaing?.constant == -bottomView.frame.height {
            return
        }
        
        bottomViewBottomConstaing?.constant = -bottomView.frame.height
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    fileprivate func getKidsnames() {
        ServerComm.instance().getKids(true, withSuccessBlock: { (result) in
            //  print("getKids=\(result)")
            
            let resultDic = result as! NSDictionary
            let kids = resultDic["result"] as! NSArray
            let nameArray: Array = kids.map({($0 as AnyObject).value(forKey: "name")}) as Array
            //      print("nameArray=\(nameArray)")
            for i in 0..<nameArray.count {
                
                if i == 0 {
                    self.strKidsNames = "\(nameArray[i]!)"
                }
                else if (i == nameArray.count-1)
                {
                    self.strKidsNames = self.strKidsNames + " and \(nameArray[i]!)."
                }
                else{
                    self.strKidsNames = self.strKidsNames + ", \(nameArray[i]!)"
                }
                
            }
            //print("self.strKidsNames=\(self.strKidsNames)")
            
            self.tableView?.reloadData()
            
        }) { (error) in
            print(error?.localizedDescription)
        }
    }

    
    fileprivate func getPlans(){
        self.couponError = ""
        //     SVProgressHUD.showWithStatus(NSLocalizedString("loading:", comment: ""))
        KPPlan.getPlansForCouponCode(self.couponCode) { (plansFromServer, error) in
            
            //print("111plansFromServer = \(plansFromServer) error = \(error)")
            
            guard let plansFromServer = plansFromServer, error == nil else
            {
                // SVProgressHUD.dismiss()
                let alert = UIAlertController(title: nil, message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .cancel) { (action:UIAlertAction!) in
                }
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion:nil)
                return
            }
            
            if(self.couponCode != "no")
            {
                self.couponApplied = plansFromServer.count > 0
                if(!self.couponApplied){
                    self.couponError = error?.localizedDescription ?? ""
                    
                }
                
                var planType = "orange"
                if(plansFromServer.count > 0){
                    let plan = plansFromServer[0]
                    planType = self.simplifiedSKUStringForPlan(plan)
                }
                
                var validCoupon = "false"
                if((self.couponCode != "no") && self.couponApplied){validCoupon = "true"}
                
                let properties = ["plan-type": planType,"coupon-code": self.couponCode,"coupon-source":self.couponSource,"coupon-valid": validCoupon]
                Mixpanel.sharedInstance()?.track("my-keepies-coupon-entered", properties: properties)
                
            }
            
            let predicate = NSPredicate(format: "SELF.layoutId != 0")
            
            let filteredArray = plansFromServer.filter{predicate.evaluate(with: $0)}
            
            if (plansFromServer.count > filteredArray.count){
                UserManager.sharedInstance().refreshMyData()
            }
            
             // // latest commented by Ashvini while implementing new popup design
            //self.plans = filteredArray
            //let skus = self.plans.map({$0.value(forKey: "sku")})
            let skus = filteredArray.map({$0.value(forKey: "sku")})
            let props = ["skus": "\(skus)"]
            Mixpanel.sharedInstance()?.track("receipt-debugging-loaded-skus", properties:props)
            
            let tempPlans = plansFromServer
            
            // self.plans = tempPlans
            
            
          //  let skusSet: Set = [tempPlans[0].sku,tempPlans[1].sku]
            
            if(tempPlans.count > 1) {
                var skusSet:Set<String> = []
                for currentplan in tempPlans {
                   // skusSet = [currentplan.sku]
                    skusSet.insert(currentplan.sku)
                }
                
              //  let skusSet: Set = [tempPlans[0].sku,tempPlans[1].sku]
                StoreKitManager.sharedManager.productsWithIdentifiers(skusSet as! Set<String>, completion: { (response, error) in
                    //  SVProgressHUD.dismiss()
                    if(error != nil){
                        let alertError = UIAlertController.alert(message: (error?.localizedDescription)!)
                        self.present(alertError, animated: true, completion:nil)
                    }
                    else
                    {
                            // by Ashvini - stored all plans from response
                            for plan in tempPlans {
                                //let currentplan = plan
                              //  currentplan.product = response?.product(withIdentifier: plan.sku)
                              //  self.plans.append(currentplan)
                                
                                let currentplan = plan
                                currentplan.product = response?.product(withIdentifier: plan.sku)
                                switch plan.sku {
                                    case "me.keepy.orange.monthly", "me.keepy.orange.life.1":
                                          // self.plans.append(currentplan)
                                           self.plansInAnyOrder.append(currentplan)
                                    case "me.keepy.orange.yearly1":
                                        if(!self.isFromAnotherVC){
                                           // self.plans.append(currentplan)
                                            self.plansInAnyOrder.append(currentplan)
                                        } else {
                                            // When user first time signed in
                                            // temperory purpose only
                                            self.plansInAnyOrder.append(currentplan)
                                         }
                                    case "me.keepy.orange.yearly1.promo":
                                        if(!self.isFromAnotherVC){
                                        } else {
                                          //  // When user first time signed in
                                         //   self.plans.append(currentplan)
                                            self.plansInAnyOrder.append(currentplan)
                                        }
                                    default:
                                        return
                                }
                            }
                        
                
                        // filtering all plans for order in sequence
                        let monthlyplan = self.plansInAnyOrder.filter { $0.sku == "me.keepy.orange.monthly" }
                        let yearlyplan = self.plansInAnyOrder.filter { $0.sku == "me.keepy.orange.yearly1" }
                        let yearlyPromoplan = self.plansInAnyOrder.filter { $0.sku == "me.keepy.orange.yearly1.promo" }
                        let onetimeplan = self.plansInAnyOrder.filter { $0.sku == "me.keepy.orange.life.1" }
                        
                        // Adding plans in sequence
                        if monthlyplan.count > 0 {
                            for plan in monthlyplan {
                               self.plans.append(plan)
                            }
                        }
                        if yearlyplan.count > 0 {
                            for plan in yearlyplan {
                                self.plans.append(plan)
                            }
                        }
                        if yearlyPromoplan.count > 0 {
                            for plan in yearlyPromoplan {
                                self.plans.append(plan)
                            }
                        }
                        if onetimeplan.count > 0 {
                            for plan in onetimeplan {
                                self.plans.append(plan)
                            }
                        }
                        
                        
                        // // calculate final discount
                        //  if (skusSet.contains("me.keepy.orange.monthly") && skusSet.contains("me.keepy.orange.yearly1")) {
                        if (self.plans.count >= 2) {
                            let monthlyPrice: Double = response?.product(withIdentifier: self.plans[0].sku)?.price.doubleValue ?? 0.0
                            let yearlyPrice: Double = response?.product(withIdentifier: self.plans[1].sku)?.price.doubleValue ?? 0.0
                            if monthlyPrice != 0 && yearlyPrice != 0 {
                                let yearlyDiscount = Int(ceil((1.0 - yearlyPrice / (monthlyPrice * 12.0)) * 10)) * 10
                            self.finalDiscount = "\(yearlyDiscount)"
                            self.selectedPlanNo = 1
                         }
                       }
                            // Aakash
                            let dataOfPlans = NSMutableArray()
                            for purchasedPlan in self.plans {
                                let values: [String: Any] = [AFEventParamContentId: purchasedPlan.sku ?? "",
                                                             AFEventParamContentType: purchasedPlan.product.localizedTitle,
                                                             AFEventParamRevenue: purchasedPlan.product.price.intValue,
                                                             AFEventParamCurrency: purchasedPlan.product.priceLocale.currencyCode ?? ""]
                                dataOfPlans.add(values)
                            }
                            
                            UserDefaults.standard.set(dataOfPlans, forKey: "PlansObject")
                            UserDefaults.standard.synchronize()
                            
                            // by Ashvini - show popup
                        if self.plans.count > 0 {
                            self.showBottomPopup()
                        }
                        
                            // commented by Ashvini
                            // self.displayPromotionView();
                            
                            
                            if(response!.invalidProductIdentifiers.count > 0)
                            {
                                print("\(response!.invalidProductIdentifiers)")
                            }
                       // }
                    }
                })
                
            }
            
        }
    }
    
    fileprivate func simplifiedSKUStringForPlan(_ plan: KPPlan) -> String{
        var planType = "orange"
        let componets = plan.sku.components(separatedBy: ".")
        if(componets.count > 2)
        {
            planType = componets[2]
        }
        return planType
    }
    
    
    
//    fileprivate func displayPromotionView() {
//        if !isFromAnotherVC {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.reloadTweaks()
//        guard let tweakValue: Int = appDelegate.tweakValues[TweakPaywallActionButtonsPresentationTypeKey] as? Int else {
//            actionButtonsPresentation = .montlyAndAnnaully1
////            actionButtonsPresentation = .onlyAnnually
//            return
//        }
//        actionButtonsPresentation = ActionButtonsPresentation(rawValue: tweakValue) ?? .montlyAndAnnaully2//montlyAndAnnaully1
//        } else {
////            actionButtonsPresentation = ActionButtonsPresentation.onlyMonthly
////            isOnTheTop = false
////            isOnTheBottom = false
//
//
//            actionButtonsPresentation = ActionButtonsPresentation.onlyAnnually
//            isOnTheTop = false
//            isOnTheBottom = false
//
////            actionButtonsPresentation = ActionButtonsPresentation(rawValue: .onlyMonthly) ?? .montlyAndAnnaully1
//        }
//    }
    
}

// MARK: - UITableViewDelegete
extension UnlimitedViewController: UITableViewDelegate {
  
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
     //  if self.isBottomTblView == true {
//            self.hideBottomPopup()
//        }
        
    if self.isBottomTblView == true {
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y

        isOnTheBottom = (scrollContentSizeHeight - scrollOffset - scrollViewHeight < 10)
        isOnTheTop = scrollOffset < 10
        
        if isOnTheBottom {
            self.isBottomPopupHide = true
            self.hideBottomPopup()
        }
        if isOnTheTop {
            self.isBottomPopupHide = false
            self.showBottomPopup()
        }
    }
        
        // // Commented by Ashvini - old code
//        let scrollViewHeight = scrollView.frame.size.height
//        let scrollContentSizeHeight = scrollView.contentSize.height
//        let scrollOffset = scrollView.contentOffset.y
//
//        isOnTheBottom = (scrollContentSizeHeight - scrollOffset - scrollViewHeight < 10)
//        isOnTheTop = scrollOffset < 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //if self.isBottomTblView == true {
        if tableView.tag == 100 {
//            if (indexPath.row == 0) || (indexPath.row == 1) || (indexPath.row == 2) {
//                self.selectedPlanNo = indexPath.row
//                self.bottomTblView.reloadData()
//            }
            if (indexPath.row == self.plans.count) {
                // nothing
            } else {
                self.selectedPlanNo = indexPath.row
                self.bottomTblView.reloadData()
            }
        }
    }
    
//    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        isAutomaticScroll = false
//    }
}

// MARK: - UITableViewDataSource

extension UnlimitedViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
       
      //  if self.isBottomTblView == true {
        if tableView.tag == 100 {
            //return 4
            return self.plans.count + 1
        } else {
             return numberOfRows
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // if self.isBottomTblView == true {
        if tableView.tag == 100 {
//            switch indexPath.row {
//            case 0:
//                return 60
//            case 1:
//                 return 60
//            case 2:
//                return 60
//            case 3:
//                return self.sbscrptnCellHeight
//            default:
//                return 0
//            }
            
            if (indexPath.row == self.plans.count) {
                return self.sbscrptnCellHeight
            } else {
                switch self.plans[indexPath.row].sku {
                case "me.keepy.orange.monthly":
                    return 60
                case "me.keepy.orange.yearly1":
                   // return 60
                    return 70
                case "me.keepy.orange.life.1":
                    return 60
                case "me.keepy.orange.yearly1.promo":
                   // return 60
                    return 70
                default:
                    return 0
                }
                
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      //  if self.isBottomTblView == true {
        if tableView.tag == 100 {
            if indexPath.row == self.plans.count {
                let cell = self.bottomTblView.dequeueReusableCell(withIdentifier: "SubscriptionInfoCell", for: indexPath) as! SubscriptionInfoCell
                cell.selectionStyle = .none
                
                if self.isBottomPopupHide == true {
                    cell.subscrptnInfoLbl.isHidden = true
                } else {
                    cell.subscrptnInfoLbl.isHidden = false
                }
                
                cell.subscrptnInfoLbl.textColor = UIColor.darkGray.withAlphaComponent(0.7)
               // cell.subscrptionLblHeight.constant = self.sbscrptnCellHeight
                cell.subscrptionLblHeight.constant = self.sbscrptnLblHeight
                self.view.layoutIfNeeded()
                cell.continueSubscrptnBtn.titleLabel?.textAlignment = .center
                if self.selectedPlanNo == 2 {
                    cell.continueSubscrptnBtn.setAttributedTitle(NSAttributedString(string: "Continue", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15.0),                                                                                                                     NSForegroundColorAttributeName : UIColor.white]), for: .normal)
                } else {
                    cell.continueSubscrptnBtn.titleLabel?.numberOfLines = 2
                    // getting free trial period
                    let selectedPlan = self.plans[self.selectedPlanNo]
                    var freetrialStr:String = ""
                    if #available(iOS 11.2, *) {
                        let period = selectedPlan.product.introductoryPrice!.subscriptionPeriod
                        let unit:Int = Int(period.unit.rawValue)
                        let numofunits:Int = period.numberOfUnits
                        if unit == 1 {
                            freetrialStr = "Get \(numofunits * 7) Days Free!"
                        } else {
                            freetrialStr = "Get \(period.numberOfUnits) \(self.unitName(unitRawValue: period.unit.rawValue)) Free!"
                        }
                    } else {
                        freetrialStr = "Get 7 Days Free!"
                    }
                    
                  //  let buttonText: NSString = "Continue\nGet 1 Month Free!"
                    let buttonText: NSString = "Continue\n\(freetrialStr)" as NSString
                    let newlineRange: NSRange = buttonText.range(of: "\n")
                    var substring1: NSString = ""
                    var substring2: NSString = ""
                    if(newlineRange.location != NSNotFound) {
                        substring1 = buttonText.substring(to: newlineRange.location) as NSString
                        substring2 = buttonText.substring(from: newlineRange.location) as NSString
                    }
                    let attributes1: [String:AnyObject] =
                        [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15.0),
                         NSForegroundColorAttributeName : UIColor.white]
                    let attr1str = NSAttributedString(string: substring1 as String, attributes: attributes1)
                    let attributes2: [String:AnyObject] =
                        [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 11.0),
                         NSForegroundColorAttributeName : UIColor.white.withAlphaComponent(0.8)]
                    let attr2str = NSAttributedString(string: substring2 as String, attributes: attributes2)
                    let attrString = NSMutableAttributedString(attributedString: attr1str)
                    attrString.append(attr2str)
                    cell.continueSubscrptnBtn.setAttributedTitle(attrString, for: .normal)
                }
                return cell
            } else {
                switch self.plans[indexPath.row].product.productIdentifier {
               // switch indexPath.row {
                //case 0:
                case "me.keepy.orange.monthly":
                    let cell = self.bottomTblView.dequeueReusableCell(withIdentifier: "MonthlySubscriptionCell", for: indexPath) as! MonthlySubscriptionCell
                    let permonthPrice:String = self.plans[indexPath.row].product.localizedPrice!
                    let monthPrice:NSString = NSString(format: "%.2f", (Double(self.plans[indexPath.row].product.price)))
                    let firstCharIndex = permonthPrice.index(permonthPrice.startIndex, offsetBy: 1)
                    var monthlyPrice:NSString = permonthPrice.substring(to: firstCharIndex) as NSString
                    monthlyPrice = "\(monthlyPrice)\(monthPrice)" as NSString
                    if indexPath.row == self.selectedPlanNo {
                        cell.titleLabel.attributedText = NSAttributedString(string: "1 Month: \(monthlyPrice)/mo", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0),                                                                                    NSForegroundColorAttributeName : UIColor.black])
                        cell.selectionBtn.setImage(UIImage(named: "selectednew"), for: .normal)
                        cell.backGView.layer.cornerRadius = 9
                        cell.backGView.backgroundColor = UIColor.init(red: 224/255, green: 222/255, blue: 227/255, alpha: 1)
                    } else {
                        cell.titleLabel.attributedText = NSAttributedString(string: "1 Month: \(monthlyPrice)/mo", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0),                                                                                  NSForegroundColorAttributeName: UIColor.darkGray.withAlphaComponent(0.7)])
                        cell.selectionBtn.setImage(UIImage(named: "unselectednew"), for: .normal)
                        cell.backGView.backgroundColor = UIColor.clear
                    }
                    return cell
               // case 1:
                case "me.keepy.orange.yearly1", "me.keepy.orange.yearly1.promo":
                    let cell = self.bottomTblView.dequeueReusableCell(withIdentifier: "AnnualSubscriptionCell", for: indexPath) as! AnnualSubscriptionCell
                    let yearlyTotalPrice:String = self.plans[indexPath.row].product.localizedPrice!
                    let monthlyCharges:Double = Double(self.plans[indexPath.row].product.price)/12
                    let monthPrice:NSString = NSString(format: "%.2f", monthlyCharges)
                    let firstCharIndex = yearlyTotalPrice.index(yearlyTotalPrice.startIndex, offsetBy: 1)
                    let currencyChar:NSString = yearlyTotalPrice.substring(to: firstCharIndex) as NSString
                    let permonthPrice:NSString = "\(currencyChar)\(monthPrice)" as NSString
                    let yearlyCharge:NSString = NSString(format: "%.2f", (Double(self.plans[indexPath.row].product.price)))
                    let yearlyTotalCharge:NSString = "\(currencyChar)\(yearlyCharge)" as NSString
                    if indexPath.row == self.selectedPlanNo {
                        cell.titleLbl.attributedText = NSAttributedString(string: "12 Months: \(permonthPrice)/mo", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0),                                                                                    NSForegroundColorAttributeName : UIColor.black])
                        cell.subTitleLbl.attributedText = NSAttributedString(string: "Save \(self.finalDiscount)% per year!", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 12.0),                                                                                    NSForegroundColorAttributeName : UIColor.darkGray.withAlphaComponent(0.7)])
                        cell.totalValueLbl.attributedText = NSAttributedString(string: yearlyTotalCharge as String, attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0),                                                                                    NSForegroundColorAttributeName : UIColor.black])
                        cell.subTitleLbl.isHidden = false
                        cell.subscriptioTotalView.isHidden = false
                        cell.verticalLine.isHidden = false
                        cell.titleLblTopConstraint.constant = 7
                        self.view.layoutIfNeeded()
                        cell.selectionBtn.setImage(UIImage(named: "selectednew"), for: .normal)
                        cell.backGView.layer.cornerRadius = 9
                        cell.backGView.backgroundColor = UIColor.init(red: 224/255, green: 222/255, blue: 227/255, alpha: 1)
                    } else {
                        cell.titleLbl.attributedText = NSAttributedString(string: "12 Months: \(permonthPrice)/mo", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0),                                                                                    NSForegroundColorAttributeName : UIColor.darkGray.withAlphaComponent(0.7)])
                        cell.subTitleLbl.isHidden = true
                        cell.subscriptioTotalView.isHidden = true
                        cell.verticalLine.isHidden = true
                        cell.titleLblTopConstraint.constant = 21
                        self.view.layoutIfNeeded()
                        cell.selectionBtn.setImage(UIImage(named: "unselectednew"), for: .normal)
                        cell.backGView.backgroundColor = UIColor.clear
                    }
                    return cell
              //  case 2:
                case "me.keepy.orange.life.1":
                    let cell = self.bottomTblView.dequeueReusableCell(withIdentifier: "MonthlySubscriptionCell", for: indexPath) as! MonthlySubscriptionCell
                    let onetimePrice:String = self.plans[indexPath.row].product.localizedPrice!
                    let onetimeCharge:NSString = NSString(format: "%.2f", (Double(self.plans[indexPath.row].product.price)))
                    let firstCharIndex = onetimePrice.index(onetimePrice.startIndex, offsetBy: 1)
                    var onetimePurchasePrice:NSString = onetimePrice.substring(to: firstCharIndex) as NSString
                    onetimePurchasePrice = "\(onetimePurchasePrice)\(onetimeCharge)" as NSString
                    if indexPath.row == self.selectedPlanNo {
                        cell.titleLabel.attributedText = NSAttributedString(string: "One-Time Purchase:\(onetimePurchasePrice)", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0),                                                                                                            NSForegroundColorAttributeName : UIColor.black])
                        cell.selectionBtn.setImage(UIImage(named: "selectednew"), for: .normal)
                        cell.backGView.layer.cornerRadius = 9
                        cell.backGView.backgroundColor = UIColor.init(red: 224/255, green: 222/255, blue: 227/255, alpha: 1)
                    } else {
                        cell.titleLabel.attributedText = NSAttributedString(string: "One-Time Purchase:\(onetimePurchasePrice)", attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0),                                                                                                            NSForegroundColorAttributeName : UIColor.darkGray.withAlphaComponent(0.7)])
                        cell.selectionBtn.setImage(UIImage(named: "unselectednew"), for: .normal)
                        cell.backGView.backgroundColor = UIColor.clear
                    }
                    return cell
                default:
                    let cell = self.bottomTblView.dequeueReusableCell(withIdentifier: "MonthlySubscriptionCell", for: indexPath) as! MonthlySubscriptionCell
                    cell.selectionBtn.isHidden = true
                    cell.titleLabel.isHidden = true
                    cell.backGView.isHidden = true
                    return cell
                }
            }
            
        } else {
            let i = indexPath.row + 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "UnlimitedCell", for: indexPath) as! UnlimitedTableViewCell
            //UTleftIcon
            if (!(NSLocalizedString(("UTleftIcon\(i)"), comment: "") == "UTleftIcon\(i)")){
                cell.leftIcon?.image = UIImage(named:NSLocalizedString(("UTleftIcon\(i)"), comment: ""))
            }
            else{ cell.leftIcon?.image = nil}
            
            //UTtopLabel
            if (!(NSLocalizedString(("UTtopLabel\(i)"), comment: "") == "UTtopLabel\(i)")){
                cell.topLabel?.text = NSLocalizedString(("UTtopLabel\(i)"), comment: "")
            }else{ cell.topLabel?.text = nil}
            
            //UTmainTextLabel
            if (!(NSLocalizedString(("UTmainTextLabel\(i)"), comment: "") == "UTmainTextLabel\(i)")){
                var mainText = NSLocalizedString(("UTmainTextLabel\(i)"), comment: "")
                if i == 1 {
                    mainText = mainText + self.strKidsNames
                }
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 5
                let attrString = NSMutableAttributedString(string: mainText)
                attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: NSMakeRange(0, attrString.length))
                cell.mainTextLabel?.attributedText = attrString
            }else{ cell.mainTextLabel?.text = nil}
            return cell
        }
        
    }
}



