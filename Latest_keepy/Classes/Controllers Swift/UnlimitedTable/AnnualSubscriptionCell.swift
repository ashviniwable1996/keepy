//
//  AnnualSubscriptionCell.swift
//  Keepy
//
//  Created by Mindbowser on 09/01/19.
//  Copyright © 2019 Keepy. All rights reserved.
//

import UIKit

class AnnualSubscriptionCell: UITableViewCell {    
    @IBOutlet weak var backGView: UIView!
    @IBOutlet weak var selectionBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!    
    @IBOutlet weak var subscriptioTotalView: UIView!    
    @IBOutlet weak var totalValueLbl: UILabel!
    @IBOutlet weak var verticalLine: UIView!
    @IBOutlet weak var titleLblTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
