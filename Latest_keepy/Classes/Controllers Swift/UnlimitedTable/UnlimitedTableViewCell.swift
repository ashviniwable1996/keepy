//
//  UnlimitedTableViewCell.swift
//  Keepy
//
//  Created by Kolchakov Ruslan on 5/29/17.
//  Copyright © 2017 Keepy. All rights reserved.
//

import UIKit

class UnlimitedTableViewCell: UITableViewCell {
    @IBOutlet weak var leftIcon: UIImageView?
    @IBOutlet weak var topLabel: UILabel?
    @IBOutlet weak var mainTextLabel: UILabel?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
