//
//  KPPlan+API.h
//  Keepy
//
//  Created by Dunja Lalic on 24/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPPlan.h"

@interface KPPlan (API)

+ (void)getPlansForCouponCode:(NSString *)couponCode resultBlock:(void(^)(NSArray <KPPlan *> *plans, NSError *error))block;

@end
