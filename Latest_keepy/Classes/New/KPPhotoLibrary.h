//
//  KPPhotoLibrary.h
//  Keepy
//
//  Created by Dunja Lalic on 15/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import "Keepy-Swift.h"

@class ALAsset;
@class ALAssetsLibrary;
@class PHAsset;
@class Photos;

typedef NSComparisonResult (^KPAssetComparator)(ALAsset *asset1, ALAsset *asset2);

@interface KPPhotoLibrary : NSObject

@property(nonatomic, readonly) ALAssetsLibrary *assetLibrary;
@property(nonatomic, assign) BOOL sortAscending;
@property(nonatomic, copy) KPAssetComparator sortComparator;
@property(nonatomic, readonly) PHPhotoLibrary *assetsLibrary;
@property(nonatomic, strong) PHAssetCollection *collection;

+ (BOOL)isAuthorized;

+ (BOOL)authorizationIsDenied;

- (void)loadPhotosAsynchronously:(void (^)(NSArray *assets, NSError *error))callbackBlock;

@end
