//
//  ALAssetsGroup+KP.h
//  Keepy
//
//  Created by Dunja Lalic on 15/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetsGroup (KP)

@property(nonatomic, readonly) NSArray *assets;

@end
