//
//  KPRequestSerializer.h
//  Keepy
//
//  Created by Dunja Lalic on 27/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "AFURLRequestSerialization.h"

@interface KPRequestSerializer : AFHTTPRequestSerializer

@property(readonly, nonatomic, strong) id <AFURLRequestSerialization> serializer;

+ (instancetype)serializerWithSerializer:(id <AFURLRequestSerialization>)serializer;

@end
