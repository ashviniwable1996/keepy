//
//  KPAPIClient.m
//  Keepy
//
//  Created by Dunja Lalic on 16/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPAPIClient.h"
#import "AFNetworkActivityLogger.h"
#import "KPRequestSerializer.h"

#import "Keepy-Swift.h"

@interface KPAPIClient ()

@property(nonatomic, strong) NSDateFormatter *requestDateFormatter;

@end

@implementation KPAPIClient

+ (KPAPIClient *)sharedClient {
    static KPAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[KPAPIClient alloc] initWithBaseURL:[NSURL URLWithString:Defaults.apiEndpoint]];

    });
    return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {

        self.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];

        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
        responseSerializer.removesKeysWithNullValues = YES;

        [self setResponseSerializer:responseSerializer];
        [self setRequestSerializer:[KPRequestSerializer serializer]];

        //[self.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Accept"];
        //[self.requestSerializer setValue: @"application/json" forHTTPHeaderField: @"Content-Type"];

        //[[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        self.requestDateFormatter = formatter;
    }

    return self;
}

-(NSString *)getLocalDateTimeFromUTC:(NSString *)strDate
{
    NSDateFormatter *dtFormat = [[NSDateFormatter alloc] init];
    [dtFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dtFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *aDate = [dtFormat dateFromString:strDate];
    
    [dtFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dtFormat setTimeZone:[NSTimeZone systemTimeZone]];
    
    return [dtFormat stringFromDate:aDate];
}
- (NSString *)requestDateString {
//    NSString * todaysDate = [self getDateInString];
//    return [self getLocalDateTimeFromUTC:todaysDate];

    return [self.requestDateFormatter stringFromDate:[NSDate date]];
}
- (NSString *)getDateInString {
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]; // Date formater
    NSLog(@"date :%@",[dateformate stringFromDate:[NSDate date]]);

    return [dateformate stringFromDate:[NSDate date]]; // Convert date to string
}

@end
