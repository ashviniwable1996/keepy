//
//  KPPlan.h
//  Keepy
//
//  Created by Dunja Lalic on 24/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

@class SKProduct;

@interface KPPlan : NSObject

@property(nonatomic, strong) NSNumber *layoutId;
@property(nonatomic, strong) NSString *planType;
@property(nonatomic, strong) NSString *sku;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) SKProduct *product;

@end
