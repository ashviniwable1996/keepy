//
//  SharedHeaders.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>

#ifdef DEBUG
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
#else
static const DDLogLevel ddLogLevel = DDLogLevelError;
#endif

//TODO: untangle this:
#import "UserManager.h"
#import "GlobalUtils.h"
#import "Analytics.h"

#import "Mixpanel.h"

#import "UIImageView+WebCache.h"
#import "UIImage-Extensions.h"

//UIView+position is an extension that allows to access the frame properties directly, like view.frameWidth or view.frameX
#import "UIView+position.h"

#import "SVProgressHUD.h"
