//
//  ALAssetsGroup+KP.m
//  Keepy
//
//  Created by Dunja Lalic on 15/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "ALAssetsGroup+KP.h"

@implementation ALAssetsGroup (KP)

- (NSArray *)assets {
    NSMutableArray *assets = [[NSMutableArray alloc] init];
    [self enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (result) {
            [assets addObject:result];
        }
    }];
    return [assets copy];
}

@end
