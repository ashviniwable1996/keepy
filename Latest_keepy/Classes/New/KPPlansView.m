//
//  KPPlansView.m
//  Keepy
//
//  Created by Dunja Lalic on 18/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPPlansView.h"

#import "Keepy-Swift.h"

@implementation KPPlansView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        CGRect spreadTheLoveFrame = CGRectZero; // the actual frame
        spreadTheLoveFrame.origin.x = 5;
        spreadTheLoveFrame.origin.y = 5;
        spreadTheLoveFrame.size.width = 310;
        spreadTheLoveFrame.size.height = 80;
        
        float pricePlanButtonWidth = 87;
        
        float singleBoxWidth = (310 - 5 * (2 - 1)) / 2;
        // float planLabelWidth = 66;
        
        // GREEN MONTH BOX START
        
        CGRect monthBoxFrame = CGRectZero;
        monthBoxFrame.origin.x = 5;
        monthBoxFrame.origin.y = 90;
        monthBoxFrame.size.width = singleBoxWidth;
        monthBoxFrame.size.height = 194 / 2.0f;
        
        monthBoxFrame.origin.y -= spreadTheLoveFrame.origin.y + spreadTheLoveFrame.size.height;
        
        UIImageView* bkgView = [[UIImageView alloc] initWithFrame:monthBoxFrame];
        bkgView.backgroundColor = [UIColor keepyDarkGreenColor];
        bkgView.userInteractionEnabled = YES;
        [self addSubview:bkgView];
        
        UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundImage:[[UIImage imageNamed:@"start-keepying-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor keepyDarkGreenColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont regularFontOfSize:15];
        [btn setTitle:KPLocalizedString(@"$2.99") forState:UIControlStateNormal];
        // btn.frame = CGRectMake(0, 18, pricePlanButtonWidth, 44);
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        // btn.center = CGPointMake(bkgView.center.x, btn.center.y);
        btn.frame = CGRectMake((monthBoxFrame.size.width - pricePlanButtonWidth) * 0.5f, 18, pricePlanButtonWidth, 44);
        btn.tag = 1;
        self.leftPriceButton = btn;
        [btn addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
        [bkgView addSubview:btn];
        
        UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 130 / 2.0f, bkgView.bounds.size.width, 20)];
        lbl.textColor = [UIColor whiteColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont regularFontOfSize:17];
        lbl.text = KPLocalizedString(@"monthly");
        // lbl.center = CGPointMake(bkgView.center.x, lbl.center.y);
        self.leftTitleLabel = lbl;
        [bkgView addSubview:lbl];
        
        // GREEN MONTH BOX END
        
        
        
        // GREEN YEAR BOX START
        
        CGRect yearBoxFrame = CGRectZero;
        yearBoxFrame.origin.x = 5 + singleBoxWidth + 5; // we add the 5 twice because 5 is also the initial padding
        yearBoxFrame.origin.y = 90;
        yearBoxFrame.size.width = singleBoxWidth;
        yearBoxFrame.size.height = 194 / 2.0f;
        
        yearBoxFrame.origin.y -= spreadTheLoveFrame.origin.y + spreadTheLoveFrame.size.height;
        
        bkgView = [[UIImageView alloc] initWithFrame:yearBoxFrame];
        bkgView.backgroundColor = [UIColor keepyDarkGreenColor];
        bkgView.userInteractionEnabled = YES;
        [self addSubview:bkgView];
        
        //btn
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundImage:[[UIImage imageNamed:@"start-keepying-button-bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor keepyDarkGreenColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont regularFontOfSize:15];
        [btn setTitle:KPLocalizedString(@"$29.99") forState:UIControlStateNormal];
        btn.frame = CGRectMake((yearBoxFrame.size.width - pricePlanButtonWidth) * 0.5f, 18, pricePlanButtonWidth, 44);
        // btn.center = CGPointMake(bkgView.center.x, btn.center.y);
        self.rightPriceButton = btn;
        [btn addTarget:self action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
        [bkgView addSubview:btn];
        
        lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 130 / 2.0f, bkgView.bounds.size.width, 20)];
        lbl.textColor = [UIColor whiteColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [UIFont regularFontOfSize:17];
        lbl.text = KPLocalizedString(@"annualy");
        // lbl.center = CGPointMake(bkgView.center.x, lbl.center.y);
        self.rightTitleLabel = lbl;
        [bkgView addSubview:lbl];
        
        // GREEN YEAR BOX END
        
        
        
        // COUPON FIELD START
        
        CGRect couponFieldFrame = CGRectMake(-1, 380 / 2.0f, self.bounds.size.width + 2, 44);
        couponFieldFrame.origin.y -= spreadTheLoveFrame.origin.y + spreadTheLoveFrame.size.height;
        
        UITextField *textField = [[UITextField alloc] initWithFrame:couponFieldFrame];
        textField.borderStyle = UITextBorderStyleNone;
        textField.backgroundColor = [UIColor colorWithWhite:250 / 250.0f alpha:1.0f];
        textField.layer.borderWidth = 0.5f;
        textField.layer.borderColor = [[UIColor keepyLightBrownColor] CGColor];
        textField.placeholder = NSLocalizedString(@"enter coupon code", @"");
        textField.font = [UIFont regularFontOfSize:17];
        textField.textColor = [UIColor keepyBrownColor];
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.returnKeyType = UIReturnKeyDone;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.delegate = self;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitleColor:[UIColor keepyBlueColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor disabledButtonTitleColor] forState:UIControlStateDisabled];
        [button setTitle:NSLocalizedString(@"apply", @"") forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont regularFontOfSize:15];
        [button addTarget:self action:@selector(apply:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0, 0, 60, 44);
        textField.rightView = button;
        textField.rightViewMode = UITextFieldViewModeAlways;
        button.enabled = NO;
        
        textField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, textField.bounds.size.height)];
        textField.leftViewMode = UITextFieldViewModeAlways;
        
        [self addSubview:textField];
        self.couponTextField = textField;
        
        // COUPON FIELD END
        
        
        // COUPON ERROR LABEL START
        
        CGRect couponErrorLabelFrame = CGRectMake(0, 490 / 2.0f, self.bounds.size.width, 20);
        couponErrorLabelFrame.origin.y -= spreadTheLoveFrame.origin.y + spreadTheLoveFrame.size.height;
        
        lbl = [[UILabel alloc] initWithFrame:couponErrorLabelFrame];
        lbl.textColor = [UIColor keepyPinkColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor clearColor];
        lbl.adjustsFontSizeToFitWidth = YES;
        lbl.font = [UIFont fontWithName:@"ARSMaquettePro-Bold" size:17];
        self.errorLabel = lbl;
        [self addSubview:lbl];
        
        // COUPON ERROR LABEL END
    }
    return self;
}


- (void)tellFriendTap:(UIButton *)sender {
    [self.delegate plansView:self didSelectReferFriends:sender];
}

- (void)leftButton:(UIButton *)sender {
    [self.delegate plansView:self didSelectLeftButton:sender];
}

- (void)rightButton:(UIButton *)sender {
    [self.delegate plansView:self didSelectRightButton:sender];
}

- (void)apply:(UIButton *)sender {
    [self.couponTextField resignFirstResponder];
    [self.delegate plansView:self didEnterCouponCode:self.couponTextField.text];
}

- (void)onCouponApplied {
    [self.leftPriceButton setTitleColor:[UIColor keepyPinkColor] forState:UIControlStateNormal];
    [self.rightPriceButton setTitleColor:[UIColor keepyPinkColor] forState:UIControlStateNormal];
    
    self.couponTextField.text = NSLocalizedString(@"coupon applied", @"");
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, self.couponTextField.frame.size.height)];
    UIImageView *checkmark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"coupon-applied-checkmark"]];
    checkmark.frame = CGRectMake(20, 0, 0, 0);
    [checkmark sizeToFit];
    checkmark.center = CGPointMake(checkmark.center.x, leftView.frame.size.height / 2.0f);
    [leftView addSubview:checkmark];
    self.couponTextField.leftView = leftView;
    
    self.couponTextField.rightView = nil;
    self.couponTextField.enabled = NO;
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    UIButton *rightView = (UIButton *) textField.rightView;
    rightView.enabled = (newString.length != 0);
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
