//
//  KPAddressBookUtils.m
//  Keepy
//
//  Created by Dunja Lalic on 28/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPAddressBookUtils.h"
#import <AddressBook/AddressBook.h>

@interface KPAddressBookUtils ()

@end

@implementation KPAddressBookUtils

+ (instancetype)sharedUtils {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.emailArray = [[NSMutableArray alloc] init];
        self.searchArray = [[NSMutableArray alloc] init];


        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);

        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    // First time access has been granted, add the contact
                    [self getAllEmails:addressBookRef];
                } else {
                    // User denied access
                    // Display an alert telling user the contact could not be added
                }
            });
        }
        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
            // The user has previously given access, add the contact
            [self getAllEmails:addressBookRef];
        }
        else {
            // The user has previously denied access
            // Send an alert telling user to change privacy setting in settings app
        }


    }
    return self;
}

- (void)getAllEmails:(ABAddressBookRef)addressBook {
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
    NSMutableArray *allEmails = [[NSMutableArray alloc] initWithCapacity:CFArrayGetCount(people)];
    for (CFIndex i = 0; i < CFArrayGetCount(people); i++) {
        ABRecordRef person = CFArrayGetValueAtIndex(people, i);
        ABMultiValueRef emails = ABRecordCopyValue(person, kABPersonEmailProperty);
        for (CFIndex j = 0; j < ABMultiValueGetCount(emails); j++) {
            NSString *email = (__bridge NSString *) ABMultiValueCopyValueAtIndex(emails, j);
            [allEmails addObject:email];
        }
        CFRelease(emails);
    }

    self.emailArray = allEmails;
}

@end
