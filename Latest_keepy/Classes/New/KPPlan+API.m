//
//  KPPlan+API.m
//  Keepy
//
//  Created by Dunja Lalic on 24/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPPlan+API.h"
#import "KPAPIClient.h"

@implementation KPPlan (API)

+ (void)getPlansForCouponCode:(NSString *)couponCode resultBlock:(void(^)(NSArray *plans, NSError *error))block {
    NSString *url = [NSString stringWithFormat:@"getPlans%@", couponCode == nil ? @"" : [NSString stringWithFormat:@"/%@", couponCode]];

    [[KPAPIClient sharedClient] GET:url
                         parameters:nil
                            success:^(AFHTTPRequestOperation *operation, id responseObject) {

                                if ([responseObject[@"status"] integerValue] == 1) {
                                    NSDictionary *result = operation.responseObject;
                                    if (result != nil && [[result valueForKey:@"error"] valueForKey:@"errors"] != nil) {
                                        NSString *description;
                                        NSArray *arr = [(NSArray *) [result valueForKey:@"error"] valueForKey:@"errors"];
                                        if ([arr count] > 0) {
                                            description = arr[0];
                                            NSDictionary *errorDictionary = @{NSLocalizedDescriptionKey : description};

                                            NSError *anError = [[NSError alloc] initWithDomain:@"KPErrorDomain" code:403 userInfo:errorDictionary];
                                            return block(nil, anError);
                                        }
                                    }
                                }
                                else {
                                    NSMutableArray *array = [NSMutableArray array];
                                    for (NSDictionary *dict in responseObject[@"result"]) {
                                        KPPlan *plan = [[KPPlan alloc] init];
                                        plan.layoutId = dict[@"layoutId"];
                                        plan.planType = dict[@"planType"];
                                        plan.sku = dict[@"sku"];
                                        plan.title = dict[@"title"];
                                        [array addObject:plan];
                                    }

                                    return block(array, nil);
                                }
                            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                return block(nil, error);
            }];
}

@end
