//
//  KPPhotoLibrary.m
//  Keepy
//
//  Created by Dunja Lalic on 15/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPPhotoLibrary.h"
#import "ALAssetsGroup+KP.h"

@implementation KPPhotoLibrary

- (instancetype)init {
    self = [super init];
    if (self) {
        _assetLibrary = [[ALAssetsLibrary alloc] init];
    }
    return self;
}

#pragma mark - public

+ (BOOL)isAuthorized {
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    return (status == ALAuthorizationStatusAuthorized ||
            status == ALAuthorizationStatusNotDetermined);
}

+ (BOOL)authorizationIsDenied {
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    return (status == ALAuthorizationStatusDenied ||
            status == ALAuthorizationStatusRestricted);
}

- (void)loadPhotosAsynchronously:(void (^)(NSArray *assets, NSError *error))callbackBlock {
    NSMutableArray *assets = [[NSMutableArray alloc] init];    
    
    PHFetchOptions* options = [PHFetchOptions new];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult* images = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
    
    [images enumerateObjectsUsingBlock:^(PHAsset* asset, NSUInteger idx, BOOL *stop) {
        if (images.count == 0) {
            [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"no photos found")];
            dispatch_async(dispatch_get_main_queue(), ^{
                callbackBlock ? callbackBlock(nil, nil) : nil;
            });
        }

        [assets insertObject:asset atIndex:0];

        if (idx + 1 == images.count) {
            dispatch_async(dispatch_get_main_queue(), ^{
                callbackBlock ? callbackBlock([assets copy], nil) : nil;
            });
            *stop = YES;
        }
    }];
    
//    [self.assetLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
//                                     usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
//                                         [assets addObjectsFromArray:group.assets];
//                                         if (!group) {
//                                             [assets sortUsingDescriptors:@[descriptor]];
//                                             dispatch_async(dispatch_get_main_queue(), ^{
//                                                 callbackBlock ? callbackBlock([assets copy], nil) : nil;
//                                             });
//                                         }
//                                     } failureBlock:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    callbackBlock ? callbackBlock(nil, error) : nil;
//                });
//            }];
}

- (KPAssetComparator)sortComparator {
    if (!_sortComparator) {
        _sortComparator = ^NSComparisonResult(ALAsset *asset1, ALAsset *asset2) {
            NSDate *date1 = [asset1 valueForProperty:ALAssetPropertyDate];
            NSDate *date2 = [asset2 valueForProperty:ALAssetPropertyDate];
            return [date1 compare:date2];
        };
    }
    return _sortComparator;
}
@end
