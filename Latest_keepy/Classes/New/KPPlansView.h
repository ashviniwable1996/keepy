//
//  KPPlansView.h
//  Keepy
//
//  Created by Dunja Lalic on 18/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

@class KPPlansView;

@protocol KPPlansViewDelegate <NSObject>

- (void)plansView:(KPPlansView *)view didSelectReferFriends:(UIButton *)sender;

- (void)plansView:(KPPlansView *)view didSelectLeftButton:(UIButton *)sender;

- (void)plansView:(KPPlansView *)view didSelectRightButton:(UIButton *)sender;

- (void)plansView:(KPPlansView *)view didEnterCouponCode:(NSString *)couponCode;

@end

@interface KPPlansView : UIView <UITextFieldDelegate>

@property(nonatomic, weak) UITextField *couponTextField;
@property(nonatomic, weak) UILabel *leftTitleLabel;
@property(nonatomic, weak) UILabel *rightTitleLabel;
@property(nonatomic, weak) UIButton *leftPriceButton;
@property(nonatomic, weak) UIButton *rightPriceButton;
@property(nonatomic, weak) UILabel *errorLabel;
@property(nonatomic, weak) id <KPPlansViewDelegate> delegate;

- (void)onCouponApplied;

@end
