//
//  Item+KP.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Item.h"

typedef NS_ENUM(NSUInteger, KPItemType) {
    KPItemTypeQuote = 2,
    KPItemTypeVideo = 4,
};

@interface Item (KP)

- (int)getCommentIndexById:(int)commentId;

- (NSArray *)getCommentsOrderedByDate;

- (Comment *)getVideoStory;

@end
