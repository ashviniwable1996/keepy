//
//  Asset+KP.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

@implementation Asset (KP)

- (NSString *)urlHash {
    NSString *result = self.assetHash;
    if (result == nil || result.length == 0) {
        result = [NSString stringWithFormat:@"%d", self.assetId.intValue];
    }
    return result;
}

@end
