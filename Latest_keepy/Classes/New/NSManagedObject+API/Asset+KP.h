//
//  Asset+KP.h
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Asset.h"

@interface Asset (KP)

@property(readonly, strong) NSString *urlHash;

@end
