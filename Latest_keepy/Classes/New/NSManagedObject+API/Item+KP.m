//
//  Item+KP.m
//  Keepy
//
//  Created by Dunja Lalic on 10/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "Comment.h"

@implementation Item (KP)

//TODO: use predicates

- (int)getCommentIndexById:(int)commentId {
    NSArray *arr = [self getCommentsOrderedByDate];
    for (int i = 0; i < [arr count]; i++) {
        Comment *comment = [arr objectAtIndex:i];
        if (comment.commentId.intValue == commentId) {
            return i;
        }
    }

    return -1;
}

- (NSArray *)getCommentsOrderedByDate {
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"commentDate" ascending:YES]];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObjectsFromArray:[self.comments.allObjects sortedArrayUsingDescriptors:sortDescriptors]];

    return arr;
}

- (Comment *)getVideoStory {
    for (Comment *comment in self.comments) {
        if (comment.isStory.boolValue) {
            return comment;
        }
    }

    return nil;
}

@end
