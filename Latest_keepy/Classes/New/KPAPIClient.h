//
//  KPAPIClient.h
//  Keepy
//
//  Created by Dunja Lalic on 16/09/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

typedef void (^KPAPIResultBlock)(BOOL succeeded, NSError *error);

@interface KPAPIClient : AFHTTPRequestOperationManager

@property(nonatomic, readonly) NSString *requestDateString;

+ (KPAPIClient *)sharedClient;

@end
