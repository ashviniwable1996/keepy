//
//  KPRequestSerializer.m
//  Keepy
//
//  Created by Dunja Lalic on 27/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import "KPRequestSerializer.h"
#import <CommonCrypto/CommonHMAC.h>
#import "NSData+MPBase64.h"
#import "KPAPIClient.h"

@interface KPRequestSerializer ()
@property(readwrite, nonatomic, strong) id <AFURLRequestSerialization> serializer;
@end

@implementation KPRequestSerializer

+ (instancetype)serializerWithSerializer:(id <AFURLRequestSerialization>)serializer {
    KPRequestSerializer *kpSerializer = [KPRequestSerializer serializer];
    kpSerializer.serializer = serializer;

    return kpSerializer;
}

#pragma mark - AFURLRequestSerialization

- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request
                               withParameters:(id)parameters
                                        error:(NSError *__autoreleasing *)error {
    NSError *serializationError = nil;
    NSMutableURLRequest *mutableRequest = [[super requestBySerializingRequest:request withParameters:parameters error:&serializationError] mutableCopy];
    if (!serializationError) {
        [mutableRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [mutableRequest addValue:[NSString stringWithFormat:@"%d", [[UserManager sharedInstance] getMe].userId.intValue] forHTTPHeaderField:@"uid"];

        [mutableRequest addValue:[[KPAPIClient sharedClient] requestDateString] forHTTPHeaderField:@"reqtime"];

        NSString *hash = [self signedRequest:mutableRequest];
        [mutableRequest addValue:[NSString stringWithFormat:@"Basic %@", hash] forHTTPHeaderField:@"Authorization"];
    } else {
        if (error) {
            *error = serializationError;
        }
    }

    return mutableRequest;
}

- (NSString *)signedRequest:(NSURLRequest *)req {
    NSString *key = [[UserManager sharedInstance] getMe].authKey;

    NSMutableString *input = [[NSMutableString alloc] init];

    [input appendFormat:@"%@", [req.URL.path lowercaseString]];
    [input appendFormat:@"%@", [req.HTTPMethod lowercaseString]];

    NSDictionary *headers = [req allHTTPHeaderFields];
    [input appendFormat:@"content-type%@", [[headers valueForKey:@"content-type"] lowercaseString]];
    [input appendFormat:@"reqtime%@", [[headers valueForKey:@"reqtime"] lowercaseString]];
    [input appendFormat:@"uid%@", [[headers valueForKey:@"uid"] lowercaseString]];

    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [input cStringUsingEncoding:NSASCIIStringEncoding];

    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];

    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);

    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];

    NSString *hash = [HMAC mp_base64EncodedString];

    return hash;
}

#pragma mark - NSCoder

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }

    self.serializer = [decoder decodeObjectForKey:NSStringFromSelector(@selector(serializer))];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.serializer forKey:NSStringFromSelector(@selector(serializer))];
}

#pragma mark - NSCopying

- (instancetype)initcopyWithZone:(NSZone *)zone {
    KPRequestSerializer *serializer = [[[self class] allocWithZone:zone] init];
    serializer.serializer = self.serializer;

    return serializer;
}

@end