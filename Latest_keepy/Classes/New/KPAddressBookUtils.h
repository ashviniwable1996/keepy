//
//  KPAddressBookUtils.h
//  Keepy
//
//  Created by Dunja Lalic on 28/10/14.
//  Copyright (c) 2014 Keepy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPAddressBookUtils : NSObject

@property(nonatomic, retain) NSMutableArray *searchArray;
@property(nonatomic, strong) NSMutableArray *emailArray;


+ (instancetype)sharedUtils;

@end
