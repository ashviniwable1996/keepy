//
// Created by Antonio Bello on 4/22/16.
// Copyright (c) 2016 Keepy. All rights reserved.
//

import Mixpanel
import Branch

private enum BranchParam : String {
	case IsFirstSession = "+is_first_session"
	case ClickedBranchLink = "+clicked_branch_link"
	case MarketingTitle = "$marketing_title"
}

@objc final class BranchHandler : NSObject {
	static func processOnInit(params: [AnyHashable: Any]?, error: NSError?) {
		guard error == nil else { print("[ERROR] branch.io: \(error!.localizedDescription)"); return }
		guard var params = params else { print("[WARN] branch.io: no param provided"); return }

		if let referringParam = params[BranchParam.IsFirstSession.rawValue] as? Bool, referringParam == true {
			if let branchLink = params[BranchParam.ClickedBranchLink.rawValue] as? Bool, let marketingTitle = params[BranchParam.MarketingTitle.rawValue] as? String, branchLink == true {
				params["campaign"] = marketingTitle
			} else {
				params["campaign"] = "none"
			}
//zvika601 Moving to AppDelegate
//			let mixpanel = Mixpanel.sharedInstance()
//            
//			mixpanel.track("branch.io", properties: params)
        }
		print("[INFO] branch.io: \(params.description)")
	}
}
