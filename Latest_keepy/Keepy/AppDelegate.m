//
//  AppDelegate.m
//  Keepy
//
//  Created by Yaniv Solnik on 2/5/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandSupport.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalThreading.h>

#import "AppDelegate.h"

#import <CocoaLumberjack/CocoaLumberjack.h>

#import "HomeViewController.h"
#import "Reachability.h"
#import "SDImageCache.h"
#import "KidsMenuViewController.h"
#import "SettingsMenuViewController.h"
#import "RegisterViewController.h"
#import "KPDataParser.h"
#import "OpenningViewController.h"
#import "LoadingViewController.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>
#import "EditKidViewController.h"
#import "KPNotificationCenter.h"
#import "FansViewController.h"
#import "AccountViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <Mixpanel/Mixpanel.h>
#import <Mixpanel/MPTweakInline.h>
#import "AFNetworkActivityLogger.h"
#import "KPSQLiteManager.h"
#import "KPLocalKid.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TwitterKit/TwitterKit.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>

#import <LaunchKit/LaunchKit.h>

#import "ABBI.h"

#import "Branch.h"

#import "Keepy-Swift.h"

#import "PrintIOManager.h"

#import "TestFairy.h"

#import "HomeViewController.h"

#import "OLKitePrintSDK.h"

//#import "Analytics.h"

@interface NSString (XQueryComponents)
- (NSString *)stringByDecodingURLFormat;
- (NSString *)stringByEncodingURLFormat;
@end

@interface AppDelegate() </*DBSessionDelegate, */ UIWebViewDelegate, EditKidDelegate,CrashlyticsDelegate>
{
    BOOL isFirstReachabilityNotification;
    SKPaymentQueue * queues;
}

@property (nonatomic, strong) NSString *dropboxUserId;
@property (nonatomic, strong) NSString *kidAddSource;

@end

@implementation AppDelegate


- (NSDictionary *)parseQueryString:(NSString *)query {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        NSString *key = [elements[0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *val = [elements[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        dict[key] = val;
    }
    return dict;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  //  [OLKitePrintSDK setAPIKey:@"a1c17f1ff259f1de37f16e5183fa6728a027f77c" withEnvironment:OLKitePrintSDKEnvironmentSandbox];
    [OLKitePrintSDK setAPIKey:@"4a86de6d769c29bf881dd0c595a15da7bd9cd1ac" withEnvironment:OLKitePrintSDKEnvironmentLive];
    //Or OLKitePrintSDKEnvironmentSandbox for testing
#if !DEBUG
    Defaults.itemsLastUpdatedAt = nil;
    [[AppsFlyerTracker sharedTracker] setIsDebug:YES];
#endif
    Defaults.UserLastUpdatedAt = nil;
    
    [UINavigationBar appearance].tintColor = UIColorFromRGB(0x2CA0C4);
    [UINavigationBar appearance].barTintColor = nil;
    [UINavigationBar appearance].titleTextAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"ARSMaquettePro-Medium" size:17.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:0.298 green:0.235 blue:0.212 alpha:1.0f]};
    
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setForegroundColor:[UIColor keepyBrownColor]];
    [SVProgressHUD setFont:[UIFont lightFontOfSize:17.0f]];
    [SVProgressHUD setSuccessImage:[UIImage imageNamed:@"success"]];
    [SVProgressHUD setErrorImage:[UIImage imageNamed:@"error"]];
    
#ifdef DEBUG
    // [[AFNetworkActivityLogger sharedLogger] startLogging];
    // [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
    
    // THIS IS IMPORTANT!
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelOff];
#endif
    
    [StoreKitManager sharedManager];  // Instantiate the singleton.
    
    // added by Ashvini
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    //
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:[@"Keepy" stringByAppendingPathExtension:@"sqlite"]];

    [Mixpanel sharedInstanceWithToken:@"992bf3f37b221e8c2db42980a03285d4"];
    [LaunchKit launchWithToken:@"GdxMWaAyMjBKKnL-RMaSUXgS5YymhT0dIYtmY5qPlAOL"];
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"ZHSNYazQaAUHP5vChavHm";
    [AppsFlyerTracker sharedTracker].appleAppID = @"647088205";
    
    //pending renewals
    queues = [SKPaymentQueue defaultQueue];
    
    for(SKPaymentTransaction *transaction in queues.transactions){
        
        NSDictionary *userInfo =
        [NSDictionary dictionaryWithObject:transaction forKey:@"transaction"];
        [[NSNotificationCenter defaultCenter] postNotificationName:
         @"TransactionUpdated" object:nil userInfo:userInfo];
        
        if (transaction.transactionState == SKPaymentTransactionStateFailed) {
            //possibly handle the error
            NSLog(@"Transaction state -> Failed");
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        } else if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
            NSLog(@"Transaction state -> Purchased");
        } else if (transaction.transactionState == SKPaymentTransactionStateRestored) {
            NSLog(@"Transaction state -> Restored");
        } else if (transaction.transactionState == SKPaymentTransactionStatePurchasing) {
            NSLog(@"Transaction state -> Purchasing");
        } else {
            //handle other transaction states
        }
    }

    //160620 Only once
    
    if(![[NSUserDefaults standardUserDefaults] stringForKey:@"BranchSentDef"])
    {
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        [BranchHandler processOnInitWithParams:params error:error];
        //zvika601 saving for later and uploading to Mixpanel
        [[NSUserDefaults standardUserDefaults] setObject:params forKey:@"BranchIOParamsKey"];
        [[Mixpanel sharedInstance] track: @"branch.io"  properties: params];
        }];
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"BranchSentDef"];
    }


    STAStartAppSDK *startApp = [STAStartAppSDK sharedInstance];
    startApp.appID = @"203554931";
    startApp.devID = @"db265e6efcd411e586aa5e5517507c66";
    [startApp disableReturnAd];

    [[Twitter sharedInstance] startWithConsumerKey:@"WNlkOeJsRTSAelDj7B5fw" consumerSecret:@"O5bFKh3kX80SxsXgvebwAA8j2uQOrtdclxWebKFlU"];

    [Fabric with:@[CrashlyticsKit, [Twitter class]]];
    
    [TestFairy begin:@"27edb8827b24acb13e21575ff94973a5b30ac61e"];

    isFirstReachabilityNotification = YES;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    reachability.unreachableBlock = ^(Reachability *reachability){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self internetConnectionOFF];
        });
    };
    reachability.reachableBlock = ^(Reachability *reachability){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self internetConnectionON];
        });
    };
    
    [reachability startNotifier];

    [ABBI start:@"d722eac5-6ae3-4fe0-b053-86c19848f7e3" withSecretKey:@"Nk15MFJKTCs5VVMvYmNkZWZBRmxnMTNHVDhUQlNXenJKSlJhK09jSG4rWjRpd1VVOXU3bi9jQTlnUE9BYzkzR1NT"];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    if (![[UserManager sharedInstance] isLoggedIn])
    {
        //RegisterViewController *registerVC = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
        
        //self.navController = [[UINavigationController alloc] initWithRootViewController:registerVC];
        
//        OpenningViewController *openningVC = [[OpenningViewController alloc] initWithNibName:@"OpenningViewController" bundle:nil];
//        self.navController = [[UINavigationController alloc] initWithRootViewController:openningVC];
        

        UIStoryboard *onboardStoryboard = [UIStoryboard storyboardWithName:@"Onboard" bundle:nil];
        self.navController = (KPLoadingNavigationController *)[onboardStoryboard instantiateViewControllerWithIdentifier:@"KPLoadingNavigationController"];
    } else {

        BOOL didDataStoreExist = NO;
        
        User *me = [[UserManager sharedInstance] getMe];
        if(me){

            didDataStoreExist = [self doesDataStoreExistForEmail:me.email];
            [self initializeDataStoreForEmail:me.email];

        }
        
        
        [self createHome];
        self.navController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
        
        
        // if the data store did not exist previously, let's make the redirection slightly different
        if(!didDataStoreExist){
            
            [[UserManager sharedInstance] setIsAfterLogin:YES];
            
            // we do not need to change the text that is being shown
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showWithStatus:KPLocalizedString(@"loading kids' data")];
                // [SVProgressHUD dismiss];
            });
            //
            
        }else{
        

            

            NSArray *children = [KPLocalKid fetchAll];
            // if ([[Kid MR_findAll] count] < 2)
            if(children.count == 0) { // TODO: check for Keepy-child-activation and own-ness of children
                [self.navController pushViewController:((HomeViewController*)self.viewController.frontViewController).editKidVC animated:YES];
            }
            
        }
        
    }

    [self.navController setNavigationBarHidden:YES];
    self.window.rootViewController = self.navController;
    
    [self.window makeKeyAndVisible];
    
    if (launchOptions != nil)
    {
        if ([launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"] != nil)
            [self handleNotification:[launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"] appWasOpen:NO];
        else if ([launchOptions valueForKey:UIApplicationLaunchOptionsURLKey] != nil)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_CAMERA_NOTIFICATION object:[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey]];
        }
    }
    
    NSString* appKey = @"64wu53t3q5upc23";
    NSString* appSecret = @"2keojzwz0puoss1";
    [DBClientsManager setupWithAppKey:appKey];
    
    
    if (!Defaults.firstTimeOpen) {
        [[Mixpanel sharedInstance] track:@"first-time-open"];
        
        [[Analytics sharedAnalytics] track:@"first-time-open"];
        Defaults.firstTimeOpen = YES;
        
        UIWebView *w = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        w.delegate = self;
        w.hidden = YES;
        [w loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/signRegisteredClient", Defaults.apiEndpoint]]]];
        [self.window addSubview:w];
    }
    
    // note: you also need to add AVfoundation.framework to your project's
    // list of linked frameworks
    NSError *error = nil;
    BOOL success = [[AVAudioSession sharedInstance]
                    setCategory:AVAudioSessionCategoryPlayback
                    error:&error];
    if (!success) {
        // Handle error here, as appropriate
    }
    
    
    NSArray *launchDates = [[NSUserDefaults standardUserDefaults] objectForKey:@"LaunchDates"];
    NSMutableArray *mutableLaunchDates = [NSMutableArray new];
    if (launchDates || launchDates.count < 5000) {
        mutableLaunchDates = [NSMutableArray arrayWithArray:launchDates];
    }
    [mutableLaunchDates addObject:[NSDate date]];
    [[NSUserDefaults standardUserDefaults] setObject:mutableLaunchDates forKey:@"LaunchDates"];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey: @"PurchaseHasBeenMade"]) {
        NSArray *launchDatesAfterPayment = [[NSUserDefaults standardUserDefaults] objectForKey:@"LaunchDatesAfterPurchase"];
        NSMutableArray *mutableLaunchDatesAfterPayment = [NSMutableArray new];
        if (launchDatesAfterPayment || launchDatesAfterPayment.count < 5000) {
            mutableLaunchDatesAfterPayment = [NSMutableArray arrayWithArray:launchDatesAfterPayment];
        }
        [mutableLaunchDatesAfterPayment addObject:[NSDate date]];
        [[NSUserDefaults standardUserDefaults] setObject:mutableLaunchDatesAfterPayment forKey:@"LaunchDatesAfterPurchase"];
    }
    return YES;
}



- (NSString *)dataModelLocationForEmail:(NSString *)emailAddress{
    NSString *sqliteFileName = [[emailAddress componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:@""];
    sqliteFileName = [NSString stringWithFormat:@"data-%@.sqlite", sqliteFileName];
    
    NSArray *documentDirectoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = documentDirectoryPaths[0];
    NSString *dataModelDirectory = [documentDirectory stringByAppendingPathComponent:@"data-model"];
    NSString *dataModelSQLitePath = [dataModelDirectory stringByAppendingPathComponent:sqliteFileName];
    
    return dataModelSQLitePath;
}

- (BOOL)doesDataStoreExistForEmail:(NSString *)emailAddress{
    
    NSString *dataModelSQLitePath = [self dataModelLocationForEmail:emailAddress];
    return [[NSFileManager defaultManager] fileExistsAtPath:dataModelSQLitePath];
    
}

- (void)initializeDataStoreForEmail:(NSString *)emailAddress{

    // let's initialize the local database
    NSString *dataModelSQLitePath = [self dataModelLocationForEmail:emailAddress];
    
    NSLog(@"\n\nSQLite: \n%@\n\n", dataModelSQLitePath);

    [KPSQLiteManager initManagerWithPath:dataModelSQLitePath];
}



-(void)createHome
{
    [self createHome:NO];
}

-(void)createHome:(BOOL)isFromLogin
{

    HomeViewController *hvc = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    hvc.isFromLogin = isFromLogin;
    self.viewController = [PKRevealController revealControllerWithFrontViewController:hvc
                                                                   leftViewController:[[KidsMenuViewController alloc] initWithNibName:@"KidsMenuViewController" bundle:nil]
                                                                  rightViewController:[[SettingsMenuViewController alloc] initWithNibName:@"SettingsMenuViewController" bundle:nil]
                                                                              options:nil];
    
    self.viewController.title = KPLocalizedString(@"back");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *html = [webView stringByEvaluatingJavaScriptFromString:
                      @"document.body.innerHTML"];
    NSRange r1 = [html rangeOfString:@"{"];
    if (r1.location == NSNotFound)
        return;
    
    r1.length = html.length - r1.location;
    NSRange r2 = [html rangeOfString:@"}" options:0 range:r1];
    if (r2.location == NSNotFound)
        return;
    
    r1.length = r2.location - r1.location + 1;
    NSString *s = [html substringWithRange:r1];
    
    NSError *e;
    NSDictionary *JSON =
    [NSJSONSerialization JSONObjectWithData: [s dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: &e];
    
    Defaults.registrationSignature  = [JSON valueForKey:@"rsig"];
    
    if ([[UserManager sharedInstance] isLoggedIn])
    {
        [[ServerComm instance] updateRegistrationSignature:^(id result) {
            
        } andFailBlock:^(NSError *error) {
            
        }];
    }
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    Defaults.UserLastUpdatedAt = nil;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"photoItemRefresh" object:nil];
   
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseClickedOnShare" object:nil];
    
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
   
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    
    if ([[UserManager sharedInstance] getMe]) {
        [[Mixpanel sharedInstance] track:@"become-active"];
    }

    // Register tweaks
    
    [self reloadTweaks];
    
//    [self reloadTweaksForAfterSignUP];
    [GlobalUtils clearAssetsUpload];
    
    if ([[UserManager sharedInstance] isLoggedIn] && ![[UserManager sharedInstance] isAfterRegistration]) {
        
        // Don't bother retrieving user data too often.
        if (Defaults.UserLastUpdatedAt == nil || [Defaults.UserLastUpdatedAt timeIntervalSinceNow] < -600) {
            Defaults.UserLastUpdatedAt = [NSDate date];
            [[UserManager sharedInstance] refreshMyData];
        }
    }
    

    [FBSDKAppEvents activateApp];
    [FBSDKSettings enableLoggingBehavior:FBSDKLoggingBehaviorAppEvents];
    [FBSDKAppEvents flush];
    [GlobalUtils fireTrigger:TRIGGER_TYPE_OPENAPP];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    if (reachability.isReachable) {
        [self internetConnectionON];
    } else {
        [self internetConnectionOFF];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings: (UIUserNotificationSettings *) notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler {
    [self handleNotification:userInfo appWasOpen:(application.applicationState == UIApplicationStateActive)];
    completionHandler();
}

//-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//    [self handleNotification:userInfo appWasOpen:(application.applicationState == UIApplicationStateActive)];
//    completionHandler(UIBackgroundFetchResultNewData);
//}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    
    NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString: @"<" withString: @""]
                              stringByReplacingOccurrencesOfString: @">" withString: @""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    Defaults.deviceToken = deviceToken;
    NSLog(@"device token: %@", deviceToken);
    
    [[[Mixpanel sharedInstance] people] addPushDeviceToken:devToken];

    if ([[UserManager sharedInstance] isLoggedIn])
    {
        [[ServerComm instance] updateDeviceToken:^(id result) {
            
        } andFailBlock:^(NSError *error) {
            
        }];
    }
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    [[Mixpanel sharedInstance] track: @"application-urlsession"  properties: @{ @"identifier" : identifier }];
    AssetUploader* uploader = [[AssetUploader alloc] initWithSessionIdentifier:identifier];
    [uploader commit];
    NSLog(@"AssetUploader: %@", uploader);
    completionHandler();
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    [[Mixpanel sharedInstance] track:@"deeplink-opened" properties:@{@"url" : url.description}];
    
    [[Branch getInstance] handleDeepLink:url];
    
    if ([[url absoluteString] hasPrefix:@"kpyme"])
    {
        [[Analytics sharedAnalytics] track:@"keepy-url-open" properties:@{@"source" : [url absoluteString]}];
        
        if ([[url absoluteString] rangeOfString: @"kpyme://linkToDropbox"].location != NSNotFound)
        {
            if (!(DBClientsManager.authorizedClient || DBClientsManager.authorizedTeamClient)) {
                //[self.revealController showViewController:self.revealController.frontViewController];
                
                UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
                
                while (topController.presentedViewController) {
                    topController = topController.presentedViewController;
                }
                
                double delayInSeconds = 0.1;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    [DBClientsManager authorizeFromController:[UIApplication sharedApplication]
                                                   controller:topController
                                                      openURL:^(NSURL *url) {
                                                          [[UIApplication sharedApplication] openURL:url];
                                                      }];
                });
            } else {
                [DBClientsManager unlinkAndResetClients];
                
                //   [self.dropboxButton setTitle:KPLocalizedString(@"link to\nDropbox") forState:UIControlStateNormal];
                
                [[ServerComm instance] updateDropboxCredentials:@"oauth_token= &oauth_token_secret= &uid= " withSuccessBlock:^(id result) {
                    
                    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
                    
                    while (topController.presentedViewController) {
                        topController = topController.presentedViewController;
                    }
                    
                    double delayInSeconds = 0.1;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                        [DBClientsManager authorizeFromController:[UIApplication sharedApplication]
                                                       controller:topController
                                                          openURL:^(NSURL *url) {
                                                              [[UIApplication sharedApplication] openURL:url];
                                                          }];
                    });
                    
                }                                  andFailBlock:^(NSError *error) {
                    
                }];
                
            }
        }
        else if ([[url absoluteString] rangeOfString: @"kpyme://coupon/"].location != NSNotFound)
        {
            
            NSMutableDictionary *properties = [NSMutableDictionary dictionary];
            [properties setValue: @"deeplink" forKey: @"source"];
            [[Mixpanel sharedInstance] track: @"my-keepies-opened" properties: properties];
            
            NSString *couponCode = [[url absoluteString] substringFromIndex: [@"kpyme://coupon/" length]];

            if (couponCode == nil) {
                return YES;
            }
            
            AccountViewController *avc = [[AccountViewController alloc] initWithNibName:@"AccountViewController" bundle:nil];
            [avc enterCouponCode: couponCode];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
            navController.view.tag = 1;
            AppDelegate *d = (AppDelegate*)[UIApplication sharedApplication].delegate;
            
            if (d.navController.presentedViewController != nil)
                [d.navController.presentedViewController presentViewController:navController animated:YES completion:nil];
            else
                [d.window.rootViewController presentViewController:navController animated:YES completion:nil];
            
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://promo"])
        {
            
//            NSMutableDictionary *properties = [NSMutableDictionary dictionary];
//            [properties setValue: @"max-keepies-reached-upgrade" forKey: @"source"];
//            [[Mixpanel sharedInstance] track: @"my-keepies-opened" properties: properties];
//            
//            properties = [NSMutableDictionary dictionary];
//            [[Mixpanel sharedInstance] track: @"max-keepies-reached-popup-upgrade" properties: properties];
            
            [GlobalUtils showUpgradeScreenWithParameters:nil fromViewController:@"After SignUP"];
            
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://upgrade"])
        {
            
            NSMutableDictionary *properties = [NSMutableDictionary dictionary];
            [properties setValue: @"max-keepies-reached-upgrade" forKey: @"source"];
            [[Mixpanel sharedInstance] track: @"my-keepies-opened" properties: properties];
            
            properties = [NSMutableDictionary dictionary];
            [[Mixpanel sharedInstance] track: @"max-keepies-reached-popup-upgrade" properties: properties];
            
            [GlobalUtils showUpgrade];
            
            return YES;
        }
        else if ([[url absoluteString] hasPrefix:@"kpyme://activator"])
        {
            NSDictionary *Query =[self parseQueryString:[url query]];
            
          //  DDLogInfo(@"%@",Query);
            
           // [GlobalUtils showUpgrade];
            
            NSTimeInterval delayInSeconds = 1.55;
            
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:TELL_A_FRIEND_TRIGGERD object:Query];
                
            });
            
           
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://keepystore"]) {
            [PrintIOManager showPrintIODialogInViewController:self.viewController withImage:nil];
        }else if ([[url host] isEqualToString:@"keepyphotobookstore"]) {
            //[[url absoluteString] isEqualToString:@"kpyme://keepyphotobookstore"]
           // [PrintIOManager showPrintIODialogInViewController:self.viewController withImage:nil];
            SettingsMenuViewController *settings = [[SettingsMenuViewController alloc] init];
            settings.allKidOLAssetDictionary = [[NSMutableDictionary alloc] init];
            settings.allKidNamesArray  = [[NSMutableArray alloc] init];
            [settings openPhotoBook:self.viewController isfromdeeplink:YES];
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://tellafriend"]) {
            [GlobalUtils spreadTheLove];
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://addfans"])
        {
            FansViewController *avc = [[FansViewController alloc] initWithNibName:@"FansViewController" bundle:nil];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
            [self.viewController presentViewController:navController animated:NO completion:^{
                [SVProgressHUD dismiss];
            }];
            
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://refresh"])
        {
            [[UserManager sharedInstance] refreshMyData];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://nextstep"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_NEXTSTEP_NOTIFICATION object:nil];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://closenextstep"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:CLOSE_NEXT_STEP_NOTIFICATION object:nil];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://spreadlove"])
        {
            [GlobalUtils spreadTheLove];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://rateapp"])
        {
            [GlobalUtils showRateApp];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://hateapp"])
        {
            [GlobalUtils showHateApp];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://sendfeedback"])
        {
            [GlobalUtils sendFeedback];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://rateapponstore"])
        {
            [GlobalUtils rateAppOnStore];
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://photobook"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:ShowedPopupDate];
            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:ShowedNotificationDate];
            
            UIViewController *topViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            while (topViewController.presentedViewController) topViewController = topViewController.presentedViewController;
            
            if ([PrintIOManager items].count > 0) {
                [PrintIOManager showBookReadyNotification];
                [PrintIOManager showBookReadyDialogInViewController:topViewController];
            }
            
            return YES;
        }
        else if ([[url absoluteString] isEqualToString:@"kpyme://addphoto"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_CAMERA_NOTIFICATION object:nil];
            return YES;
        }
        else if ([[url absoluteString] rangeOfString:@"kpyme://addkid"].location != NSNotFound)
        {
            EditKidViewController *avc = [[EditKidViewController alloc] initWithKid:nil];
            avc.delegate = self;
            self.kidAddSource = @"addkid link";
            NSArray *arr = [[url absoluteString] componentsSeparatedByString:@"?"];
            if ([arr count] > 1)
            {
                NSArray *arr2 = [arr[1] componentsSeparatedByString:@"="];
                if ([arr2 count] > 1)
                    self.kidAddSource = arr2[1];
            }
            
            [self.navController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
            [self.navController pushViewController:avc animated:YES];
            
            return YES;
        }
        else if ([[url absoluteString] rangeOfString:@"kpyme://viewitem"].location != NSNotFound)
        {

            // naively parse url
            NSArray *urlComponents = [[url absoluteString] componentsSeparatedByString:@"?"];
            NSArray *requestParameterChunks = [urlComponents[1] componentsSeparatedByString:@"&"];
            for (NSString *chunk in requestParameterChunks) {
                NSArray *keyVal = [chunk componentsSeparatedByString:@"="];
                if ([keyVal[0] isEqualToString:@"itemIds"]) {
                    NSMutableArray *itemIds = [[NSMutableArray alloc] initWithArray:[keyVal[1] componentsSeparatedByString:@","]];
                    
                    [SVProgressHUD showWithStatus:KPLocalizedString(@"loading")];
                    //get missing items from server...

                    [[ServerComm instance] getItems:^(id result) {
                        [[KPDataParser instance] parseKids:(NSArray *) [result valueForKey:@"result"] complete:^{
                            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                            NSArray *arr = [Item MR_findAllSortedBy:@"createDate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"itemId in %@", itemIds]];
                            if ([arr count] > 0)
                                [[GlobalUtils sharedInstance] showItemsInFullScreen:arr withIndex:0 andCommentId:-1];
                            [SVProgressHUD dismiss];
                        }];
                    }                  andFailBlock:^(NSError *error) {

                    }              andForceItemsIds:keyVal[1]];
                    
                    
                    break;
                }
            }
            
            return YES;
        }
        
        NSMutableDictionary *notificationData = [[NSMutableDictionary alloc] init];
        NSArray *arr = [[url absoluteString] componentsSeparatedByString:@"?"];
        if ([arr count] < 2)
            return YES;
        
        for(NSString *keyValuePairString in [arr[1] componentsSeparatedByString:@"&"])
        {
            NSArray *keyValuePairArray = [keyValuePairString componentsSeparatedByString:@"="];
            if ([keyValuePairArray count] < 2) continue; // Verify that there is at least one key, and at least one value.  Ignore extra = signs
            NSString *key = [keyValuePairArray[0] stringByDecodingURLFormat];
            NSString *value = [keyValuePairArray[1] stringByDecodingURLFormat];
            notificationData[key] = value;
        }

        [[Mixpanel sharedInstance] track:@"Open Notification from Email"];

        [[Analytics sharedAnalytics] track:@"notification-open" properties:@{@"source" : @"email"}];
        [self handleNotification:notificationData appWasOpen:NO isFromEmail:YES];
        
        return YES;
    } else if ([DBClientsManager handleRedirectURL:url] != nil) {
        if (DBClientsManager.authorizedClient || DBClientsManager.authorizedTeamClient) {
            NSDictionary *d = @{@"id" : @(-1), @"body" : KPLocalizedString(@"all your items will be synced to Dropbox and located under Apps-->keepy"), @"buttons" : @[@{@"title" : @"ok", @"link" : @""}]};
            [GlobalUtils showKeepyPopup:d];
            
            
            NSArray *arr = [[url absoluteString] componentsSeparatedByString:@"?"];
            if ([arr count] > 1)
            {
                [[ServerComm instance] updateDropboxCredentials:arr[1] withSuccessBlock:^(id result) {

                    [[ServerComm instance] syncDropbox:^(id result) {

                    }                     andFailBlock:^(NSError *error) {

                    }];

                    [[NSNotificationCenter defaultCenter] postNotificationName:DROPBOX_REFRESHED_NOTIFICATION object:nil];

                }                                  andFailBlock:^(NSError *error) {

                }];
            }
        }
        return YES;
    }
    else if ([url isFileURL])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_CAMERA_NOTIFICATION object:url];
    }
    else
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    
    return YES;
}


-(void) handleNotification:(NSDictionary*)userInfo appWasOpen:(BOOL)appWasOpen
{
    [self handleNotification:userInfo appWasOpen:appWasOpen isFromEmail:NO];
}

-(void) handleNotification:(NSDictionary*)userInfo appWasOpen:(BOOL)appWasOpen isFromEmail:(BOOL)isFromEmail
{
    
    if ([[UserManager sharedInstance] isAfterRegistration]) {
        return;
    }
    
    if (!isFromEmail)
    {
         [[Mixpanel sharedInstance] track:@"Open Notification from Popup"];
        
        if (!appWasOpen)
            [[Analytics sharedAnalytics] track:@"notification-open" properties:@{@"source" : @"push"}];

    }
    
    NSDictionary *bgColorDict = [userInfo valueForKey:@"bgColor"];
    UIColor *bgColor = nil;
    if (bgColorDict) {
        bgColor = [UIColor colorWithRed:[[bgColorDict valueForKey:@"r"] floatValue] green:[[bgColorDict valueForKey:@"g"] floatValue] blue:[[bgColorDict valueForKey:@"b"] floatValue] alpha:[[bgColorDict valueForKey:@"a"] floatValue]];
    }
    NSDictionary *textColorDict = [userInfo valueForKey:@"textColor"];
    UIColor *textColor = nil;
    if (textColorDict) {
        textColor = [UIColor colorWithRed:[[textColorDict valueForKey:@"r"] floatValue] green:[[textColorDict valueForKey:@"g"] floatValue] blue:[[textColorDict valueForKey:@"b"] floatValue] alpha:[[textColorDict valueForKey:@"a"] floatValue]];
    }
    int notificationType = [[userInfo valueForKey:@"notificationType"] intValue];
    if (notificationType == NotificationTypeInappFan1 || notificationType == NotificationTypeInappKids || notificationType == NotificationTypeInappFan2) {
        KPNotificationMessage *notification = [[KPNotificationMessage alloc] init];
        notification.message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        if (notificationType == NotificationTypeInappFan1) {
            if ([userInfo valueForKey:@"fileName"]) {
                notification.image = [UIImage imageNamed:[userInfo valueForKey:@"fileName"]];
            } else {
                notification.image = [UIImage imageNamed:@"icon-grandparents-message"];
            }
        } else {
            notification.image = [UIImage imageNamed:[userInfo valueForKey:@"fileName"]];
        }
       
        if (bgColor) {
            notification.bgColor = bgColor;
        }
        if (textColor) {
            notification.textColor = textColor;
        }
        notification.tapHandler = ^{
            if (notificationType == NotificationTypeInappFan1 || notificationType == NotificationTypeInappFan2) {
                         [[Mixpanel sharedInstance] track:@"invite fans popup - ok tap"];
                
                FansViewController *avc = [[FansViewController alloc] initWithNibName:@"FansViewController" bundle:nil];
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:avc];
                [self.viewController presentViewController:navController animated:YES completion:nil];
            } else if (notificationType == NotificationTypeInappKids) {
                         [[Mixpanel sharedInstance] track:@"add kids popup - ok tap"];
                
                EditKidViewController *editKidViewController = [[EditKidViewController alloc] initWithKid:nil];
                editKidViewController.delegate = self;
                [self.navController pushViewController:editKidViewController animated:YES];
            }
        };
        
        notification.dismissHandler = ^{
            if (notificationType == NotificationTypeInappFan1 || notificationType == NotificationTypeInappFan2) {
                         [[Mixpanel sharedInstance] track:@"invite fans popup - later tap"];
                
            } else if (notificationType == NotificationTypeInappKids) {
                         [[Mixpanel sharedInstance] track:@"add Kids popup - later tap"];
                
            }
        };
    
        [[KPNotificationCenter sharedCenter] revealNotification:notification];
    } else if (appWasOpen || notificationType == NotificationTypeFanApproved) {
        if (notificationType != NotificationTypeFanApproved) {
            [[ServerComm instance] getNotifications:^(id result) {
                [[KPDataParser instance] parseNotifications:[result valueForKey:@"result"]];
            } andFailBlock:^(NSError *error) {
                
            }];
        }
        
        KPNotificationMessage *notification = [[KPNotificationMessage alloc] init];
        
        // TODO: make sure that userInfo[aps][alert] does actually say something
        notification.message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        if (bgColor) {
            notification.bgColor = bgColor;
        }
        if (textColor) {
            notification.textColor = textColor;
        }
        notification.timeout = 5.0f;
        notification.tapHandler = ^{
            if (notificationType == NotificationTypeNewFan) {
                [self executeNotificationRequest:userInfo];
            } else {
                if (notificationType == NotificationTypeFanApproved) {
                    int fanRequestId = [[userInfo valueForKey:@"items"] intValue];
                    [SVProgressHUD showWithStatus:KPLocalizedString(@"updating")];
                    [[ServerComm instance] respondToFanRequest:fanRequestId andAccept:YES withSuccessBlock:^(id result) {
                        [[ServerComm instance] deleteNotification:[[userInfo valueForKey:@"notificationId"] intValue] withSuccessBlock:^(id result2) {
                            KPNotification *notification = [KPNotification MR_findFirstByAttribute:@"notificationId" withValue:[userInfo valueForKey:@"notificationId"]];
                            [notification MR_deleteEntity];
                            
                            [[UserManager sharedInstance] refreshMyData];
                            [GlobalUtils addFanPop:[result valueForKey:@"result"]];
                        } andFailBlock:^(NSError *error) {
                            
                        }];
                        [SVProgressHUD dismiss];
                    } andFailBlock:^(NSError *error) {
                        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"there was a problem connecting to keepy\'s servers, please try again soon")];
                    }];
                } else {
                    [[Analytics sharedAnalytics] track:@"notification-open" properties:@{@"source" : @"popup"}];
                    [self executeNotificationRequest:userInfo];
                }
            }
        };
        
        NSString *items = [userInfo valueForKey:@"items"];
        NSString *urlHash = [items componentsSeparatedByString:@","][0];
        if (notificationType == NotificationTypeItems) {
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@_small.jpg", Defaults.assetsURLString, urlHash]]];
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                if (!error) {
                    notification.image = [[UIImage alloc] initWithData:data];;
                }
                [[KPNotificationCenter sharedCenter] presentNotification:notification];
            }];
        } else if (notificationType != NotificationTypeFanApproved && notificationType != NotificationTypeNewFan) {
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg", Defaults.assetsURLString, urlHash]]];
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                if (!error) {
                    notification.image = [[UIImage alloc] initWithData:data];;
                }
                [[KPNotificationCenter sharedCenter] presentNotification:notification];
            }];
        } else {
            notification.image = [UIImage imageNamed:@"app-icon57x57"];
            [[KPNotificationCenter sharedCenter] presentNotification:notification];
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
   // NSUserDefaults.set(false, forKey: "isNewNoticationReceived");
   // [userDefs setObject:@(1) forKey:[NSString stringWithFormat:@"tip%ld", (long)tipIndex]];
   
    [NSUserDefaults.standardUserDefaults setBool:false forKey:@"isNewNoticationReceived"];
    [NSUserDefaults.standardUserDefaults synchronize];
    
    [self handleNotification:userInfo appWasOpen:(application.applicationState == UIApplicationStateActive)];
    
}

-(void) executeNotificationRequest:(NSDictionary*)userInfo
{
    [GlobalUtils execNotification:[[userInfo valueForKey:@"notificationId"] intValue] withType:[[userInfo valueForKey:@"notificationType"] intValue] andItems:[userInfo valueForKey:@"items"]];
}

#pragma mark Internet Connection Check methods

- (void)internetConnectionOFF
{
    if (self.isConnected)
        [SVProgressHUD showErrorWithStatus:KPLocalizedString(@"There is a problem with your internet connection")];
    self.isConnected = NO;
    
    if (isFirstReachabilityNotification)
    {
        isFirstReachabilityNotification = NO;
        //[self createNavigationController];
    }
}

- (void)internetConnectionON
{
    if (isFirstReachabilityNotification)
    {
        isFirstReachabilityNotification = NO;
        //[self createNavigationController];
    }
    else
    {
        if (!self.isConnected)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:INTERNET_CONNECTION_REVIVED_NOTIFICATION object:nil];
            [SVProgressHUD showSuccessWithStatus:KPLocalizedString(@"Internet connection revived!")];
        }
    }
    self.isConnected = YES;
}


//#pragma mark -
//#pragma mark DBSessionDelegate methods
//
//- (void)sessionDidReceiveAuthorizationFailure:(DBSession*)session userId:(NSString *)userId {
//    self.dropboxUserId = userId;
//}

#pragma mark - EditKidDelegate

-(void)kidAdded:(Kid *)kid
{
    NSUInteger c1 = [[Item MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY kids in %@", [[UserManager sharedInstance] myKids]]] count];
    NSUInteger c2 = [[[UserManager sharedInstance] myKids] count];
    
    if (c1 <= c2)
    {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CHANGE_KID_NOTIFICATION object:kid];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KID_ADDED_NOTIFICATION object:nil];
        /*
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:@(-1), @"id", KPLocalizedString(@"add another kid?"), @"body", [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"no"), @"title", @"kpyme://nextstep", @"link", nil], [NSDictionary dictionaryWithObjectsAndKeys:KPLocalizedString(@"yes"), @"title", @"kpyme://addkid", @"link", nil],nil], @"buttons", nil];
        [GlobalUtils showKeepyPopup:d];
         */
    }
    else
    {
        [GlobalUtils fireTrigger:TRIGGER_TYPE_ADDKID];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CHANGE_KID_NOTIFICATION object:kid];    
}

#pragma mark - Reload tweaks

- (void)reloadTweaks {
    NSMutableDictionary *tweakValues = [NSMutableDictionary new];
    tweakValues[TweakPaywallActionButtonsPresentationTypeKey] = [NSNumber numberWithInt: MPTweakValue(TweakPaywallActionButtonsPresentationTypeKey, 1)];//1
    tweakValues[TweakPromoUnlimitedAfterSignUP] = [NSNumber numberWithInt: MPTweakValue(TweakPromoUnlimitedAfterSignUP, 0)];//0
    tweakValues[TweakPromoUnlimitedAfterUpload] = [NSNumber numberWithInt: MPTweakValue(TweakPromoUnlimitedAfterUpload, 0)];//0
    tweakValues[TweakPromoUnlimitedAfterOnboarding] = [NSNumber numberWithInt: MPTweakValue(TweakPromoUnlimitedAfterOnboarding, 0)];//0

    self.tweakValues = tweakValues;
}

@end

@implementation NSString (XQueryComponents)
- (NSString *)stringByDecodingURLFormat
{
    NSString *result = [self stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

- (NSString *)stringByEncodingURLFormat
{
    NSString *result = [self stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    result = [result stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

@end
