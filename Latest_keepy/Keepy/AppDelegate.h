//
//  AppDelegate.h
//  Keepy
//
//  Created by Yaniv Solnik on 2/5/13.
//  Copyright (c) 2013 Jhaniv LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PKRevealController/PKRevealController.h>

#define TweakPaywallActionButtonsPresentationTypeKey @"Paywall buttons type"
#define TweakPromoUnlimitedAfterSignUP @"Promo Unlimited Screen After SignUP"
#define TweakPromoUnlimitedAfterUpload @"Promo Unlimited Screen After Upload"
#define TweakPromoUnlimitedAfterOnboarding @"Promo Unlimited Screen After Onboarding"

@class HomeViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSDictionary<NSString *, id> *tweakValues;

@property (strong, nonatomic) PKRevealController *viewController;
@property (strong, nonatomic) UINavigationController *navController;
@property (nonatomic) BOOL isConnected;
@property (nonatomic,strong)    NSMutableArray *failedVideos;

@property(nonatomic,copy)NSDate *VideoDateCreated;

- (void)initializeDataStoreForEmail:(NSString *)emailAddress;

-(void) createHome;
-(void) createHome:(BOOL)isFromLogin;


@property (nonatomic) BOOL cleanTheUnreadNotificationNumber;

-(void) handleNotification:(NSDictionary*)userInfo appWasOpen:(BOOL)appWasOpen isFromEmail:(BOOL)isFromEmail;

- (void)reloadTweaks;
- (void)reloadTweaksForAfterSignUP;
@end
